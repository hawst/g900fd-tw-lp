.class public Lcom/sec/android/app/dictionary/download/DictDownloader;
.super Landroid/os/AsyncTask;
.source "DictDownloader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Long;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static DEBUG:Z


# instance fields
.field private TAG:Ljava/lang/String;

.field private mExpired:Z

.field private mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

.field private mNeedSizeUpdate:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    .line 30
    iput-boolean v1, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mExpired:Z

    .line 32
    iput-boolean v1, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mNeedSizeUpdate:Z

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " tid("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    iget-object v1, v1, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget v1, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 41
    iget-object v2, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    const-string v3, "dispose()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    iput-boolean v1, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mExpired:Z

    .line 43
    iget-object v2, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    if-eqz v2, :cond_0

    .line 44
    iget-object v2, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    iget v2, v2, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    move v0, v1

    .line 45
    .local v0, "success":Z
    :goto_0
    invoke-static {}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDictDownloadManager()Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    iget-object v3, v3, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget-object v3, v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->showCompleteToast(Ljava/lang/String;Z)V

    .line 47
    invoke-static {}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDictDownloadManager()Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->updateView(Z)V

    .line 48
    if-eqz v0, :cond_0

    .line 49
    sget-object v2, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->instance:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v2}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->notifySearchListAdapter()V

    .line 51
    .end local v0    # "success":Z
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    .line 52
    invoke-virtual {p0, v1}, Lcom/sec/android/app/dictionary/download/DictDownloader;->cancel(Z)Z

    .line 53
    invoke-static {}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDictDownloadManager()Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->startScheduler()V

    .line 54
    return-void

    .line 44
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 26
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    .line 67
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "doInBackground()"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/dictionary/download/DictDownloader;->isCancelled()Z

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 69
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "download canceled"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    const/16 v21, -0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    .line 197
    :goto_0
    return-object v21

    .line 72
    :cond_0
    const/16 v18, 0x0

    .line 73
    .local v18, "sourceUrl":Ljava/lang/String;
    const/16 v19, 0x0

    .line 74
    .local v19, "targetFile":Ljava/io/File;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mExpired:Z

    move/from16 v21, v0

    if-nez v21, :cond_2

    .line 75
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbPackageName:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/dictionary/download/DictSamsungApps;->getDownloadInfo(Ljava/lang/String;)Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mAppsResult:Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;

    .line 76
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mAppsResult:Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;

    move-object/from16 v21, v0

    if-nez v21, :cond_1

    .line 77
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "stub xml failed"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const/16 v21, -0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    goto :goto_0

    .line 81
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mAppsResult:Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;->downloadURI:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 82
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->getApkFilePath()Ljava/io/File;

    move-result-object v19

    .line 88
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/dictionary/download/DictDownloader;->isCancelled()Z

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_3

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "download canceled"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const/16 v21, -0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    goto/16 :goto_0

    .line 84
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "download expired"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/16 v21, -0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    goto/16 :goto_0

    .line 93
    :cond_3
    const-wide/16 v16, 0x0

    .line 94
    .local v16, "sourceFileSize":J
    const-wide/16 v14, 0x0

    .line 95
    .local v14, "savedFileSize":J
    const/4 v9, 0x0

    .line 96
    .local v9, "inputStream":Ljava/io/InputStream;
    const/4 v10, 0x0

    .line 97
    .local v10, "outputStream":Ljava/io/FileOutputStream;
    const/4 v6, 0x0

    .line 98
    .local v6, "conn":Ljava/net/HttpURLConnection;
    const/16 v21, 0x400

    move/from16 v0, v21

    new-array v4, v0, [B

    .line 99
    .local v4, "buffer":[B
    const/4 v5, 0x0

    .line 100
    .local v5, "bufferLength":I
    const/4 v12, 0x1

    .line 103
    .local v12, "retVal":Z
    :try_start_0
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_4

    .line 104
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 106
    :cond_4
    :try_start_1
    new-instance v11, Ljava/io/FileOutputStream;

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 112
    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .local v11, "outputStream":Ljava/io/FileOutputStream;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, ">> verify download url:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 114
    const/4 v6, 0x0

    .line 116
    :try_start_3
    new-instance v21, Ljava/net/URL;

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v21

    move-object/from16 v0, v21

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v6, v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 121
    const v21, 0xea60

    :try_start_4
    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 122
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 123
    const v21, 0xea60

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "<< response : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v21

    const/16 v22, 0xc8

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_11

    .line 127
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v21

    const-string v22, "content-length"

    invoke-interface/range {v21 .. v22}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    .line 128
    .local v8, "headerInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v8, :cond_e

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_e

    .line 129
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 130
    .local v13, "slength":Ljava/lang/String;
    if-nez v13, :cond_c

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "fail to get \'content-length\'"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    new-instance v21, Ljava/io/IOException;

    invoke-direct/range {v21 .. v21}, Ljava/io/IOException;-><init>()V

    throw v21
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 166
    .end local v8    # "headerInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "slength":Ljava/lang/String;
    :catch_0
    move-exception v7

    move-object v10, v11

    .line 167
    .end local v11    # "outputStream":Ljava/io/FileOutputStream;
    .local v7, "e":Ljava/io/IOException;
    .restart local v10    # "outputStream":Ljava/io/FileOutputStream;
    :goto_1
    :try_start_5
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 168
    const/4 v12, 0x0

    .line 171
    if-eqz v10, :cond_5

    .line 172
    :try_start_6
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    .line 176
    :cond_5
    :goto_2
    if-eqz v9, :cond_6

    .line 177
    :try_start_7
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    .line 180
    :cond_6
    :goto_3
    if-eqz v6, :cond_7

    .line 181
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 183
    .end local v7    # "e":Ljava/io/IOException;
    :cond_7
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "download "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v12, v0, :cond_15

    const-string v21, "success"

    :goto_5
    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mExpired:Z

    move/from16 v21, v0

    if-nez v21, :cond_8

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v12, v0, :cond_8

    .line 185
    const/16 v20, 0x0

    .line 186
    .local v20, "versionNumber":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->installPackage()I

    move-result v20

    if-ltz v20, :cond_16

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mState:I

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-object/from16 v21, v0

    move/from16 v0, v20

    move-object/from16 v1, v21

    iput v0, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mVersion:I

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDbServerSize:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbSize:J

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-object/from16 v21, v0

    const/16 v22, -0x2710

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "install success - version : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mVersion:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    sget-object v21, Lcom/sec/android/app/dictionary/db/DictDbManager;->instance:Lcom/sec/android/app/dictionary/db/DictDbManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/dictionary/db/DictDbManager;->saveItem(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V

    .line 197
    .end local v20    # "versionNumber":I
    :cond_8
    :goto_6
    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v12, v0, :cond_17

    const/16 v21, 0x1

    :goto_7
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    goto/16 :goto_0

    .line 107
    :catch_1
    move-exception v7

    .line 108
    .local v7, "e":Ljava/io/FileNotFoundException;
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "fail to open stream: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    throw v7
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 166
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v7

    goto/16 :goto_1

    .line 117
    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v11    # "outputStream":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v7

    .line 118
    .local v7, "e":Ljava/io/IOException;
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "url connection fail : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    throw v7
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 170
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v21

    move-object v10, v11

    .line 171
    .end local v11    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v10    # "outputStream":Ljava/io/FileOutputStream;
    :goto_8
    if-eqz v10, :cond_9

    .line 172
    :try_start_a
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 176
    :cond_9
    :goto_9
    if-eqz v9, :cond_a

    .line 177
    :try_start_b
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9

    .line 180
    :cond_a
    :goto_a
    if-eqz v6, :cond_b

    .line 181
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_b
    throw v21

    .line 134
    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "headerInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v11    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v13    # "slength":Ljava/lang/String;
    :cond_c
    :try_start_c
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 135
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mExpired:Z

    move/from16 v21, v0

    if-nez v21, :cond_d

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    move-wide/from16 v0, v16

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDbServerSize:J

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDbServerSize:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbSize:J

    move-wide/from16 v24, v0

    cmp-long v21, v22, v24

    if-eqz v21, :cond_d

    .line 138
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/dictionary/download/DictDownloader;->mNeedSizeUpdate:Z

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDbServerSize:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbSize:J

    .line 140
    sget-object v21, Lcom/sec/android/app/dictionary/db/DictDbManager;->instance:Lcom/sec/android/app/dictionary/db/DictDbManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/dictionary/db/DictDbManager;->saveItem(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V

    .line 144
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mExpired:Z

    move/from16 v21, v0

    if-nez v21, :cond_e

    .line 145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "download start : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "bytes"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    .end local v13    # "slength":Ljava/lang/String;
    :cond_e
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    .line 150
    :goto_b
    invoke-virtual {v9, v4}, Ljava/io/InputStream;->read([B)I

    move-result v5

    if-lez v5, :cond_12

    .line 151
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v11, v4, v0, v5}, Ljava/io/FileOutputStream;->write([BII)V

    .line 152
    int-to-long v0, v5

    move-wide/from16 v22, v0

    add-long v14, v14, v22

    .line 153
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mExpired:Z

    move/from16 v21, v0

    if-nez v21, :cond_f

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iput-wide v14, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDownloadedSize:J

    .line 156
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/dictionary/download/DictDownloader;->isCancelled()Z

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_10

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "download canceled"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    new-instance v21, Ljava/io/IOException;

    invoke-direct/range {v21 .. v21}, Ljava/io/IOException;-><init>()V

    throw v21

    .line 160
    :cond_10
    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Long;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/download/DictDownloader;->publishProgress([Ljava/lang/Object;)V

    goto :goto_b

    .line 163
    .end local v8    # "headerInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "http response code : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    new-instance v21, Ljava/io/IOException;

    invoke-direct/range {v21 .. v21}, Ljava/io/IOException;-><init>()V

    throw v21
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 171
    .restart local v8    # "headerInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_12
    if-eqz v11, :cond_13

    .line 172
    :try_start_d
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4

    .line 176
    :cond_13
    :goto_c
    if-eqz v9, :cond_14

    .line 177
    :try_start_e
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5

    .line 180
    :cond_14
    :goto_d
    if-eqz v6, :cond_18

    .line 181
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v10, v11

    .end local v11    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v10    # "outputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_4

    .line 183
    .end local v8    # "headerInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_15
    const-string v21, "failed"

    goto/16 :goto_5

    .line 194
    .restart local v20    # "versionNumber":I
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "install failed"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 197
    .end local v20    # "versionNumber":I
    :cond_17
    const/16 v21, -0x1

    goto/16 :goto_7

    .line 173
    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "headerInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v11    # "outputStream":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v21

    goto :goto_c

    .line 178
    :catch_5
    move-exception v21

    goto :goto_d

    .line 173
    .end local v8    # "headerInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v11    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "e":Ljava/io/IOException;
    .restart local v10    # "outputStream":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v21

    goto/16 :goto_2

    .line 178
    :catch_7
    move-exception v21

    goto/16 :goto_3

    .line 173
    .end local v7    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v22

    goto/16 :goto_9

    .line 178
    :catch_9
    move-exception v22

    goto/16 :goto_a

    .line 170
    :catchall_1
    move-exception v21

    goto/16 :goto_8

    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "headerInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v11    # "outputStream":Ljava/io/FileOutputStream;
    :cond_18
    move-object v10, v11

    .end local v11    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v10    # "outputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_4
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 23
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/download/DictDownloader;->doInBackground([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    const-string v1, "onCancelled()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 228
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    const-string v1, "onPostExecute()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->reset(I)V

    .line 221
    :goto_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 222
    return-void

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->reset(I)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 23
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/download/DictDownloader;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mExpired:Z

    if-nez v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mState:I

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    const-string v1, "onPreExecute()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 63
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Long;)V
    .locals 5
    .param p1, "values"    # [Ljava/lang/Long;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 202
    sget-boolean v0, Lcom/sec/android/app/dictionary/download/DictDownloader;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onProgressUpdate() :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p1, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p1, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mExpired:Z

    if-nez v0, :cond_1

    .line 205
    invoke-static {}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDictDownloadManager()Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mInfo:Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->updateDownloadProgress(Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;)V

    .line 207
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mNeedSizeUpdate:Z

    if-ne v0, v4, :cond_2

    .line 208
    iput-boolean v3, p0, Lcom/sec/android/app/dictionary/download/DictDownloader;->mNeedSizeUpdate:Z

    .line 209
    invoke-static {}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->getDictDownloadManager()Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->updateView(Z)V

    .line 211
    :cond_2
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 212
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 23
    check-cast p1, [Ljava/lang/Long;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/download/DictDownloader;->onProgressUpdate([Ljava/lang/Long;)V

    return-void
.end method
