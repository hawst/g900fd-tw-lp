.class public final Lcom/sec/android/app/dictionary/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final CR_ABBYY:I = 0x7f07000c

.field public static final CR_CHN_SIMP:I = 0x7f070023

.field public static final CR_CHN_TRAD:I = 0x7f070024

.field public static final CR_DEU:I = 0x7f070007

.field public static final CR_ENJP:I = 0x7f07001e

.field public static final CR_ESP:I = 0x7f070006

.field public static final CR_FAR:I = 0x7f070019

.field public static final CR_FIN:I = 0x7f070011

.field public static final CR_FRE:I = 0x7f070001

.field public static final CR_GLE:I = 0x7f070014

.field public static final CR_GRE:I = 0x7f070004

.field public static final CR_HIN:I = 0x7f070018

.field public static final CR_IND:I = 0x7f070015

.field public static final CR_ITA:I = 0x7f070002

.field public static final CR_NDL:I = 0x7f07000e

.field public static final CR_NOR:I = 0x7f070010

.field public static final CR_POL:I = 0x7f070005

.field public static final CR_POR:I = 0x7f070003

.field public static final CR_SWE:I = 0x7f07000f

.field public static final CR_THA:I = 0x7f070016

.field public static final CR_TUR:I = 0x7f07000d

.field public static final CR_URD:I = 0x7f07001a

.field public static final CR_VIE:I = 0x7f070017

.field public static final CR_arEn:I = 0x7f070009

.field public static final CR_cnKo:I = 0x7f070021

.field public static final CR_danEn:I = 0x7f070013

.field public static final CR_enAr:I = 0x7f070008

.field public static final CR_enDan:I = 0x7f070012

.field public static final CR_enMa:I = 0x7f07000a

.field public static final CR_eng:I = 0x7f070000

.field public static final CR_enko:I = 0x7f07001b

.field public static final CR_jpKo:I = 0x7f07001f

.field public static final CR_koCn:I = 0x7f070022

.field public static final CR_koJp:I = 0x7f070020

.field public static final CR_koen:I = 0x7f07001c

.field public static final CR_kor:I = 0x7f07001d

.field public static final CR_maEn:I = 0x7f07000b

.field public static final about:I = 0x7f070030

.field public static final app_name:I = 0x7f070025

.field public static final arabic_to_english:I = 0x7f070058

.field public static final avilable_dictionaries:I = 0x7f070029

.field public static final cancel:I = 0x7f070038

.field public static final change_order:I = 0x7f07002c

.field public static final chinese_simp_to_english:I = 0x7f070081

.field public static final chinese_simp_to_korean:I = 0x7f07007f

.field public static final chinese_trad_to_english:I = 0x7f070083

.field public static final dansk_to_english:I = 0x7f070068

.field public static final data_connection_dialog_checkbox:I = 0x7f070034

.field public static final data_connection_dialog_summary:I = 0x7f070033

.field public static final data_connection_dialog_summary_china:I = 0x7f070032

.field public static final data_connection_dialog_summary_roam:I = 0x7f070036

.field public static final data_connection_dialog_title:I = 0x7f070031

.field public static final data_connection_dialog_title_roam:I = 0x7f070035

.field public static final dbname:I = 0x7f070044

.field public static final delete:I = 0x7f07003c

.field public static final deutsch_to_english:I = 0x7f070054

.field public static final done:I = 0x7f07002d

.field public static final download:I = 0x7f07003d

.field public static final download_failed:I = 0x7f07002f

.field public static final download_success:I = 0x7f07002e

.field public static final downloaded_dictionaries:I = 0x7f070028

.field public static final english_to_arabic:I = 0x7f070057

.field public static final english_to_chinese_simp:I = 0x7f070080

.field public static final english_to_chinese_trad:I = 0x7f070082

.field public static final english_to_dansk:I = 0x7f070067

.field public static final english_to_deutsch:I = 0x7f070053

.field public static final english_to_english:I = 0x7f070046

.field public static final english_to_farsi:I = 0x7f070073

.field public static final english_to_finnish:I = 0x7f070065

.field public static final english_to_french:I = 0x7f070047

.field public static final english_to_greek:I = 0x7f07004d

.field public static final english_to_hindi:I = 0x7f070071

.field public static final english_to_indonesia:I = 0x7f07006b

.field public static final english_to_irish:I = 0x7f070069

.field public static final english_to_italian:I = 0x7f070049

.field public static final english_to_japanese:I = 0x7f07007a

.field public static final english_to_korean:I = 0x7f070077

.field public static final english_to_malay:I = 0x7f070055

.field public static final english_to_nederlands:I = 0x7f07005f

.field public static final english_to_norsk:I = 0x7f070063

.field public static final english_to_polish:I = 0x7f07004f

.field public static final english_to_portuguese:I = 0x7f07004b

.field public static final english_to_russian:I = 0x7f070059

.field public static final english_to_spanish:I = 0x7f070051

.field public static final english_to_sweden:I = 0x7f070061

.field public static final english_to_thai:I = 0x7f07006d

.field public static final english_to_turkisch:I = 0x7f07005d

.field public static final english_to_ukrainian:I = 0x7f07005b

.field public static final english_to_urdu:I = 0x7f070075

.field public static final english_to_vietnamese:I = 0x7f07006f

.field public static final farsi_to_english:I = 0x7f070074

.field public static final finnish_to_english:I = 0x7f070066

.field public static final french_to_english:I = 0x7f070048

.field public static final greek_to_english:I = 0x7f07004e

.field public static final header:I = 0x7f07003b

.field public static final hindi_to_english:I = 0x7f070072

.field public static final indonesia_to_english:I = 0x7f07006c

.field public static final irish_to_english:I = 0x7f07006a

.field public static final italian_to_english:I = 0x7f07004a

.field public static final japanese_to_english:I = 0x7f07007b

.field public static final japanese_to_korean:I = 0x7f07007c

.field public static final korean_to_chinese_simp:I = 0x7f07007e

.field public static final korean_to_english:I = 0x7f070078

.field public static final korean_to_japanese:I = 0x7f07007d

.field public static final korean_to_korean:I = 0x7f070079

.field public static final lang_arabic:I = 0x7f07009d

.field public static final lang_bengali:I = 0x7f0700a4

.field public static final lang_chinese:I = 0x7f070088

.field public static final lang_chinese_pinyin:I = 0x7f07008c

.field public static final lang_chinese_pinyin_initial:I = 0x7f07008d

.field public static final lang_chinese_simp:I = 0x7f070089

.field public static final lang_chinese_simptrad:I = 0x7f07008b

.field public static final lang_chinese_trad:I = 0x7f07008a

.field public static final lang_chinese_zhuyin:I = 0x7f0700b0

.field public static final lang_danish:I = 0x7f070098

.field public static final lang_dutch:I = 0x7f070099

.field public static final lang_eastern:I = 0x7f0700ac

.field public static final lang_engchnjpnkor:I = 0x7f0700ae

.field public static final lang_english:I = 0x7f070086

.field public static final lang_english_uk:I = 0x7f070087

.field public static final lang_espanol:I = 0x7f070092

.field public static final lang_finnish:I = 0x7f07009c

.field public static final lang_french:I = 0x7f070090

.field public static final lang_gaeilge:I = 0x7f0700aa

.field public static final lang_german:I = 0x7f070091

.field public static final lang_greek:I = 0x7f0700a8

.field public static final lang_hanja:I = 0x7f0700b1

.field public static final lang_hindi:I = 0x7f0700a3

.field public static final lang_indonesian:I = 0x7f07009e

.field public static final lang_italian:I = 0x7f070094

.field public static final lang_japanese:I = 0x7f07008e

.field public static final lang_japanese_kanji:I = 0x7f07008f

.field public static final lang_korean:I = 0x7f070084

.field public static final lang_korean_old:I = 0x7f070085

.field public static final lang_malaysian:I = 0x7f0700a2

.field public static final lang_norwegian:I = 0x7f07009a

.field public static final lang_persian:I = 0x7f0700a6

.field public static final lang_polish:I = 0x7f0700a9

.field public static final lang_portuguese:I = 0x7f070093

.field public static final lang_russian:I = 0x7f070096

.field public static final lang_swedish:I = 0x7f07009b

.field public static final lang_tagalog:I = 0x7f0700a7

.field public static final lang_thai:I = 0x7f0700a0

.field public static final lang_thai_phonetic:I = 0x7f0700a1

.field public static final lang_turkish:I = 0x7f070095

.field public static final lang_ukrainian:I = 0x7f070097

.field public static final lang_urdu:I = 0x7f0700a5

.field public static final lang_urdu_arabic:I = 0x7f0700af

.field public static final lang_vietnamese:I = 0x7f07009f

.field public static final lang_western:I = 0x7f0700ab

.field public static final lang_world:I = 0x7f0700ad

.field public static final malay_to_english:I = 0x7f070056

.field public static final manage_dictionary:I = 0x7f070027

.field public static final mobile_data_connect_title:I = 0x7f070039

.field public static final nederlands_to_english:I = 0x7f070060

.field public static final no_dictionary:I = 0x7f07002a

.field public static final no_matches:I = 0x7f07002b

.field public static final norsk_to_english:I = 0x7f070064

.field public static final ok:I = 0x7f070037

.field public static final polish_to_english:I = 0x7f070050

.field public static final portuguese_to_english:I = 0x7f07004c

.field public static final russian_to_english:I = 0x7f07005a

.field public static final scrapbook:I = 0x7f07003f

.field public static final search_web:I = 0x7f070026

.field public static final spanish_to_english:I = 0x7f070052

.field public static final stms_appgroup:I = 0x7f0700b2

.field public static final sweden_to_english:I = 0x7f070062

.field public static final talkback_changeorder:I = 0x7f07003e

.field public static final tap_to_download:I = 0x7f070045

.field public static final test_h:I = 0x7f070041

.field public static final test_keyword:I = 0x7f070043

.field public static final test_provider:I = 0x7f070042

.field public static final test_w:I = 0x7f070040

.field public static final thai_to_english:I = 0x7f07006e

.field public static final turkisch_to_english:I = 0x7f07005e

.field public static final ukrainian_to_english:I = 0x7f07005c

.field public static final urdu_to_english:I = 0x7f070076

.field public static final use_data_through_current_connect:I = 0x7f07003a

.field public static final vietnamese_to_english:I = 0x7f070070


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
