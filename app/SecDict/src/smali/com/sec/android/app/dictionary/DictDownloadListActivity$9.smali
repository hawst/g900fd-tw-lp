.class Lcom/sec/android/app/dictionary/DictDownloadListActivity$9;
.super Ljava/lang/Object;
.source "DictDownloadListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/DictDownloadListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$9;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 210
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 211
    .local v2, "pos":I
    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    invoke-static {}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$100()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 212
    .local v0, "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    if-eqz v0, :cond_0

    .line 213
    new-instance v1, Ljava/io/File;

    iget-object v3, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDBPath:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDBFileName:Ljava/lang/String;

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 215
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 216
    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    invoke-static {}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$100()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->updateView(Z)Z

    .line 219
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    return-void
.end method
