.class final enum Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;
.super Ljava/lang/Enum;
.source "DiotekEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "CssType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

.field public static final enum COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

.field public static final enum DIMENS:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 299
    new-instance v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const-string v1, "COLOR"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    new-instance v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const-string v1, "DIMENS"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->DIMENS:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    .line 298
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    sget-object v1, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->DIMENS:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->$VALUES:[Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 298
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 298
    const-class v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;
    .locals 1

    .prologue
    .line 298
    sget-object v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->$VALUES:[Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    invoke-virtual {v0}, [Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    return-object v0
.end method
