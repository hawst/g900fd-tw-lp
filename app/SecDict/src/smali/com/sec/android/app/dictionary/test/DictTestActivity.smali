.class public Lcom/sec/android/app/dictionary/test/DictTestActivity;
.super Landroid/app/Activity;
.source "DictTestActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;,
        Lcom/sec/android/app/dictionary/test/DictTestActivity$CheckForLongPress;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field isPopupMode:Z

.field mDrawingView:Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;

.field mH:I

.field private mHandler:Landroid/os/Handler;

.field private mHasPerformedLongPress:Z

.field mInputBox:Landroid/widget/EditText;

.field mInputHeight:Landroid/widget/EditText;

.field mInputWidth:Landroid/widget/EditText;

.field private mLastMotionX:F

.field private mLastMotionY:F

.field private mPendingCheckForLongPress:Lcom/sec/android/app/dictionary/test/DictTestActivity$CheckForLongPress;

.field mProviderButton:Landroid/widget/Button;

.field private mTouchSlop:I

.field mW:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->TAG:Ljava/lang/String;

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->isPopupMode:Z

    .line 73
    iput v2, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionX:F

    .line 75
    iput v2, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionY:F

    .line 85
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mHandler:Landroid/os/Handler;

    .line 233
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/dictionary/test/DictTestActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/test/DictTestActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mHasPerformedLongPress:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/dictionary/test/DictTestActivity;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/test/DictTestActivity;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionX:F

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/dictionary/test/DictTestActivity;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/dictionary/test/DictTestActivity;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionY:F

    return v0
.end method

.method private performOneClick()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 214
    const-string v0, "CLICK"

    const-string v1, "One Click OK"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    iget-boolean v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->isPopupMode:Z

    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputBox:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mDrawingView:Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->setVisibility(I)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputBox:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputWidth:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputHeight:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 231
    :goto_0
    return-void

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mDrawingView:Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->setVisibility(I)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputBox:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputWidth:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputHeight:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_0

    .line 229
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->performLongClick()Z

    goto :goto_0
.end method

.method private postCheckForLongClick(I)V
    .locals 4
    .param p1, "delayOffset"    # I

    .prologue
    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mHasPerformedLongPress:Z

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mPendingCheckForLongPress:Lcom/sec/android/app/dictionary/test/DictTestActivity$CheckForLongPress;

    if-nez v0, :cond_0

    .line 167
    new-instance v0, Lcom/sec/android/app/dictionary/test/DictTestActivity$CheckForLongPress;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/test/DictTestActivity$CheckForLongPress;-><init>(Lcom/sec/android/app/dictionary/test/DictTestActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mPendingCheckForLongPress:Lcom/sec/android/app/dictionary/test/DictTestActivity$CheckForLongPress;

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mPendingCheckForLongPress:Lcom/sec/android/app/dictionary/test/DictTestActivity$CheckForLongPress;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v2

    sub-int/2addr v2, p1

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 174
    return-void
.end method

.method private removeLongPressCallback()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mPendingCheckForLongPress:Lcom/sec/android/app/dictionary/test/DictTestActivity$CheckForLongPress;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mPendingCheckForLongPress:Lcom/sec/android/app/dictionary/test/DictTestActivity$CheckForLongPress;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 180
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, -0x1

    .line 44
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v0, 0x7f03000c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->setContentView(I)V

    .line 48
    const v0, 0x7f0b001f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputBox:Landroid/widget/EditText;

    .line 49
    const v0, 0x7f0b0020

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputWidth:Landroid/widget/EditText;

    .line 50
    const v0, 0x7f0b0021

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputHeight:Landroid/widget/EditText;

    .line 51
    const v0, 0x7f0b0022

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mProviderButton:Landroid/widget/Button;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mProviderButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/dictionary/test/DictTestActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/dictionary/test/DictTestActivity$1;-><init>(Lcom/sec/android/app/dictionary/test/DictTestActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 63
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mTouchSlop:I

    .line 65
    new-instance v0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;-><init>(Lcom/sec/android/app/dictionary/test/DictTestActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mDrawingView:Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mDrawingView:Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mDrawingView:Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->setVisibility(I)V

    .line 70
    invoke-static {p0}, Lcom/sec/android/app/dictionary/util/DictUtils;->init(Landroid/content/Context;)V

    .line 71
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    .line 99
    sget-boolean v4, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 100
    iget-object v4, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 159
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    return v4

    .line 105
    :pswitch_0
    const-string v4, "CLICK"

    const-string v5, "ACTION_DOWN"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionX:F

    .line 108
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionY:F

    .line 110
    iput-boolean v6, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mHasPerformedLongPress:Z

    .line 112
    invoke-direct {p0, v6}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->postCheckForLongClick(I)V

    goto :goto_0

    .line 117
    :pswitch_1
    const-string v4, "CLICK"

    const-string v5, "ACTION_MOVE"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 120
    .local v2, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 121
    .local v3, "y":F
    iget v4, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionX:F

    sub-float/2addr v4, v2

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 122
    .local v0, "deltaX":I
    iget v4, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionY:F

    sub-float/2addr v4, v3

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 125
    .local v1, "deltaY":I
    iget v4, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mTouchSlop:I

    if-ge v0, v4, :cond_2

    iget v4, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mTouchSlop:I

    if-lt v1, v4, :cond_1

    .line 126
    :cond_2
    iget-boolean v4, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mHasPerformedLongPress:Z

    if-nez v4, :cond_1

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->removeLongPressCallback()V

    goto :goto_0

    .line 135
    .end local v0    # "deltaX":I
    .end local v1    # "deltaY":I
    .end local v2    # "x":F
    .end local v3    # "y":F
    :pswitch_2
    iget-boolean v4, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mHasPerformedLongPress:Z

    if-nez v4, :cond_1

    .line 137
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->removeLongPressCallback()V

    goto :goto_0

    .line 142
    :pswitch_3
    const-string v4, "CLICK"

    const-string v5, "ACTION_UP"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget-boolean v4, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mHasPerformedLongPress:Z

    if-nez v4, :cond_1

    .line 146
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->removeLongPressCallback()V

    .line 149
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->performOneClick()V

    goto :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public performLongClick()Z
    .locals 6

    .prologue
    .line 184
    const-string v0, "CLICK"

    const-string v2, "Long Click OK"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputWidth:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mW:I

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputHeight:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mH:I

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mDrawingView:Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->invalidate()V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mInputBox:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 190
    .local v1, "word":Ljava/lang/String;
    iget v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionX:F

    float-to-int v2, v0

    iget v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionY:F

    float-to-int v3, v0

    iget v4, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mW:I

    iget v5, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mH:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->startSearchActivity(Ljava/lang/String;IIII)V

    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method public startSearchActivity(Ljava/lang/String;IIII)V
    .locals 2
    .param p1, "word"    # Ljava/lang/String;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "w"    # I
    .param p5, "h"    # I

    .prologue
    .line 195
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 197
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.dictionary.SEARCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    const-string v1, "keyword"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 201
    const-string v1, "X"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 202
    const-string v1, "Y"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 203
    const-string v1, "W"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 204
    const-string v1, "H"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 207
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :goto_0
    return-void

    .line 208
    :catch_0
    move-exception v1

    goto :goto_0
.end method
