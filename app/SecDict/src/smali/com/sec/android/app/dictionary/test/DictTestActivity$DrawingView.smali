.class public Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;
.super Landroid/view/View;
.source "DictTestActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/dictionary/test/DictTestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DrawingView"
.end annotation


# instance fields
.field paint:Landroid/graphics/Paint;

.field final synthetic this$0:Lcom/sec/android/app/dictionary/test/DictTestActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/dictionary/test/DictTestActivity;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 236
    iput-object p1, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->this$0:Lcom/sec/android/app/dictionary/test/DictTestActivity;

    .line 237
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 234
    new-instance v0, Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->paint:Landroid/graphics/Paint;

    .line 238
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->init()V

    .line 239
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/dictionary/test/DictTestActivity;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->this$0:Lcom/sec/android/app/dictionary/test/DictTestActivity;

    .line 247
    invoke-direct {p0, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 234
    new-instance v0, Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->paint:Landroid/graphics/Paint;

    .line 248
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->init()V

    .line 249
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/dictionary/test/DictTestActivity;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .param p4, "defStyle"    # I

    .prologue
    .line 241
    iput-object p1, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->this$0:Lcom/sec/android/app/dictionary/test/DictTestActivity;

    .line 242
    invoke-direct {p0, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 234
    new-instance v0, Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->paint:Landroid/graphics/Paint;

    .line 243
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->init()V

    .line 244
    return-void
.end method

.method private final init()V
    .locals 1

    .prologue
    .line 252
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->setBackgroundColor(I)V

    .line 253
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 257
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->paint:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->this$0:Lcom/sec/android/app/dictionary/test/DictTestActivity;

    # getter for: Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionX:F
    invoke-static {v0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->access$100(Lcom/sec/android/app/dictionary/test/DictTestActivity;)F

    move-result v0

    float-to-int v0, v0

    int-to-float v1, v0

    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->this$0:Lcom/sec/android/app/dictionary/test/DictTestActivity;

    # getter for: Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionY:F
    invoke-static {v0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->access$200(Lcom/sec/android/app/dictionary/test/DictTestActivity;)F

    move-result v0

    float-to-int v0, v0

    int-to-float v2, v0

    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->this$0:Lcom/sec/android/app/dictionary/test/DictTestActivity;

    # getter for: Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionX:F
    invoke-static {v0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->access$100(Lcom/sec/android/app/dictionary/test/DictTestActivity;)F

    move-result v0

    iget-object v3, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->this$0:Lcom/sec/android/app/dictionary/test/DictTestActivity;

    iget v3, v3, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mW:I

    int-to-float v3, v3

    add-float/2addr v3, v0

    iget-object v0, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->this$0:Lcom/sec/android/app/dictionary/test/DictTestActivity;

    # getter for: Lcom/sec/android/app/dictionary/test/DictTestActivity;->mLastMotionY:F
    invoke-static {v0}, Lcom/sec/android/app/dictionary/test/DictTestActivity;->access$200(Lcom/sec/android/app/dictionary/test/DictTestActivity;)F

    move-result v0

    iget-object v4, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->this$0:Lcom/sec/android/app/dictionary/test/DictTestActivity;

    iget v4, v4, Lcom/sec/android/app/dictionary/test/DictTestActivity;->mH:I

    int-to-float v4, v4

    add-float/2addr v4, v0

    iget-object v5, p0, Lcom/sec/android/app/dictionary/test/DictTestActivity$DrawingView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 265
    return-void
.end method
