.class public Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;
.super Ljava/lang/Object;
.source "DictDownloadInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo$DicDownloadState;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field public mAppsResult:Lcom/sec/android/app/dictionary/download/DictSamsungApps$StubDownloadResult;

.field public mDbServerSize:J

.field public mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

.field public mDownloadedDbFile:Ljava/lang/String;

.field public mDownloadedSize:J

.field public mDownloader:Lcom/sec/android/app/dictionary/download/DictDownloader;

.field public mLocalFile:Lcom/sec/android/app/dictionary/download/DictFile;

.field public mState:I

.field public mUpdatable:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V
    .locals 6
    .param p1, "dictDataInfo"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->TAG:Ljava/lang/String;

    .line 32
    iput v2, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mState:I

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDownloadedDbFile:Ljava/lang/String;

    .line 36
    iput-wide v4, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDbServerSize:J

    .line 38
    iput-wide v4, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDownloadedSize:J

    .line 48
    iput-boolean v2, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mUpdatable:Z

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 53
    return-void
.end method


# virtual methods
.method public getApkFilePath()Ljava/io/File;
    .locals 3

    .prologue
    .line 78
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_CACHE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget-object v2, v2, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public installPackage()I
    .locals 4

    .prologue
    .line 82
    iget-object v1, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mLocalFile:Lcom/sec/android/app/dictionary/download/DictFile;

    sget-object v2, Lcom/sec/android/app/dictionary/download/DictDownloadService;->mServiceContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/dictionary/download/DictFile;->getVersionNumber(Landroid/content/Context;)I

    move-result v0

    .line 83
    .local v0, "version":I
    if-lez v0, :cond_0

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mLocalFile:Lcom/sec/android/app/dictionary/download/DictFile;

    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_DB:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/dictionary/download/DictFile;->extractTo(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 85
    const/4 v0, -0x1

    .line 87
    .end local v0    # "version":I
    :cond_0
    return v0
.end method

.method public isDownloading()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 70
    iget v1, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mState:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mState:I

    if-ne v1, v0, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final reset(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    const-wide/16 v2, 0x0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->TAG:Ljava/lang/String;

    const-string v1, "reset()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iput p1, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mState:I

    .line 58
    iput-wide v2, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDbServerSize:J

    .line 59
    iput-wide v2, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDownloadedSize:J

    .line 60
    new-instance v0, Lcom/sec/android/app/dictionary/download/DictFile;

    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->getApkFilePath()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/dictionary/download/DictFile;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mLocalFile:Lcom/sec/android/app/dictionary/download/DictFile;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mLocalFile:Lcom/sec/android/app/dictionary/download/DictFile;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/download/DictFile;->clearCacheFile()V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDownloader:Lcom/sec/android/app/dictionary/download/DictDownloader;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDownloader:Lcom/sec/android/app/dictionary/download/DictDownloader;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/download/DictDownloader;->dispose()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDownloader:Lcom/sec/android/app/dictionary/download/DictDownloader;

    .line 67
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v4, 0xa

    .line 91
    const/4 v2, 0x5

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "NOT_INSTALLED"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "READY"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "DOWNLOADIG"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "INSTALLED"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "FAILED"

    aput-object v3, v1, v2

    .line 94
    .local v1, "stateStr":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 95
    .local v0, "retVal":Ljava/lang/StringBuffer;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Dict("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget v3, v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mState:I

    add-int/lit8 v3, v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget-object v3, v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 97
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 98
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  package name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget-object v3, v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", versionNumber : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDictDataInfo:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget v3, v3, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 101
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  fileSize : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDbServerSize:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/dictionary/datainfo/DictDownloadInfo;->mDownloadedSize:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 102
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
