.class public Lcom/sec/android/app/dictionary/engine/DictEngineManager;
.super Ljava/lang/Object;
.source "DictEngineManager.java"


# static fields
.field public static final MAX_KEYWORD:I = 0x64

.field public static final instance:Lcom/sec/android/app/dictionary/engine/DictEngineManager;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mAvailableDictCount:I

.field private mContext:Landroid/content/Context;

.field private mDbManager:Lcom/sec/android/app/dictionary/db/DictDbManager;

.field mDictDataInfoComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

.field private mFontTypeface:Landroid/graphics/Typeface;

.field private mFullDictList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mFullDictMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchAdapter:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/DictSearchListAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-direct {v0}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->instance:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->TAG:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    .line 30
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictMap:Landroid/util/SparseArray;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mSearchAdapter:Ljava/util/List;

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mAvailableDictCount:I

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mContext:Landroid/content/Context;

    .line 42
    sget-object v0, Lcom/sec/android/app/dictionary/db/DictDbManager;->instance:Lcom/sec/android/app/dictionary/db/DictDbManager;

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mDbManager:Lcom/sec/android/app/dictionary/db/DictDbManager;

    .line 108
    new-instance v0, Lcom/sec/android/app/dictionary/engine/DictEngineManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/dictionary/engine/DictEngineManager$1;-><init>(Lcom/sec/android/app/dictionary/engine/DictEngineManager;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mDictDataInfoComparator:Ljava/util/Comparator;

    .line 53
    new-instance v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;

    invoke-direct {v0}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/dictionary/engine/DictEngine;->setEngineManager(Lcom/sec/android/app/dictionary/engine/DictEngineManager;)V

    .line 55
    return-void
.end method


# virtual methods
.method public addSearchListAdapter(Lcom/sec/android/app/dictionary/DictSearchListAdapter;)V
    .locals 1
    .param p1, "searchAdapter"    # Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    .prologue
    .line 202
    iget v0, p1, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mRefCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mRefCount:I

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mSearchAdapter:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    return-void
.end method

.method public getAvailableDictCount()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mAvailableDictCount:I

    return v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->TAG:Ljava/lang/String;

    const-string v1, "getContext() : context is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDictDataInfo(I)Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    return-object v0
.end method

.method public getDictDataInfo(Ljava/lang/String;)Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 147
    iget-object v3, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    monitor-enter v3

    .line 148
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 149
    .local v1, "info":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-object v2, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDbPackageName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 150
    monitor-exit v3

    .line 154
    .end local v1    # "info":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    :goto_0
    return-object v1

    .line 153
    :cond_1
    monitor-exit v3

    .line 154
    const/4 v1, 0x0

    goto :goto_0

    .line 153
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getDictDataInfoMap()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictMap:Landroid/util/SparseArray;

    return-object v0
.end method

.method public getDictList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    return-object v0
.end method

.method public getFontTypeface()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFontTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getMeaning(Ljava/lang/String;Ljava/util/List;)[Ljava/lang/String;
    .locals 2
    .param p1, "keyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p2, "searchKeyData":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v1, 0x64

    .line 192
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 193
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/dictionary/engine/DictEngine;->getMeaning(Ljava/lang/String;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMeaning(Ljava/util/List;I)[Ljava/lang/String;
    .locals 1
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;",
            ">;I)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 188
    .local p1, "searchList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;>;"
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/dictionary/engine/DictEngine;->getMeaning(Ljava/util/List;I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSearchListAdapter(I)Lcom/sec/android/app/dictionary/DictSearchListAdapter;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 207
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mSearchAdapter:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    .line 208
    .local v0, "adapter":Lcom/sec/android/app/dictionary/DictSearchListAdapter;
    iget v2, v0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->_id:I

    if-ne v2, p1, :cond_0

    .line 211
    .end local v0    # "adapter":Lcom/sec/android/app/dictionary/DictSearchListAdapter;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mContext:Landroid/content/Context;

    if-nez v0, :cond_2

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->TAG:Ljava/lang/String;

    const-string v1, "init DictEngine"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iput-object p1, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mContext:Landroid/content/Context;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/dictionary/util/DictUtils;->init(Landroid/content/Context;)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mDbManager:Lcom/sec/android/app/dictionary/db/DictDbManager;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/db/DictDbManager;->init(Landroid/content/Context;)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/engine/DictEngine;->init(Landroid/content/Context;)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->updateDictList()Ljava/util/List;

    .line 65
    sget-boolean v0, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_CSS:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

    sget-object v1, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_CSS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/engine/DictEngine;->makeMeaningCssFile(Ljava/lang/String;)V

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/engine/DictEngine;->getFontTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFontTypeface:Landroid/graphics/Typeface;

    .line 70
    :cond_2
    return-void
.end method

.method public installAppData()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/engine/DictEngine;->installAppData()Z

    .line 74
    return-void
.end method

.method public notifySearchListAdapter()V
    .locals 4

    .prologue
    .line 223
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mSearchAdapter:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    .line 224
    .local v0, "adapter":Lcom/sec/android/app/dictionary/DictSearchListAdapter;
    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->isActivityEnable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 225
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->TAG:Ljava/lang/String;

    const-string v3, "notifySearchListAdapter()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->search(Ljava/lang/String;)I

    .line 227
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->notifyDataSetChanged(Z)V

    goto :goto_0

    .line 229
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->TAG:Ljava/lang/String;

    const-string v3, "notifySearchListAdapter() - skip"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 232
    .end local v0    # "adapter":Lcom/sec/android/app/dictionary/DictSearchListAdapter;
    :cond_1
    return-void
.end method

.method public removeSearchListAdapter(Lcom/sec/android/app/dictionary/DictSearchListAdapter;)V
    .locals 3
    .param p1, "searchAdapter"    # Lcom/sec/android/app/dictionary/DictSearchListAdapter;

    .prologue
    .line 215
    iget v0, p1, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mRefCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mRefCount:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mSearchAdapter:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Removed searchAdapter id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->_id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", word:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mWord:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :cond_0
    return-void
.end method

.method public search(Ljava/lang/String;Ljava/util/List;)I
    .locals 3
    .param p1, "keyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p2, "searchList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;>;"
    const/16 v2, 0x64

    const/4 v0, 0x0

    .line 164
    if-nez p1, :cond_0

    .line 165
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 173
    :goto_0
    return v0

    .line 169
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_1

    .line 170
    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 172
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    monitor-enter v1

    .line 173
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    invoke-virtual {v0, v2, p1, p2}, Lcom/sec/android/app/dictionary/engine/DictEngine;->search(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)I

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public search(Ljava/lang/String;Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    .locals 2
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "dict"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .prologue
    const/16 v1, 0x64

    .line 178
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 179
    :cond_0
    const/4 v0, 0x0

    .line 184
    :goto_0
    return-object v0

    .line 181
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_2

    .line 182
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

    invoke-virtual {v0, p2, p1}, Lcom/sec/android/app/dictionary/engine/DictEngine;->search(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;Ljava/lang/String;)Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    move-result-object v0

    goto :goto_0
.end method

.method public sortDictList(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "dictList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mDictDataInfoComparator:Ljava/util/Comparator;

    invoke-static {p1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 122
    sget-boolean v2, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v2, :cond_3

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->TAG:Ljava/lang/String;

    const-string v3, "--- sortDictList() ---"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 125
    .local v1, "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-boolean v2, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mIsAvailable:Z

    if-eqz v2, :cond_0

    .line 126
    iget-object v3, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", order:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v2, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    const/16 v5, 0x2710

    if-ne v2, v5, :cond_1

    const-string v2, "MAX"

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget v2, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mOrder:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_1

    .line 130
    .end local v1    # "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->TAG:Ljava/lang/String;

    const-string v3, "--- --- ---"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_3
    return-void
.end method

.method public updateDictList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->TAG:Ljava/lang/String;

    const-string v3, "update()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iget-object v3, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    monitor-enter v3

    .line 79
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 80
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

    iget-object v4, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/dictionary/engine/DictEngine;->update(Ljava/util/List;)I

    .line 81
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 82
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 83
    .local v1, "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictMap:Landroid/util/SparseArray;

    iget v4, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-virtual {v2, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 91
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 85
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mDbManager:Lcom/sec/android/app/dictionary/db/DictDbManager;

    iget-object v4, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/dictionary/db/DictDbManager;->load(Landroid/util/SparseArray;)V

    .line 88
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mEngine:Lcom/sec/android/app/dictionary/engine/DictEngine;

    iget-object v4, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/dictionary/engine/DictEngine;->update(Ljava/util/List;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mAvailableDictCount:I

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->sortDictList(Ljava/util/List;)V

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mDbManager:Lcom/sec/android/app/dictionary/db/DictDbManager;

    iget-object v4, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/dictionary/db/DictDbManager;->save(Ljava/util/List;Z)V

    .line 91
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 94
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->TAG:Ljava/lang/String;

    const-string v3, "size is different!! (mFullDictMap < mFullDictList)"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 96
    .restart local v1    # "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictMap:Landroid/util/SparseArray;

    iget v3, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictMap:Landroid/util/SparseArray;

    iget v3, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    invoke-virtual {v2, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 101
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->mFullDictList:Ljava/util/List;

    return-object v2
.end method
