.class public abstract Lcom/sec/android/app/dictionary/engine/DictEngine;
.super Ljava/lang/Object;
.source "DictEngine.java"


# instance fields
.field BUFFERSIZE_MEAN:I

.field protected TAG:Ljava/lang/String;

.field protected mEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

.field protected mTextFormatter:Lcom/sec/android/app/dictionary/engine/DictTextFormatter;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngine;->TAG:Ljava/lang/String;

    .line 15
    const v0, 0x1a000

    iput v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngine;->BUFFERSIZE_MEAN:I

    .line 19
    new-instance v0, Lcom/sec/android/app/dictionary/engine/DictTextFormatter;

    invoke-direct {v0}, Lcom/sec/android/app/dictionary/engine/DictTextFormatter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/engine/DictEngine;->mTextFormatter:Lcom/sec/android/app/dictionary/engine/DictTextFormatter;

    return-void
.end method


# virtual methods
.method protected abstract getFontTypeface()Landroid/graphics/Typeface;
.end method

.method protected abstract getMeaning(Ljava/lang/String;Ljava/util/List;)[Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method protected abstract getMeaning(Ljava/util/List;I)[Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;",
            ">;I)[",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method protected abstract init(Landroid/content/Context;)V
.end method

.method protected abstract installAppData()Z
.end method

.method protected abstract makeMeaningCssFile(Ljava/lang/String;)V
.end method

.method protected abstract search(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;",
            ">;)I"
        }
    .end annotation
.end method

.method protected abstract search(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;Ljava/lang/String;)Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
.end method

.method setEngineManager(Lcom/sec/android/app/dictionary/engine/DictEngineManager;)V
    .locals 0
    .param p1, "engineManager"    # Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/android/app/dictionary/engine/DictEngine;->mEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    .line 23
    return-void
.end method

.method protected abstract update(Ljava/util/List;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;)I"
        }
    .end annotation
.end method
