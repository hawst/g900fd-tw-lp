.class Lcom/sec/android/app/dictionary/DictDownloadListActivity$11;
.super Ljava/lang/Object;
.source "DictDownloadListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/dictionary/DictDownloadListActivity;->showDataWarningDialog(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

.field final synthetic val$dict:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

.field final synthetic val$doNotShow:Landroid/widget/CheckBox;

.field final synthetic val$prefKey:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/dictionary/DictDownloadListActivity;Ljava/lang/String;Landroid/widget/CheckBox;Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)V
    .locals 0

    .prologue
    .line 387
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$11;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    iput-object p2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$11;->val$prefKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$11;->val$doNotShow:Landroid/widget/CheckBox;

    iput-object p4, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$11;->val$dict:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 390
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$11;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    iget-object v0, v0, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClick() which="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    sget-object v1, Lcom/sec/android/app/dictionary/db/DictDbManager;->instance:Lcom/sec/android/app/dictionary/db/DictDbManager;

    iget-object v2, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$11;->val$prefKey:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$11;->val$doNotShow:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/dictionary/db/DictDbManager;->savePref(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$11;->this$0:Lcom/sec/android/app/dictionary/DictDownloadListActivity;

    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDictDownMgr:Lcom/sec/android/app/dictionary/download/DictDownloadManager;
    invoke-static {v0}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$300(Lcom/sec/android/app/dictionary/DictDownloadListActivity;)Lcom/sec/android/app/dictionary/download/DictDownloadManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/dictionary/DictDownloadListActivity$11;->val$dict:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/dictionary/download/DictDownloadManager;->startDownload(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;)Z

    .line 396
    # getter for: Lcom/sec/android/app/dictionary/DictDownloadListActivity;->mDownAdapter:Lcom/sec/android/app/dictionary/DictDownloadListAdapter;
    invoke-static {}, Lcom/sec/android/app/dictionary/DictDownloadListActivity;->access$100()Lcom/sec/android/app/dictionary/DictDownloadListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/dictionary/DictDownloadListAdapter;->notifyDataSetChanged()V

    .line 398
    :cond_0
    return-void

    .line 391
    :cond_1
    const-string v0, "0"

    goto :goto_0
.end method
