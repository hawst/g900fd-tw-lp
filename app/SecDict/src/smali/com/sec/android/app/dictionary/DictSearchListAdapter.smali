.class public Lcom/sec/android/app/dictionary/DictSearchListAdapter;
.super Landroid/widget/BaseAdapter;
.source "DictSearchListAdapter.java"


# static fields
.field private static sIdCount:I


# instance fields
.field private TAG:Ljava/lang/String;

.field public _id:I

.field public mRefCount:I

.field private mSearchList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;",
            ">;"
        }
    .end annotation
.end field

.field mSearchListActivity:Lcom/sec/android/app/dictionary/DictSearchListActivity;

.field public mWord:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->sIdCount:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/dictionary/DictSearchListActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/app/dictionary/DictSearchListActivity;

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dictionary:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->TAG:Ljava/lang/String;

    .line 23
    sget v0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->sIdCount:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->sIdCount:I

    iput v0, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->_id:I

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mRefCount:I

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mSearchList:Ljava/util/ArrayList;

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mSearchListActivity:Lcom/sec/android/app/dictionary/DictSearchListActivity;

    .line 35
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mSearchList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mSearchList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 70
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 75
    iget-object v5, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mSearchListActivity:Lcom/sec/android/app/dictionary/DictSearchListActivity;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 78
    .local v4, "vi":Landroid/view/LayoutInflater;
    if-nez p2, :cond_0

    .line 79
    const v5, 0x7f03000a

    const/4 v6, 0x0

    invoke-virtual {v4, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 82
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    .line 84
    .local v1, "item":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    const v5, 0x7f0b0019

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 85
    .local v0, "headertv":Landroid/widget/TextView;
    if-eqz v0, :cond_1

    .line 87
    iget-object v5, v1, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->dict:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget-object v5, v5, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    :cond_1
    const v5, 0x7f0b001a

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 90
    .local v2, "keywordtv":Landroid/widget/TextView;
    if-eqz v2, :cond_2

    .line 91
    iget-object v5, v1, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->displayKeyword:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    sget-object v5, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->instance:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v5}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getFontTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 94
    :cond_2
    const v5, 0x7f0b001b

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 95
    .local v3, "previewtv":Landroid/widget/TextView;
    if-eqz v3, :cond_3

    .line 96
    iget-object v5, v1, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->preview:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    sget-object v5, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->instance:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v5}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getFontTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 100
    :cond_3
    return-object p2
.end method

.method public isActivityEnable()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mSearchListActivity:Lcom/sec/android/app/dictionary/DictSearchListActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/dictionary/DictSearchListActivity;->mEnable:Z

    return v0
.end method

.method public notifyDataSetChanged(Z)V
    .locals 1
    .param p1, "bAutoMean"    # Z

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mSearchListActivity:Lcom/sec/android/app/dictionary/DictSearchListActivity;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/dictionary/DictSearchListActivity;->updateLayout(Z)V

    .line 105
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 106
    return-void
.end method

.method public search(Ljava/lang/String;)I
    .locals 6
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 44
    .local v0, "count":I
    if-eqz p1, :cond_0

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mWord:Ljava/lang/String;

    .line 47
    :cond_0
    sget-object v3, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->instance:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    iget-object v4, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mWord:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mSearchList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->search(Ljava/lang/String;Ljava/util/List;)I

    move-result v0

    .line 49
    sget-boolean v3, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v3, :cond_1

    .line 50
    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->mSearchList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    .line 51
    .local v2, "item":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    iget-object v3, p0, Lcom/sec/android/app/dictionary/DictSearchListAdapter;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 55
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    :cond_1
    return v0
.end method
