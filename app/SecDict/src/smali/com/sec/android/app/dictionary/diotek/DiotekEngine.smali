.class public Lcom/sec/android/app/dictionary/diotek/DiotekEngine;
.super Lcom/sec/android/app/dictionary/engine/DictEngine;
.source "DiotekEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/dictionary/diotek/DiotekEngine$1;,
        Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;
    }
.end annotation


# instance fields
.field final LEMMA_DIR_NAME:Ljava/lang/String;

.field mDiotekDictList:Lcom/sec/android/app/dictionary/diotek/DiotekDictList;

.field mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

.field mMeanparser:Lcom/diotek/diodict/mean/MeanParser;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/dictionary/engine/DictEngine;-><init>()V

    .line 41
    new-instance v0, Lcom/diotek/diodict/mean/MeanParser3DB;

    invoke-direct {v0}, Lcom/diotek/diodict/mean/MeanParser3DB;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mMeanparser:Lcom/diotek/diodict/mean/MeanParser;

    .line 43
    const-string v0, "lemmaDB"

    iput-object v0, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->LEMMA_DIR_NAME:Ljava/lang/String;

    .line 297
    return-void
.end method

.method private getAvailableDBList(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p2, "word"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "dictList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    const/4 v8, 0x0

    .line 176
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    move-object v1, v8

    .line 200
    :cond_1
    :goto_0
    return-object v1

    .line 180
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 181
    .local v1, "availableDBList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 182
    .local v4, "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-boolean v7, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mIsAvailable:Z

    if-eqz v7, :cond_3

    move-object v7, v4

    .line 183
    check-cast v7, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;

    iget-object v0, v7, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mSupportDBInfos:[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    .local v0, "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_3

    aget-object v6, v0, v3

    .line 184
    .local v6, "supportDbInfo":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    invoke-virtual {v6}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getSourceLang()I

    move-result v7

    invoke-static {p2, v7}, Lcom/diotek/diodict4/core/adapter/LanguageChecker;->isSupportLangType(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_5

    move-object v7, v4

    .line 185
    check-cast v7, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    sget-boolean v7, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v7, :cond_4

    .line 187
    iget-object v7, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getAvailableDBList() : add "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_4
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 189
    :cond_5
    sget-boolean v7, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v7, :cond_4

    .line 190
    iget-object v7, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getAvailableDBList() : except "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v4, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 196
    .end local v0    # "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    .end local v3    # "i$":I
    .end local v4    # "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .end local v5    # "len$":I
    .end local v6    # "supportDbInfo":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    move-object v1, v8

    .line 197
    goto :goto_0
.end method

.method private getKeywordSpellCheck(ILjava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p1, "dbType"    # I
    .param p2, "searchWord"    # Ljava/lang/String;
    .param p3, "srcEnLang"    # I

    .prologue
    const/4 v3, 0x0

    .line 150
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getSpellCheckDBFileName(I)Ljava/lang/String;

    move-result-object v1

    .line 152
    .local v1, "szSpellCheckDBName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getDBPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 153
    .local v2, "szSpellCheckDBPath":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    invoke-virtual {v4, v2, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->initSpellcheck(Ljava/lang/String;I)I

    move-result v0

    .line 154
    .local v0, "err":I
    if-eqz v0, :cond_1

    .line 161
    :cond_0
    :goto_0
    return-object v3

    .line 157
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    invoke-virtual {v4, p2, p3}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->searchSpellcheck(Ljava/lang/String;I)I

    move-result v0

    .line 158
    if-nez v0, :cond_0

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getSpellcheckWord(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private getMeaning(ILjava/lang/String;I)[Ljava/lang/String;
    .locals 10
    .param p1, "dbType"    # I
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "uniqueid"    # I

    .prologue
    .line 275
    iget-object v7, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    move-result-object v1

    .line 278
    .local v1, "engine":Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;
    invoke-virtual {v1, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->setCurrentDbType(I)I

    .line 279
    invoke-virtual {v1, p1, p2, p3}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getMeaningCommon(ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 280
    .local v2, "html":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 281
    iget-object v7, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    const-string v8, "getMeaningCommon return is null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    const/4 v3, 0x0

    .line 294
    :cond_0
    return-object v3

    .line 285
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mMeanparser:Lcom/diotek/diodict/mean/MeanParser;

    iget-object v8, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v8}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {}, Lcom/diotek/diodict4/main/DioDictSettings;->getFontPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v2, v9}, Lcom/diotek/diodict/mean/MeanParser;->parseMeaning(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 288
    .local v3, "htmls":[Ljava/lang/String;
    sget-boolean v7, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v7, :cond_0

    .line 289
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v6, v0, v4

    .line 290
    .local v6, "str":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    invoke-static {v7, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private newDictSearchItem(ILcom/diotek/diodict/core/engine/ResultWordListItem;)Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;
    .locals 8
    .param p1, "dbtype"    # I
    .param p2, "diotekItem"    # Lcom/diotek/diodict/core/engine/ResultWordListItem;

    .prologue
    const/4 v6, 0x0

    .line 480
    new-instance v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;

    invoke-direct {v0}, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;-><init>()V

    .line 481
    .local v0, "dictItem":Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;
    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/ResultWordListItem;->getPreview()Lcom/diotek/diodict/core/engine/Preview;

    move-result-object v2

    .line 484
    .local v2, "preview":Lcom/diotek/diodict/core/engine/Preview;
    invoke-virtual {v2, v6}, Lcom/diotek/diodict/core/engine/Preview;->getKeyword(Z)Ljava/lang/String;

    move-result-object v1

    .line 485
    .local v1, "keyword":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/diotek/diodict/core/engine/ResultWordListItem;->getUniqueId()I

    move-result v4

    .line 486
    .local v4, "uniqueid":I
    if-eqz v1, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    invoke-virtual {v5, v1, v4, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getPronounce(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v3

    .line 488
    .local v3, "pron":Ljava/lang/String;
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->displayKeyword:Ljava/lang/String;

    .line 489
    iget-object v5, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    invoke-virtual {v5, p1, v1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getNormalizedText(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->keyword:Ljava/lang/String;

    .line 490
    iget-object v5, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mTextFormatter:Lcom/sec/android/app/dictionary/engine/DictTextFormatter;

    invoke-virtual {v2, v6}, Lcom/diotek/diodict/core/engine/Preview;->getEquivString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/dictionary/engine/DictTextFormatter;->covertPreviewText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->preview:Ljava/lang/String;

    .line 491
    iget-object v5, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mDiotekDictList:Lcom/sec/android/app/dictionary/diotek/DiotekDictList;

    invoke-virtual {v5, p1}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->getDictDataInfo(I)Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->dict:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 493
    iput p1, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->dictType:I

    .line 494
    iput v4, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->uniqueId:I

    .line 495
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper;->getEngineType(I)I

    move-result v5

    iput v5, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->engineType:I

    .line 498
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->searchKey:Ljava/util/ArrayList;

    .line 499
    iget-object v5, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->searchKey:Ljava/util/ArrayList;

    iget v6, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->dictType:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 500
    iget-object v5, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->searchKey:Ljava/util/ArrayList;

    iget v6, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->uniqueId:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502
    sget-boolean v5, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 503
    iget-object v5, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "newDictSearchItem:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->dict:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    iget-object v7, v7, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->dictType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->searchKey:Ljava/util/ArrayList;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    :cond_0
    return-object v0

    .line 486
    .end local v3    # "pron":Ljava/lang/String;
    :cond_1
    const-string v3, ""

    goto/16 :goto_0
.end method

.method private searchAll(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Z
    .locals 12
    .param p2, "keyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 205
    .local p1, "dictList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    .local p3, "searchList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;>;"
    sget-boolean v9, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v9, :cond_0

    .line 206
    iget-object v9, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "searchAll() : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->getAvailableDBList(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 209
    .local v2, "availableDBList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;>;"
    if-eqz v2, :cond_9

    .line 210
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;

    .line 211
    .local v6, "item":Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;
    iget-object v1, v6, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mSupportDBInfos:[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    .local v1, "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v7, :cond_1

    aget-object v8, v1, v5

    .line 212
    .local v8, "supportDbInfo":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    invoke-virtual {v8}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDBType()I

    move-result v3

    .line 213
    .local v3, "dbtype":I
    sget-boolean v9, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v9, :cond_2

    .line 214
    iget-object v9, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "searchAll() ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v6, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    invoke-virtual {v9, v3}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->initEngine(I)I

    .line 216
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->totalSearchWord(ILjava/lang/String;)I

    move-result v9

    if-eqz v9, :cond_3

    .line 217
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->updateSearchList(Ljava/lang/String;Ljava/util/List;)Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    .line 211
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 221
    .end local v1    # "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    .end local v3    # "dbtype":I
    .end local v5    # "i$":I
    .end local v6    # "item":Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;
    .end local v7    # "len$":I
    .end local v8    # "supportDbInfo":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_4
    sget-boolean v9, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v9, :cond_9

    .line 222
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 224
    .local v0, "allSearchList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 225
    .local v6, "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-boolean v9, v6, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mIsAvailable:Z

    if-eqz v9, :cond_5

    invoke-interface {v2, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    move-object v9, v6

    .line 226
    check-cast v9, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;

    iget-object v1, v9, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mSupportDBInfos:[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    .restart local v1    # "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    array-length v7, v1

    .restart local v7    # "len$":I
    const/4 v5, 0x0

    .restart local v5    # "i$":I
    :goto_1
    if-ge v5, v7, :cond_5

    aget-object v8, v1, v5

    .line 227
    .restart local v8    # "supportDbInfo":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    invoke-virtual {v8}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDBType()I

    move-result v3

    .line 228
    .restart local v3    # "dbtype":I
    sget-boolean v9, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v9, :cond_6

    .line 229
    iget-object v9, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "searchAll() - recheck for debug ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v6, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mShortName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_6
    iget-object v9, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    invoke-virtual {v9, v3}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->initEngine(I)I

    .line 232
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->totalSearchWord(ILjava/lang/String;)I

    move-result v9

    if-eqz v9, :cond_7

    .line 233
    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->updateSearchList(Ljava/lang/String;Ljava/util/List;)Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    .line 226
    :cond_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 239
    .end local v1    # "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    .end local v3    # "dbtype":I
    .end local v5    # "i$":I
    .end local v6    # "item":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .end local v7    # "len$":I
    .end local v8    # "supportDbInfo":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_8
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_9

    .line 240
    iget-object v9, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v9}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "Search result was omitted!!"

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 242
    iget-object v9, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Search result was omitted!! : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    .end local v0    # "allSearchList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;>;"
    :cond_9
    const/4 v9, 0x1

    return v9
.end method

.method private setAvailableDBList(Ljava/util/List;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 446
    .local p1, "dictList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    const/4 v0, 0x0

    .line 448
    .local v0, "count":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    .line 449
    .local v1, "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    new-instance v3, Ljava/io/File;

    iget-object v5, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDBPath:Ljava/lang/String;

    iget-object v6, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mDBFileName:Ljava/lang/String;

    invoke-direct {v3, v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    iput-boolean v5, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mIsAvailable:Z

    .line 451
    iget-boolean v5, v1, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mIsAvailable:Z

    if-eqz v5, :cond_0

    .line 452
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 455
    .end local v1    # "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .end local v3    # "file":Ljava/io/File;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mDiotekDictList:Lcom/sec/android/app/dictionary/diotek/DiotekDictList;

    invoke-virtual {v5}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->getDisabledDictList()Ljava/util/List;

    move-result-object v2

    .line 456
    .local v2, "disabledList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 457
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;

    .line 458
    .local v1, "dict":Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;
    new-instance v3, Ljava/io/File;

    iget-object v5, v1, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mDBPath:Ljava/lang/String;

    iget-object v6, v1, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mDBFileName:Ljava/lang/String;

    invoke-direct {v3, v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    .restart local v3    # "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    iput-boolean v5, v1, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mIsAvailable:Z

    .line 460
    iget-boolean v5, v1, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mIsAvailable:Z

    if-eqz v5, :cond_3

    .line 461
    add-int/lit8 v0, v0, 0x1

    .line 462
    iget-object v5, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Add DB"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    iget-boolean v5, v1, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mEnable:Z

    if-nez v5, :cond_2

    .line 464
    const/4 v5, 0x1

    iput-boolean v5, v1, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mEnable:Z

    .line 465
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 468
    :cond_3
    iget-boolean v5, v1, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mEnable:Z

    if-eqz v5, :cond_2

    .line 469
    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mEnable:Z

    .line 470
    invoke-interface {p1, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 476
    .end local v1    # "dict":Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;
    .end local v3    # "file":Ljava/io/File;
    :cond_4
    return v0
.end method

.method private updateSearchList(Ljava/lang/String;Ljava/util/List;)Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    .locals 10
    .param p1, "keyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;",
            ">;)",
            "Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;"
        }
    .end annotation

    .prologue
    .line 511
    .local p2, "searchList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;>;"
    const/4 v1, 0x0

    .line 512
    .local v1, "first":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    iget-object v7, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getSearchList(I)Lcom/diotek/diodict/core/engine/ResultWordList;

    move-result-object v5

    .line 513
    .local v5, "resultList":Lcom/diotek/diodict/core/engine/ResultWordList;
    if-eqz v5, :cond_5

    .line 514
    invoke-virtual {v5}, Lcom/diotek/diodict/core/engine/ResultWordList;->getSize()I

    move-result v3

    .line 515
    .local v3, "maxItems":I
    invoke-virtual {v5}, Lcom/diotek/diodict/core/engine/ResultWordList;->getDbType()I

    move-result v0

    .line 517
    .local v0, "dbtype":I
    if-lez v3, :cond_5

    .line 518
    const/4 v4, 0x0

    .local v4, "pos":I
    :goto_0
    if-ge v4, v3, :cond_4

    .line 519
    invoke-virtual {v5, v4}, Lcom/diotek/diodict/core/engine/ResultWordList;->getWordItem(I)Lcom/diotek/diodict/core/engine/ResultWordListItem;

    move-result-object v6

    .line 520
    .local v6, "resultListItem":Lcom/diotek/diodict/core/engine/ResultWordListItem;
    if-nez v6, :cond_1

    .line 518
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 523
    :cond_1
    invoke-virtual {v6}, Lcom/diotek/diodict/core/engine/ResultWordListItem;->getPreview()Lcom/diotek/diodict/core/engine/Preview;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 527
    invoke-direct {p0, v0, v6}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->newDictSearchItem(ILcom/diotek/diodict/core/engine/ResultWordListItem;)Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;

    move-result-object v2

    .line 528
    .local v2, "item":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    if-nez v1, :cond_2

    .line 529
    move-object v1, v2

    .line 530
    iput-object p1, v1, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->requestKeyword:Ljava/lang/String;

    .line 531
    const/4 v7, 0x1

    if-le v3, v7, :cond_0

    .line 532
    add-int/lit8 v7, v3, -0x1

    new-array v7, v7, [Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    iput-object v7, v1, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->next:[Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    goto :goto_1

    .line 534
    :cond_2
    if-lez v4, :cond_3

    .line 535
    iget-object v7, v1, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->next:[Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    add-int/lit8 v8, v4, -0x1

    aput-object v2, v7, v8

    goto :goto_1

    .line 537
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[ERROR]updateSearchList() pos="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 540
    .end local v2    # "item":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    .end local v6    # "resultListItem":Lcom/diotek/diodict/core/engine/ResultWordListItem;
    :cond_4
    if-eqz p2, :cond_5

    .line 541
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 544
    .end local v0    # "dbtype":I
    .end local v3    # "maxItems":I
    .end local v4    # "pos":I
    :cond_5
    return-object v1
.end method


# virtual methods
.method public getFontTypeface()Landroid/graphics/Typeface;
    .locals 7

    .prologue
    .line 422
    iget-object v4, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v4}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 423
    .local v0, "am":Landroid/content/res/AssetManager;
    const/4 v3, 0x0

    .line 426
    .local v3, "in":Ljava/io/InputStream;
    const/4 v4, 0x3

    :try_start_0
    invoke-static {v4}, Lcom/diotek/diodict4/main/DioDictSettings;->getFontName(I)Ljava/lang/String;

    move-result-object v2

    .line 427
    .local v2, "fontName":Ljava/lang/String;
    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 428
    invoke-static {v0, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 435
    if-eqz v3, :cond_0

    .line 436
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 442
    .end local v2    # "fontName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v4

    .line 437
    .restart local v2    # "fontName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 438
    .local v1, "e":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 429
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fontName":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 430
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 435
    if-eqz v3, :cond_1

    .line 436
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 442
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :cond_1
    :goto_1
    const/4 v4, 0x0

    goto :goto_0

    .line 437
    .restart local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v1

    .line 438
    .local v1, "e":Ljava/io/IOException;
    iget-object v4, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 431
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 432
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_4
    iget-object v4, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 435
    if-eqz v3, :cond_1

    .line 436
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1

    .line 437
    :catch_4
    move-exception v1

    .line 438
    iget-object v4, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 434
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 435
    if-eqz v3, :cond_2

    .line 436
    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 439
    :cond_2
    :goto_2
    throw v4

    .line 437
    :catch_5
    move-exception v1

    .line 438
    .restart local v1    # "e":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method protected getMeaning(Ljava/lang/String;Ljava/util/List;)[Ljava/lang/String;
    .locals 2
    .param p1, "keyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 265
    .local p2, "searchKeyData":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    const-string v1, "wrong searched word id"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    const/4 v0, 0x0

    .line 270
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v0, 0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v1, p1, v0}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->getMeaning(ILjava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getMeaning(Ljava/util/List;I)[Ljava/lang/String;
    .locals 4
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;",
            ">;I)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 252
    .local p1, "searchList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;>;"
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;

    .line 254
    .local v0, "item":Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;
    if-nez v0, :cond_0

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    const-string v2, "wrong searched word id"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    const/4 v1, 0x0

    .line 259
    :goto_0
    return-object v1

    :cond_0
    iget v1, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->dictType:I

    iget-object v2, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->keyword:Ljava/lang/String;

    iget v3, v0, Lcom/sec/android/app/dictionary/diotek/DiotekDataSearchItem;->uniqueId:I

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->getMeaning(ILjava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected init(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-static {}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getInstance()Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    .line 50
    return-void
.end method

.method protected installAppData()Z
    .locals 6

    .prologue
    .line 54
    const/4 v2, 0x0

    .line 57
    .local v2, "result":Z
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_DB:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "lemmaDB"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "dstPath":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 59
    iget-object v3, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    const-string v4, "installAppData() : lemmaDB"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v3, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v3}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "lemmaDB"

    invoke-static {v3, v4, v0}, Lcom/sec/android/app/dictionary/util/DictUtils;->copyAssets(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_0
    const-string v1, "CollinsEngSpellCheckDB.bin"

    .line 64
    .local v1, "engSpellDb":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_DB:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "CollinsEngSpellCheckDB.bin"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 65
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    const-string v4, "installAppData() : CollinsEngSpellCheckDB.bin"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v3, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v3}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "CollinsEngSpellCheckDB.bin"

    sget-object v5, Lcom/sec/android/app/dictionary/util/DictUtils;->PATH_DICT_DB:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/dictionary/util/DictUtils;->copyAssets(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_1
    return v2
.end method

.method protected makeMeaningCssFile(Ljava/lang/String;)V
    .locals 27
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 317
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getContext()Landroid/content/Context;

    move-result-object v9

    .line 318
    .local v9, "context":Landroid/content/Context;
    const-string v20, "diodict3_default.css"

    .line 319
    .local v20, "templateCssPath":Ljava/lang/String;
    invoke-virtual {v9}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    .line 320
    .local v3, "assetManager":Landroid/content/res/AssetManager;
    const/4 v15, 0x0

    .line 321
    .local v15, "in":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 323
    .local v4, "bufString":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v15

    .line 325
    invoke-virtual {v15}, Ljava/io/InputStream;->available()I

    move-result v19

    .line 326
    .local v19, "size":I
    move/from16 v0, v19

    new-array v6, v0, [B

    .line 327
    .local v6, "buffer":[B
    const/16 v22, 0x0

    move/from16 v0, v22

    move/from16 v1, v19

    invoke-virtual {v15, v6, v0, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v10

    .line 328
    .local v10, "count":I
    move/from16 v0, v19

    if-ge v10, v0, :cond_0

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "makeMeaningCssFile() : retry to read!!"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    :goto_0
    move/from16 v0, v19

    if-ge v10, v0, :cond_0

    .line 331
    sub-int v22, v19, v10

    move/from16 v0, v22

    invoke-virtual {v15, v6, v10, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    .line 332
    .local v7, "c":I
    if-gtz v7, :cond_4

    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "makeMeaningCssFile() : read end!! , "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    .end local v7    # "c":I
    :cond_0
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    .end local v4    # "bufString":Ljava/lang/String;
    .local v5, "bufString":Ljava/lang/String;
    if-eqz v15, :cond_1

    .line 346
    :try_start_1
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 352
    :cond_1
    :goto_1
    const/16 v22, 0x13

    move/from16 v0, v22

    new-array v12, v0, [Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const/16 v22, 0x0

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#default#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f060008

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0x1

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#background#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f060007

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0x2

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#italic#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f060009

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0x3

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#keyfield#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f06000a

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0x4

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#pure#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f06000b

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0x5

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#example#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f06000c

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0x6

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#idiom#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f06000d

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0x7

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#idiom_bg#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f06000e

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0x8

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#idiom_border#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f06000f

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0x9

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#note#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f060010

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0xa

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#originate#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f060012

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0xb

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#submeaning#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f060013

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0xc

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#keyword#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f060014

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0xd

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#part#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f060015

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0xe

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#category1#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f060016

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0xf

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#emphasis#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->COLOR:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f060017

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0x10

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#marginTop#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->DIMENS:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f080019

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0x11

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#marginLeft#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->DIMENS:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f08001a

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    const/16 v22, 0x12

    new-instance v23, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;

    const-string v24, "#marginRight#"

    sget-object v25, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->DIMENS:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    const v26, 0x7f08001b

    invoke-direct/range {v23 .. v26}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;-><init>(Ljava/lang/String;Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;I)V

    aput-object v23, v12, v22

    .line 374
    .local v12, "cssStyleTable":[Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;
    move-object v2, v12

    .local v2, "arr$":[Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;
    array-length v0, v2

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    move-object v4, v5

    .end local v5    # "bufString":Ljava/lang/String;
    .restart local v4    # "bufString":Ljava/lang/String;
    :goto_2
    move/from16 v0, v16

    if-ge v14, v0, :cond_7

    aget-object v11, v2, v14

    .line 375
    .local v11, "cssStyle":Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;
    const/16 v21, 0x0

    .line 376
    .local v21, "value":Ljava/lang/String;
    sget-object v22, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$1;->$SwitchMap$com$sec$android$app$dictionary$diotek$DiotekEngine$CssStyle$CssType:[I

    iget-object v0, v11, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;->type:Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle$CssType;->ordinal()I

    move-result v23

    aget v22, v22, v23

    packed-switch v22, :pswitch_data_0

    .line 382
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    iget v0, v11, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;->id:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 383
    .local v8, "color":I
    const v22, -0xedcbaa

    move/from16 v0, v22

    if-ne v8, v0, :cond_2

    .line 384
    sget v8, Lcom/sec/android/app/dictionary/util/DictUtils;->DEFAULT_BG_COLOR:I

    .line 386
    :cond_2
    const v22, 0xffffff

    and-int v8, v8, v22

    .line 387
    const-string v22, "#%X"

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    .line 391
    .end local v8    # "color":I
    :goto_3
    sget-boolean v22, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v22, :cond_3

    .line 392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "CssStyle : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    iget-object v0, v11, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;->tag:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    :cond_3
    iget-object v0, v11, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;->tag:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 374
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2

    .line 336
    .end local v2    # "arr$":[Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;
    .end local v11    # "cssStyle":Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;
    .end local v12    # "cssStyleTable":[Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;
    .end local v14    # "i$":I
    .end local v16    # "len$":I
    .end local v21    # "value":Ljava/lang/String;
    .restart local v7    # "c":I
    :cond_4
    add-int/2addr v10, v7

    .line 337
    goto/16 :goto_0

    .line 347
    .end local v4    # "bufString":Ljava/lang/String;
    .end local v7    # "c":I
    .restart local v5    # "bufString":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 348
    .local v13, "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 340
    .end local v5    # "bufString":Ljava/lang/String;
    .end local v6    # "buffer":[B
    .end local v10    # "count":I
    .end local v13    # "e":Ljava/io/IOException;
    .end local v19    # "size":I
    .restart local v4    # "bufString":Ljava/lang/String;
    :catch_1
    move-exception v13

    .line 341
    .restart local v13    # "e":Ljava/io/IOException;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 345
    if-eqz v15, :cond_5

    .line 346
    :try_start_3
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 418
    .end local v13    # "e":Ljava/io/IOException;
    :cond_5
    :goto_4
    return-void

    .line 347
    .restart local v13    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v13

    .line 348
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 344
    .end local v13    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v22

    .line 345
    if-eqz v15, :cond_6

    .line 346
    :try_start_4
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 349
    :cond_6
    :goto_5
    throw v22

    .line 347
    :catch_3
    move-exception v13

    .line 348
    .restart local v13    # "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 378
    .end local v13    # "e":Ljava/io/IOException;
    .restart local v2    # "arr$":[Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;
    .restart local v6    # "buffer":[B
    .restart local v10    # "count":I
    .restart local v11    # "cssStyle":Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;
    .restart local v12    # "cssStyleTable":[Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;
    .restart local v14    # "i$":I
    .restart local v16    # "len$":I
    .restart local v19    # "size":I
    .restart local v21    # "value":Ljava/lang/String;
    :pswitch_0
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    iget v0, v11, Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;->id:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v23

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/dictionary/util/DictUtils;->pxToDp(I)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "px"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 380
    goto/16 :goto_3

    .line 397
    .end local v11    # "cssStyle":Lcom/sec/android/app/dictionary/diotek/DiotekEngine$CssStyle;
    .end local v21    # "value":Ljava/lang/String;
    :cond_7
    const/16 v17, 0x0

    .line 399
    .local v17, "out":Ljava/io/OutputStream;
    :try_start_5
    new-instance v18, Ljava/io/FileOutputStream;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 400
    .end local v17    # "out":Ljava/io/OutputStream;
    .local v18, "out":Ljava/io/OutputStream;
    :try_start_6
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v22

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 401
    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_b
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_a
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 408
    if-eqz v18, :cond_8

    .line 409
    :try_start_7
    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_8
    move-object/from16 v17, v18

    .line 414
    .end local v18    # "out":Ljava/io/OutputStream;
    .restart local v17    # "out":Ljava/io/OutputStream;
    :cond_9
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "makeMeaningCssFile() end"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    sget-boolean v22, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v22, :cond_5

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 410
    .end local v17    # "out":Ljava/io/OutputStream;
    .restart local v18    # "out":Ljava/io/OutputStream;
    :catch_4
    move-exception v13

    .line 411
    .restart local v13    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v17, v18

    .line 413
    .end local v18    # "out":Ljava/io/OutputStream;
    .restart local v17    # "out":Ljava/io/OutputStream;
    goto :goto_6

    .line 402
    .end local v13    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v13

    .line 403
    .local v13, "e":Ljava/io/FileNotFoundException;
    :goto_7
    :try_start_8
    invoke-virtual {v13}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 408
    if-eqz v17, :cond_9

    .line 409
    :try_start_9
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_6

    .line 410
    :catch_6
    move-exception v13

    .line 411
    .local v13, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 404
    .end local v13    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v13

    .line 405
    .restart local v13    # "e":Ljava/io/IOException;
    :goto_8
    :try_start_a
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 408
    if-eqz v17, :cond_9

    .line 409
    :try_start_b
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    goto :goto_6

    .line 410
    :catch_8
    move-exception v13

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 407
    .end local v13    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v22

    .line 408
    :goto_9
    if-eqz v17, :cond_a

    .line 409
    :try_start_c
    invoke-virtual/range {v17 .. v17}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 412
    :cond_a
    :goto_a
    throw v22

    .line 410
    :catch_9
    move-exception v13

    .line 411
    .restart local v13    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 407
    .end local v13    # "e":Ljava/io/IOException;
    .end local v17    # "out":Ljava/io/OutputStream;
    .restart local v18    # "out":Ljava/io/OutputStream;
    :catchall_2
    move-exception v22

    move-object/from16 v17, v18

    .end local v18    # "out":Ljava/io/OutputStream;
    .restart local v17    # "out":Ljava/io/OutputStream;
    goto :goto_9

    .line 404
    .end local v17    # "out":Ljava/io/OutputStream;
    .restart local v18    # "out":Ljava/io/OutputStream;
    :catch_a
    move-exception v13

    move-object/from16 v17, v18

    .end local v18    # "out":Ljava/io/OutputStream;
    .restart local v17    # "out":Ljava/io/OutputStream;
    goto :goto_8

    .line 402
    .end local v17    # "out":Ljava/io/OutputStream;
    .restart local v18    # "out":Ljava/io/OutputStream;
    :catch_b
    move-exception v13

    move-object/from16 v17, v18

    .end local v18    # "out":Ljava/io/OutputStream;
    .restart local v17    # "out":Ljava/io/OutputStream;
    goto :goto_7

    .line 376
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected search(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)I
    .locals 19
    .param p2, "keyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "dictList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    .local p3, "searchList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v16

    if-lez v16, :cond_6

    if-eqz p2, :cond_0

    const/16 v16, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->requestKeyword:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 88
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    move-object/from16 v16, v0

    const-string v17, "search() - quick searchAll()"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const/16 v16, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->keyword:Ljava/lang/String;

    move-object/from16 p2, v0

    .line 90
    new-instance v5, Ljava/util/ArrayList;

    move-object/from16 v0, p3

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 92
    .local v5, "backupSearchItem":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->clear()V

    .line 94
    invoke-direct/range {p0 .. p2}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->getAvailableDBList(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 95
    .local v4, "availableDBList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;>;"
    if-eqz v4, :cond_7

    .line 96
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;

    .line 97
    .local v6, "dbInfo":Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;
    const/4 v8, 0x0

    .line 98
    .local v8, "exist":Z
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v14

    .line 99
    .local v14, "size":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v14, :cond_2

    .line 100
    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    .line 101
    .local v13, "searchItem":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    iget v0, v6, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mID:I

    move/from16 v16, v0

    iget-object v0, v13, Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;->dict:Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;->mID:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_5

    .line 102
    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    invoke-interface {v5, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 104
    const/4 v8, 0x1

    .line 109
    .end local v13    # "searchItem":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    :cond_2
    if-nez v8, :cond_1

    .line 110
    sget-boolean v16, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v16, :cond_3

    .line 111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "search() ("

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-object v0, v6, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mShortName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ")"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_3
    iget-object v3, v6, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mSupportDBInfos:[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    .local v3, "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    array-length v12, v3

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_1
    if-ge v11, v12, :cond_1

    aget-object v15, v3, v11

    .line 113
    .local v15, "supportDbInfo":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    invoke-virtual {v15}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDBType()I

    move-result v7

    .line 114
    .local v7, "dbtype":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->initEngine(I)I

    .line 115
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v7, v1}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->totalSearchWord(ILjava/lang/String;)I

    move-result v16

    if-eqz v16, :cond_4

    .line 116
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->updateSearchList(Ljava/lang/String;Ljava/util/List;)Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    .line 112
    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 99
    .end local v3    # "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    .end local v7    # "dbtype":I
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    .end local v15    # "supportDbInfo":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    .restart local v13    # "searchItem":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 123
    .end local v4    # "availableDBList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;>;"
    .end local v5    # "backupSearchItem":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;>;"
    .end local v6    # "dbInfo":Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;
    .end local v8    # "exist":Z
    .end local v9    # "i":I
    .end local v13    # "searchItem":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    .end local v14    # "size":I
    :cond_6
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->clear()V

    .line 124
    invoke-direct/range {p0 .. p3}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->searchAll(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Z

    .line 127
    :cond_7
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v16

    return v16
.end method

.method protected search(Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;Ljava/lang/String;)Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    .locals 9
    .param p1, "dict"    # Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    .param p2, "keyword"    # Ljava/lang/String;

    .prologue
    .line 132
    const/4 v3, 0x0

    .line 134
    .local v3, "item":Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;
    check-cast p1, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;

    .end local p1    # "dict":Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;
    iget-object v0, p1, Lcom/sec/android/app/dictionary/diotek/DiotekDataInfo;->mSupportDBInfos:[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;

    .local v0, "arr$":[Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v0, v2

    .line 135
    .local v5, "supportDbInfo":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    invoke-virtual {v5}, Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;->getDBType()I

    move-result v1

    .line 137
    .local v1, "dbtype":I
    sget-boolean v6, Lcom/sec/android/app/dictionary/util/DictUtils;->DEBUG:Z

    if-eqz v6, :cond_0

    .line 138
    iget-object v6, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "search() : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    invoke-virtual {v6, v1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->initEngine(I)I

    .line 140
    invoke-virtual {p0, v1, p2}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->totalSearchWord(ILjava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_2

    .line 141
    const/4 v6, 0x0

    invoke-direct {p0, p2, v6}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->updateSearchList(Ljava/lang/String;Ljava/util/List;)Lcom/sec/android/app/dictionary/engine/DictDataSearchItem;

    move-result-object v3

    .line 146
    .end local v1    # "dbtype":I
    .end local v5    # "supportDbInfo":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_1
    return-object v3

    .line 134
    .restart local v1    # "dbtype":I
    .restart local v5    # "supportDbInfo":Lcom/diotek/diodict4/core/adapter/DictDBManagerWrapper$SupportDBInfo;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method totalSearchWord(ILjava/lang/String;)I
    .locals 7
    .param p1, "dbtype"    # I
    .param p2, "keyword"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0xa

    const/4 v4, 0x0

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    move v1, p1

    move-object v2, p2

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->totalSearchWord(ILjava/lang/String;IIZ)I

    move-result v6

    .line 167
    .local v6, "result":I
    if-nez v6, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngine:Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;

    invoke-virtual {v1, p1}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->getEntryLanguage(I)I

    move-result v1

    invoke-direct {p0, p1, p2, v1}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->getKeywordSpellCheck(ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    move v1, p1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/diotek/diodict4/core/adapter/EngineManagerWrapper;->totalSearchWord(ILjava/lang/String;IIZ)I

    move-result v6

    .line 172
    :cond_0
    return v6
.end method

.method protected update(Ljava/util/List;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "dictList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/dictionary/datainfo/DictDataInfo;>;"
    iget-object v0, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mDiotekDictList:Lcom/sec/android/app/dictionary/diotek/DiotekDictList;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;

    iget-object v1, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mEngineManager:Lcom/sec/android/app/dictionary/engine/DictEngineManager;

    invoke-virtual {v1}, Lcom/sec/android/app/dictionary/engine/DictEngineManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mDiotekDictList:Lcom/sec/android/app/dictionary/diotek/DiotekDictList;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->mDiotekDictList:Lcom/sec/android/app/dictionary/diotek/DiotekDictList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/dictionary/diotek/DiotekDictList;->updateDictDataInfoList(Ljava/util/List;)V

    .line 80
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/dictionary/diotek/DiotekEngine;->setAvailableDBList(Ljava/util/List;)I

    move-result v0

    return v0
.end method
