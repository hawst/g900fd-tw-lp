.class public Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;
.super Ljava/lang/Object;
.source "FirmwareFileMgr.java"


# static fields
.field protected static final CAMERA_FIRMWARE_INFO_FILE_NVIDIA:Ljava/lang/String; = "/sys/class/sec/sec_cam/camerafw"

.field protected static final CAMERA_FIRMWARE_INFO_USER_FILE:Ljava/lang/String;

.field protected static final CAMERA_FIRMWARE_TYPE_FILE_REAR:Ljava/lang/String; = "/sys/class/camera/rear/rear_camtype"

.field protected static final CAMERA_ISP_VOLTAGE_INFO_FILE_REAR:Ljava/lang/String; = "/sys/class/camera/rear/isp_core"

.field protected static final CAMERA_NO_FIRMWARE_TYPE_FILE_REAR:Ljava/lang/String; = "/sys/class/camera/rear/rear_type"

.field protected static final CAMERA_SPECIFIC_COMPANION_USER_FILE:Ljava/lang/String; = "/sdcard/CamFW_Companion.bin"

.field protected static final CAMERA_SPECIFIC_FIRMWARE_TEST_KEY_FILE:Ljava/lang/String; = "/sdcard/1q2w3e4r.key"

.field protected static final CAMERA_SPECIFIC_USER_FILE:Ljava/lang/String; = "/sdcard/CamFW_Main.bin"

.field protected static final CAMERA_VENDORID_INFO_FILE_REAR:Ljava/lang/String; = "/sys/class/camera/rear/rear_vendorid"

.field protected static CAM_FLAG_COMP_FIRMWARE_DUMP:[B = null

.field protected static CAM_FLAG_COMP_FIRMWARE_FORCE:[B = null

.field protected static CAM_FLAG_COMP_FIRMWARE_UPDATE:[B = null

.field protected static CAM_FLAG_FIRMWARE_DUMP:[B = null

.field protected static CAM_FLAG_FIRMWARE_FORCE:[B = null

.field protected static CAM_FLAG_FIRMWARE_UPDATE:[B = null

.field protected static final FIRMWARE:Ljava/lang/String; = "RS_M10MO.bin"

.field protected static final FW_VER_DEFAULT_STRING:Ljava/lang/String; = "NONE"

.field protected static final TAG:Ljava/lang/String; = "FirmwareFileMgr"


# instance fields
.field protected CAMERA_COMPANION_FIRMWARE_INFO_FILE:Ljava/lang/String;

.field protected CAMERA_COMPANION_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

.field protected CAMERA_COMPANION_FIRMWARE_INFO_FILE_REAR:Ljava/lang/String;

.field protected CAMERA_FIRMWARE_CAL_CHECK_FILE:Ljava/lang/String;

.field protected CAMERA_FIRMWARE_INFO_FILE:Ljava/lang/String;

.field protected CAMERA_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

.field protected CAMERA_FIRMWARE_INFO_FILE_REAR:Ljava/lang/String;

.field protected CAMERA_FIRMWARE_TYPE_FILE:Ljava/lang/String;

.field protected CAMERA_OIS_FIRMWARE_INFO_FILE:Ljava/lang/String;

.field protected FW_INFO_TOKENS_NUM:I

.field public Feature:Lcom/sec/android/app/camera/Feature;

.field protected mCAMFWCalAF:Ljava/lang/String;

.field protected mCAMFWCalSEN:Ljava/lang/String;

.field protected mCamCompFWCalMain:Ljava/lang/String;

.field protected mCamCompFWVer:Ljava/lang/String;

.field protected mCamFWCalFront:Ljava/lang/String;

.field protected mCamFWCalMain:Ljava/lang/String;

.field protected mCamFWVer:Ljava/lang/String;

.field protected mFWManageMode:[B

.field protected mFWUpdateCount:Ljava/lang/String;

.field protected mISPVer1:Ljava/lang/String;

.field protected mISPVer2:Ljava/lang/String;

.field protected mISPVoltage:Ljava/lang/String;

.field protected mIspFWVer:Ljava/lang/String;

.field protected mLoadedCompFWVer:Ljava/lang/String;

.field protected mLoadedFWVer:Ljava/lang/String;

.field protected mOISBinVer:Ljava/lang/String;

.field protected mOISCalVer:Ljava/lang/String;

.field protected mOISFWVer:Ljava/lang/String;

.field protected mPhoneCompFWVer:Ljava/lang/String;

.field protected mPhoneFWVer:Ljava/lang/String;

.field protected mValidResult:Z

.field protected mVendorID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_USER_FILE:Ljava/lang/String;

    .line 55
    new-array v0, v2, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_UPDATE:[B

    .line 59
    new-array v0, v2, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_DUMP:[B

    .line 63
    new-array v0, v2, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_FORCE:[B

    .line 67
    new-array v0, v2, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_COMP_FIRMWARE_FORCE:[B

    .line 71
    new-array v0, v2, [B

    fill-array-data v0, :array_4

    sput-object v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_COMP_FIRMWARE_UPDATE:[B

    .line 75
    new-array v0, v2, [B

    fill-array-data v0, :array_5

    sput-object v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_COMP_FIRMWARE_DUMP:[B

    return-void

    .line 55
    nop

    :array_0
    .array-data 1
        0x31t
        0x0t
    .end array-data

    .line 59
    nop

    :array_1
    .array-data 1
        0x32t
        0x0t
    .end array-data

    .line 63
    nop

    :array_2
    .array-data 1
        0x33t
        0x0t
    .end array-data

    .line 67
    nop

    :array_3
    .array-data 1
        0x34t
        0x0t
    .end array-data

    .line 71
    nop

    :array_4
    .array-data 1
        0x35t
        0x0t
    .end array-data

    .line 75
    nop

    :array_5
    .array-data 1
        0x36t
        0x0t
    .end array-data
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE:Ljava/lang/String;

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_TYPE_FILE:Ljava/lang/String;

    .line 24
    const-string v0, "/sys/class/camera/rear/rear_camfw"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE_REAR:Ljava/lang/String;

    .line 25
    const-string v0, "/sys/class/camera/rear/rear_camfw_full"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    .line 33
    const-string v0, "/sys/class/camera/rear/rear_calcheck"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_CAL_CHECK_FILE:Ljava/lang/String;

    .line 34
    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE:Ljava/lang/String;

    .line 35
    const-string v0, "/sys/class/camera/rear/rear_companionfw"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE_REAR:Ljava/lang/String;

    .line 36
    const-string v0, "/sys/class/camera/rear/rear_companionfw_full"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    .line 51
    const-string v0, "/sys/class/camera/ois/oisfw"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_OIS_FIRMWARE_INFO_FILE:Ljava/lang/String;

    .line 53
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->FW_INFO_TOKENS_NUM:I

    .line 79
    sget-object v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_UPDATE:[B

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mFWManageMode:[B

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mValidResult:Z

    .line 126
    const-string v0, "FirmwareFileMgr"

    const-string v1, "FirmwareFileMgr"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-static {}, Lcom/sec/android/app/camera/CameraApp;->getAppFeature()Lcom/sec/android/app/camera/Feature;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->Feature:Lcom/sec/android/app/camera/Feature;

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->DISPLAY_EXTERNAL_ISP:Z

    if-eqz v0, :cond_0

    .line 131
    const-string v0, "/sys/class/camera/rear/rear_checkfw"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE_REAR:Ljava/lang/String;

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->ChkFirmwareFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE_REAR:Ljava/lang/String;

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE_REAR:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE:Ljava/lang/String;

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->ChkFirmwareFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE:Ljava/lang/String;

    .line 146
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_TYPE_FILE:Z

    if-eqz v0, :cond_3

    .line 147
    const-string v0, "/sys/class/camera/rear/rear_type"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_TYPE_FILE:Ljava/lang/String;

    .line 151
    :goto_1
    return-void

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE_REAR:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE:Ljava/lang/String;

    goto :goto_0

    .line 149
    :cond_3
    const-string v0, "/sys/class/camera/rear/rear_camtype"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_TYPE_FILE:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method protected ChkFirmwareFile(Ljava/lang/String;)Z
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 764
    const-string v2, "FirmwareFileMgr"

    const-string v3, "ChkFirmwareFile"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    const/4 v0, 0x0

    .line 767
    .local v0, "FirmwareFile":Ljava/lang/String;
    move-object v0, p1

    .line 769
    const-string v2, "FirmwareFileMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FirmwareFile : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 771
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 772
    const-string v2, "FirmwareFileMgr"

    const-string v3, "Firmware info data file exists"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    const/4 v2, 0x1

    .line 776
    :goto_0
    return v2

    .line 775
    :cond_0
    const-string v2, "FirmwareFileMgr"

    const-string v3, "Firmware info data file doesn\'t exist"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getCAMFWCalAF()Ljava/lang/String;
    .locals 3

    .prologue
    .line 698
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getFWInfo()V

    .line 699
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCAMFWCalAF() - mCAMFWCalAF["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCAMFWCalAF:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCAMFWCalAF:Ljava/lang/String;

    return-object v0
.end method

.method public getCAMFWCalSEN()Ljava/lang/String;
    .locals 3

    .prologue
    .line 704
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getFWInfo()V

    .line 705
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCAMFWCalSEN() - mCAMFWCalSEN["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCAMFWCalSEN:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCAMFWCalSEN:Ljava/lang/String;

    return-object v0
.end method

.method public getCamCompanionFWCalMain()Ljava/lang/String;
    .locals 3

    .prologue
    .line 752
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWCalCheck()V

    .line 753
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCamCompanionFWCalMain() - mCamCompFWCalMain["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamCompFWCalMain:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 754
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamCompFWCalMain:Ljava/lang/String;

    return-object v0
.end method

.method public getCamCompanionFWVer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 728
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCompanionFWInfo()V

    .line 729
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCamCompanionFWVer() - mCamCompFWVer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamCompFWVer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamCompFWVer:Ljava/lang/String;

    return-object v0
.end method

.method protected getCamFWCalCheck()V
    .locals 11

    .prologue
    .line 610
    const-string v8, "FirmwareFileMgr"

    const-string v9, "getCamFWCalCheck() - the fw info will be updated"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    const/4 v3, 0x0

    .line 613
    .local v3, "frFW":Ljava/io/FileReader;
    const/4 v0, 0x0

    .line 614
    .local v0, "brFW":Ljava/io/BufferedReader;
    const-string v8, "NONE"

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWCalMain:Ljava/lang/String;

    .line 615
    const-string v8, "NONE"

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamCompFWCalMain:Ljava/lang/String;

    .line 616
    const-string v8, "NONE"

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWCalFront:Ljava/lang/String;

    .line 620
    const/4 v7, 0x0

    .line 621
    .local v7, "sysFsFwPath":Ljava/lang/String;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_CAL_CHECK_FILE:Ljava/lang/String;

    .line 623
    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 624
    .end local v3    # "frFW":Ljava/io/FileReader;
    .local v4, "frFW":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 626
    .end local v0    # "brFW":Ljava/io/BufferedReader;
    .local v1, "brFW":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 628
    .local v5, "mFWCalInfo":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 629
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 631
    if-nez v5, :cond_3

    .line 632
    const-string v8, "FirmwareFileMgr"

    const-string v9, "getCamFWCalCheck() - can not get the FW info"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 648
    if-eqz v4, :cond_0

    .line 649
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 652
    :cond_0
    if-eqz v1, :cond_1

    .line 653
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_1
    :goto_0
    move-object v0, v1

    .end local v1    # "brFW":Ljava/io/BufferedReader;
    .restart local v0    # "brFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 659
    .end local v4    # "frFW":Ljava/io/FileReader;
    .end local v5    # "mFWCalInfo":Ljava/lang/String;
    .restart local v3    # "frFW":Ljava/io/FileReader;
    :cond_2
    :goto_1
    return-void

    .line 655
    .end local v0    # "brFW":Ljava/io/BufferedReader;
    .end local v3    # "frFW":Ljava/io/FileReader;
    .restart local v1    # "brFW":Ljava/io/BufferedReader;
    .restart local v4    # "frFW":Ljava/io/FileReader;
    .restart local v5    # "mFWCalInfo":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 656
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 636
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_4
    const-string v8, "FirmwareFileMgr"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getCamFWCalCheck() - FW info["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    new-instance v6, Ljava/util/StringTokenizer;

    const-string v8, " "

    const/4 v9, 0x0

    invoke-direct {v6, v5, v8, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 639
    .local v6, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWCalMain:Ljava/lang/String;

    .line 640
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamCompFWCalMain:Ljava/lang/String;

    .line 641
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWCalFront:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 648
    if-eqz v4, :cond_4

    .line 649
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 652
    :cond_4
    if-eqz v1, :cond_5

    .line 653
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_5
    move-object v0, v1

    .end local v1    # "brFW":Ljava/io/BufferedReader;
    .restart local v0    # "brFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 657
    .end local v4    # "frFW":Ljava/io/FileReader;
    .restart local v3    # "frFW":Ljava/io/FileReader;
    goto :goto_1

    .line 655
    .end local v0    # "brFW":Ljava/io/BufferedReader;
    .end local v3    # "frFW":Ljava/io/FileReader;
    .restart local v1    # "brFW":Ljava/io/BufferedReader;
    .restart local v4    # "frFW":Ljava/io/FileReader;
    :catch_1
    move-exception v2

    .line 656
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .end local v1    # "brFW":Ljava/io/BufferedReader;
    .restart local v0    # "brFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 658
    .end local v4    # "frFW":Ljava/io/FileReader;
    .restart local v3    # "frFW":Ljava/io/FileReader;
    goto :goto_1

    .line 643
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "mFWCalInfo":Ljava/lang/String;
    .end local v6    # "st":Ljava/util/StringTokenizer;
    :catch_2
    move-exception v2

    .line 644
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 645
    const-string v8, "FirmwareFileMgr"

    const-string v9, "Companion FW File reading error"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 648
    if-eqz v3, :cond_6

    .line 649
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 652
    :cond_6
    if-eqz v0, :cond_2

    .line 653
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 655
    :catch_3
    move-exception v2

    .line 656
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 647
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 648
    :goto_3
    if-eqz v3, :cond_7

    .line 649
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 652
    :cond_7
    if-eqz v0, :cond_8

    .line 653
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 657
    :cond_8
    :goto_4
    throw v8

    .line 655
    :catch_4
    move-exception v2

    .line 656
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 647
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "frFW":Ljava/io/FileReader;
    .restart local v4    # "frFW":Ljava/io/FileReader;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "frFW":Ljava/io/FileReader;
    .restart local v3    # "frFW":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "brFW":Ljava/io/BufferedReader;
    .end local v3    # "frFW":Ljava/io/FileReader;
    .restart local v1    # "brFW":Ljava/io/BufferedReader;
    .restart local v4    # "frFW":Ljava/io/FileReader;
    :catchall_2
    move-exception v8

    move-object v0, v1

    .end local v1    # "brFW":Ljava/io/BufferedReader;
    .restart local v0    # "brFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "frFW":Ljava/io/FileReader;
    .restart local v3    # "frFW":Ljava/io/FileReader;
    goto :goto_3

    .line 643
    .end local v3    # "frFW":Ljava/io/FileReader;
    .restart local v4    # "frFW":Ljava/io/FileReader;
    :catch_5
    move-exception v2

    move-object v3, v4

    .end local v4    # "frFW":Ljava/io/FileReader;
    .restart local v3    # "frFW":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "brFW":Ljava/io/BufferedReader;
    .end local v3    # "frFW":Ljava/io/FileReader;
    .restart local v1    # "brFW":Ljava/io/BufferedReader;
    .restart local v4    # "frFW":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "brFW":Ljava/io/BufferedReader;
    .restart local v0    # "brFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "frFW":Ljava/io/FileReader;
    .restart local v3    # "frFW":Ljava/io/FileReader;
    goto :goto_2
.end method

.method public getCamFWCalFront()Ljava/lang/String;
    .locals 3

    .prologue
    .line 758
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWCalCheck()V

    .line 759
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCamFWCalFront() - mCamFWCalFront["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWCalFront:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWCalFront:Ljava/lang/String;

    return-object v0
.end method

.method public getCamFWCalMain()Ljava/lang/String;
    .locals 3

    .prologue
    .line 746
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWCalCheck()V

    .line 747
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCamFWCalMain() - mCamFWCalMain["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWCalMain:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWCalMain:Ljava/lang/String;

    return-object v0
.end method

.method public getCamFWVer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 668
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getFWInfo()V

    .line 669
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCamFWVer() - mCamFWVer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWVer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWVer:Ljava/lang/String;

    return-object v0
.end method

.method protected getCompanionFWInfo()V
    .locals 11

    .prologue
    .line 505
    const-string v8, "FirmwareFileMgr"

    const-string v9, "getCompanionFWInfo() - the fw info will be updated"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    const/4 v3, 0x0

    .line 508
    .local v3, "frCompFW":Ljava/io/FileReader;
    const/4 v0, 0x0

    .line 509
    .local v0, "brCompFW":Ljava/io/BufferedReader;
    const-string v8, "NONE"

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamCompFWVer:Ljava/lang/String;

    .line 510
    const-string v8, "NONE"

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneCompFWVer:Ljava/lang/String;

    .line 511
    const-string v8, "NONE"

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mLoadedCompFWVer:Ljava/lang/String;

    .line 515
    const/4 v7, 0x0

    .line 516
    .local v7, "sysFsCompFwPath":Ljava/lang/String;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE:Ljava/lang/String;

    .line 517
    const-string v8, "FirmwareFileMgr"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getCompanionFWInfo() sysFsCompFwPath = CAMERA_COMPANION_FIRMWARE_INFO_FILE : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 520
    .end local v3    # "frCompFW":Ljava/io/FileReader;
    .local v4, "frCompFW":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 522
    .end local v0    # "brCompFW":Ljava/io/BufferedReader;
    .local v1, "brCompFW":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 524
    .local v5, "mCompFWInfo":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 525
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 527
    if-nez v5, :cond_3

    .line 528
    const-string v8, "FirmwareFileMgr"

    const-string v9, "getCompanionFWInfo() - can not get the FW info"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 546
    if-eqz v4, :cond_0

    .line 547
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 550
    :cond_0
    if-eqz v1, :cond_1

    .line 551
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_1
    :goto_0
    move-object v0, v1

    .end local v1    # "brCompFW":Ljava/io/BufferedReader;
    .restart local v0    # "brCompFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 557
    .end local v4    # "frCompFW":Ljava/io/FileReader;
    .end local v5    # "mCompFWInfo":Ljava/lang/String;
    .restart local v3    # "frCompFW":Ljava/io/FileReader;
    :cond_2
    :goto_1
    return-void

    .line 553
    .end local v0    # "brCompFW":Ljava/io/BufferedReader;
    .end local v3    # "frCompFW":Ljava/io/FileReader;
    .restart local v1    # "brCompFW":Ljava/io/BufferedReader;
    .restart local v4    # "frCompFW":Ljava/io/FileReader;
    .restart local v5    # "mCompFWInfo":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 554
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 532
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_4
    const-string v8, "FirmwareFileMgr"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getCompanionFWInfo() - FW info["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    new-instance v6, Ljava/util/StringTokenizer;

    const-string v8, " "

    const/4 v9, 0x0

    invoke-direct {v6, v5, v8, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 535
    .local v6, "st":Ljava/util/StringTokenizer;
    iget-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v8, v8, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_UPDATE_IN_USER:Z

    if-eqz v8, :cond_6

    .line 536
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamCompFWVer:Ljava/lang/String;

    .line 537
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneCompFWVer:Ljava/lang/String;

    .line 538
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mLoadedCompFWVer:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 546
    if-eqz v4, :cond_4

    .line 547
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 550
    :cond_4
    if-eqz v1, :cond_5

    .line 551
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_5
    :goto_2
    move-object v0, v1

    .end local v1    # "brCompFW":Ljava/io/BufferedReader;
    .restart local v0    # "brCompFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 555
    .end local v4    # "frCompFW":Ljava/io/FileReader;
    .restart local v3    # "frCompFW":Ljava/io/FileReader;
    goto :goto_1

    .line 553
    .end local v0    # "brCompFW":Ljava/io/BufferedReader;
    .end local v3    # "frCompFW":Ljava/io/FileReader;
    .restart local v1    # "brCompFW":Ljava/io/BufferedReader;
    .restart local v4    # "frCompFW":Ljava/io/FileReader;
    :catch_1
    move-exception v2

    .line 554
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 546
    .end local v2    # "e":Ljava/io/IOException;
    :cond_6
    if-eqz v4, :cond_7

    .line 547
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 550
    :cond_7
    if-eqz v1, :cond_8

    .line 551
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :cond_8
    move-object v0, v1

    .end local v1    # "brCompFW":Ljava/io/BufferedReader;
    .restart local v0    # "brCompFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 555
    .end local v4    # "frCompFW":Ljava/io/FileReader;
    .restart local v3    # "frCompFW":Ljava/io/FileReader;
    goto :goto_1

    .line 553
    .end local v0    # "brCompFW":Ljava/io/BufferedReader;
    .end local v3    # "frCompFW":Ljava/io/FileReader;
    .restart local v1    # "brCompFW":Ljava/io/BufferedReader;
    .restart local v4    # "frCompFW":Ljava/io/FileReader;
    :catch_2
    move-exception v2

    .line 554
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .end local v1    # "brCompFW":Ljava/io/BufferedReader;
    .restart local v0    # "brCompFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 556
    .end local v4    # "frCompFW":Ljava/io/FileReader;
    .restart local v3    # "frCompFW":Ljava/io/FileReader;
    goto :goto_1

    .line 541
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "mCompFWInfo":Ljava/lang/String;
    .end local v6    # "st":Ljava/util/StringTokenizer;
    :catch_3
    move-exception v2

    .line 542
    .local v2, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_7
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 543
    const-string v8, "FirmwareFileMgr"

    const-string v9, "Companion FW File reading error"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 546
    if-eqz v3, :cond_9

    .line 547
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 550
    :cond_9
    if-eqz v0, :cond_2

    .line 551
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_1

    .line 553
    :catch_4
    move-exception v2

    .line 554
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 545
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 546
    :goto_4
    if-eqz v3, :cond_a

    .line 547
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 550
    :cond_a
    if-eqz v0, :cond_b

    .line 551
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 555
    :cond_b
    :goto_5
    throw v8

    .line 553
    :catch_5
    move-exception v2

    .line 554
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 545
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "frCompFW":Ljava/io/FileReader;
    .restart local v4    # "frCompFW":Ljava/io/FileReader;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "frCompFW":Ljava/io/FileReader;
    .restart local v3    # "frCompFW":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "brCompFW":Ljava/io/BufferedReader;
    .end local v3    # "frCompFW":Ljava/io/FileReader;
    .restart local v1    # "brCompFW":Ljava/io/BufferedReader;
    .restart local v4    # "frCompFW":Ljava/io/FileReader;
    :catchall_2
    move-exception v8

    move-object v0, v1

    .end local v1    # "brCompFW":Ljava/io/BufferedReader;
    .restart local v0    # "brCompFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "frCompFW":Ljava/io/FileReader;
    .restart local v3    # "frCompFW":Ljava/io/FileReader;
    goto :goto_4

    .line 541
    .end local v3    # "frCompFW":Ljava/io/FileReader;
    .restart local v4    # "frCompFW":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v3, v4

    .end local v4    # "frCompFW":Ljava/io/FileReader;
    .restart local v3    # "frCompFW":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "brCompFW":Ljava/io/BufferedReader;
    .end local v3    # "frCompFW":Ljava/io/FileReader;
    .restart local v1    # "brCompFW":Ljava/io/BufferedReader;
    .restart local v4    # "frCompFW":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v0, v1

    .end local v1    # "brCompFW":Ljava/io/BufferedReader;
    .restart local v0    # "brCompFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "frCompFW":Ljava/io/FileReader;
    .restart local v3    # "frCompFW":Ljava/io/FileReader;
    goto :goto_3
.end method

.method protected getFWInfo()V
    .locals 21

    .prologue
    .line 394
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mValidResult:Z

    move/from16 v18, v0

    if-nez v18, :cond_1e

    .line 395
    const-string v18, "FirmwareFileMgr"

    const-string v19, "getFWInfo() - the fw info will be updated"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    const/4 v7, 0x0

    .line 397
    .local v7, "fr":Ljava/io/FileReader;
    const/4 v2, 0x0

    .line 398
    .local v2, "br":Ljava/io/BufferedReader;
    const/4 v9, 0x0

    .line 399
    .local v9, "frType":Ljava/io/FileReader;
    const/4 v4, 0x0

    .line 403
    .local v4, "brType":Ljava/io/BufferedReader;
    const/16 v16, 0x0

    .line 404
    .local v16, "sysFsPath":Ljava/lang/String;
    const/16 v17, 0x0

    .line 405
    .local v17, "sysFsTypePath":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_TYPE_FILE:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 407
    const-string v18, "FirmwareFileMgr"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getFWInfo() sysFsPath = CAMERA_FIRMWARE_INFO_FILE : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    const-string v18, "FirmwareFileMgr"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getFWInfo() sysFsTypePath = CAMERA_FIRMWARE_TYPE_FILE : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    new-instance v8, Ljava/io/FileReader;

    move-object/from16 v0, v16

    invoke-direct {v8, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 411
    .end local v7    # "fr":Ljava/io/FileReader;
    .local v8, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 413
    .end local v2    # "br":Ljava/io/BufferedReader;
    .local v3, "br":Ljava/io/BufferedReader;
    :try_start_2
    new-instance v10, Ljava/io/FileReader;

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 414
    .end local v9    # "frType":Ljava/io/FileReader;
    .local v10, "frType":Ljava/io/FileReader;
    :try_start_3
    new-instance v5, Ljava/io/BufferedReader;

    invoke-direct {v5, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 416
    .end local v4    # "brType":Ljava/io/BufferedReader;
    .local v5, "brType":Ljava/io/BufferedReader;
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v11

    .line 417
    .local v11, "mFWInfo":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v12

    .line 418
    .local v12, "mFWType":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 419
    invoke-virtual {v8}, Ljava/io/FileReader;->close()V

    .line 420
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 421
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V

    .line 423
    if-eqz v11, :cond_0

    if-nez v12, :cond_5

    .line 424
    :cond_0
    const-string v18, "FirmwareFileMgr"

    const-string v19, "getFWInfo() - can not get the FW info"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 487
    if-eqz v3, :cond_1

    .line 488
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 489
    :cond_1
    if-eqz v8, :cond_2

    .line 490
    invoke-virtual {v8}, Ljava/io/FileReader;->close()V

    .line 491
    :cond_2
    if-eqz v5, :cond_3

    .line 492
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 493
    :cond_3
    if-eqz v10, :cond_4

    .line 494
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 502
    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v5    # "brType":Ljava/io/BufferedReader;
    .end local v8    # "fr":Ljava/io/FileReader;
    .end local v10    # "frType":Ljava/io/FileReader;
    .end local v11    # "mFWInfo":Ljava/lang/String;
    .end local v12    # "mFWType":Ljava/lang/String;
    .end local v16    # "sysFsPath":Ljava/lang/String;
    .end local v17    # "sysFsTypePath":Ljava/lang/String;
    :cond_4
    :goto_0
    return-void

    .line 495
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "brType":Ljava/io/BufferedReader;
    .restart local v8    # "fr":Ljava/io/FileReader;
    .restart local v10    # "frType":Ljava/io/FileReader;
    .restart local v11    # "mFWInfo":Ljava/lang/String;
    .restart local v12    # "mFWType":Ljava/lang/String;
    .restart local v16    # "sysFsPath":Ljava/lang/String;
    .restart local v17    # "sysFsTypePath":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 496
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 428
    .end local v6    # "e":Ljava/io/IOException;
    :cond_5
    :try_start_6
    const-string v18, "FirmwareFileMgr"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getFWInfo() - FW info 1 ["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "::"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    new-instance v13, Ljava/util/StringTokenizer;

    const-string v18, " "

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v13, v11, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 431
    .local v13, "st":Ljava/util/StringTokenizer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_UPDATE_IN_USER:Z

    move/from16 v18, v0

    if-eqz v18, :cond_9

    .line 432
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVer1:Ljava/lang/String;

    .line 433
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWVer:Ljava/lang/String;

    .line 434
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneFWVer:Ljava/lang/String;

    .line 435
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mLoadedFWVer:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 487
    if-eqz v3, :cond_6

    .line 488
    :try_start_7
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 489
    :cond_6
    if-eqz v8, :cond_7

    .line 490
    invoke-virtual {v8}, Ljava/io/FileReader;->close()V

    .line 491
    :cond_7
    if-eqz v5, :cond_8

    .line 492
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 493
    :cond_8
    if-eqz v10, :cond_4

    .line 494
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_0

    .line 495
    :catch_1
    move-exception v6

    .line 496
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 439
    .end local v6    # "e":Ljava/io/IOException;
    :cond_9
    :try_start_8
    const-string v18, "FirmwareFileMgr"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getFWInfo() - FW info 2 ["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "::"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    new-instance v14, Ljava/util/StringTokenizer;

    const-string v18, " "

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v14, v11, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 441
    .local v14, "st1":Ljava/util/StringTokenizer;
    const-string v18, "FirmwareFileMgr"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "FirmwareFileMgr.getFWInfo() st1.countTokens() : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v14}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    invoke-virtual {v14}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_e

    .line 443
    const-string v18, "FirmwareFileMgr"

    const-string v19, "invalid FW Info!"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    const-string v18, "NONE"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWVer:Ljava/lang/String;

    .line 445
    const-string v18, "NONE"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneFWVer:Ljava/lang/String;

    .line 446
    const-string v18, "NONE"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mLoadedFWVer:Ljava/lang/String;

    .line 447
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->DISPLAY_EXTERNAL_ISP:Z

    move/from16 v18, v0

    if-eqz v18, :cond_a

    .line 448
    const-string v18, "NONE"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mIspFWVer:Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 487
    :cond_a
    if-eqz v3, :cond_b

    .line 488
    :try_start_9
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 489
    :cond_b
    if-eqz v8, :cond_c

    .line 490
    invoke-virtual {v8}, Ljava/io/FileReader;->close()V

    .line 491
    :cond_c
    if-eqz v5, :cond_d

    .line 492
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 493
    :cond_d
    if-eqz v10, :cond_4

    .line 494
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    goto/16 :goto_0

    .line 495
    :catch_2
    move-exception v6

    .line 496
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 453
    .end local v6    # "e":Ljava/io/IOException;
    :cond_e
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->DISPLAY_EXTERNAL_ISP:Z

    move/from16 v18, v0

    if-eqz v18, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE_REAR:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_16

    .line 455
    invoke-virtual {v14}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mIspFWVer:Ljava/lang/String;

    .line 456
    invoke-virtual {v14}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneFWVer:Ljava/lang/String;

    .line 457
    invoke-virtual {v14}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWVer:Ljava/lang/String;

    .line 463
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->DISPLAY_EXTERNAL_ISP:Z

    move/from16 v18, v0

    if-eqz v18, :cond_11

    .line 464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mIspFWVer:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mIspFWVer:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "[0-9|a-z|A-Z|]*"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_f

    .line 465
    const-string v18, "NONE"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mIspFWVer:Ljava/lang/String;

    .line 468
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneFWVer:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "[0-9|a-z|A-Z|]*"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_10

    .line 469
    const-string v18, "NONE"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneFWVer:Ljava/lang/String;

    .line 472
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWVer:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "[0-9|a-z|A-Z|]*"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_11

    .line 473
    const-string v18, "NONE"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWVer:Ljava/lang/String;

    .line 477
    :cond_11
    const-string v18, "FirmwareFileMgr"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getFWInfo() - mFWType ["

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "]"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    new-instance v15, Ljava/util/StringTokenizer;

    const-string v18, " "

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v15, v12, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 479
    .local v15, "stType":Ljava/util/StringTokenizer;
    invoke-virtual {v15}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVer1:Ljava/lang/String;

    .line 481
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mValidResult:Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 487
    if-eqz v3, :cond_12

    .line 488
    :try_start_b
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 489
    :cond_12
    if-eqz v8, :cond_13

    .line 490
    invoke-virtual {v8}, Ljava/io/FileReader;->close()V

    .line 491
    :cond_13
    if-eqz v5, :cond_14

    .line 492
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 493
    :cond_14
    if-eqz v10, :cond_15

    .line 494
    invoke-virtual {v10}, Ljava/io/FileReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    :cond_15
    move-object v4, v5

    .end local v5    # "brType":Ljava/io/BufferedReader;
    .restart local v4    # "brType":Ljava/io/BufferedReader;
    move-object v9, v10

    .end local v10    # "frType":Ljava/io/FileReader;
    .restart local v9    # "frType":Ljava/io/FileReader;
    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    move-object v7, v8

    .line 497
    .end local v8    # "fr":Ljava/io/FileReader;
    .restart local v7    # "fr":Ljava/io/FileReader;
    goto/16 :goto_0

    .line 459
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v4    # "brType":Ljava/io/BufferedReader;
    .end local v7    # "fr":Ljava/io/FileReader;
    .end local v9    # "frType":Ljava/io/FileReader;
    .end local v15    # "stType":Ljava/util/StringTokenizer;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "brType":Ljava/io/BufferedReader;
    .restart local v8    # "fr":Ljava/io/FileReader;
    .restart local v10    # "frType":Ljava/io/FileReader;
    :cond_16
    :try_start_c
    invoke-virtual {v14}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWVer:Ljava/lang/String;

    .line 460
    invoke-virtual {v14}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneFWVer:Ljava/lang/String;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    goto/16 :goto_1

    .line 482
    .end local v11    # "mFWInfo":Ljava/lang/String;
    .end local v12    # "mFWType":Ljava/lang/String;
    .end local v13    # "st":Ljava/util/StringTokenizer;
    .end local v14    # "st1":Ljava/util/StringTokenizer;
    :catch_3
    move-exception v6

    move-object v4, v5

    .end local v5    # "brType":Ljava/io/BufferedReader;
    .restart local v4    # "brType":Ljava/io/BufferedReader;
    move-object v9, v10

    .end local v10    # "frType":Ljava/io/FileReader;
    .restart local v9    # "frType":Ljava/io/FileReader;
    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    move-object v7, v8

    .line 483
    .end local v8    # "fr":Ljava/io/FileReader;
    .local v6, "e":Ljava/lang/Exception;
    .restart local v7    # "fr":Ljava/io/FileReader;
    :goto_2
    :try_start_d
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 484
    const-string v18, "FirmwareFileMgr"

    const-string v19, "file reading error"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 487
    if-eqz v2, :cond_17

    .line 488
    :try_start_e
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 489
    :cond_17
    if-eqz v7, :cond_18

    .line 490
    invoke-virtual {v7}, Ljava/io/FileReader;->close()V

    .line 491
    :cond_18
    if-eqz v4, :cond_19

    .line 492
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 493
    :cond_19
    if-eqz v9, :cond_4

    .line 494
    invoke-virtual {v9}, Ljava/io/FileReader;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_4

    goto/16 :goto_0

    .line 495
    :catch_4
    move-exception v6

    .line 496
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 495
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v4    # "brType":Ljava/io/BufferedReader;
    .end local v6    # "e":Ljava/io/IOException;
    .end local v7    # "fr":Ljava/io/FileReader;
    .end local v9    # "frType":Ljava/io/FileReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "brType":Ljava/io/BufferedReader;
    .restart local v8    # "fr":Ljava/io/FileReader;
    .restart local v10    # "frType":Ljava/io/FileReader;
    .restart local v11    # "mFWInfo":Ljava/lang/String;
    .restart local v12    # "mFWType":Ljava/lang/String;
    .restart local v13    # "st":Ljava/util/StringTokenizer;
    .restart local v14    # "st1":Ljava/util/StringTokenizer;
    .restart local v15    # "stType":Ljava/util/StringTokenizer;
    :catch_5
    move-exception v6

    .line 496
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "brType":Ljava/io/BufferedReader;
    .restart local v4    # "brType":Ljava/io/BufferedReader;
    move-object v9, v10

    .end local v10    # "frType":Ljava/io/FileReader;
    .restart local v9    # "frType":Ljava/io/FileReader;
    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    move-object v7, v8

    .line 498
    .end local v8    # "fr":Ljava/io/FileReader;
    .restart local v7    # "fr":Ljava/io/FileReader;
    goto/16 :goto_0

    .line 486
    .end local v6    # "e":Ljava/io/IOException;
    .end local v11    # "mFWInfo":Ljava/lang/String;
    .end local v12    # "mFWType":Ljava/lang/String;
    .end local v13    # "st":Ljava/util/StringTokenizer;
    .end local v14    # "st1":Ljava/util/StringTokenizer;
    .end local v15    # "stType":Ljava/util/StringTokenizer;
    :catchall_0
    move-exception v18

    .line 487
    :goto_3
    if-eqz v2, :cond_1a

    .line 488
    :try_start_f
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 489
    :cond_1a
    if-eqz v7, :cond_1b

    .line 490
    invoke-virtual {v7}, Ljava/io/FileReader;->close()V

    .line 491
    :cond_1b
    if-eqz v4, :cond_1c

    .line 492
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 493
    :cond_1c
    if-eqz v9, :cond_1d

    .line 494
    invoke-virtual {v9}, Ljava/io/FileReader;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_6

    .line 497
    :cond_1d
    :goto_4
    throw v18

    .line 495
    :catch_6
    move-exception v6

    .line 496
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 500
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v4    # "brType":Ljava/io/BufferedReader;
    .end local v6    # "e":Ljava/io/IOException;
    .end local v7    # "fr":Ljava/io/FileReader;
    .end local v9    # "frType":Ljava/io/FileReader;
    .end local v16    # "sysFsPath":Ljava/lang/String;
    .end local v17    # "sysFsTypePath":Ljava/lang/String;
    :cond_1e
    const-string v18, "FirmwareFileMgr"

    const-string v19, "getFWInfo() - do not need the firmware info"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 486
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "brType":Ljava/io/BufferedReader;
    .restart local v8    # "fr":Ljava/io/FileReader;
    .restart local v9    # "frType":Ljava/io/FileReader;
    .restart local v16    # "sysFsPath":Ljava/lang/String;
    .restart local v17    # "sysFsTypePath":Ljava/lang/String;
    :catchall_1
    move-exception v18

    move-object v7, v8

    .end local v8    # "fr":Ljava/io/FileReader;
    .restart local v7    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v7    # "fr":Ljava/io/FileReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v18

    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    move-object v7, v8

    .end local v8    # "fr":Ljava/io/FileReader;
    .restart local v7    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v7    # "fr":Ljava/io/FileReader;
    .end local v9    # "frType":Ljava/io/FileReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "fr":Ljava/io/FileReader;
    .restart local v10    # "frType":Ljava/io/FileReader;
    :catchall_3
    move-exception v18

    move-object v9, v10

    .end local v10    # "frType":Ljava/io/FileReader;
    .restart local v9    # "frType":Ljava/io/FileReader;
    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    move-object v7, v8

    .end local v8    # "fr":Ljava/io/FileReader;
    .restart local v7    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v4    # "brType":Ljava/io/BufferedReader;
    .end local v7    # "fr":Ljava/io/FileReader;
    .end local v9    # "frType":Ljava/io/FileReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "brType":Ljava/io/BufferedReader;
    .restart local v8    # "fr":Ljava/io/FileReader;
    .restart local v10    # "frType":Ljava/io/FileReader;
    :catchall_4
    move-exception v18

    move-object v4, v5

    .end local v5    # "brType":Ljava/io/BufferedReader;
    .restart local v4    # "brType":Ljava/io/BufferedReader;
    move-object v9, v10

    .end local v10    # "frType":Ljava/io/FileReader;
    .restart local v9    # "frType":Ljava/io/FileReader;
    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    move-object v7, v8

    .end local v8    # "fr":Ljava/io/FileReader;
    .restart local v7    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .line 482
    :catch_7
    move-exception v6

    goto :goto_2

    .end local v7    # "fr":Ljava/io/FileReader;
    .restart local v8    # "fr":Ljava/io/FileReader;
    :catch_8
    move-exception v6

    move-object v7, v8

    .end local v8    # "fr":Ljava/io/FileReader;
    .restart local v7    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v7    # "fr":Ljava/io/FileReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "fr":Ljava/io/FileReader;
    :catch_9
    move-exception v6

    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    move-object v7, v8

    .end local v8    # "fr":Ljava/io/FileReader;
    .restart local v7    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v7    # "fr":Ljava/io/FileReader;
    .end local v9    # "frType":Ljava/io/FileReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "fr":Ljava/io/FileReader;
    .restart local v10    # "frType":Ljava/io/FileReader;
    :catch_a
    move-exception v6

    move-object v9, v10

    .end local v10    # "frType":Ljava/io/FileReader;
    .restart local v9    # "frType":Ljava/io/FileReader;
    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    move-object v7, v8

    .end local v8    # "fr":Ljava/io/FileReader;
    .restart local v7    # "fr":Ljava/io/FileReader;
    goto :goto_2
.end method

.method public getFWUpdateCount()Ljava/lang/String;
    .locals 3

    .prologue
    .line 692
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getFWInfo()V

    .line 693
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFWUpdateCount() - mFWUpdateCount["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mFWUpdateCount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mFWUpdateCount:Ljava/lang/String;

    return-object v0
.end method

.method public getISPVer1()Ljava/lang/String;
    .locals 3

    .prologue
    .line 674
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getFWInfo()V

    .line 675
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getISPVer1() - mISPVer1["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVer1:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVer1:Ljava/lang/String;

    return-object v0
.end method

.method public getISPVer2()Ljava/lang/String;
    .locals 3

    .prologue
    .line 686
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getFWInfo()V

    .line 687
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getISPVer2() - mISPVer2["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVer2:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVer2:Ljava/lang/String;

    return-object v0
.end method

.method public getISPVoltage()Ljava/lang/String;
    .locals 3

    .prologue
    .line 710
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getISPVoltageInfo()V

    .line 711
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getISPVoltage() - mISPVoltage["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVoltage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVoltage:Ljava/lang/String;

    return-object v0
.end method

.method protected getISPVoltageInfo()V
    .locals 9

    .prologue
    .line 249
    const/4 v5, 0x0

    .line 250
    .local v5, "sysFsPath":Ljava/lang/String;
    const/4 v3, 0x0

    .line 251
    .local v3, "fr":Ljava/io/FileReader;
    const/4 v0, 0x0

    .line 253
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    const-string v5, "/sys/class/camera/rear/isp_core"

    .line 254
    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 255
    .end local v3    # "fr":Ljava/io/FileReader;
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 256
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 257
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVoltage:Ljava/lang/String;

    .line 258
    :cond_0
    if-eqz v4, :cond_1

    if-eqz v1, :cond_1

    .line 259
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 260
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 262
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVoltage:Ljava/lang/String;

    if-nez v6, :cond_4

    .line 263
    const-string v6, "FirmwareFileMgr"

    const-string v7, "getISPVoltageInfo() - can not get the VOLTAGE info"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 272
    if-eqz v4, :cond_2

    if-eqz v1, :cond_2

    .line 273
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 274
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    :cond_2
    :goto_0
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 280
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :cond_3
    :goto_1
    return-void

    .line 266
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :cond_4
    :try_start_4
    const-string v6, "FirmwareFileMgr"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getISPVoltageInfo() - VOLTAGE info["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVoltage:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 272
    if-eqz v4, :cond_5

    if-eqz v1, :cond_5

    .line 273
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 274
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :cond_5
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 278
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_1

    .line 276
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v6

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 279
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_1

    .line 267
    :catch_1
    move-exception v2

    .line 268
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 269
    const-string v6, "FirmwareFileMgr"

    const-string v7, "file reading error"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 272
    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    .line 273
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 274
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_1

    .line 276
    :catch_2
    move-exception v6

    goto :goto_1

    .line 271
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 272
    :goto_3
    if-eqz v3, :cond_6

    if-eqz v0, :cond_6

    .line 273
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 274
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 278
    :cond_6
    :goto_4
    throw v6

    .line 276
    :catch_3
    move-exception v7

    goto :goto_4

    .line 271
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v6

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .line 267
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_5
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .line 276
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v6

    goto :goto_0
.end method

.method public getIspFWVer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 662
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getFWInfo()V

    .line 663
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getIspFWVer() - mIspFWVer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mIspFWVer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mIspFWVer:Ljava/lang/String;

    return-object v0
.end method

.method public getLoadedCompanionFWVer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 740
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCompanionFWInfo()V

    .line 741
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLoadedCompanionFWVer() - mLoadedCompFWVer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mLoadedCompFWVer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mLoadedCompFWVer:Ljava/lang/String;

    return-object v0
.end method

.method public getLoadedFWVer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 722
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getFWInfo()V

    .line 723
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLoadedFWVer() - mLoadedFWInfo["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mLoadedFWVer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mLoadedFWVer:Ljava/lang/String;

    return-object v0
.end method

.method public getManageMode()[B
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mFWManageMode:[B

    return-object v0
.end method

.method public getOISBinVer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 796
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getOISFWInfo()V

    .line 797
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getOISBinVer() - mOISBinVer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mOISBinVer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mOISBinVer:Ljava/lang/String;

    return-object v0
.end method

.method public getOISCalVer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 789
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getOISFWInfo()V

    .line 790
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getOISCalVer() - mOISCalVer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mOISCalVer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mOISCalVer:Ljava/lang/String;

    return-object v0
.end method

.method protected getOISFWInfo()V
    .locals 11

    .prologue
    .line 560
    const-string v8, "FirmwareFileMgr"

    const-string v9, "getOISFWInfo() - the fw info will be updated"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    const/4 v3, 0x0

    .line 563
    .local v3, "frOISFW":Ljava/io/FileReader;
    const/4 v0, 0x0

    .line 564
    .local v0, "brOISFW":Ljava/io/BufferedReader;
    const-string v8, "NONE"

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mOISFWVer:Ljava/lang/String;

    .line 565
    const-string v8, "NONE"

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mOISCalVer:Ljava/lang/String;

    .line 566
    const-string v8, "NONE"

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mOISBinVer:Ljava/lang/String;

    .line 569
    const/4 v7, 0x0

    .line 570
    .local v7, "sysFsOISFwPath":Ljava/lang/String;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_OIS_FIRMWARE_INFO_FILE:Ljava/lang/String;

    .line 571
    const-string v8, "FirmwareFileMgr"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getOISFWInfo() sysFsOISFwPath = CAMERA_OIS_FIRMWARE_INFO_FILE : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 574
    .end local v3    # "frOISFW":Ljava/io/FileReader;
    .local v4, "frOISFW":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 576
    .end local v0    # "brOISFW":Ljava/io/BufferedReader;
    .local v1, "brOISFW":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 578
    .local v5, "mOISFWInfo":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 579
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 581
    if-nez v5, :cond_3

    .line 582
    const-string v8, "FirmwareFileMgr"

    const-string v9, "getOISFWInfo() - can not get the FW info"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 596
    if-eqz v4, :cond_0

    .line 597
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 600
    :cond_0
    if-eqz v1, :cond_1

    .line 601
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_1
    :goto_0
    move-object v0, v1

    .end local v1    # "brOISFW":Ljava/io/BufferedReader;
    .restart local v0    # "brOISFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 607
    .end local v4    # "frOISFW":Ljava/io/FileReader;
    .end local v5    # "mOISFWInfo":Ljava/lang/String;
    .restart local v3    # "frOISFW":Ljava/io/FileReader;
    :cond_2
    :goto_1
    return-void

    .line 603
    .end local v0    # "brOISFW":Ljava/io/BufferedReader;
    .end local v3    # "frOISFW":Ljava/io/FileReader;
    .restart local v1    # "brOISFW":Ljava/io/BufferedReader;
    .restart local v4    # "frOISFW":Ljava/io/FileReader;
    .restart local v5    # "mOISFWInfo":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 604
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 586
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_4
    const-string v8, "FirmwareFileMgr"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getOISFWInfo() - OIS FW info["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    new-instance v6, Ljava/util/StringTokenizer;

    const-string v8, " "

    const/4 v9, 0x0

    invoke-direct {v6, v5, v8, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 589
    .local v6, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mOISFWVer:Ljava/lang/String;

    .line 590
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mOISBinVer:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 596
    if-eqz v4, :cond_4

    .line 597
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 600
    :cond_4
    if-eqz v1, :cond_5

    .line 601
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_5
    move-object v0, v1

    .end local v1    # "brOISFW":Ljava/io/BufferedReader;
    .restart local v0    # "brOISFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 605
    .end local v4    # "frOISFW":Ljava/io/FileReader;
    .restart local v3    # "frOISFW":Ljava/io/FileReader;
    goto :goto_1

    .line 603
    .end local v0    # "brOISFW":Ljava/io/BufferedReader;
    .end local v3    # "frOISFW":Ljava/io/FileReader;
    .restart local v1    # "brOISFW":Ljava/io/BufferedReader;
    .restart local v4    # "frOISFW":Ljava/io/FileReader;
    :catch_1
    move-exception v2

    .line 604
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .end local v1    # "brOISFW":Ljava/io/BufferedReader;
    .restart local v0    # "brOISFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 606
    .end local v4    # "frOISFW":Ljava/io/FileReader;
    .restart local v3    # "frOISFW":Ljava/io/FileReader;
    goto :goto_1

    .line 591
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "mOISFWInfo":Ljava/lang/String;
    .end local v6    # "st":Ljava/util/StringTokenizer;
    :catch_2
    move-exception v2

    .line 592
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 593
    const-string v8, "FirmwareFileMgr"

    const-string v9, "OIS FW File reading error"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 596
    if-eqz v3, :cond_6

    .line 597
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 600
    :cond_6
    if-eqz v0, :cond_2

    .line 601
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 603
    :catch_3
    move-exception v2

    .line 604
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 595
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 596
    :goto_3
    if-eqz v3, :cond_7

    .line 597
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 600
    :cond_7
    if-eqz v0, :cond_8

    .line 601
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 605
    :cond_8
    :goto_4
    throw v8

    .line 603
    :catch_4
    move-exception v2

    .line 604
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 595
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "frOISFW":Ljava/io/FileReader;
    .restart local v4    # "frOISFW":Ljava/io/FileReader;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "frOISFW":Ljava/io/FileReader;
    .restart local v3    # "frOISFW":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "brOISFW":Ljava/io/BufferedReader;
    .end local v3    # "frOISFW":Ljava/io/FileReader;
    .restart local v1    # "brOISFW":Ljava/io/BufferedReader;
    .restart local v4    # "frOISFW":Ljava/io/FileReader;
    :catchall_2
    move-exception v8

    move-object v0, v1

    .end local v1    # "brOISFW":Ljava/io/BufferedReader;
    .restart local v0    # "brOISFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "frOISFW":Ljava/io/FileReader;
    .restart local v3    # "frOISFW":Ljava/io/FileReader;
    goto :goto_3

    .line 591
    .end local v3    # "frOISFW":Ljava/io/FileReader;
    .restart local v4    # "frOISFW":Ljava/io/FileReader;
    :catch_5
    move-exception v2

    move-object v3, v4

    .end local v4    # "frOISFW":Ljava/io/FileReader;
    .restart local v3    # "frOISFW":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "brOISFW":Ljava/io/BufferedReader;
    .end local v3    # "frOISFW":Ljava/io/FileReader;
    .restart local v1    # "brOISFW":Ljava/io/BufferedReader;
    .restart local v4    # "frOISFW":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "brOISFW":Ljava/io/BufferedReader;
    .restart local v0    # "brOISFW":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "frOISFW":Ljava/io/FileReader;
    .restart local v3    # "frOISFW":Ljava/io/FileReader;
    goto :goto_2
.end method

.method public getOISFWVer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 782
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getOISFWInfo()V

    .line 783
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getOISFWVer() - mOISFWVer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mOISFWVer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mOISFWVer:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneCompanionFWVer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 734
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCompanionFWInfo()V

    .line 735
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPhoneCompanionFWVer() - mPhoneCompFWVer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneCompFWVer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneCompFWVer:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneFWVer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 680
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getFWInfo()V

    .line 681
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPhoneFWVer() - mPhoneFWVer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneFWVer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneFWVer:Ljava/lang/String;

    return-object v0
.end method

.method public getVendorID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 716
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getVendorIDInfo()V

    .line 717
    const-string v0, "FirmwareFileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVendorID() - mVendorID["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mVendorID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mVendorID:Ljava/lang/String;

    return-object v0
.end method

.method protected getVendorIDInfo()V
    .locals 9

    .prologue
    .line 284
    const/4 v5, 0x0

    .line 286
    .local v5, "sysFsPath":Ljava/lang/String;
    const/4 v3, 0x0

    .line 288
    .local v3, "fr":Ljava/io/FileReader;
    const/4 v0, 0x0

    .line 292
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    const-string v5, "/sys/class/camera/rear/rear_vendorid"

    .line 294
    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    .end local v3    # "fr":Ljava/io/FileReader;
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 298
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 300
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mVendorID:Ljava/lang/String;

    .line 302
    :cond_0
    if-eqz v4, :cond_1

    if-eqz v1, :cond_1

    .line 304
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 306
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 310
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mVendorID:Ljava/lang/String;

    if-nez v6, :cond_4

    .line 312
    const-string v6, "FirmwareFileMgr"

    const-string v7, "getVendorIDInfo() - can not get the VendorIDInfo info"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 330
    if-eqz v4, :cond_2

    if-eqz v1, :cond_2

    .line 332
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 334
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    :cond_2
    :goto_0
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 346
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :cond_3
    :goto_1
    return-void

    .line 318
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :cond_4
    :try_start_4
    const-string v6, "FirmwareFileMgr"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getVendorIDInfo() - VendorIDInfo info["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mVendorID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 330
    if-eqz v4, :cond_5

    if-eqz v1, :cond_5

    .line 332
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 334
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :cond_5
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 342
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_1

    .line 338
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v6

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 344
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_1

    .line 320
    :catch_1
    move-exception v2

    .line 322
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 324
    const-string v6, "FirmwareFileMgr"

    const-string v7, "file reading error"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 330
    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    .line 332
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 334
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_1

    .line 338
    :catch_2
    move-exception v6

    goto :goto_1

    .line 328
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 330
    :goto_3
    if-eqz v3, :cond_6

    if-eqz v0, :cond_6

    .line 332
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 334
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 342
    :cond_6
    :goto_4
    throw v6

    .line 338
    :catch_3
    move-exception v7

    goto :goto_4

    .line 328
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v6

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .line 320
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_5
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .line 338
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v6

    goto :goto_0
.end method

.method public resetFWInfo()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 217
    const-string v0, "FirmwareFileMgr"

    const-string v1, "resetFWInfo() called!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    sget-object v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_UPDATE:[B

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mFWManageMode:[B

    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mValidResult:Z

    .line 221
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWVer:Ljava/lang/String;

    .line 222
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneFWVer:Ljava/lang/String;

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->DISPLAY_EXTERNAL_ISP:Z

    if-eqz v0, :cond_0

    .line 224
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mIspFWVer:Ljava/lang/String;

    .line 225
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVer1:Ljava/lang/String;

    .line 226
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mISPVer2:Ljava/lang/String;

    .line 227
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mFWUpdateCount:Ljava/lang/String;

    .line 228
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCAMFWCalAF:Ljava/lang/String;

    .line 229
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCAMFWCalSEN:Ljava/lang/String;

    .line 230
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mLoadedFWVer:Ljava/lang/String;

    .line 232
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamCompFWVer:Ljava/lang/String;

    .line 233
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mPhoneCompFWVer:Ljava/lang/String;

    .line 234
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mLoadedCompFWVer:Ljava/lang/String;

    .line 235
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWCalMain:Ljava/lang/String;

    .line 236
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamCompFWCalMain:Ljava/lang/String;

    .line 237
    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mCamFWCalFront:Ljava/lang/String;

    .line 238
    return-void
.end method

.method public setManageMode([B)V
    .locals 0
    .param p1, "mode"    # [B

    .prologue
    .line 241
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->mFWManageMode:[B

    .line 242
    return-void
.end method

.method public setSysfsFile([B)V
    .locals 11
    .param p1, "value"    # [B

    .prologue
    .line 154
    iget-object v8, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->ChkFirmwareFile(Ljava/lang/String;)Z

    move-result v1

    .line 155
    .local v1, "misCompanionFile":Z
    const-string v8, "FirmwareFileMgr"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "misCompanionFile : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    sget-object v8, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_UPDATE:[B

    if-eq p1, v8, :cond_0

    sget-object v8, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_DUMP:[B

    if-eq p1, v8, :cond_0

    .line 158
    const-string v8, "FirmwareFileMgr"

    const-string v9, "setSysfsFile() - FW manageMode is not defined"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    :goto_0
    return-void

    .line 162
    :cond_0
    const/4 v2, 0x0

    .line 163
    .local v2, "out":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .line 166
    .local v4, "out1":Ljava/io/FileOutputStream;
    :try_start_0
    const-string v8, "FirmwareFileMgr"

    const-string v9, "setSysfsFile() called!"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    const/4 v6, 0x0

    .line 169
    .local v6, "sysFs":Ljava/lang/String;
    const/4 v7, 0x0

    .line 177
    .local v7, "sysFs1":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE:Ljava/lang/String;

    .line 179
    if-eqz v1, :cond_1

    .line 180
    iget-object v7, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE:Ljava/lang/String;

    .line 183
    :cond_1
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .local v3, "out":Ljava/io/FileOutputStream;
    if-eqz v1, :cond_2

    .line 186
    :try_start_1
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .end local v4    # "out1":Ljava/io/FileOutputStream;
    .local v5, "out1":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 189
    .end local v5    # "out1":Ljava/io/FileOutputStream;
    .restart local v4    # "out1":Ljava/io/FileOutputStream;
    :cond_2
    const-string v8, "FirmwareFileMgr"

    const-string v9, "FileOutputStream success!"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-virtual {v3, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 192
    if-eqz v1, :cond_3

    .line 193
    invoke-virtual {v4, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 196
    :cond_3
    const-string v8, "FirmwareFileMgr"

    const-string v9, "write success!"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 202
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 204
    if-eqz v1, :cond_4

    .line 205
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 211
    :cond_4
    :goto_1
    const-string v8, "FirmwareFileMgr"

    const-string v9, "close success!"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    .line 213
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .end local v6    # "sysFs":Ljava/lang/String;
    .end local v7    # "sysFs1":Ljava/lang/String;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->resetFWInfo()V

    goto :goto_0

    .line 207
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "sysFs":Ljava/lang/String;
    .restart local v7    # "sysFs1":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 209
    const-string v8, "FirmwareFileMgr"

    const-string v9, "close error"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 197
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .end local v6    # "sysFs":Ljava/lang/String;
    .end local v7    # "sysFs1":Ljava/lang/String;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v0

    .line 198
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 199
    const-string v8, "FirmwareFileMgr"

    const-string v9, "file writing error"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 202
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 204
    if-eqz v1, :cond_5

    .line 205
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 211
    :cond_5
    :goto_4
    const-string v8, "FirmwareFileMgr"

    const-string v9, "close success!"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 207
    :catch_2
    move-exception v0

    .line 208
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 209
    const-string v8, "FirmwareFileMgr"

    const-string v9, "close error"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 201
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 202
    :goto_5
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 204
    if-eqz v1, :cond_6

    .line 205
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 211
    :cond_6
    :goto_6
    const-string v9, "FirmwareFileMgr"

    const-string v10, "close success!"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    throw v8

    .line 207
    :catch_3
    move-exception v0

    .line 208
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 209
    const-string v9, "FirmwareFileMgr"

    const-string v10, "close error"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 201
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "sysFs":Ljava/lang/String;
    .restart local v7    # "sysFs1":Ljava/lang/String;
    :catchall_1
    move-exception v8

    move-object v2, v3

    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 197
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    goto :goto_3
.end method
