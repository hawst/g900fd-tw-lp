.class Lcom/sec/android/app/camerafirmware/CameraFirmware$1;
.super Ljava/lang/Object;
.source "CameraFirmware.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camerafirmware/CameraFirmware;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camerafirmware/CameraFirmware;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v9, 0x7f09001f

    const v8, 0x7f090017

    const v7, 0x7f0a0011

    const v5, 0x7f090019

    const/4 v6, 0x1

    .line 167
    const-string v3, "CameraFirmware"

    const-string v4, "Button Clicked "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    const/4 v2, 0x0

    .line 169
    .local v2, "toast_string":Ljava/lang/String;
    const/4 v1, 0x0

    .line 170
    .local v1, "mSocTypeModel":Z
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 559
    :cond_0
    :goto_0
    return-void

    .line 172
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->cancel()V

    .line 176
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    if-eqz v3, :cond_2

    .line 177
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V

    .line 181
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v3, :cond_3

    .line 182
    const-string v3, "CameraFirmware"

    const-string v4, "mRear is null, rear FW info will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 185
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 186
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->ChkSOCTypeModel()Z

    move-result v1

    .line 187
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->DISPLAY_EXTERNAL_ISP:Z

    if-nez v3, :cond_e

    .line 188
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->REAR_INTERNAL_ISP:Z

    if-nez v3, :cond_9

    .line 189
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rear Camera\nCam FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Phone FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 243
    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->misOISFile:Z
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$200(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v3

    if-eqz v3, :cond_5

    if-nez v1, :cond_5

    .line 244
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\nOIS\nCam FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getOISFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Phone FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getOISBinVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 250
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    if-nez v3, :cond_6

    .line 251
    const-string v3, "CameraFirmware"

    const-string v4, "mFront is null, front FW infor will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    .line 254
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 256
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->FRONT_LOADED_FIRMWARE:Z

    if-eqz v3, :cond_10

    .line 257
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\nFront Camera\nCam FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Load FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getLoadedFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Phone FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 267
    :goto_2
    if-eqz v2, :cond_7

    .line 268
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-static {v4, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$002(Lcom/sec/android/app/camerafirmware/CameraFirmware;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 270
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 273
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$100(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 274
    :cond_8
    const-string v3, "CameraFirmware"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CameraFirmware.phoneCAMFWVerCheck mCameraDevice : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    if-eqz v3, :cond_0

    .line 276
    const-string v3, "CameraFirmware"

    const-string v4, "Phone/CAM FW Ver Check stopCameraPreview"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->stopPreview()V

    goto/16 :goto_0

    .line 193
    :cond_9
    if-eqz v1, :cond_a

    .line 194
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rear Camera (Main)\nCam FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Phone FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 210
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$100(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 211
    if-eqz v1, :cond_c

    .line 212
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\nRear Camera (73CX)\nCam FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamCompanionFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Phone FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneCompanionFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 198
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->REAR_QCLOADED_ISP:Z

    if-eqz v3, :cond_b

    .line 199
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rear Camera (Main)\nCam FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Load FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getLoadedFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Phone FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 204
    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rear Camera (Main)\nCam FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Phone FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 216
    :cond_c
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->REAR_QCLOADED_ISP:Z

    if-eqz v3, :cond_d

    .line 217
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\nRear Camera (73CX)\nCam FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamCompanionFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Load FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getLoadedCompanionFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Phone FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneCompanionFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 222
    :cond_d
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\nRear Camera (73CX)\nCam FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamCompanionFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Phone FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneCompanionFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 230
    :cond_e
    if-eqz v1, :cond_f

    .line 231
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rear Camera (Main)\nCam FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Phone FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 235
    :cond_f
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rear Camera (Main)\nCam FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Isp FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getIspFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Phone FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 262
    :cond_10
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\nFront Camera\nCam FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Phone FW Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 284
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-eq v3, v8, :cond_11

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, v5, :cond_15

    .line 285
    :cond_11
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v3, :cond_12

    .line 286
    const-string v3, "CameraFirmware"

    const-string v4, "mRear is null, rear FW info will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 289
    :cond_12
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 298
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    if-eqz v3, :cond_13

    .line 300
    :try_start_0
    const-string v3, "CameraFirmware"

    const-string v4, "mCameraDevice.setFirmwareMode(CameraDeviceController.FIRMWAREMODE_VERSION)"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 309
    :cond_13
    :goto_5
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$100(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, v5, :cond_14

    .line 310
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdate:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$302(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z

    .line 311
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdateCountCheck:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$402(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z

    .line 314
    :cond_14
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->JF_FIRMWARE_FORCE_UPDATE:Z

    if-eqz v3, :cond_17

    .line 315
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware;->DialogPopup(I)Landroid/app/AlertDialog;
    invoke-static {v4, v7}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$600(Lcom/sec/android/app/camerafirmware/CameraFirmware;I)Landroid/app/AlertDialog;

    move-result-object v4

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mPopup:Landroid/app/AlertDialog;
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$502(Lcom/sec/android/app/camerafirmware/CameraFirmware;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 316
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mPopup:Landroid/app/AlertDialog;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$500(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 291
    :cond_15
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    if-nez v3, :cond_16

    .line 292
    const-string v3, "CameraFirmware"

    const-string v4, "mFront is null, front FW infor will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    .line 295
    :cond_16
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    goto :goto_4

    .line 320
    :cond_17
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-eq v3, v8, :cond_18

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f090018

    if-ne v3, v4, :cond_19

    :cond_18
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->ChkUserFirmwareFile()Z

    move-result v3

    if-nez v3, :cond_1a

    :cond_19
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, v5, :cond_1b

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->ChkUserCompanionFirmwareFile()Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 322
    :cond_1a
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware;->DialogPopup(I)Landroid/app/AlertDialog;
    invoke-static {v4, v7}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$600(Lcom/sec/android/app/camerafirmware/CameraFirmware;I)Landroid/app/AlertDialog;

    move-result-object v4

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mPopup:Landroid/app/AlertDialog;
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$502(Lcom/sec/android/app/camerafirmware/CameraFirmware;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 323
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mPopup:Landroid/app/AlertDialog;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$500(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 327
    :cond_1b
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware;->isSameVendor()Z
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$700(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 328
    const-string v3, "CameraFirmware"

    const-string v4, "valid vendor"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->IsNewFirmwareDate()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    goto/16 :goto_0

    .line 356
    :pswitch_2
    const-string v3, "CameraFirmware"

    const-string v4, "This is the latest version.."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    const v4, 0x7f0a0010

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware;->dialogErrorPopup(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$800(Lcom/sec/android/app/camerafirmware/CameraFirmware;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 364
    :catch_0
    move-exception v0

    .line 365
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "CameraFirmware"

    const-string v4, "Something goes wrong"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    const v4, 0x7f0a000f

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware;->dialogErrorPopup(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$800(Lcom/sec/android/app/camerafirmware/CameraFirmware;I)V

    goto/16 :goto_0

    .line 332
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_3
    :try_start_2
    const-string v3, "CameraFirmware"

    const-string v4, "Updating New Firmwareversion..."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    sget-object v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_UPDATE:[B

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->setManageMode([B)V

    .line 336
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->showUpdateProgress()V

    .line 337
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->startUpdateThread()V

    goto/16 :goto_0

    .line 342
    :pswitch_4
    const-string v3, "CameraFirmware"

    const-string v4, "Same date.."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->IsNewFirmwareVersion()Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 344
    const-string v3, "CameraFirmware"

    const-string v4, "Updating New Firmwareversion..."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    sget-object v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_UPDATE:[B

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->setManageMode([B)V

    .line 348
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->showUpdateProgress()V

    .line 349
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->startUpdateThread()V

    goto/16 :goto_0

    .line 351
    :cond_1c
    const-string v3, "CameraFirmware"

    const-string v4, "This is the latest version.."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    const v4, 0x7f0a0010

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware;->dialogErrorPopup(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$800(Lcom/sec/android/app/camerafirmware/CameraFirmware;I)V

    goto/16 :goto_0

    .line 361
    :cond_1d
    const-string v3, "CameraFirmware"

    const-string v4, "Invalid vendor"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    const v4, 0x7f0a000e

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware;->dialogErrorPopup(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$800(Lcom/sec/android/app/camerafirmware/CameraFirmware;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 374
    :pswitch_5
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v3, :cond_1e

    .line 375
    const-string v3, "CameraFirmware"

    const-string v4, "mRear is null, rear FW info will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 378
    :cond_1e
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 380
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    if-eqz v3, :cond_1f

    .line 382
    :try_start_3
    const-string v3, "CameraFirmware"

    const-string v4, "mCameraDevice.setFirmwareMode(CameraDeviceController.FIRMWAREMODE_VERSION)"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    .line 389
    :cond_1f
    :goto_6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f090015

    if-ne v3, v4, :cond_20

    .line 390
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mForceUpdate:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$902(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z

    .line 395
    :goto_7
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware;->DialogPopup(I)Landroid/app/AlertDialog;
    invoke-static {v4, v7}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$600(Lcom/sec/android/app/camerafirmware/CameraFirmware;I)Landroid/app/AlertDialog;

    move-result-object v4

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mPopup:Landroid/app/AlertDialog;
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$502(Lcom/sec/android/app/camerafirmware/CameraFirmware;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 396
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mPopup:Landroid/app/AlertDialog;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$500(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 392
    :cond_20
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompForceUpdate:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$1002(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z

    goto :goto_7

    .line 399
    :pswitch_6
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    if-eqz v3, :cond_21

    .line 400
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->cancel()V

    .line 402
    :cond_21
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    if-eqz v3, :cond_22

    .line 403
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V

    .line 406
    :cond_22
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v3, :cond_23

    .line 407
    const-string v3, "CameraFirmware"

    const-string v4, "mRear is null, rear FW info will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 410
    :cond_23
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 411
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rear Camera\n ISP Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getISPVer1()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 413
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    if-nez v3, :cond_24

    .line 414
    const-string v3, "CameraFirmware"

    const-string v4, "mFront is null, front FW infor will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    .line 417
    :cond_24
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 418
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\nFront Camera\n ISP Ver : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getISPVer1()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 420
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-static {v4, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$002(Lcom/sec/android/app/camerafirmware/CameraFirmware;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 422
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 424
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    if-nez v3, :cond_25

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$100(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 425
    :cond_25
    const-string v3, "CameraFirmware"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CameraFirmware.getISPVersion mCameraDevice : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    if-eqz v3, :cond_0

    .line 427
    const-string v3, "CameraFirmware"

    const-string v4, "ISP Ver Check stopCameraPreview"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->stopPreview()V

    goto/16 :goto_0

    .line 434
    :pswitch_7
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    if-eqz v3, :cond_26

    .line 435
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->cancel()V

    .line 438
    :cond_26
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$100(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 439
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rear Camera (Main)\n( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_USER:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_ENG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " )\n\nRear Camera (73CX)\n( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_73CX_USER:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_73CX_ENG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " )\n\nFront\n( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_USER_FRONT:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_ENG_FRONT:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " )"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 454
    :goto_8
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-static {v4, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$002(Lcom/sec/android/app/camerafirmware/CameraFirmware;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 455
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 447
    :cond_27
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rear Camera (Main)\n( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_USER:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_ENG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " )\n\nFront\n( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_USER_FRONT:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_ENG_FRONT:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " )"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_8

    .line 459
    :pswitch_8
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    if-eqz v3, :cond_28

    .line 460
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->cancel()V

    .line 463
    :cond_28
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    if-eqz v3, :cond_29

    .line 464
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V

    .line 467
    :cond_29
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v3, :cond_2a

    .line 468
    const-string v3, "CameraFirmware"

    const-string v4, "mRear is null, rear FW info will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 471
    :cond_2a
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 472
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rear Camera (Main)\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWCalMain()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 475
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$100(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 476
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\nRear Camera (73CX)\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamCompanionFWCalMain()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 480
    :cond_2b
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    if-nez v3, :cond_2c

    .line 481
    const-string v3, "CameraFirmware"

    const-string v4, "mFront is null, front FW infor will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    .line 484
    :cond_2c
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 485
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\nFront Camera\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWCalFront()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 488
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-static {v4, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$002(Lcom/sec/android/app/camerafirmware/CameraFirmware;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 490
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 492
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    if-nez v3, :cond_2d

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$100(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 493
    :cond_2d
    const-string v3, "CameraFirmware"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CameraFirmware.getCheckCal mCameraDevice : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v5, v5, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    if-eqz v3, :cond_0

    .line 495
    const-string v3, "CameraFirmware"

    const-string v4, "CAM FW Cal Check stopCameraPreview"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->stopPreview()V

    goto/16 :goto_0

    .line 504
    :pswitch_9
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f09001d

    if-eq v3, v4, :cond_2e

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, v9, :cond_30

    .line 505
    :cond_2e
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v3, :cond_2f

    .line 506
    const-string v3, "CameraFirmware"

    const-string v4, "mRear is null, rear FW info will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 509
    :cond_2f
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 518
    :goto_9
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, v9, :cond_32

    .line 519
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    sget-object v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_COMP_FIRMWARE_DUMP:[B

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->setManageMode([B)V

    .line 524
    :goto_a
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->showUpdateProgress(Z)V

    .line 525
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->startUpdateThread()V

    goto/16 :goto_0

    .line 511
    :cond_30
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    if-nez v3, :cond_31

    .line 512
    const-string v3, "CameraFirmware"

    const-string v4, "mFront is null, front FW infor will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    .line 515
    :cond_31
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    goto :goto_9

    .line 521
    :cond_32
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    sget-object v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_DUMP:[B

    invoke-virtual {v3, v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->setManageMode([B)V

    goto :goto_a

    .line 528
    :pswitch_a
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    if-eqz v3, :cond_33

    .line 529
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->cancel()V

    .line 532
    :cond_33
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v3, :cond_34

    .line 533
    const-string v3, "CameraFirmware"

    const-string v4, "mRear is null, rear FW info will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 536
    :cond_34
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 537
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rear Camera\nISP Core Voltage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getISPVoltage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 539
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-static {v4, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$002(Lcom/sec/android/app/camerafirmware/CameraFirmware;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 541
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 544
    :pswitch_b
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    if-eqz v3, :cond_35

    .line 545
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->cancel()V

    .line 548
    :cond_35
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v3, :cond_36

    .line 549
    const-string v3, "CameraFirmware"

    const-string v4, "mRear is null, rear FW info will be created"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    new-instance v4, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 552
    :cond_36
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v4, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 553
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Vendor ID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, v4, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getVendorID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 554
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-static {v4, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$002(Lcom/sec/android/app/camerafirmware/CameraFirmware;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 556
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 384
    :catch_1
    move-exception v3

    goto/16 :goto_6

    .line 304
    :catch_2
    move-exception v3

    goto/16 :goto_5

    .line 170
    :pswitch_data_0
    .packed-switch 0x7f090014
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 330
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
