.class public final Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;
.super Ljava/lang/Object;
.source "DataMatrixReader.java"

# interfaces
.implements Lcom/sec/android/app/camaftest/core/Reader;


# static fields
.field private static final NO_POINTS:[Lcom/sec/android/app/camaftest/core/ResultPoint;


# instance fields
.field private final decoder:Lcom/sec/android/app/camaftest/core/datamatrix/decoder/Decoder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/sec/android/app/camaftest/core/ResultPoint;

    sput-object v0, Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;->NO_POINTS:[Lcom/sec/android/app/camaftest/core/ResultPoint;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lcom/sec/android/app/camaftest/core/datamatrix/decoder/Decoder;

    invoke-direct {v0}, Lcom/sec/android/app/camaftest/core/datamatrix/decoder/Decoder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;->decoder:Lcom/sec/android/app/camaftest/core/datamatrix/decoder/Decoder;

    return-void
.end method

.method private static extractPureBits(Lcom/sec/android/app/camaftest/core/common/BitMatrix;)Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .locals 15
    .param p0, "image"    # Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/core/common/BitMatrix;->getTopLeftOnBit()[I

    move-result-object v4

    .line 106
    .local v4, "leftTopBlack":[I
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/core/common/BitMatrix;->getBottomRightOnBit()[I

    move-result-object v10

    .line 107
    .local v10, "rightBottomBlack":[I
    if-eqz v4, :cond_0

    if-nez v10, :cond_1

    .line 108
    :cond_0
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v14

    throw v14

    .line 111
    :cond_1
    invoke-static {v4, p0}, Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;->moduleSize([ILcom/sec/android/app/camaftest/core/common/BitMatrix;)I

    move-result v7

    .line 113
    .local v7, "moduleSize":I
    const/4 v14, 0x1

    aget v11, v4, v14

    .line 114
    .local v11, "top":I
    const/4 v14, 0x1

    aget v1, v10, v14

    .line 115
    .local v1, "bottom":I
    const/4 v14, 0x0

    aget v3, v4, v14

    .line 116
    .local v3, "left":I
    const/4 v14, 0x0

    aget v9, v10, v14

    .line 118
    .local v9, "right":I
    sub-int v14, v9, v3

    add-int/lit8 v14, v14, 0x1

    div-int v6, v14, v7

    .line 119
    .local v6, "matrixWidth":I
    sub-int v14, v1, v11

    add-int/lit8 v14, v14, 0x1

    div-int v5, v14, v7

    .line 120
    .local v5, "matrixHeight":I
    if-lez v6, :cond_2

    if-gtz v5, :cond_3

    .line 121
    :cond_2
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v14

    throw v14

    .line 127
    :cond_3
    shr-int/lit8 v8, v7, 0x1

    .line 128
    .local v8, "nudge":I
    add-int/2addr v11, v8

    .line 129
    add-int/2addr v3, v8

    .line 132
    new-instance v0, Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    invoke-direct {v0, v6, v5}, Lcom/sec/android/app/camaftest/core/common/BitMatrix;-><init>(II)V

    .line 133
    .local v0, "bits":Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    const/4 v13, 0x0

    .local v13, "y":I
    :goto_0
    if-ge v13, v5, :cond_6

    .line 134
    mul-int v14, v13, v7

    add-int v2, v11, v14

    .line 135
    .local v2, "iOffset":I
    const/4 v12, 0x0

    .local v12, "x":I
    :goto_1
    if-ge v12, v6, :cond_5

    .line 136
    mul-int v14, v12, v7

    add-int/2addr v14, v3

    invoke-virtual {p0, v14, v2}, Lcom/sec/android/app/camaftest/core/common/BitMatrix;->get(II)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 137
    invoke-virtual {v0, v12, v13}, Lcom/sec/android/app/camaftest/core/common/BitMatrix;->set(II)V

    .line 135
    :cond_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 133
    :cond_5
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 141
    .end local v2    # "iOffset":I
    .end local v12    # "x":I
    :cond_6
    return-object v0
.end method

.method private static moduleSize([ILcom/sec/android/app/camaftest/core/common/BitMatrix;)I
    .locals 6
    .param p0, "leftTopBlack"    # [I
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 145
    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/common/BitMatrix;->getWidth()I

    move-result v1

    .line 146
    .local v1, "width":I
    aget v2, p0, v5

    .line 147
    .local v2, "x":I
    const/4 v4, 0x1

    aget v3, p0, v4

    .line 148
    .local v3, "y":I
    :goto_0
    if-ge v2, v1, :cond_0

    invoke-virtual {p1, v2, v3}, Lcom/sec/android/app/camaftest/core/common/BitMatrix;->get(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 149
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 151
    :cond_0
    if-ne v2, v1, :cond_1

    .line 152
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v4

    throw v4

    .line 155
    :cond_1
    aget v4, p0, v5

    sub-int v0, v2, v4

    .line 156
    .local v0, "moduleSize":I
    if-nez v0, :cond_2

    .line 157
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v4

    throw v4

    .line 159
    :cond_2
    return v0
.end method


# virtual methods
.method public decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 1
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/ChecksumException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;->decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    return-object v0
.end method

.method public decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 10
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/camaftest/core/BinaryBitmap;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/ChecksumException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    .line 67
    .local p2, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    if-eqz p2, :cond_2

    sget-object v7, Lcom/sec/android/app/camaftest/core/DecodeHintType;->PURE_BARCODE:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    invoke-interface {p2, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 68
    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->getBlackMatrix()Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;->extractPureBits(Lcom/sec/android/app/camaftest/core/common/BitMatrix;)Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    move-result-object v0

    .line 69
    .local v0, "bits":Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    iget-object v7, p0, Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;->decoder:Lcom/sec/android/app/camaftest/core/datamatrix/decoder/Decoder;

    invoke-virtual {v7, v0}, Lcom/sec/android/app/camaftest/core/datamatrix/decoder/Decoder;->decode(Lcom/sec/android/app/camaftest/core/common/BitMatrix;)Lcom/sec/android/app/camaftest/core/common/DecoderResult;

    move-result-object v2

    .line 70
    .local v2, "decoderResult":Lcom/sec/android/app/camaftest/core/common/DecoderResult;
    sget-object v5, Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;->NO_POINTS:[Lcom/sec/android/app/camaftest/core/ResultPoint;

    .line 76
    .end local v0    # "bits":Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .local v5, "points":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    :goto_0
    new-instance v6, Lcom/sec/android/app/camaftest/core/Result;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/common/DecoderResult;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/common/DecoderResult;->getRawBytes()[B

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->DATA_MATRIX:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-direct {v6, v7, v8, v5, v9}, Lcom/sec/android/app/camaftest/core/Result;-><init>(Ljava/lang/String;[B[Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/BarcodeFormat;)V

    .line 78
    .local v6, "result":Lcom/sec/android/app/camaftest/core/Result;
    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/common/DecoderResult;->getByteSegments()Ljava/util/List;

    move-result-object v1

    .line 79
    .local v1, "byteSegments":Ljava/util/List;, "Ljava/util/List<[B>;"
    if-eqz v1, :cond_0

    .line 80
    sget-object v7, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->BYTE_SEGMENTS:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-virtual {v6, v7, v1}, Lcom/sec/android/app/camaftest/core/Result;->putMetadata(Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;)V

    .line 82
    :cond_0
    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/common/DecoderResult;->getECLevel()Ljava/lang/String;

    move-result-object v4

    .line 83
    .local v4, "ecLevel":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 84
    sget-object v7, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->ERROR_CORRECTION_LEVEL:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-virtual {v6, v7, v4}, Lcom/sec/android/app/camaftest/core/Result;->putMetadata(Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;)V

    .line 86
    :cond_1
    return-object v6

    .line 72
    .end local v1    # "byteSegments":Ljava/util/List;, "Ljava/util/List<[B>;"
    .end local v2    # "decoderResult":Lcom/sec/android/app/camaftest/core/common/DecoderResult;
    .end local v4    # "ecLevel":Ljava/lang/String;
    .end local v5    # "points":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    .end local v6    # "result":Lcom/sec/android/app/camaftest/core/Result;
    :cond_2
    new-instance v7, Lcom/sec/android/app/camaftest/core/datamatrix/detector/Detector;

    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->getBlackMatrix()Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/sec/android/app/camaftest/core/datamatrix/detector/Detector;-><init>(Lcom/sec/android/app/camaftest/core/common/BitMatrix;)V

    invoke-virtual {v7}, Lcom/sec/android/app/camaftest/core/datamatrix/detector/Detector;->detect()Lcom/sec/android/app/camaftest/core/common/DetectorResult;

    move-result-object v3

    .line 73
    .local v3, "detectorResult":Lcom/sec/android/app/camaftest/core/common/DetectorResult;
    iget-object v7, p0, Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;->decoder:Lcom/sec/android/app/camaftest/core/datamatrix/decoder/Decoder;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/DetectorResult;->getBits()Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/camaftest/core/datamatrix/decoder/Decoder;->decode(Lcom/sec/android/app/camaftest/core/common/BitMatrix;)Lcom/sec/android/app/camaftest/core/common/DecoderResult;

    move-result-object v2

    .line 74
    .restart local v2    # "decoderResult":Lcom/sec/android/app/camaftest/core/common/DecoderResult;
    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/DetectorResult;->getPoints()[Lcom/sec/android/app/camaftest/core/ResultPoint;

    move-result-object v5

    .restart local v5    # "points":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    goto :goto_0
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 92
    return-void
.end method
