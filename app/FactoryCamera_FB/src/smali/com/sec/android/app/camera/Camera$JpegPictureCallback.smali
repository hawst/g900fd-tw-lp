.class final Lcom/sec/android/app/camera/Camera$JpegPictureCallback;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "JpegPictureCallback"
.end annotation


# instance fields
.field mJpegData:[B

.field final synthetic this$0:Lcom/sec/android/app/camera/Camera;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camera/Camera;Landroid/location/Location;)V
    .locals 1
    .param p2, "loc"    # Landroid/location/Location;

    .prologue
    .line 603
    iput-object p1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 601
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->mJpegData:[B

    .line 605
    return-void
.end method


# virtual methods
.method public SavingImageForCaptureIntent(Landroid/hardware/Camera;)V
    .locals 6
    .param p1, "camera"    # Landroid/hardware/Camera;

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 624
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->mJpegData:[B

    if-eqz v1, :cond_0

    .line 627
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->mJpegData:[B

    iput-object v2, v1, Lcom/sec/android/app/camera/Camera;->capturedData:[B

    .line 628
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->mJpegData:[B

    .line 631
    :cond_0
    const-string v1, "FactoryCamera"

    const-string v2, "mStatus = SNAPSHOT_COMPLETED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    const/4 v2, 0x4

    # setter for: Lcom/sec/android/app/camera/Camera;->mStatus:I
    invoke-static {v1, v2}, Lcom/sec/android/app/camera/Camera;->access$402(Lcom/sec/android/app/camera/Camera;I)I

    .line 634
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v1, v1, Lcom/sec/android/app/camera/Camera;->capturedData:[B

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->storeImage([BLandroid/hardware/Camera;)V

    .line 636
    const-string v1, "selftest"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->isLowLightCaptureTesting()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 637
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/camera/Camera;->mLowLightCaptureTestSkip:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/camera/Camera;->access$2202(Lcom/sec/android/app/camera/Camera;Z)Z

    .line 639
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v1, v1, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v1, v1, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    if-eqz v1, :cond_3

    .line 640
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v1, v1, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_POSTVIEW_TEST:Z

    if-eqz v1, :cond_3

    .line 641
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-static {}, Lcom/sec/android/app/camera/CameraStorage;->getInstance()Lcom/sec/android/app/camera/CameraStorage;

    move-result-object v2

    # setter for: Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;
    invoke-static {v1, v2}, Lcom/sec/android/app/camera/Camera;->access$2302(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/CameraStorage;)Lcom/sec/android/app/camera/CameraStorage;

    .line 642
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$2300(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/CameraStorage;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cameraType:I
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$1200(Lcom/sec/android/app/camera/Camera;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->filePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$2400(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/camera/CameraStorage;->setFilePath(ILjava/lang/String;)V

    .line 643
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->displayLowLightCaptureImage()V

    .line 691
    :goto_0
    return-void

    .line 648
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->clearFocusState()V
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$700(Lcom/sec/android/app/camera/Camera;)V

    .line 649
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->updateFocusIndicator()V
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$800(Lcom/sec/android/app/camera/Camera;)V

    .line 650
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->restartPreview()V
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$2500(Lcom/sec/android/app/camera/Camera;)V

    goto :goto_0

    .line 652
    :cond_4
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SavingImageForCaptureIntent() : testType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 654
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mImageIndexCount:I
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$2600(Lcom/sec/android/app/camera/Camera;)I

    move-result v1

    const/16 v2, 0x63

    if-lt v1, v2, :cond_6

    .line 655
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # setter for: Lcom/sec/android/app/camera/Camera;->mImageIndexCount:I
    invoke-static {v1, v4}, Lcom/sec/android/app/camera/Camera;->access$2602(Lcom/sec/android/app/camera/Camera;I)I

    .line 660
    :goto_1
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SavingImageForCaptureIntent() : bIsUsbOrUartCommand : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$1700(Lcom/sec/android/app/camera/Camera;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$1700(Lcom/sec/android/app/camera/Camera;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 662
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    const-string v2, "com.android.samsungtest.CAMERA_SHOT_ACK"

    # invokes: Lcom/sec/android/app/camera/Camera;->sendBroadCastAck(Ljava/lang/String;I)V
    invoke-static {v1, v2, v4}, Lcom/sec/android/app/camera/Camera;->access$2700(Lcom/sec/android/app/camera/Camera;Ljava/lang/String;I)V

    .line 665
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # setter for: Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/camera/Camera;->access$1702(Lcom/sec/android/app/camera/Camera;Z)Z

    .line 667
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-static {}, Lcom/sec/android/app/camera/CameraStorage;->getInstance()Lcom/sec/android/app/camera/CameraStorage;

    move-result-object v2

    # setter for: Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;
    invoke-static {v1, v2}, Lcom/sec/android/app/camera/Camera;->access$2302(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/CameraStorage;)Lcom/sec/android/app/camera/CameraStorage;

    .line 668
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$2300(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/CameraStorage;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cameraType:I
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$1200(Lcom/sec/android/app/camera/Camera;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->filePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$2400(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/camera/CameraStorage;->setFilePath(ILjava/lang/String;)V

    .line 670
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->clearFocusState()V
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$700(Lcom/sec/android/app/camera/Camera;)V

    .line 671
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->updateFocusIndicator()V
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$800(Lcom/sec/android/app/camera/Camera;)V

    .line 672
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->restartPreview()V
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$2500(Lcom/sec/android/app/camera/Camera;)V

    goto/16 :goto_0

    .line 657
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # operator++ for: Lcom/sec/android/app/camera/Camera;->mImageIndexCount:I
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$2608(Lcom/sec/android/app/camera/Camera;)I

    goto :goto_1

    .line 674
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v1, v1, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_POSTVIEW_TEST:Z

    if-eqz v1, :cond_9

    .line 675
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SavingImageForCaptureIntent() : cameratype : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cameraType:I
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$1200(Lcom/sec/android/app/camera/Camera;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " filePath "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->filePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$2400(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-static {}, Lcom/sec/android/app/camera/CameraStorage;->getInstance()Lcom/sec/android/app/camera/CameraStorage;

    move-result-object v2

    # setter for: Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;
    invoke-static {v1, v2}, Lcom/sec/android/app/camera/Camera;->access$2302(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/CameraStorage;)Lcom/sec/android/app/camera/CameraStorage;

    .line 677
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$2300(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/CameraStorage;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cameraType:I
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$1200(Lcom/sec/android/app/camera/Camera;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->filePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$2400(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/camera/CameraStorage;->setFilePath(ILjava/lang/String;)V

    .line 678
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cameraType:I
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$1200(Lcom/sec/android/app/camera/Camera;)I

    move-result v1

    if-nez v1, :cond_8

    .line 679
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 680
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "data_filepath"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->filePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$2400(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 681
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1, v5, v0}, Lcom/sec/android/app/camera/Camera;->setResult(ILandroid/content/Intent;)V

    .line 688
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_8
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$500(Lcom/sec/android/app/camera/Camera;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 684
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 685
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "data_filepath"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->filePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$2400(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 686
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1, v5, v0}, Lcom/sec/android/app/camera/Camera;->setResult(ILandroid/content/Intent;)V

    goto :goto_2
.end method

.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 6
    .param p1, "jpegData"    # [B
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 608
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mPausing:Z
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$2100(Lcom/sec/android/app/camera/Camera;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621
    :goto_0
    return-void

    .line 613
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/camera/Camera;->mJpegPictureCallbackTime:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/camera/Camera;->access$1002(Lcom/sec/android/app/camera/Camera;J)J

    .line 614
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mJpegPictureCallbackTime:J
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$1000(Lcom/sec/android/app/camera/Camera;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mRawPictureCallbackTime:J
    invoke-static {v4}, Lcom/sec/android/app/camera/Camera;->access$2000(Lcom/sec/android/app/camera/Camera;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms elapsed between"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " RawPictureCallback and JpegPictureCallback."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    iput-object p1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->mJpegData:[B

    .line 620
    invoke-virtual {p0, p2}, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->SavingImageForCaptureIntent(Landroid/hardware/Camera;)V

    goto :goto_0
.end method

.method public storeImage([BLandroid/hardware/Camera;)V
    .locals 12
    .param p1, "data"    # [B
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 694
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "storeImage : bUseSdcard["

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v6, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->bUseSdcard:Z
    invoke-static {v6}, Lcom/sec/android/app/camera/Camera;->access$2800(Lcom/sec/android/app/camera/Camera;)Z

    move-result v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "]"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 696
    .local v4, "dateTaken":J
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->createName(J)Ljava/lang/String;
    invoke-static {v2, v4, v5}, Lcom/sec/android/app/camera/Camera;->access$2900(Lcom/sec/android/app/camera/Camera;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 699
    .local v3, "name":Ljava/lang/String;
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->bUseSdcard:Z
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$2800(Lcom/sec/android/app/camera/Camera;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 700
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->getAvailableSpaceSd()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-lez v1, :cond_0

    .line 701
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SavingImageForCaptureIntent() : testType : "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v6, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/camera/Camera;->access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 703
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_AUTO_TEST:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->access$3000()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/camera/Camera;->writeImage(Ljava/lang/String;Ljava/lang/String;[B)V
    invoke-static {v1, v2, v3, p1}, Lcom/sec/android/app/camera/Camera;->access$3100(Lcom/sec/android/app/camera/Camera;Ljava/lang/String;Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 728
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getOrientationOnTake()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/Camera;->calculateOrientationForPicture(I)I

    move-result v7

    .line 730
    .local v7, "orientationForPicture":I
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->access$3500()Landroid/content/ContentResolver;

    move-result-object v2

    move-object v6, p1

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/camera/Camera;->addImage(Landroid/content/ContentResolver;Ljava/lang/String;J[BI)Landroid/net/Uri;

    .line 731
    return-void

    .line 705
    .end local v7    # "orientationForPicture":I
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->isLowLightCaptureTesting()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 706
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_LOW_LIGHT_CAPTURE_TEST:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->access$3200()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/camera/Camera;->writeImage(Ljava/lang/String;Ljava/lang/String;[B)V
    invoke-static {v1, v2, v3, p1}, Lcom/sec/android/app/camera/Camera;->access$3100(Lcom/sec/android/app/camera/Camera;Ljava/lang/String;Ljava/lang/String;[B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 724
    :catch_0
    move-exception v0

    .line 725
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "FactoryCamera"

    const-string v2, "Exception while compressing image."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 708
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_SDCARD:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->access$3300()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/camera/Camera;->writeImage(Ljava/lang/String;Ljava/lang/String;[B)V
    invoke-static {v1, v2, v3, p1}, Lcom/sec/android/app/camera/Camera;->access$3100(Lcom/sec/android/app/camera/Camera;Ljava/lang/String;Ljava/lang/String;[B)V

    goto :goto_0

    .line 713
    :cond_3
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SavingImageForCaptureIntent() : testType : "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v6, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/camera/Camera;->access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 715
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_AUTO_TEST:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->access$3000()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/camera/Camera;->writeImage(Ljava/lang/String;Ljava/lang/String;[B)V
    invoke-static {v1, v2, v3, p1}, Lcom/sec/android/app/camera/Camera;->access$3100(Lcom/sec/android/app/camera/Camera;Ljava/lang/String;Ljava/lang/String;[B)V

    goto :goto_0

    .line 717
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Camera;->isLowLightCaptureTesting()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 718
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_LOW_LIGHT_CAPTURE_TEST:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->access$3200()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/camera/Camera;->writeImage(Ljava/lang/String;Ljava/lang/String;[B)V
    invoke-static {v1, v2, v3, p1}, Lcom/sec/android/app/camera/Camera;->access$3100(Lcom/sec/android/app/camera/Camera;Ljava/lang/String;Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 720
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_DATA:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->access$3400()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/camera/Camera;->writeImage(Ljava/lang/String;Ljava/lang/String;[B)V
    invoke-static {v1, v2, v3, p1}, Lcom/sec/android/app/camera/Camera;->access$3100(Lcom/sec/android/app/camera/Camera;Ljava/lang/String;Ljava/lang/String;[B)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0
.end method
