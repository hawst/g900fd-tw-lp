.class public Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;
.super Landroid/os/CountDownTimer;
.source "CameraFirmwareUpdate_Booting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CountDown"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;JJ)V
    .locals 0
    .param p2, "millisInFuture"    # J
    .param p4, "countDownInterval"    # J

    .prologue
    .line 263
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    .line 264
    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 265
    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->hideUpdateProgress()V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->setBlockHold(Z)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;Z)Z

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->SendFWUpdateInfo(Z)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->finish()V

    .line 277
    return-void
.end method

.method public onTick(J)V
    .locals 3
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->isNeedUpdate()Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    const-string v1, "Camera ISP Firmware updated"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->SendFWUpdateInfo(Z)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->finish()V

    .line 296
    :cond_0
    return-void
.end method
