.class public final Lcom/sec/android/app/camaftest/core/multi/ByQuadrantReader;
.super Ljava/lang/Object;
.source "ByQuadrantReader.java"

# interfaces
.implements Lcom/sec/android/app/camaftest/core/Reader;


# instance fields
.field private final delegate:Lcom/sec/android/app/camaftest/core/Reader;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camaftest/core/Reader;)V
    .locals 0
    .param p1, "delegate"    # Lcom/sec/android/app/camaftest/core/Reader;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/camaftest/core/multi/ByQuadrantReader;->delegate:Lcom/sec/android/app/camaftest/core/Reader;

    .line 44
    return-void
.end method


# virtual methods
.method public decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 1
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/ChecksumException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/camaftest/core/multi/ByQuadrantReader;->decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    return-object v0
.end method

.method public decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 13
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/camaftest/core/BinaryBitmap;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/ChecksumException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    .local p2, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    const/4 v12, 0x0

    .line 56
    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->getWidth()I

    move-result v10

    .line 57
    .local v10, "width":I
    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->getHeight()I

    move-result v5

    .line 58
    .local v5, "height":I
    div-int/lit8 v4, v10, 0x2

    .line 59
    .local v4, "halfWidth":I
    div-int/lit8 v3, v5, 0x2

    .line 61
    .local v3, "halfHeight":I
    invoke-virtual {p1, v12, v12, v4, v3}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->crop(IIII)Lcom/sec/android/app/camaftest/core/BinaryBitmap;

    move-result-object v8

    .line 63
    .local v8, "topLeft":Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    :try_start_0
    iget-object v11, p0, Lcom/sec/android/app/camaftest/core/multi/ByQuadrantReader;->delegate:Lcom/sec/android/app/camaftest/core/Reader;

    invoke-interface {v11, v8, p2}, Lcom/sec/android/app/camaftest/core/Reader;->decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    :try_end_0
    .catch Lcom/sec/android/app/camaftest/core/NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 92
    :goto_0
    return-object v11

    .line 64
    :catch_0
    move-exception v11

    .line 68
    invoke-virtual {p1, v4, v12, v4, v3}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->crop(IIII)Lcom/sec/android/app/camaftest/core/BinaryBitmap;

    move-result-object v9

    .line 70
    .local v9, "topRight":Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    :try_start_1
    iget-object v11, p0, Lcom/sec/android/app/camaftest/core/multi/ByQuadrantReader;->delegate:Lcom/sec/android/app/camaftest/core/Reader;

    invoke-interface {v11, v9, p2}, Lcom/sec/android/app/camaftest/core/Reader;->decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    :try_end_1
    .catch Lcom/sec/android/app/camaftest/core/NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v11

    goto :goto_0

    .line 71
    :catch_1
    move-exception v11

    .line 75
    invoke-virtual {p1, v12, v3, v4, v3}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->crop(IIII)Lcom/sec/android/app/camaftest/core/BinaryBitmap;

    move-result-object v0

    .line 77
    .local v0, "bottomLeft":Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    :try_start_2
    iget-object v11, p0, Lcom/sec/android/app/camaftest/core/multi/ByQuadrantReader;->delegate:Lcom/sec/android/app/camaftest/core/Reader;

    invoke-interface {v11, v0, p2}, Lcom/sec/android/app/camaftest/core/Reader;->decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    :try_end_2
    .catch Lcom/sec/android/app/camaftest/core/NotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v11

    goto :goto_0

    .line 78
    :catch_2
    move-exception v11

    .line 82
    invoke-virtual {p1, v4, v3, v4, v3}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->crop(IIII)Lcom/sec/android/app/camaftest/core/BinaryBitmap;

    move-result-object v1

    .line 84
    .local v1, "bottomRight":Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    :try_start_3
    iget-object v11, p0, Lcom/sec/android/app/camaftest/core/multi/ByQuadrantReader;->delegate:Lcom/sec/android/app/camaftest/core/Reader;

    invoke-interface {v11, v1, p2}, Lcom/sec/android/app/camaftest/core/Reader;->decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    :try_end_3
    .catch Lcom/sec/android/app/camaftest/core/NotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v11

    goto :goto_0

    .line 85
    :catch_3
    move-exception v11

    .line 89
    div-int/lit8 v7, v4, 0x2

    .line 90
    .local v7, "quarterWidth":I
    div-int/lit8 v6, v3, 0x2

    .line 91
    .local v6, "quarterHeight":I
    invoke-virtual {p1, v7, v6, v4, v3}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->crop(IIII)Lcom/sec/android/app/camaftest/core/BinaryBitmap;

    move-result-object v2

    .line 92
    .local v2, "center":Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    iget-object v11, p0, Lcom/sec/android/app/camaftest/core/multi/ByQuadrantReader;->delegate:Lcom/sec/android/app/camaftest/core/Reader;

    invoke-interface {v11, v2, p2}, Lcom/sec/android/app/camaftest/core/Reader;->decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v11

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/multi/ByQuadrantReader;->delegate:Lcom/sec/android/app/camaftest/core/Reader;

    invoke-interface {v0}, Lcom/sec/android/app/camaftest/core/Reader;->reset()V

    .line 98
    return-void
.end method
