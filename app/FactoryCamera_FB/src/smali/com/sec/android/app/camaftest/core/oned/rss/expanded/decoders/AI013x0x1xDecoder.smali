.class final Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;
.super Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI01weightDecoder;
.source "AI013x0x1xDecoder.java"


# static fields
.field private static final DATE_SIZE:I = 0x10

.field private static final HEADER_SIZE:I = 0x8

.field private static final WEIGHT_SIZE:I = 0x14


# instance fields
.field private final dateCode:Ljava/lang/String;

.field private final firstAIdigits:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camaftest/core/common/BitArray;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "information"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .param p2, "firstAIdigits"    # Ljava/lang/String;
    .param p3, "dateCode"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI01weightDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;)V

    .line 48
    iput-object p3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;->dateCode:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;->firstAIdigits:Ljava/lang/String;

    .line 50
    return-void
.end method

.method private encodeCompressedDate(Ljava/lang/StringBuilder;I)V
    .locals 7
    .param p1, "buf"    # Ljava/lang/StringBuilder;
    .param p2, "currentPos"    # I

    .prologue
    const/16 v6, 0x30

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;->getGeneralDecoder()Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;

    move-result-object v4

    const/16 v5, 0x10

    invoke-virtual {v4, p2, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v2

    .line 69
    .local v2, "numericDate":I
    const v4, 0x9600

    if-ne v2, v4, :cond_0

    .line 95
    :goto_0
    return-void

    .line 73
    :cond_0
    const/16 v4, 0x28

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 74
    iget-object v4, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;->dateCode:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    const/16 v4, 0x29

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 77
    rem-int/lit8 v0, v2, 0x20

    .line 78
    .local v0, "day":I
    div-int/lit8 v2, v2, 0x20

    .line 79
    rem-int/lit8 v4, v2, 0xc

    add-int/lit8 v1, v4, 0x1

    .line 80
    .local v1, "month":I
    div-int/lit8 v2, v2, 0xc

    .line 81
    move v3, v2

    .line 83
    .local v3, "year":I
    div-int/lit8 v4, v3, 0xa

    if-nez v4, :cond_1

    .line 84
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 86
    :cond_1
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 87
    div-int/lit8 v4, v1, 0xa

    if-nez v4, :cond_2

    .line 88
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 90
    :cond_2
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 91
    div-int/lit8 v4, v0, 0xa

    if-nez v4, :cond_3

    .line 92
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 94
    :cond_3
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method protected addWeightCode(Ljava/lang/StringBuilder;I)V
    .locals 2
    .param p1, "buf"    # Ljava/lang/StringBuilder;
    .param p2, "weight"    # I

    .prologue
    .line 99
    const v1, 0x186a0

    div-int v0, p2, v1

    .line 100
    .local v0, "lastAI":I
    const/16 v1, 0x28

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;->firstAIdigits:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 103
    const/16 v1, 0x29

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 104
    return-void
.end method

.method protected checkWeight(I)I
    .locals 1
    .param p1, "weight"    # I

    .prologue
    .line 108
    const v0, 0x186a0

    rem-int v0, p1, v0

    return v0
.end method

.method public parseInformation()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;->getInformation()Lcom/sec/android/app/camaftest/core/common/BitArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v1

    const/16 v2, 0x54

    if-eq v1, v2, :cond_0

    .line 55
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v1

    throw v1

    .line 58
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .local v0, "buf":Ljava/lang/StringBuilder;
    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;->encodeCompressedGtin(Ljava/lang/StringBuilder;I)V

    .line 61
    const/16 v1, 0x30

    const/16 v2, 0x14

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;->encodeCompressedWeight(Ljava/lang/StringBuilder;II)V

    .line 62
    const/16 v1, 0x44

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;->encodeCompressedDate(Ljava/lang/StringBuilder;I)V

    .line 64
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
