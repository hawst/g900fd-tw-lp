.class public Lcom/sec/android/app/camera/OISTest;
.super Landroid/app/Activity;
.source "OISTest.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camera/OISTest$PreviewCallback;,
        Lcom/sec/android/app/camera/OISTest$MainHandler;,
        Lcom/sec/android/app/camera/OISTest$ShutterCallback;,
        Lcom/sec/android/app/camera/OISTest$ErrorCallback;
    }
.end annotation


# static fields
.field protected static final CAMERA_FIRMWARE_INFO_FILE_REAR:Ljava/lang/String; = "/sys/class/camera/rear/rear_camfw"

.field public static final KEY_POWER:Ljava/lang/String; = "Hold"

.field public static final KEY_VOLUME_UP:Ljava/lang/String; = "Volume Up"

.field public static final OIS_FAIL:Ljava/lang/String; = "OIS FAIL"

.field private static final STATE_CHECK_INIT:I = -0x1

.field private static final STATE_CHECK_X:I = 0x0

.field private static final STATE_CHECK_Y:I = 0x1

.field public static TAG:Ljava/lang/String;


# instance fields
.field public Feature:Lcom/sec/android/app/camera/Feature;

.field private camcorderpreview:[[I

.field private cameraPicSize:Ljava/lang/String;

.field private cameraType:I

.field private isPreviewStarted:Z

.field private mCameraDevice:Landroid/hardware/Camera;

.field private mCameraSettings:Lcom/sec/android/app/camera/framework/CameraSettings;

.field private mCheckingState:I

.field private mCurResolution:I

.field private mCurrentTime:J

.field private mDVFSHelper:Landroid/os/DVFSHelper;

.field private mErrorCallback:Lcom/sec/android/app/camera/OISTest$ErrorCallback;

.field private mErrorPopup:Landroid/app/AlertDialog;

.field private mFailButton:Landroid/widget/Button;

.field private mHelpText:Landroid/widget/TextView;

.field private mIsPressedBackkey:Z

.field private mKeyFailToast:Landroid/widget/Toast;

.field private mMainHandler:Landroid/os/Handler;

.field private mParameters:Landroid/hardware/Camera$Parameters;

.field private mPassButton:Landroid/widget/Button;

.field private mPausing:Z

.field private mPreviewCallback:Lcom/sec/android/app/camera/OISTest$PreviewCallback;

.field private mPreviewing:Z

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mShutterCallback:Lcom/sec/android/app/camera/OISTest$ShutterCallback;

.field private mStopCamera:Z

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceView:Landroid/view/SurfaceView;

.field protected mTimerHandler:Landroid/os/Handler;

.field protected mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mchkopencamera:Z

.field private supportedCPUCoreTable:[I

.field private supportedCPUFreqTable:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-string v0, "OISTest"

    sput-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 61
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 69
    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 70
    iput v4, p0, Lcom/sec/android/app/camera/OISTest;->cameraType:I

    .line 71
    const-string v0, "3264x1836"

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->cameraPicSize:Ljava/lang/String;

    .line 76
    iput v2, p0, Lcom/sec/android/app/camera/OISTest;->mCurResolution:I

    .line 79
    new-instance v0, Lcom/sec/android/app/camera/OISTest$MainHandler;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/camera/OISTest$MainHandler;-><init>(Lcom/sec/android/app/camera/OISTest;Lcom/sec/android/app/camera/OISTest$1;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mMainHandler:Landroid/os/Handler;

    .line 82
    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mErrorPopup:Landroid/app/AlertDialog;

    .line 85
    iput-boolean v2, p0, Lcom/sec/android/app/camera/OISTest;->mIsPressedBackkey:Z

    .line 87
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/camera/OISTest;->mCurrentTime:J

    .line 90
    iput-boolean v2, p0, Lcom/sec/android/app/camera/OISTest;->mchkopencamera:Z

    .line 92
    new-instance v0, Lcom/sec/android/app/camera/OISTest$ShutterCallback;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/camera/OISTest$ShutterCallback;-><init>(Lcom/sec/android/app/camera/OISTest;Lcom/sec/android/app/camera/OISTest$1;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mShutterCallback:Lcom/sec/android/app/camera/OISTest$ShutterCallback;

    .line 99
    iput v4, p0, Lcom/sec/android/app/camera/OISTest;->mCheckingState:I

    .line 114
    iput-boolean v2, p0, Lcom/sec/android/app/camera/OISTest;->mPausing:Z

    .line 116
    iput-boolean v2, p0, Lcom/sec/android/app/camera/OISTest;->mStopCamera:Z

    .line 117
    iput-boolean v2, p0, Lcom/sec/android/app/camera/OISTest;->mPreviewing:Z

    .line 118
    iput-boolean v2, p0, Lcom/sec/android/app/camera/OISTest;->isPreviewStarted:Z

    .line 131
    new-instance v0, Lcom/sec/android/app/camera/OISTest$PreviewCallback;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/OISTest$PreviewCallback;-><init>(Lcom/sec/android/app/camera/OISTest;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mPreviewCallback:Lcom/sec/android/app/camera/OISTest$PreviewCallback;

    .line 133
    new-instance v0, Lcom/sec/android/app/camera/OISTest$ErrorCallback;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/OISTest$ErrorCallback;-><init>(Lcom/sec/android/app/camera/OISTest;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mErrorCallback:Lcom/sec/android/app/camera/OISTest$ErrorCallback;

    .line 135
    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 139
    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mKeyFailToast:Landroid/widget/Toast;

    .line 493
    new-instance v0, Lcom/sec/android/app/camera/framework/CameraSettings;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/framework/CameraSettings;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mCameraSettings:Lcom/sec/android/app/camera/framework/CameraSettings;

    .line 785
    new-instance v0, Lcom/sec/android/app/camera/OISTest$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/OISTest$5;-><init>(Lcom/sec/android/app/camera/OISTest;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 878
    new-instance v0, Lcom/sec/android/app/camera/OISTest$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/OISTest$6;-><init>(Lcom/sec/android/app/camera/OISTest;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mTimerHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/camera/OISTest;)Lcom/sec/android/app/camera/OISTest$ErrorCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mErrorCallback:Lcom/sec/android/app/camera/OISTest$ErrorCallback;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/camera/OISTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/camera/OISTest;->mCheckingState:I

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/camera/OISTest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/android/app/camera/OISTest;->mCheckingState:I

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/camera/OISTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mHelpText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/camera/OISTest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;
    .param p1, "x1"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/OISTest;->finishOISTest(I)V

    return-void
.end method

.method static synthetic access$1402(Lcom/sec/android/app/camera/OISTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/android/app/camera/OISTest;->isPreviewStarted:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/camera/OISTest;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mPassButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/camera/OISTest;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mFailButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/camera/OISTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/android/app/camera/OISTest;->mStopCamera:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/camera/OISTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/sec/android/app/camera/OISTest;->mIsPressedBackkey:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/camera/OISTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/android/app/camera/OISTest;->mIsPressedBackkey:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/camera/OISTest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;
    .param p1, "x1"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/OISTest;->dialogErrorPopup(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/camera/OISTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/camera/OISTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/camera/OISTest;->cameraType:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/camera/OISTest;Landroid/hardware/Camera;)Landroid/hardware/Camera;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;
    .param p1, "x1"    # Landroid/hardware/Camera;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    return-object p1
.end method

.method static synthetic access$602(Lcom/sec/android/app/camera/OISTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/android/app/camera/OISTest;->mchkopencamera:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera$Parameters;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/camera/OISTest;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;
    .param p1, "x1"    # Landroid/hardware/Camera$Parameters;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/camera/OISTest;)[[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->camcorderpreview:[[I

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/camera/OISTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/OISTest;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->cameraPicSize:Ljava/lang/String;

    return-object v0
.end method

.method private closeCamera()V
    .locals 2

    .prologue
    .line 704
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "closeCamera"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 706
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 707
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    .line 709
    :cond_0
    return-void
.end method

.method private dialogErrorPopup(I)V
    .locals 4
    .param p1, "messageId"    # I

    .prologue
    const/4 v3, 0x0

    .line 759
    sget-object v1, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v2, "dialogErrorPopup"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 762
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 765
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/camera/OISTest;->mchkopencamera:Z

    if-nez v1, :cond_1

    .line 766
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 767
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a000d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 769
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 770
    const v1, 0x7f0a0001

    new-instance v2, Lcom/sec/android/app/camera/OISTest$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camera/OISTest$4;-><init>(Lcom/sec/android/app/camera/OISTest;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 778
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 779
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/OISTest;->mErrorPopup:Landroid/app/AlertDialog;

    .line 780
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 782
    .end local v0    # "dialog":Landroid/app/AlertDialog$Builder;
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/app/camera/OISTest;->mchkopencamera:Z

    .line 783
    return-void
.end method

.method private dialogNGPopup(I)V
    .locals 5
    .param p1, "messageId"    # I

    .prologue
    const/4 v4, 0x0

    .line 1142
    sget-object v1, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v2, "OISTest.dialogNGPopup"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1144
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1145
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 1148
    :cond_0
    sget-object v1, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OISTest.dialogNGPopup mchkopencamera : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/camera/OISTest;->mchkopencamera:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1149
    iget-boolean v1, p0, Lcom/sec/android/app/camera/OISTest;->mchkopencamera:Z

    if-nez v1, :cond_1

    .line 1150
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1151
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a000d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1152
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1154
    const v1, 0x7f0a0001

    new-instance v2, Lcom/sec/android/app/camera/OISTest$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camera/OISTest$7;-><init>(Lcom/sec/android/app/camera/OISTest;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1159
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1160
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/OISTest;->mErrorPopup:Landroid/app/AlertDialog;

    .line 1161
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1163
    .end local v0    # "dialog":Landroid/app/AlertDialog$Builder;
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/app/camera/OISTest;->mchkopencamera:Z

    .line 1164
    return-void
.end method

.method private finishOISTest(I)V
    .locals 5
    .param p1, "result"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 801
    sget-object v1, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v2, "finishOISTest()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    invoke-virtual {p0}, Lcom/sec/android/app/camera/OISTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 803
    .local v0, "intent":Landroid/content/Intent;
    if-nez p1, :cond_0

    .line 804
    const-string v1, "OIS FAIL"

    iget v2, p0, Lcom/sec/android/app/camera/OISTest;->mCheckingState:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 811
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/camera/OISTest;->setResult(ILandroid/content/Intent;)V

    .line 812
    invoke-virtual {p0}, Lcom/sec/android/app/camera/OISTest;->finish()V

    .line 814
    return-void

    .line 806
    :cond_0
    const-string v1, "camera_id"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 807
    const-string v1, "ommision_test"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 808
    const-string v1, "torch_on"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 809
    const-string v1, "ois_test"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static isSupported(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2
    .param p0, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "supported":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 644
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setSystemKeyBlock(Landroid/content/ComponentName;I)V
    .locals 4
    .param p0, "componentName"    # Landroid/content/ComponentName;
    .param p1, "keyCode"    # I

    .prologue
    .line 993
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 996
    .local v1, "wm":Landroid/view/IWindowManager;
    const/4 v2, 0x1

    :try_start_0
    invoke-interface {v1, p1, p0, v2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1000
    :goto_0
    return-void

    .line 997
    :catch_0
    move-exception v0

    .line 998
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v3, "setSystemKeyBlock exception!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private stopPreview()V
    .locals 2

    .prologue
    .line 868
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/camera/OISTest;->mPreviewing:Z

    if-eqz v0, :cond_0

    .line 870
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 871
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 874
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/OISTest;->mPreviewing:Z

    .line 875
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "stopPreview : mPreviewing set false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    return-void
.end method


# virtual methods
.method public acquireDVFS(I)V
    .locals 5
    .param p1, "milisecond"    # I

    .prologue
    const/4 v4, 0x0

    .line 1003
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-nez v0, :cond_0

    .line 1004
    new-instance v0, Landroid/os/DVFSHelper;

    const/16 v1, 0xc

    invoke-direct {v0, p0, v1}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 1005
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->supportedCPUFreqTable:[I

    .line 1006
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->supportedCPUCoreTable:[I

    .line 1008
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->supportedCPUFreqTable:[I

    if-eqz v0, :cond_2

    .line 1009
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lcom/sec/android/app/camera/OISTest;->supportedCPUFreqTable:[I

    aget v2, v2, v4

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 1010
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "supportedCPUFreqTable is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/OISTest;->supportedCPUFreqTable:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->supportedCPUCoreTable:[I

    if-eqz v0, :cond_3

    .line 1016
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    iget-object v2, p0, Lcom/sec/android/app/camera/OISTest;->supportedCPUCoreTable:[I

    aget v2, v2, v4

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 1017
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "supportedCPUCoreTable is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/OISTest;->supportedCPUCoreTable:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1023
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    .line 1024
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0, p1}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 1025
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDVFSHelper.acquire : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1027
    :cond_1
    return-void

    .line 1012
    :cond_2
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "supportedCPUFreqTable is null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1019
    :cond_3
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "supportedCPUFreqTable is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected findBestFpsRange(Landroid/hardware/Camera$Parameters;II)[I
    .locals 10
    .param p1, "parameters"    # Landroid/hardware/Camera$Parameters;
    .param p2, "requestedMinFps"    # I
    .param p3, "requestedMaxFps"    # I

    .prologue
    .line 277
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Requsted fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    const/4 v1, 0x0

    .line 280
    .local v1, "MIN_IDX":I
    const/4 v0, 0x1

    .line 281
    .local v0, "MAX_IDX":I
    const/4 v7, 0x2

    new-array v3, v7, [I

    .line 282
    .local v3, "fpsRange":[I
    const/4 v7, 0x2

    new-array v2, v7, [I

    .line 284
    .local v2, "bestFpsRange":[I
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v4

    .line 286
    .local v4, "fpsRangeList":Ljava/util/List;, "Ljava/util/List<[I>;"
    if-nez v4, :cond_0

    .line 287
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v8, "supported preview fps range is null"

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    const/4 v3, 0x0

    .line 345
    .end local v3    # "fpsRange":[I
    :goto_0
    return-object v3

    .line 291
    .restart local v3    # "fpsRange":[I
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v5, v7, -0x1

    .local v5, "i":I
    :goto_1
    if-ltz v5, :cond_8

    .line 292
    const/4 v8, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    const/4 v9, 0x0

    aget v7, v7, v9

    aput v7, v3, v8

    .line 293
    const/4 v8, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    const/4 v9, 0x1

    aget v7, v7, v9

    aput v7, v3, v8

    .line 295
    const/4 v7, 0x1

    aget v7, v3, v7

    if-ne p3, v7, :cond_6

    .line 296
    const/4 v7, 0x0

    aget v7, v3, v7

    if-ne p2, v7, :cond_1

    .line 298
    move-object v2, v3

    .line 299
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 302
    :cond_1
    if-nez v5, :cond_2

    .line 304
    move-object v2, v3

    .line 305
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 310
    :cond_2
    move v6, v5

    .local v6, "j":I
    :goto_2
    if-ltz v6, :cond_5

    .line 311
    const/4 v8, 0x0

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    const/4 v9, 0x0

    aget v7, v7, v9

    aput v7, v3, v8

    .line 312
    const/4 v8, 0x1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    const/4 v9, 0x1

    aget v7, v7, v9

    aput v7, v3, v8

    .line 314
    const/4 v7, 0x0

    aget v7, v3, v7

    if-ne p2, v7, :cond_3

    .line 316
    move-object v2, v3

    .line 317
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 320
    :cond_3
    const/4 v7, 0x0

    aget v7, v3, v7

    if-le p2, v7, :cond_4

    .line 321
    move-object v2, v3

    .line 322
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 310
    :cond_4
    add-int/lit8 v6, v6, -0x1

    goto :goto_2

    .line 329
    :cond_5
    move-object v2, v3

    .line 330
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 334
    .end local v6    # "j":I
    :cond_6
    const/4 v7, 0x1

    aget v7, v3, v7

    if-le p3, v7, :cond_7

    .line 335
    move-object v2, v3

    .line 336
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 291
    :cond_7
    add-int/lit8 v5, v5, -0x1

    goto/16 :goto_1

    .line 343
    :cond_8
    move-object v2, v3

    .line 344
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public findOptimalPreviewSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;
    .locals 16
    .param p2, "targetRatio"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;D)",
            "Landroid/hardware/Camera$Size;"
        }
    .end annotation

    .prologue
    .line 1031
    .local p1, "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    const-wide v0, 0x3f50624dd2f1a9fcL    # 0.001

    .line 1032
    .local v0, "ASPECT_TOLERANCE":D
    if-nez p1, :cond_1

    .line 1033
    const/4 v6, 0x0

    .line 1080
    :cond_0
    return-object v6

    .line 1035
    :cond_1
    const/4 v6, 0x0

    .line 1036
    .local v6, "optimalSize":Landroid/hardware/Camera$Size;
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 1044
    .local v4, "minDiff":D
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/OISTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "window"

    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/WindowManager;

    invoke-interface {v11}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 1047
    .local v2, "display":Landroid/view/Display;
    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v11

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v12

    invoke-static {v11, v12}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 1049
    .local v10, "targetHeight":I
    sget-object v11, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "display.getHeight() = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " display.getWidth() = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    if-gtz v10, :cond_2

    .line 1054
    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v10

    .line 1058
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/hardware/Camera$Size;

    .line 1059
    .local v7, "size":Landroid/hardware/Camera$Size;
    iget v11, v7, Landroid/hardware/Camera$Size;->width:I

    int-to-double v12, v11

    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    int-to-double v14, v11

    div-double v8, v12, v14

    .line 1060
    .local v8, "ratio":D
    sub-double v12, v8, p2

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    const-wide v14, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v11, v12, v14

    if-gtz v11, :cond_3

    .line 1062
    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v12, v11

    cmpg-double v11, v12, v4

    if-gez v11, :cond_3

    .line 1063
    move-object v6, v7

    .line 1064
    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v4, v11

    goto :goto_0

    .line 1070
    .end local v7    # "size":Landroid/hardware/Camera$Size;
    .end local v8    # "ratio":D
    :cond_4
    if-nez v6, :cond_0

    .line 1071
    sget-object v11, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v12, "No preview size match the aspect ratio"

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1072
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 1073
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/hardware/Camera$Size;

    .line 1074
    .restart local v7    # "size":Landroid/hardware/Camera$Size;
    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v12, v11

    cmpg-double v11, v12, v4

    if-gez v11, :cond_5

    .line 1075
    move-object v6, v7

    .line 1076
    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v4, v11

    goto :goto_1
.end method

.method public getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mCameraSettings:Lcom/sec/android/app/camera/framework/CameraSettings;

    return-object v0
.end method

.method public isCamcorderFirmwareGood()Z
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 1088
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v8, "isCamcorderFirmwareGood..."

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1089
    const/4 v5, 0x0

    .line 1090
    .local v5, "mFWInfo":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1091
    .local v3, "fr":Ljava/io/FileReader;
    const/4 v0, 0x0

    .line 1094
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    const-string v6, "/sys/class/camera/rear/rear_camfw"

    .line 1095
    .local v6, "sysFsPath":Ljava/lang/String;
    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1096
    .end local v3    # "fr":Ljava/io/FileReader;
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1097
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_2
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OISTest.isCamcorderFirmwareGood() fr : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1098
    if-eqz v4, :cond_0

    if-eqz v1, :cond_0

    .line 1099
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OISTest.isCamcorderFirmwareGood() br : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1100
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 1101
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OISTest.isCamcorderFirmwareGood - FW info["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    :cond_0
    if-eqz v4, :cond_1

    .line 1105
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 1108
    :cond_1
    if-eqz v1, :cond_2

    .line 1109
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1115
    :cond_2
    if-eqz v4, :cond_3

    .line 1116
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 1119
    :cond_3
    if-eqz v1, :cond_4

    .line 1120
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_4
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 1128
    .end local v4    # "fr":Ljava/io/FileReader;
    .end local v6    # "sysFsPath":Ljava/lang/String;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :cond_5
    :goto_0
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OISTest.isCamcorderFirmwareGood() mFWInfo : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1129
    if-eqz v5, :cond_6

    .line 1130
    const-string v7, "NG_"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1131
    const v7, 0x7f0a0024

    invoke-direct {p0, v7}, Lcom/sec/android/app/camera/OISTest;->dialogNGPopup(I)V

    .line 1138
    :cond_6
    :goto_1
    return v10

    .line 1122
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    .restart local v6    # "sysFsPath":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 1123
    .local v2, "e":Ljava/io/IOException;
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v8, "OISTest.isCamcorderFirmwareGood() exception!!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 1126
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_0

    .line 1111
    .end local v2    # "e":Ljava/io/IOException;
    .end local v6    # "sysFsPath":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 1112
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v8, "OISTest.isCamcorderFirmwareGood Read FW info fail"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1115
    if-eqz v3, :cond_7

    .line 1116
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 1119
    :cond_7
    if-eqz v0, :cond_5

    .line 1120
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 1122
    :catch_2
    move-exception v2

    .line 1123
    .local v2, "e":Ljava/io/IOException;
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v8, "OISTest.isCamcorderFirmwareGood() exception!!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1114
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 1115
    :goto_3
    if-eqz v3, :cond_8

    .line 1116
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 1119
    :cond_8
    if-eqz v0, :cond_9

    .line 1120
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 1125
    :cond_9
    :goto_4
    throw v7

    .line 1122
    :catch_3
    move-exception v2

    .line 1123
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v8, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v9, "OISTest.isCamcorderFirmwareGood() exception!!"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 1133
    .end local v2    # "e":Ljava/io/IOException;
    :cond_a
    sget-object v7, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v8, "OISTest.isCamcorderFirmwareGood() Ok."

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1114
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    .restart local v6    # "sysFsPath":Ljava/lang/String;
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .line 1111
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_5
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_2
.end method

.method public isSupportedFocusModes(Ljava/lang/String;Ljava/util/List;)Z
    .locals 1
    .param p1, "checkfocusmode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 654
    .local p2, "focusmodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1, p2}, Lcom/sec/android/app/camera/OISTest;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 655
    const/4 v0, 0x1

    .line 658
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    .line 350
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 351
    sget-object v3, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v4, "onCreate"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    invoke-static {}, Lcom/sec/android/app/camera/CameraApp;->getAppFeature()Lcom/sec/android/app/camera/Feature;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->Feature:Lcom/sec/android/app/camera/Feature;

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/camera/OISTest;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/sec/android/app/camera/OISTest;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/app/camera/OISTest;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    const/16 v4, 0x1a

    invoke-static {v3, v4}, Lcom/sec/android/app/camera/OISTest;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/camera/OISTest;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    const/16 v4, 0xbb

    invoke-static {v3, v4}, Lcom/sec/android/app/camera/OISTest;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 359
    invoke-virtual {p0}, Lcom/sec/android/app/camera/OISTest;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "camera_id"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/camera/OISTest;->cameraType:I

    .line 366
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-object v3, v3, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    array-length v3, v3

    const/4 v4, 0x2

    filled-new-array {v3, v4}, [I

    move-result-object v3

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[I

    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->camcorderpreview:[[I

    .line 367
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-object v3, v3, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->camcorderpreview:[[I

    .line 368
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/app/camera/OISTest;->mCurResolution:I

    .line 370
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->CAMERA_BOOST_ENABLE:Z

    if-eqz v3, :cond_0

    .line 371
    const/16 v3, 0x7d0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/OISTest;->acquireDVFS(I)V

    .line 374
    :cond_0
    sget-object v3, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v4, "onCreate"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    new-instance v1, Ljava/lang/Thread;

    new-instance v3, Lcom/sec/android/app/camera/OISTest$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/camera/OISTest$1;-><init>(Lcom/sec/android/app/camera/OISTest;)V

    invoke-direct {v1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 414
    .local v1, "openCameraThread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 416
    invoke-virtual {p0}, Lcom/sec/android/app/camera/OISTest;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 417
    .local v2, "win":Landroid/view/Window;
    const v3, 0x680080

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 418
    const v3, 0x7f030008

    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/OISTest;->setContentView(I)V

    .line 420
    const v3, 0x7f09000e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/OISTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mHelpText:Landroid/widget/TextView;

    .line 421
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mHelpText:Landroid/widget/TextView;

    const-string v4, "x test"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 424
    const v3, 0x7f09000b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/OISTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/SurfaceView;

    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mSurfaceView:Landroid/view/SurfaceView;

    .line 425
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 426
    .local v0, "holder":Landroid/view/SurfaceHolder;
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 427
    invoke-interface {v0, v6}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 429
    const v3, 0x7f090049

    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/OISTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mPassButton:Landroid/widget/Button;

    .line 430
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mPassButton:Landroid/widget/Button;

    const-string v4, "PASS"

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 431
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mPassButton:Landroid/widget/Button;

    new-instance v4, Lcom/sec/android/app/camera/OISTest$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/camera/OISTest$2;-><init>(Lcom/sec/android/app/camera/OISTest;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 470
    const v3, 0x7f09004a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/OISTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mFailButton:Landroid/widget/Button;

    .line 471
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mFailButton:Landroid/widget/Button;

    const-string v4, "FAIL"

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 472
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mFailButton:Landroid/widget/Button;

    new-instance v4, Lcom/sec/android/app/camera/OISTest$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/camera/OISTest$3;-><init>(Lcom/sec/android/app/camera/OISTest;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 484
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mPassButton:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 485
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mFailButton:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 488
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 491
    :goto_0
    return-void

    .line 489
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public onError(Landroid/media/MediaRecorder;II)V
    .locals 3
    .param p1, "mr"    # Landroid/media/MediaRecorder;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 194
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaRecorder onError = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const v0, 0x7f0a000b

    invoke-direct {p0, v0}, Lcom/sec/android/app/camera/OISTest;->dialogErrorPopup(I)V

    .line 197
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 11
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v10, 0x0

    const/4 v3, 0x1

    .line 817
    sget-object v4, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onKeyDown keyCode = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    const/4 v1, 0x0

    .line 820
    .local v1, "mKeyString":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 852
    sget-object v4, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v5, "OISTest.onKeyDown() default return true"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    const/16 v4, 0x1a

    if-ne p1, v4, :cond_0

    .line 854
    const-string v1, "Hold"

    .line 856
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mKeyFailToast:Landroid/widget/Toast;

    if-nez v4, :cond_0

    .line 857
    const v4, 0x7f0a00a4

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v10

    invoke-virtual {p0, v4, v3}, Lcom/sec/android/app/camera/OISTest;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mKeyFailToast:Landroid/widget/Toast;

    .line 858
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mKeyFailToast:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 864
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    :goto_1
    :sswitch_0
    return v3

    .line 834
    :sswitch_1
    iget-boolean v4, p0, Lcom/sec/android/app/camera/OISTest;->mIsPressedBackkey:Z

    if-nez v4, :cond_1

    .line 835
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 836
    .local v2, "rightNow":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/camera/OISTest;->mCurrentTime:J

    .line 837
    iput-boolean v3, p0, Lcom/sec/android/app/camera/OISTest;->mIsPressedBackkey:Z

    .line 838
    invoke-virtual {p0}, Lcom/sec/android/app/camera/OISTest;->startTimer()V

    goto :goto_1

    .line 841
    .end local v2    # "rightNow":Ljava/util/Calendar;
    :cond_1
    iput-boolean v10, p0, Lcom/sec/android/app/camera/OISTest;->mIsPressedBackkey:Z

    .line 842
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 844
    .restart local v2    # "rightNow":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/android/app/camera/OISTest;->mCurrentTime:J

    const-wide/16 v8, 0x7d0

    add-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    .line 845
    invoke-virtual {p0}, Lcom/sec/android/app/camera/OISTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 846
    .local v0, "intents":Landroid/content/Intent;
    const-string v4, "ois_test_backkey_canceled"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 847
    invoke-virtual {p0, v10, v0}, Lcom/sec/android/app/camera/OISTest;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 820
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 902
    sget-object v1, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKeyUp keyCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/camera/OISTest;->mKeyFailToast:Landroid/widget/Toast;

    .line 905
    sparse-switch p1, :sswitch_data_0

    .line 917
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_0
    :goto_0
    :sswitch_0
    return v0

    .line 907
    :sswitch_1
    sget-object v1, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KEYCODE_VOLUME_DOWN isPreviewStarted : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/camera/OISTest;->isPreviewStarted:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 908
    iget-boolean v1, p0, Lcom/sec/android/app/camera/OISTest;->isPreviewStarted:Z

    if-nez v1, :cond_0

    goto :goto_0

    .line 905
    nop

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_1
        0xbb -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 664
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 667
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 668
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "releaseWakelock"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 671
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/camera/OISTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 674
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/OISTest;->mPausing:Z

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_2

    .line 681
    invoke-direct {p0}, Lcom/sec/android/app/camera/OISTest;->stopPreview()V

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 683
    iput-object v2, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    .line 686
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_3

    .line 687
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 691
    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/OISTest;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 696
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/camera/OISTest;->mStopCamera:Z

    if-eqz v0, :cond_4

    .line 698
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/OISTest;->mStopCamera:Z

    .line 700
    :cond_4
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 701
    return-void

    .line 692
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/16 v7, 0x9

    const/4 v6, 0x0

    .line 501
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 502
    sget-object v4, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v5, "[WRG] onResume()"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    iput-boolean v6, p0, Lcom/sec/android/app/camera/OISTest;->mStopCamera:Z

    .line 508
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/android/app/camera/OISTest;->mCheckingState:I

    .line 510
    const-string v4, "power"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/OISTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    .line 511
    .local v3, "pm":Landroid/os/PowerManager;
    const v4, 0x3000001a

    sget-object v5, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 513
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 515
    iput-boolean v6, p0, Lcom/sec/android/app/camera/OISTest;->mPausing:Z

    .line 518
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 519
    .local v2, "intentFilter":Landroid/content/IntentFilter;
    const-string v4, "com.android.samsungtest.CameraStop"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 520
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v2}, Lcom/sec/android/app/camera/OISTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 526
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->camcorderpreview:[[I

    if-nez v4, :cond_0

    .line 527
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-object v4, v4, Lcom/sec/android/app/camera/Feature;->OIS_TEST_RESOLUTION:[[I

    iput-object v4, p0, Lcom/sec/android/app/camera/OISTest;->camcorderpreview:[[I

    .line 531
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    if-nez v4, :cond_1

    .line 533
    :try_start_0
    iget v4, p0, Lcom/sec/android/app/camera/OISTest;->cameraType:I

    invoke-static {v4}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 539
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    const/16 v5, 0x5a

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 540
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 544
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v5, p0, Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 552
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v5, p0, Lcom/sec/android/app/camera/OISTest;->mErrorCallback:Lcom/sec/android/app/camera/OISTest$ErrorCallback;

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    .line 555
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v4, :cond_2

    .line 557
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v5, p0, Lcom/sec/android/app/camera/OISTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 572
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 573
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v5, "focus-mode"

    const-string v6, "continuous-picture"

    invoke-virtual {v4, v5, v6}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v5, "ois"

    const-string v6, "sine_x"

    invoke-virtual {v4, v5, v6}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v5, p0, Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3

    .line 638
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v5, p0, Lcom/sec/android/app/camera/OISTest;->mPreviewCallback:Lcom/sec/android/app/camera/OISTest$PreviewCallback;

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 640
    :cond_3
    :goto_2
    return-void

    .line 534
    :catch_0
    move-exception v0

    .line 535
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v5, "service cannot connect. critical exception occured."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    const v4, 0x7f0a000a

    invoke-direct {p0, v4}, Lcom/sec/android/app/camera/OISTest;->dialogErrorPopup(I)V

    goto :goto_2

    .line 547
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 548
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v4, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v5, "setParameter fail"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    .line 558
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_2
    move-exception v1

    .line 559
    .local v1, "exception":Ljava/io/IOException;
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v4, :cond_3

    .line 560
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->release()V

    .line 561
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    goto :goto_2

    .line 579
    .end local v1    # "exception":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 580
    .restart local v0    # "e":Ljava/lang/RuntimeException;
    sget-object v4, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v5, "setParameter fail"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    .line 577
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_4
    move-exception v4

    goto :goto_1

    .line 545
    :catch_5
    move-exception v4

    goto :goto_0
.end method

.method public setVideoSize(Landroid/hardware/Camera$Parameters;II)V
    .locals 3
    .param p1, "p"    # Landroid/hardware/Camera$Parameters;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 1083
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1084
    .local v0, "v":Ljava/lang/String;
    const-string v1, "video-size"

    invoke-virtual {p1, v1, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    return-void
.end method

.method protected startTimer()V
    .locals 4

    .prologue
    .line 897
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mTimerHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 899
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 9
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    const v8, 0x7f0a000a

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 932
    sget-object v3, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v4, "surfaceChanged"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    if-ge p3, p4, :cond_0

    .line 935
    move v2, p3

    .line 936
    .local v2, "tmp_swp":I
    move p3, p4

    .line 937
    move p4, v2

    .line 939
    .end local v2    # "tmp_swp":I
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3, v6}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 941
    iget-boolean v3, p0, Lcom/sec/android/app/camera/OISTest;->mPausing:Z

    if-eqz v3, :cond_2

    .line 942
    sget-object v3, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v4, "surfaceChanged - mPausing == true, return"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    :cond_1
    :goto_0
    return-void

    .line 946
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v3, :cond_3

    .line 947
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v3}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 948
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->camcorderpreview:[[I

    iget v5, p0, Lcom/sec/android/app/camera/OISTest;->mCurResolution:I

    aget-object v4, v4, v5

    aget v4, v4, v6

    iget-object v5, p0, Lcom/sec/android/app/camera/OISTest;->camcorderpreview:[[I

    iget v6, p0, Lcom/sec/android/app/camera/OISTest;->mCurResolution:I

    aget-object v5, v5, v6

    aget v5, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 951
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3, v4}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 961
    :goto_1
    :try_start_1
    sget-object v3, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[WRG] surfaceChanged - mSurfaceHolder = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camera/OISTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 962
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v4, p0, Lcom/sec/android/app/camera/OISTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v3, v4}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 972
    :try_start_2
    sget-object v3, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v4, "[WRG] surfaceChanged startPreview"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 973
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/camera/OISTest;->isPreviewStarted:Z

    .line 974
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v3}, Landroid/hardware/Camera;->startPreview()V

    .line 975
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/app/camera/OISTest;->mCheckingState:I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 982
    iput-boolean v7, p0, Lcom/sec/android/app/camera/OISTest;->mPreviewing:Z

    goto :goto_0

    .line 954
    :catch_0
    move-exception v0

    .line 955
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v3, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v4, "setParameter fail"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mMainHandler:Landroid/os/Handler;

    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 963
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v1

    .line 964
    .local v1, "exception":Ljava/io/IOException;
    sget-object v3, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v4, "mCameraDevice.setPreviewDisplay exception!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v3, :cond_1

    .line 966
    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v3}, Landroid/hardware/Camera;->release()V

    .line 967
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;

    goto/16 :goto_0

    .line 977
    .end local v1    # "exception":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 978
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v3, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v4, "exception while startPreview"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 979
    invoke-direct {p0, v8}, Lcom/sec/android/app/camera/OISTest;->dialogErrorPopup(I)V

    goto/16 :goto_0

    .line 986
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_3
    sget-object v3, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v4, "surfaceChanged - mCameraDevice NULL! "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    invoke-direct {p0, v8}, Lcom/sec/android/app/camera/OISTest;->dialogErrorPopup(I)V

    goto/16 :goto_0

    .line 952
    :catch_3
    move-exception v3

    goto :goto_1
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 921
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "surfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 922
    iput-object p1, p0, Lcom/sec/android/app/camera/OISTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 923
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 926
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "surfaceDestroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 927
    invoke-direct {p0}, Lcom/sec/android/app/camera/OISTest;->stopPreview()V

    .line 928
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/OISTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 929
    return-void
.end method
