.class public Lcom/sec/android/app/camera/Camera;
.super Landroid/app/Activity;
.source "Camera.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/sec/android/app/camera/framework/ShutterButton$OnShutterButtonListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camera/Camera$FailDialogFragment;,
        Lcom/sec/android/app/camera/Camera$ImageCapture;,
        Lcom/sec/android/app/camera/Camera$AutoFocusCallback;,
        Lcom/sec/android/app/camera/Camera$PreviewCallback;,
        Lcom/sec/android/app/camera/Camera$ErrorCallback;,
        Lcom/sec/android/app/camera/Camera$JpegPictureCallback;,
        Lcom/sec/android/app/camera/Camera$RawPictureCallback;,
        Lcom/sec/android/app/camera/Camera$ShutterCallback;,
        Lcom/sec/android/app/camera/Camera$MainHandler;
    }
.end annotation


# static fields
.field public static final CAMERA_FAIL:Ljava/lang/String; = "CAMERA FAIL"

.field protected static final CAMERA_FIRMWARE_CHECK_FILE_REAR:Ljava/lang/String; = "/sys/class/camera/rear/rear_checkfw_factory"

.field protected static final CAMERA_FIRMWARE_CHECK_LATEST_MODULE_REAR:Ljava/lang/String; = "/sys/class/camera/rear/rear_latest_module_check"

.field protected static final CAMERA_FIRMWARE_INFO_FILE_REAR:Ljava/lang/String; = "/sys/class/camera/rear/rear_camfw"

.field protected static final CAMERA_LOADED_FW_INFO:Ljava/lang/String; = "/sys/class/camera/rear/rear_camfw_load"

.field private static final DIRECTORY_CAMERA_AUTO_TEST:Ljava/lang/String;

.field private static final DIRECTORY_CAMERA_DATA:Ljava/lang/String;

.field private static final DIRECTORY_CAMERA_LOW_LIGHT_CAPTURE_TEST:Ljava/lang/String;

.field private static final DIRECTORY_CAMERA_SDCARD:Ljava/lang/String;

.field public static final FRONT_CAMERA:Ljava/lang/String; = "FRONT CAMERA"

.field public static final FRONT_CAMERA_VIDEO:Ljava/lang/String; = "FRONT CAMERA VIDEO PLAYBACK"

.field public static final MENU_KEY_EVENT_TIMEOUT:J = 0x7d0L

.field public static final OIS_FAIL:Ljava/lang/String; = "OIS FAIL"

.field public static final PREPARING:J = -0x2L

.field public static final REAR_CAMERA:Ljava/lang/String; = "REAR CAMERA"

.field private static final TAG:Ljava/lang/String; = "FactoryCamera"

.field private static final TEST_15_MODE:Ljava/lang/String; = "15 Test"

.field public static final UNAVAILABLE:J = -0x1L

.field public static final UNKNOWN_SIZE:J = -0x3L

.field private static lasttouchtime:J

.field private static mContentResolver:Landroid/content/ContentResolver;


# instance fields
.field private final FINISH_CAMCORDER_PREVIEW_TEST:I

.field private final FINISH_LOW_LIGHT_CAPTURE_DISPLAY:I

.field private final FINISH_OIS_TEST:I

.field private final FINISH_POSTVIEW_TEST:I

.field public Feature:Lcom/sec/android/app/camera/Feature;

.field private final REQUEST_CAMCORDER_PREVIEW_TEST:I

.field private final REQUEST_LOW_LIGHT_CAPTURE_DISPLAY:I

.field private final REQUEST_OIS_TEST:I

.field private final REQUEST_POSTVIEW_TEST:I

.field private bCheckDTP:Z

.field private bDoneDTP:Z

.field private bEnablePreviewCb:Z

.field private bIsCamcorderPreviewFail:Z

.field private bIsOISFail:Z

.field private bIsUsbOrUartCommand:Z

.field private bSentAck:Z

.field private bUseSdcard:Z

.field private cameraType:I

.field public capturedData:[B

.field private cs:Lcom/sec/android/app/camera/CameraStorage;

.field private filePath:Ljava/lang/String;

.field private mAF_Fail_Count:I

.field private mAutoFocusCallback:Lcom/sec/android/app/camera/Camera$AutoFocusCallback;

.field private mCameraAutoCapture:Z

.field private mCameraDevice:Landroid/hardware/Camera;

.field protected mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

.field private mCameraSettings:Lcom/sec/android/app/camera/framework/CameraSettings;

.field private mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

.field private mCaptureStartTime:J

.field private mCurrentTime:J

.field private mDVFSHelper:Landroid/os/DVFSHelper;

.field mDatalineCheck:Ljava/lang/Runnable;

.field private mErrorCallback:Lcom/sec/android/app/camera/Camera$ErrorCallback;

.field private mErrorPopup:Landroid/app/AlertDialog;

.field private mFinishCamcorderPreviewTest:Z

.field private mFinishCamcorderPreviewTestByBackKey:Z

.field private mFinishOISTest:Z

.field private mFinishOISTestByBackKey:Z

.field private mFinishPostViewTest:Z

.field private mFlashEnable:I

.field private mFocusBlinkAnimation:Landroid/view/animation/Animation;

.field private mFocusCallbackTime:J

.field private mFocusRectangle:Lcom/sec/android/app/camera/framework/FocusRectangle;

.field private mFocusStartTime:J

.field private mFocusState:I

.field private mFocusToneGenerator:Landroid/media/ToneGenerator;

.field private mImageCapture:Lcom/sec/android/app/camera/Camera$ImageCapture;

.field private mImageIndexCount:I

.field private mIsCaptureEnable:Z

.field private mIsOrientationReverse:Z

.field private mIsPressedBackkey:Z

.field private mJpegPictureCallbackTime:J

.field private mLastOrientation:I

.field private mLowLightCaptureTestSkip:Z

.field private mLowLightCaptureText:Landroid/widget/TextView;

.field private mMainHandler:Landroid/os/Handler;

.field private mMovieIndexCount:I

.field protected mNumberOfCameras:I

.field protected mOpenCameraThread:Ljava/lang/Thread;

.field protected mOrientationListener:Landroid/view/OrientationEventListener;

.field private mOrientationOnTake:I

.field private mOriginalViewFinderHeight:I

.field private mOriginalViewFinderWidth:I

.field private mParameters:Landroid/hardware/Camera$Parameters;

.field private mPausing:Z

.field private mPreviewCallback:Lcom/sec/android/app/camera/Camera$PreviewCallback;

.field private mPreviewing:Z

.field private mRawPictureCallback:Lcom/sec/android/app/camera/Camera$RawPictureCallback;

.field private mRawPictureCallbackTime:J

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mShutterBtnlock:Z

.field private mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

.field private mShutterButtonLayout:Landroid/widget/RelativeLayout;

.field private mShutterCallback:Lcom/sec/android/app/camera/Camera$ShutterCallback;

.field private mShutterCallbackTime:J

.field mStartCheck:Ljava/lang/Runnable;

.field private mStatus:I

.field private mStopCamera:Z

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceView:Lcom/sec/android/app/camera/VideoPreview;

.field protected mTimerHandler:Landroid/os/Handler;

.field private mViewFinderHeight:I

.field private mViewFinderWidth:I

.field protected mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWindowManager:Landroid/view/IWindowManager;

.field private mZoomValue:I

.field private mchkopencamera:Z

.field private ommisionTest:Z

.field private supportedCPUCoreTable:[I

.field private supportedCPUFreqTable:[I

.field private testType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 120
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/camera/Camera;->mContentResolver:Landroid/content/ContentResolver;

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/log/DCIM/Camera"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_DATA:Ljava/lang/String;

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Camera"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_SDCARD:Ljava/lang/String;

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/CameraLowLightTest"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_LOW_LIGHT_CAPTURE_TEST:Ljava/lang/String;

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/fImage"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_AUTO_TEST:Ljava/lang/String;

    .line 188
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/sec/android/app/camera/Camera;->lasttouchtime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 94
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 100
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->bEnablePreviewCb:Z

    .line 101
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->bUseSdcard:Z

    .line 102
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->bDoneDTP:Z

    .line 103
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->bCheckDTP:Z

    .line 118
    iput-object v3, p0, Lcom/sec/android/app/camera/Camera;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 121
    iput-object v3, p0, Lcom/sec/android/app/camera/Camera;->filePath:Ljava/lang/String;

    .line 129
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureTestSkip:Z

    .line 140
    new-instance v0, Lcom/sec/android/app/camera/Camera$ShutterCallback;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/camera/Camera$ShutterCallback;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/Camera$1;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mShutterCallback:Lcom/sec/android/app/camera/Camera$ShutterCallback;

    .line 141
    new-instance v0, Lcom/sec/android/app/camera/Camera$RawPictureCallback;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/camera/Camera$RawPictureCallback;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/Camera$1;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mRawPictureCallback:Lcom/sec/android/app/camera/Camera$RawPictureCallback;

    .line 142
    new-instance v0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/Camera$1;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mAutoFocusCallback:Lcom/sec/android/app/camera/Camera$AutoFocusCallback;

    .line 143
    new-instance v0, Lcom/sec/android/app/camera/Camera$ErrorCallback;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/Camera$ErrorCallback;-><init>(Lcom/sec/android/app/camera/Camera;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mErrorCallback:Lcom/sec/android/app/camera/Camera$ErrorCallback;

    .line 144
    new-instance v0, Lcom/sec/android/app/camera/Camera$PreviewCallback;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/Camera$PreviewCallback;-><init>(Lcom/sec/android/app/camera/Camera;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mPreviewCallback:Lcom/sec/android/app/camera/Camera$PreviewCallback;

    .line 154
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mPausing:Z

    .line 155
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->bSentAck:Z

    .line 156
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mStopCamera:Z

    .line 158
    iput-object v3, p0, Lcom/sec/android/app/camera/Camera;->mImageCapture:Lcom/sec/android/app/camera/Camera$ImageCapture;

    .line 165
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    .line 166
    iput v2, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    .line 167
    iput v4, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    .line 168
    iput v2, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    .line 169
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->ommisionTest:Z

    .line 170
    iput v5, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    .line 171
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTest:Z

    .line 172
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mFinishOISTest:Z

    .line 174
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mFinishPostViewTest:Z

    .line 175
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTestByBackKey:Z

    .line 176
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mFinishOISTestByBackKey:Z

    .line 179
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    .line 180
    iput-object v3, p0, Lcom/sec/android/app/camera/Camera;->mOpenCameraThread:Ljava/lang/Thread;

    .line 181
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mIsOrientationReverse:Z

    .line 184
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mCameraAutoCapture:Z

    .line 187
    iput-object v3, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    .line 200
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mIsPressedBackkey:Z

    .line 201
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mIsCaptureEnable:Z

    .line 202
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/camera/Camera;->mCurrentTime:J

    .line 205
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mShutterBtnlock:Z

    .line 208
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mchkopencamera:Z

    .line 211
    iput-object v3, p0, Lcom/sec/android/app/camera/Camera;->mWindowManager:Landroid/view/IWindowManager;

    .line 214
    iput v2, p0, Lcom/sec/android/app/camera/Camera;->mAF_Fail_Count:I

    .line 217
    iput v4, p0, Lcom/sec/android/app/camera/Camera;->REQUEST_CAMCORDER_PREVIEW_TEST:I

    .line 218
    iput v4, p0, Lcom/sec/android/app/camera/Camera;->FINISH_CAMCORDER_PREVIEW_TEST:I

    .line 221
    iput v6, p0, Lcom/sec/android/app/camera/Camera;->REQUEST_POSTVIEW_TEST:I

    .line 222
    iput v6, p0, Lcom/sec/android/app/camera/Camera;->FINISH_POSTVIEW_TEST:I

    .line 225
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/camera/Camera;->REQUEST_LOW_LIGHT_CAPTURE_DISPLAY:I

    .line 226
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/camera/Camera;->FINISH_LOW_LIGHT_CAPTURE_DISPLAY:I

    .line 229
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/camera/Camera;->REQUEST_OIS_TEST:I

    .line 230
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/camera/Camera;->FINISH_OIS_TEST:I

    .line 246
    iput-object v3, p0, Lcom/sec/android/app/camera/Camera;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 247
    iput v5, p0, Lcom/sec/android/app/camera/Camera;->mLastOrientation:I

    .line 248
    iput v5, p0, Lcom/sec/android/app/camera/Camera;->mOrientationOnTake:I

    .line 251
    new-instance v0, Lcom/sec/android/app/camera/Camera$MainHandler;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/camera/Camera$MainHandler;-><init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/Camera$1;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    .line 253
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->bIsCamcorderPreviewFail:Z

    .line 254
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->bIsOISFail:Z

    .line 257
    iput-object v3, p0, Lcom/sec/android/app/camera/Camera;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 262
    iput v2, p0, Lcom/sec/android/app/camera/Camera;->mImageIndexCount:I

    .line 263
    iput v2, p0, Lcom/sec/android/app/camera/Camera;->mMovieIndexCount:I

    .line 264
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    .line 363
    new-instance v0, Lcom/sec/android/app/camera/Camera$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/Camera$1;-><init>(Lcom/sec/android/app/camera/Camera;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1962
    new-instance v0, Lcom/sec/android/app/camera/Camera$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/Camera$5;-><init>(Lcom/sec/android/app/camera/Camera;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mStartCheck:Ljava/lang/Runnable;

    .line 1970
    new-instance v0, Lcom/sec/android/app/camera/Camera$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/Camera$6;-><init>(Lcom/sec/android/app/camera/Camera;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mDatalineCheck:Ljava/lang/Runnable;

    .line 2352
    new-instance v0, Lcom/sec/android/app/camera/Camera$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/Camera$7;-><init>(Lcom/sec/android/app/camera/Camera;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mTimerHandler:Landroid/os/Handler;

    .line 3293
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/camera/Camera;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/sec/android/app/camera/Camera;->mJpegPictureCallbackTime:J

    return-wide v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/camera/Camera;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # J

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/sec/android/app/camera/Camera;->mJpegPictureCallbackTime:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/camera/Camera;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/sec/android/app/camera/Camera;->mShutterCallbackTime:J

    return-wide v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/camera/Camera;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # J

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/sec/android/app/camera/Camera;->mShutterCallbackTime:J

    return-wide p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/camera/Camera;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/camera/Camera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraAutoCapture:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/camera/Camera;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/sec/android/app/camera/Camera;->mCameraAutoCapture:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/camera/Camera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mFinishPostViewTest:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/camera/Camera;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/sec/android/app/camera/Camera;->mStopCamera:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/camera/Camera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/camera/Camera;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/camera/Camera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mIsCaptureEnable:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/camera/Camera;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/sec/android/app/camera/Camera;->mIsCaptureEnable:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/camera/Camera;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureStartTime:J

    return-wide v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/camera/Camera;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # J

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/sec/android/app/camera/Camera;->mCaptureStartTime:J

    return-wide p1
.end method

.method static synthetic access$2000(Lcom/sec/android/app/camera/Camera;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/sec/android/app/camera/Camera;->mRawPictureCallbackTime:J

    return-wide v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/camera/Camera;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # J

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/sec/android/app/camera/Camera;->mRawPictureCallbackTime:J

    return-wide p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/camera/Camera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mPausing:Z

    return v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/camera/Camera;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureTestSkip:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/CameraStorage;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/CameraStorage;)Lcom/sec/android/app/camera/CameraStorage;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Lcom/sec/android/app/camera/CameraStorage;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->filePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/camera/Camera;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->restartPreview()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/app/camera/Camera;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mImageIndexCount:I

    return v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/camera/Camera;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/sec/android/app/camera/Camera;->mImageIndexCount:I

    return p1
.end method

.method static synthetic access$2608(Lcom/sec/android/app/camera/Camera;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mImageIndexCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/camera/Camera;->mImageIndexCount:I

    return v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/camera/Camera;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/camera/Camera;->sendBroadCastAck(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$2800(Lcom/sec/android/app/camera/Camera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->bUseSdcard:Z

    return v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/camera/Camera;J)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # J

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/camera/Camera;->createName(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_AUTO_TEST:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/camera/Camera;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # [B

    .prologue
    .line 94
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/camera/Camera;->writeImage(Ljava/lang/String;Ljava/lang/String;[B)V

    return-void
.end method

.method static synthetic access$3200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_LOW_LIGHT_CAPTURE_TEST:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_SDCARD:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_DATA:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3500()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/app/camera/Camera;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/camera/Camera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->bDoneDTP:Z

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/camera/Camera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->bCheckDTP:Z

    return v0
.end method

.method static synthetic access$3702(Lcom/sec/android/app/camera/Camera;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/sec/android/app/camera/Camera;->bCheckDTP:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/camera/Camera;[B)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # [B

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/Camera;->checkDataline([B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/camera/Camera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->bSentAck:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/camera/Camera;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    return v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/camera/Camera;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/Camera;->sendBroadCastAck(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$402(Lcom/sec/android/app/camera/Camera;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    return p1
.end method

.method static synthetic access$4102(Lcom/sec/android/app/camera/Camera;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/sec/android/app/camera/Camera;->mShutterBtnlock:Z

    return p1
.end method

.method static synthetic access$4200(Lcom/sec/android/app/camera/Camera;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mAF_Fail_Count:I

    return v0
.end method

.method static synthetic access$4202(Lcom/sec/android/app/camera/Camera;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/sec/android/app/camera/Camera;->mAF_Fail_Count:I

    return p1
.end method

.method static synthetic access$4208(Lcom/sec/android/app/camera/Camera;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mAF_Fail_Count:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/camera/Camera;->mAF_Fail_Count:I

    return v0
.end method

.method static synthetic access$4300(Lcom/sec/android/app/camera/Camera;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusCallbackTime:J

    return-wide v0
.end method

.method static synthetic access$4302(Lcom/sec/android/app/camera/Camera;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # J

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/sec/android/app/camera/Camera;->mFocusCallbackTime:J

    return-wide p1
.end method

.method static synthetic access$4400(Lcom/sec/android/app/camera/Camera;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusStartTime:J

    return-wide v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/Camera$ImageCapture;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mImageCapture:Lcom/sec/android/app/camera/Camera$ImageCapture;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/camera/Camera;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    return v0
.end method

.method static synthetic access$4602(Lcom/sec/android/app/camera/Camera;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    return p1
.end method

.method static synthetic access$4700(Lcom/sec/android/app/camera/Camera;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->dialogFocusPopup()V

    return-void
.end method

.method static synthetic access$4800(Lcom/sec/android/app/camera/Camera;)Landroid/media/ToneGenerator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusToneGenerator:Landroid/media/ToneGenerator;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object v0
.end method

.method static synthetic access$4902(Lcom/sec/android/app/camera/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Landroid/hardware/Camera$Parameters;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/camera/Camera;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    return-object v0
.end method

.method static synthetic access$5002(Lcom/sec/android/app/camera/Camera;Landroid/hardware/Camera;)Landroid/hardware/Camera;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Landroid/hardware/Camera;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    return-object p1
.end method

.method static synthetic access$5100(Lcom/sec/android/app/camera/Camera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    return v0
.end method

.method static synthetic access$5102(Lcom/sec/android/app/camera/Camera;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    return p1
.end method

.method static synthetic access$5200(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/Camera$ShutterCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mShutterCallback:Lcom/sec/android/app/camera/Camera$ShutterCallback;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/Camera$RawPictureCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mRawPictureCallback:Lcom/sec/android/app/camera/Camera$RawPictureCallback;

    return-object v0
.end method

.method static synthetic access$5402(Lcom/sec/android/app/camera/Camera;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/sec/android/app/camera/Camera;->mchkopencamera:Z

    return p1
.end method

.method static synthetic access$5500(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/Camera$ErrorCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mErrorCallback:Lcom/sec/android/app/camera/Camera$ErrorCallback;

    return-object v0
.end method

.method static synthetic access$5600(Lcom/sec/android/app/camera/Camera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mIsPressedBackkey:Z

    return v0
.end method

.method static synthetic access$5602(Lcom/sec/android/app/camera/Camera;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/sec/android/app/camera/Camera;->mIsPressedBackkey:Z

    return p1
.end method

.method static synthetic access$5700(Lcom/sec/android/app/camera/Camera;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # I

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/Camera;->setLastOrientation(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/Interface/Capturer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/Interface/Capturer;)Lcom/sec/android/app/camera/Interface/Capturer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # Lcom/sec/android/app/camera/Interface/Capturer;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/camera/Camera;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->clearFocusState()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/camera/Camera;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->updateFocusIndicator()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/camera/Camera;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p1, "x1"    # I

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/Camera;->dialogErrorPopup(I)V

    return-void
.end method

.method private autoFocus()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 495
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call autoFocus mFocusState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 500
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusStartTime:J

    .line 502
    iput v3, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    .line 503
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->updateFocusIndicator()V

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mAutoFocusCallback:Lcom/sec/android/app/camera/Camera$AutoFocusCallback;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 508
    :cond_0
    return-void
.end method

.method private checkDataline([B)Z
    .locals 6
    .param p1, "data"    # [B

    .prologue
    .line 1029
    const/4 v0, 0x0

    .line 1030
    .local v0, "ins":Ljava/io/InputStream;
    const/4 v1, 0x0

    .line 1031
    .local v1, "sampleDTP":[B
    const-string v3, "FactoryCamera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkDataline - cameraType:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1033
    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 1034
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 1038
    :goto_0
    const/4 v2, 0x0

    .line 1040
    .local v2, "size":I
    :try_start_0
    invoke-virtual {v0}, Ljava/io/InputStream;->available()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1044
    :goto_1
    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/camera/Camera;->streamToBytes(Ljava/io/InputStream;I)[B

    move-result-object v1

    .line 1048
    :try_start_1
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1052
    :goto_2
    invoke-static {v1, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    return v3

    .line 1036
    .end local v2    # "size":I
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f060000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    .line 1041
    .restart local v2    # "size":I
    :catch_0
    move-exception v3

    goto :goto_1

    .line 1049
    :catch_1
    move-exception v3

    goto :goto_2
.end method

.method private static checkFsWritable()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2734
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/DCIM"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2736
    .local v1, "directoryName":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2737
    .local v0, "directory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2738
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2753
    :cond_0
    :goto_0
    return v4

    .line 2742
    :cond_1
    new-instance v3, Ljava/io/File;

    const-string v5, ".probe"

    invoke-direct {v3, v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2745
    .local v3, "f":Ljava/io/File;
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2746
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 2748
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2750
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2751
    const/4 v4, 0x1

    goto :goto_0

    .line 2752
    :catch_0
    move-exception v2

    .line 2753
    .local v2, "ex":Ljava/io/IOException;
    goto :goto_0
.end method

.method private clearFocusState()V
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    .line 441
    return-void
.end method

.method private closeCamera()V
    .locals 2

    .prologue
    .line 1411
    const-string v0, "FactoryCamera"

    const-string v1, "closeCamera"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1412
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 1413
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 1414
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    .line 1415
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    .line 1416
    const-string v0, "FactoryCamera"

    const-string v1, "closeCamera : mPreviewing set false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1418
    :cond_0
    return-void
.end method

.method private createName(J)Ljava/lang/String;
    .locals 7
    .param p1, "dateTaken"    # J

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 791
    const-string v1, ""

    .line 793
    .local v1, "saveName":Ljava/lang/String;
    const-string v2, "FactoryCamera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createName() - testType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    const-string v2, "autotest"

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 795
    const-string v0, ""

    .line 797
    .local v0, "TempSaveName":Ljava/lang/String;
    iget v2, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v2, :cond_1

    .line 798
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "REARIMAGE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%02d"

    new-array v4, v5, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->mImageIndexCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 803
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "yyyyMMddkkmmss"

    invoke-static {v3, p1, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 808
    .end local v0    # "TempSaveName":Ljava/lang/String;
    :goto_1
    const-string v2, "TAG"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createName saveName : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->isLowLightCaptureTesting()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 811
    iget v2, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v2, :cond_3

    .line 812
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LLS-R-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 817
    .end local v1    # "saveName":Ljava/lang/String;
    :cond_0
    :goto_2
    return-object v1

    .line 800
    .restart local v0    # "TempSaveName":Ljava/lang/String;
    .restart local v1    # "saveName":Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FRONTIMAGE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%02d"

    new-array v4, v5, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->mImageIndexCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 805
    .end local v0    # "TempSaveName":Ljava/lang/String;
    :cond_2
    const-string v2, "yyyy-MM-dd_kk_mm_ss"

    invoke-static {v2, p1, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 814
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LLS-F-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method private dialogErrorPopup(I)V
    .locals 5
    .param p1, "messageId"    # I

    .prologue
    const/4 v4, 0x0

    .line 912
    const-string v1, "FactoryCamera"

    const-string v2, "dialogErrorPopup"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 915
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 916
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mStartCheck:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 917
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mDatalineCheck:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 919
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogErrorPopup() : testType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogErrorPopup() : bIsUsbOrUartCommand : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 921
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    if-eqz v1, :cond_2

    .line 922
    const-string v1, "com.android.samsungtest.CAMERA_SHOT_ACK"

    const/4 v2, 0x2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/camera/Camera;->sendBroadCastAck(Ljava/lang/String;I)V

    .line 944
    :cond_1
    :goto_0
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->mchkopencamera:Z

    .line 945
    return-void

    .line 924
    :cond_2
    const-string v1, "com.android.samsungtest.CAMERA_BAD"

    invoke-direct {p0, v1}, Lcom/sec/android/app/camera/Camera;->sendBroadCastAck(Ljava/lang/String;)V

    .line 925
    iget-boolean v1, p0, Lcom/sec/android/app/camera/Camera;->mchkopencamera:Z

    if-nez v1, :cond_1

    .line 926
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 927
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a000d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 928
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 930
    const v1, 0x7f0a0001

    new-instance v2, Lcom/sec/android/app/camera/Camera$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camera/Camera$2;-><init>(Lcom/sec/android/app/camera/Camera;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 938
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 939
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    .line 940
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private dialogFocusPopup()V
    .locals 4

    .prologue
    .line 2774
    const-string v1, "FactoryCamera"

    const-string v2, "dialogFocusPopup"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2776
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2777
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mStartCheck:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2778
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mDatalineCheck:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2780
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogFocusPopup() : testType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2781
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogFocusPopup() : bIsUsbOrUartCommand : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2782
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    if-eqz v1, :cond_2

    .line 2783
    const-string v1, "com.android.samsungtest.CAMERA_SHOT_ACK"

    const/4 v2, 0x3

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/camera/Camera;->sendBroadCastAck(Ljava/lang/String;I)V

    .line 2803
    :cond_1
    :goto_0
    return-void

    .line 2785
    :cond_2
    const-string v1, "com.android.samsungtest.AUTOFOCUS_BAD"

    invoke-direct {p0, v1}, Lcom/sec/android/app/camera/Camera;->sendBroadCastAck(Ljava/lang/String;)V

    .line 2787
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2788
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a000d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2789
    const v1, 0x7f0a0007

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 2790
    const v1, 0x7f0a0001

    new-instance v2, Lcom/sec/android/app/camera/Camera$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camera/Camera$9;-><init>(Lcom/sec/android/app/camera/Camera;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2798
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2799
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    .line 2800
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private dialogNGPopup(I)V
    .locals 5
    .param p1, "messageId"    # I

    .prologue
    const/4 v4, 0x0

    .line 948
    const-string v1, "FactoryCamera"

    const-string v2, "dialogNGPopup"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 950
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 951
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 952
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mStartCheck:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 953
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mDatalineCheck:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 955
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogNGPopup() : testType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogNGPopup() : bIsUsbOrUartCommand : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 957
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    if-eqz v1, :cond_2

    .line 958
    const-string v1, "com.android.samsungtest.CAMERA_SHOT_ACK"

    const/4 v2, 0x4

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/camera/Camera;->sendBroadCastAck(Ljava/lang/String;I)V

    .line 981
    :cond_1
    :goto_0
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->mchkopencamera:Z

    .line 982
    return-void

    .line 960
    :cond_2
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 961
    const-string v1, "com.android.samsungtest.CAMERA_BAD"

    invoke-direct {p0, v1}, Lcom/sec/android/app/camera/Camera;->sendBroadCastAck(Ljava/lang/String;)V

    .line 964
    :cond_3
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogNGPopup mchkopencamera : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mchkopencamera:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    iget-boolean v1, p0, Lcom/sec/android/app/camera/Camera;->mchkopencamera:Z

    if-nez v1, :cond_1

    .line 966
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 967
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a000d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 968
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 970
    const v1, 0x7f0a0001

    new-instance v2, Lcom/sec/android/app/camera/Camera$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camera/Camera$3;-><init>(Lcom/sec/android/app/camera/Camera;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 975
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 976
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    .line 977
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private doFocus(Z)V
    .locals 3
    .param p1, "pressed"    # Z

    .prologue
    .line 444
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doFocus - pressed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cameraType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mPreviewing:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-nez v0, :cond_1

    .line 448
    const-string v0, "FactoryCamera"

    const-string v1, "mCameraDevice is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    :cond_0
    :goto_0
    return-void

    .line 452
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getLastOrientation()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Camera;->setOrientationOnTake(I)V

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 455
    if-eqz p1, :cond_7

    .line 456
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_6

    .line 459
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/framework/CameraSettings;->getSupportedAutofocus()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->isLowLightCaptureTesting()Z

    move-result v0

    if-nez v0, :cond_4

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_3

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->NO_CANCEL_FOCUS:Z

    if-nez v0, :cond_3

    .line 462
    const-string v0, "FactoryCamera"

    const-string v1, "cancelAutoFocus before autofocus..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    .line 467
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "auto"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 469
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->autoFocus()V

    goto :goto_0

    .line 470
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    if-eqz v0, :cond_5

    .line 471
    const-string v0, "FactoryCamera"

    const-string v1, "AF not supported or preview not started"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    invoke-interface {v0}, Lcom/sec/android/app/camera/Interface/Capturer;->onSnap()V

    goto :goto_0

    .line 475
    :cond_5
    const-string v0, "FactoryCamera"

    const-string v1, "CaptureObj NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 478
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    invoke-interface {v0}, Lcom/sec/android/app/camera/Interface/Capturer;->onSnap()V

    goto :goto_0

    .line 483
    :cond_7
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_8

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->NO_CANCEL_FOCUS:Z

    if-nez v0, :cond_8

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    .line 488
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->clearFocusState()V

    .line 489
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->updateFocusIndicator()V

    goto/16 :goto_0
.end method

.method private doSnap()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 550
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getLastOrientation()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Camera;->setOrientationOnTake(I)V

    .line 552
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doSnap()- mFocusState:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-ne v0, v3, :cond_1

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    invoke-interface {v0}, Lcom/sec/android/app/camera/Interface/Capturer;->onSnap()V

    .line 577
    :cond_0
    :goto_0
    return-void

    .line 556
    :cond_1
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    if-nez v0, :cond_4

    .line 558
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    if-eqz v0, :cond_3

    .line 559
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    invoke-interface {v0}, Lcom/sec/android/app/camera/Interface/Capturer;->onSnap()V

    .line 561
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->clearFocusState()V

    .line 562
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->updateFocusIndicator()V

    goto :goto_0

    .line 563
    :cond_4
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    .line 564
    const-string v0, "FactoryCamera"

    const-string v1, "doSnap()- FOCUS_FAIL : the shot is canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 565
    :cond_5
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    if-ne v0, v3, :cond_6

    .line 569
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    .line 570
    const-string v0, "FactoryCamera"

    const-string v1, "doSnap()- FOCUSING : the shot is canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 571
    :cond_6
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    if-nez v0, :cond_0

    .line 574
    const-string v0, "FactoryCamera"

    const-string v1, "doSnap()- FOCUS_NOT_STARTED : the shot is canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private ensureCameraDevice()Z
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1433
    const-string v9, "FactoryCamera"

    const-string v10, "ensureCameraDevice"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1434
    iget-object v9, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-nez v9, :cond_3

    .line 1435
    const-string v9, "FactoryCamera"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ensureCameraDevice:Type:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1438
    :try_start_0
    const-string v5, "/sys/class/camera/rear/rear_camfw"

    .line 1439
    .local v5, "sysFsPath":Ljava/lang/String;
    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    .line 1441
    .local v2, "fr":Ljava/io/FileReader;
    if-eqz v2, :cond_0

    .line 1442
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1443
    .local v0, "br":Ljava/io/BufferedReader;
    if-eqz v0, :cond_0

    .line 1444
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 1445
    .local v4, "mFWInfo":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 1446
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V

    .line 1447
    const-string v9, "FactoryCamera"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "try Util.openCamera to getFWInfo() - FW info["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1450
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "mFWInfo":Ljava/lang/String;
    :cond_0
    const-string v6, "/sys/class/camera/rear/rear_camfw_load"

    .line 1451
    .local v6, "sysLoadedFsPath":Ljava/lang/String;
    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    .line 1453
    .local v3, "frLoadedFW":Ljava/io/FileReader;
    if-eqz v3, :cond_1

    .line 1454
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1455
    .restart local v0    # "br":Ljava/io/BufferedReader;
    if-eqz v0, :cond_1

    .line 1456
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 1457
    .restart local v4    # "mFWInfo":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 1458
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 1459
    const-string v9, "FactoryCamera"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "try Util.openCamera to getFWInfo() - FW LoadFWinfo["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1467
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "fr":Ljava/io/FileReader;
    .end local v3    # "frLoadedFW":Ljava/io/FileReader;
    .end local v4    # "mFWInfo":Ljava/lang/String;
    .end local v5    # "sysFsPath":Ljava/lang/String;
    .end local v6    # "sysLoadedFsPath":Ljava/lang/String;
    :cond_1
    :goto_0
    :try_start_1
    iget-object v9, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v9, v9, Lcom/sec/android/app/camera/Feature;->CAMERA_BOOST_ENABLE:Z

    if-eqz v9, :cond_2

    .line 1468
    const/16 v9, 0x7d0

    invoke-virtual {p0, v9}, Lcom/sec/android/app/camera/Camera;->acquireDVFS(I)V

    .line 1471
    :cond_2
    iget v9, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-static {v9}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1478
    iget v9, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    iget-object v10, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {p0, p0, v9, v10}, Lcom/sec/android/app/camera/Camera;->setCameraDisplayOrientation(Landroid/app/Activity;ILandroid/hardware/Camera;)V

    .line 1480
    iget-object v9, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v9}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 1481
    iget-object v9, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v10, "factorytest"

    const-string v11, "1"

    invoke-virtual {v9, v10, v11}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1482
    iget-object v9, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v10, "cam_mode"

    const-string v11, "0"

    invoke-virtual {v9, v10, v11}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1483
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v10}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/camera/framework/CameraSettings;->isSupportedFocusModes(Ljava/util/List;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1484
    iget-object v9, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/camera/framework/CameraSettings;->getFocusMode()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 1489
    :goto_1
    const-string v9, "selftest"

    iget-object v10, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1490
    iget v9, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-ne v9, v7, :cond_6

    .line 1491
    iget-object v9, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v9, v9, Lcom/sec/android/app/camera/Feature;->SET_MAX_SUPPORTED_PICTURE_SIZE:Z

    if-nez v9, :cond_5

    .line 1492
    iget-object v9, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v10, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v10, v10, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_W:I

    iget-object v11, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v11, v11, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_H:I

    invoke-virtual {v9, v10, v11}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 1505
    :goto_2
    :try_start_2
    const-string v9, "FactoryCamera"

    const-string v10, "mCameraDevice.setParameters(mParameters)"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1506
    iget-object v9, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v10, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v10}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1515
    :goto_3
    iget-object v9, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v10, p0, Lcom/sec/android/app/camera/Camera;->mErrorCallback:Lcom/sec/android/app/camera/Camera$ErrorCallback;

    invoke-virtual {v9, v10}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    .line 1517
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v9, :cond_8

    :goto_4
    move v8, v7

    :goto_5
    return v8

    .line 1462
    :catch_0
    move-exception v1

    .line 1463
    .local v1, "e":Ljava/lang/Exception;
    const-string v9, "FactoryCamera"

    const-string v10, "read FW info fail"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1472
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 1473
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v7, "FactoryCamera"

    const-string v9, "service cannot connect. critical exception occured."

    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1474
    const v7, 0x7f0a000a

    invoke-direct {p0, v7}, Lcom/sec/android/app/camera/Camera;->dialogErrorPopup(I)V

    goto :goto_5

    .line 1486
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    const-string v9, "FactoryCamera"

    const-string v10, "not support focusmode"

    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1495
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    iget-object v11, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/camera/framework/CameraSettings;->setPictureSize(ILandroid/hardware/Camera$Parameters;)V

    goto :goto_2

    .line 1498
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    iget-object v11, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/camera/framework/CameraSettings;->setPictureSize(ILandroid/hardware/Camera$Parameters;)V

    goto :goto_2

    .line 1501
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    iget-object v11, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/camera/framework/CameraSettings;->setPictureSize(ILandroid/hardware/Camera$Parameters;)V

    goto :goto_2

    .line 1509
    :catch_2
    move-exception v1

    .line 1510
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v7, "FactoryCamera"

    const-string v9, "setParameter fail"

    invoke-static {v7, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1511
    iget-object v7, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/16 v9, 0x9

    invoke-virtual {v7, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_5

    .end local v1    # "e":Ljava/lang/RuntimeException;
    :cond_8
    move v7, v8

    .line 1517
    goto :goto_4

    .line 1507
    :catch_3
    move-exception v9

    goto :goto_3
.end method

.method public static getAvailableSpaceSd()J
    .locals 10

    .prologue
    const-wide/16 v2, -0x1

    .line 765
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v5

    .line 766
    .local v5, "state":Ljava/lang/String;
    const-string v6, "checking"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 767
    const-wide/16 v2, -0x2

    .line 787
    :cond_0
    :goto_0
    return-wide v2

    .line 769
    :cond_1
    const-string v6, "mounted"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 773
    new-instance v0, Ljava/io/File;

    sget-object v6, Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_SDCARD:Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 774
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 775
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 780
    :try_start_0
    new-instance v4, Landroid/os/StatFs;

    sget-object v6, Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_SDCARD:Ljava/lang/String;

    invoke-direct {v4, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 781
    .local v4, "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v8

    int-to-long v8, v8

    mul-long v2, v6, v8

    .line 782
    .local v2, "space":J
    const-string v6, "FactoryCamera"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAvailableSpaceSd : space ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 784
    .end local v2    # "space":J
    .end local v4    # "stat":Landroid/os/StatFs;
    :catch_0
    move-exception v1

    .line 785
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "FactoryCamera"

    const-string v7, "Fail to access external storage"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 787
    const-wide/16 v2, -0x3

    goto :goto_0
.end method

.method private getCameraInfo()V
    .locals 4

    .prologue
    .line 3458
    const-string v1, "FactoryCamera"

    const-string v2, "Camera.getCameraInfo()..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3460
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/camera/Camera;->mNumberOfCameras:I

    .line 3461
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Camera.getCameraInfo() mNumberOfCameras : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/camera/Camera;->mNumberOfCameras:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3463
    iget v1, p0, Lcom/sec/android/app/camera/Camera;->mNumberOfCameras:I

    new-array v1, v1, [Landroid/hardware/Camera$CameraInfo;

    iput-object v1, p0, Lcom/sec/android/app/camera/Camera;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    .line 3465
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/app/camera/Camera;->mNumberOfCameras:I

    if-ge v0, v1, :cond_0

    .line 3466
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    aput-object v2, v1, v0

    .line 3467
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    aget-object v1, v1, v0

    invoke-static {v0, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 3465
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3469
    :cond_0
    return-void
.end method

.method private getIntentInfo()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1521
    const-string v0, "FactoryCamera"

    const-string v1, "getIntentInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1522
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "test_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    .line 1523
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "camera_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    .line 1525
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ommision_test"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->ommisionTest:Z

    .line 1528
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getIntentInfo() : testType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1529
    const-string v0, "autotest"

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1530
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    .line 1536
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "camcorder_preview_test"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTest:Z

    .line 1538
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ois_test"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mFinishOISTest:Z

    .line 1541
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "auto_capture"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraAutoCapture:Z

    .line 1542
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "postview_test"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mFinishPostViewTest:Z

    .line 1543
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "orientation_reverse"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mIsOrientationReverse:Z

    .line 1544
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getIntentInfo EX : testType["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] cameraType["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ommisionTest["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->ommisionTest:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] mFlashEnable["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mFinishCamcorderPreviewTest["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTest:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] mFinishPostViewTest["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mFinishPostViewTest:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] mIsOrientationReverse["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mIsOrientationReverse:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] mCameraAutoCapture["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mCameraAutoCapture:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1548
    return-void

    .line 1532
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "torch_on"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    goto/16 :goto_0
.end method

.method public static hasStorage()Z
    .locals 5

    .prologue
    .line 2718
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 2719
    .local v0, "state":Ljava/lang/String;
    const-string v2, "FactoryCamera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "hasStorage - storage state is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2721
    const-string v2, "mounted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2722
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->checkFsWritable()Z

    move-result v1

    .line 2723
    .local v1, "writable":Z
    const-string v2, "FactoryCamera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "storage writable is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2726
    .end local v1    # "writable":Z
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initDTPsetting()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1552
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v0

    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v0, v3}, Lcom/sec/android/app/camera/framework/CameraSettings;->needToCheckDTP(I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->bDoneDTP:Z

    .line 1553
    iput-boolean v1, p0, Lcom/sec/android/app/camera/Camera;->bCheckDTP:Z

    .line 1555
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    if-ne v0, v1, :cond_1

    .line 1556
    const-string v0, "FactoryCamera"

    const-string v1, "FlashEnable = true"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1557
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->bEnablePreviewCb:Z

    .line 1563
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 1552
    goto :goto_0

    .line 1560
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/app/camera/Camera;->bEnablePreviewCb:Z

    goto :goto_1
.end method

.method private initLayoutSetting()V
    .locals 4

    .prologue
    .line 1635
    const-string v2, "FactoryCamera"

    const-string v3, "initLayoutSetting()..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1636
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1637
    .local v1, "win":Landroid/view/Window;
    const v2, 0x680080

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 1638
    const v2, 0x7f030001

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/Camera;->setContentView(I)V

    .line 1640
    const v2, 0x7f090013

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    .line 1641
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    if-eqz v2, :cond_8

    .line 1642
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->isLowLightCaptureTesting()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1643
    iget v2, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v2, :cond_4

    .line 1644
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    if-eqz v2, :cond_3

    .line 1645
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v3, "Please shoot low-light environments. (REAR)"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1670
    :goto_0
    const v2, 0x7f09000b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/camera/VideoPreview;

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mSurfaceView:Lcom/sec/android/app/camera/VideoPreview;

    .line 1675
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mSurfaceView:Lcom/sec/android/app/camera/VideoPreview;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/VideoPreview;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 1676
    .local v0, "holder":Landroid/view/SurfaceHolder;
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 1677
    const/4 v2, 0x3

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 1679
    const v2, 0x7f09000f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mShutterButtonLayout:Landroid/widget/RelativeLayout;

    .line 1680
    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mIsOrientationReverse:Z

    if-eqz v2, :cond_1

    .line 1681
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mShutterButtonLayout:Landroid/widget/RelativeLayout;

    const/high16 v3, 0x43340000    # 180.0f

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setRotation(F)V

    .line 1684
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/framework/CameraSettings;->needToCheckCamcorderPreviewTest()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->bDoneDTP:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTest:Z

    if-nez v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v2, :cond_2

    .line 1688
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mShutterButtonLayout:Landroid/widget/RelativeLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1691
    :cond_2
    const v2, 0x7f09000c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/camera/framework/ShutterButton;

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    .line 1692
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/camera/framework/ShutterButton;->setOnShutterButtonListener(Lcom/sec/android/app/camera/framework/ShutterButton$OnShutterButtonListener;)V

    .line 1694
    const/high16 v2, 0x7f040000

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mFocusBlinkAnimation:Landroid/view/animation/Animation;

    .line 1695
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mFocusBlinkAnimation:Landroid/view/animation/Animation;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 1696
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mFocusBlinkAnimation:Landroid/view/animation/Animation;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    .line 1700
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mOpenCameraThread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1704
    :goto_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/camera/Camera;->mZoomValue:I

    .line 1706
    const v2, 0x7f090010

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/Camera;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/camera/framework/FocusRectangle;

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mFocusRectangle:Lcom/sec/android/app/camera/framework/FocusRectangle;

    .line 1707
    return-void

    .line 1647
    .end local v0    # "holder":Landroid/view/SurfaceHolder;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v3, "Please shoot camera chart. (REAR)"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1650
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    if-eqz v2, :cond_5

    .line 1651
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v3, "Please shoot low-light environments. (FRONT)"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1653
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v3, "Please shoot camera chart. (FRONT)"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1657
    :cond_6
    iget v2, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v2, :cond_7

    .line 1658
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v3, "Please shoot camera chart. (REAR)"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1660
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v3, "Please shoot camera chart. (FRONT)"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1664
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1701
    .restart local v0    # "holder":Landroid/view/SurfaceHolder;
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public static isShowFailDialog()Z
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v2, 0x0

    .line 2337
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2338
    .local v0, "thistime":J
    sget-wide v4, Lcom/sec/android/app/camera/Camera;->lasttouchtime:J

    cmp-long v3, v4, v8

    if-eqz v3, :cond_1

    .line 2339
    sget-wide v4, Lcom/sec/android/app/camera/Camera;->lasttouchtime:J

    sub-long v4, v0, v4

    const-wide/16 v6, 0x7d0

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 2340
    sput-wide v8, Lcom/sec/android/app/camera/Camera;->lasttouchtime:J

    .line 2341
    const/4 v2, 0x1

    .line 2348
    :goto_0
    return v2

    .line 2343
    :cond_0
    sput-wide v0, Lcom/sec/android/app/camera/Camera;->lasttouchtime:J

    goto :goto_0

    .line 2347
    :cond_1
    sput-wide v0, Lcom/sec/android/app/camera/Camera;->lasttouchtime:J

    goto :goto_0
.end method

.method public static isSupported(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2
    .param p0, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "supported":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 1806
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private restartPreview()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2406
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mSurfaceView:Lcom/sec/android/app/camera/VideoPreview;

    .line 2407
    .local v2, "surfaceView":Lcom/sec/android/app/camera/VideoPreview;
    const/4 v1, 0x0

    .line 2408
    .local v1, "previewWidth":I
    const/4 v0, 0x0

    .line 2410
    .local v0, "previewHeight":I
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    if-eqz v3, :cond_7

    .line 2411
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->isLowLightCaptureTesting()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2412
    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v3, :cond_3

    .line 2413
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    if-eqz v3, :cond_2

    .line 2414
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v4, "Please shoot low-light environments. (REAR)"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2436
    :goto_0
    const-string v3, "selftest"

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2437
    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-ne v3, v5, :cond_8

    .line 2438
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v1, v3, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_W:I

    .line 2439
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v0, v3, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_H:I

    .line 2449
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v3, :cond_e

    .line 2450
    const-string v3, "selftest"

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 2451
    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-ne v3, v5, :cond_b

    .line 2452
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->SET_SURFACE_VIEW_FINDER:Z

    if-eqz v3, :cond_a

    .line 2453
    invoke-virtual {v2}, Lcom/sec/android/app/camera/VideoPreview;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v2}, Lcom/sec/android/app/camera/VideoPreview;->getMeasuredHeight()I

    move-result v4

    invoke-direct {p0, v3, v4, v5}, Lcom/sec/android/app/camera/Camera;->setViewFinder(IIZ)V

    .line 2476
    :goto_2
    iput v5, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    .line 2478
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v3, :cond_1

    .line 2479
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v3}, Landroid/view/OrientationEventListener;->enable()V

    .line 2480
    :cond_1
    return-void

    .line 2416
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v4, "Please shoot camera chart. (REAR)"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2419
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    if-eqz v3, :cond_4

    .line 2420
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v4, "Please shoot low-light environments. (FRONT)"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2422
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v4, "Please shoot camera chart. (FRONT)"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2426
    :cond_5
    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v3, :cond_6

    .line 2427
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v4, "Please shoot camera chart. (REAR)"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2429
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v4, "Please shoot camera chart. (FRONT)"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2433
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureText:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2441
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v1, v3, Lcom/sec/android/app/camera/Feature;->PREVIEW_WIDTH:I

    .line 2442
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v0, v3, Lcom/sec/android/app/camera/Feature;->PREVIEW_HEIGHT:I

    goto :goto_1

    .line 2445
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v3

    iget v1, v3, Landroid/hardware/Camera$Size;->width:I

    .line 2446
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v3

    iget v0, v3, Landroid/hardware/Camera$Size;->height:I

    goto/16 :goto_1

    .line 2456
    :cond_a
    invoke-direct {p0, v1, v0, v5}, Lcom/sec/android/app/camera/Camera;->setViewFinder(IIZ)V

    goto :goto_2

    .line 2459
    :cond_b
    invoke-direct {p0, v1, v0, v5}, Lcom/sec/android/app/camera/Camera;->setViewFinder(IIZ)V

    goto :goto_2

    .line 2462
    :cond_c
    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-ne v3, v5, :cond_d

    .line 2463
    invoke-virtual {v2}, Lcom/sec/android/app/camera/VideoPreview;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v2}, Lcom/sec/android/app/camera/VideoPreview;->getMeasuredHeight()I

    move-result v4

    invoke-direct {p0, v3, v4, v5}, Lcom/sec/android/app/camera/Camera;->setViewFinder(IIZ)V

    goto :goto_2

    .line 2466
    :cond_d
    invoke-direct {p0, v1, v0, v5}, Lcom/sec/android/app/camera/Camera;->setViewFinder(IIZ)V

    goto :goto_2

    .line 2473
    :cond_e
    invoke-virtual {v2, v1, v0}, Lcom/sec/android/app/camera/VideoPreview;->setAspectRatio(II)V

    goto :goto_2
.end method

.method public static roundOrientation(I)I
    .locals 1
    .param p0, "orientation"    # I

    .prologue
    .line 3070
    add-int/lit8 v0, p0, 0x2d

    div-int/lit8 v0, v0, 0x5a

    mul-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    return v0
.end method

.method private sendBroadCastAck(Ljava/lang/String;)V
    .locals 5
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 896
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendBroadCastAck - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "TYPE"

    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "TYPE"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Camera;->sendBroadcast(Landroid/content/Intent;)V

    .line 900
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->bSentAck:Z

    .line 901
    return-void
.end method

.method private sendBroadCastAck(Ljava/lang/String;I)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "result"    # I

    .prologue
    .line 904
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendBroadCastAck - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "result"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Camera;->sendBroadcast(Landroid/content/Intent;)V

    .line 907
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->bSentAck:Z

    .line 908
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    .line 909
    return-void
.end method

.method private setLastOrientation(I)V
    .locals 0
    .param p1, "lastOrientation"    # I

    .prologue
    .line 3074
    iput p1, p0, Lcom/sec/android/app/camera/Camera;->mLastOrientation:I

    .line 3075
    return-void
.end method

.method private setStoragepath()V
    .locals 1

    .prologue
    .line 1566
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->bUseSdcard:Z

    .line 1568
    return-void
.end method

.method public static setSystemKeyBlock(Landroid/content/ComponentName;I)V
    .locals 4
    .param p0, "componentName"    # Landroid/content/ComponentName;
    .param p1, "keyCode"    # I

    .prologue
    .line 3148
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 3151
    .local v1, "wm":Landroid/view/IWindowManager;
    const/4 v2, 0x1

    :try_start_0
    invoke-interface {v1, p1, p0, v2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3155
    :goto_0
    return-void

    .line 3152
    :catch_0
    move-exception v0

    .line 3153
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "FactoryCamera"

    const-string v3, "setSystemKeyBlock exception!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setViewFinder(IIZ)V
    .locals 28
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "startPreview"    # Z

    .prologue
    .line 2483
    const-string v22, "FactoryCamera"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "setViewFinder - w: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " h: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " startPreview: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2484
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Camera;->mPausing:Z

    move/from16 v22, v0

    if-eqz v22, :cond_1

    .line 2485
    const-string v22, "FactoryCamera"

    const-string v23, "setViewFinder - mPausing == true, return"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2715
    :cond_0
    :goto_0
    return-void

    .line 2489
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    move/from16 v22, v0

    if-eqz v22, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/Camera;->mViewFinderWidth:I

    move/from16 v22, v0

    move/from16 v0, p1

    move/from16 v1, v22

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/Camera;->mViewFinderHeight:I

    move/from16 v22, v0

    move/from16 v0, p2

    move/from16 v1, v22

    if-ne v0, v1, :cond_2

    .line 2490
    const-string v22, "FactoryCamera"

    const-string v23, "mPreviewing && w == mViewFinderWidth && h == mViewFinderHeight"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2491
    const-string v22, "FactoryCamera"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "mPreviewing("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " mViewFinderWidth("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/Camera;->mViewFinderWidth:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " mViewFinderHeight("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/Camera;->mViewFinderHeight:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2496
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/camera/Camera;->ensureCameraDevice()Z

    move-result v22

    if-nez v22, :cond_3

    .line 2497
    const-string v22, "FactoryCamera"

    const-string v23, "!ensureCameraDevice()"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2501
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    move-object/from16 v22, v0

    if-nez v22, :cond_4

    .line 2502
    const-string v22, "FactoryCamera"

    const-string v23, "mSurfaceHolder == null"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2506
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/Camera;->isFinishing()Z

    move-result v22

    if-eqz v22, :cond_5

    .line 2507
    const-string v22, "FactoryCamera"

    const-string v23, "isFinishing()"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2511
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Camera;->mPausing:Z

    move/from16 v22, v0

    if-eqz v22, :cond_6

    .line 2512
    const-string v22, "FactoryCamera"

    const-string v23, "setViewFinder - mPausing == true, return"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2517
    :cond_6
    const-string v22, "FactoryCamera"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "set ViewFinderWidth("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ","

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2519
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/camera/Camera;->mViewFinderWidth:I

    .line 2520
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/camera/Camera;->mViewFinderHeight:I

    .line 2521
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/Camera;->mOriginalViewFinderHeight:I

    move/from16 v22, v0

    if-nez v22, :cond_7

    .line 2522
    const-string v22, "FactoryCamera"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "set OriginalViewFinderWidth("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ","

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2523
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/camera/Camera;->mOriginalViewFinderWidth:I

    .line 2524
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/camera/Camera;->mOriginalViewFinderHeight:I

    .line 2527
    :cond_7
    if-nez p3, :cond_8

    .line 2528
    const-string v22, "FactoryCamera"

    const-string v23, "startPreview == false"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2535
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 2543
    move/from16 v0, p1

    move/from16 v1, p2

    if-ge v0, v1, :cond_9

    .line 2544
    move/from16 v11, p1

    .line 2545
    .local v11, "temp":I
    move/from16 p1, p2

    .line 2546
    move/from16 p2, v11

    .line 2547
    const-string v22, "FactoryCamera"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Swapped dimensions W = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " H = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2550
    .end local v11    # "temp":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v22, v0

    if-eqz v22, :cond_b

    .line 2551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v10

    .line 2552
    .local v10, "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    if-nez v10, :cond_a

    .line 2553
    const-string v22, "FactoryCamera"

    const-string v23, "supported preview size is null"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2556
    :cond_a
    const/4 v9, 0x0

    .line 2557
    .local v9, "previewsize":Landroid/hardware/Camera$Size;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v8

    .line 2558
    .local v8, "picturesize":Landroid/hardware/Camera$Size;
    iget v0, v8, Landroid/hardware/Camera$Size;->width:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-double v0, v0

    move-wide/from16 v22, v0

    iget v0, v8, Landroid/hardware/Camera$Size;->height:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    div-double v22, v22, v24

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-virtual {v0, v10, v1, v2}, Lcom/sec/android/app/camera/Camera;->findOptimalPreviewSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;

    move-result-object v9

    .line 2561
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->FACTORY_CAMERA_FIX_RESOLUTION_640X480:Z

    move/from16 v22, v0

    if-eqz v22, :cond_13

    .line 2562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/camera/Feature;->PREVIEW_WIDTH:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/android/app/camera/Feature;->PREVIEW_HEIGHT:I

    move/from16 v24, v0

    invoke-virtual/range {v22 .. v24}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 2567
    :goto_1
    const-string v22, "selftest"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 2568
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_b

    .line 2569
    const-string v22, "FactoryCamera"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "FRONT_CAMERA_MODE: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MODE:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2570
    const-string v22, "FactoryCamera"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "FRONT_CAMERA_PREIVEW_WIDTH: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_W:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", VT_CAMERA_PREIVEW_HEIGHT: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_H:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2574
    const-string v22, "FactoryCamera"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "FRONT_CAMERA_MIN_FPS: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MIN_FPS:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", VT_CAMERA_MAX_FPS: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MAX_FPS:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2577
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v22, v0

    const-string v23, "selftestmode"

    const/16 v24, 0x1

    invoke-virtual/range {v22 .. v24}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 2578
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v22, v0

    const-string v23, "vtmode"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MODE:I

    move/from16 v24, v0

    invoke-virtual/range {v22 .. v24}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 2579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_W:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_H:I

    move/from16 v24, v0

    invoke-virtual/range {v22 .. v24}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 2581
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MIN_FPS:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MAX_FPS:I

    move/from16 v24, v0

    invoke-virtual/range {v22 .. v24}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    .line 2587
    .end local v8    # "picturesize":Landroid/hardware/Camera$Size;
    .end local v9    # "previewsize":Landroid/hardware/Camera$Size;
    .end local v10    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v22, v0

    if-eqz v22, :cond_c

    .line 2588
    const-string v22, "FactoryCamera"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "mParameters.setPreviewSize W = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " H = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2590
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Camera;->bDoneDTP:Z

    move/from16 v22, v0

    if-nez v22, :cond_14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_14

    .line 2591
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v22, v0

    const-string v23, "chk_dataline"

    const/16 v24, 0x1

    invoke-virtual/range {v22 .. v24}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 2596
    :goto_2
    :try_start_0
    const-string v22, "FactoryCamera"

    const-string v23, "mCameraDevice.setParameters(mParameters)"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2597
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2609
    :cond_c
    :goto_3
    :try_start_1
    const-string v22, "FactoryCamera"

    const-string v23, "mCameraDevice.setPreviewDisplay"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2610
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2619
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    .line 2620
    .local v18, "wallTimeStart":J
    invoke-static {}, Landroid/os/Debug;->threadCpuTimeNanos()J

    move-result-wide v14

    .line 2622
    .local v14, "threadTimeStart":J
    new-instance v21, Ljava/lang/Object;

    invoke-direct/range {v21 .. v21}, Ljava/lang/Object;-><init>()V

    .line 2623
    .local v21, "watchDogSync":Ljava/lang/Object;
    new-instance v20, Ljava/lang/Thread;

    new-instance v22, Lcom/sec/android/app/camera/Camera$8;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    move-wide/from16 v3, v18

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/camera/Camera$8;-><init>(Lcom/sec/android/app/camera/Camera;Ljava/lang/Object;J)V

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2658
    .local v20, "watchDog":Ljava/lang/Thread;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Thread;->start()V

    .line 2669
    const-string v22, "FactoryCamera"

    const-string v23, "calling mCameraDevice.startPreview"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2671
    :try_start_2
    const-string v22, "FactoryCamera"

    const-string v23, "mCameraDevice.startPreview()"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2673
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Camera;->bEnablePreviewCb:Z

    move/from16 v22, v0

    if-eqz v22, :cond_d

    .line 2674
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mPreviewCallback:Lcom/sec/android/app/camera/Camera$PreviewCallback;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 2675
    const-string v22, "FactoryCamera"

    const-string v23, "setViewFinder :: postDelayed-mDatalineCheck()"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2676
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mDatalineCheck:Ljava/lang/Runnable;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2677
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mDatalineCheck:Ljava/lang/Runnable;

    move-object/from16 v23, v0

    const-wide/16 v24, 0x1388

    invoke-virtual/range {v22 .. v25}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2680
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/hardware/Camera;->startPreview()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 2689
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    move/from16 v22, v0

    if-eqz v22, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    move/from16 v22, v0

    const/16 v23, -0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_f

    .line 2690
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/camera/framework/CameraSettings;->getSupportedAutofocus()Z

    move-result v22

    if-eqz v22, :cond_f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    move/from16 v22, v0

    if-eqz v22, :cond_f

    .line 2691
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/camera/Camera;->autoFocus()V

    .line 2694
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Camera;->bEnablePreviewCb:Z

    move/from16 v22, v0

    if-nez v22, :cond_10

    .line 2695
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mStartCheck:Ljava/lang/Runnable;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2696
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mStartCheck:Ljava/lang/Runnable;

    move-object/from16 v23, v0

    const-wide/16 v24, 0x3e8

    invoke-virtual/range {v22 .. v25}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2698
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/app/camera/framework/CameraSettings;->needToCheckDTP(I)Z

    move-result v22

    if-eqz v22, :cond_11

    const-string v22, "selftest"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_12

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/app/camera/framework/CameraSettings;->needToCheckDTP(I)Z

    move-result v22

    if-eqz v22, :cond_12

    .line 2700
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/Camera;->startTimerCaptureBlock()V

    .line 2702
    :cond_12
    const-string v22, "FactoryCamera"

    const-string v23, "setViewFinder : set mPreviewing = true"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2703
    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    .line 2705
    monitor-enter v21

    .line 2706
    :try_start_3
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->notify()V

    .line 2707
    monitor-exit v21
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2709
    invoke-static {}, Landroid/os/Debug;->threadCpuTimeNanos()J

    move-result-wide v12

    .line 2710
    .local v12, "threadTimeEnd":J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v16

    .line 2711
    .local v16, "wallTimeEnd":J
    sub-long v22, v16, v18

    const-wide/16 v24, 0xbb8

    cmp-long v22, v22, v24

    if-lez v22, :cond_0

    .line 2712
    const-string v22, "FactoryCamera"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "startPreview() to "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sub-long v24, v16, v18

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " ms. Thread time was"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sub-long v24, v12, v14

    const-wide/32 v26, 0xf4240

    div-long v24, v24, v26

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " ms."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2564
    .end local v12    # "threadTimeEnd":J
    .end local v14    # "threadTimeStart":J
    .end local v16    # "wallTimeEnd":J
    .end local v18    # "wallTimeStart":J
    .end local v20    # "watchDog":Ljava/lang/Thread;
    .end local v21    # "watchDogSync":Ljava/lang/Object;
    .restart local v8    # "picturesize":Landroid/hardware/Camera$Size;
    .restart local v9    # "previewsize":Landroid/hardware/Camera$Size;
    .restart local v10    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v22, v0

    iget v0, v9, Landroid/hardware/Camera$Size;->width:I

    move/from16 v23, v0

    iget v0, v9, Landroid/hardware/Camera$Size;->height:I

    move/from16 v24, v0

    invoke-virtual/range {v22 .. v24}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    goto/16 :goto_1

    .line 2593
    .end local v8    # "picturesize":Landroid/hardware/Camera$Size;
    .end local v9    # "previewsize":Landroid/hardware/Camera$Size;
    .end local v10    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v22, v0

    const-string v23, "chk_dataline"

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    goto/16 :goto_2

    .line 2600
    :catch_0
    move-exception v6

    .line 2601
    .local v6, "e":Ljava/lang/RuntimeException;
    const-string v22, "FactoryCamera"

    const-string v23, "setParameter fail"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2602
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    const/16 v23, 0x9

    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 2611
    .end local v6    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v7

    .line 2612
    .local v7, "exception":Ljava/io/IOException;
    const-string v22, "FactoryCamera"

    const-string v23, "mCameraDevice.setPreviewDisplay exception!!!"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2613
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/hardware/Camera;->release()V

    .line 2614
    const/16 v22, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    goto/16 :goto_0

    .line 2681
    .end local v7    # "exception":Ljava/io/IOException;
    .restart local v14    # "threadTimeStart":J
    .restart local v18    # "wallTimeStart":J
    .restart local v20    # "watchDog":Ljava/lang/Thread;
    .restart local v21    # "watchDogSync":Ljava/lang/Object;
    :catch_2
    move-exception v6

    .line 2685
    .local v6, "e":Ljava/lang/Throwable;
    const-string v22, "FactoryCamera"

    const-string v23, "exception while startPreview"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2686
    const v22, 0x7f0a000a

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/sec/android/app/camera/Camera;->dialogErrorPopup(I)V

    goto/16 :goto_0

    .line 2707
    .end local v6    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v22

    :try_start_4
    monitor-exit v21
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v22

    .line 2598
    .end local v14    # "threadTimeStart":J
    .end local v18    # "wallTimeStart":J
    .end local v20    # "watchDog":Ljava/lang/Thread;
    .end local v21    # "watchDogSync":Ljava/lang/Object;
    :catch_3
    move-exception v22

    goto/16 :goto_3
.end method

.method private startCamera()V
    .locals 2

    .prologue
    .line 1571
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/camera/Camera$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camera/Camera$4;-><init>(Lcom/sec/android/app/camera/Camera;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mOpenCameraThread:Ljava/lang/Thread;

    .line 1632
    return-void
.end method

.method private stopPreview()V
    .locals 2

    .prologue
    .line 1421
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    if-eqz v0, :cond_0

    .line 1422
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 1424
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    .line 1425
    const-string v0, "FactoryCamera"

    const-string v1, "stopPreview : mPreviewing set false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1426
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->clearFocusState()V

    .line 1428
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_1

    .line 1429
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 1430
    :cond_1
    return-void
.end method

.method private updateFocusIndicator()V
    .locals 2

    .prologue
    .line 2758
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusRectangle:Lcom/sec/android/app/camera/framework/FocusRectangle;

    if-nez v0, :cond_0

    .line 2771
    :goto_0
    return-void

    .line 2761
    :cond_0
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 2763
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusRectangle:Lcom/sec/android/app/camera/framework/FocusRectangle;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/framework/FocusRectangle;->showStart()V

    goto :goto_0

    .line 2764
    :cond_2
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 2765
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusRectangle:Lcom/sec/android/app/camera/framework/FocusRectangle;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/framework/FocusRectangle;->showSuccess()V

    goto :goto_0

    .line 2766
    :cond_3
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 2767
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusRectangle:Lcom/sec/android/app/camera/framework/FocusRectangle;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/framework/FocusRectangle;->showFail()V

    goto :goto_0

    .line 2769
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusRectangle:Lcom/sec/android/app/camera/framework/FocusRectangle;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/framework/FocusRectangle;->clear()V

    goto :goto_0
.end method

.method private writeImage(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 10
    .param p1, "directory"    # Ljava/lang/String;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "data"    # [B

    .prologue
    .line 825
    const-string v6, "FactoryCamera"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "writeImage : directory = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " data size "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, p3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    const/4 v3, 0x0

    .line 827
    .local v3, "outputStream":Ljava/io/OutputStream;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/camera/Camera;->filePath:Ljava/lang/String;

    .line 830
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 831
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 832
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 833
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 834
    .local v5, "redir":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 836
    .end local v5    # "redir":Ljava/io/File;
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_1

    .line 837
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 838
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    .local v2, "file":Ljava/io/File;
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 840
    .end local v3    # "outputStream":Ljava/io/OutputStream;
    .local v4, "outputStream":Ljava/io/OutputStream;
    :try_start_1
    invoke-virtual {v4, p3}, Ljava/io/OutputStream;->write([B)V

    .line 841
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 843
    const-string v6, "FactoryCamera"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "writeImage : file.length = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 850
    :try_start_2
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 855
    .end local v0    # "dir":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v3    # "outputStream":Ljava/io/OutputStream;
    :goto_0
    return-void

    .line 851
    .end local v3    # "outputStream":Ljava/io/OutputStream;
    .restart local v0    # "dir":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    :catch_0
    move-exception v6

    move-object v3, v4

    .line 854
    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v3    # "outputStream":Ljava/io/OutputStream;
    goto :goto_0

    .line 844
    .end local v0    # "dir":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    :catch_1
    move-exception v1

    .line 845
    .local v1, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    const-string v6, "FactoryCamera"

    invoke-static {v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 850
    :try_start_4
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 851
    :catch_2
    move-exception v6

    goto :goto_0

    .line 846
    .end local v1    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v1

    .line 847
    .local v1, "ex":Ljava/io/IOException;
    :goto_2
    :try_start_5
    const-string v6, "FactoryCamera"

    invoke-static {v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 850
    :try_start_6
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 851
    :catch_4
    move-exception v6

    goto :goto_0

    .line 849
    .end local v1    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 850
    :goto_3
    :try_start_7
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 853
    :goto_4
    throw v6

    .line 851
    :catch_5
    move-exception v7

    goto :goto_4

    .line 849
    .end local v3    # "outputStream":Ljava/io/OutputStream;
    .restart local v0    # "dir":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v3    # "outputStream":Ljava/io/OutputStream;
    goto :goto_3

    .line 846
    .end local v3    # "outputStream":Ljava/io/OutputStream;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    :catch_6
    move-exception v1

    move-object v3, v4

    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v3    # "outputStream":Ljava/io/OutputStream;
    goto :goto_2

    .line 844
    .end local v3    # "outputStream":Ljava/io/OutputStream;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    :catch_7
    move-exception v1

    move-object v3, v4

    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v3    # "outputStream":Ljava/io/OutputStream;
    goto :goto_1
.end method


# virtual methods
.method public acquireDVFS(I)V
    .locals 5
    .param p1, "milisecond"    # I

    .prologue
    const/4 v4, 0x0

    .line 3176
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-nez v0, :cond_0

    .line 3177
    new-instance v0, Landroid/os/DVFSHelper;

    const/16 v1, 0xc

    invoke-direct {v0, p0, v1}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 3178
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->supportedCPUFreqTable:[I

    .line 3179
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->supportedCPUCoreTable:[I

    .line 3181
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->supportedCPUFreqTable:[I

    if-eqz v0, :cond_2

    .line 3182
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mDVFSHelper:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->supportedCPUFreqTable:[I

    aget v2, v2, v4

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 3183
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "supportedCPUFreqTable is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->supportedCPUFreqTable:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3188
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->supportedCPUCoreTable:[I

    if-eqz v0, :cond_3

    .line 3189
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mDVFSHelper:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->supportedCPUCoreTable:[I

    aget v2, v2, v4

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 3190
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "supportedCPUCoreTable is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->supportedCPUCoreTable:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3196
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    .line 3197
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0, p1}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 3198
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDVFSHelper.acquire : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3200
    :cond_1
    return-void

    .line 3185
    :cond_2
    const-string v0, "FactoryCamera"

    const-string v1, "supportedCPUFreqTable is null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3192
    :cond_3
    const-string v0, "FactoryCamera"

    const-string v1, "supportedCPUFreqTable is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public addImage(Landroid/content/ContentResolver;Ljava/lang/String;J[BI)Landroid/net/Uri;
    .locals 7
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "date"    # J
    .param p5, "jpeg"    # [B
    .param p6, "orientation"    # I

    .prologue
    .line 736
    const-string v3, "FactoryCamera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addImage title : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x6

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 739
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "title"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    const-string v3, "_display_name"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    const-string v3, "datetaken"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 742
    const-string v3, "mime_type"

    const-string v4, "image/jpeg"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    const-string v3, "_size"

    array-length v4, p5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 744
    const-string v3, "_data"

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->filePath:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    const-string v3, "orientation"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 747
    const/4 v1, 0x0

    .line 749
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p1, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 753
    :goto_0
    const-string v3, "FactoryCamera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addImage uri:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 755
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.camera.NEW_PICTURE"

    invoke-direct {v3, v4, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/camera/Camera;->sendBroadcast(Landroid/content/Intent;)V

    .line 757
    return-object v1

    .line 750
    :catch_0
    move-exception v0

    .line 751
    .local v0, "th":Ljava/lang/Throwable;
    const-string v3, "FactoryCamera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to write MediaStore"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected calculateOrientationForPicture(I)I
    .locals 4
    .param p1, "orientation"    # I

    .prologue
    .line 3090
    const/4 v1, 0x0

    .line 3092
    .local v1, "rotation":I
    const/4 v2, -0x1

    if-eq p1, v2, :cond_0

    .line 3093
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    aget-object v0, v2, v3

    .line 3094
    .local v0, "info":Landroid/hardware/Camera$CameraInfo;
    iget v2, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 3095
    iget v2, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v2, p1

    add-int/lit16 v2, v2, 0x168

    rem-int/lit16 v1, v2, 0x168

    .line 3101
    .end local v0    # "info":Landroid/hardware/Camera$CameraInfo;
    :cond_0
    :goto_0
    return v1

    .line 3097
    .restart local v0    # "info":Landroid/hardware/Camera$CameraInfo;
    :cond_1
    iget v2, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v2, p1

    rem-int/lit16 v1, v2, 0x168

    goto :goto_0
.end method

.method public checkCameraLatestModule()V
    .locals 10

    .prologue
    .line 3356
    const/4 v3, 0x0

    .line 3357
    .local v3, "fr":Ljava/io/FileReader;
    const/4 v0, 0x0

    .line 3359
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    const-string v6, "/sys/class/camera/rear/rear_latest_module_check"

    .line 3360
    .local v6, "sysFsPath":Ljava/lang/String;
    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3361
    .end local v3    # "fr":Ljava/io/FileReader;
    .local v4, "fr":Ljava/io/FileReader;
    if-eqz v4, :cond_1

    .line 3362
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3363
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 3364
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 3366
    .local v5, "mCheckLatestModule":Ljava/lang/String;
    const-string v7, "FactoryCamera"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CheckLatestModule: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3368
    const-string v7, "NG"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 3369
    const v7, 0x7f0a0025

    const/4 v8, 0x2

    invoke-static {p0, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .end local v5    # "mCheckLatestModule":Ljava/lang/String;
    :cond_0
    move-object v0, v1

    .line 3378
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :cond_1
    if-eqz v4, :cond_2

    .line 3379
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 3380
    :cond_2
    if-eqz v0, :cond_3

    .line 3381
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :cond_3
    move-object v3, v4

    .line 3386
    .end local v4    # "fr":Ljava/io/FileReader;
    .end local v6    # "sysFsPath":Ljava/lang/String;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :cond_4
    :goto_0
    return-void

    .line 3382
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    .restart local v6    # "sysFsPath":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 3383
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move-object v3, v4

    .line 3385
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_0

    .line 3374
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v6    # "sysFsPath":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 3375
    .restart local v2    # "e":Ljava/lang/Exception;
    :goto_1
    :try_start_4
    const-string v7, "FactoryCamera"

    const-string v8, "failed to read the latest module info"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3378
    if-eqz v3, :cond_5

    .line 3379
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 3380
    :cond_5
    if-eqz v0, :cond_4

    .line 3381
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 3382
    :catch_2
    move-exception v2

    .line 3383
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 3377
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    .line 3378
    :goto_2
    if-eqz v3, :cond_6

    .line 3379
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 3380
    :cond_6
    if-eqz v0, :cond_7

    .line 3381
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 3384
    :cond_7
    :goto_3
    throw v7

    .line 3382
    :catch_3
    move-exception v2

    .line 3383
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 3377
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    .restart local v6    # "sysFsPath":Ljava/lang/String;
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .line 3374
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_1

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_5
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_1
.end method

.method public dialogAFFailPopup()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 3251
    const-string v2, "FactoryCamera"

    const-string v3, "dialogAFFailPopup..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3252
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3253
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mStartCheck:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3254
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mDatalineCheck:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3256
    const-string v2, "FactoryCamera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dialogAFFailPopup() : testType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3257
    const-string v2, "FactoryCamera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dialogAFFailPopup() : bIsUsbOrUartCommand : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3258
    const-string v2, "autotest"

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    if-eqz v2, :cond_2

    .line 3259
    const-string v2, "com.android.samsungtest.CAMERA_SHOT_ACK"

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/camera/Camera;->sendBroadCastAck(Ljava/lang/String;I)V

    .line 3281
    :cond_1
    :goto_0
    return-void

    .line 3261
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 3262
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.factory.CAMERA_AF_FAIL"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3263
    const-string v2, "AF_STATUS"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3264
    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/Camera;->sendBroadcast(Landroid/content/Intent;)V

    .line 3266
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 3267
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0a000d

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 3268
    const v2, 0x7f0a0007

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 3269
    const v2, 0x7f0a0001

    new-instance v3, Lcom/sec/android/app/camera/Camera$11;

    invoke-direct {v3, p0}, Lcom/sec/android/app/camera/Camera$11;-><init>(Lcom/sec/android/app/camera/Camera;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3276
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 3277
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    .line 3278
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public displayLowLightCaptureImage()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 3121
    const-string v1, "FactoryCamera"

    const-string v2, "displayLowLightCaptureImage..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3122
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/camera/PostViewTest;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3123
    .local v0, "intent":Landroid/content/Intent;
    iget v1, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v1, :cond_0

    .line 3124
    const-string v1, "low_light_capture_display"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3130
    :goto_0
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/camera/Camera;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3131
    return-void

    .line 3125
    :cond_0
    iget v1, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-ne v1, v3, :cond_1

    .line 3126
    const-string v1, "low_light_capture_display"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 3128
    :cond_1
    const-string v1, "low_light_capture_display"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public findOptimalPreviewSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;
    .locals 16
    .param p2, "targetRatio"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;D)",
            "Landroid/hardware/Camera$Size;"
        }
    .end annotation

    .prologue
    .line 2807
    .local p1, "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    const-wide v0, 0x3f50624dd2f1a9fcL    # 0.001

    .line 2808
    .local v0, "ASPECT_TOLERANCE":D
    if-nez p1, :cond_1

    .line 2809
    const/4 v6, 0x0

    .line 2855
    :cond_0
    return-object v6

    .line 2811
    :cond_1
    const/4 v6, 0x0

    .line 2812
    .local v6, "optimalSize":Landroid/hardware/Camera$Size;
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 2820
    .local v4, "minDiff":D
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/Camera;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "window"

    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/WindowManager;

    invoke-interface {v11}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 2822
    .local v2, "display":Landroid/view/Display;
    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v11

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v12

    invoke-static {v11, v12}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 2824
    .local v10, "targetHeight":I
    const-string v11, "FactoryCamera"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "display.getHeight() = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " display.getWidth() = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2827
    if-gtz v10, :cond_2

    .line 2829
    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v10

    .line 2833
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/hardware/Camera$Size;

    .line 2834
    .local v7, "size":Landroid/hardware/Camera$Size;
    iget v11, v7, Landroid/hardware/Camera$Size;->width:I

    int-to-double v12, v11

    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    int-to-double v14, v11

    div-double v8, v12, v14

    .line 2835
    .local v8, "ratio":D
    sub-double v12, v8, p2

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    const-wide v14, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v11, v12, v14

    if-gtz v11, :cond_3

    .line 2837
    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v12, v11

    cmpg-double v11, v12, v4

    if-gez v11, :cond_3

    .line 2838
    move-object v6, v7

    .line 2839
    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v4, v11

    goto :goto_0

    .line 2845
    .end local v7    # "size":Landroid/hardware/Camera$Size;
    .end local v8    # "ratio":D
    :cond_4
    if-nez v6, :cond_0

    .line 2846
    const-string v11, "FactoryCamera"

    const-string v12, "No preview size match the aspect ratio"

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2847
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 2848
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/hardware/Camera$Size;

    .line 2849
    .restart local v7    # "size":Landroid/hardware/Camera$Size;
    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v12, v11

    cmpg-double v11, v12, v4

    if-gez v11, :cond_5

    .line 2850
    move-object v6, v7

    .line 2851
    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v4, v11

    goto :goto_1
.end method

.method public finishAutoCaptureTest()V
    .locals 4

    .prologue
    .line 3284
    const-string v1, "FactoryCamera"

    const-string v2, "finishAutoCaptureTest..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3285
    invoke-static {}, Lcom/sec/android/app/camera/CameraStorage;->getInstance()Lcom/sec/android/app/camera/CameraStorage;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;

    .line 3286
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 3287
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "data_filepath"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camera/CameraStorage;->getFilePath(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3288
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/camera/Camera;->setResult(ILandroid/content/Intent;)V

    .line 3289
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraStorage;->clearFilePath()V

    .line 3290
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->finish()V

    .line 3291
    return-void
.end method

.method public finishPostViewTest()V
    .locals 4

    .prologue
    .line 3135
    invoke-static {}, Lcom/sec/android/app/camera/CameraStorage;->getInstance()Lcom/sec/android/app/camera/CameraStorage;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;

    .line 3136
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 3137
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_POSTVIEW_TEST:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_FRONT:Z

    if-eqz v1, :cond_0

    .line 3138
    const-string v1, "data_filepath"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camera/CameraStorage;->getFilePath(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3142
    :goto_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/camera/Camera;->setResult(ILandroid/content/Intent;)V

    .line 3143
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CameraStorage;->clearFilePath()V

    .line 3144
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->finish()V

    .line 3145
    return-void

    .line 3140
    :cond_0
    const-string v1, "data_filepath"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->cs:Lcom/sec/android/app/camera/CameraStorage;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camera/CameraStorage;->getFilePath(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraSettings:Lcom/sec/android/app/camera/framework/CameraSettings;

    return-object v0
.end method

.method public getLastOrientation()I
    .locals 1

    .prologue
    .line 3078
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mLastOrientation:I

    return v0
.end method

.method public getNumberOfCameras()I
    .locals 1

    .prologue
    .line 3105
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mNumberOfCameras:I

    return v0
.end method

.method public getOrientationOnTake()I
    .locals 1

    .prologue
    .line 3086
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mOrientationOnTake:I

    return v0
.end method

.method public initialize()V
    .locals 8

    .prologue
    const/16 v7, 0x55

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1710
    const-string v2, "FactoryCamera"

    const-string v3, "initialize()..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1711
    iput-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->bSentAck:Z

    .line 1712
    iput-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->mStopCamera:Z

    .line 1715
    iput-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->mShutterBtnlock:Z

    .line 1717
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/Camera;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 1718
    .local v1, "pm":Landroid/os/PowerManager;
    const v2, 0x3000001a

    const-string v3, "FactoryCamera"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1720
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1724
    iput-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->mPausing:Z

    .line 1733
    sget-object v2, Lcom/sec/android/app/camera/Camera;->mContentResolver:Landroid/content/ContentResolver;

    if-nez v2, :cond_0

    .line 1734
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/camera/Camera;->mContentResolver:Landroid/content/ContentResolver;

    .line 1736
    :cond_0
    new-instance v2, Lcom/sec/android/app/camera/Camera$ImageCapture;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camera/Camera$ImageCapture;-><init>(Lcom/sec/android/app/camera/Camera;)V

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mImageCapture:Lcom/sec/android/app/camera/Camera$ImageCapture;

    .line 1738
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->ensureCameraDevice()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1739
    const-string v2, "FactoryCamera"

    const-string v3, "initialize() - ensureCameraDevice is failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1803
    :goto_0
    return-void

    .line 1743
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 1745
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/framework/CameraSettings;->hasFlash()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->isLowLightCaptureTesting()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1746
    iget v2, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v2, :cond_7

    .line 1747
    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    if-ne v2, v6, :cond_5

    .line 1748
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "flash-mode"

    const-string v4, "torch"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1749
    const-string v2, "FactoryCamera"

    const-string v3, "flash-mode is FLASH_TORCH"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1762
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2, v5}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    .line 1763
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->NOT_SUPPORT_AUTO_ANTIBANDING:Z

    if-nez v2, :cond_3

    .line 1764
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "auto"

    invoke-virtual {v2, v3}, Landroid/hardware/Camera$Parameters;->setAntibanding(Ljava/lang/String;)V

    .line 1766
    :cond_3
    const-string v2, "selftest"

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-ne v2, v6, :cond_8

    .line 1767
    const-string v2, "FactoryCamera"

    const-string v3, "set VT Camera quality..."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1768
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const/16 v3, 0x60

    invoke-virtual {v2, v3}, Landroid/hardware/Camera$Parameters;->setJpegQuality(I)V

    .line 1775
    :goto_2
    const-string v2, "continuous-picture"

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/camera/Camera;->isSupportedFocusModes(Ljava/lang/String;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1776
    const-string v2, "FactoryCamera"

    const-string v3, "Focus - Camera SupportedFocusModes() : set continuous-picture mode (CAF ON)"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1777
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "continuous-picture"

    invoke-virtual {v2, v3}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 1782
    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->SUPPORT_PHASE_AF:Z

    if-eqz v2, :cond_4

    .line 1783
    const-string v2, "FactoryCamera"

    const-string v3, "Set phase-af on"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1784
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "phase-af"

    const-string v4, "on"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1787
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 1789
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->DELAYED_START_PREVIEW:Z

    if-eqz v2, :cond_a

    .line 1790
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mTimerHandler:Landroid/os/Handler;

    const/16 v3, 0xb

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1794
    :goto_4
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->updateFocusIndicator()V

    .line 1797
    :try_start_0
    new-instance v2, Landroid/media/ToneGenerator;

    const/4 v3, 0x1

    const/16 v4, 0x55

    invoke-direct {v2, v3, v4}, Landroid/media/ToneGenerator;-><init>(II)V

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mFocusToneGenerator:Landroid/media/ToneGenerator;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1799
    :catch_0
    move-exception v0

    .line 1800
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v2, "FactoryCamera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception caught while creating local tone generator: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1801
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mFocusToneGenerator:Landroid/media/ToneGenerator;

    goto/16 :goto_0

    .line 1750
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_5
    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    .line 1751
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "flash-mode"

    const-string v4, "off"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1752
    const-string v2, "FactoryCamera"

    const-string v3, "flash-mode is FLASH_OFF"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1754
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "flash-mode"

    const-string v4, "on"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1755
    const-string v2, "FactoryCamera"

    const-string v3, "flash-mode is FLASH_ON"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1758
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "flash-mode"

    const-string v4, "off"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1759
    const-string v2, "FactoryCamera"

    const-string v3, "flash-mode is FLASH_OFF"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1770
    :cond_8
    const-string v2, "FactoryCamera"

    const-string v3, "set Factory Camera quality..."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1771
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2, v7}, Landroid/hardware/Camera$Parameters;->setJpegQuality(I)V

    goto/16 :goto_2

    .line 1779
    :cond_9
    const-string v2, "FactoryCamera"

    const-string v3, "Focus - not support CAF focusmode"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1792
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->restartPreview()V

    goto/16 :goto_4
.end method

.method public isCameraFirmwareGood()Z
    .locals 14

    .prologue
    const v13, 0x7f0a0024

    const/4 v12, 0x1

    .line 3389
    const-string v9, "FactoryCamera"

    const-string v10, "isCameraFirmwareGood..."

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3390
    const/4 v7, 0x0

    .line 3391
    .local v7, "mFWInfo":Ljava/lang/String;
    const/4 v5, 0x0

    .line 3392
    .local v5, "fr":Ljava/io/FileReader;
    const/4 v1, 0x0

    .line 3393
    .local v1, "br":Ljava/io/BufferedReader;
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 3396
    .local v0, "IsOldPath":Ljava/lang/Boolean;
    :try_start_0
    const-string v8, "/sys/class/camera/rear/rear_checkfw_factory"

    .line 3397
    .local v8, "sysFsPath":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3399
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_0

    .line 3400
    const-string v8, "/sys/class/camera/rear/rear_camfw"

    .line 3401
    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 3404
    :cond_0
    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, v8}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3405
    .end local v5    # "fr":Ljava/io/FileReader;
    .local v6, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3406
    .end local v1    # "br":Ljava/io/BufferedReader;
    .local v2, "br":Ljava/io/BufferedReader;
    :try_start_2
    const-string v9, "FactoryCamera"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Camera.isCameraFirmwareGood() fr : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3407
    if-eqz v6, :cond_1

    if-eqz v2, :cond_1

    .line 3408
    const-string v9, "FactoryCamera"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Camera.isCameraFirmwareGood() br : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3409
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    .line 3410
    const-string v9, "FactoryCamera"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Camera.isCameraFirmwareGood - FW info["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3413
    :cond_1
    if-eqz v6, :cond_2

    .line 3414
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V

    .line 3417
    :cond_2
    if-eqz v2, :cond_3

    .line 3418
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 3424
    :cond_3
    if-eqz v6, :cond_4

    .line 3425
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V

    .line 3428
    :cond_4
    if-eqz v2, :cond_5

    .line 3429
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_5
    move-object v1, v2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    move-object v5, v6

    .line 3437
    .end local v4    # "file":Ljava/io/File;
    .end local v6    # "fr":Ljava/io/FileReader;
    .end local v8    # "sysFsPath":Ljava/lang/String;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :cond_6
    :goto_0
    if-eqz v7, :cond_7

    .line 3438
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_c

    .line 3439
    const-string v9, "NG_"

    invoke-virtual {v7, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 3440
    invoke-direct {p0, v13}, Lcom/sec/android/app/camera/Camera;->dialogNGPopup(I)V

    .line 3454
    :cond_7
    :goto_1
    return v12

    .line 3431
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "file":Ljava/io/File;
    .restart local v6    # "fr":Ljava/io/FileReader;
    .restart local v8    # "sysFsPath":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 3432
    .local v3, "e":Ljava/io/IOException;
    const-string v9, "FactoryCamera"

    const-string v10, "Camera.isCameraFirmwareGood() exception!!"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    move-object v5, v6

    .line 3435
    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_0

    .line 3420
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "file":Ljava/io/File;
    .end local v8    # "sysFsPath":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 3421
    .local v3, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    const-string v9, "FactoryCamera"

    const-string v10, "Camera.isCameraFirmwareGood Read FW info fail"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3424
    if-eqz v5, :cond_8

    .line 3425
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 3428
    :cond_8
    if-eqz v1, :cond_6

    .line 3429
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 3431
    :catch_2
    move-exception v3

    .line 3432
    .local v3, "e":Ljava/io/IOException;
    const-string v9, "FactoryCamera"

    const-string v10, "Camera.isCameraFirmwareGood() exception!!"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3423
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    .line 3424
    :goto_3
    if-eqz v5, :cond_9

    .line 3425
    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 3428
    :cond_9
    if-eqz v1, :cond_a

    .line 3429
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 3434
    :cond_a
    :goto_4
    throw v9

    .line 3431
    :catch_3
    move-exception v3

    .line 3432
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryCamera"

    const-string v11, "Camera.isCameraFirmwareGood() exception!!"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 3442
    .end local v3    # "e":Ljava/io/IOException;
    :cond_b
    const-string v9, "FactoryCamera"

    const-string v10, "Camera.isCameraFirmwareGood() Ok."

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 3446
    :cond_c
    const-string v9, "NG"

    invoke-virtual {v7, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 3447
    invoke-direct {p0, v13}, Lcom/sec/android/app/camera/Camera;->dialogNGPopup(I)V

    goto :goto_1

    .line 3449
    :cond_d
    const-string v9, "FactoryCamera"

    const-string v10, "Camera.isCameraFirmwareGood() Ok.."

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 3423
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "file":Ljava/io/File;
    .restart local v6    # "fr":Ljava/io/FileReader;
    .restart local v8    # "sysFsPath":Ljava/lang/String;
    :catchall_1
    move-exception v9

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v9

    move-object v1, v2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .line 3420
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_4
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_5
    move-exception v3

    move-object v1, v2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_2
.end method

.method public isLowLightCaptureTesting()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3158
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Camera.isLowLightCaptureTesting() cameraType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", CHECK_LOW_LIGHT_CAPTURE_TEST : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mLowLightCaptureTestSkip : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureTestSkip:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3161
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    if-eqz v1, :cond_3

    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureTestSkip:Z

    if-nez v1, :cond_3

    .line 3162
    iget-boolean v1, p0, Lcom/sec/android/app/camera/Camera;->mCameraAutoCapture:Z

    if-nez v1, :cond_1

    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3163
    :cond_1
    const-string v1, "FactoryCamera"

    const-string v2, "isLowLightCaptureTesting false!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3171
    :goto_0
    return v0

    .line 3166
    :cond_2
    const-string v0, "FactoryCamera"

    const-string v1, "isLowLightCaptureTesting true!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3167
    const/4 v0, 0x1

    goto :goto_0

    .line 3170
    :cond_3
    const-string v1, "FactoryCamera"

    const-string v2, "isLowLightCaptureTesting false..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isSupportedFocusModes(Ljava/lang/String;Ljava/util/List;)Z
    .locals 1
    .param p1, "checkfocusmode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1810
    .local p2, "focusmodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1, p2}, Lcom/sec/android/app/camera/Camera;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1811
    const/4 v0, 0x1

    .line 1813
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 10
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const v9, 0x7f0a000a

    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 2900
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2901
    const-string v3, "FactoryCamera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Camera.onActivityResult() requestCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", resultCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", intent : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2902
    packed-switch p1, :pswitch_data_0

    .line 3052
    :cond_0
    :goto_0
    return-void

    .line 2905
    :pswitch_0
    if-ne p2, v7, :cond_2

    .line 2921
    const-string v3, "camera_id"

    invoke-virtual {p3, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    .line 2923
    const-string v3, "ommision_test"

    invoke-virtual {p3, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->ommisionTest:Z

    .line 2926
    const-string v3, "FactoryCamera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onActivityResult() : testType : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2927
    const-string v3, "autotest"

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2928
    const-string v3, "torch_on"

    invoke-virtual {p3, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    .line 2932
    :cond_1
    const-string v3, "camcorder_preview_test"

    invoke-virtual {p3, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTest:Z

    .line 2936
    iput-boolean v6, p0, Lcom/sec/android/app/camera/Camera;->bIsUsbOrUartCommand:Z

    .line 2938
    const-string v3, "FactoryCamera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onActivityResult : cameraType["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] ommisionTest["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->ommisionTest:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] mFlashEnable["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mFinishCamcorderPreviewTest["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTest:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2942
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->initDTPsetting()V

    .line 2943
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->setStoragepath()V

    .line 2944
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->startCamera()V

    .line 2945
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->initLayoutSetting()V

    .line 2947
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->initialize()V

    goto/16 :goto_0

    .line 2948
    :cond_2
    if-nez p2, :cond_7

    .line 2949
    const/4 v2, 0x0

    .line 2950
    .local v2, "strCameraFail":Ljava/lang/String;
    if-eqz p3, :cond_3

    .line 2951
    const-string v3, "camcorder_preview_test_backkey_canceled"

    invoke-virtual {p3, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTestByBackKey:Z

    .line 2954
    const-string v3, "CAMERA FAIL"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2957
    :cond_3
    const-string v3, "FactoryCamera"

    const-string v4, "onActivityResult() : RESULT_CANCELED"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2958
    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTestByBackKey:Z

    if-eq v3, v8, :cond_4

    if-eqz v2, :cond_6

    .line 2960
    :cond_4
    const-string v3, "FactoryCamera"

    const-string v4, "Activity.RESULT_CANCELED is caused by Back key or user mark fail"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2967
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2968
    .local v1, "intents":Landroid/content/Intent;
    const-string v3, "CAMERA FAIL"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2969
    invoke-virtual {p0, v6, v1}, Lcom/sec/android/app/camera/Camera;->setResult(ILandroid/content/Intent;)V

    .line 2970
    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTestByBackKey:Z

    if-eq v3, v8, :cond_5

    if-eqz v2, :cond_0

    .line 2972
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->finish()V

    goto/16 :goto_0

    .line 2962
    .end local v1    # "intents":Landroid/content/Intent;
    :cond_6
    const-string v3, "FactoryCamera"

    const-string v4, "Activity.RESULT_CANCELED is not caused by Back key"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2963
    invoke-direct {p0, v9}, Lcom/sec/android/app/camera/Camera;->dialogErrorPopup(I)V

    .line 2964
    iput-boolean v8, p0, Lcom/sec/android/app/camera/Camera;->bIsCamcorderPreviewFail:Z

    goto :goto_1

    .line 2975
    .end local v2    # "strCameraFail":Ljava/lang/String;
    :cond_7
    invoke-direct {p0, v9}, Lcom/sec/android/app/camera/Camera;->dialogErrorPopup(I)V

    goto/16 :goto_0

    .line 2979
    :pswitch_1
    if-ne p2, v7, :cond_9

    .line 2980
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_FRONT:Z

    if-eqz v3, :cond_8

    .line 2981
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->finish()V

    goto/16 :goto_0

    .line 2983
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->finishPostViewTest()V

    goto/16 :goto_0

    .line 2985
    :cond_9
    if-nez p2, :cond_0

    if-eqz p3, :cond_0

    .line 2986
    const-string v3, "CAMERA FAIL"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2987
    .restart local v2    # "strCameraFail":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 2989
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2990
    .restart local v1    # "intents":Landroid/content/Intent;
    const-string v3, "CAMERA FAIL"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2991
    invoke-virtual {p0, v6, v1}, Lcom/sec/android/app/camera/Camera;->setResult(ILandroid/content/Intent;)V

    .line 2992
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->finish()V

    goto/16 :goto_0

    .line 2997
    .end local v1    # "intents":Landroid/content/Intent;
    .end local v2    # "strCameraFail":Ljava/lang/String;
    :pswitch_2
    if-ne p2, v7, :cond_a

    .line 2998
    const-string v3, "camera_id"

    invoke-virtual {p3, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    .line 3000
    const-string v3, "ommision_test"

    invoke-virtual {p3, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->ommisionTest:Z

    .line 3002
    const-string v3, "torch_on"

    invoke-virtual {p3, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    .line 3004
    const-string v3, "ois_test"

    invoke-virtual {p3, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mFinishOISTest:Z

    .line 3008
    const-string v3, "FactoryCamera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onActivityResult : cameraType["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] ommisionTest["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->ommisionTest:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] mFlashEnable["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mFinishOISTest["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->mFinishOISTest:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3012
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->initDTPsetting()V

    .line 3013
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->setStoragepath()V

    .line 3014
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->startCamera()V

    .line 3015
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->initLayoutSetting()V

    .line 3017
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->initialize()V

    goto/16 :goto_0

    .line 3019
    :cond_a
    if-nez p2, :cond_f

    .line 3020
    const/4 v0, -0x1

    .line 3021
    .local v0, "intOISFail":I
    if-eqz p3, :cond_b

    .line 3022
    const-string v3, "ois_test_backkey_canceled"

    invoke-virtual {p3, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mFinishOISTestByBackKey:Z

    .line 3025
    const-string v3, "OIS FAIL"

    invoke-virtual {p3, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 3028
    :cond_b
    const-string v3, "FactoryCamera"

    const-string v4, "onActivityResult() : RESULT_CANCELED"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3029
    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mFinishOISTestByBackKey:Z

    if-eq v3, v8, :cond_c

    if-eq v0, v7, :cond_e

    .line 3032
    :cond_c
    const-string v3, "FactoryCamera"

    const-string v4, "Activity.RESULT_CANCELED is caused by Back key"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3039
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 3040
    .restart local v1    # "intents":Landroid/content/Intent;
    const-string v3, "OIS FAIL"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3041
    invoke-virtual {p0, v6, v1}, Lcom/sec/android/app/camera/Camera;->setResult(ILandroid/content/Intent;)V

    .line 3042
    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mFinishOISTestByBackKey:Z

    if-eq v3, v8, :cond_d

    if-eq v0, v7, :cond_0

    .line 3044
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->finish()V

    goto/16 :goto_0

    .line 3034
    .end local v1    # "intents":Landroid/content/Intent;
    :cond_e
    const-string v3, "FactoryCamera"

    const-string v4, "Activity.RESULT_CANCELED is not caused by Back key"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3036
    iput-boolean v8, p0, Lcom/sec/android/app/camera/Camera;->bIsOISFail:Z

    goto :goto_2

    .line 3047
    .end local v0    # "intOISFail":I
    :cond_f
    invoke-direct {p0, v9}, Lcom/sec/android/app/camera/Camera;->dialogErrorPopup(I)V

    goto/16 :goto_0

    .line 2902
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onAutoCapture()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 3203
    const-string v0, "FactoryCamera"

    const-string v1, "call onAutoCapture..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3204
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mPausing:Z

    if-eqz v0, :cond_1

    .line 3248
    :cond_0
    :goto_0
    return-void

    .line 3208
    :cond_1
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    if-eq v0, v3, :cond_2

    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    if-ne v0, v3, :cond_3

    .line 3212
    :cond_2
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ignore onAutoCapture - mFocusState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3213
    iput v3, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    goto :goto_0

    .line 3217
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mIsCaptureEnable:Z

    if-nez v0, :cond_4

    .line 3218
    const-string v0, "FactoryCamera"

    const-string v1, "onAutoCapture mIsCaptureEnble is false"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3222
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mShutterBtnlock:Z

    if-eqz v0, :cond_5

    .line 3223
    const-string v0, "FactoryCamera"

    const-string v1, "onAutoCapture mShutterBtnlock is true"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3227
    :cond_5
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_9

    .line 3228
    :cond_6
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Camera.onAutoCapture() Supported AutoFocus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/framework/CameraSettings;->getSupportedAutofocus()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3229
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Camera.onAutoCapture() mLowLightCaptureTestSkip : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureTestSkip:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3230
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camera/framework/CameraSettings;->getSupportedAutofocus()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->isLowLightCaptureTesting()Z

    move-result v0

    if-nez v0, :cond_7

    .line 3231
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "auto"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 3232
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 3233
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->autoFocus()V

    .line 3234
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->doSnap()V

    goto/16 :goto_0

    .line 3235
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    if-eqz v0, :cond_8

    .line 3236
    const-string v0, "FactoryCamera"

    const-string v1, "onAutoCapture AF not supported or preview not started"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3238
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    invoke-interface {v0}, Lcom/sec/android/app/camera/Interface/Capturer;->onSnap()V

    goto/16 :goto_0

    .line 3240
    :cond_8
    const-string v0, "FactoryCamera"

    const-string v1, "onAutoCapture CaptureObj NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3243
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    if-eqz v0, :cond_0

    .line 3244
    const-string v0, "FactoryCamera"

    const-string v1, "onAutoCapture Front CAM onSnap()..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3245
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    invoke-interface {v0}, Lcom/sec/android/app/camera/Interface/Capturer;->onSnap()V

    goto/16 :goto_0
.end method

.method public onCheckDataLineDone()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1169
    const-string v2, "FactoryCamera"

    const-string v3, "onChkDataLineDone"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1171
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-nez v2, :cond_0

    .line 1172
    const-string v2, "FactoryCamera"

    const-string v3, "mCameraDevice is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    :goto_0
    return-void

    .line 1176
    :cond_0
    iput-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->bDoneDTP:Z

    .line 1178
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 1179
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->dump()V

    .line 1180
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "chk_dataline"

    const-string v4, "0"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/framework/CameraSettings;->hasFlash()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1183
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "flash-mode"

    const-string v4, "auto"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1186
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->stopPreview()V

    .line 1189
    :try_start_0
    const-string v2, "FactoryCamera"

    const-string v3, "mCameraDevice.setParameters(mParameters)"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1190
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1195
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/framework/CameraSettings;->needToCheckCamcorderPreviewTest()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTest:Z

    if-nez v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v2, :cond_2

    const-string v2, "autotest"

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1198
    const-string v2, "FactoryCamera"

    const-string v3, "finish onchkdataline. go to camcorder preview test"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1199
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1200
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "camera_id"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1201
    const-string v2, "picture-size"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1202
    const-string v2, "orientation_reverse"

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mIsOrientationReverse:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1203
    invoke-virtual {p0, v1, v5}, Lcom/sec/android/app/camera/Camera;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1207
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 1208
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camera/framework/CameraSettings;->hasFlash()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->isLowLightCaptureTesting()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1209
    iget v2, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v2, :cond_9

    .line 1210
    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    if-ne v2, v5, :cond_7

    .line 1211
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "flash-mode"

    const-string v4, "torch"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1212
    const-string v2, "FactoryCamera"

    const-string v3, "flash-mode is FLASH_TORCH"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    :cond_3
    :goto_2
    :try_start_1
    const-string v2, "FactoryCamera"

    const-string v3, "mCameraDevice.setParameters(mParameters)"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1228
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1234
    :goto_3
    :try_start_2
    const-string v2, "FactoryCamera"

    const-string v3, "mCameraDevice.startPreview()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1235
    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->bEnablePreviewCb:Z

    if-eqz v2, :cond_4

    .line 1236
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mPreviewCallback:Lcom/sec/android/app/camera/Camera$PreviewCallback;

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1239
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->startPreview()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 1249
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v2, :cond_5

    .line 1250
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v2}, Landroid/view/OrientationEventListener;->enable()V

    .line 1252
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camera/framework/CameraSettings;->needToCheckDTP(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1253
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->startTimerCaptureBlock()V

    .line 1255
    :cond_6
    iput-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    .line 1256
    const-string v2, "FactoryCamera"

    const-string v3, "onChkDataLineDone : mPreviewing set true"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1213
    :cond_7
    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    .line 1214
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "flash-mode"

    const-string v4, "off"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    const-string v2, "FactoryCamera"

    const-string v3, "flash-mode is FLASH_OFF"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1217
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "flash-mode"

    const-string v4, "on"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1218
    const-string v2, "FactoryCamera"

    const-string v3, "flash-mode is FLASH_ON"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1221
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v3, "flash-mode"

    const-string v4, "off"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1222
    const-string v2, "FactoryCamera"

    const-string v3, "flash-mode is FLASH_OFF"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1240
    :catch_0
    move-exception v0

    .line 1244
    .local v0, "e":Ljava/lang/Throwable;
    const-string v2, "FactoryCamera"

    const-string v3, "exception while startPreview"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1245
    const v2, 0x7f0a000a

    invoke-direct {p0, v2}, Lcom/sec/android/app/camera/Camera;->dialogErrorPopup(I)V

    goto/16 :goto_0

    .line 1229
    .end local v0    # "e":Ljava/lang/Throwable;
    :catch_1
    move-exception v2

    goto/16 :goto_3

    .line 1191
    :catch_2
    move-exception v2

    goto/16 :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1819
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 1821
    const-string v1, "FactoryCamera"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1824
    :try_start_0
    new-instance v1, Lcom/sec/android/app/camera/framework/CameraSettings;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camera/framework/CameraSettings;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/sec/android/app/camera/Camera;->mCameraSettings:Lcom/sec/android/app/camera/framework/CameraSettings;

    .line 1825
    invoke-static {}, Lcom/sec/android/app/camera/CameraApp;->getAppFeature()Lcom/sec/android/app/camera/Feature;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1830
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->getCameraInfo()V

    .line 1832
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/Camera;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 1833
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/16 v2, 0x1a

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/Camera;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 1834
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/16 v2, 0xbb

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/Camera;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 1836
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->getIntentInfo()V

    .line 1839
    iget v1, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v1, :cond_0

    .line 1840
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->isCameraFirmwareGood()Z

    .line 1843
    :cond_0
    const-string v1, "FactoryCamera"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate() mLowLightCaptureTestSkip cameraType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1844
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-eqz v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-ne v1, v4, :cond_4

    .line 1846
    :cond_2
    iput-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureTestSkip:Z

    .line 1851
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->initDTPsetting()V

    .line 1853
    const-string v1, "selftest"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1854
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->bDoneDTP:Z

    .line 1855
    iput-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->bCheckDTP:Z

    .line 1856
    iput-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->bEnablePreviewCb:Z

    .line 1870
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->setStoragepath()V

    .line 1873
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->startCamera()V

    .line 1875
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->initLayoutSetting()V

    .line 1886
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/Camera;->mWindowManager:Landroid/view/IWindowManager;

    .line 1887
    :goto_2
    return-void

    .line 1826
    :catch_0
    move-exception v0

    .line 1827
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1848
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->mLowLightCaptureTestSkip:Z

    goto :goto_1

    .line 1860
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/framework/CameraSettings;->needToCheckCamcorderPreviewTest()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/sec/android/app/camera/Camera;->bDoneDTP:Z

    if-ne v1, v4, :cond_3

    iget-boolean v1, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTest:Z

    if-nez v1, :cond_3

    iget v1, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    if-eq v1, v4, :cond_3

    iget v1, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v1, :cond_3

    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1865
    const-string v1, "FactoryCamera"

    const-string v2, "onCreate skip DTP test..go to camcorder preview test"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 13
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v12, 0x3

    const/4 v11, -0x1

    const/4 v7, 0x0

    const/4 v10, 0x2

    const/4 v3, 0x1

    .line 2075
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    .line 2077
    sparse-switch p1, :sswitch_data_0

    .line 2274
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    :cond_1
    :goto_1
    :sswitch_0
    return v3

    .line 2086
    :sswitch_1
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camera/framework/CameraSettings;->hasZoom(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2090
    iget v4, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v4, :cond_1

    .line 2091
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 2092
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getZoom()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/camera/Camera;->mZoomValue:I

    .line 2093
    const-string v4, "FactoryCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KEYCODE_VOLUME_DOWN - [zoom max]:["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/Camera;->mZoomValue:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2095
    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mZoomValue:I

    iget-object v5, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result v5

    if-lt v4, v5, :cond_2

    .line 2096
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v5, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    .line 2100
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v5, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto :goto_1

    .line 2098
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->mZoomValue:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/camera/Camera;->mZoomValue:I

    invoke-virtual {v4, v5}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    goto :goto_2

    .line 2104
    :sswitch_2
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camera/framework/CameraSettings;->hasZoom(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2108
    iget v4, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v4, :cond_1

    .line 2109
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 2110
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getZoom()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/camera/Camera;->mZoomValue:I

    .line 2111
    const-string v4, "FactoryCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KEYCODE_VOLUME_UP - zoom : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/Camera;->mZoomValue:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2112
    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mZoomValue:I

    if-gtz v4, :cond_3

    .line 2113
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4, v7}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    .line 2117
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v5, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto/16 :goto_1

    .line 2115
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->mZoomValue:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/sec/android/app/camera/Camera;->mZoomValue:I

    invoke-virtual {v4, v5}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    goto :goto_3

    .line 2122
    :sswitch_3
    iget-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->mPreviewing:Z

    if-eqz v4, :cond_1

    .line 2124
    iget-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->ommisionTest:Z

    if-eqz v4, :cond_5

    .line 2125
    iget-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->mIsPressedBackkey:Z

    if-nez v4, :cond_4

    .line 2126
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2127
    .local v2, "rightNow":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/camera/Camera;->mCurrentTime:J

    .line 2128
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mIsPressedBackkey:Z

    .line 2129
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->startTimer()V

    goto/16 :goto_1

    .line 2132
    .end local v2    # "rightNow":Ljava/util/Calendar;
    :cond_4
    iput-boolean v7, p0, Lcom/sec/android/app/camera/Camera;->mIsPressedBackkey:Z

    .line 2133
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2134
    .restart local v2    # "rightNow":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/android/app/camera/Camera;->mCurrentTime:J

    const-wide/16 v8, 0x7d0

    add-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    .line 2135
    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    if-eq v4, v10, :cond_1

    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    if-eq v4, v12, :cond_1

    .line 2140
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2141
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v11, v1}, Lcom/sec/android/app/camera/Camera;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 2146
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "rightNow":Ljava/util/Calendar;
    :cond_5
    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    if-eq v4, v10, :cond_1

    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    if-eq v4, v12, :cond_1

    .line 2151
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2152
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v11, v1}, Lcom/sec/android/app/camera/Camera;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 2159
    .end local v1    # "intent":Landroid/content/Intent;
    :sswitch_4
    iget-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->mIsCaptureEnable:Z

    if-eqz v4, :cond_1

    .line 2161
    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    if-eq v4, v3, :cond_6

    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    if-eq v4, v10, :cond_6

    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    if-ne v4, v10, :cond_7

    .line 2165
    :cond_6
    const-string v4, "FactoryCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ignore KEYCODE_FOCUS - mFocusState : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mStatus: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2169
    :cond_7
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 2170
    invoke-direct {p0, v3}, Lcom/sec/android/app/camera/Camera;->doFocus(Z)V

    goto/16 :goto_1

    .line 2174
    :sswitch_5
    iget-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->mIsCaptureEnable:Z

    if-eqz v4, :cond_1

    .line 2176
    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    if-ne v4, v10, :cond_8

    .line 2178
    const-string v4, "FactoryCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ignore KEYCODE_CAMERA - mFocusState : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mStatus: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2182
    :cond_8
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 2183
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v4, v4, Lcom/sec/android/app/camera/Feature;->SUPPORT_CAMERA_HARD_KEY:Z

    if-eqz v4, :cond_9

    .line 2184
    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    if-nez v4, :cond_1

    .line 2185
    invoke-direct {p0, v3}, Lcom/sec/android/app/camera/Camera;->doFocus(Z)V

    .line 2186
    iput v10, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    goto/16 :goto_1

    .line 2189
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->doSnap()V

    goto/16 :goto_1

    .line 2194
    :sswitch_6
    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    if-eq v4, v3, :cond_a

    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    if-eq v4, v10, :cond_a

    iget v4, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    if-ne v4, v10, :cond_b

    .line 2198
    :cond_a
    const-string v4, "FactoryCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ignore KEYCODE_DPAD_CENTER - mFocusState : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mStatus: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2205
    :cond_b
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 2211
    invoke-direct {p0, v3}, Lcom/sec/android/app/camera/Camera;->doFocus(Z)V

    .line 2212
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/framework/ShutterButton;->isInTouchMode()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 2213
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/framework/ShutterButton;->requestFocusFromTouch()Z

    .line 2217
    :goto_4
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/camera/framework/ShutterButton;->setPressed(Z)V

    goto/16 :goto_1

    .line 2215
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/framework/ShutterButton;->requestFocus()Z

    goto :goto_4

    .line 2221
    :sswitch_7
    const-string v4, "FactoryCamera"

    const-string v5, "KEYCODE_1 pressed"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2222
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 2223
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v4, :cond_1

    .line 2224
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 2225
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v4

    const-string v5, "macro"

    iget-object v6, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/camera/framework/CameraSettings;->isSupportedFocusModes(Ljava/lang/String;Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2227
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v5, "macro"

    invoke-virtual {v4, v5}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 2229
    :try_start_0
    const-string v4, "FactoryCamera"

    const-string v5, "mCameraDevice.setParameters(mParameters)"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2230
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v5, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1

    .line 2231
    :catch_0
    move-exception v4

    goto/16 :goto_1

    .line 2234
    :catch_1
    move-exception v0

    .line 2235
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v4, "FactoryCamera"

    const-string v5, "setParameter fail"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2236
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 2240
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_d
    const-string v4, "FactoryCamera"

    const-string v5, "not support macro mode"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2241
    const-string v4, "Not support Macro Mode"

    invoke-static {p0, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2248
    :sswitch_8
    const-string v4, "FactoryCamera"

    const-string v5, "KEYCODE_2 pressed"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2249
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 2250
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v4, :cond_1

    .line 2251
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 2252
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/camera/framework/CameraSettings;->isSupportedFocusModes(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 2254
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/camera/framework/CameraSettings;->getFocusMode()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 2256
    :try_start_1
    const-string v4, "FactoryCamera"

    const-string v5, "mCameraDevice.setParameters(mParameters)"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2257
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v5, p0, Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_1

    .line 2258
    :catch_2
    move-exception v4

    goto/16 :goto_1

    .line 2261
    :catch_3
    move-exception v0

    .line 2262
    .restart local v0    # "e":Ljava/lang/RuntimeException;
    const-string v4, "FactoryCamera"

    const-string v5, "setParameter fail"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2263
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 2267
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_e
    const-string v4, "FactoryCamera"

    const-string v5, "not support focusmode"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2077
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_3
        0x5 -> :sswitch_0
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0x17 -> :sswitch_6
        0x18 -> :sswitch_2
        0x19 -> :sswitch_1
        0x1b -> :sswitch_5
        0x50 -> :sswitch_4
        0x52 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    .line 2279
    sparse-switch p1, :sswitch_data_0

    .line 2333
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    :cond_0
    :goto_0
    :sswitch_0
    return v2

    .line 2281
    :sswitch_1
    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mIsCaptureEnable:Z

    if-eqz v3, :cond_0

    .line 2283
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/sec/android/app/camera/Camera;->doFocus(Z)V

    goto :goto_0

    .line 2302
    :sswitch_2
    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mIsCaptureEnable:Z

    if-eqz v3, :cond_0

    .line 2305
    iget v3, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    if-ne v3, v6, :cond_1

    .line 2307
    const-string v3, "FactoryCamera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onKeyUp ignore KEYCODE_ENTER - mFocusState : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mStatus: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2311
    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-nez v3, :cond_0

    .line 2312
    const-string v3, "FactoryCamera"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onKeyUp - mFocusState : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mStatus: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2314
    iget v3, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    if-nez v3, :cond_0

    .line 2315
    invoke-direct {p0, v2}, Lcom/sec/android/app/camera/Camera;->doFocus(Z)V

    .line 2316
    iput v6, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    goto :goto_0

    .line 2321
    :sswitch_3
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->isShowFailDialog()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2323
    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v3, :cond_2

    .line 2324
    const-string v1, "REAR CAMERA"

    .line 2328
    .local v1, "strFailMessage":Ljava/lang/String;
    :goto_1
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera$FailDialogFragment;->newInstance(Ljava/lang/String;)Lcom/sec/android/app/camera/Camera$FailDialogFragment;

    move-result-object v0

    .line 2329
    .local v0, "alertDialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "dialog"

    invoke-virtual {v0, v3, v4}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2326
    .end local v0    # "alertDialog":Landroid/app/DialogFragment;
    .end local v1    # "strFailMessage":Ljava/lang/String;
    :cond_2
    const-string v1, "FRONT CAMERA"

    .restart local v1    # "strFailMessage":Ljava/lang/String;
    goto :goto_1

    .line 2279
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x42 -> :sswitch_2
        0x50 -> :sswitch_1
        0x52 -> :sswitch_0
        0xbb -> :sswitch_3
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1990
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v1, :cond_0

    .line 1991
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v1, v3}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1992
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v1, v3}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    .line 1995
    :cond_0
    const-string v1, "FactoryCamera"

    const-string v2, "onPause()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1997
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_2

    .line 1998
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1999
    const-string v1, "FactoryCamera"

    const-string v2, "releaseWakelock"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2000
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2002
    :cond_1
    iput-object v3, p0, Lcom/sec/android/app/camera/Camera;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 2005
    :cond_2
    iput-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->mShutterBtnlock:Z

    .line 2007
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->mPausing:Z

    .line 2008
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mStartCheck:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2009
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mDatalineCheck:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2010
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mTimerHandler:Landroid/os/Handler;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2019
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->stopPreview()V

    .line 2020
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->closeCamera()V

    .line 2022
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2023
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 2026
    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/Camera;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2031
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mFocusToneGenerator:Landroid/media/ToneGenerator;

    if-eqz v1, :cond_4

    .line 2032
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mFocusToneGenerator:Landroid/media/ToneGenerator;

    invoke-virtual {v1}, Landroid/media/ToneGenerator;->release()V

    .line 2033
    iput-object v3, p0, Lcom/sec/android/app/camera/Camera;->mFocusToneGenerator:Landroid/media/ToneGenerator;

    .line 2036
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-eqz v1, :cond_5

    .line 2037
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v1}, Landroid/os/DVFSHelper;->release()V

    .line 2040
    :cond_5
    const-string v1, "FactoryCamera"

    const-string v2, "mImageCapture = null"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2041
    iput-object v3, p0, Lcom/sec/android/app/camera/Camera;->mImageCapture:Lcom/sec/android/app/camera/Camera$ImageCapture;

    .line 2043
    iget-boolean v1, p0, Lcom/sec/android/app/camera/Camera;->mStopCamera:Z

    if-eqz v1, :cond_6

    .line 2044
    const-string v1, "com.android.samsungtest.CAMERA_STOP_ACK"

    invoke-direct {p0, v1}, Lcom/sec/android/app/camera/Camera;->sendBroadCastAck(Ljava/lang/String;)V

    .line 2045
    iput-boolean v5, p0, Lcom/sec/android/app/camera/Camera;->mStopCamera:Z

    .line 2048
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CAMERA_NO_TRANSITION_ANIMATION:Z

    if-eqz v1, :cond_7

    .line 2049
    iget v1, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v1, :cond_9

    .line 2051
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mWindowManager:Landroid/view/IWindowManager;

    if-eqz v1, :cond_8

    .line 2052
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mWindowManager:Landroid/view/IWindowManager;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/view/IWindowManager;->setAnimationScale(IF)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2070
    :cond_7
    :goto_1
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 2071
    return-void

    .line 2054
    :cond_8
    :try_start_2
    const-string v1, "FactoryCamera"

    const-string v2, "mWindowManager is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 2055
    :catch_0
    move-exception v0

    .line 2056
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 2058
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_9
    iget v1, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-ne v1, v4, :cond_7

    .line 2060
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mWindowManager:Landroid/view/IWindowManager;

    if-eqz v1, :cond_a

    .line 2061
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->mWindowManager:Landroid/view/IWindowManager;

    const/4 v2, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v2, v3}, Landroid/view/IWindowManager;->setAnimationScale(IF)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 2064
    :catch_1
    move-exception v0

    .line 2065
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 2063
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_a
    :try_start_4
    const-string v1, "FactoryCamera"

    const-string v2, "mWindowManager is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 2027
    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method public onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1891
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1892
    const-string v3, "FactoryCamera"

    const-string v4, "onResume()"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1894
    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->bIsCamcorderPreviewFail:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->bIsOISFail:Z

    if-eqz v3, :cond_1

    .line 1960
    :cond_0
    :goto_0
    return-void

    .line 1898
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "device_policy"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/admin/DevicePolicyManager;

    .line 1899
    .local v2, "mDPM":Landroid/app/admin/DevicePolicyManager;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/admin/DevicePolicyManager;->getAllowCamera(Landroid/content/ComponentName;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1900
    const-string v3, "FactoryCamera"

    const-string v4, "onResume CAMERA disable"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1901
    const v3, 0x7f0a0013

    invoke-static {p0, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1903
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->finish()V

    goto :goto_0

    .line 1908
    :cond_2
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 1909
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string v3, "com.android.samsungtest.CameraStop"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1910
    const-string v3, "com.android.samsungtest.CameraShot"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1911
    const-string v3, "com.android.samsungtest.RecordingStart"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1913
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/app/camera/Camera;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1915
    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->CHECK_CAMERA_LATEST_MODULE:Z

    if-eqz v3, :cond_3

    .line 1916
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->checkCameraLatestModule()V

    .line 1921
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/camera/framework/CameraSettings;->needToCheckCamcorderPreviewTest()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->bEnablePreviewCb:Z

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->bDoneDTP:Z

    if-ne v3, v5, :cond_4

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mFinishCamcorderPreviewTest:Z

    if-nez v3, :cond_4

    iget v3, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    if-eq v3, v5, :cond_4

    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v3, :cond_4

    const-string v3, "autotest"

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1926
    const-string v3, "FactoryCamera"

    const-string v4, "onResume skip DTP test..go to camcorder preview test"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1927
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1928
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "camera_id"

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1929
    const-string v3, "picture-size"

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1930
    const-string v3, "orientation_reverse"

    iget-boolean v4, p0, Lcom/sec/android/app/camera/Camera;->mIsOrientationReverse:Z

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1931
    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/camera/Camera;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1937
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/camera/framework/CameraSettings;->needToCheckOISTest()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->bEnablePreviewCb:Z

    if-eqz v3, :cond_5

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->bDoneDTP:Z

    if-ne v3, v5, :cond_5

    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mFinishOISTest:Z

    if-nez v3, :cond_5

    iget v3, p0, Lcom/sec/android/app/camera/Camera;->mFlashEnable:I

    if-eq v3, v5, :cond_5

    iget v3, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    if-nez v3, :cond_5

    const-string v3, "autotest"

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1942
    const-string v3, "FactoryCamera"

    const-string v4, "onResume skip DTP test..go to OIS test"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1943
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/camera/OISTest;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1944
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v3, "camera_id"

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1947
    const/4 v3, 0x4

    invoke-virtual {p0, v0, v3}, Lcom/sec/android/app/camera/Camera;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1951
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->setOrientationListener()V

    .line 1953
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->initialize()V

    .line 1955
    iget-boolean v3, p0, Lcom/sec/android/app/camera/Camera;->mCameraAutoCapture:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->isLowLightCaptureTesting()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "autotest"

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1957
    const-string v3, "FactoryCamera"

    const-string v4, "Camera.onResume() startTimerAutoCapture..."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1958
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera;->startTimerAutoCapture()V

    goto/16 :goto_0
.end method

.method public onShutterButtonClick(Lcom/sec/android/app/camera/framework/ShutterButton;)V
    .locals 3
    .param p1, "button"    # Lcom/sec/android/app/camera/framework/ShutterButton;

    .prologue
    .line 511
    const-string v0, "FactoryCamera"

    const-string v1, "call onShutterButtonClick"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onShutterButtonClick - testType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    const-string v0, "autotest"

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 514
    const-string v0, "FactoryCamera"

    const-string v1, "onShutterButtonClick - Shutter button is not used in auto test mode."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    :cond_0
    :goto_0
    return-void

    .line 518
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mPausing:Z

    if-nez v0, :cond_0

    .line 522
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 524
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ignore onShutterButtonClick - mFocusState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 529
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mIsCaptureEnable:Z

    if-nez v0, :cond_3

    .line 530
    const-string v0, "FactoryCamera"

    const-string v1, "mIsCaptureEnble is false"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 534
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mShutterBtnlock:Z

    if-eqz v0, :cond_4

    .line 535
    const-string v0, "FactoryCamera"

    const-string v1, "mShutterBtnlock is true"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 539
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/app/camera/framework/ShutterButton;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 541
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->doSnap()V

    goto :goto_0

    .line 539
    nop

    :pswitch_data_0
    .packed-switch 0x7f09000c
        :pswitch_0
    .end packed-switch
.end method

.method public onShutterButtonFocus(Lcom/sec/android/app/camera/framework/ShutterButton;Z)V
    .locals 4
    .param p1, "button"    # Lcom/sec/android/app/camera/framework/ShutterButton;
    .param p2, "pressed"    # Z

    .prologue
    const/4 v3, 0x2

    .line 401
    const-string v0, "FactoryCamera"

    const-string v1, "call onShutterButtonFocus"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onShutterButtonFocus - testType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const-string v0, "autotest"

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 404
    const-string v0, "FactoryCamera"

    const-string v1, "onShutterButtonFocus - Shutter button is not used in auto test mode."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mPausing:Z

    if-nez v0, :cond_0

    .line 412
    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    if-eq v0, v3, :cond_2

    iget v0, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    if-ne v0, v3, :cond_3

    .line 416
    :cond_2
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ignore onShutterButtonFocus - mFocusState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    iput v3, p0, Lcom/sec/android/app/camera/Camera;->mFocusState:I

    goto :goto_0

    .line 422
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mIsCaptureEnable:Z

    if-nez v0, :cond_4

    .line 423
    const-string v0, "FactoryCamera"

    const-string v1, "mIsCaptureEnble is false"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 427
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mShutterBtnlock:Z

    if-eqz v0, :cond_5

    .line 428
    const-string v0, "FactoryCamera"

    const-string v1, "mShutterBtnlock is true"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 432
    :cond_5
    invoke-virtual {p1}, Lcom/sec/android/app/camera/framework/ShutterButton;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 434
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/sec/android/app/camera/Camera;->doFocus(Z)V

    goto :goto_0

    .line 432
    :pswitch_data_0
    .packed-switch 0x7f09000c
        :pswitch_0
    .end packed-switch
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 1983
    const-string v0, "FactoryCamera"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1985
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 1986
    return-void
.end method

.method public setCameraDisplayOrientation(Landroid/app/Activity;ILandroid/hardware/Camera;)V
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "cameraId"    # I
    .param p3, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 2860
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 2862
    .local v1, "info":Landroid/hardware/Camera$CameraInfo;
    invoke-static {p2, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 2863
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v3

    .line 2864
    .local v3, "rotation":I
    const/4 v0, 0x0

    .line 2866
    .local v0, "degrees":I
    packed-switch v3, :pswitch_data_0

    .line 2882
    :goto_0
    const-string v4, "FactoryCamera"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "rotation: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " degrees: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " info.orientation: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2884
    iget v4, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 2885
    iget v4, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v4, v0

    rem-int/lit16 v2, v4, 0x168

    .line 2886
    .local v2, "result":I
    rsub-int v4, v2, 0x168

    rem-int/lit16 v2, v4, 0x168

    .line 2891
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v4, v4, Lcom/sec/android/app/camera/Feature;->FIX_CAMERA_DISPLAY_ORIENTATION:Z

    if-eqz v4, :cond_1

    .line 2892
    const/4 v4, 0x0

    invoke-virtual {p3, v4}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 2896
    :goto_2
    return-void

    .line 2868
    .end local v2    # "result":I
    :pswitch_0
    const/4 v0, 0x0

    .line 2869
    goto :goto_0

    .line 2871
    :pswitch_1
    const/16 v0, 0x5a

    .line 2872
    goto :goto_0

    .line 2874
    :pswitch_2
    const/16 v0, 0xb4

    .line 2875
    goto :goto_0

    .line 2877
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 2888
    :cond_0
    iget v4, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v4, v0

    add-int/lit16 v4, v4, 0x168

    rem-int/lit16 v2, v4, 0x168

    .restart local v2    # "result":I
    goto :goto_1

    .line 2894
    :cond_1
    invoke-virtual {p3, v2}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    goto :goto_2

    .line 2866
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected setOrientationListener()V
    .locals 1

    .prologue
    .line 3055
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_0

    .line 3056
    new-instance v0, Lcom/sec/android/app/camera/Camera$10;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/camera/Camera$10;-><init>(Lcom/sec/android/app/camera/Camera;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 3066
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 3067
    return-void
.end method

.method protected setOrientationOnTake(I)V
    .locals 0
    .param p1, "orientationOnTake"    # I

    .prologue
    .line 3082
    iput p1, p0, Lcom/sec/android/app/camera/Camera;->mOrientationOnTake:I

    .line 3083
    return-void
.end method

.method public shortSnapshotcancelAutoFocus()V
    .locals 4

    .prologue
    .line 1260
    const-string v0, "FactoryCamera"

    const-string v1, "short snapshot cancelAutoFocus"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1261
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 1262
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    .line 1264
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1265
    return-void
.end method

.method public startCamcorderRecording()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 3472
    const-string v1, "FactoryCamera"

    const-string v2, "startCamcorderRecording..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3474
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3475
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "test_type"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3476
    const-string v1, "camera_id"

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->cameraType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3477
    const-string v1, "picture-size"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3478
    const-string v1, "orientation_reverse"

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Camera;->mIsOrientationReverse:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3479
    const-string v1, "usb_or_uart_command"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3480
    const-string v1, "movie_index_count"

    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mMovieIndexCount:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/android/app/camera/Camera;->mMovieIndexCount:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3481
    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/camera/Camera;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3482
    return-void
.end method

.method public startPostViewTest()V
    .locals 3

    .prologue
    .line 3109
    const-string v1, "FactoryCamera"

    const-string v2, "startPostViewTest..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3110
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/camera/PostViewTest;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3111
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_REAR:Z

    if-eqz v1, :cond_0

    .line 3112
    const-string v1, "low_light_capture_display"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3116
    :goto_0
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/camera/Camera;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3117
    return-void

    .line 3114
    :cond_0
    const-string v1, "low_light_capture_display"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected startTimer()V
    .locals 4

    .prologue
    .line 2386
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mTimerHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2388
    return-void
.end method

.method protected startTimerAutoCapture()V
    .locals 5

    .prologue
    const/16 v4, 0x15

    .line 2399
    const-string v0, "FactoryCamera"

    const-string v1, "Camera.startTimerAutoCapture()..."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2400
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2401
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2403
    return-void
.end method

.method protected startTimerCaptureBlock()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 2391
    const-string v0, "FactoryCamera"

    const-string v1, "Camera.startTimerCaptureBlock()..."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2392
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera;->mIsCaptureEnable:Z

    .line 2393
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2394
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2396
    return-void
.end method

.method public streamToBytes(Ljava/io/InputStream;I)[B
    .locals 4
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "size"    # I

    .prologue
    .line 1016
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2, p2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 1017
    .local v2, "os":Ljava/io/ByteArrayOutputStream;
    new-array v0, p2, [B

    .line 1020
    .local v0, "buffer":[B
    :goto_0
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "len":I
    if-ltz v1, :cond_0

    .line 1021
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1023
    .end local v1    # "len":I
    :catch_0
    move-exception v3

    .line 1025
    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 5
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 1370
    const-string v2, "FactoryCamera"

    const-string v3, "surfaceChanged"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1373
    if-ge p3, p4, :cond_0

    .line 1374
    move v1, p3

    .line 1375
    .local v1, "tmp_swp":I
    move p3, p4

    .line 1376
    move p4, v1

    .line 1377
    const-string v2, "FactoryCamera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "swap - w : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " h : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1380
    .end local v1    # "tmp_swp":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mSurfaceView:Lcom/sec/android/app/camera/VideoPreview;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camera/VideoPreview;->setVisibility(I)V

    .line 1382
    const/4 v0, 0x1

    .line 1390
    .local v0, "preview":Z
    iget v2, p0, Lcom/sec/android/app/camera/Camera;->mOriginalViewFinderWidth:I

    if-nez v2, :cond_1

    .line 1391
    iput p3, p0, Lcom/sec/android/app/camera/Camera;->mOriginalViewFinderWidth:I

    .line 1392
    iput p4, p0, Lcom/sec/android/app/camera/Camera;->mOriginalViewFinderHeight:I

    .line 1394
    :cond_1
    invoke-direct {p0, p3, p4, v0}, Lcom/sec/android/app/camera/Camera;->setViewFinder(IIZ)V

    .line 1396
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera;->mImageCapture:Lcom/sec/android/app/camera/Camera$ImageCapture;

    iput-object v2, p0, Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;

    .line 1397
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 1400
    const-string v0, "FactoryCamera"

    const-string v1, "surfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1401
    iput-object p1, p0, Lcom/sec/android/app/camera/Camera;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 1402
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 1405
    const-string v0, "FactoryCamera"

    const-string v1, "surfaceDestroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1406
    invoke-direct {p0}, Lcom/sec/android/app/camera/Camera;->stopPreview()V

    .line 1407
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 1408
    return-void
.end method
