.class final Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;
.super Ljava/lang/Object;
.source "GeneralAppIdDecoder.java"


# instance fields
.field private final buffer:Ljava/lang/StringBuilder;

.field private final current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

.field private final information:Lcom/sec/android/app/camaftest/core/common/BitArray;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camaftest/core/common/BitArray;)V
    .locals 1
    .param p1, "information"    # Lcom/sec/android/app/camaftest/core/common/BitArray;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-direct {v0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    .line 44
    iput-object p1, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    .line 45
    return-void
.end method

.method private decodeAlphanumeric(I)Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;
    .locals 6
    .param p1, "pos"    # I

    .prologue
    const/16 v4, 0xf

    const/4 v3, 0x5

    .line 390
    invoke-virtual {p0, p1, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v1

    .line 391
    .local v1, "fiveBitValue":I
    if-ne v1, v4, :cond_0

    .line 392
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;

    add-int/lit8 v4, p1, 0x5

    const/16 v5, 0x24

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;-><init>(IC)V

    .line 425
    :goto_0
    return-object v3

    .line 395
    :cond_0
    if-lt v1, v3, :cond_1

    if-ge v1, v4, :cond_1

    .line 396
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;

    add-int/lit8 v4, p1, 0x5

    add-int/lit8 v5, v1, 0x30

    add-int/lit8 v5, v5, -0x5

    int-to-char v5, v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;-><init>(IC)V

    goto :goto_0

    .line 399
    :cond_1
    const/4 v3, 0x6

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v2

    .line 401
    .local v2, "sixBitValue":I
    const/16 v3, 0x20

    if-lt v2, v3, :cond_2

    const/16 v3, 0x3a

    if-ge v2, v3, :cond_2

    .line 402
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;

    add-int/lit8 v4, p1, 0x6

    add-int/lit8 v5, v2, 0x21

    int-to-char v5, v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;-><init>(IC)V

    goto :goto_0

    .line 406
    :cond_2
    packed-switch v2, :pswitch_data_0

    .line 423
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Decoding invalid alphanumeric value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 408
    :pswitch_0
    const/16 v0, 0x2a

    .line 425
    .local v0, "c":C
    :goto_1
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;

    add-int/lit8 v4, p1, 0x6

    invoke-direct {v3, v4, v0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;-><init>(IC)V

    goto :goto_0

    .line 411
    .end local v0    # "c":C
    :pswitch_1
    const/16 v0, 0x2c

    .line 412
    .restart local v0    # "c":C
    goto :goto_1

    .line 414
    .end local v0    # "c":C
    :pswitch_2
    const/16 v0, 0x2d

    .line 415
    .restart local v0    # "c":C
    goto :goto_1

    .line 417
    .end local v0    # "c":C
    :pswitch_3
    const/16 v0, 0x2e

    .line 418
    .restart local v0    # "c":C
    goto :goto_1

    .line 420
    .end local v0    # "c":C
    :pswitch_4
    const/16 v0, 0x2f

    .line 421
    .restart local v0    # "c":C
    goto :goto_1

    .line 406
    :pswitch_data_0
    .packed-switch 0x3a
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private decodeIsoIec646(I)Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;
    .locals 7
    .param p1, "pos"    # I

    .prologue
    const/16 v6, 0x5a

    const/16 v5, 0xf

    const/4 v4, 0x5

    .line 279
    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v2

    .line 280
    .local v2, "fiveBitValue":I
    if-ne v2, v5, :cond_0

    .line 281
    new-instance v4, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;

    add-int/lit8 v5, p1, 0x5

    const/16 v6, 0x24

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;-><init>(IC)V

    .line 367
    :goto_0
    return-object v4

    .line 284
    :cond_0
    if-lt v2, v4, :cond_1

    if-ge v2, v5, :cond_1

    .line 285
    new-instance v4, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;

    add-int/lit8 v5, p1, 0x5

    add-int/lit8 v6, v2, 0x30

    add-int/lit8 v6, v6, -0x5

    int-to-char v6, v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;-><init>(IC)V

    goto :goto_0

    .line 288
    :cond_1
    const/4 v4, 0x7

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v3

    .line 290
    .local v3, "sevenBitValue":I
    const/16 v4, 0x40

    if-lt v3, v4, :cond_2

    if-ge v3, v6, :cond_2

    .line 291
    new-instance v4, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;

    add-int/lit8 v5, p1, 0x7

    add-int/lit8 v6, v3, 0x1

    int-to-char v6, v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;-><init>(IC)V

    goto :goto_0

    .line 294
    :cond_2
    if-lt v3, v6, :cond_3

    const/16 v4, 0x74

    if-ge v3, v4, :cond_3

    .line 295
    new-instance v4, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;

    add-int/lit8 v5, p1, 0x7

    add-int/lit8 v6, v3, 0x7

    int-to-char v6, v6

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;-><init>(IC)V

    goto :goto_0

    .line 298
    :cond_3
    const/16 v4, 0x8

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v1

    .line 300
    .local v1, "eightBitValue":I
    packed-switch v1, :pswitch_data_0

    .line 365
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Decoding invalid ISO/IEC 646 value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 302
    :pswitch_0
    const/16 v0, 0x21

    .line 367
    .local v0, "c":C
    :goto_1
    new-instance v4, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;

    add-int/lit8 v5, p1, 0x8

    invoke-direct {v4, v5, v0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;-><init>(IC)V

    goto :goto_0

    .line 305
    .end local v0    # "c":C
    :pswitch_1
    const/16 v0, 0x22

    .line 306
    .restart local v0    # "c":C
    goto :goto_1

    .line 308
    .end local v0    # "c":C
    :pswitch_2
    const/16 v0, 0x25

    .line 309
    .restart local v0    # "c":C
    goto :goto_1

    .line 311
    .end local v0    # "c":C
    :pswitch_3
    const/16 v0, 0x26

    .line 312
    .restart local v0    # "c":C
    goto :goto_1

    .line 314
    .end local v0    # "c":C
    :pswitch_4
    const/16 v0, 0x27

    .line 315
    .restart local v0    # "c":C
    goto :goto_1

    .line 317
    .end local v0    # "c":C
    :pswitch_5
    const/16 v0, 0x28

    .line 318
    .restart local v0    # "c":C
    goto :goto_1

    .line 320
    .end local v0    # "c":C
    :pswitch_6
    const/16 v0, 0x29

    .line 321
    .restart local v0    # "c":C
    goto :goto_1

    .line 323
    .end local v0    # "c":C
    :pswitch_7
    const/16 v0, 0x2a

    .line 324
    .restart local v0    # "c":C
    goto :goto_1

    .line 326
    .end local v0    # "c":C
    :pswitch_8
    const/16 v0, 0x2b

    .line 327
    .restart local v0    # "c":C
    goto :goto_1

    .line 329
    .end local v0    # "c":C
    :pswitch_9
    const/16 v0, 0x2c

    .line 330
    .restart local v0    # "c":C
    goto :goto_1

    .line 332
    .end local v0    # "c":C
    :pswitch_a
    const/16 v0, 0x2d

    .line 333
    .restart local v0    # "c":C
    goto :goto_1

    .line 335
    .end local v0    # "c":C
    :pswitch_b
    const/16 v0, 0x2e

    .line 336
    .restart local v0    # "c":C
    goto :goto_1

    .line 338
    .end local v0    # "c":C
    :pswitch_c
    const/16 v0, 0x2f

    .line 339
    .restart local v0    # "c":C
    goto :goto_1

    .line 341
    .end local v0    # "c":C
    :pswitch_d
    const/16 v0, 0x3a

    .line 342
    .restart local v0    # "c":C
    goto :goto_1

    .line 344
    .end local v0    # "c":C
    :pswitch_e
    const/16 v0, 0x3b

    .line 345
    .restart local v0    # "c":C
    goto :goto_1

    .line 347
    .end local v0    # "c":C
    :pswitch_f
    const/16 v0, 0x3c

    .line 348
    .restart local v0    # "c":C
    goto :goto_1

    .line 350
    .end local v0    # "c":C
    :pswitch_10
    const/16 v0, 0x3d

    .line 351
    .restart local v0    # "c":C
    goto :goto_1

    .line 353
    .end local v0    # "c":C
    :pswitch_11
    const/16 v0, 0x3e

    .line 354
    .restart local v0    # "c":C
    goto :goto_1

    .line 356
    .end local v0    # "c":C
    :pswitch_12
    const/16 v0, 0x3f

    .line 357
    .restart local v0    # "c":C
    goto :goto_1

    .line 359
    .end local v0    # "c":C
    :pswitch_13
    const/16 v0, 0x5f

    .line 360
    .restart local v0    # "c":C
    goto :goto_1

    .line 362
    .end local v0    # "c":C
    :pswitch_14
    const/16 v0, 0x20

    .line 363
    .restart local v0    # "c":C
    goto :goto_1

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0xe8
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method

.method private decodeNumeric(I)Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;
    .locals 7
    .param p1, "pos"    # I

    .prologue
    const/16 v6, 0xa

    .line 88
    add-int/lit8 v3, p1, 0x7

    iget-object v4, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v4

    if-le v3, v4, :cond_1

    .line 89
    const/4 v3, 0x4

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v2

    .line 90
    .local v2, "numeric":I
    if-nez v2, :cond_0

    .line 91
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;

    iget-object v4, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v4

    invoke-direct {v3, v4, v6, v6}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;-><init>(III)V

    .line 100
    :goto_0
    return-object v3

    .line 93
    :cond_0
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;

    iget-object v4, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v4

    add-int/lit8 v5, v2, -0x1

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;-><init>(III)V

    goto :goto_0

    .line 95
    .end local v2    # "numeric":I
    :cond_1
    const/4 v3, 0x7

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v2

    .line 97
    .restart local v2    # "numeric":I
    add-int/lit8 v3, v2, -0x8

    div-int/lit8 v0, v3, 0xb

    .line 98
    .local v0, "digit1":I
    add-int/lit8 v3, v2, -0x8

    rem-int/lit8 v1, v3, 0xb

    .line 100
    .local v1, "digit2":I
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;

    add-int/lit8 v4, p1, 0x7

    invoke-direct {v3, v4, v0, v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;-><init>(III)V

    goto :goto_0
.end method

.method static extractNumericValueFromBitArray(Lcom/sec/android/app/camaftest/core/common/BitArray;II)I
    .locals 4
    .param p0, "information"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .param p1, "pos"    # I
    .param p2, "bits"    # I

    .prologue
    .line 108
    const/16 v2, 0x20

    if-le p2, v2, :cond_0

    .line 109
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "extractNumberValueFromBitArray can\'t handle more than 32 bits"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 112
    :cond_0
    const/4 v1, 0x0

    .line 113
    .local v1, "value":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_2

    .line 114
    add-int v2, p1, v0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 115
    const/4 v2, 0x1

    sub-int v3, p2, v0

    add-int/lit8 v3, v3, -0x1

    shl-int/2addr v2, v3

    or-int/2addr v1, v2

    .line 113
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    :cond_2
    return v1
.end method

.method private isAlphaOr646ToNumericLatch(I)Z
    .locals 4
    .param p1, "pos"    # I

    .prologue
    const/4 v1, 0x0

    .line 448
    add-int/lit8 v2, p1, 0x3

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 457
    :cond_0
    :goto_0
    return v1

    .line 452
    :cond_1
    move v0, p1

    .local v0, "i":I
    :goto_1
    add-int/lit8 v2, p1, 0x3

    if-ge v0, v2, :cond_2

    .line 453
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 452
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 457
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isAlphaTo646ToAlphaLatch(I)Z
    .locals 4
    .param p1, "pos"    # I

    .prologue
    const/4 v1, 0x0

    .line 429
    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 443
    :cond_0
    :goto_0
    return v1

    .line 433
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v2, 0x5

    if-ge v0, v2, :cond_4

    add-int v2, v0, p1

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 434
    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 435
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    add-int/lit8 v3, p1, 0x2

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 433
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 438
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    add-int v3, p1, v0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 443
    :cond_4
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isNumericToAlphaNumericLatch(I)Z
    .locals 4
    .param p1, "pos"    # I

    .prologue
    const/4 v1, 0x0

    .line 464
    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 473
    :cond_0
    :goto_0
    return v1

    .line 468
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v2, 0x4

    if-ge v0, v2, :cond_2

    add-int v2, v0, p1

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 469
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    add-int v3, p1, v0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 468
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 473
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isStillAlpha(I)Z
    .locals 8
    .param p1, "pos"    # I

    .prologue
    const/16 v7, 0x10

    const/4 v6, 0x5

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 371
    add-int/lit8 v4, p1, 0x5

    iget-object v5, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v5}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v5

    if-le v4, v5, :cond_1

    .line 386
    :cond_0
    :goto_0
    return v3

    .line 376
    :cond_1
    invoke-virtual {p0, p1, v6}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v0

    .line 377
    .local v0, "fiveBitValue":I
    if-lt v0, v6, :cond_2

    if-ge v0, v7, :cond_2

    move v3, v2

    .line 378
    goto :goto_0

    .line 381
    :cond_2
    add-int/lit8 v4, p1, 0x6

    iget-object v5, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v5}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v5

    if-gt v4, v5, :cond_0

    .line 385
    const/4 v4, 0x6

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v1

    .line 386
    .local v1, "sixBitValue":I
    if-lt v1, v7, :cond_3

    const/16 v4, 0x3f

    if-ge v1, v4, :cond_3

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method private isStillIsoIec646(I)Z
    .locals 8
    .param p1, "pos"    # I

    .prologue
    const/4 v7, 0x5

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 251
    add-int/lit8 v5, p1, 0x5

    iget-object v6, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v6}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v6

    if-le v5, v6, :cond_1

    .line 274
    :cond_0
    :goto_0
    return v4

    .line 255
    :cond_1
    invoke-virtual {p0, p1, v7}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v1

    .line 256
    .local v1, "fiveBitValue":I
    if-lt v1, v7, :cond_2

    const/16 v5, 0x10

    if-ge v1, v5, :cond_2

    move v4, v3

    .line 257
    goto :goto_0

    .line 260
    :cond_2
    add-int/lit8 v5, p1, 0x7

    iget-object v6, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v6}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v6

    if-gt v5, v6, :cond_0

    .line 264
    const/4 v5, 0x7

    invoke-virtual {p0, p1, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v2

    .line 265
    .local v2, "sevenBitValue":I
    const/16 v5, 0x40

    if-lt v2, v5, :cond_3

    const/16 v5, 0x74

    if-ge v2, v5, :cond_3

    move v4, v3

    .line 266
    goto :goto_0

    .line 269
    :cond_3
    add-int/lit8 v5, p1, 0x8

    iget-object v6, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v6}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v6

    if-gt v5, v6, :cond_0

    .line 273
    const/16 v5, 0x8

    invoke-virtual {p0, p1, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(II)I

    move-result v0

    .line 274
    .local v0, "eightBitValue":I
    const/16 v5, 0xe8

    if-lt v0, v5, :cond_4

    const/16 v5, 0xfd

    if-ge v0, v5, :cond_4

    :goto_1
    move v4, v3

    goto :goto_0

    :cond_4
    move v3, v4

    goto :goto_1
.end method

.method private isStillNumeric(I)Z
    .locals 4
    .param p1, "pos"    # I

    .prologue
    const/4 v1, 0x1

    .line 74
    add-int/lit8 v2, p1, 0x7

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v3

    if-le v2, v3, :cond_2

    .line 75
    add-int/lit8 v2, p1, 0x4

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v3

    if-gt v2, v3, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v1

    .line 75
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 78
    :cond_2
    move v0, p1

    .local v0, "i":I
    :goto_1
    add-int/lit8 v2, p1, 0x3

    if-ge v0, v2, :cond_3

    .line 79
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 84
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    add-int/lit8 v2, p1, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v1

    goto :goto_0
.end method

.method private parseAlphaBlock()Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;
    .locals 4

    .prologue
    .line 222
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->isStillAlpha(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->decodeAlphanumeric(I)Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;

    move-result-object v0

    .line 224
    .local v0, "alpha":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;->getNewPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->setPosition(I)V

    .line 226
    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;->isFNC1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 227
    new-instance v1, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;-><init>(ILjava/lang/String;)V

    .line 228
    .local v1, "information":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;-><init>(Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;Z)V

    .line 247
    .end local v0    # "alpha":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;
    .end local v1    # "information":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    :goto_1
    return-object v2

    .line 232
    .restart local v0    # "alpha":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;->getValue()C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 235
    .end local v0    # "alpha":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->isAlphaOr646ToNumericLatch(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->incrementPosition(I)V

    .line 237
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->setNumeric()V

    .line 247
    :cond_2
    :goto_2
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;-><init>(Z)V

    goto :goto_1

    .line 238
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->isAlphaTo646ToAlphaLatch(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 239
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    add-int/lit8 v2, v2, 0x5

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 240
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->incrementPosition(I)V

    .line 245
    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->setIsoIec646()V

    goto :goto_2

    .line 242
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->setPosition(I)V

    goto :goto_3
.end method

.method private parseBlocks()Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    .locals 5

    .prologue
    .line 142
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v0

    .line 144
    .local v0, "initialPosition":I
    iget-object v4, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->isAlpha()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 145
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->parseAlphaBlock()Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;

    move-result-object v3

    .line 146
    .local v3, "result":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;
    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;->isFinished()Z

    move-result v1

    .line 155
    .local v1, "isFinished":Z
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v4

    if-eq v0, v4, :cond_3

    const/4 v2, 0x1

    .line 156
    .local v2, "positionChanged":Z
    :goto_1
    if-nez v2, :cond_4

    if-nez v1, :cond_4

    .line 161
    :goto_2
    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;->getDecodedInformation()Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

    move-result-object v4

    return-object v4

    .line 147
    .end local v1    # "isFinished":Z
    .end local v2    # "positionChanged":Z
    .end local v3    # "result":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->isIsoIec646()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->parseIsoIec646Block()Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;

    move-result-object v3

    .line 149
    .restart local v3    # "result":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;
    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;->isFinished()Z

    move-result v1

    .restart local v1    # "isFinished":Z
    goto :goto_0

    .line 151
    .end local v1    # "isFinished":Z
    .end local v3    # "result":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->parseNumericBlock()Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;

    move-result-object v3

    .line 152
    .restart local v3    # "result":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;
    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;->isFinished()Z

    move-result v1

    .restart local v1    # "isFinished":Z
    goto :goto_0

    .line 155
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 159
    .restart local v2    # "positionChanged":Z
    :cond_4
    if-eqz v1, :cond_0

    goto :goto_2
.end method

.method private parseIsoIec646Block()Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;
    .locals 4

    .prologue
    .line 195
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->isStillIsoIec646(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 196
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->decodeIsoIec646(I)Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;

    move-result-object v1

    .line 197
    .local v1, "iso":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;->getNewPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->setPosition(I)V

    .line 199
    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;->isFNC1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 200
    new-instance v0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;-><init>(ILjava/lang/String;)V

    .line 201
    .local v0, "information":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;-><init>(Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;Z)V

    .line 218
    .end local v0    # "information":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    .end local v1    # "iso":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;
    :goto_1
    return-object v2

    .line 203
    .restart local v1    # "iso":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;->getValue()C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 206
    .end local v1    # "iso":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedChar;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->isAlphaOr646ToNumericLatch(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 207
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->incrementPosition(I)V

    .line 208
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->setNumeric()V

    .line 218
    :cond_2
    :goto_2
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;-><init>(Z)V

    goto :goto_1

    .line 209
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->isAlphaTo646ToAlphaLatch(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 210
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    add-int/lit8 v2, v2, 0x5

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 211
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->incrementPosition(I)V

    .line 216
    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->setAlpha()V

    goto :goto_2

    .line 213
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->setPosition(I)V

    goto :goto_3
.end method

.method private parseNumericBlock()Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 165
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->isStillNumeric(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->decodeNumeric(I)Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;

    move-result-object v1

    .line 167
    .local v1, "numeric":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;->getNewPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->setPosition(I)V

    .line 169
    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;->isFirstDigitFNC1()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 171
    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;->isSecondDigitFNC1()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 172
    new-instance v0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;-><init>(ILjava/lang/String;)V

    .line 176
    .local v0, "information":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    :goto_1
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;

    invoke-direct {v2, v0, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;-><init>(Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;Z)V

    .line 191
    .end local v0    # "information":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    .end local v1    # "numeric":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;
    :goto_2
    return-object v2

    .line 174
    .restart local v1    # "numeric":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;
    :cond_0
    new-instance v0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;->getSecondDigit()I

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;-><init>(ILjava/lang/String;I)V

    .restart local v0    # "information":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    goto :goto_1

    .line 178
    .end local v0    # "information":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;->getFirstDigit()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 180
    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;->isSecondDigitFNC1()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 181
    new-instance v0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;-><init>(ILjava/lang/String;)V

    .line 182
    .restart local v0    # "information":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;

    invoke-direct {v2, v0, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;-><init>(Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;Z)V

    goto :goto_2

    .line 184
    .end local v0    # "information":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;->getSecondDigit()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 187
    .end local v1    # "numeric":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedNumeric;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->isNumericToAlphaNumericLatch(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 188
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->setAlpha()V

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->incrementPosition(I)V

    .line 191
    :cond_4
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;-><init>(Z)V

    goto :goto_2
.end method


# virtual methods
.method decodeAllCodes(Ljava/lang/StringBuilder;I)Ljava/lang/String;
    .locals 5
    .param p1, "buff"    # Ljava/lang/StringBuilder;
    .param p2, "initialPosition"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 48
    move v0, p2

    .line 49
    .local v0, "currentPosition":I
    const/4 v3, 0x0

    .line 51
    .local v3, "remaining":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v0, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->decodeGeneralPurposeField(ILjava/lang/String;)Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

    move-result-object v1

    .line 52
    .local v1, "info":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->getNewString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/FieldParser;->parseFieldsInGeneralPurpose(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 53
    .local v2, "parsedFields":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 54
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->isRemaining()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 57
    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->getRemainingValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 62
    :goto_1
    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->getNewPosition()I

    move-result v4

    if-ne v0, v4, :cond_2

    .line 68
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 59
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 65
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->getNewPosition()I

    move-result v0

    .line 66
    goto :goto_0
.end method

.method decodeGeneralPurposeField(ILjava/lang/String;)Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    .locals 5
    .param p1, "pos"    # I
    .param p2, "remaining"    # Ljava/lang/String;

    .prologue
    .line 123
    iget-object v1, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 125
    if-eqz p2, :cond_0

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->setPosition(I)V

    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->parseBlocks()Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

    move-result-object v0

    .line 132
    .local v0, "lastDecoded":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->isRemaining()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 133
    new-instance v1, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->getRemainingValue()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;-><init>(ILjava/lang/String;I)V

    .line 135
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->current:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->getPosition()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;-><init>(ILjava/lang/String;)V

    goto :goto_0
.end method

.method extractNumericValueFromBitArray(II)I
    .locals 1
    .param p1, "pos"    # I
    .param p2, "bits"    # I

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-static {v0, p1, p2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(Lcom/sec/android/app/camaftest/core/common/BitArray;II)I

    move-result v0

    return v0
.end method
