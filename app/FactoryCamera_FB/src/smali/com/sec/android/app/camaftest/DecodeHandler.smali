.class final Lcom/sec/android/app/camaftest/DecodeHandler;
.super Landroid/os/Handler;
.source "DecodeHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final activity:Lcom/sec/android/app/camaftest/CaptureActivity;

.field private final multiFormatReader:Lcom/sec/android/app/camaftest/core/MultiFormatReader;

.field private running:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/android/app/camaftest/DecodeHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/DecodeHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/sec/android/app/camaftest/CaptureActivity;Ljava/util/Map;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/app/camaftest/CaptureActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/camaftest/CaptureActivity;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p2, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;Ljava/lang/Object;>;"
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camaftest/DecodeHandler;->running:Z

    .line 49
    new-instance v0, Lcom/sec/android/app/camaftest/core/MultiFormatReader;

    invoke-direct {v0}, Lcom/sec/android/app/camaftest/core/MultiFormatReader;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/DecodeHandler;->multiFormatReader:Lcom/sec/android/app/camaftest/core/MultiFormatReader;

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/camaftest/DecodeHandler;->multiFormatReader:Lcom/sec/android/app/camaftest/core/MultiFormatReader;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->setHints(Ljava/util/Map;)V

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/camaftest/DecodeHandler;->activity:Lcom/sec/android/app/camaftest/CaptureActivity;

    .line 52
    return-void
.end method

.method private decode([BII)V
    .locals 22
    .param p1, "data"    # [B
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 79
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 80
    .local v20, "start":J
    const/16 v17, 0x0

    .line 81
    .local v17, "rawResult":Lcom/sec/android/app/camaftest/core/Result;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camaftest/DecodeHandler;->activity:Lcom/sec/android/app/camaftest/CaptureActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/camaftest/CaptureActivity;->getCameraManager()Lcom/sec/android/app/camaftest/camera/CameraManager;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2}, Lcom/sec/android/app/camaftest/camera/CameraManager;->buildLuminanceSource([BII)Lcom/sec/android/app/camaftest/PlanarYUVLuminanceSource;

    move-result-object v18

    .line 82
    .local v18, "source":Lcom/sec/android/app/camaftest/PlanarYUVLuminanceSource;
    if-eqz v18, :cond_0

    .line 83
    new-instance v11, Lcom/sec/android/app/camaftest/core/BinaryBitmap;

    new-instance v5, Lcom/sec/android/app/camaftest/core/common/HybridBinarizer;

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Lcom/sec/android/app/camaftest/core/common/HybridBinarizer;-><init>(Lcom/sec/android/app/camaftest/core/LuminanceSource;)V

    invoke-direct {v11, v5}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;-><init>(Lcom/sec/android/app/camaftest/core/Binarizer;)V

    .line 85
    .local v11, "bitmap":Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camaftest/DecodeHandler;->multiFormatReader:Lcom/sec/android/app/camaftest/core/MultiFormatReader;

    invoke-virtual {v5, v11}, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->decodeWithState(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;
    :try_end_0
    .catch Lcom/sec/android/app/camaftest/core/ReaderException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v17

    .line 89
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camaftest/DecodeHandler;->multiFormatReader:Lcom/sec/android/app/camaftest/core/MultiFormatReader;

    invoke-virtual {v5}, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->reset()V

    .line 93
    .end local v11    # "bitmap":Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camaftest/DecodeHandler;->activity:Lcom/sec/android/app/camaftest/CaptureActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/camaftest/CaptureActivity;->getHandler()Landroid/os/Handler;

    move-result-object v13

    .line 94
    .local v13, "handler":Landroid/os/Handler;
    if-eqz v17, :cond_2

    .line 96
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 97
    .local v14, "end":J
    sget-object v5, Lcom/sec/android/app/camaftest/DecodeHandler;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Found barcode in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v8, v14, v20

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    if-eqz v13, :cond_1

    .line 99
    new-instance v10, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v10}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 100
    .local v10, "baos":Ljava/io/ByteArrayOutputStream;
    new-instance v4, Landroid/graphics/YuvImage;

    const/16 v6, 0x11

    const/4 v9, 0x0

    move-object/from16 v5, p1

    move/from16 v7, p2

    move/from16 v8, p3

    invoke-direct/range {v4 .. v9}, Landroid/graphics/YuvImage;-><init>([BIII[I)V

    .line 101
    .local v4, "yuvimage":Landroid/graphics/YuvImage;
    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-direct {v5, v6, v7, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    const/16 v6, 0x64

    invoke-virtual {v4, v5, v6, v10}, Landroid/graphics/YuvImage;->compressToJpeg(Landroid/graphics/Rect;ILjava/io/OutputStream;)Z

    .line 103
    const v5, 0x7f090003

    move-object/from16 v0, v17

    invoke-static {v13, v5, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v16

    .line 104
    .local v16, "message":Landroid/os/Message;
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 105
    .local v12, "bundle":Landroid/os/Bundle;
    const-string v5, "preview_jpg_data"

    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v12, v5, v6}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 106
    const-string v5, "barcode_bitmap"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/camaftest/PlanarYUVLuminanceSource;->renderCroppedGreyscaleBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v12, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 107
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 108
    invoke-virtual/range {v16 .. v16}, Landroid/os/Message;->sendToTarget()V

    .line 111
    :try_start_1
    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 122
    .end local v4    # "yuvimage":Landroid/graphics/YuvImage;
    .end local v10    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v12    # "bundle":Landroid/os/Bundle;
    .end local v14    # "end":J
    .end local v16    # "message":Landroid/os/Message;
    :cond_1
    :goto_1
    return-void

    .line 86
    .end local v13    # "handler":Landroid/os/Handler;
    .restart local v11    # "bitmap":Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    :catch_0
    move-exception v5

    .line 89
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/camaftest/DecodeHandler;->multiFormatReader:Lcom/sec/android/app/camaftest/core/MultiFormatReader;

    invoke-virtual {v5}, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->reset()V

    goto/16 :goto_0

    :catchall_0
    move-exception v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/camaftest/DecodeHandler;->multiFormatReader:Lcom/sec/android/app/camaftest/core/MultiFormatReader;

    invoke-virtual {v6}, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->reset()V

    throw v5

    .line 117
    .end local v11    # "bitmap":Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .restart local v13    # "handler":Landroid/os/Handler;
    :cond_2
    if-eqz v13, :cond_1

    .line 118
    const v5, 0x7f090002

    invoke-static {v13, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v16

    .line 119
    .restart local v16    # "message":Landroid/os/Message;
    invoke-virtual/range {v16 .. v16}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    .line 112
    .restart local v4    # "yuvimage":Landroid/graphics/YuvImage;
    .restart local v10    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v12    # "bundle":Landroid/os/Bundle;
    .restart local v14    # "end":J
    :catch_1
    move-exception v5

    goto :goto_1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/android/app/camaftest/DecodeHandler;->running:Z

    if-nez v0, :cond_0

    .line 68
    :goto_0
    return-void

    .line 59
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 61
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/camaftest/DecodeHandler;->decode([BII)V

    goto :goto_0

    .line 64
    :sswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camaftest/DecodeHandler;->running:Z

    .line 65
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto :goto_0

    .line 59
    :sswitch_data_0
    .sparse-switch
        0x7f090001 -> :sswitch_0
        0x7f090005 -> :sswitch_1
    .end sparse-switch
.end method
