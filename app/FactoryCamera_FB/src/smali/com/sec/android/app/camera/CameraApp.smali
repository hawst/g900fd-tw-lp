.class public Lcom/sec/android/app/camera/CameraApp;
.super Landroid/app/Application;
.source "CameraApp.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CameraApp"

.field static mFeature:Lcom/sec/android/app/camera/Feature;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static getAppFeature()Lcom/sec/android/app/camera/Feature;
    .locals 2

    .prologue
    .line 18
    const-string v0, "CameraApp"

    const-string v1, "CameraApp.getAppFeature()..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    sget-object v0, Lcom/sec/android/app/camera/CameraApp;->mFeature:Lcom/sec/android/app/camera/Feature;

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 12
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 13
    const-string v0, "CameraApp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CameraApp.onCreate()... mFeature : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camera/CameraApp;->mFeature:Lcom/sec/android/app/camera/Feature;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 14
    new-instance v0, Lcom/sec/android/app/camera/Feature;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/Feature;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/camera/CameraApp;->mFeature:Lcom/sec/android/app/camera/Feature;

    .line 15
    return-void
.end method
