.class public final Lcom/sec/android/app/camaftest/core/client/result/ISBNParsedResult;
.super Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;
.source "ISBNParsedResult.java"


# instance fields
.field private final isbn:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "isbn"    # Ljava/lang/String;

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/camaftest/core/client/result/ParsedResultType;->ISBN:Lcom/sec/android/app/camaftest/core/client/result/ParsedResultType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;-><init>(Lcom/sec/android/app/camaftest/core/client/result/ParsedResultType;)V

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/camaftest/core/client/result/ISBNParsedResult;->isbn:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public getDisplayResult()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/client/result/ISBNParsedResult;->isbn:Ljava/lang/String;

    return-object v0
.end method

.method public getISBN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/client/result/ISBNParsedResult;->isbn:Ljava/lang/String;

    return-object v0
.end method
