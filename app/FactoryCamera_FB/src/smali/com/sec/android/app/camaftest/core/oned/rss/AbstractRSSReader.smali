.class public abstract Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;
.super Lcom/sec/android/app/camaftest/core/oned/OneDReader;
.source "AbstractRSSReader.java"


# static fields
.field private static final MAX_AVG_VARIANCE:I = 0x33

.field private static final MAX_FINDER_PATTERN_RATIO:F = 0.89285713f

.field private static final MAX_INDIVIDUAL_VARIANCE:I = 0x66

.field private static final MIN_FINDER_PATTERN_RATIO:F = 0.7916667f


# instance fields
.field private final dataCharacterCounters:[I

.field private final decodeFinderCounters:[I

.field private final evenCounts:[I

.field private final evenRoundingErrors:[F

.field private final oddCounts:[I

.field private final oddRoundingErrors:[F


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/oned/OneDReader;-><init>()V

    .line 38
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->decodeFinderCounters:[I

    .line 39
    const/16 v0, 0x8

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->dataCharacterCounters:[I

    .line 40
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->oddRoundingErrors:[F

    .line 41
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->evenRoundingErrors:[F

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->dataCharacterCounters:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->oddCounts:[I

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->dataCharacterCounters:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->evenCounts:[I

    .line 44
    return-void
.end method

.method protected static count([I)I
    .locals 5
    .param p0, "array"    # [I

    .prologue
    .line 80
    const/4 v2, 0x0

    .line 81
    .local v2, "count":I
    move-object v1, p0

    .local v1, "arr$":[I
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget v0, v1, v3

    .line 82
    .local v0, "a":I
    add-int/2addr v2, v0

    .line 81
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 84
    .end local v0    # "a":I
    :cond_0
    return v2
.end method

.method protected static decrement([I[F)V
    .locals 4
    .param p0, "array"    # [I
    .param p1, "errors"    # [F

    .prologue
    .line 100
    const/4 v2, 0x0

    .line 101
    .local v2, "index":I
    const/4 v3, 0x0

    aget v0, p1, v3

    .line 102
    .local v0, "biggestError":F
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_1

    .line 103
    aget v3, p1, v1

    cmpg-float v3, v3, v0

    if-gez v3, :cond_0

    .line 104
    aget v0, p1, v1

    .line 105
    move v2, v1

    .line 102
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 108
    :cond_1
    aget v3, p0, v2

    add-int/lit8 v3, v3, -0x1

    aput v3, p0, v2

    .line 109
    return-void
.end method

.method protected static increment([I[F)V
    .locals 4
    .param p0, "array"    # [I
    .param p1, "errors"    # [F

    .prologue
    .line 88
    const/4 v2, 0x0

    .line 89
    .local v2, "index":I
    const/4 v3, 0x0

    aget v0, p1, v3

    .line 90
    .local v0, "biggestError":F
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_1

    .line 91
    aget v3, p1, v1

    cmpl-float v3, v3, v0

    if-lez v3, :cond_0

    .line 92
    aget v0, p1, v1

    .line 93
    move v2, v1

    .line 90
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    :cond_1
    aget v3, p0, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, p0, v2

    .line 97
    return-void
.end method

.method protected static isFinderPattern([I)Z
    .locals 13
    .param p0, "counters"    # [I

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 112
    aget v11, p0, v10

    aget v12, p0, v9

    add-int v2, v11, v12

    .line 113
    .local v2, "firstTwoSum":I
    const/4 v11, 0x2

    aget v11, p0, v11

    add-int/2addr v11, v2

    const/4 v12, 0x3

    aget v12, p0, v12

    add-int v8, v11, v12

    .line 114
    .local v8, "sum":I
    int-to-float v11, v2

    int-to-float v12, v8

    div-float v7, v11, v12

    .line 115
    .local v7, "ratio":F
    const v11, 0x3f4aaaab

    cmpl-float v11, v7, v11

    if-ltz v11, :cond_4

    const v11, 0x3f649249

    cmpg-float v11, v7, v11

    if-gtz v11, :cond_4

    .line 117
    const v6, 0x7fffffff

    .line 118
    .local v6, "minCounter":I
    const/high16 v5, -0x80000000

    .line 119
    .local v5, "maxCounter":I
    move-object v0, p0

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget v1, v0, v3

    .line 120
    .local v1, "counter":I
    if-le v1, v5, :cond_0

    .line 121
    move v5, v1

    .line 123
    :cond_0
    if-ge v1, v6, :cond_1

    .line 124
    move v6, v1

    .line 119
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 127
    .end local v1    # "counter":I
    :cond_2
    mul-int/lit8 v11, v6, 0xa

    if-ge v5, v11, :cond_3

    .line 129
    .end local v0    # "arr$":[I
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "maxCounter":I
    .end local v6    # "minCounter":I
    :goto_1
    return v9

    .restart local v0    # "arr$":[I
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    .restart local v5    # "maxCounter":I
    .restart local v6    # "minCounter":I
    :cond_3
    move v9, v10

    .line 127
    goto :goto_1

    .end local v0    # "arr$":[I
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "maxCounter":I
    .end local v6    # "minCounter":I
    :cond_4
    move v9, v10

    .line 129
    goto :goto_1
.end method

.method protected static parseFinderValue([I[[I)I
    .locals 3
    .param p0, "counters"    # [I
    .param p1, "finderPatterns"    # [[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 71
    const/4 v0, 0x0

    .local v0, "value":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 72
    aget-object v1, p1, v0

    const/16 v2, 0x66

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->patternMatchVariance([I[II)I

    move-result v1

    const/16 v2, 0x33

    if-ge v1, v2, :cond_0

    .line 73
    return v0

    .line 71
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_1
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v1

    throw v1
.end method


# virtual methods
.method protected final getDataCharacterCounters()[I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->dataCharacterCounters:[I

    return-object v0
.end method

.method protected final getDecodeFinderCounters()[I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->decodeFinderCounters:[I

    return-object v0
.end method

.method protected final getEvenCounts()[I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->evenCounts:[I

    return-object v0
.end method

.method protected final getEvenRoundingErrors()[F
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->evenRoundingErrors:[F

    return-object v0
.end method

.method protected final getOddCounts()[I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->oddCounts:[I

    return-object v0
.end method

.method protected final getOddRoundingErrors()[F
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/AbstractRSSReader;->oddRoundingErrors:[F

    return-object v0
.end method
