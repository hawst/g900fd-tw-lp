.class public Lcom/sec/android/app/camera/framework/CameraSettings;
.super Ljava/lang/Object;
.source "CameraSettings.java"


# static fields
.field public static final AUTO_MODE_FLASH_OFF:I = 0x2

.field public static final AUTO_TEST_RESULT_NG1:I = 0x1

.field public static final AUTO_TEST_RESULT_NG2:I = 0x2

.field public static final AUTO_TEST_RESULT_NG3:I = 0x3

.field public static final AUTO_TEST_RESULT_NG4:I = 0x4

.field public static final AUTO_TEST_RESULT_OK:I = 0x0

.field public static final BACKKEY_CANCELED:Z = true

.field public static final CAMERA_AUTO_CAPTURE_FALSE:Z = false

.field public static final CAMERA_NONE:I = -0x1

.field public static final CAMERA_OPEN_ERROR:I = 0x7

.field public static final CANNOT_STAT_ERROR:I = -0x2

.field public static final CHECK_DATALINE_DONE:I = 0x6

.field public static final CLEAR_FOCUS_STATE:I = 0x8

.field public static final CLEAR_SCREEN_DELAY:I = 0x4

.field public static final CLOSE_CAMERA:I = 0xa

.field public static final DEBUG:Z = true

.field public static final DEBUG_TIME_OPERATIONS:Z = true

.field public static final DIALOG_ERROR_POPUP:I = 0x9

.field public static final FINISH_TEST:Z = true

.field public static final FLASH_AUTO:Ljava/lang/String; = "auto"

.field public static final FLASH_DISABLE:I = 0x0

.field public static final FLASH_ENABLE:I = 0x1

.field public static final FLASH_OFF:Ljava/lang/String; = "off"

.field public static final FLASH_ON:Ljava/lang/String; = "on"

.field public static final FLASH_TORCH:Ljava/lang/String; = "torch"

.field public static final FOCUSING:I = 0x1

.field public static final FOCUSING_SNAP_ON_FINISH:I = 0x2

.field public static final FOCUS_BEEP_VOLUME:I = 0x55

.field public static final FOCUS_FAIL:I = 0x4

.field public static final FOCUS_NOT_STARTED:I = 0x0

.field public static final FOCUS_SUCCESS:I = 0x3

.field public static final FRONT_CAMERA:I = 0x1

.field public static final KEY_CAMCORDER_PREVIEW_TEST:Ljava/lang/String; = "camcorder_preview_test"

.field public static final KEY_CAMCORDER_PREVIEW_TEST_BY_BACKKEY_CANCELED:Ljava/lang/String; = "camcorder_preview_test_backkey_canceled"

.field public static final KEY_CAMERA_AUTO_CAPTURE:Ljava/lang/String; = "auto_capture"

.field public static final KEY_CAMERA_ID:Ljava/lang/String; = "camera_id"

.field public static final KEY_FLASH_ENABLE:Ljava/lang/String; = "torch_on"

.field public static final KEY_FLASH_MODE:Ljava/lang/String; = "flash-mode"

.field public static final KEY_LOW_LIGHT_CAPTURE_DISPLAY:Ljava/lang/String; = "low_light_capture_display"

.field public static final KEY_MOVIE_INDEX_COUNT:Ljava/lang/String; = "movie_index_count"

.field public static final KEY_OIS_TEST:Ljava/lang/String; = "ois_test"

.field public static final KEY_OIS_TEST_BY_BACKKEY_CANCELED:Ljava/lang/String; = "ois_test_backkey_canceled"

.field public static final KEY_OMMISION_TEST:Ljava/lang/String; = "ommision_test"

.field public static final KEY_ORIENTATION_REVERSE:Ljava/lang/String; = "orientation_reverse"

.field public static final KEY_PICTURE_SIZE:Ljava/lang/String; = "picture-size"

.field public static final KEY_POSTVIEW_TEST:Ljava/lang/String; = "postview_test"

.field public static final KEY_TEST_TYPE:Ljava/lang/String; = "test_type"

.field public static final KEY_USB_OR_UART_COMMAND:Ljava/lang/String; = "usb_or_uart_command"

.field public static final LOW_LIGHT_CAPTURE_DISPLAY_FALSE:Z = false

.field public static final LOW_LIGHT_CAPTURE_DISPLAY_TRUE:Z = true

.field public static final MDNIE_CAMERA_MODE:I = 0x4

.field public static final MDNIE_UI_MODE:I = 0x0

.field public static final MILLIS_IN_SEC:I = 0x3e8

.field public static final NOT_BACKKEY_CANCELED:Z = false

.field public static final NOT_FINISH_TEST:Z = false

.field public static final NOT_REVERSE:Z = false

.field public static final NO_STORAGE_ERROR:I = -0x1

.field public static final OMMISION_TEST_FALSE:Z = false

.field public static final OMMISION_TEST_TRUE:Z = true

.field public static final REAR_CAMERA:I = 0x0

.field public static final RESTART_PREVIEW:I = 0x3

.field public static final RES_AUTOFOCUS_CANCELED:I = 0x2

.field public static final RES_AUTOFOCUS_FAILED:I = 0x0

.field public static final RES_AUTOFOCUS_SUCCESS:I = 0x1

.field public static final REVERSE:Z = true

.field public static final SCREEN_DELAY:I = 0x2

.field public static final SEC_IN_MINUTE:I = 0x3c

.field public static final SIZE_1600_1200:I = 0x2

.field public static final SIZE_2048_1536:I = 0x1

.field public static final SIZE_240_400:I = 0x4

.field public static final SIZE_2560_1920:I = 0x0

.field public static final SIZE_320_240:I = 0x5

.field public static final SIZE_640_480:I = 0x3

.field public static final SNAPSHOT_COMPLETED:I = 0x4

.field public static final SNAPSHOT_IN_IDLE:I = 0x1

.field public static final SNAPSHOT_IN_PROGRESS:I = 0x2

.field public static final SNAPSHOT_SAVING_IMAGE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "CameraSettings"

.field public static final TEST_TYPE_AUTO:Ljava/lang/String; = "autotest"

.field public static final TEST_TYPE_SELF:Ljava/lang/String; = "selftest"

.field public static final TIMEOUT_DELAY:I = 0x2

.field public static final TIMER_CAMCORDER_AUTO_RECORDING_CHECK:I = 0x21

.field public static final TIMER_CAMCORDER_STOP:I = 0x1f

.field public static final TIMER_EXPIRED:I = 0x1

.field public static final TIMER_EXPIRED_CAPTUREBLOCK:I = 0x2

.field public static final TIMER_START_AUTO_CAPTURE:I = 0x15

.field public static final TIMER_START_CAMCORDER_AUTO_RECORDING:I = 0x20

.field public static final TIMER_START_PREVIEW:I = 0xb


# instance fields
.field Feature:Lcom/sec/android/app/camera/Feature;

.field public mContext:Landroid/content/Context;

.field private mFocusMode:Ljava/lang/String;

.field private mUserXmlLoadingStatus:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 3
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->mUserXmlLoadingStatus:Z

    .line 166
    const-string v1, "CameraSettings"

    const-string v2, "CameraSettings..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iput-object p1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->mContext:Landroid/content/Context;

    .line 168
    invoke-static {}, Lcom/sec/android/app/camera/CameraApp;->getAppFeature()Lcom/sec/android/app/camera/Feature;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    if-eqz v1, :cond_0

    .line 172
    :try_start_0
    const-string v1, "CameraSettings"

    const-string v2, "CameraSettings readUserXml..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Feature;->readUserXml()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->mUserXmlLoadingStatus:Z

    .line 175
    iget-boolean v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->mUserXmlLoadingStatus:Z

    if-nez v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/Feature;->readInternalDefaultXml()V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 179
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v1, "CameraSettings"

    const-string v2, "CameraSettings readUserXml XmlPullParserException....."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_0

    .line 183
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v0

    .line 185
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "CameraSettings"

    const-string v2, "CameraSettings readUserXml IOException....."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static hasCamera(I)Z
    .locals 1
    .param p0, "camera_id"    # I

    .prologue
    const/4 v0, 0x1

    .line 199
    if-nez p0, :cond_1

    .line 204
    :cond_0
    :goto_0
    return v0

    .line 201
    :cond_1
    if-ne v0, p0, :cond_0

    goto :goto_0
.end method

.method public static isSupported(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2
    .param p0, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "supported":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 354
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public findOptimalPreviewSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;
    .locals 18
    .param p2, "targetRatio"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;D)",
            "Landroid/hardware/Camera$Size;"
        }
    .end annotation

    .prologue
    .line 284
    .local p1, "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    .line 285
    .local v2, "ASPECT_TOLERANCE":D
    if-nez p1, :cond_1

    .line 286
    const/4 v8, 0x0

    .line 333
    :cond_0
    return-object v8

    .line 288
    :cond_1
    const/4 v8, 0x0

    .line 289
    .local v8, "optimalSize":Landroid/hardware/Camera$Size;
    const-wide v6, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 297
    .local v6, "minDiff":D
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camera/framework/CameraSettings;->mContext:Landroid/content/Context;

    const-string v14, "window"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/WindowManager;

    invoke-interface {v13}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    .line 300
    .local v4, "display":Landroid/view/Display;
    invoke-virtual {v4}, Landroid/view/Display;->getHeight()I

    move-result v13

    invoke-virtual {v4}, Landroid/view/Display;->getWidth()I

    move-result v14

    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 302
    .local v12, "targetHeight":I
    const-string v13, "CameraSettings"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "display.getHeight() = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v4}, Landroid/view/Display;->getHeight()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " display.getWidth() = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v4}, Landroid/view/Display;->getWidth()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    if-gtz v12, :cond_2

    .line 307
    invoke-virtual {v4}, Landroid/view/Display;->getHeight()I

    move-result v12

    .line 311
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/hardware/Camera$Size;

    .line 312
    .local v9, "size":Landroid/hardware/Camera$Size;
    iget v13, v9, Landroid/hardware/Camera$Size;->width:I

    int-to-double v14, v13

    iget v13, v9, Landroid/hardware/Camera$Size;->height:I

    int-to-double v0, v13

    move-wide/from16 v16, v0

    div-double v10, v14, v16

    .line 313
    .local v10, "ratio":D
    sub-double v14, v10, p2

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    const-wide v16, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v13, v14, v16

    if-gtz v13, :cond_3

    .line 315
    iget v13, v9, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v13, v12

    invoke-static {v13}, Ljava/lang/Math;->abs(I)I

    move-result v13

    int-to-double v14, v13

    cmpg-double v13, v14, v6

    if-gez v13, :cond_3

    .line 316
    move-object v8, v9

    .line 317
    iget v13, v9, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v13, v12

    invoke-static {v13}, Ljava/lang/Math;->abs(I)I

    move-result v13

    int-to-double v6, v13

    goto :goto_0

    .line 323
    .end local v9    # "size":Landroid/hardware/Camera$Size;
    .end local v10    # "ratio":D
    :cond_4
    if-nez v8, :cond_0

    .line 324
    const-string v13, "CameraSettings"

    const-string v14, "No preview size match the aspect ratio"

    invoke-static {v13, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    const-wide v6, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 326
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/hardware/Camera$Size;

    .line 327
    .restart local v9    # "size":Landroid/hardware/Camera$Size;
    iget v13, v9, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v13, v12

    invoke-static {v13}, Ljava/lang/Math;->abs(I)I

    move-result v13

    int-to-double v14, v13

    cmpg-double v13, v14, v6

    if-gez v13, :cond_5

    .line 328
    move-object v8, v9

    .line 329
    iget v13, v9, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v13, v12

    invoke-static {v13}, Ljava/lang/Math;->abs(I)I

    move-result v13

    int-to-double v6, v13

    goto :goto_1
.end method

.method public getFocusMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->mFocusMode:Ljava/lang/String;

    return-object v0
.end method

.method public getFocusMode(I)Ljava/lang/String;
    .locals 4
    .param p1, "camera_id"    # I

    .prologue
    .line 339
    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->SUPPORT_AF_R:Z

    if-eqz v1, :cond_0

    .line 340
    const-string v0, "auto"

    .line 349
    .local v0, "focusMode":Ljava/lang/String;
    :goto_0
    const-string v1, "CameraSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFocusMode - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    return-object v0

    .line 342
    .end local v0    # "focusMode":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->SUPPORT_FOCUS_INFINITY:Z

    if-eqz v1, :cond_1

    .line 343
    const-string v0, "infinity"

    .restart local v0    # "focusMode":Ljava/lang/String;
    goto :goto_0

    .line 345
    .end local v0    # "focusMode":Ljava/lang/String;
    :cond_1
    const-string v0, "fixed"

    .restart local v0    # "focusMode":Ljava/lang/String;
    goto :goto_0
.end method

.method public getSupportedAutofocus()Z
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->SUPPORT_AF_R:Z

    return v0
.end method

.method public hasFlash()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->FLASH_EXIST:Z

    if-ne v1, v0, :cond_0

    .line 195
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasZoom(I)Z
    .locals 2
    .param p1, "camera_id"    # I

    .prologue
    const/4 v0, 0x0

    .line 209
    if-nez p1, :cond_1

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->SUPPORT_ZOOM_R:Z

    .line 214
    :cond_0
    :goto_0
    return v0

    .line 211
    :cond_1
    const/4 v1, 0x1

    if-ne v1, p1, :cond_0

    goto :goto_0
.end method

.method public isSupportedFocusModes(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 372
    .local p2, "focusmodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1, p2}, Lcom/sec/android/app/camera/framework/CameraSettings;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    const/4 v0, 0x1

    .line 377
    :goto_0
    return v0

    .line 375
    :cond_0
    const-string v0, "CameraSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "not support ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSupportedFocusModes(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "focusmodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x1

    .line 358
    const-string v1, "auto"

    invoke-static {v1, p1}, Lcom/sec/android/app/camera/framework/CameraSettings;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 359
    const-string v1, "auto"

    iput-object v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->mFocusMode:Ljava/lang/String;

    .line 368
    :goto_0
    return v0

    .line 361
    :cond_0
    const-string v1, "fixed"

    invoke-static {v1, p1}, Lcom/sec/android/app/camera/framework/CameraSettings;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 362
    const-string v1, "fixed"

    iput-object v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->mFocusMode:Ljava/lang/String;

    goto :goto_0

    .line 364
    :cond_1
    const-string v1, "infinity"

    invoke-static {v1, p1}, Lcom/sec/android/app/camera/framework/CameraSettings;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 365
    const-string v1, "infinity"

    iput-object v1, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->mFocusMode:Ljava/lang/String;

    goto :goto_0

    .line 368
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needToCheckCamcorderPreviewTest()Z
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_PREVIEW_TEST:Z

    return v0
.end method

.method public needToCheckDTP(I)Z
    .locals 3
    .param p1, "camera_id"    # I

    .prologue
    const/4 v1, 0x1

    .line 219
    iget-object v2, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v0, v2, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    .line 220
    .local v0, "data_output":I
    if-nez p1, :cond_2

    .line 221
    iget-object v2, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v0, v2, Lcom/sec/android/app/camera/Feature;->BACK_CAMERA_OUTPUT:I

    .line 226
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v2, v2, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    if-ne v0, v2, :cond_1

    .line 227
    const/4 v1, 0x0

    .line 229
    :cond_1
    return v1

    .line 222
    :cond_2
    if-ne p1, v1, :cond_0

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v0, v2, Lcom/sec/android/app/camera/Feature;->FRONT_CAMERA_OUTPUT:I

    goto :goto_0
.end method

.method public needToCheckOISTest()Z
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->CHECK_OIS_TEST:Z

    return v0
.end method

.method public setPictureSize(ILandroid/hardware/Camera$Parameters;)V
    .locals 12
    .param p1, "camera_id"    # I
    .param p2, "parameters"    # Landroid/hardware/Camera$Parameters;

    .prologue
    const/4 v11, 0x1

    .line 246
    invoke-virtual {p2}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v5

    .line 247
    .local v5, "supported":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    const-string v8, "CameraSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CameraSettings.setPictureSize() supported : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    if-nez v5, :cond_1

    .line 249
    if-ne p1, v11, :cond_0

    .line 250
    const-string v8, "picture-size"

    iget-object v9, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-object v9, v9, Lcom/sec/android/app/camera/Feature;->PICTURE_RESOLUTION_F:Ljava/lang/String;

    invoke-virtual {p2, v8, v9}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :goto_0
    return-void

    .line 252
    :cond_0
    const-string v8, "picture-size"

    iget-object v9, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-object v9, v9, Lcom/sec/android/app/camera/Feature;->PICTURE_RESOLUTION_R:Ljava/lang/String;

    invoke-virtual {p2, v8, v9}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 255
    :cond_1
    const/4 v7, 0x0

    .line 256
    .local v7, "width":I
    const/4 v0, 0x0

    .line 257
    .local v0, "height":I
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/Camera$Size;

    .line 258
    .local v4, "size":Landroid/hardware/Camera$Size;
    iget v8, v4, Landroid/hardware/Camera$Size;->width:I

    iget v9, v4, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v8, v9

    mul-int v9, v7, v0

    if-le v8, v9, :cond_2

    .line 259
    iget v7, v4, Landroid/hardware/Camera$Size;->width:I

    .line 260
    iget v0, v4, Landroid/hardware/Camera$Size;->height:I

    .line 261
    const-string v8, "CameraSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CameraSettings.setPictureSize() width : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", height : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 264
    .end local v4    # "size":Landroid/hardware/Camera$Size;
    :cond_3
    const-string v8, "CameraSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setPictureSize - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "x"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    if-ne p1, v11, :cond_4

    iget-object v8, p0, Lcom/sec/android/app/camera/framework/CameraSettings;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v8, v8, Lcom/sec/android/app/camera/Feature;->CAMERA_FIXED_PICTURE_SIZE:Z

    if-nez v8, :cond_4

    .line 268
    invoke-virtual {p2}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v6

    .line 269
    .local v6, "supportedpreview":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    const/4 v3, 0x0

    .line 270
    .local v3, "previewsize":Landroid/hardware/Camera$Size;
    invoke-virtual {p2}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    .line 271
    .local v2, "picturesize":Landroid/hardware/Camera$Size;
    iget v8, v2, Landroid/hardware/Camera$Size;->width:I

    int-to-double v8, v8

    iget v10, v2, Landroid/hardware/Camera$Size;->height:I

    int-to-double v10, v10

    div-double/2addr v8, v10

    invoke-virtual {p0, v6, v8, v9}, Lcom/sec/android/app/camera/framework/CameraSettings;->findOptimalPreviewSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;

    move-result-object v3

    .line 273
    iget v7, v3, Landroid/hardware/Camera$Size;->width:I

    .line 274
    iget v0, v3, Landroid/hardware/Camera$Size;->height:I

    .line 275
    const-string v8, "CameraSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setPictureSize - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "x"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    .end local v2    # "picturesize":Landroid/hardware/Camera$Size;
    .end local v3    # "previewsize":Landroid/hardware/Camera$Size;
    .end local v6    # "supportedpreview":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_4
    invoke-virtual {p2, v7, v0}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    goto/16 :goto_0
.end method
