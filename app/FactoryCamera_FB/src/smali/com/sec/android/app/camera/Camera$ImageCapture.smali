.class Lcom/sec/android/app/camera/Camera$ImageCapture;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Lcom/sec/android/app/camera/Interface/Capturer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageCapture"
.end annotation


# instance fields
.field private mCapturing:Z

.field private mLastContentUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/sec/android/app/camera/Camera;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camera/Camera;)V
    .locals 1

    .prologue
    .line 1272
    iput-object p1, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1269
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->mCapturing:Z

    .line 1273
    return-void
.end method

.method private capture(Z)V
    .locals 7
    .param p1, "captureOnly"    # Z

    .prologue
    .line 1312
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/camera/Camera;->mPreviewing:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/camera/Camera;->access$5102(Lcom/sec/android/app/camera/Camera;Z)Z

    .line 1313
    const-string v2, "FactoryCamera"

    const-string v3, "capture : mPreviewing set false"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1314
    const/4 v0, 0x0

    .line 1316
    .local v0, "loc":Landroid/location/Location;
    const-string v2, "selftest"

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1317
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cameraType:I
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$1200(Lcom/sec/android/app/camera/Camera;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1318
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v2, v2, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->SET_MAX_SUPPORTED_PICTURE_SIZE:Z

    if-nez v2, :cond_1

    .line 1319
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v3, v3, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v3, v3, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_W:I

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v4, v4, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v4, v4, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_H:I

    invoke-virtual {v2, v3, v4}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 1331
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v2, v2, Lcom/sec/android/app/camera/Camera;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->SET_PARAM_PICTURE_ROTATION:Z

    if-eqz v2, :cond_0

    .line 1332
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v3}, Lcom/sec/android/app/camera/Camera;->getOrientationOnTake()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camera/Camera;->calculateOrientationForPicture(I)I

    move-result v1

    .line 1333
    .local v1, "rotation":I
    const-string v2, "FactoryCamera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setRotation param: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1334
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/hardware/Camera$Parameters;->setRotation(I)V

    .line 1337
    .end local v1    # "rotation":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 1339
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mShutterCallback:Lcom/sec/android/app/camera/Camera$ShutterCallback;
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$5200(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/Camera$ShutterCallback;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mRawPictureCallback:Lcom/sec/android/app/camera/Camera$RawPictureCallback;
    invoke-static {v4}, Lcom/sec/android/app/camera/Camera;->access$5300(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/Camera$RawPictureCallback;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;

    iget-object v6, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-direct {v5, v6, v0}, Lcom/sec/android/app/camera/Camera$JpegPictureCallback;-><init>(Lcom/sec/android/app/camera/Camera;Landroid/location/Location;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    .line 1341
    return-void

    .line 1322
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cameraType:I
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$1200(Lcom/sec/android/app/camera/Camera;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v4}, Lcom/sec/android/app/camera/Camera;->access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/camera/framework/CameraSettings;->setPictureSize(ILandroid/hardware/Camera$Parameters;)V

    goto :goto_0

    .line 1325
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cameraType:I
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$1200(Lcom/sec/android/app/camera/Camera;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v4}, Lcom/sec/android/app/camera/Camera;->access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/camera/framework/CameraSettings;->setPictureSize(ILandroid/hardware/Camera$Parameters;)V

    goto/16 :goto_0

    .line 1328
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/Camera;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cameraType:I
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$1200(Lcom/sec/android/app/camera/Camera;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v4}, Lcom/sec/android/app/camera/Camera;->access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/camera/framework/CameraSettings;->setPictureSize(ILandroid/hardware/Camera$Parameters;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public dismissFreezeFrame()V
    .locals 2

    .prologue
    .line 1288
    const-string v0, "FactoryCamera"

    const-string v1, "dismissFreezeFrame"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1289
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mStatus:I
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$400(Lcom/sec/android/app/camera/Camera;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1292
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$500(Lcom/sec/android/app/camera/Camera;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1296
    :goto_0
    return-void

    .line 1294
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->restartPreview()V
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$2500(Lcom/sec/android/app/camera/Camera;)V

    goto :goto_0
.end method

.method public getLastCaptureUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1276
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->mLastContentUri:Landroid/net/Uri;

    return-object v0
.end method

.method public initiate(Z)V
    .locals 1
    .param p1, "captureOnly"    # Z

    .prologue
    .line 1302
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1309
    :goto_0
    return-void

    .line 1306
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->mCapturing:Z

    .line 1308
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/Camera$ImageCapture;->capture(Z)V

    goto :goto_0
.end method

.method public onSnap()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 1344
    const-string v0, "FactoryCamera"

    const-string v1, "onSnap()"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1345
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mPausing:Z
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$2100(Lcom/sec/android/app/camera/Camera;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1346
    const-string v0, "FactoryCamera"

    const-string v1, "onSnap() - mPausing == true, return "

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1366
    :goto_0
    return-void

    .line 1350
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/camera/Camera;->mCaptureStartTime:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/camera/Camera;->access$1902(Lcom/sec/android/app/camera/Camera;J)J

    .line 1355
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mStatus:I
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$400(Lcom/sec/android/app/camera/Camera;)I

    move-result v0

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mStatus:I
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$400(Lcom/sec/android/app/camera/Camera;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1358
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$500(Lcom/sec/android/app/camera/Camera;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1362
    :cond_2
    const-string v0, "FactoryCamera"

    const-string v1, "mStatus = SNAPSHOT_IN_PROGRESS"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1363
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # setter for: Lcom/sec/android/app/camera/Camera;->mStatus:I
    invoke-static {v0, v4}, Lcom/sec/android/app/camera/Camera;->access$402(Lcom/sec/android/app/camera/Camera;I)I

    .line 1365
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mImageCapture:Lcom/sec/android/app/camera/Camera$ImageCapture;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4500(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/Camera$ImageCapture;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/Camera$ImageCapture;->initiate(Z)V

    goto :goto_0
.end method

.method public setCapturingLocked(Z)V
    .locals 0
    .param p1, "capturing"    # Z

    .prologue
    .line 1284
    iput-boolean p1, p0, Lcom/sec/android/app/camera/Camera$ImageCapture;->mCapturing:Z

    .line 1285
    return-void
.end method
