.class Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$2;
.super Ljava/lang/Object;
.source "CameraFirmwareUpdate_Booting.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$2;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 178
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    .line 181
    .local v0, "cameraCnt":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$2;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    const/4 v3, 0x0

    invoke-static {v3}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mCameraDevice:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    :goto_0
    return-void

    .line 182
    :catch_0
    move-exception v1

    .line 183
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "CameraFirmwareBootChecker"

    const-string v3, "service cannot connect. critical exception occured."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
