.class final Lcom/sec/android/app/camaftest/core/oned/rss/Pair;
.super Lcom/sec/android/app/camaftest/core/oned/rss/DataCharacter;
.source "Pair.java"


# instance fields
.field private count:I

.field private final finderPattern:Lcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;


# direct methods
.method constructor <init>(IILcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;)V
    .locals 0
    .param p1, "value"    # I
    .param p2, "checksumPortion"    # I
    .param p3, "finderPattern"    # Lcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/camaftest/core/oned/rss/DataCharacter;-><init>(II)V

    .line 26
    iput-object p3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/Pair;->finderPattern:Lcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;

    .line 27
    return-void
.end method


# virtual methods
.method getCount()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/Pair;->count:I

    return v0
.end method

.method getFinderPattern()Lcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/Pair;->finderPattern:Lcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;

    return-object v0
.end method

.method incrementCount()V
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/Pair;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/Pair;->count:I

    .line 39
    return-void
.end method
