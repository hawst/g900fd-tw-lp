.class public abstract Lcom/sec/android/app/camaftest/result/ResultHandler;
.super Ljava/lang/Object;
.source "ResultHandler.java"


# static fields
.field private static final ADDRESS_TYPE_STRINGS:[Ljava/lang/String;

.field private static final ADDRESS_TYPE_VALUES:[I

.field private static final DATE_FORMAT:Ljava/text/DateFormat;

.field private static final DATE_TIME_FORMAT:Ljava/text/DateFormat;

.field private static final EMAIL_TYPE_STRINGS:[Ljava/lang/String;

.field private static final EMAIL_TYPE_VALUES:[I

.field private static final MARKET_REFERRER_SUFFIX:Ljava/lang/String; = "&referrer=utm_source%3Dbarcodescanner%26utm_medium%3Dapps%26utm_campaign%3Dscan"

.field private static final MARKET_URI_PREFIX:Ljava/lang/String; = "market://details?id="

.field public static final MAX_BUTTON_COUNT:I = 0x4

.field private static final NO_TYPE:I = -0x1

.field private static final PHONE_TYPE_STRINGS:[Ljava/lang/String;

.field private static final PHONE_TYPE_VALUES:[I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final activity:Landroid/app/Activity;

.field private final customProductSearch:Ljava/lang/String;

.field private final rawResult:Lcom/sec/android/app/camaftest/core/Result;

.field private final result:Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 59
    const-class v0, Lcom/sec/android/app/camaftest/result/ResultHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->TAG:Ljava/lang/String;

    .line 63
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd"

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->DATE_FORMAT:Ljava/text/DateFormat;

    .line 67
    sget-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->DATE_FORMAT:Ljava/text/DateFormat;

    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 69
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd\'T\'HHmmss"

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->DATE_TIME_FORMAT:Ljava/text/DateFormat;

    .line 75
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "home"

    aput-object v1, v0, v4

    const-string v1, "work"

    aput-object v1, v0, v5

    const-string v1, "mobile"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->EMAIL_TYPE_STRINGS:[Ljava/lang/String;

    .line 76
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "home"

    aput-object v1, v0, v4

    const-string v1, "work"

    aput-object v1, v0, v5

    const-string v1, "mobile"

    aput-object v1, v0, v3

    const-string v1, "fax"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "pager"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "main"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->PHONE_TYPE_STRINGS:[Ljava/lang/String;

    .line 77
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "home"

    aput-object v1, v0, v4

    const-string v1, "work"

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->ADDRESS_TYPE_STRINGS:[Ljava/lang/String;

    .line 78
    new-array v0, v6, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->EMAIL_TYPE_VALUES:[I

    .line 83
    new-array v0, v7, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->PHONE_TYPE_VALUES:[I

    .line 91
    new-array v0, v3, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->ADDRESS_TYPE_VALUES:[I

    return-void

    .line 78
    :array_0
    .array-data 4
        0x1
        0x2
        0x4
    .end array-data

    .line 83
    :array_1
    .array-data 4
        0x1
        0x3
        0x2
        0x4
        0x6
        0xc
    .end array-data

    .line 91
    :array_2
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method constructor <init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "result"    # Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    .prologue
    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;-><init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;Lcom/sec/android/app/camaftest/core/Result;)V

    .line 106
    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;Lcom/sec/android/app/camaftest/core/Result;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "result"    # Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;
    .param p3, "rawResult"    # Lcom/sec/android/app/camaftest/core/Result;

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p2, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->result:Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    .line 110
    iput-object p1, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->activity:Landroid/app/Activity;

    .line 111
    iput-object p3, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->rawResult:Lcom/sec/android/app/camaftest/core/Result;

    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->parseCustomSearchURL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->customProductSearch:Ljava/lang/String;

    .line 113
    return-void
.end method

.method private static calculateMilliseconds(Ljava/lang/String;)J
    .locals 12
    .param p0, "when"    # Ljava/lang/String;

    .prologue
    const/16 v11, 0x10

    const/16 v10, 0xf

    .line 228
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_1

    .line 231
    sget-object v6, Lcom/sec/android/app/camaftest/result/ResultHandler;->DATE_FORMAT:Ljava/text/DateFormat;

    monitor-enter v6

    .line 232
    :try_start_0
    sget-object v5, Lcom/sec/android/app/camaftest/result/ResultHandler;->DATE_FORMAT:Ljava/text/DateFormat;

    new-instance v7, Ljava/text/ParsePosition;

    const/4 v8, 0x0

    invoke-direct {v7, v8}, Ljava/text/ParsePosition;-><init>(I)V

    invoke-virtual {v5, p0, v7}, Ljava/text/DateFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;

    move-result-object v1

    .line 233
    .local v1, "date":Ljava/util/Date;
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 248
    :cond_0
    :goto_0
    return-wide v2

    .line 233
    .end local v1    # "date":Ljava/util/Date;
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 239
    :cond_1
    sget-object v6, Lcom/sec/android/app/camaftest/result/ResultHandler;->DATE_TIME_FORMAT:Ljava/text/DateFormat;

    monitor-enter v6

    .line 240
    :try_start_2
    sget-object v5, Lcom/sec/android/app/camaftest/result/ResultHandler;->DATE_TIME_FORMAT:Ljava/text/DateFormat;

    const/4 v7, 0x0

    const/16 v8, 0xf

    invoke-virtual {p0, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/text/ParsePosition;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Ljava/text/ParsePosition;-><init>(I)V

    invoke-virtual {v5, v7, v8}, Ljava/text/DateFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;

    move-result-object v1

    .line 241
    .restart local v1    # "date":Ljava/util/Date;
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 242
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 243
    .local v2, "milliseconds":J
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v5, v11, :cond_0

    invoke-virtual {p0, v10}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x5a

    if-ne v5, v6, :cond_0

    .line 244
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 245
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, v10}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->get(I)I

    move-result v6

    add-int v4, v5, v6

    .line 246
    .local v4, "offset":I
    int-to-long v6, v4

    add-long/2addr v2, v6

    goto :goto_0

    .line 241
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v1    # "date":Ljava/util/Date;
    .end local v2    # "milliseconds":J
    .end local v4    # "offset":I
    :catchall_1
    move-exception v5

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v5
.end method

.method private static doToContractType(Ljava/lang/String;[Ljava/lang/String;[I)I
    .locals 4
    .param p0, "typeString"    # Ljava/lang/String;
    .param p1, "types"    # [Ljava/lang/String;
    .param p2, "values"    # [I

    .prologue
    const/4 v2, -0x1

    .line 344
    if-nez p0, :cond_1

    .line 353
    :cond_0
    :goto_0
    return v2

    .line 347
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 348
    aget-object v1, p1, v0

    .line 349
    .local v1, "type":Ljava/lang/String;
    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 350
    :cond_2
    aget v2, p2, v0

    goto :goto_0

    .line 347
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private parseCustomSearchURL()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 470
    iget-object v3, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->activity:Landroid/app/Activity;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 471
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v3, "preferences_custom_product_search"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 473
    .local v0, "customProductSearch":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    move-object v0, v2

    .line 476
    .end local v0    # "customProductSearch":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private static putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 464
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 465
    invoke-virtual {p0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 467
    :cond_0
    return-void
.end method

.method private static toAddressContractType(Ljava/lang/String;)I
    .locals 2
    .param p0, "typeString"    # Ljava/lang/String;

    .prologue
    .line 340
    sget-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->ADDRESS_TYPE_STRINGS:[Ljava/lang/String;

    sget-object v1, Lcom/sec/android/app/camaftest/result/ResultHandler;->ADDRESS_TYPE_VALUES:[I

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/camaftest/result/ResultHandler;->doToContractType(Ljava/lang/String;[Ljava/lang/String;[I)I

    move-result v0

    return v0
.end method

.method private static toEmailContractType(Ljava/lang/String;)I
    .locals 2
    .param p0, "typeString"    # Ljava/lang/String;

    .prologue
    .line 332
    sget-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->EMAIL_TYPE_STRINGS:[Ljava/lang/String;

    sget-object v1, Lcom/sec/android/app/camaftest/result/ResultHandler;->EMAIL_TYPE_VALUES:[I

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/camaftest/result/ResultHandler;->doToContractType(Ljava/lang/String;[Ljava/lang/String;[I)I

    move-result v0

    return v0
.end method

.method private static toPhoneContractType(Ljava/lang/String;)I
    .locals 2
    .param p0, "typeString"    # Ljava/lang/String;

    .prologue
    .line 336
    sget-object v0, Lcom/sec/android/app/camaftest/result/ResultHandler;->PHONE_TYPE_STRINGS:[Ljava/lang/String;

    sget-object v1, Lcom/sec/android/app/camaftest/result/ResultHandler;->PHONE_TYPE_VALUES:[I

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/camaftest/result/ResultHandler;->doToContractType(Ljava/lang/String;[Ljava/lang/String;[I)I

    move-result v0

    return v0
.end method


# virtual methods
.method final addCalendarEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "summary"    # Ljava/lang/String;
    .param p2, "start"    # Ljava/lang/String;
    .param p3, "end"    # Ljava/lang/String;
    .param p4, "location"    # Ljava/lang/String;
    .param p5, "description"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 201
    new-instance v1, Landroid/content/Intent;

    const-string v7, "android.intent.action.EDIT"

    invoke-direct {v1, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 202
    .local v1, "intent":Landroid/content/Intent;
    const-string v7, "vnd.android.cursor.item/event"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 203
    invoke-static {p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;->calculateMilliseconds(Ljava/lang/String;)J

    move-result-wide v4

    .line 204
    .local v4, "startMilliseconds":J
    const-string v7, "beginTime"

    invoke-virtual {v1, v7, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 205
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0x8

    if-ne v7, v8, :cond_1

    move v0, v6

    .line 206
    .local v0, "allDay":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 207
    const-string v7, "allDay"

    invoke-virtual {v1, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 210
    :cond_0
    if-nez p3, :cond_3

    .line 211
    if-eqz v0, :cond_2

    .line 213
    const-wide/32 v6, 0x5265c00

    add-long v2, v4, v6

    .line 220
    .local v2, "endMilliseconds":J
    :goto_1
    const-string v6, "endTime"

    invoke-virtual {v1, v6, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 221
    const-string v6, "title"

    invoke-virtual {v1, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 222
    const-string v6, "eventLocation"

    invoke-virtual {v1, v6, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    const-string v6, "description"

    invoke-virtual {v1, v6, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    invoke-virtual {p0, v1}, Lcom/sec/android/app/camaftest/result/ResultHandler;->launchIntent(Landroid/content/Intent;)V

    .line 225
    return-void

    .line 205
    .end local v0    # "allDay":Z
    .end local v2    # "endMilliseconds":J
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 215
    .restart local v0    # "allDay":Z
    :cond_2
    move-wide v2, v4

    .restart local v2    # "endMilliseconds":J
    goto :goto_1

    .line 218
    .end local v2    # "endMilliseconds":J
    :cond_3
    invoke-static {p3}, Lcom/sec/android/app/camaftest/result/ResultHandler;->calculateMilliseconds(Ljava/lang/String;)J

    move-result-wide v2

    .restart local v2    # "endMilliseconds":J
    goto :goto_1
.end method

.method final addContact([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p1, "names"    # [Ljava/lang/String;
    .param p2, "pronunciation"    # Ljava/lang/String;
    .param p3, "phoneNumbers"    # [Ljava/lang/String;
    .param p4, "phoneTypes"    # [Ljava/lang/String;
    .param p5, "emails"    # [Ljava/lang/String;
    .param p6, "emailTypes"    # [Ljava/lang/String;
    .param p7, "note"    # Ljava/lang/String;
    .param p8, "instantMessenger"    # Ljava/lang/String;
    .param p9, "address"    # Ljava/lang/String;
    .param p10, "addressType"    # Ljava/lang/String;
    .param p11, "org"    # Ljava/lang/String;
    .param p12, "title"    # Ljava/lang/String;
    .param p13, "url"    # Ljava/lang/String;
    .param p14, "birthday"    # Ljava/lang/String;

    .prologue
    .line 276
    new-instance v6, Landroid/content/Intent;

    const-string v11, "android.intent.action.INSERT_OR_EDIT"

    sget-object v12, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v6, v11, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 277
    .local v6, "intent":Landroid/content/Intent;
    const-string v11, "vnd.android.cursor.item/contact"

    invoke-virtual {v6, v11}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    const-string v12, "name"

    if-eqz p1, :cond_1

    const/4 v11, 0x0

    aget-object v11, p1, v11

    :goto_0
    invoke-static {v6, v12, v11}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const-string v11, "phonetic_name"

    invoke-static {v6, v11, p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    if-eqz p3, :cond_2

    move-object/from16 v0, p3

    array-length v11, v0

    :goto_1
    sget-object v12, Lcom/sec/android/app/camaftest/Contents;->PHONE_KEYS:[Ljava/lang/String;

    array-length v12, v12

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 283
    .local v8, "phoneCount":I
    const/4 v10, 0x0

    .local v10, "x":I
    :goto_2
    if-ge v10, v8, :cond_3

    .line 284
    sget-object v11, Lcom/sec/android/app/camaftest/Contents;->PHONE_KEYS:[Ljava/lang/String;

    aget-object v11, v11, v10

    aget-object v12, p3, v10

    invoke-static {v6, v11, v12}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    if-eqz p4, :cond_0

    move-object/from16 v0, p4

    array-length v11, v0

    if-ge v10, v11, :cond_0

    .line 286
    aget-object v11, p4, v10

    invoke-static {v11}, Lcom/sec/android/app/camaftest/result/ResultHandler;->toPhoneContractType(Ljava/lang/String;)I

    move-result v9

    .line 287
    .local v9, "type":I
    if-ltz v9, :cond_0

    .line 288
    sget-object v11, Lcom/sec/android/app/camaftest/Contents;->PHONE_TYPE_KEYS:[Ljava/lang/String;

    aget-object v11, v11, v10

    invoke-virtual {v6, v11, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 283
    .end local v9    # "type":I
    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 278
    .end local v8    # "phoneCount":I
    .end local v10    # "x":I
    :cond_1
    const/4 v11, 0x0

    goto :goto_0

    .line 282
    :cond_2
    const/4 v11, 0x0

    goto :goto_1

    .line 293
    .restart local v8    # "phoneCount":I
    .restart local v10    # "x":I
    :cond_3
    if-eqz p5, :cond_5

    move-object/from16 v0, p5

    array-length v11, v0

    :goto_3
    sget-object v12, Lcom/sec/android/app/camaftest/Contents;->EMAIL_KEYS:[Ljava/lang/String;

    array-length v12, v12

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 294
    .local v4, "emailCount":I
    const/4 v10, 0x0

    :goto_4
    if-ge v10, v4, :cond_6

    .line 295
    sget-object v11, Lcom/sec/android/app/camaftest/Contents;->EMAIL_KEYS:[Ljava/lang/String;

    aget-object v11, v11, v10

    aget-object v12, p5, v10

    invoke-static {v6, v11, v12}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    if-eqz p6, :cond_4

    move-object/from16 v0, p6

    array-length v11, v0

    if-ge v10, v11, :cond_4

    .line 297
    aget-object v11, p6, v10

    invoke-static {v11}, Lcom/sec/android/app/camaftest/result/ResultHandler;->toEmailContractType(Ljava/lang/String;)I

    move-result v9

    .line 298
    .restart local v9    # "type":I
    if-ltz v9, :cond_4

    .line 299
    sget-object v11, Lcom/sec/android/app/camaftest/Contents;->EMAIL_TYPE_KEYS:[Ljava/lang/String;

    aget-object v11, v11, v10

    invoke-virtual {v6, v11, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 294
    .end local v9    # "type":I
    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 293
    .end local v4    # "emailCount":I
    :cond_5
    const/4 v11, 0x0

    goto :goto_3

    .line 305
    .restart local v4    # "emailCount":I
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 306
    .local v2, "aggregatedNotes":Ljava/lang/StringBuilder;
    const/4 v11, 0x3

    new-array v3, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object p13, v3, v11

    const/4 v11, 0x1

    aput-object p14, v3, v11

    const/4 v11, 0x2

    aput-object p7, v3, v11

    .local v3, "arr$":[Ljava/lang/String;
    array-length v7, v3

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_5
    if-ge v5, v7, :cond_9

    aget-object v1, v3, v5

    .line 307
    .local v1, "aNote":Ljava/lang/String;
    if-eqz v1, :cond_8

    .line 308
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    if-lez v11, :cond_7

    .line 309
    const/16 v11, 0xa

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 311
    :cond_7
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 314
    .end local v1    # "aNote":Ljava/lang/String;
    :cond_9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    if-lez v11, :cond_a

    .line 315
    const-string v11, "notes"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v6, v11, v12}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :cond_a
    const-string v11, "im_handle"

    move-object/from16 v0, p8

    invoke-static {v6, v11, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const-string v11, "postal"

    move-object/from16 v0, p9

    invoke-static {v6, v11, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    if-eqz p10, :cond_b

    .line 321
    invoke-static/range {p10 .. p10}, Lcom/sec/android/app/camaftest/result/ResultHandler;->toAddressContractType(Ljava/lang/String;)I

    move-result v9

    .line 322
    .restart local v9    # "type":I
    if-ltz v9, :cond_b

    .line 323
    const-string v11, "postal_type"

    invoke-virtual {v6, v11, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 326
    .end local v9    # "type":I
    :cond_b
    const-string v11, "company"

    move-object/from16 v0, p11

    invoke-static {v6, v11, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const-string v11, "job_title"

    move-object/from16 v0, p12

    invoke-static {v6, v11, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    invoke-virtual {p0, v6}, Lcom/sec/android/app/camaftest/result/ResultHandler;->launchIntent(Landroid/content/Intent;)V

    .line 329
    return-void
.end method

.method final addEmailOnlyContact([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 15
    .param p1, "emails"    # [Ljava/lang/String;
    .param p2, "emailTypes"    # [Ljava/lang/String;

    .prologue
    .line 257
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v0 .. v14}, Lcom/sec/android/app/camaftest/result/ResultHandler;->addContact([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    return-void
.end method

.method final addPhoneOnlyContact([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 15
    .param p1, "phoneNumbers"    # [Ljava/lang/String;
    .param p2, "phoneTypes"    # [Ljava/lang/String;

    .prologue
    .line 253
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-virtual/range {v0 .. v14}, Lcom/sec/android/app/camaftest/result/ResultHandler;->addContact([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    return-void
.end method

.method public areContentsSecure()Z
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method

.method final dialPhone(Ljava/lang/String;)V
    .locals 4
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 412
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->launchIntent(Landroid/content/Intent;)V

    .line 413
    return-void
.end method

.method final dialPhoneFromUri(Ljava/lang/String;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 416
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->launchIntent(Landroid/content/Intent;)V

    .line 417
    return-void
.end method

.method fillInCustomSearchURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 480
    iget-object v2, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->customProductSearch:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 491
    .end local p1    # "text":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 483
    .restart local p1    # "text":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->customProductSearch:Ljava/lang/String;

    const-string v3, "%s"

    invoke-virtual {v2, v3, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 484
    .local v1, "url":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->rawResult:Lcom/sec/android/app/camaftest/core/Result;

    if-eqz v2, :cond_1

    .line 485
    const-string v2, "%f"

    iget-object v3, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->rawResult:Lcom/sec/android/app/camaftest/core/Result;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/Result;->getBarcodeFormat()Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 486
    const-string v2, "%t"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 487
    iget-object v2, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->rawResult:Lcom/sec/android/app/camaftest/core/Result;

    invoke-static {v2}, Lcom/sec/android/app/camaftest/core/client/result/ResultParser;->parseResult(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    move-result-object v0

    .line 488
    .local v0, "parsedResultAgain":Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;
    const-string v2, "%t"

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;->getType()Lcom/sec/android/app/camaftest/core/client/result/ParsedResultType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/client/result/ParsedResultType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .end local v0    # "parsedResultAgain":Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;
    :cond_1
    move-object p1, v1

    .line 491
    goto :goto_0
.end method

.method getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method public abstract getButtonCount()I
.end method

.method public abstract getButtonText(I)I
.end method

.method public getDisplayContents()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 166
    iget-object v1, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->result:Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;->getDisplayResult()Ljava/lang/String;

    move-result-object v0

    .line 167
    .local v0, "contents":Ljava/lang/String;
    const-string v1, "\r"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public abstract getDisplayTitle()I
.end method

.method public getResult()Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->result:Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    return-object v0
.end method

.method public final getType()Lcom/sec/android/app/camaftest/core/client/result/ParsedResultType;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->result:Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;->getType()Lcom/sec/android/app/camaftest/core/client/result/ParsedResultType;

    move-result-object v0

    return-object v0
.end method

.method public abstract handleButtonPress(I)V
.end method

.method hasCustomProductSearch()Z
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->customProductSearch:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method launchIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 448
    if-eqz p1, :cond_0

    .line 449
    const/high16 v2, 0x80000

    invoke-virtual {p1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 450
    sget-object v2, Lcom/sec/android/app/camaftest/result/ResultHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Launching intent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with extras: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->activity:Landroid/app/Activity;

    invoke-virtual {v2, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 461
    :cond_0
    :goto_0
    return-void

    .line 453
    :catch_0
    move-exception v1

    .line 454
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->activity:Landroid/app/Activity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 455
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const/high16 v2, 0x7f0a0000

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 456
    const v2, 0x7f0a006a

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 457
    const v2, 0x7f0a003b

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 458
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method final openMap(Ljava/lang/String;)V
    .locals 3
    .param p1, "geoURI"    # Ljava/lang/String;

    .prologue
    .line 420
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->launchIntent(Landroid/content/Intent;)V

    .line 421
    return-void
.end method

.method final openURL(Ljava/lang/String;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 438
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->launchIntent(Landroid/content/Intent;)V

    .line 439
    return-void
.end method

.method final searchMap(Ljava/lang/String;Ljava/lang/CharSequence;)V
    .locals 5
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 430
    move-object v0, p1

    .line 431
    .local v0, "query":Ljava/lang/String;
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 432
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 434
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "geo:0,0?q="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camaftest/result/ResultHandler;->launchIntent(Landroid/content/Intent;)V

    .line 435
    return-void
.end method

.method final sendEmail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "subject"    # Ljava/lang/String;
    .param p3, "body"    # Ljava/lang/String;

    .prologue
    .line 362
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mailto:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/sec/android/app/camaftest/result/ResultHandler;->sendEmailFromUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    return-void
.end method

.method final sendEmailFromUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "email"    # Ljava/lang/String;
    .param p3, "subject"    # Ljava/lang/String;
    .param p4, "body"    # Ljava/lang/String;

    .prologue
    .line 367
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 368
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 369
    const-string v1, "android.intent.extra.EMAIL"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 371
    :cond_0
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-static {v0, v1, p3}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const-string v1, "android.intent.extra.TEXT"

    invoke-static {v0, v1, p4}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 374
    invoke-virtual {p0, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->launchIntent(Landroid/content/Intent;)V

    .line 375
    return-void
.end method

.method final sendMMS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "subject"    # Ljava/lang/String;
    .param p3, "body"    # Ljava/lang/String;

    .prologue
    .line 395
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mmsto:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/sec/android/app/camaftest/result/ResultHandler;->sendMMSFromUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    return-void
.end method

.method final sendMMSFromUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "subject"    # Ljava/lang/String;
    .param p3, "body"    # Ljava/lang/String;

    .prologue
    .line 399
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 401
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 402
    :cond_0
    const-string v1, "subject"

    iget-object v2, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->activity:Landroid/app/Activity;

    const v3, 0x7f0a0063

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    :goto_0
    const-string v1, "sms_body"

    invoke-static {v0, v1, p3}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string v1, "compose_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 408
    invoke-virtual {p0, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->launchIntent(Landroid/content/Intent;)V

    .line 409
    return-void

    .line 404
    :cond_1
    const-string v1, "subject"

    invoke-static {v0, v1, p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method final sendSMS(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;

    .prologue
    .line 383
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "smsto:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;->sendSMSFromUri(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    return-void
.end method

.method final sendSMSFromUri(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;

    .prologue
    .line 387
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 388
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "sms_body"

    invoke-static {v0, v1, p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;->putExtra(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    const-string v1, "compose_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 391
    invoke-virtual {p0, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->launchIntent(Landroid/content/Intent;)V

    .line 392
    return-void
.end method

.method final shareByEmail(Ljava/lang/String;)V
    .locals 4
    .param p1, "contents"    # Ljava/lang/String;

    .prologue
    .line 357
    const-string v0, "mailto:"

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->activity:Landroid/app/Activity;

    const v3, 0x7f0a0074

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/sec/android/app/camaftest/result/ResultHandler;->sendEmailFromUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    return-void
.end method

.method final shareBySMS(Ljava/lang/String;)V
    .locals 4
    .param p1, "contents"    # Ljava/lang/String;

    .prologue
    .line 378
    const-string v0, "smsto:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/camaftest/result/ResultHandler;->activity:Landroid/app/Activity;

    const v3, 0x7f0a0074

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/camaftest/result/ResultHandler;->sendSMSFromUri(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    return-void
.end method

.method final webSearch(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 442
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.WEB_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 443
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 444
    invoke-virtual {p0, v0}, Lcom/sec/android/app/camaftest/result/ResultHandler;->launchIntent(Landroid/content/Intent;)V

    .line 445
    return-void
.end method
