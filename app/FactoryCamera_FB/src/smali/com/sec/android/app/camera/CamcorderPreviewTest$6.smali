.class Lcom/sec/android/app/camera/CamcorderPreviewTest$6;
.super Landroid/content/BroadcastReceiver;
.source "CamcorderPreviewTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/CamcorderPreviewTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V
    .locals 0

    .prologue
    .line 1372
    iput-object p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    .line 1375
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1376
    .local v0, "action":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1377
    const-string v1, "com.android.samsungtest.CameraStop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1378
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "onReceive - get Stop Camera"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1379
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # setter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopCamera:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2002(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z

    .line 1380
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->finish()V

    .line 1411
    :goto_0
    return-void

    .line 1381
    :cond_0
    const-string v1, "com.android.samsungtest.RecordingStart"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1382
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "onReceive - RecordingStart..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1383
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive - testType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2100(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mErrorPopUp : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;
    invoke-static {v3}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2200(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1384
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2100(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2200(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/app/AlertDialog;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1385
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # setter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2302(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z

    .line 1386
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->startRecordingForAutoTest()V

    goto :goto_0

    .line 1388
    :cond_1
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "onReceive - Is not an auto test mode."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1390
    :cond_2
    const-string v1, "com.android.samsungtest.RecordingStop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1391
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "onReceive - RecordingStop..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1392
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2100(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1393
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # setter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2302(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z

    .line 1395
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive - isRecording : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-virtual {v3}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isRecording()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mCamcorderStop : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z
    invoke-static {v3}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2400(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1396
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z
    invoke-static {v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2400(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1397
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # setter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopCamera:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2002(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z

    .line 1398
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doStopVideoRecordingSync()V

    .line 1399
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->finish()V

    goto/16 :goto_0

    .line 1401
    :cond_3
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "onReceive - Recording stop false!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1406
    :cond_4
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "onReceive - Is not an auto test mode."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1409
    :cond_5
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive - this action["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] is not defined"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
