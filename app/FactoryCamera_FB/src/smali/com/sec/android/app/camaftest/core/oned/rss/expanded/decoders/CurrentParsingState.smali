.class final Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;
.super Ljava/lang/Object;
.source "CurrentParsingState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;
    }
.end annotation


# instance fields
.field private encoding:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

.field private position:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->position:I

    .line 43
    sget-object v0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;->NUMERIC:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->encoding:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    .line 44
    return-void
.end method


# virtual methods
.method getPosition()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->position:I

    return v0
.end method

.method incrementPosition(I)V
    .locals 1
    .param p1, "delta"    # I

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->position:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->position:I

    .line 56
    return-void
.end method

.method isAlpha()Z
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->encoding:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    sget-object v1, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;->ALPHA:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isIsoIec646()Z
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->encoding:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    sget-object v1, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;->ISO_IEC_646:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isNumeric()Z
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->encoding:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    sget-object v1, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;->NUMERIC:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setAlpha()V
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;->ALPHA:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->encoding:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    .line 76
    return-void
.end method

.method setIsoIec646()V
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;->ISO_IEC_646:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->encoding:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    .line 80
    return-void
.end method

.method setNumeric()V
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;->NUMERIC:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->encoding:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState$State;

    .line 72
    return-void
.end method

.method setPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/CurrentParsingState;->position:I

    .line 52
    return-void
.end method
