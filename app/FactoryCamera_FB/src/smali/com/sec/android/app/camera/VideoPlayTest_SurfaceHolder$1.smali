.class Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$1;
.super Ljava/lang/Object;
.source "VideoPlayTest_SurfaceHolder.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$1;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$1;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$000(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$1;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mProgress:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$100(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$1;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlay:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$200(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f020013

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$1;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPass:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$300(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$1;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$1;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mFail:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$400(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$1;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$1;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->bPlayComplet:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$502(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;Z)Z

    .line 183
    return-void
.end method
