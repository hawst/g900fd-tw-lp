.class public final Lcom/sec/android/app/camera/Camera$PreviewCallback;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "PreviewCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/Camera;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camera/Camera;)V
    .locals 0

    .prologue
    .line 984
    iput-object p1, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 7
    .param p1, "data"    # [B
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 986
    const-string v3, "FactoryCamera"

    const-string v4, "onPreviewFrame - get the preview image"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$500(Lcom/sec/android/app/camera/Camera;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v4, v4, Lcom/sec/android/app/camera/Camera;->mDatalineCheck:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 988
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->bDoneDTP:Z
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$3600(Lcom/sec/android/app/camera/Camera;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->bCheckDTP:Z
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$3700(Lcom/sec/android/app/camera/Camera;)Z

    move-result v3

    if-ne v3, v5, :cond_0

    .line 989
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->checkDataline([B)Z
    invoke-static {v3, p1}, Lcom/sec/android/app/camera/Camera;->access$3800(Lcom/sec/android/app/camera/Camera;[B)Z

    move-result v3

    if-nez v3, :cond_3

    .line 990
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 991
    .local v0, "dateTaken":J
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->createName(J)Ljava/lang/String;
    invoke-static {v4, v0, v1}, Lcom/sec/android/app/camera/Camera;->access$2900(Lcom/sec/android/app/camera/Camera;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".dtp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 992
    .local v2, "name":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->DIRECTORY_CAMERA_DATA:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->access$3400()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/android/app/camera/Camera;->writeImage(Ljava/lang/String;Ljava/lang/String;[B)V
    invoke-static {v3, v4, v2, p1}, Lcom/sec/android/app/camera/Camera;->access$3100(Lcom/sec/android/app/camera/Camera;Ljava/lang/String;Ljava/lang/String;[B)V

    .line 993
    const-string v3, "FactoryCamera"

    const-string v4, "checkDataline - false"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 994
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cameraType:I
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$1200(Lcom/sec/android/app/camera/Camera;)I

    move-result v3

    if-ne v3, v5, :cond_2

    .line 995
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    const v4, 0x7f0a0009

    # invokes: Lcom/sec/android/app/camera/Camera;->dialogErrorPopup(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/camera/Camera;->access$900(Lcom/sec/android/app/camera/Camera;I)V

    .line 1003
    .end local v0    # "dateTaken":J
    .end local v2    # "name":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # setter for: Lcom/sec/android/app/camera/Camera;->bCheckDTP:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/camera/Camera;->access$3702(Lcom/sec/android/app/camera/Camera;Z)Z

    .line 1006
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->bDoneDTP:Z
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$3600(Lcom/sec/android/app/camera/Camera;)Z

    move-result v3

    if-ne v3, v5, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->bSentAck:Z
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$3900(Lcom/sec/android/app/camera/Camera;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1007
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    const-string v4, "com.android.samsungtest.CAMERA_GOOD"

    # invokes: Lcom/sec/android/app/camera/Camera;->sendBroadCastAck(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/android/app/camera/Camera;->access$4000(Lcom/sec/android/app/camera/Camera;Ljava/lang/String;)V

    .line 1008
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # setter for: Lcom/sec/android/app/camera/Camera;->mShutterBtnlock:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/camera/Camera;->access$4102(Lcom/sec/android/app/camera/Camera;Z)Z

    .line 1011
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1012
    return-void

    .line 997
    .restart local v0    # "dateTaken":J
    .restart local v2    # "name":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    const v4, 0x7f0a0008

    # invokes: Lcom/sec/android/app/camera/Camera;->dialogErrorPopup(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/camera/Camera;->access$900(Lcom/sec/android/app/camera/Camera;I)V

    goto :goto_0

    .line 999
    .end local v0    # "dateTaken":J
    .end local v2    # "name":Ljava/lang/String;
    :cond_3
    const-string v3, "FactoryCamera"

    const-string v4, "checkDataline - success"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1000
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$PreviewCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mMainHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/camera/Camera;->access$500(Lcom/sec/android/app/camera/Camera;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
