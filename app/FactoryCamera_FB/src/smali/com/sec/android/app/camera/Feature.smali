.class public Lcom/sec/android/app/camera/Feature;
.super Ljava/lang/Object;
.source "Feature.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "testFeature"


# instance fields
.field public BACK_CAMERA_OUTPUT:I

.field public CAMCORDER_PREVIEW_FPS_MAX:I

.field public CAMCORDER_PREVIEW_FPS_MIN:I

.field public CAMCORDER_SUPPORT_WIDE_RESOLUTION:Z

.field public CAMERA_BOOST_ENABLE:Z

.field public CAMERA_FEATURE_END:Z

.field public CAMERA_FIRMWARE_UPDATE_BOOTING:Z

.field public CAMERA_FIXED_MEDIARECORDER_PROFILE:Z

.field public CAMERA_FIXED_PICTURE_SIZE:Z

.field public CAMERA_NO_TRANSITION_ANIMATION:Z

.field public CAMERA_USE_DI_CAMERAFIRMWARE:Z

.field public CHECK_CAMCORDER_PREVIEW_TEST:Z

.field public CHECK_CAMCORDER_RECORDING_TEST:Z

.field public CHECK_CAMERA_LATEST_MODULE:Z

.field public CHECK_ISP_CORE_VOLTAGE:Z

.field public CHECK_LOW_LIGHT_CAPTURE_TEST:Z

.field public CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

.field public CHECK_OIS_TEST:Z

.field public CHECK_POSTVIEW_TEST:Z

.field public CHECK_USER_FIRMWARE_FILE_DISABLE:Z

.field public CROP_CAMCORDER_PREVIEW_ENABLE:Z

.field public DATA_OUTPUT_MIPI:I

.field public DATA_OUTPUT_PARALLEL:I

.field public DEFAULT_FIRMWARE_UPDATE:Z

.field public DELAYED_START_PREVIEW:Z

.field public DISPLAY_EXTERNAL_ISP:Z

.field public DURATION_COMPANION_FIRMWARE_UPDATE:I

.field public DURATION_DUMP:I

.field public DURATION_FIRMWARE_UPDATE:I

.field public FACTORY_CAMERA_FIX_RESOLUTION_640X480:Z

.field public FIXED_AUDIO_CHANNELS:I

.field public FIXED_AUDIO_ENCODING_BITRATE:I

.field public FIXED_AUDIO_SAMPLINGRATE:I

.field public FIXED_VIDEO_ENCODING_BITRATE:I

.field public FIX_CAMERA_DISPLAY_ORIENTATION:Z

.field public FLASH_EXIST:Z

.field public FRONT_CAMERA_OUTPUT:I

.field public FRONT_INTERNAL_ISP:Z

.field public FRONT_LOADED_FIRMWARE:Z

.field public INTERNAL_ISP:Z

.field public JF_FIRMWARE_FORCE_UPDATE:Z

.field public LAUNCHING_POSTVIEW_DELAY_TIME:I

.field public LCD_ORIENTATION_LANDSCAPE:Z

.field public NOT_SUPPORT_AUTO_ANTIBANDING:Z

.field public NO_CANCEL_FOCUS:Z

.field public NO_FIRMWARE_TYPE_FILE:Z

.field public NO_FIRMWARE_UPDATE_IN_USER:Z

.field public NO_VENDOR_ID:Z

.field public OIS_TEST_RESOLUTION:[[I

.field public PICTURE_RESOLUTION_F:Ljava/lang/String;

.field public PICTURE_RESOLUTION_R:Ljava/lang/String;

.field public PREVIEW_HEIGHT:I

.field public PREVIEW_WIDTH:I

.field public REAR_INTERNAL_ISP:Z

.field public REAR_QCLOADED_ISP:Z

.field public SELF_TEST_FRONT_CAMERA_MAX_FPS:I

.field public SELF_TEST_FRONT_CAMERA_MIN_FPS:I

.field public SELF_TEST_FRONT_CAMERA_MODE:I

.field public SELF_TEST_FRONT_CAMERA_PICTURE_H:I

.field public SELF_TEST_FRONT_CAMERA_PICTURE_W:I

.field public SELF_TEST_FRONT_CAMERA_PREIVEW_H:I

.field public SELF_TEST_FRONT_CAMERA_PREIVEW_W:I

.field public SET_CAMCORDER_VIDEO_SIZE:Z

.field public SET_MAX_SUPPORTED_PICTURE_SIZE:Z

.field public SET_PARAM_PICTURE_ROTATION:Z

.field public SET_SURFACE_VIEW_FINDER:Z

.field public SHUTTER_DELAY_TIME:I

.field public SKIP_POSTVIEW_FRONT:Z

.field public SKIP_POSTVIEW_REAR:Z

.field public SUPPORT_AF_R:Z

.field public SUPPORT_CAMERA_HARD_KEY:Z

.field public SUPPORT_FOCUS_INFINITY:Z

.field public SUPPORT_PHASE_AF:Z

.field public SUPPORT_ZOOM_R:Z

.field public TEST_RESOLUTION:[[I

.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/16 v6, 0x1e0

    const/16 v5, 0x4e20

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->FLASH_EXIST:Z

    .line 44
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_AF_R:Z

    .line 45
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_FOCUS_INFINITY:Z

    .line 46
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_ZOOM_R:Z

    .line 47
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->FIX_CAMERA_DISPLAY_ORIENTATION:Z

    .line 52
    const-string v0, "640x480"

    iput-object v0, p0, Lcom/sec/android/app/camera/Feature;->PICTURE_RESOLUTION_F:Ljava/lang/String;

    .line 53
    const-string v0, "640x480"

    iput-object v0, p0, Lcom/sec/android/app/camera/Feature;->PICTURE_RESOLUTION_R:Ljava/lang/String;

    .line 55
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SET_MAX_SUPPORTED_PICTURE_SIZE:Z

    .line 56
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SET_SURFACE_VIEW_FINDER:Z

    .line 58
    const/16 v0, 0x280

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->PREVIEW_WIDTH:I

    .line 59
    iput v6, p0, Lcom/sec/android/app/camera/Feature;->PREVIEW_HEIGHT:I

    .line 62
    iput v2, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_PARALLEL:I

    .line 63
    iput v3, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    .line 64
    iget v0, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FRONT_CAMERA_OUTPUT:I

    .line 65
    iget v0, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->BACK_CAMERA_OUTPUT:I

    .line 67
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->DISPLAY_EXTERNAL_ISP:Z

    .line 68
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->REAR_INTERNAL_ISP:Z

    .line 69
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->REAR_QCLOADED_ISP:Z

    .line 70
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->FRONT_INTERNAL_ISP:Z

    .line 71
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->FRONT_LOADED_FIRMWARE:Z

    .line 72
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->INTERNAL_ISP:Z

    .line 73
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CHECK_ISP_CORE_VOLTAGE:Z

    .line 74
    iput v5, p0, Lcom/sec/android/app/camera/Feature;->DURATION_FIRMWARE_UPDATE:I

    .line 75
    iput v5, p0, Lcom/sec/android/app/camera/Feature;->DURATION_DUMP:I

    .line 76
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_UPDATE_IN_USER:Z

    .line 79
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_CAMERA_HARD_KEY:Z

    .line 82
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_PREVIEW_TEST:Z

    .line 85
    new-array v0, v3, [[I

    new-array v1, v4, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    .line 88
    new-array v0, v3, [[I

    new-array v1, v4, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/camera/Feature;->OIS_TEST_RESOLUTION:[[I

    .line 92
    iput v4, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MODE:I

    .line 93
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MAX_FPS:I

    .line 94
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MIN_FPS:I

    .line 95
    const/16 v0, 0x160

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_W:I

    .line 96
    const/16 v0, 0x120

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_H:I

    .line 97
    const/16 v0, 0x280

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_W:I

    .line 98
    iput v6, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_H:I

    .line 101
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    .line 104
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_POSTVIEW_TEST:Z

    .line 105
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_REAR:Z

    .line 106
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_FRONT:Z

    .line 109
    const/16 v0, 0x1b58

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_PREVIEW_FPS_MIN:I

    .line 110
    const/16 v0, 0x7530

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_PREVIEW_FPS_MAX:I

    .line 113
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->LCD_ORIENTATION_LANDSCAPE:Z

    .line 116
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SET_PARAM_PICTURE_ROTATION:Z

    .line 119
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->DELAYED_START_PREVIEW:Z

    .line 122
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_USER_FIRMWARE_FILE_DISABLE:Z

    .line 125
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIRMWARE_UPDATE_BOOTING:Z

    .line 128
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIXED_MEDIARECORDER_PROFILE:Z

    .line 129
    const v0, 0x1036640

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FIXED_VIDEO_ENCODING_BITRATE:I

    .line 130
    const v0, 0x1f400

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_ENCODING_BITRATE:I

    .line 131
    iput v4, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_CHANNELS:I

    .line 132
    const v0, 0xbb80

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_SAMPLINGRATE:I

    .line 135
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->JF_FIRMWARE_FORCE_UPDATE:Z

    .line 138
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_SUPPORT_WIDE_RESOLUTION:Z

    .line 141
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIXED_PICTURE_SIZE:Z

    .line 143
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_TYPE_FILE:Z

    .line 146
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    .line 149
    iput v5, p0, Lcom/sec/android/app/camera/Feature;->DURATION_COMPANION_FIRMWARE_UPDATE:I

    .line 152
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_NO_TRANSITION_ANIMATION:Z

    .line 155
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_USE_DI_CAMERAFIRMWARE:Z

    .line 158
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    .line 159
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    .line 162
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_BOOST_ENABLE:Z

    .line 165
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_PHASE_AF:Z

    .line 168
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SHUTTER_DELAY_TIME:I

    .line 171
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NOT_SUPPORT_AUTO_ANTIBANDING:Z

    .line 174
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->FACTORY_CAMERA_FIX_RESOLUTION_640X480:Z

    .line 177
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CROP_CAMCORDER_PREVIEW_ENABLE:Z

    .line 180
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SET_CAMCORDER_VIDEO_SIZE:Z

    .line 183
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMERA_LATEST_MODULE:Z

    .line 186
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_OIS_TEST:Z

    .line 189
    iput v2, p0, Lcom/sec/android/app/camera/Feature;->LAUNCHING_POSTVIEW_DELAY_TIME:I

    .line 192
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NO_CANCEL_FOCUS:Z

    .line 195
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NO_VENDOR_ID:Z

    .line 204
    const-string v0, "testFeature"

    const-string v1, "Feature.Feature()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    return-void

    .line 85
    nop

    :array_0
    .array-data 4
        0x500
        0x2d0
    .end array-data

    .line 88
    :array_1
    .array-data 4
        0x780
        0x500
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v7, 0x1e0

    const/16 v6, 0x4e20

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Feature;->FLASH_EXIST:Z

    .line 44
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_AF_R:Z

    .line 45
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_FOCUS_INFINITY:Z

    .line 46
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_ZOOM_R:Z

    .line 47
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->FIX_CAMERA_DISPLAY_ORIENTATION:Z

    .line 52
    const-string v1, "640x480"

    iput-object v1, p0, Lcom/sec/android/app/camera/Feature;->PICTURE_RESOLUTION_F:Ljava/lang/String;

    .line 53
    const-string v1, "640x480"

    iput-object v1, p0, Lcom/sec/android/app/camera/Feature;->PICTURE_RESOLUTION_R:Ljava/lang/String;

    .line 55
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Feature;->SET_MAX_SUPPORTED_PICTURE_SIZE:Z

    .line 56
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Feature;->SET_SURFACE_VIEW_FINDER:Z

    .line 58
    const/16 v1, 0x280

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->PREVIEW_WIDTH:I

    .line 59
    iput v7, p0, Lcom/sec/android/app/camera/Feature;->PREVIEW_HEIGHT:I

    .line 62
    iput v3, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_PARALLEL:I

    .line 63
    iput v4, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    .line 64
    iget v1, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->FRONT_CAMERA_OUTPUT:I

    .line 65
    iget v1, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->BACK_CAMERA_OUTPUT:I

    .line 67
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->DISPLAY_EXTERNAL_ISP:Z

    .line 68
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->REAR_INTERNAL_ISP:Z

    .line 69
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Feature;->REAR_QCLOADED_ISP:Z

    .line 70
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Feature;->FRONT_INTERNAL_ISP:Z

    .line 71
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Feature;->FRONT_LOADED_FIRMWARE:Z

    .line 72
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Feature;->INTERNAL_ISP:Z

    .line 73
    iput-boolean v4, p0, Lcom/sec/android/app/camera/Feature;->CHECK_ISP_CORE_VOLTAGE:Z

    .line 74
    iput v6, p0, Lcom/sec/android/app/camera/Feature;->DURATION_FIRMWARE_UPDATE:I

    .line 75
    iput v6, p0, Lcom/sec/android/app/camera/Feature;->DURATION_DUMP:I

    .line 76
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_UPDATE_IN_USER:Z

    .line 79
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_CAMERA_HARD_KEY:Z

    .line 82
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_PREVIEW_TEST:Z

    .line 85
    new-array v1, v4, [[I

    new-array v2, v5, [I

    fill-array-data v2, :array_0

    aput-object v2, v1, v3

    iput-object v1, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    .line 88
    new-array v1, v4, [[I

    new-array v2, v5, [I

    fill-array-data v2, :array_1

    aput-object v2, v1, v3

    iput-object v1, p0, Lcom/sec/android/app/camera/Feature;->OIS_TEST_RESOLUTION:[[I

    .line 92
    iput v5, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MODE:I

    .line 93
    const/16 v1, 0x3a98

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MAX_FPS:I

    .line 94
    const/16 v1, 0x3a98

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MIN_FPS:I

    .line 95
    const/16 v1, 0x160

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_W:I

    .line 96
    const/16 v1, 0x120

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_H:I

    .line 97
    const/16 v1, 0x280

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_W:I

    .line 98
    iput v7, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_H:I

    .line 101
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    .line 104
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CHECK_POSTVIEW_TEST:Z

    .line 105
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_REAR:Z

    .line 106
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_FRONT:Z

    .line 109
    const/16 v1, 0x1b58

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_PREVIEW_FPS_MIN:I

    .line 110
    const/16 v1, 0x7530

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_PREVIEW_FPS_MAX:I

    .line 113
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->LCD_ORIENTATION_LANDSCAPE:Z

    .line 116
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SET_PARAM_PICTURE_ROTATION:Z

    .line 119
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->DELAYED_START_PREVIEW:Z

    .line 122
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CHECK_USER_FIRMWARE_FILE_DISABLE:Z

    .line 125
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIRMWARE_UPDATE_BOOTING:Z

    .line 128
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIXED_MEDIARECORDER_PROFILE:Z

    .line 129
    const v1, 0x1036640

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->FIXED_VIDEO_ENCODING_BITRATE:I

    .line 130
    const v1, 0x1f400

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_ENCODING_BITRATE:I

    .line 131
    iput v5, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_CHANNELS:I

    .line 132
    const v1, 0xbb80

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_SAMPLINGRATE:I

    .line 135
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->JF_FIRMWARE_FORCE_UPDATE:Z

    .line 138
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_SUPPORT_WIDE_RESOLUTION:Z

    .line 141
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIXED_PICTURE_SIZE:Z

    .line 143
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_TYPE_FILE:Z

    .line 146
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    .line 149
    iput v6, p0, Lcom/sec/android/app/camera/Feature;->DURATION_COMPANION_FIRMWARE_UPDATE:I

    .line 152
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_NO_TRANSITION_ANIMATION:Z

    .line 155
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_USE_DI_CAMERAFIRMWARE:Z

    .line 158
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    .line 159
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    .line 162
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_BOOST_ENABLE:Z

    .line 165
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_PHASE_AF:Z

    .line 168
    const/16 v1, 0xc8

    iput v1, p0, Lcom/sec/android/app/camera/Feature;->SHUTTER_DELAY_TIME:I

    .line 171
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->NOT_SUPPORT_AUTO_ANTIBANDING:Z

    .line 174
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->FACTORY_CAMERA_FIX_RESOLUTION_640X480:Z

    .line 177
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CROP_CAMCORDER_PREVIEW_ENABLE:Z

    .line 180
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SET_CAMCORDER_VIDEO_SIZE:Z

    .line 183
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMERA_LATEST_MODULE:Z

    .line 186
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CHECK_OIS_TEST:Z

    .line 189
    iput v3, p0, Lcom/sec/android/app/camera/Feature;->LAUNCHING_POSTVIEW_DELAY_TIME:I

    .line 192
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->NO_CANCEL_FOCUS:Z

    .line 195
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->NO_VENDOR_ID:Z

    .line 208
    iput-object p1, p0, Lcom/sec/android/app/camera/Feature;->mContext:Landroid/content/Context;

    .line 209
    const-string v1, "testFeature"

    const-string v2, "Feature.Feature(context)"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Feature;->readInternalDefaultXml()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :goto_0
    return-void

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 85
    nop

    :array_0
    .array-data 4
        0x500
        0x2d0
    .end array-data

    .line 88
    :array_1
    .array-data 4
        0x780
        0x500
    .end array-data
.end method


# virtual methods
.method protected ChkUserXMLFeatureFile(Ljava/lang/String;)Z
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 318
    const-string v2, "testFeature"

    const-string v3, "ChkUserXMLFeatureFile"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    const/4 v0, 0x0

    .line 321
    .local v0, "UserXMLFeatureFile":Ljava/lang/String;
    move-object v0, p1

    .line 323
    const-string v2, "testFeature"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ChkUserXMLFeatureFile : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 325
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 326
    const-string v2, "testFeature"

    const-string v3, "User XML feature file exists"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    const/4 v2, 0x1

    .line 330
    :goto_0
    return v2

    .line 329
    :cond_0
    const-string v2, "testFeature"

    const-string v3, "User XML feature file doesn\'t exist"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public FeatureValueCheck()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 716
    const-string v0, "testFeature"

    const-string v1, "Feature.FeatureValueCheck()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() FLASH_EXIST : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->FLASH_EXIST:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SUPPORT_AF_R : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_AF_R:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SUPPORT_FOCUS_INFINITY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_FOCUS_INFINITY:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SUPPORT_ZOOM_R : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_ZOOM_R:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() FIX_CAMERA_DISPLAY_ORIENTATION : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->FIX_CAMERA_DISPLAY_ORIENTATION:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() PICTURE_RESOLUTION_F : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Feature;->PICTURE_RESOLUTION_F:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() PICTURE_RESOLUTION_R : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Feature;->PICTURE_RESOLUTION_R:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SET_MAX_SUPPORTED_PICTURE_SIZE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SET_MAX_SUPPORTED_PICTURE_SIZE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SET_SURFACE_VIEW_FINDER : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SET_SURFACE_VIEW_FINDER:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 728
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() PREVIEW_WIDTH : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->PREVIEW_WIDTH:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() PREVIEW_HEIGHT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->PREVIEW_HEIGHT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() DATA_OUTPUT_PARALLEL : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_PARALLEL:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() DATA_OUTPUT_MIPI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() FRONT_CAMERA_OUTPUT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->FRONT_CAMERA_OUTPUT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() BACK_CAMERA_OUTPUT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->BACK_CAMERA_OUTPUT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() DISPLAY_EXTERNAL_ISP : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->DISPLAY_EXTERNAL_ISP:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() REAR_INTERNAL_ISP : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->REAR_INTERNAL_ISP:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() REAR_QCLOADED_ISP : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->REAR_QCLOADED_ISP:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() FRONT_INTERNAL_ISP : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->FRONT_INTERNAL_ISP:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() INTERNAL_ISP : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->INTERNAL_ISP:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CHECK_ISP_CORE_VOLTAGE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_ISP_CORE_VOLTAGE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() DURATION_FIRMWARE_UPDATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->DURATION_FIRMWARE_UPDATE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() DURATION_DUMP : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->DURATION_DUMP:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() NO_FIRMWARE_UPDATE_IN_USER : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_UPDATE_IN_USER:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SUPPORT_CAMERA_HARD_KEY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_CAMERA_HARD_KEY:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CHECK_CAMCORDER_PREVIEW_TEST : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_PREVIEW_TEST:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    iget-object v0, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    array-length v0, v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 751
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() TEST_RESOLUTION : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    aget-object v2, v2, v3

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    aget-object v2, v2, v3

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Length : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 757
    :goto_0
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SELF_TEST_FRONT_CAMERA_MODE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MODE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 758
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SELF_TEST_FRONT_CAMERA_MAX_FPS : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MAX_FPS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SELF_TEST_FRONT_CAMERA_MIN_FPS : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MIN_FPS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SELF_TEST_FRONT_CAMERA_PREIVEW_W : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_W:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SELF_TEST_FRONT_CAMERA_PREIVEW_H : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_H:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SELF_TEST_FRONT_CAMERA_PICTURE_W : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_W:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SELF_TEST_FRONT_CAMERA_PICTURE_H : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_H:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CHECK_CAMCORDER_RECORDING_TEST : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CHECK_POSTVIEW_TEST : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_POSTVIEW_TEST:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SKIP_POSTVIEW_REAR : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_REAR:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SKIP_POSTVIEW_FRONT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_FRONT:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CAMCORDER_PREVIEW_FPS_MIN : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_PREVIEW_FPS_MIN:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CAMCORDER_PREVIEW_FPS_MAX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_PREVIEW_FPS_MAX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() LCD_ORIENTATION_LANDSCAPE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->LCD_ORIENTATION_LANDSCAPE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 775
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SET_PARAM_PICTURE_ROTATION : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SET_PARAM_PICTURE_ROTATION:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() DELAYED_START_PREVIEW : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->DELAYED_START_PREVIEW:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CHECK_USER_FIRMWARE_FILE_DISABLE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_USER_FIRMWARE_FILE_DISABLE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CAMERA_FIRMWARE_UPDATE_BOOTING : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIRMWARE_UPDATE_BOOTING:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CAMERA_FIXED_MEDIARECORDER_PROFILE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIXED_MEDIARECORDER_PROFILE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() FIXED_VIDEO_ENCODING_BITRATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->FIXED_VIDEO_ENCODING_BITRATE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 781
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() FIXED_AUDIO_ENCODING_BITRATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_ENCODING_BITRATE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 782
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() FIXED_AUDIO_CHANNELS : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_CHANNELS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() FIXED_AUDIO_SAMPLINGRATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_SAMPLINGRATE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() JF_FIRMWARE_FORCE_UPDATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->JF_FIRMWARE_FORCE_UPDATE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CAMCORDER_SUPPORT_WIDE_RESOLUTION : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_SUPPORT_WIDE_RESOLUTION:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CAMERA_FIXED_PICTURE_SIZE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIXED_PICTURE_SIZE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() NO_FIRMWARE_TYPE_FILE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_TYPE_FILE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() DEFAULT_FIRMWARE_UPDATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() DURATION_COMPANION_FIRMWARE_UPDATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->DURATION_COMPANION_FIRMWARE_UPDATE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CAMERA_NO_TRANSITION_ANIMATION : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_NO_TRANSITION_ANIMATION:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 793
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CAMERA_USE_DI_CAMERAFIRMWARE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_USE_DI_CAMERAFIRMWARE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CHECK_LOW_LIGHT_CAPTURE_TEST : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CAMERA_BOOST_ENABLE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_BOOST_ENABLE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SUPPORT_PHASE_AF : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_PHASE_AF:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SUTTER_DELAY_TIME : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->SHUTTER_DELAY_TIME:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() NOT_SUPPORT_AUTO_ANTIBANDING : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NOT_SUPPORT_AUTO_ANTIBANDING:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() FACTORY_CAMERA_FIX_RESOLUTION_640X480 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->FACTORY_CAMERA_FIX_RESOLUTION_640X480:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CROP_CAMCORDER_PREVIEW_ENABLE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CROP_CAMCORDER_PREVIEW_ENABLE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() SET_CAMCORDER_VIDEO_SIZE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SET_CAMCORDER_VIDEO_SIZE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CHECK_CAMERA_LATEST_MODULE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMERA_LATEST_MODULE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() CHECK_OIS_TEST : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_OIS_TEST:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() LAUNCHING_POSTVIEW_DELAY_TIME : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/Feature;->LAUNCHING_POSTVIEW_DELAY_TIME:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() NO_VENDOR_ID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NO_VENDOR_ID:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() NO_CANCEL_FOCUS : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NO_CANCEL_FOCUS:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    return-void

    .line 753
    :cond_0
    const-string v0, "testFeature"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.getFeatureValue() TEST_RESOLUTION : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    aget-object v2, v2, v3

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    aget-object v2, v2, v3

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    aget-object v2, v2, v4

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    aget-object v2, v2, v4

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Length : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public ResetFeatureValue()V
    .locals 7

    .prologue
    const/16 v6, 0x1e0

    const/4 v5, 0x2

    const/16 v4, 0x4e20

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 557
    const-string v0, "testFeature"

    const-string v1, "ResetFeatureValue"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->FLASH_EXIST:Z

    .line 563
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_AF_R:Z

    .line 564
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_FOCUS_INFINITY:Z

    .line 565
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_ZOOM_R:Z

    .line 566
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->FIX_CAMERA_DISPLAY_ORIENTATION:Z

    .line 571
    const-string v0, "640x480"

    iput-object v0, p0, Lcom/sec/android/app/camera/Feature;->PICTURE_RESOLUTION_F:Ljava/lang/String;

    .line 572
    const-string v0, "640x480"

    iput-object v0, p0, Lcom/sec/android/app/camera/Feature;->PICTURE_RESOLUTION_R:Ljava/lang/String;

    .line 574
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SET_MAX_SUPPORTED_PICTURE_SIZE:Z

    .line 575
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->SET_SURFACE_VIEW_FINDER:Z

    .line 577
    const/16 v0, 0x280

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->PREVIEW_WIDTH:I

    .line 578
    iput v6, p0, Lcom/sec/android/app/camera/Feature;->PREVIEW_HEIGHT:I

    .line 581
    iput v2, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_PARALLEL:I

    .line 582
    iput v3, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    .line 583
    iget v0, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FRONT_CAMERA_OUTPUT:I

    .line 584
    iget v0, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->BACK_CAMERA_OUTPUT:I

    .line 586
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->DISPLAY_EXTERNAL_ISP:Z

    .line 587
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->REAR_INTERNAL_ISP:Z

    .line 588
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->REAR_QCLOADED_ISP:Z

    .line 589
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->FRONT_INTERNAL_ISP:Z

    .line 590
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->FRONT_LOADED_FIRMWARE:Z

    .line 591
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->INTERNAL_ISP:Z

    .line 592
    iput-boolean v3, p0, Lcom/sec/android/app/camera/Feature;->CHECK_ISP_CORE_VOLTAGE:Z

    .line 593
    iput v4, p0, Lcom/sec/android/app/camera/Feature;->DURATION_FIRMWARE_UPDATE:I

    .line 594
    iput v4, p0, Lcom/sec/android/app/camera/Feature;->DURATION_DUMP:I

    .line 595
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_UPDATE_IN_USER:Z

    .line 598
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_CAMERA_HARD_KEY:Z

    .line 601
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_PREVIEW_TEST:Z

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    aget-object v0, v0, v2

    const/16 v1, 0x500

    aput v1, v0, v2

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    aget-object v0, v0, v2

    const/16 v1, 0x2d0

    aput v1, v0, v3

    .line 608
    iput v5, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MODE:I

    .line 609
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MAX_FPS:I

    .line 610
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MIN_FPS:I

    .line 611
    const/16 v0, 0x160

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_W:I

    .line 612
    const/16 v0, 0x120

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_H:I

    .line 613
    const/16 v0, 0x280

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_W:I

    .line 614
    iput v6, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_H:I

    .line 617
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    .line 620
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_POSTVIEW_TEST:Z

    .line 621
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_REAR:Z

    .line 622
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_FRONT:Z

    .line 625
    const/16 v0, 0x1b58

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_PREVIEW_FPS_MIN:I

    .line 626
    const/16 v0, 0x7530

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_PREVIEW_FPS_MAX:I

    .line 629
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->LCD_ORIENTATION_LANDSCAPE:Z

    .line 632
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SET_PARAM_PICTURE_ROTATION:Z

    .line 635
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->DELAYED_START_PREVIEW:Z

    .line 638
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_USER_FIRMWARE_FILE_DISABLE:Z

    .line 641
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIRMWARE_UPDATE_BOOTING:Z

    .line 644
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIXED_MEDIARECORDER_PROFILE:Z

    .line 645
    const v0, 0x1036640

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FIXED_VIDEO_ENCODING_BITRATE:I

    .line 646
    const v0, 0x1f400

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_ENCODING_BITRATE:I

    .line 647
    iput v5, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_CHANNELS:I

    .line 648
    const v0, 0xbb80

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_SAMPLINGRATE:I

    .line 651
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->JF_FIRMWARE_FORCE_UPDATE:Z

    .line 654
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_SUPPORT_WIDE_RESOLUTION:Z

    .line 657
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIXED_PICTURE_SIZE:Z

    .line 659
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_TYPE_FILE:Z

    .line 662
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    .line 665
    iput v4, p0, Lcom/sec/android/app/camera/Feature;->DURATION_COMPANION_FIRMWARE_UPDATE:I

    .line 668
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_NO_TRANSITION_ANIMATION:Z

    .line 671
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_USE_DI_CAMERAFIRMWARE:Z

    .line 674
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    .line 675
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    .line 678
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_BOOST_ENABLE:Z

    .line 681
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_PHASE_AF:Z

    .line 684
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SHUTTER_DELAY_TIME:I

    .line 687
    iput v2, p0, Lcom/sec/android/app/camera/Feature;->LAUNCHING_POSTVIEW_DELAY_TIME:I

    .line 690
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NO_CANCEL_FOCUS:Z

    .line 693
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NOT_SUPPORT_AUTO_ANTIBANDING:Z

    .line 696
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->FACTORY_CAMERA_FIX_RESOLUTION_640X480:Z

    .line 699
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CROP_CAMCORDER_PREVIEW_ENABLE:Z

    .line 702
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->SET_CAMCORDER_VIDEO_SIZE:Z

    .line 705
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMERA_LATEST_MODULE:Z

    .line 708
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->CHECK_OIS_TEST:Z

    .line 711
    iput-boolean v2, p0, Lcom/sec/android/app/camera/Feature;->NO_VENDOR_ID:Z

    .line 713
    return-void
.end method

.method public StringToArrayInt(Ljava/lang/String;)V
    .locals 6
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 540
    const-string v4, ","

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 541
    .local v3, "st":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "j":I
    const/4 v2, 0x0

    .line 543
    .local v2, "m":I
    array-length v4, v3

    div-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    filled-new-array {v4, v5}, [I

    move-result-object v4

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[I

    iput-object v4, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    .line 544
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 545
    iget-object v4, p0, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    aget-object v4, v4, v1

    aget-object v5, v3, v0

    invoke-virtual {p0, v5}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v4, v2

    .line 547
    rem-int/lit8 v4, v0, 0x2

    if-eqz v4, :cond_0

    .line 548
    add-int/lit8 v1, v1, 0x1

    .line 549
    const/4 v2, 0x0

    .line 544
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 551
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 554
    :cond_1
    return-void
.end method

.method public StringToBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 526
    const-string v0, "true"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    const/4 v0, 0x1

    .line 529
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public StringToInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 534
    const/4 v0, 0x0

    .line 535
    .local v0, "mReturn":I
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 536
    return v0
.end method

.method public getValue(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "parser"    # Lorg/xmlpull/v1/XmlPullParser;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 335
    const-string v0, "local"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FLASH_EXIST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 337
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->FLASH_EXIST:Z

    .line 523
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SUPPORT_AF_R"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 339
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_AF_R:Z

    goto :goto_0

    .line 340
    :cond_2
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SUPPORT_FOCUS_INFINITY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 341
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_FOCUS_INFINITY:Z

    goto :goto_0

    .line 342
    :cond_3
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SUPPORT_ZOOM_R"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 343
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_ZOOM_R:Z

    goto :goto_0

    .line 344
    :cond_4
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FIX_CAMERA_DISPLAY_ORIENTATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 345
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->FIX_CAMERA_DISPLAY_ORIENTATION:Z

    goto :goto_0

    .line 346
    :cond_5
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PICTURE_RESOLUTION_F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 350
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/Feature;->PICTURE_RESOLUTION_F:Ljava/lang/String;

    goto :goto_0

    .line 351
    :cond_6
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PICTURE_RESOLUTION_R"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 352
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/Feature;->PICTURE_RESOLUTION_R:Ljava/lang/String;

    goto/16 :goto_0

    .line 353
    :cond_7
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SET_MAX_SUPPORTED_PICTURE_SIZE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 354
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->SET_MAX_SUPPORTED_PICTURE_SIZE:Z

    goto/16 :goto_0

    .line 355
    :cond_8
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SET_SURFACE_VIEW_FINDER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 356
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->SET_SURFACE_VIEW_FINDER:Z

    goto/16 :goto_0

    .line 357
    :cond_9
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PREVIEW_WIDTH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 358
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->PREVIEW_WIDTH:I

    goto/16 :goto_0

    .line 359
    :cond_a
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PREVIEW_HEIGHT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 360
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->PREVIEW_HEIGHT:I

    goto/16 :goto_0

    .line 361
    :cond_b
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DATA_OUTPUT_PARALLEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 363
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_PARALLEL:I

    goto/16 :goto_0

    .line 364
    :cond_c
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DATA_OUTPUT_MIPI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 365
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->DATA_OUTPUT_MIPI:I

    goto/16 :goto_0

    .line 366
    :cond_d
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FRONT_CAMERA_OUTPUT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 367
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FRONT_CAMERA_OUTPUT:I

    goto/16 :goto_0

    .line 368
    :cond_e
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "BACK_CAMERA_OUTPUT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 369
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->BACK_CAMERA_OUTPUT:I

    goto/16 :goto_0

    .line 370
    :cond_f
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DISPLAY_EXTERNAL_ISP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 371
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->DISPLAY_EXTERNAL_ISP:Z

    goto/16 :goto_0

    .line 372
    :cond_10
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "REAR_INTERNAL_ISP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 373
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->REAR_INTERNAL_ISP:Z

    goto/16 :goto_0

    .line 374
    :cond_11
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "REAR_QCLOADED_ISP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 375
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->REAR_QCLOADED_ISP:Z

    goto/16 :goto_0

    .line 376
    :cond_12
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FRONT_INTERNAL_ISP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 377
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->FRONT_INTERNAL_ISP:Z

    goto/16 :goto_0

    .line 378
    :cond_13
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FRONT_LOADED_FIRMWARE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 379
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->FRONT_LOADED_FIRMWARE:Z

    goto/16 :goto_0

    .line 380
    :cond_14
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "INTERNAL_ISP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 381
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->INTERNAL_ISP:Z

    goto/16 :goto_0

    .line 382
    :cond_15
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHECK_ISP_CORE_VOLTAGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 383
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CHECK_ISP_CORE_VOLTAGE:Z

    goto/16 :goto_0

    .line 384
    :cond_16
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DURATION_FIRMWARE_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 385
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->DURATION_FIRMWARE_UPDATE:I

    goto/16 :goto_0

    .line 386
    :cond_17
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DURATION_DUMP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 387
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->DURATION_DUMP:I

    goto/16 :goto_0

    .line 388
    :cond_18
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "NO_FIRMWARE_UPDATE_IN_USER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 389
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_UPDATE_IN_USER:Z

    goto/16 :goto_0

    .line 390
    :cond_19
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SUPPORT_CAMERA_HARD_KEY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 392
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_CAMERA_HARD_KEY:Z

    goto/16 :goto_0

    .line 393
    :cond_1a
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHECK_CAMCORDER_PREVIEW_TEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 395
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_PREVIEW_TEST:Z

    goto/16 :goto_0

    .line 396
    :cond_1b
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "TEST_RESOLUTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 399
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToArrayInt(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 400
    :cond_1c
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SELF_TEST_FRONT_CAMERA_MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 402
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MODE:I

    goto/16 :goto_0

    .line 403
    :cond_1d
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SELF_TEST_FRONT_CAMERA_MAX_FPS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 404
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MAX_FPS:I

    goto/16 :goto_0

    .line 405
    :cond_1e
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SELF_TEST_FRONT_CAMERA_MIN_FPS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 406
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_MIN_FPS:I

    goto/16 :goto_0

    .line 407
    :cond_1f
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SELF_TEST_FRONT_CAMERA_PREIVEW_W"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 408
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_W:I

    goto/16 :goto_0

    .line 409
    :cond_20
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SELF_TEST_FRONT_CAMERA_PREIVEW_H"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 410
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PREIVEW_H:I

    goto/16 :goto_0

    .line 411
    :cond_21
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SELF_TEST_FRONT_CAMERA_PICTURE_W"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 412
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_W:I

    goto/16 :goto_0

    .line 413
    :cond_22
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SELF_TEST_FRONT_CAMERA_PICTURE_H"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 414
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SELF_TEST_FRONT_CAMERA_PICTURE_H:I

    goto/16 :goto_0

    .line 415
    :cond_23
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHECK_CAMCORDER_RECORDING_TEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 417
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    goto/16 :goto_0

    .line 418
    :cond_24
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHECK_POSTVIEW_TEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 420
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CHECK_POSTVIEW_TEST:Z

    goto/16 :goto_0

    .line 421
    :cond_25
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SKIP_POSTVIEW_REAR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 422
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_REAR:Z

    goto/16 :goto_0

    .line 423
    :cond_26
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SKIP_POSTVIEW_FRONT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 424
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_FRONT:Z

    goto/16 :goto_0

    .line 425
    :cond_27
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CAMCORDER_PREVIEW_FPS_MIN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 427
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_PREVIEW_FPS_MIN:I

    goto/16 :goto_0

    .line 428
    :cond_28
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CAMCORDER_PREVIEW_FPS_MAX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 429
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_PREVIEW_FPS_MAX:I

    goto/16 :goto_0

    .line 430
    :cond_29
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "LCD_ORIENTATION_LANDSCAPE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 432
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->LCD_ORIENTATION_LANDSCAPE:Z

    goto/16 :goto_0

    .line 433
    :cond_2a
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SET_PARAM_PICTURE_ROTATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 435
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->SET_PARAM_PICTURE_ROTATION:Z

    goto/16 :goto_0

    .line 436
    :cond_2b
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DELAYED_START_PREVIEW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 438
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->DELAYED_START_PREVIEW:Z

    goto/16 :goto_0

    .line 439
    :cond_2c
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHECK_USER_FIRMWARE_FILE_DISABLE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 441
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CHECK_USER_FIRMWARE_FILE_DISABLE:Z

    goto/16 :goto_0

    .line 442
    :cond_2d
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CAMERA_FIRMWARE_UPDATE_BOOTING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 444
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIRMWARE_UPDATE_BOOTING:Z

    goto/16 :goto_0

    .line 445
    :cond_2e
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CAMERA_FIXED_MEDIARECORDER_PROFILE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 447
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIXED_MEDIARECORDER_PROFILE:Z

    goto/16 :goto_0

    .line 448
    :cond_2f
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FIXED_VIDEO_ENCODING_BITRATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 449
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FIXED_VIDEO_ENCODING_BITRATE:I

    goto/16 :goto_0

    .line 450
    :cond_30
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FIXED_AUDIO_ENCODING_BITRATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 451
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_ENCODING_BITRATE:I

    goto/16 :goto_0

    .line 452
    :cond_31
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FIXED_AUDIO_CHANNELS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 453
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_CHANNELS:I

    goto/16 :goto_0

    .line 454
    :cond_32
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FIXED_AUDIO_SAMPLINGRATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 455
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_SAMPLINGRATE:I

    goto/16 :goto_0

    .line 456
    :cond_33
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "JF_FIRMWARE_FORCE_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 458
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->JF_FIRMWARE_FORCE_UPDATE:Z

    goto/16 :goto_0

    .line 459
    :cond_34
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CAMCORDER_SUPPORT_WIDE_RESOLUTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 461
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CAMCORDER_SUPPORT_WIDE_RESOLUTION:Z

    goto/16 :goto_0

    .line 462
    :cond_35
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CAMERA_FIXED_PICTURE_SIZE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 464
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_FIXED_PICTURE_SIZE:Z

    goto/16 :goto_0

    .line 465
    :cond_36
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "NO_FIRMWARE_TYPE_FILE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 466
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_TYPE_FILE:Z

    goto/16 :goto_0

    .line 467
    :cond_37
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DEFAULT_FIRMWARE_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 469
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    goto/16 :goto_0

    .line 470
    :cond_38
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DURATION_COMPANION_FIRMWARE_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 472
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->DURATION_COMPANION_FIRMWARE_UPDATE:I

    goto/16 :goto_0

    .line 473
    :cond_39
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CAMERA_NO_TRANSITION_ANIMATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 475
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_NO_TRANSITION_ANIMATION:Z

    goto/16 :goto_0

    .line 476
    :cond_3a
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CAMERA_USE_DI_CAMERAFIRMWARE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 478
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_USE_DI_CAMERAFIRMWARE:Z

    goto/16 :goto_0

    .line 479
    :cond_3b
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHECK_LOW_LIGHT_CAPTURE_TEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 481
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    goto/16 :goto_0

    .line 482
    :cond_3c
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 484
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    goto/16 :goto_0

    .line 485
    :cond_3d
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CAMERA_BOOST_ENABLE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 487
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CAMERA_BOOST_ENABLE:Z

    goto/16 :goto_0

    .line 488
    :cond_3e
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SUPPORT_PHASE_AF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 490
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->SUPPORT_PHASE_AF:Z

    goto/16 :goto_0

    .line 491
    :cond_3f
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SHUTTER_DELAY_TIME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 493
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->SHUTTER_DELAY_TIME:I

    goto/16 :goto_0

    .line 494
    :cond_40
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "LAUNCHING_POSTVIEW_DELAY_TIME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 496
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/Feature;->LAUNCHING_POSTVIEW_DELAY_TIME:I

    goto/16 :goto_0

    .line 497
    :cond_41
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "NO_CANCEL_FOCUS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 499
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->NO_CANCEL_FOCUS:Z

    goto/16 :goto_0

    .line 500
    :cond_42
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "NOT_SUPPORT_AUTO_ANTIBANDING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 502
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->NOT_SUPPORT_AUTO_ANTIBANDING:Z

    goto/16 :goto_0

    .line 503
    :cond_43
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FACTORY_CAMERA_FIX_RESOLUTION_640X480"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 505
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->FACTORY_CAMERA_FIX_RESOLUTION_640X480:Z

    goto/16 :goto_0

    .line 506
    :cond_44
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CROP_CAMCORDER_PREVIEW_ENABLE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 508
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CROP_CAMCORDER_PREVIEW_ENABLE:Z

    goto/16 :goto_0

    .line 509
    :cond_45
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SET_CAMCORDER_VIDEO_SIZE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 511
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->SET_CAMCORDER_VIDEO_SIZE:Z

    goto/16 :goto_0

    .line 512
    :cond_46
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHECK_CAMERA_LATEST_MODULE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 514
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMERA_LATEST_MODULE:Z

    goto/16 :goto_0

    .line 515
    :cond_47
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "NO_VENDOR_ID"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 517
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->NO_VENDOR_ID:Z

    goto/16 :goto_0

    .line 518
    :cond_48
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHECK_OIS_TEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    invoke-interface {p2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/Feature;->StringToBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/Feature;->CHECK_OIS_TEST:Z

    goto/16 :goto_0
.end method

.method public readInternalDefaultXml()V
    .locals 4

    .prologue
    .line 219
    const-string v2, "testFeature"

    const-string v3, "Feature.readInternalDefaultXml()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Feature;->ResetFeatureValue()V

    .line 221
    iget-object v2, p0, Lcom/sec/android/app/camera/Feature;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f050000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 224
    .local v1, "parser":Lorg/xmlpull/v1/XmlPullParser;
    :goto_0
    :try_start_0
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 225
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 226
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/camera/Feature;->getValue(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    .line 229
    :cond_0
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v2, "testFeature"

    const-string v3, "Feature.readInternalDefaultXml XmlPullParserException....."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 238
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_1
    :goto_1
    return-void

    .line 234
    :catch_1
    move-exception v0

    .line 235
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "testFeature"

    const-string v3, "Feature.readInternalDefaultXml Parser IOException....."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public readUserXml()Z
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    const-string v11, "testFeature"

    const-string v12, "Feature.readUserXml()..."

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    .line 244
    .local v6, "mStoragePath":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/CameraTest/userfeature.xml"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 245
    .local v9, "path":Ljava/lang/String;
    const-string v11, "testFeature"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Feature.readUserXml() mStoragePath : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    const-string v11, "testFeature"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Feature.readUserXml() path : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-virtual {p0, v9}, Lcom/sec/android/app/camera/Feature;->ChkUserXMLFeatureFile(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 249
    const-string v11, "testFeature"

    const-string v12, "readUserXml File not found!!!"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const/4 v11, 0x0

    .line 313
    :cond_0
    :goto_0
    return v11

    .line 252
    :cond_1
    const-string v10, ""

    .line 253
    .local v10, "title":Ljava/lang/String;
    const/4 v4, 0x0

    .line 254
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v0, 0x0

    .line 255
    .local v0, "File":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 258
    .local v5, "mIsr":Ljava/io/InputStreamReader;
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 259
    .end local v0    # "File":Ljava/io/FileInputStream;
    .local v1, "File":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v5, Ljava/io/InputStreamReader;

    .end local v5    # "mIsr":Ljava/io/InputStreamReader;
    invoke-direct {v5, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c

    .line 273
    .restart local v5    # "mIsr":Ljava/io/InputStreamReader;
    :try_start_2
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 275
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v8

    .line 276
    .local v8, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v8, v5}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 277
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 278
    .local v3, "event":I
    const/4 v7, 0x0

    .line 280
    .local v7, "name":Ljava/lang/String;
    :goto_1
    const/4 v11, 0x1

    if-eq v3, v11, :cond_3

    .line 281
    const/4 v11, 0x2

    if-ne v3, v11, :cond_2

    .line 282
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    .line 283
    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/camera/Feature;->getValue(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V

    .line 286
    :cond_2
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    goto :goto_1

    .line 260
    .end local v1    # "File":Ljava/io/FileInputStream;
    .end local v3    # "event":I
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    :catch_0
    move-exception v2

    .line 262
    .end local v5    # "mIsr":Ljava/io/InputStreamReader;
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_2
    const-string v11, "testFeature"

    const-string v12, "readUserXml File not found!!!"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 264
    const/4 v11, 0x0

    goto :goto_0

    .line 265
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .restart local v5    # "mIsr":Ljava/io/InputStreamReader;
    :catch_1
    move-exception v2

    .line 267
    .end local v5    # "mIsr":Ljava/io/InputStreamReader;
    .local v2, "e":Ljava/io/IOException;
    :goto_3
    const-string v11, "testFeature"

    const-string v12, "readUserXml IOException..."

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 269
    const/4 v11, 0x0

    goto :goto_0

    .line 299
    .end local v0    # "File":Ljava/io/FileInputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    .restart local v3    # "event":I
    .restart local v5    # "mIsr":Ljava/io/InputStreamReader;
    .restart local v7    # "name":Ljava/lang/String;
    .restart local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :cond_3
    if-eqz v1, :cond_9

    .line 300
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 301
    const/4 v0, 0x0

    .line 304
    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    :goto_4
    if-eqz v5, :cond_4

    .line 305
    :try_start_4
    invoke-virtual {v5}, Ljava/io/InputStreamReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_b

    .line 306
    const/4 v5, 0x0

    .line 313
    :cond_4
    :goto_5
    const/4 v11, 0x1

    goto :goto_0

    .line 308
    .end local v0    # "File":Ljava/io/FileInputStream;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    :catch_2
    move-exception v2

    move-object v0, v1

    .line 310
    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_6
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 288
    .end local v0    # "File":Ljava/io/FileInputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "event":I
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    :catch_3
    move-exception v2

    .line 290
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_5
    const-string v11, "testFeature"

    const-string v12, "readUserXml XmlPullParserException....."

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 292
    const/4 v11, 0x0

    .line 299
    if-eqz v1, :cond_8

    .line 300
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 301
    const/4 v0, 0x0

    .line 304
    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    :goto_7
    if-eqz v5, :cond_0

    .line 305
    :try_start_7
    invoke-virtual {v5}, Ljava/io/InputStreamReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_a

    .line 306
    const/4 v5, 0x0

    goto :goto_0

    .line 308
    .end local v0    # "File":Ljava/io/FileInputStream;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .line 310
    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    .local v2, "e":Ljava/io/IOException;
    :goto_8
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 293
    .end local v0    # "File":Ljava/io/FileInputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    :catch_5
    move-exception v2

    .line 294
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_8
    const-string v11, "testFeature"

    const-string v12, "readUserXml Parser IOException....."

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 296
    const/4 v11, 0x0

    .line 299
    if-eqz v1, :cond_7

    .line 300
    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 301
    const/4 v0, 0x0

    .line 304
    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    :goto_9
    if-eqz v5, :cond_0

    .line 305
    :try_start_a
    invoke-virtual {v5}, Ljava/io/InputStreamReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    .line 306
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 308
    .end local v0    # "File":Ljava/io/FileInputStream;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    :catch_6
    move-exception v2

    move-object v0, v1

    .line 310
    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    :goto_a
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 298
    .end local v0    # "File":Ljava/io/FileInputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v11

    .line 299
    if-eqz v1, :cond_6

    .line 300
    :try_start_b
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 301
    const/4 v0, 0x0

    .line 304
    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    :goto_b
    if-eqz v5, :cond_5

    .line 305
    :try_start_c
    invoke-virtual {v5}, Ljava/io/InputStreamReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 306
    const/4 v5, 0x0

    .line 311
    :cond_5
    :goto_c
    throw v11

    .line 308
    .end local v0    # "File":Ljava/io/FileInputStream;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    :catch_7
    move-exception v2

    move-object v0, v1

    .line 310
    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_d
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 308
    .end local v2    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v2

    goto :goto_d

    .restart local v2    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v2

    goto :goto_a

    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_a
    move-exception v2

    goto :goto_8

    .end local v2    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v3    # "event":I
    .restart local v7    # "name":Ljava/lang/String;
    .restart local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_b
    move-exception v2

    goto :goto_6

    .line 265
    .end local v0    # "File":Ljava/io/FileInputStream;
    .end local v3    # "event":I
    .end local v5    # "mIsr":Ljava/io/InputStreamReader;
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    :catch_c
    move-exception v2

    move-object v0, v1

    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    goto :goto_3

    .line 260
    .end local v0    # "File":Ljava/io/FileInputStream;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    :catch_d
    move-exception v2

    move-object v0, v1

    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .end local v0    # "File":Ljava/io/FileInputStream;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    .restart local v5    # "mIsr":Ljava/io/InputStreamReader;
    :cond_6
    move-object v0, v1

    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    goto :goto_b

    .end local v0    # "File":Ljava/io/FileInputStream;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    .local v2, "e":Ljava/io/IOException;
    :cond_7
    move-object v0, v1

    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    goto :goto_9

    .end local v0    # "File":Ljava/io/FileInputStream;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_8
    move-object v0, v1

    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    goto :goto_7

    .end local v0    # "File":Ljava/io/FileInputStream;
    .end local v2    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v1    # "File":Ljava/io/FileInputStream;
    .restart local v3    # "event":I
    .restart local v7    # "name":Ljava/lang/String;
    .restart local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :cond_9
    move-object v0, v1

    .end local v1    # "File":Ljava/io/FileInputStream;
    .restart local v0    # "File":Ljava/io/FileInputStream;
    goto :goto_4
.end method
