.class public final Lcom/sec/android/app/camaftest/core/oned/Code93Reader;
.super Lcom/sec/android/app/camaftest/core/oned/OneDReader;
.source "Code93Reader.java"


# static fields
.field private static final ALPHABET:[C

.field private static final ALPHABET_STRING:Ljava/lang/String; = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*"

.field private static final ASTERISK_ENCODING:I

.field private static final CHARACTER_ENCODINGS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    const-string v0, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->ALPHABET:[C

    .line 49
    const/16 v0, 0x30

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->CHARACTER_ENCODINGS:[I

    .line 56
    sget-object v0, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->CHARACTER_ENCODINGS:[I

    const/16 v1, 0x2f

    aget v0, v0, v1

    sput v0, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->ASTERISK_ENCODING:I

    return-void

    .line 49
    :array_0
    .array-data 4
        0x114
        0x148
        0x144
        0x142
        0x128
        0x124
        0x122
        0x150
        0x112
        0x10a
        0x1a8
        0x1a4
        0x1a2
        0x194
        0x192
        0x18a
        0x168
        0x164
        0x162
        0x134
        0x11a
        0x158
        0x14c
        0x146
        0x12c
        0x116
        0x1b4
        0x1b2
        0x1ac
        0x1a6
        0x196
        0x19a
        0x16c
        0x166
        0x136
        0x13a
        0x12e
        0x1d4
        0x1d2
        0x1ca
        0x16e
        0x176
        0x1ae
        0x126
        0x1da
        0x1d6
        0x132
        0x15e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/oned/OneDReader;-><init>()V

    return-void
.end method

.method private static checkChecksums(Ljava/lang/CharSequence;)V
    .locals 3
    .param p0, "result"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/ChecksumException;
        }
    .end annotation

    .prologue
    .line 238
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 239
    .local v0, "length":I
    add-int/lit8 v1, v0, -0x2

    const/16 v2, 0x14

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->checkOneChecksum(Ljava/lang/CharSequence;II)V

    .line 240
    add-int/lit8 v1, v0, -0x1

    const/16 v2, 0xf

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->checkOneChecksum(Ljava/lang/CharSequence;II)V

    .line 241
    return-void
.end method

.method private static checkOneChecksum(Ljava/lang/CharSequence;II)V
    .locals 6
    .param p0, "result"    # Ljava/lang/CharSequence;
    .param p1, "checkPosition"    # I
    .param p2, "weightMax"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/ChecksumException;
        }
    .end annotation

    .prologue
    .line 244
    const/4 v2, 0x1

    .line 245
    .local v2, "weight":I
    const/4 v1, 0x0

    .line 246
    .local v1, "total":I
    add-int/lit8 v0, p1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 247
    const-string v3, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*"

    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    mul-int/2addr v3, v2

    add-int/2addr v1, v3

    .line 248
    add-int/lit8 v2, v2, 0x1

    if-le v2, p2, :cond_0

    .line 249
    const/4 v2, 0x1

    .line 246
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 252
    :cond_1
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    sget-object v4, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->ALPHABET:[C

    rem-int/lit8 v5, v1, 0x2f

    aget-char v4, v4, v5

    if-eq v3, v4, :cond_2

    .line 253
    invoke-static {}, Lcom/sec/android/app/camaftest/core/ChecksumException;->getChecksumInstance()Lcom/sec/android/app/camaftest/core/ChecksumException;

    move-result-object v3

    throw v3

    .line 255
    :cond_2
    return-void
.end method

.method private static decodeExtended(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 9
    .param p0, "encoded"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x5a

    const/16 v7, 0x41

    .line 179
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    .line 180
    .local v4, "length":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 181
    .local v1, "decoded":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_8

    .line 182
    invoke-interface {p0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 183
    .local v0, "c":C
    const/16 v6, 0x61

    if-lt v0, v6, :cond_7

    const/16 v6, 0x64

    if-gt v0, v6, :cond_7

    .line 184
    add-int/lit8 v6, v4, -0x1

    if-lt v3, v6, :cond_0

    .line 185
    invoke-static {}, Lcom/sec/android/app/camaftest/core/FormatException;->getFormatInstance()Lcom/sec/android/app/camaftest/core/FormatException;

    move-result-object v6

    throw v6

    .line 187
    :cond_0
    add-int/lit8 v6, v3, 0x1

    invoke-interface {p0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    .line 188
    .local v5, "next":C
    const/4 v2, 0x0

    .line 189
    .local v2, "decodedChar":C
    packed-switch v0, :pswitch_data_0

    .line 227
    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 229
    add-int/lit8 v3, v3, 0x1

    .line 181
    .end local v2    # "decodedChar":C
    .end local v5    # "next":C
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 192
    .restart local v2    # "decodedChar":C
    .restart local v5    # "next":C
    :pswitch_0
    if-lt v5, v7, :cond_1

    if-gt v5, v8, :cond_1

    .line 193
    add-int/lit8 v6, v5, 0x20

    int-to-char v2, v6

    goto :goto_1

    .line 195
    :cond_1
    invoke-static {}, Lcom/sec/android/app/camaftest/core/FormatException;->getFormatInstance()Lcom/sec/android/app/camaftest/core/FormatException;

    move-result-object v6

    throw v6

    .line 200
    :pswitch_1
    if-lt v5, v7, :cond_2

    if-gt v5, v8, :cond_2

    .line 201
    add-int/lit8 v6, v5, -0x40

    int-to-char v2, v6

    goto :goto_1

    .line 203
    :cond_2
    invoke-static {}, Lcom/sec/android/app/camaftest/core/FormatException;->getFormatInstance()Lcom/sec/android/app/camaftest/core/FormatException;

    move-result-object v6

    throw v6

    .line 208
    :pswitch_2
    if-lt v5, v7, :cond_3

    const/16 v6, 0x45

    if-gt v5, v6, :cond_3

    .line 209
    add-int/lit8 v6, v5, -0x26

    int-to-char v2, v6

    goto :goto_1

    .line 210
    :cond_3
    const/16 v6, 0x46

    if-lt v5, v6, :cond_4

    const/16 v6, 0x57

    if-gt v5, v6, :cond_4

    .line 211
    add-int/lit8 v6, v5, -0xb

    int-to-char v2, v6

    goto :goto_1

    .line 213
    :cond_4
    invoke-static {}, Lcom/sec/android/app/camaftest/core/FormatException;->getFormatInstance()Lcom/sec/android/app/camaftest/core/FormatException;

    move-result-object v6

    throw v6

    .line 218
    :pswitch_3
    if-lt v5, v7, :cond_5

    const/16 v6, 0x4f

    if-gt v5, v6, :cond_5

    .line 219
    add-int/lit8 v6, v5, -0x20

    int-to-char v2, v6

    goto :goto_1

    .line 220
    :cond_5
    if-ne v5, v8, :cond_6

    .line 221
    const/16 v2, 0x3a

    goto :goto_1

    .line 223
    :cond_6
    invoke-static {}, Lcom/sec/android/app/camaftest/core/FormatException;->getFormatInstance()Lcom/sec/android/app/camaftest/core/FormatException;

    move-result-object v6

    throw v6

    .line 231
    .end local v2    # "decodedChar":C
    .end local v5    # "next":C
    :cond_7
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 234
    .end local v0    # "c":C
    :cond_8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private static findAsteriskPattern(Lcom/sec/android/app/camaftest/core/common/BitArray;)[I
    .locals 13
    .param p0, "row"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v7

    .line 111
    .local v7, "width":I
    invoke-virtual {p0, v9}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getNextSet(I)I

    move-result v6

    .line 113
    .local v6, "rowOffset":I
    const/4 v0, 0x0

    .line 114
    .local v0, "counterPosition":I
    const/4 v10, 0x6

    new-array v1, v10, [I

    .line 115
    .local v1, "counters":[I
    move v5, v6

    .line 116
    .local v5, "patternStart":I
    const/4 v3, 0x0

    .line 117
    .local v3, "isWhite":Z
    array-length v4, v1

    .line 119
    .local v4, "patternLength":I
    move v2, v6

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_4

    .line 120
    invoke-virtual {p0, v2}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v10

    xor-int/2addr v10, v3

    if-eqz v10, :cond_0

    .line 121
    aget v10, v1, v0

    add-int/lit8 v10, v10, 0x1

    aput v10, v1, v0

    .line 119
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 123
    :cond_0
    add-int/lit8 v10, v4, -0x1

    if-ne v0, v10, :cond_2

    .line 124
    invoke-static {v1}, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->toPattern([I)I

    move-result v10

    sget v11, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->ASTERISK_ENCODING:I

    if-ne v10, v11, :cond_1

    .line 125
    new-array v10, v12, [I

    aput v5, v10, v9

    aput v2, v10, v8

    return-object v10

    .line 127
    :cond_1
    aget v10, v1, v9

    aget v11, v1, v8

    add-int/2addr v10, v11

    add-int/2addr v5, v10

    .line 128
    add-int/lit8 v10, v4, -0x2

    invoke-static {v1, v12, v1, v9, v10}, Ljava/lang/System;->arraycopy([II[III)V

    .line 129
    add-int/lit8 v10, v4, -0x2

    aput v9, v1, v10

    .line 130
    add-int/lit8 v10, v4, -0x1

    aput v9, v1, v10

    .line 131
    add-int/lit8 v0, v0, -0x1

    .line 135
    :goto_2
    aput v8, v1, v0

    .line 136
    if-nez v3, :cond_3

    move v3, v8

    :goto_3
    goto :goto_1

    .line 133
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v3, v9

    .line 136
    goto :goto_3

    .line 139
    :cond_4
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v8

    throw v8
.end method

.method private static patternToChar(I)C
    .locals 2
    .param p0, "pattern"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 170
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->CHARACTER_ENCODINGS:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 171
    sget-object v1, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->CHARACTER_ENCODINGS:[I

    aget v1, v1, v0

    if-ne v1, p0, :cond_0

    .line 172
    sget-object v1, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->ALPHABET:[C

    aget-char v1, v1, v0

    return v1

    .line 170
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 175
    :cond_1
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v1

    throw v1
.end method

.method private static toPattern([I)I
    .locals 13
    .param p0, "counters"    # [I

    .prologue
    .line 143
    array-length v6, p0

    .line 144
    .local v6, "max":I
    const/4 v10, 0x0

    .line 145
    .local v10, "sum":I
    move-object v0, p0

    .local v0, "arr$":[I
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_0

    aget v1, v0, v3

    .line 146
    .local v1, "counter":I
    add-int/2addr v10, v1

    .line 145
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 148
    .end local v1    # "counter":I
    :cond_0
    const/4 v7, 0x0

    .line 149
    .local v7, "pattern":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v6, :cond_3

    .line 150
    aget v11, p0, v2

    shl-int/lit8 v11, v11, 0x8

    mul-int/lit8 v11, v11, 0x9

    div-int v8, v11, v10

    .line 151
    .local v8, "scaledShifted":I
    shr-int/lit8 v9, v8, 0x8

    .line 152
    .local v9, "scaledUnshifted":I
    and-int/lit16 v11, v8, 0xff

    const/16 v12, 0x7f

    if-le v11, v12, :cond_1

    .line 153
    add-int/lit8 v9, v9, 0x1

    .line 155
    :cond_1
    const/4 v11, 0x1

    if-lt v9, v11, :cond_2

    const/4 v11, 0x4

    if-le v9, v11, :cond_4

    .line 156
    :cond_2
    const/4 v7, -0x1

    .line 166
    .end local v7    # "pattern":I
    .end local v8    # "scaledShifted":I
    .end local v9    # "scaledUnshifted":I
    :cond_3
    return v7

    .line 158
    .restart local v7    # "pattern":I
    .restart local v8    # "scaledShifted":I
    .restart local v9    # "scaledUnshifted":I
    :cond_4
    and-int/lit8 v11, v2, 0x1

    if-nez v11, :cond_5

    .line 159
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_2
    if-ge v4, v9, :cond_6

    .line 160
    shl-int/lit8 v11, v7, 0x1

    or-int/lit8 v7, v11, 0x1

    .line 159
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 163
    .end local v4    # "j":I
    :cond_5
    shl-int/2addr v7, v9

    .line 149
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public decodeRow(ILcom/sec/android/app/camaftest/core/common/BitArray;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 26
    .param p1, "rowNumber"    # I
    .param p2, "row"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/app/camaftest/core/common/BitArray;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/ChecksumException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    .line 61
    .local p3, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    invoke-static/range {p2 .. p2}, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->findAsteriskPattern(Lcom/sec/android/app/camaftest/core/common/BitArray;)[I

    move-result-object v19

    .line 63
    .local v19, "start":[I
    const/16 v20, 0x1

    aget v20, v19, v20

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getNextSet(I)I

    move-result v14

    .line 64
    .local v14, "nextStart":I
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v9

    .line 66
    .local v9, "end":I
    new-instance v16, Ljava/lang/StringBuilder;

    const/16 v20, 0x14

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 67
    .local v16, "result":Ljava/lang/StringBuilder;
    const/16 v20, 0x6

    move/from16 v0, v20

    new-array v7, v0, [I

    .line 71
    .local v7, "counters":[I
    :cond_0
    move-object/from16 v0, p2

    invoke-static {v0, v14, v7}, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->recordPattern(Lcom/sec/android/app/camaftest/core/common/BitArray;I[I)V

    .line 72
    invoke-static {v7}, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->toPattern([I)I

    move-result v15

    .line 73
    .local v15, "pattern":I
    if-gez v15, :cond_1

    .line 74
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v20

    throw v20

    .line 76
    :cond_1
    invoke-static {v15}, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->patternToChar(I)C

    move-result v8

    .line 77
    .local v8, "decodedChar":C
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 78
    move v11, v14

    .line 79
    .local v11, "lastStart":I
    move-object v5, v7

    .local v5, "arr$":[I
    array-length v13, v5

    .local v13, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_0
    if-ge v10, v13, :cond_2

    aget v6, v5, v10

    .line 80
    .local v6, "counter":I
    add-int/2addr v14, v6

    .line 79
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 83
    .end local v6    # "counter":I
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getNextSet(I)I

    move-result v14

    .line 84
    const/16 v20, 0x2a

    move/from16 v0, v20

    if-ne v8, v0, :cond_0

    .line 85
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v20

    add-int/lit8 v20, v20, -0x1

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 88
    if-eq v14, v9, :cond_3

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v20

    if-nez v20, :cond_4

    .line 89
    :cond_3
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v20

    throw v20

    .line 92
    :cond_4
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v20

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_5

    .line 94
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v20

    throw v20

    .line 97
    :cond_5
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->checkChecksums(Ljava/lang/CharSequence;)V

    .line 99
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v20

    add-int/lit8 v20, v20, -0x2

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 101
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;->decodeExtended(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v17

    .line 103
    .local v17, "resultString":Ljava/lang/String;
    const/16 v20, 0x1

    aget v20, v19, v20

    const/16 v21, 0x0

    aget v21, v19, v21

    add-int v20, v20, v21

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    div-float v12, v20, v21

    .line 104
    .local v12, "left":F
    add-int v20, v14, v11

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    div-float v18, v20, v21

    .line 105
    .local v18, "right":F
    new-instance v20, Lcom/sec/android/app/camaftest/core/Result;

    const/16 v21, 0x0

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Lcom/sec/android/app/camaftest/core/ResultPoint;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    new-instance v24, Lcom/sec/android/app/camaftest/core/ResultPoint;

    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v0, v12, v1}, Lcom/sec/android/app/camaftest/core/ResultPoint;-><init>(FF)V

    aput-object v24, v22, v23

    const/16 v23, 0x1

    new-instance v24, Lcom/sec/android/app/camaftest/core/ResultPoint;

    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v24

    move/from16 v1, v18

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/camaftest/core/ResultPoint;-><init>(FF)V

    aput-object v24, v22, v23

    sget-object v23, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODE_93:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    move-object/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/camaftest/core/Result;-><init>(Ljava/lang/String;[B[Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/BarcodeFormat;)V

    return-object v20
.end method
