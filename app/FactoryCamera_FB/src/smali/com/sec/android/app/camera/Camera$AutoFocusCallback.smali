.class final Lcom/sec/android/app/camera/Camera$AutoFocusCallback;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AutoFocusCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/Camera;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/camera/Camera;)V
    .locals 0

    .prologue
    .line 1055
    iput-object p1, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/Camera$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/camera/Camera;
    .param p2, "x1"    # Lcom/sec/android/app/camera/Camera$1;

    .prologue
    .line 1055
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;-><init>(Lcom/sec/android/app/camera/Camera;)V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 11
    .param p1, "focused"    # Z
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    const/16 v10, 0x1c

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 1057
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AutoFocusCallback onAutoFocus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1058
    if-eqz p1, :cond_3

    .line 1059
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # setter for: Lcom/sec/android/app/camera/Camera;->mAF_Fail_Count:I
    invoke-static {v0, v6}, Lcom/sec/android/app/camera/Camera;->access$4202(Lcom/sec/android/app/camera/Camera;I)I

    .line 1065
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/app/camera/Camera;->mFocusCallbackTime:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/camera/Camera;->access$4302(Lcom/sec/android/app/camera/Camera;J)J

    .line 1066
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Auto focus took "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mFocusCallbackTime:J
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$4300(Lcom/sec/android/app/camera/Camera;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mFocusStartTime:J
    invoke-static {v4}, Lcom/sec/android/app/camera/Camera;->access$4400(Lcom/sec/android/app/camera/Camera;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$600(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/Interface/Capturer;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1070
    const-string v0, "FactoryCamera"

    const-string v1, "onAutoFocus - mCaptureObject == null "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1073
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mImageCapture:Lcom/sec/android/app/camera/Camera$ImageCapture;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$4500(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/Camera$ImageCapture;

    move-result-object v1

    # setter for: Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/Camera;->access$602(Lcom/sec/android/app/camera/Camera;Lcom/sec/android/app/camera/Interface/Capturer;)Lcom/sec/android/app/camera/Interface/Capturer;

    .line 1076
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mFocusState:I
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4600(Lcom/sec/android/app/camera/Camera;)I

    move-result v0

    if-ne v0, v7, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$600(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/Interface/Capturer;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1077
    const-string v0, "FactoryCamera"

    const-string v1, "onAutoFocus - before mFocusState == FOCUSING_SNAP_ON_FINISH && mCaptureObject != null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1083
    if-eqz p1, :cond_4

    .line 1084
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # setter for: Lcom/sec/android/app/camera/Camera;->mFocusState:I
    invoke-static {v0, v8}, Lcom/sec/android/app/camera/Camera;->access$4602(Lcom/sec/android/app/camera/Camera;I)I

    .line 1085
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->updateFocusIndicator()V
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$800(Lcom/sec/android/app/camera/Camera;)V

    .line 1113
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mFocusToneGenerator:Landroid/media/ToneGenerator;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4800(Lcom/sec/android/app/camera/Camera;)Landroid/media/ToneGenerator;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/media/ToneGenerator;->startTone(I)Z

    .line 1114
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCaptureObject:Lcom/sec/android/app/camera/Interface/Capturer;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$600(Lcom/sec/android/app/camera/Camera;)Lcom/sec/android/app/camera/Interface/Capturer;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/camera/Interface/Capturer;->onSnap()V

    .line 1163
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->updateFocusIndicator()V
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$800(Lcom/sec/android/app/camera/Camera;)V

    .line 1164
    :cond_2
    :goto_2
    return-void

    .line 1061
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # operator++ for: Lcom/sec/android/app/camera/Camera;->mAF_Fail_Count:I
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4208(Lcom/sec/android/app/camera/Camera;)I

    goto/16 :goto_0

    .line 1087
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # setter for: Lcom/sec/android/app/camera/Camera;->mFocusState:I
    invoke-static {v0, v9}, Lcom/sec/android/app/camera/Camera;->access$4602(Lcom/sec/android/app/camera/Camera;I)I

    .line 1088
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->updateFocusIndicator()V
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$800(Lcom/sec/android/app/camera/Camera;)V

    .line 1089
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mAF_Fail_Count:I
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4200(Lcom/sec/android/app/camera/Camera;)I

    move-result v0

    if-le v0, v7, :cond_5

    const-string v0, "autotest"

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraAutoCapture:Z
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$1300(Lcom/sec/android/app/camera/Camera;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1091
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->dialogFocusPopup()V
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4700(Lcom/sec/android/app/camera/Camera;)V

    .line 1093
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mFocusToneGenerator:Landroid/media/ToneGenerator;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4800(Lcom/sec/android/app/camera/Camera;)Landroid/media/ToneGenerator;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/media/ToneGenerator;->startTone(I)Z

    .line 1094
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->shortSnapshotcancelAutoFocus()V

    .line 1095
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    # setter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/Camera;->access$4902(Lcom/sec/android/app/camera/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;

    .line 1096
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cameraType:I
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$1200(Lcom/sec/android/app/camera/Camera;)I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    const-string v1, "continuous-picture"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camera/Camera;->isSupportedFocusModes(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1098
    const-string v0, "FactoryCamera"

    const-string v1, "Focus - Camera SupportedFocusModes() : set continuous-picture mode (CAF ON)"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v0

    const-string v1, "continuous-picture"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 1100
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1101
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 1105
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraAutoCapture:Z
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$1300(Lcom/sec/android/app/camera/Camera;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "autotest"

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1107
    :cond_7
    const-string v0, "FactoryCamera"

    const-string v1, "onAutoFocus goto AF Fail dialog popup..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1108
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->dialogAFFailPopup()V

    goto/16 :goto_2

    .line 1115
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mFocusState:I
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4600(Lcom/sec/android/app/camera/Camera;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_e

    .line 1116
    const-string v0, "FactoryCamera"

    const-string v1, "onAutoFocus - before mFocusState == FOCUSING"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1118
    if-eqz p1, :cond_9

    .line 1119
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # setter for: Lcom/sec/android/app/camera/Camera;->mFocusState:I
    invoke-static {v0, v8}, Lcom/sec/android/app/camera/Camera;->access$4602(Lcom/sec/android/app/camera/Camera;I)I

    .line 1120
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # setter for: Lcom/sec/android/app/camera/Camera;->mAF_Fail_Count:I
    invoke-static {v0, v6}, Lcom/sec/android/app/camera/Camera;->access$4202(Lcom/sec/android/app/camera/Camera;I)I

    .line 1121
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->updateFocusIndicator()V
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$800(Lcom/sec/android/app/camera/Camera;)V

    goto/16 :goto_1

    .line 1123
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # setter for: Lcom/sec/android/app/camera/Camera;->mFocusState:I
    invoke-static {v0, v9}, Lcom/sec/android/app/camera/Camera;->access$4602(Lcom/sec/android/app/camera/Camera;I)I

    .line 1124
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->updateFocusIndicator()V
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$800(Lcom/sec/android/app/camera/Camera;)V

    .line 1125
    const-string v0, "FactoryCamera"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CameraSettings.FOCUSING = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mAF_Fail_Count:I
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$4200(Lcom/sec/android/app/camera/Camera;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1127
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mAF_Fail_Count:I
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4200(Lcom/sec/android/app/camera/Camera;)I

    move-result v0

    if-le v0, v7, :cond_a

    const-string v0, "autotest"

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraAutoCapture:Z
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$1300(Lcom/sec/android/app/camera/Camera;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1129
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # setter for: Lcom/sec/android/app/camera/Camera;->mAF_Fail_Count:I
    invoke-static {v0, v6}, Lcom/sec/android/app/camera/Camera;->access$4202(Lcom/sec/android/app/camera/Camera;I)I

    .line 1130
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # invokes: Lcom/sec/android/app/camera/Camera;->dialogFocusPopup()V
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4700(Lcom/sec/android/app/camera/Camera;)V

    .line 1132
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1133
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    .line 1135
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    # setter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/Camera;->access$4902(Lcom/sec/android/app/camera/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;

    .line 1136
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->cameraType:I
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$1200(Lcom/sec/android/app/camera/Camera;)I

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    const-string v1, "continuous-picture"

    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camera/Camera;->isSupportedFocusModes(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1138
    const-string v0, "FactoryCamera"

    const-string v1, "Focus - Camera SupportedFocusModes() : set continuous-picture mode (CAF ON)"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1139
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v0

    const-string v1, "continuous-picture"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 1140
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1141
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$4900(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 1145
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraAutoCapture:Z
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$1300(Lcom/sec/android/app/camera/Camera;)Z

    move-result v0

    if-nez v0, :cond_d

    const-string v0, "autotest"

    iget-object v1, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->testType:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/camera/Camera;->access$1600(Lcom/sec/android/app/camera/Camera;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1147
    :cond_d
    const-string v0, "FactoryCamera"

    const-string v1, "onAutoFocus FOCUSING goto AF Fail dialog popup..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    invoke-virtual {v0}, Lcom/sec/android/app/camera/Camera;->dialogAFFailPopup()V

    goto/16 :goto_2

    .line 1152
    :cond_e
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mFocusState:I
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$4600(Lcom/sec/android/app/camera/Camera;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1155
    const-string v0, "FactoryCamera"

    const-string v1, "onAutoFocus - before mFocusState == FOCUS_NOT_STARTED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1159
    iget-object v0, p0, Lcom/sec/android/app/camera/Camera$AutoFocusCallback;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/sec/android/app/camera/Camera;->access$5000(Lcom/sec/android/app/camera/Camera;)Landroid/hardware/Camera;

    move-result-object v0

    if-eqz v0, :cond_1

    goto/16 :goto_1
.end method
