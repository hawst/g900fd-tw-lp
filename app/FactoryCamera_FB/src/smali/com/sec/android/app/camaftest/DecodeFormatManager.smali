.class final Lcom/sec/android/app/camaftest/DecodeFormatManager;
.super Ljava/lang/Object;
.source "DecodeFormatManager.java"


# static fields
.field private static final COMMA_PATTERN:Ljava/util/regex/Pattern;

.field static final DATA_MATRIX_FORMATS:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/camaftest/core/BarcodeFormat;",
            ">;"
        }
    .end annotation
.end field

.field static final ONE_D_FORMATS:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/camaftest/core/BarcodeFormat;",
            ">;"
        }
    .end annotation
.end field

.field static final PRODUCT_FORMATS:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/camaftest/core/BarcodeFormat;",
            ">;"
        }
    .end annotation
.end field

.field static final QR_CODE_FORMATS:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/camaftest/core/BarcodeFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 32
    const-string v0, ","

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/DecodeFormatManager;->COMMA_PATTERN:Ljava/util/regex/Pattern;

    .line 36
    sget-object v0, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->QR_CODE:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/DecodeFormatManager;->QR_CODE_FORMATS:Ljava/util/Collection;

    .line 37
    sget-object v0, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->DATA_MATRIX:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/DecodeFormatManager;->DATA_MATRIX_FORMATS:Ljava/util/Collection;

    .line 39
    sget-object v0, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_A:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    sget-object v1, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_E:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    sget-object v2, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_13:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_8:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->RSS_14:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-static {v0, v1, v2, v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/DecodeFormatManager;->PRODUCT_FORMATS:Ljava/util/Collection;

    .line 44
    sget-object v0, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODE_39:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    sget-object v1, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODE_93:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    sget-object v2, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODE_128:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->ITF:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODABAR:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-static {v0, v1, v2, v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/DecodeFormatManager;->ONE_D_FORMATS:Ljava/util/Collection;

    .line 49
    sget-object v0, Lcom/sec/android/app/camaftest/DecodeFormatManager;->ONE_D_FORMATS:Ljava/util/Collection;

    sget-object v1, Lcom/sec/android/app/camaftest/DecodeFormatManager;->PRODUCT_FORMATS:Ljava/util/Collection;

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 50
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static parseDecodeFormats(Landroid/content/Intent;)Ljava/util/Collection;
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/camaftest/core/BarcodeFormat;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    const/4 v0, 0x0

    .line 56
    .local v0, "scanFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "SCAN_FORMATS"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 57
    .local v1, "scanFormatsString":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 58
    sget-object v2, Lcom/sec/android/app/camaftest/DecodeFormatManager;->COMMA_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 60
    :cond_0
    const-string v2, "SCAN_MODE"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/camaftest/DecodeFormatManager;->parseDecodeFormats(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v2

    return-object v2
.end method

.method static parseDecodeFormats(Landroid/net/Uri;)Ljava/util/Collection;
    .locals 4
    .param p0, "inputUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/camaftest/core/BarcodeFormat;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 64
    const-string v1, "SCAN_FORMATS"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 65
    .local v0, "formats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 66
    sget-object v2, Lcom/sec/android/app/camaftest/DecodeFormatManager;->COMMA_PATTERN:Ljava/util/regex/Pattern;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 68
    :cond_0
    const-string v1, "SCAN_MODE"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/camaftest/DecodeFormatManager;->parseDecodeFormats(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v1

    return-object v1
.end method

.method private static parseDecodeFormats(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/util/Collection;
    .locals 4
    .param p1, "decodeMode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/camaftest/core/BarcodeFormat;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "scanFormats":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    .line 74
    const-class v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-static {v3}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 76
    .local v1, "formats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    :try_start_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 77
    .local v0, "format":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->valueOf(Ljava/lang/String;)Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 80
    .end local v0    # "format":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v3

    .line 84
    .end local v1    # "formats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    :cond_0
    if-eqz p1, :cond_5

    .line 85
    const-string v3, "PRODUCT_MODE"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 86
    sget-object v1, Lcom/sec/android/app/camaftest/DecodeFormatManager;->PRODUCT_FORMATS:Ljava/util/Collection;

    .line 98
    :cond_1
    :goto_1
    return-object v1

    .line 88
    :cond_2
    const-string v3, "QR_CODE_MODE"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 89
    sget-object v1, Lcom/sec/android/app/camaftest/DecodeFormatManager;->QR_CODE_FORMATS:Ljava/util/Collection;

    goto :goto_1

    .line 91
    :cond_3
    const-string v3, "DATA_MATRIX_MODE"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 92
    sget-object v1, Lcom/sec/android/app/camaftest/DecodeFormatManager;->DATA_MATRIX_FORMATS:Ljava/util/Collection;

    goto :goto_1

    .line 94
    :cond_4
    const-string v3, "ONE_D_MODE"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 95
    sget-object v1, Lcom/sec/android/app/camaftest/DecodeFormatManager;->ONE_D_FORMATS:Ljava/util/Collection;

    goto :goto_1

    .line 98
    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method
