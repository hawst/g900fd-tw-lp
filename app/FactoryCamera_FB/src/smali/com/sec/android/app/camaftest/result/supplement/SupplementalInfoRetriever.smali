.class public abstract Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;
.super Ljava/lang/Object;
.source "SupplementalInfoRetriever.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static executorInstance:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private final handler:Landroid/os/Handler;

.field private final textViewRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;->executorInstance:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method constructor <init>(Landroid/widget/TextView;Landroid/os/Handler;)V
    .locals 1
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;->textViewRef:Ljava/lang/ref/WeakReference;

    .line 84
    iput-object p2, p0, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;->handler:Landroid/os/Handler;

    .line 85
    return-void
.end method

.method private static declared-synchronized getExecutorService()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .prologue
    .line 47
    const-class v1, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;->executorInstance:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever$1;

    invoke-direct {v0}, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever$1;-><init>()V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;->executorInstance:Ljava/util/concurrent/ExecutorService;

    .line 57
    :cond_0
    sget-object v0, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;->executorInstance:Ljava/util/concurrent/ExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static maybeInvokeRetrieval(Landroid/widget/TextView;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 9
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "result"    # Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    new-instance v4, Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 67
    .local v4, "retrievers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;>;"
    instance-of v5, p1, Lcom/sec/android/app/camaftest/core/client/result/URIParsedResult;

    if-eqz v5, :cond_0

    .line 68
    new-instance v5, Lcom/sec/android/app/camaftest/result/supplement/URIResultInfoRetriever;

    check-cast p1, Lcom/sec/android/app/camaftest/core/client/result/URIParsedResult;

    .end local p1    # "result":Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;
    invoke-direct {v5, p0, p1, p2, p3}, Lcom/sec/android/app/camaftest/result/supplement/URIResultInfoRetriever;-><init>(Landroid/widget/TextView;Lcom/sec/android/app/camaftest/core/client/result/URIParsedResult;Landroid/os/Handler;Landroid/content/Context;)V

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_0
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;

    .line 72
    .local v3, "retriever":Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;
    invoke-static {}, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 73
    .local v0, "executor":Ljava/util/concurrent/ExecutorService;
    invoke-interface {v0, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v1

    .line 75
    .local v1, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    new-instance v5, Lcom/sec/android/app/camaftest/result/supplement/KillerCallable;

    const-wide/16 v6, 0xa

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v5, v1, v6, v7, v8}, Lcom/sec/android/app/camaftest/result/supplement/KillerCallable;-><init>(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;)V

    invoke-interface {v0, v5}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 77
    .end local v0    # "executor":Ljava/util/concurrent/ExecutorService;
    .end local v1    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    .end local v3    # "retriever":Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;
    :cond_1
    return-void
.end method


# virtual methods
.method final append(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p1, "itemID"    # Ljava/lang/String;
    .param p2, "source"    # Ljava/lang/String;
    .param p3, "newTexts"    # [Ljava/lang/String;
    .param p4, "linkURL"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v11, p0, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;->textViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v11}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 98
    .local v10, "textView":Landroid/widget/TextView;
    if-nez v10, :cond_0

    .line 99
    new-instance v11, Ljava/lang/InterruptedException;

    invoke-direct {v11}, Ljava/lang/InterruptedException;-><init>()V

    throw v11

    .line 102
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .local v9, "newTextCombined":Ljava/lang/StringBuilder;
    if-eqz p2, :cond_1

    .line 105
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    :cond_1
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    .line 110
    .local v7, "linkStart":I
    const/4 v3, 0x1

    .line 111
    .local v3, "first":Z
    move-object/from16 v1, p3

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_3

    aget-object v8, v1, v4

    .line 112
    .local v8, "newText":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 113
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const/4 v3, 0x0

    .line 111
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 116
    :cond_2
    const-string v11, " ["

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const/16 v11, 0x5d

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 122
    .end local v8    # "newText":Ljava/lang/String;
    :cond_3
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    .line 124
    .local v6, "linkEnd":I
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 125
    .restart local v8    # "newText":Ljava/lang/String;
    new-instance v2, Landroid/text/SpannableString;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v2, v11}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 126
    .local v2, "content":Landroid/text/Spannable;
    if-eqz p4, :cond_4

    .line 127
    new-instance v11, Landroid/text/style/URLSpan;

    move-object/from16 v0, p4

    invoke-direct {v11, v0}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    const/16 v12, 0x21

    invoke-interface {v2, v11, v7, v6, v12}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 130
    :cond_4
    iget-object v11, p0, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;->handler:Landroid/os/Handler;

    new-instance v12, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever$2;

    invoke-direct {v12, p0, v10, v2}, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever$2;-><init>(Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;Landroid/widget/TextView;Landroid/text/Spannable;)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 137
    return-void
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final call()Ljava/lang/Void;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;->retrieveSupplementalInfo()V

    .line 90
    const/4 v0, 0x0

    return-object v0
.end method

.method abstract retrieveSupplementalInfo()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation
.end method
