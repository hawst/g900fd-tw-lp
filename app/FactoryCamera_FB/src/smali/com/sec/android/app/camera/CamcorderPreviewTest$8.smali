.class Lcom/sec/android/app/camera/CamcorderPreviewTest$8;
.super Ljava/lang/Object;
.source "CamcorderPreviewTest.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogNGPopup(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V
    .locals 0

    .prologue
    .line 2031
    iput-object p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$8;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 2033
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "CamcorderPreviewTest.dialogNGPopup OK Button..."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2034
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dialogNGPopup - testType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$8;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2100(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2035
    const-string v0, "autotest"

    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$8;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2100(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2036
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$8;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$2202(Lcom/sec/android/app/camera/CamcorderPreviewTest;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 2038
    :cond_0
    return-void
.end method
