.class public final Lcom/sec/android/app/camaftest/core/MultiFormatReader;
.super Ljava/lang/Object;
.source "MultiFormatReader.java"

# interfaces
.implements Lcom/sec/android/app/camaftest/core/Reader;


# instance fields
.field private hints:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;"
        }
    .end annotation
.end field

.field private readers:[Lcom/sec/android/app/camaftest/core/Reader;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private decodeInternal(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 5
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 162
    iget-object v4, p0, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->readers:[Lcom/sec/android/app/camaftest/core/Reader;

    if-eqz v4, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->readers:[Lcom/sec/android/app/camaftest/core/Reader;

    .local v0, "arr$":[Lcom/sec/android/app/camaftest/core/Reader;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 165
    .local v3, "reader":Lcom/sec/android/app/camaftest/core/Reader;
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->hints:Ljava/util/Map;

    invoke-interface {v3, p1, v4}, Lcom/sec/android/app/camaftest/core/Reader;->decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    :try_end_0
    .catch Lcom/sec/android/app/camaftest/core/ReaderException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    .line 166
    :catch_0
    move-exception v4

    .line 163
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 171
    .end local v0    # "arr$":[Lcom/sec/android/app/camaftest/core/Reader;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "reader":Lcom/sec/android/app/camaftest/core/Reader;
    :cond_0
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v4

    throw v4
.end method


# virtual methods
.method public decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 1
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->setHints(Ljava/util/Map;)V

    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->decodeInternal(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    return-object v0
.end method

.method public decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 1
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/camaftest/core/BinaryBitmap;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 67
    .local p2, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    invoke-virtual {p0, p2}, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->setHints(Ljava/util/Map;)V

    .line 68
    invoke-direct {p0, p1}, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->decodeInternal(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    return-object v0
.end method

.method public decodeWithState(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 1
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->readers:[Lcom/sec/android/app/camaftest/core/Reader;

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->setHints(Ljava/util/Map;)V

    .line 84
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->decodeInternal(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    return-object v0
.end method

.method public reset()V
    .locals 5

    .prologue
    .line 154
    iget-object v4, p0, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->readers:[Lcom/sec/android/app/camaftest/core/Reader;

    if-eqz v4, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->readers:[Lcom/sec/android/app/camaftest/core/Reader;

    .local v0, "arr$":[Lcom/sec/android/app/camaftest/core/Reader;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 156
    .local v3, "reader":Lcom/sec/android/app/camaftest/core/Reader;
    invoke-interface {v3}, Lcom/sec/android/app/camaftest/core/Reader;->reset()V

    .line 155
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 159
    .end local v0    # "arr$":[Lcom/sec/android/app/camaftest/core/Reader;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "reader":Lcom/sec/android/app/camaftest/core/Reader;
    :cond_0
    return-void
.end method

.method public setHints(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .local p1, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 95
    iput-object p1, p0, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->hints:Ljava/util/Map;

    .line 97
    if-eqz p1, :cond_a

    sget-object v4, Lcom/sec/android/app/camaftest/core/DecodeHintType;->TRY_HARDER:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    invoke-interface {p1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    move v3, v5

    .line 98
    .local v3, "tryHarder":Z
    :goto_0
    if-nez p1, :cond_b

    const/4 v1, 0x0

    .line 100
    .local v1, "formats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .local v2, "readers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/Reader;>;"
    if-eqz v1, :cond_7

    .line 102
    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_A:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_E:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_13:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_8:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODABAR:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODE_39:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODE_93:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODE_128:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->ITF:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->RSS_14:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->RSS_EXPANDED:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v0, v5

    .line 115
    .local v0, "addOneDReader":Z
    :cond_1
    if-eqz v0, :cond_2

    if-nez v3, :cond_2

    .line 116
    new-instance v4, Lcom/sec/android/app/camaftest/core/oned/MultiFormatOneDReader;

    invoke-direct {v4, p1}, Lcom/sec/android/app/camaftest/core/oned/MultiFormatOneDReader;-><init>(Ljava/util/Map;)V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_2
    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->QR_CODE:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 119
    new-instance v4, Lcom/sec/android/app/camaftest/core/qrcode/QRCodeReader;

    invoke-direct {v4}, Lcom/sec/android/app/camaftest/core/qrcode/QRCodeReader;-><init>()V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 121
    :cond_3
    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->DATA_MATRIX:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 122
    new-instance v4, Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;

    invoke-direct {v4}, Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;-><init>()V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 124
    :cond_4
    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->AZTEC:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 125
    new-instance v4, Lcom/sec/android/app/camaftest/core/aztec/AztecReader;

    invoke-direct {v4}, Lcom/sec/android/app/camaftest/core/aztec/AztecReader;-><init>()V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 127
    :cond_5
    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->MAXICODE:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v1, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 128
    new-instance v4, Lcom/sec/android/app/camaftest/core/maxicode/MaxiCodeReader;

    invoke-direct {v4}, Lcom/sec/android/app/camaftest/core/maxicode/MaxiCodeReader;-><init>()V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_6
    if-eqz v0, :cond_7

    if-eqz v3, :cond_7

    .line 132
    new-instance v4, Lcom/sec/android/app/camaftest/core/oned/MultiFormatOneDReader;

    invoke-direct {v4, p1}, Lcom/sec/android/app/camaftest/core/oned/MultiFormatOneDReader;-><init>(Ljava/util/Map;)V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 135
    .end local v0    # "addOneDReader":Z
    :cond_7
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 136
    if-nez v3, :cond_8

    .line 137
    new-instance v4, Lcom/sec/android/app/camaftest/core/oned/MultiFormatOneDReader;

    invoke-direct {v4, p1}, Lcom/sec/android/app/camaftest/core/oned/MultiFormatOneDReader;-><init>(Ljava/util/Map;)V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 140
    :cond_8
    new-instance v4, Lcom/sec/android/app/camaftest/core/qrcode/QRCodeReader;

    invoke-direct {v4}, Lcom/sec/android/app/camaftest/core/qrcode/QRCodeReader;-><init>()V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 141
    new-instance v4, Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;

    invoke-direct {v4}, Lcom/sec/android/app/camaftest/core/datamatrix/DataMatrixReader;-><init>()V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 142
    new-instance v4, Lcom/sec/android/app/camaftest/core/aztec/AztecReader;

    invoke-direct {v4}, Lcom/sec/android/app/camaftest/core/aztec/AztecReader;-><init>()V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 143
    new-instance v4, Lcom/sec/android/app/camaftest/core/maxicode/MaxiCodeReader;

    invoke-direct {v4}, Lcom/sec/android/app/camaftest/core/maxicode/MaxiCodeReader;-><init>()V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 145
    if-eqz v3, :cond_9

    .line 146
    new-instance v4, Lcom/sec/android/app/camaftest/core/oned/MultiFormatOneDReader;

    invoke-direct {v4, p1}, Lcom/sec/android/app/camaftest/core/oned/MultiFormatOneDReader;-><init>(Ljava/util/Map;)V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_9
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v4

    new-array v4, v4, [Lcom/sec/android/app/camaftest/core/Reader;

    invoke-interface {v2, v4}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/sec/android/app/camaftest/core/Reader;

    iput-object v4, p0, Lcom/sec/android/app/camaftest/core/MultiFormatReader;->readers:[Lcom/sec/android/app/camaftest/core/Reader;

    .line 150
    return-void

    .end local v1    # "formats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    .end local v2    # "readers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/Reader;>;"
    .end local v3    # "tryHarder":Z
    :cond_a
    move v3, v0

    .line 97
    goto/16 :goto_0

    .line 98
    .restart local v3    # "tryHarder":Z
    :cond_b
    sget-object v4, Lcom/sec/android/app/camaftest/core/DecodeHintType;->POSSIBLE_FORMATS:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Collection;

    move-object v1, v4

    goto/16 :goto_1
.end method
