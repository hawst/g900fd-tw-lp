.class Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;
.super Ljava/lang/Object;
.source "CameraFirmware_user.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camerafirmware/CameraFirmware_user;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    .line 54
    const-string v2, "CameraFirmware_user"

    const-string v3, "Button Clicked "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const/4 v1, 0x0

    .line 57
    .local v1, "toast_text":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 260
    :goto_0
    return-void

    .line 59
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/widget/Toast;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 60
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->cancel()V

    .line 62
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    if-eqz v2, :cond_1

    .line 64
    :try_start_0
    const-string v2, "CameraFirmware_user"

    const-string v3, "mCameraDevice.setFirmwareMode(CameraDeviceController.FIRMWAREMODE_VERSION)"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    .line 72
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v2, :cond_2

    .line 73
    const-string v2, "CameraFirmware_user"

    const-string v3, "mRear is null, rear FW info will be created"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    new-instance v3, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 76
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 77
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->DISPLAY_EXTERNAL_ISP:Z

    if-nez v2, :cond_4

    .line 78
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Rear Camera (Main)\nCam FW Ver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Phone FW Ver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 88
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    if-nez v2, :cond_3

    .line 89
    const-string v2, "CameraFirmware_user"

    const-string v3, "mFront is null, front FW infor will be created"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    new-instance v3, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    invoke-direct {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;-><init>()V

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    .line 92
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 93
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\nFront Camera\nCam FW Ver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Phone FW Ver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-static {v3, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$002(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 99
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 82
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Rear Camera (Main)\nCam FW Ver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Isp FW Ver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getIspFWVer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Phone FW Ver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 104
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f090023

    if-ne v2, v3, :cond_7

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v2, :cond_5

    .line 106
    const-string v2, "CameraFirmware_user"

    const-string v3, "mRear is null, rear FW info will be created"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    new-instance v3, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 109
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 119
    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    if-eqz v2, :cond_6

    .line 121
    :try_start_1
    const-string v2, "CameraFirmware_user"

    const-string v3, "mCameraDevice.setFirmwareMode(CameraDeviceController.FIRMWAREMODE_VERSION)"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 132
    :cond_6
    :goto_4
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->ChkUserFirmwareFile()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->getCamFWVendor()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->getPhoneFWVendor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 135
    const-string v2, "CameraFirmware_user"

    const-string v3, "valid vendor"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->IsNewFirmwareDate()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto/16 :goto_0

    .line 155
    :pswitch_2
    const-string v2, "CameraFirmware_user"

    const-string v3, "SDCARD: This is the latest version.."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    const v3, 0x7f0a0010

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->dialogErrorPopup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$300(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "CameraFirmware_user"

    const-string v3, "Something goes wrong"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    const v3, 0x7f0a000f

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->dialogErrorPopup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$300(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;I)V

    goto/16 :goto_0

    .line 112
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    if-nez v2, :cond_8

    .line 113
    const-string v2, "CameraFirmware_user"

    const-string v3, "mFront is null, front FW infor will be created"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    new-instance v3, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    invoke-direct {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;-><init>()V

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    .line 116
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    goto/16 :goto_3

    .line 139
    :pswitch_3
    :try_start_3
    const-string v2, "CameraFirmware_user"

    const-string v3, "SDCARD: Updating New Firmwareversion..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    const v4, 0x7f0a0011

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->DialogPopup(I)Landroid/app/AlertDialog;
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$200(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;I)Landroid/app/AlertDialog;

    move-result-object v3

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mPopup:Landroid/app/AlertDialog;
    invoke-static {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$102(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mPopup:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$100(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 144
    :pswitch_4
    const-string v2, "CameraFirmware_user"

    const-string v3, "SDCARD: Same date.."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->IsNewFirmwareVersion()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 146
    const-string v2, "CameraFirmware_user"

    const-string v3, "SDCARD: Updating New Firmwareversion..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    const v4, 0x7f0a0011

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->DialogPopup(I)Landroid/app/AlertDialog;
    invoke-static {v3, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$200(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;I)Landroid/app/AlertDialog;

    move-result-object v3

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mPopup:Landroid/app/AlertDialog;
    invoke-static {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$102(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 148
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mPopup:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$100(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 150
    :cond_9
    const-string v2, "CameraFirmware_user"

    const-string v3, "SDCARD: This is the latest version.."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    const v3, 0x7f0a0010

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->dialogErrorPopup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$300(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;I)V

    goto/16 :goto_0

    .line 160
    :cond_a
    const-string v2, "CameraFirmware_user"

    const-string v3, "SDCARD: Invalid vendor"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    const v3, 0x7f0a000e

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->dialogErrorPopup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$300(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;I)V

    goto/16 :goto_0

    .line 166
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->getCamFWVendor()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->getPhoneFWVendor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 167
    const-string v2, "CameraFirmware_user"

    const-string v3, "valid vendor"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->IsNewFirmwareDate()I

    move-result v2

    packed-switch v2, :pswitch_data_2

    goto/16 :goto_0

    .line 195
    :pswitch_5
    const-string v2, "CameraFirmware_user"

    const-string v3, "This is the latest version.."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    const v3, 0x7f0a0010

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->dialogErrorPopup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$300(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;I)V

    goto/16 :goto_0

    .line 171
    :pswitch_6
    const-string v2, "CameraFirmware_user"

    const-string v3, "Updating New Firmwareversion..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    sget-object v3, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_UPDATE:[B

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->setManageMode([B)V

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->showUpdateProgress()V

    .line 176
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->startUpdateThread()V

    goto/16 :goto_0

    .line 181
    :pswitch_7
    const-string v2, "CameraFirmware_user"

    const-string v3, "Same date.."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->IsNewFirmwareVersion()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 183
    const-string v2, "CameraFirmware_user"

    const-string v3, "Updating New Firmwareversion..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    sget-object v3, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_UPDATE:[B

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->setManageMode([B)V

    .line 187
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->showUpdateProgress()V

    .line 188
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->startUpdateThread()V

    goto/16 :goto_0

    .line 190
    :cond_c
    const-string v2, "CameraFirmware_user"

    const-string v3, "This is the latest version.."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    const v3, 0x7f0a0010

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->dialogErrorPopup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$300(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;I)V

    goto/16 :goto_0

    .line 200
    :cond_d
    const-string v2, "CameraFirmware_user"

    const-string v3, "Invalid vendor"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    const v3, 0x7f0a000e

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->dialogErrorPopup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$300(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 212
    :pswitch_8
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/widget/Toast;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 213
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->cancel()V

    .line 215
    :cond_e
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    if-eqz v2, :cond_f

    .line 217
    :try_start_4
    const-string v2, "CameraFirmware_user"

    const-string v3, "mCameraDevice.setFirmwareMode(CameraDeviceController.FIRMWAREMODE_VERSION)"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1

    .line 226
    :cond_f
    :goto_5
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v2, :cond_10

    .line 227
    const-string v2, "CameraFirmware_user"

    const-string v3, "mRear is null, rear FW info will be created"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    new-instance v3, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 230
    :cond_10
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 231
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Rear Camera (Main)\nPhone FW Ver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v2, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    if-nez v2, :cond_11

    .line 235
    const-string v2, "CameraFirmware_user"

    const-string v3, "mFront is null, front FW infor will be created"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    new-instance v3, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    invoke-direct {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;-><init>()V

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    .line 238
    :cond_11
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    iput-object v3, v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 239
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\nFront Camera\nPhone FW Ver : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, v3, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 242
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    invoke-static {v3, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$002(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 244
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 248
    :pswitch_9
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/widget/Toast;

    move-result-object v2

    if-eqz v2, :cond_12

    .line 249
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->cancel()V

    .line 252
    :cond_12
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Rear Camera (Main)\n( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v6, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v6, v6, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->PREF_KEY_UPCOUNT_USER:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->getUpdateCount(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v6, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v6, v6, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->PREF_KEY_UPCOUNT_ENG:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->getUpdateCount(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " )\n\nFront\n( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v6, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v6, v6, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->PREF_KEY_UPCOUNT_USER_FRONT:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->getUpdateCount(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v6, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    iget-object v6, v6, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->PREF_KEY_UPCOUNT_ENG_FRONT:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->getUpdateCount(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " )"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v2, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$002(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 257
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 221
    :catch_1
    move-exception v2

    goto/16 :goto_5

    .line 125
    :catch_2
    move-exception v2

    goto/16 :goto_4

    .line 68
    :catch_3
    move-exception v2

    goto/16 :goto_1

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x7f090022
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 137
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch

    .line 169
    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_5
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method
