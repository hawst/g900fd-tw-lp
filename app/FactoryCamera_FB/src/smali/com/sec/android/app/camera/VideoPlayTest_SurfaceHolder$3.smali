.class Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$3;
.super Landroid/os/Handler;
.source "VideoPlayTest_SurfaceHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$3;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 325
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 327
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$3;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    iget-object v1, v1, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$3;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # invokes: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->setProgress()I
    invoke-static {v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$600(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)I

    move-result v0

    .line 332
    .local v0, "pos":I
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$3;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mDragging:Z
    invoke-static {v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$700(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$3;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    iget-object v1, v1, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 333
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$3;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    .line 334
    rem-int/lit16 v1, v0, 0x3e8

    rsub-int v1, v1, 0x3e8

    int-to-long v2, v1

    invoke-virtual {p0, p1, v2, v3}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$3;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 325
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
