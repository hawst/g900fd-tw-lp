.class final Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
.super Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedObject;
.source "DecodedInformation.java"


# instance fields
.field private final newString:Ljava/lang/String;

.field private final remaining:Z

.field private final remainingValue:I


# direct methods
.method constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "newPosition"    # I
    .param p2, "newString"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedObject;-><init>(I)V

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->newString:Ljava/lang/String;

    .line 43
    iput-boolean v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->remaining:Z

    .line 44
    iput v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->remainingValue:I

    .line 45
    return-void
.end method

.method constructor <init>(ILjava/lang/String;I)V
    .locals 1
    .param p1, "newPosition"    # I
    .param p2, "newString"    # Ljava/lang/String;
    .param p3, "remainingValue"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedObject;-><init>(I)V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->remaining:Z

    .line 50
    iput p3, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->remainingValue:I

    .line 51
    iput-object p2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->newString:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method getNewString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->newString:Ljava/lang/String;

    return-object v0
.end method

.method getRemainingValue()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->remainingValue:I

    return v0
.end method

.method isRemaining()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;->remaining:Z

    return v0
.end method
