.class public final Lcom/sec/android/app/camaftest/result/ResultButtonListener;
.super Ljava/lang/Object;
.source "ResultButtonListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final index:I

.field private final resultHandler:Lcom/sec/android/app/camaftest/result/ResultHandler;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camaftest/result/ResultHandler;I)V
    .locals 0
    .param p1, "resultHandler"    # Lcom/sec/android/app/camaftest/result/ResultHandler;
    .param p2, "index"    # I

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/camaftest/result/ResultButtonListener;->resultHandler:Lcom/sec/android/app/camaftest/result/ResultHandler;

    .line 34
    iput p2, p0, Lcom/sec/android/app/camaftest/result/ResultButtonListener;->index:I

    .line 35
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/camaftest/result/ResultButtonListener;->resultHandler:Lcom/sec/android/app/camaftest/result/ResultHandler;

    iget v1, p0, Lcom/sec/android/app/camaftest/result/ResultButtonListener;->index:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camaftest/result/ResultHandler;->handleButtonPress(I)V

    .line 40
    return-void
.end method
