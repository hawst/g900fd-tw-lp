.class public final Lcom/sec/android/app/camaftest/core/oned/UPCAReader;
.super Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;
.source "UPCAReader.java"


# instance fields
.field private final ean13Reader:Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;-><init>()V

    .line 40
    new-instance v0, Lcom/sec/android/app/camaftest/core/oned/EAN13Reader;

    invoke-direct {v0}, Lcom/sec/android/app/camaftest/core/oned/EAN13Reader;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/UPCAReader;->ean13Reader:Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;

    return-void
.end method

.method private static maybeReturnResult(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 6
    .param p0, "result"    # Lcom/sec/android/app/camaftest/core/Result;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/core/Result;->getText()Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "text":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x30

    if-ne v1, v2, :cond_0

    .line 75
    new-instance v1, Lcom/sec/android/app/camaftest/core/Result;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/core/Result;->getResultPoints()[Lcom/sec/android/app/camaftest/core/ResultPoint;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_A:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/camaftest/core/Result;-><init>(Ljava/lang/String;[B[Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/BarcodeFormat;)V

    return-object v1

    .line 77
    :cond_0
    invoke-static {}, Lcom/sec/android/app/camaftest/core/FormatException;->getFormatInstance()Lcom/sec/android/app/camaftest/core/FormatException;

    move-result-object v1

    throw v1
.end method


# virtual methods
.method public decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 1
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/UPCAReader;->ean13Reader:Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;->decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/camaftest/core/oned/UPCAReader;->maybeReturnResult(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    return-object v0
.end method

.method public decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 1
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/camaftest/core/BinaryBitmap;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    .line 59
    .local p2, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/UPCAReader;->ean13Reader:Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;->decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/camaftest/core/oned/UPCAReader;->maybeReturnResult(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    return-object v0
.end method

.method protected decodeMiddle(Lcom/sec/android/app/camaftest/core/common/BitArray;[ILjava/lang/StringBuilder;)I
    .locals 1
    .param p1, "row"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .param p2, "startRange"    # [I
    .param p3, "resultString"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/UPCAReader;->ean13Reader:Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;->decodeMiddle(Lcom/sec/android/app/camaftest/core/common/BitArray;[ILjava/lang/StringBuilder;)I

    move-result v0

    return v0
.end method

.method public decodeRow(ILcom/sec/android/app/camaftest/core/common/BitArray;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 1
    .param p1, "rowNumber"    # I
    .param p2, "row"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/app/camaftest/core/common/BitArray;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/FormatException;,
            Lcom/sec/android/app/camaftest/core/ChecksumException;
        }
    .end annotation

    .prologue
    .line 49
    .local p3, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/UPCAReader;->ean13Reader:Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;->decodeRow(ILcom/sec/android/app/camaftest/core/common/BitArray;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/camaftest/core/oned/UPCAReader;->maybeReturnResult(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    return-object v0
.end method

.method public decodeRow(ILcom/sec/android/app/camaftest/core/common/BitArray;[ILjava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 1
    .param p1, "rowNumber"    # I
    .param p2, "row"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .param p3, "startGuardRange"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/app/camaftest/core/common/BitArray;",
            "[I",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/FormatException;,
            Lcom/sec/android/app/camaftest/core/ChecksumException;
        }
    .end annotation

    .prologue
    .line 44
    .local p4, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/UPCAReader;->ean13Reader:Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;->decodeRow(ILcom/sec/android/app/camaftest/core/common/BitArray;[ILjava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/camaftest/core/oned/UPCAReader;->maybeReturnResult(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    return-object v0
.end method

.method getBarcodeFormat()Lcom/sec/android/app/camaftest/core/BarcodeFormat;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_A:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    return-object v0
.end method
