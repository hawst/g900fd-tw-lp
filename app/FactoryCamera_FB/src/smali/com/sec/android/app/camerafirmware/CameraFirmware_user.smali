.class public Lcom/sec/android/app/camerafirmware/CameraFirmware_user;
.super Lcom/sec/android/app/camerafirmware/CameraFirmware;
.source "CameraFirmware_user.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CameraFirmware_user"


# instance fields
.field mClickListener:Landroid/view/View$OnClickListener;

.field private mCurrentToast:Landroid/widget/Toast;

.field private mErrorPopup:Landroid/app/AlertDialog;

.field private mPopup:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;-><init>()V

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mPopup:Landroid/app/AlertDialog;

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mErrorPopup:Landroid/app/AlertDialog;

    .line 52
    new-instance v0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$1;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)V

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private DialogPopup(I)Landroid/app/AlertDialog;
    .locals 3
    .param p1, "messageId"    # I

    .prologue
    .line 301
    const-string v1, "CameraFirmware_user"

    const-string v2, "DialogPopup"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 304
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 307
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 308
    .local v0, "Dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 309
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cam FW Ver : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Phone FW Ver : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 311
    const v1, 0x7f0a0002

    new-instance v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$2;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 319
    const-string v1, "No"

    new-instance v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$3;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 325
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method static synthetic access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware_user;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCurrentToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware_user;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mPopup:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;I)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware_user;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->DialogPopup(I)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware_user;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->dialogErrorPopup(I)V

    return-void
.end method

.method private dialogErrorPopup(I)V
    .locals 3
    .param p1, "messageId"    # I

    .prologue
    .line 329
    const-string v1, "CameraFirmware_user"

    const-string v2, "dialogErrorPopup"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 332
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 334
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 335
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 337
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cam FW Ver : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Phone FW Ver : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 339
    const v1, 0x7f0a0001

    new-instance v2, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user$4;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmware_user;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 344
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 345
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mErrorPopup:Landroid/app/AlertDialog;

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 347
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    const v5, 0x7f090025

    const v4, 0x7f090024

    const v3, 0x7f090023

    const/16 v2, 0x8

    .line 264
    const v0, 0x7f090022

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->INTERNAL_ISP:Z

    if-eqz v0, :cond_2

    .line 267
    invoke-virtual {p0, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 268
    invoke-virtual {p0, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 269
    invoke-virtual {p0, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 276
    :goto_0
    const v0, 0x7f090026

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;->resetFWInfo()V

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-eqz v0, :cond_1

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->resetFWInfo()V

    .line 284
    :cond_1
    return-void

    .line 271
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    invoke-virtual {p0, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 273
    invoke-virtual {p0, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method protected ChkSOCTypeModel()Z
    .locals 2

    .prologue
    .line 350
    const-string v0, "CameraFirmware_user"

    const-string v1, "ChkSOCTypeModel"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v1, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->ChkFirmwareFile(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v1, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->ChkFirmwareFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 357
    :cond_1
    const-string v0, "CameraFirmware_user"

    const-string v1, "ChkSOCTypeModel false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    const/4 v0, 0x0

    .line 361
    :goto_0
    return v0

    .line 360
    :cond_2
    const-string v0, "CameraFirmware_user"

    const-string v1, "ChkSOCTypeModel true"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->setContentView(I)V

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->init()V

    .line 50
    return-void
.end method

.method protected updateFirmwareUpdateCount()V
    .locals 4

    .prologue
    .line 289
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-ne v1, v2, :cond_0

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->PREF_KEY_UPCOUNT_USER:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->getUpdateCount(Ljava/lang/String;)I

    move-result v0

    .line 291
    .local v0, "updateCount":I
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->PREF_KEY_UPCOUNT_USER:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->setUpdateCount(Ljava/lang/String;I)V

    .line 297
    :goto_0
    const-string v1, "CameraFirmware_user"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateFirmwareUpdateCount - PREF_KEY_UPCOUNT_USER:["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    return-void

    .line 293
    .end local v0    # "updateCount":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->PREF_KEY_UPCOUNT_USER_FRONT:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->getUpdateCount(Ljava/lang/String;)I

    move-result v0

    .line 294
    .restart local v0    # "updateCount":I
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->PREF_KEY_UPCOUNT_USER_FRONT:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;->setUpdateCount(Ljava/lang/String;I)V

    goto :goto_0
.end method
