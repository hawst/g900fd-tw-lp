.class public final Lcom/sec/android/app/camaftest/core/multi/qrcode/QRCodeMultiReader;
.super Lcom/sec/android/app/camaftest/core/qrcode/QRCodeReader;
.source "QRCodeMultiReader.java"

# interfaces
.implements Lcom/sec/android/app/camaftest/core/multi/MultipleBarcodeReader;


# static fields
.field private static final EMPTY_RESULT_ARRAY:[Lcom/sec/android/app/camaftest/core/Result;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/sec/android/app/camaftest/core/Result;

    sput-object v0, Lcom/sec/android/app/camaftest/core/multi/qrcode/QRCodeMultiReader;->EMPTY_RESULT_ARRAY:[Lcom/sec/android/app/camaftest/core/Result;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/qrcode/QRCodeReader;-><init>()V

    return-void
.end method


# virtual methods
.method public decodeMultiple(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)[Lcom/sec/android/app/camaftest/core/Result;
    .locals 1
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/camaftest/core/multi/qrcode/QRCodeMultiReader;->decodeMultiple(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)[Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    return-object v0
.end method

.method public decodeMultiple(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)[Lcom/sec/android/app/camaftest/core/Result;
    .locals 15
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/camaftest/core/BinaryBitmap;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)[",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 54
    .local p2, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .local v11, "results":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/camaftest/core/Result;>;"
    new-instance v12, Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiDetector;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->getBlackMatrix()Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    move-result-object v13

    invoke-direct {v12, v13}, Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiDetector;-><init>(Lcom/sec/android/app/camaftest/core/common/BitMatrix;)V

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiDetector;->detectMulti(Ljava/util/Map;)[Lcom/sec/android/app/camaftest/core/common/DetectorResult;

    move-result-object v5

    .line 56
    .local v5, "detectorResults":[Lcom/sec/android/app/camaftest/core/common/DetectorResult;
    move-object v1, v5

    .local v1, "arr$":[Lcom/sec/android/app/camaftest/core/common/DetectorResult;
    array-length v8, v1

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_2

    aget-object v4, v1, v7

    .line 58
    .local v4, "detectorResult":Lcom/sec/android/app/camaftest/core/common/DetectorResult;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/core/multi/qrcode/QRCodeMultiReader;->getDecoder()Lcom/sec/android/app/camaftest/core/qrcode/decoder/Decoder;

    move-result-object v12

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/core/common/DetectorResult;->getBits()Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/app/camaftest/core/qrcode/decoder/Decoder;->decode(Lcom/sec/android/app/camaftest/core/common/BitMatrix;)Lcom/sec/android/app/camaftest/core/common/DecoderResult;

    move-result-object v3

    .line 59
    .local v3, "decoderResult":Lcom/sec/android/app/camaftest/core/common/DecoderResult;
    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/core/common/DetectorResult;->getPoints()[Lcom/sec/android/app/camaftest/core/ResultPoint;

    move-result-object v9

    .line 60
    .local v9, "points":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    new-instance v10, Lcom/sec/android/app/camaftest/core/Result;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/DecoderResult;->getText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/DecoderResult;->getRawBytes()[B

    move-result-object v13

    sget-object v14, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->QR_CODE:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-direct {v10, v12, v13, v9, v14}, Lcom/sec/android/app/camaftest/core/Result;-><init>(Ljava/lang/String;[B[Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/BarcodeFormat;)V

    .line 62
    .local v10, "result":Lcom/sec/android/app/camaftest/core/Result;
    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/DecoderResult;->getByteSegments()Ljava/util/List;

    move-result-object v2

    .line 63
    .local v2, "byteSegments":Ljava/util/List;, "Ljava/util/List<[B>;"
    if-eqz v2, :cond_0

    .line 64
    sget-object v12, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->BYTE_SEGMENTS:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-virtual {v10, v12, v2}, Lcom/sec/android/app/camaftest/core/Result;->putMetadata(Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;)V

    .line 66
    :cond_0
    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/DecoderResult;->getECLevel()Ljava/lang/String;

    move-result-object v6

    .line 67
    .local v6, "ecLevel":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 68
    sget-object v12, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->ERROR_CORRECTION_LEVEL:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-virtual {v10, v12, v6}, Lcom/sec/android/app/camaftest/core/Result;->putMetadata(Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;)V

    .line 70
    :cond_1
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/sec/android/app/camaftest/core/ReaderException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    .end local v2    # "byteSegments":Ljava/util/List;, "Ljava/util/List<[B>;"
    .end local v3    # "decoderResult":Lcom/sec/android/app/camaftest/core/common/DecoderResult;
    .end local v6    # "ecLevel":Ljava/lang/String;
    .end local v9    # "points":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    .end local v10    # "result":Lcom/sec/android/app/camaftest/core/Result;
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 75
    .end local v4    # "detectorResult":Lcom/sec/android/app/camaftest/core/common/DetectorResult;
    :cond_2
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 76
    sget-object v12, Lcom/sec/android/app/camaftest/core/multi/qrcode/QRCodeMultiReader;->EMPTY_RESULT_ARRAY:[Lcom/sec/android/app/camaftest/core/Result;

    .line 78
    :goto_2
    return-object v12

    :cond_3
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v12

    new-array v12, v12, [Lcom/sec/android/app/camaftest/core/Result;

    invoke-interface {v11, v12}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Lcom/sec/android/app/camaftest/core/Result;

    goto :goto_2

    .line 71
    .restart local v4    # "detectorResult":Lcom/sec/android/app/camaftest/core/common/DetectorResult;
    :catch_0
    move-exception v12

    goto :goto_1
.end method
