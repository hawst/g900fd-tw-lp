.class Lcom/sec/android/app/camera/OISTest$6;
.super Landroid/os/Handler;
.source "OISTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/OISTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/OISTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/OISTest;)V
    .locals 0

    .prologue
    .line 878
    iput-object p1, p0, Lcom/sec/android/app/camera/OISTest$6;->this$0:Lcom/sec/android/app/camera/OISTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 880
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage: mTimerHandler -msg:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 893
    :goto_0
    return-void

    .line 883
    :sswitch_0
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage: KEY_TIMER_EXPIRED -mIsCaptureEnble:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/OISTest$6;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mIsPressedBackkey:Z
    invoke-static {v2}, Lcom/sec/android/app/camera/OISTest;->access$1800(Lcom/sec/android/app/camera/OISTest;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest$6;->this$0:Lcom/sec/android/app/camera/OISTest;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/camera/OISTest;->mIsPressedBackkey:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/OISTest;->access$1802(Lcom/sec/android/app/camera/OISTest;Z)Z

    goto :goto_0

    .line 889
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "mTimerHandler TIMER_CAMCORDER_STOP"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 881
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x1f -> :sswitch_1
    .end sparse-switch
.end method
