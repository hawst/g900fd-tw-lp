.class public final Lcom/sec/android/app/camera/OISTest$PreviewCallback;
.super Ljava/lang/Object;
.source "OISTest.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/OISTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "PreviewCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/OISTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camera/OISTest;)V
    .locals 0

    .prologue
    .line 744
    iput-object p1, p0, Lcom/sec/android/app/camera/OISTest$PreviewCallback;->this$0:Lcom/sec/android/app/camera/OISTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    const/4 v2, 0x0

    .line 746
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "onPreviewFrame - get the preview image"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest$PreviewCallback;->this$0:Lcom/sec/android/app/camera/OISTest;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/camera/OISTest;->isPreviewStarted:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/OISTest;->access$1402(Lcom/sec/android/app/camera/OISTest;Z)Z

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest$PreviewCallback;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mPassButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/camera/OISTest;->access$1500(Lcom/sec/android/app/camera/OISTest;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 750
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest$PreviewCallback;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mFailButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/camera/OISTest;->access$1600(Lcom/sec/android/app/camera/OISTest;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 754
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 755
    return-void
.end method
