.class public final Lcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;
.super Ljava/lang/Object;
.source "FinderPattern.java"


# instance fields
.field private final resultPoints:[Lcom/sec/android/app/camaftest/core/ResultPoint;

.field private final startEnd:[I

.field private final value:I


# direct methods
.method public constructor <init>(I[IIII)V
    .locals 5
    .param p1, "value"    # I
    .param p2, "startEnd"    # [I
    .param p3, "start"    # I
    .param p4, "end"    # I
    .param p5, "rowNumber"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput p1, p0, Lcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;->value:I

    .line 29
    iput-object p2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;->startEnd:[I

    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/camaftest/core/ResultPoint;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/app/camaftest/core/ResultPoint;

    int-to-float v3, p3

    int-to-float v4, p5

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/camaftest/core/ResultPoint;-><init>(FF)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/app/camaftest/core/ResultPoint;

    int-to-float v3, p4

    int-to-float v4, p5

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/camaftest/core/ResultPoint;-><init>(FF)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;->resultPoints:[Lcom/sec/android/app/camaftest/core/ResultPoint;

    .line 31
    return-void
.end method


# virtual methods
.method public getResultPoints()[Lcom/sec/android/app/camaftest/core/ResultPoint;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;->resultPoints:[Lcom/sec/android/app/camaftest/core/ResultPoint;

    return-object v0
.end method

.method public getStartEnd()[I
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;->startEnd:[I

    return-object v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/FinderPattern;->value:I

    return v0
.end method
