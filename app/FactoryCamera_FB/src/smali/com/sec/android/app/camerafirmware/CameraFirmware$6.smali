.class Lcom/sec/android/app/camerafirmware/CameraFirmware$6;
.super Ljava/lang/Object;
.source "CameraFirmware.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/camerafirmware/CameraFirmware;->DialogPopup(I)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camerafirmware/CameraFirmware;)V
    .locals 0

    .prologue
    .line 1191
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$6;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog2"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v1, 0x0

    .line 1193
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$6;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mForceUpdate:Z
    invoke-static {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$900(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1194
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$6;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mForceUpdate:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$902(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z

    .line 1197
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$6;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdate:Z
    invoke-static {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$300(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1198
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$6;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdate:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$302(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z

    .line 1201
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$6;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdateCountCheck:Z
    invoke-static {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$400(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1202
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$6;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdateCountCheck:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$402(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z

    .line 1205
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$6;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompForceUpdate:Z
    invoke-static {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$1000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1206
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$6;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompForceUpdate:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$1002(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z

    .line 1208
    :cond_3
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 1209
    return-void
.end method
