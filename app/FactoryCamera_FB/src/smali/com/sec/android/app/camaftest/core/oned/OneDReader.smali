.class public abstract Lcom/sec/android/app/camaftest/core/oned/OneDReader;
.super Ljava/lang/Object;
.source "OneDReader.java"

# interfaces
.implements Lcom/sec/android/app/camaftest/core/Reader;


# static fields
.field protected static final INTEGER_MATH_SHIFT:I = 0x8

.field protected static final PATTERN_MATCH_RESULT_SCALE_FACTOR:I = 0x100


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private doDecode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 22
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/camaftest/core/BinaryBitmap;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 112
    .local p2, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->getWidth()I

    move-result v16

    .line 113
    .local v16, "width":I
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->getHeight()I

    move-result v3

    .line 114
    .local v3, "height":I
    new-instance v11, Lcom/sec/android/app/camaftest/core/common/BitArray;

    move/from16 v0, v16

    invoke-direct {v11, v0}, Lcom/sec/android/app/camaftest/core/common/BitArray;-><init>(I)V

    .line 116
    .local v11, "row":Lcom/sec/android/app/camaftest/core/common/BitArray;
    shr-int/lit8 v6, v3, 0x1

    .line 117
    .local v6, "middle":I
    if-eqz p2, :cond_1

    sget-object v18, Lcom/sec/android/app/camaftest/core/DecodeHintType;->TRY_HARDER:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    const/4 v15, 0x1

    .line 118
    .local v15, "tryHarder":Z
    :goto_0
    const/16 v19, 0x1

    if-eqz v15, :cond_2

    const/16 v18, 0x8

    :goto_1
    shr-int v18, v3, v18

    move/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 120
    .local v13, "rowStep":I
    if-eqz v15, :cond_3

    .line 121
    move v5, v3

    .line 127
    .local v5, "maxLines":I
    :goto_2
    const/16 v17, 0x0

    .local v17, "x":I
    :goto_3
    move/from16 v0, v17

    if-ge v0, v5, :cond_0

    .line 131
    add-int/lit8 v18, v17, 0x1

    shr-int/lit8 v14, v18, 0x1

    .line 132
    .local v14, "rowStepsAboveOrBelow":I
    and-int/lit8 v18, v17, 0x1

    if-nez v18, :cond_4

    const/4 v4, 0x1

    .line 133
    .local v4, "isAbove":Z
    :goto_4
    if-eqz v4, :cond_5

    .end local v14    # "rowStepsAboveOrBelow":I
    :goto_5
    mul-int v18, v13, v14

    add-int v12, v6, v18

    .line 134
    .local v12, "rowNumber":I
    if-ltz v12, :cond_0

    if-lt v12, v3, :cond_6

    .line 187
    .end local v4    # "isAbove":Z
    .end local v12    # "rowNumber":I
    :cond_0
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v18

    throw v18

    .line 117
    .end local v5    # "maxLines":I
    .end local v13    # "rowStep":I
    .end local v15    # "tryHarder":Z
    .end local v17    # "x":I
    :cond_1
    const/4 v15, 0x0

    goto :goto_0

    .line 118
    .restart local v15    # "tryHarder":Z
    :cond_2
    const/16 v18, 0x5

    goto :goto_1

    .line 123
    .restart local v13    # "rowStep":I
    :cond_3
    const/16 v5, 0xf

    .restart local v5    # "maxLines":I
    goto :goto_2

    .line 132
    .restart local v14    # "rowStepsAboveOrBelow":I
    .restart local v17    # "x":I
    :cond_4
    const/4 v4, 0x0

    goto :goto_4

    .line 133
    .restart local v4    # "isAbove":Z
    :cond_5
    neg-int v14, v14

    goto :goto_5

    .line 141
    .end local v14    # "rowStepsAboveOrBelow":I
    .restart local v12    # "rowNumber":I
    :cond_6
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->getBlackRow(ILcom/sec/android/app/camaftest/core/common/BitArray;)Lcom/sec/android/app/camaftest/core/common/BitArray;
    :try_end_0
    .catch Lcom/sec/android/app/camaftest/core/NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 149
    const/4 v2, 0x0

    .local v2, "attempt":I
    :goto_6
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ge v2, v0, :cond_9

    .line 150
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v2, v0, :cond_7

    .line 151
    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/common/BitArray;->reverse()V

    .line 159
    if-eqz p2, :cond_7

    sget-object v18, Lcom/sec/android/app/camaftest/core/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 160
    new-instance v7, Ljava/util/EnumMap;

    const-class v18, Lcom/sec/android/app/camaftest/core/DecodeHintType;

    move-object/from16 v0, v18

    invoke-direct {v7, v0}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 161
    .local v7, "newHints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;Ljava/lang/Object;>;"
    move-object/from16 v0, p2

    invoke-interface {v7, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 162
    sget-object v18, Lcom/sec/android/app/camaftest/core/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    move-object/from16 v0, v18

    invoke-interface {v7, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    move-object/from16 p2, v7

    .line 168
    .end local v7    # "newHints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;Ljava/lang/Object;>;"
    :cond_7
    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v12, v11, v1}, Lcom/sec/android/app/camaftest/core/oned/OneDReader;->decodeRow(ILcom/sec/android/app/camaftest/core/common/BitArray;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v10

    .line 170
    .local v10, "result":Lcom/sec/android/app/camaftest/core/Result;
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v2, v0, :cond_8

    .line 172
    sget-object v18, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->ORIENTATION:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    const/16 v19, 0xb4

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/app/camaftest/core/Result;->putMetadata(Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;)V

    .line 174
    invoke-virtual {v10}, Lcom/sec/android/app/camaftest/core/Result;->getResultPoints()[Lcom/sec/android/app/camaftest/core/ResultPoint;

    move-result-object v9

    .line 175
    .local v9, "points":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    if-eqz v9, :cond_8

    .line 176
    const/16 v18, 0x0

    new-instance v19, Lcom/sec/android/app/camaftest/core/ResultPoint;

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v20, v0

    const/16 v21, 0x0

    aget-object v21, v9, v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/camaftest/core/ResultPoint;->getX()F

    move-result v21

    sub-float v20, v20, v21

    const/high16 v21, 0x3f800000    # 1.0f

    sub-float v20, v20, v21

    const/16 v21, 0x0

    aget-object v21, v9, v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/camaftest/core/ResultPoint;->getY()F

    move-result v21

    invoke-direct/range {v19 .. v21}, Lcom/sec/android/app/camaftest/core/ResultPoint;-><init>(FF)V

    aput-object v19, v9, v18

    .line 177
    const/16 v18, 0x1

    new-instance v19, Lcom/sec/android/app/camaftest/core/ResultPoint;

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v20, v0

    const/16 v21, 0x1

    aget-object v21, v9, v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/camaftest/core/ResultPoint;->getX()F

    move-result v21

    sub-float v20, v20, v21

    const/high16 v21, 0x3f800000    # 1.0f

    sub-float v20, v20, v21

    const/16 v21, 0x1

    aget-object v21, v9, v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/camaftest/core/ResultPoint;->getY()F

    move-result v21

    invoke-direct/range {v19 .. v21}, Lcom/sec/android/app/camaftest/core/ResultPoint;-><init>(FF)V

    aput-object v19, v9, v18
    :try_end_1
    .catch Lcom/sec/android/app/camaftest/core/ReaderException; {:try_start_1 .. :try_end_1} :catch_1

    .line 180
    .end local v9    # "points":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    :cond_8
    return-object v10

    .line 142
    .end local v2    # "attempt":I
    .end local v10    # "result":Lcom/sec/android/app/camaftest/core/Result;
    :catch_0
    move-exception v8

    .line 127
    :cond_9
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_3

    .line 181
    .restart local v2    # "attempt":I
    :catch_1
    move-exception v18

    .line 149
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_6
.end method

.method protected static patternMatchVariance([I[II)I
    .locals 12
    .param p0, "counters"    # [I
    .param p1, "pattern"    # [I
    .param p2, "maxIndividualVariance"    # I

    .prologue
    const v10, 0x7fffffff

    .line 277
    array-length v2, p0

    .line 278
    .local v2, "numCounters":I
    const/4 v5, 0x0

    .line 279
    .local v5, "total":I
    const/4 v3, 0x0

    .line 280
    .local v3, "patternLength":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 281
    aget v11, p0, v1

    add-int/2addr v5, v11

    .line 282
    aget v11, p1, v1

    add-int/2addr v3, v11

    .line 280
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 284
    :cond_0
    if-ge v5, v3, :cond_2

    .line 308
    :cond_1
    :goto_1
    return v10

    .line 295
    :cond_2
    shl-int/lit8 v11, v5, 0x8

    div-int v7, v11, v3

    .line 296
    .local v7, "unitBarWidth":I
    mul-int v11, p2, v7

    shr-int/lit8 p2, v11, 0x8

    .line 298
    const/4 v6, 0x0

    .line 299
    .local v6, "totalVariance":I
    const/4 v9, 0x0

    .local v9, "x":I
    :goto_2
    if-ge v9, v2, :cond_4

    .line 300
    aget v11, p0, v9

    shl-int/lit8 v0, v11, 0x8

    .line 301
    .local v0, "counter":I
    aget v11, p1, v9

    mul-int v4, v11, v7

    .line 302
    .local v4, "scaledPattern":I
    if-le v0, v4, :cond_3

    sub-int v8, v0, v4

    .line 303
    .local v8, "variance":I
    :goto_3
    if-gt v8, p2, :cond_1

    .line 306
    add-int/2addr v6, v8

    .line 299
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 302
    .end local v8    # "variance":I
    :cond_3
    sub-int v8, v4, v0

    goto :goto_3

    .line 308
    .end local v0    # "counter":I
    .end local v4    # "scaledPattern":I
    :cond_4
    div-int v10, v6, v5

    goto :goto_1
.end method

.method protected static recordPattern(Lcom/sec/android/app/camaftest/core/common/BitArray;I[I)V
    .locals 8
    .param p0, "row"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .param p1, "start"    # I
    .param p2, "counters"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 210
    array-length v4, p2

    .line 211
    .local v4, "numCounters":I
    invoke-static {p2, v6, v4, v6}, Ljava/util/Arrays;->fill([IIII)V

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v1

    .line 213
    .local v1, "end":I
    if-lt p1, v1, :cond_0

    .line 214
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v5

    throw v5

    .line 216
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v7

    if-nez v7, :cond_1

    move v3, v5

    .line 217
    .local v3, "isWhite":Z
    :goto_0
    const/4 v0, 0x0

    .line 218
    .local v0, "counterPosition":I
    move v2, p1

    .line 219
    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_3

    .line 220
    invoke-virtual {p0, v2}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v7

    xor-int/2addr v7, v3

    if-eqz v7, :cond_2

    .line 221
    aget v7, p2, v0

    add-int/lit8 v7, v7, 0x1

    aput v7, p2, v0

    .line 231
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v0    # "counterPosition":I
    .end local v2    # "i":I
    .end local v3    # "isWhite":Z
    :cond_1
    move v3, v6

    .line 216
    goto :goto_0

    .line 223
    .restart local v0    # "counterPosition":I
    .restart local v2    # "i":I
    .restart local v3    # "isWhite":Z
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 224
    if-ne v0, v4, :cond_5

    .line 237
    :cond_3
    if-eq v0, v4, :cond_7

    add-int/lit8 v5, v4, -0x1

    if-ne v0, v5, :cond_4

    if-eq v2, v1, :cond_7

    .line 238
    :cond_4
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v5

    throw v5

    .line 227
    :cond_5
    aput v5, p2, v0

    .line 228
    if-nez v3, :cond_6

    move v3, v5

    :goto_3
    goto :goto_2

    :cond_6
    move v3, v6

    goto :goto_3

    .line 240
    :cond_7
    return-void
.end method

.method protected static recordPatternInReverse(Lcom/sec/android/app/camaftest/core/common/BitArray;I[I)V
    .locals 3
    .param p0, "row"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .param p1, "start"    # I
    .param p2, "counters"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 244
    array-length v1, p2

    .line 245
    .local v1, "numTransitionsLeft":I
    invoke-virtual {p0, p1}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v0

    .line 246
    .local v0, "last":Z
    :cond_0
    :goto_0
    if-lez p1, :cond_2

    if-ltz v1, :cond_2

    .line 247
    add-int/lit8 p1, p1, -0x1

    invoke-virtual {p0, p1}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v2

    if-eq v2, v0, :cond_0

    .line 248
    add-int/lit8 v1, v1, -0x1

    .line 249
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 252
    :cond_2
    if-ltz v1, :cond_3

    .line 253
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v2

    throw v2

    .line 255
    :cond_3
    add-int/lit8 v2, p1, 0x1

    invoke-static {p0, v2, p2}, Lcom/sec/android/app/camaftest/core/oned/OneDReader;->recordPattern(Lcom/sec/android/app/camaftest/core/common/BitArray;I[I)V

    .line 256
    return-void
.end method


# virtual methods
.method public decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 1
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/camaftest/core/oned/OneDReader;->decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    return-object v0
.end method

.method public decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 12
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/camaftest/core/BinaryBitmap;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    .line 57
    .local p2, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/camaftest/core/oned/OneDReader;->doDecode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    :try_end_0
    .catch Lcom/sec/android/app/camaftest/core/NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 81
    :cond_0
    return-object v6

    .line 58
    :catch_0
    move-exception v3

    .line 59
    .local v3, "nfe":Lcom/sec/android/app/camaftest/core/NotFoundException;
    if-eqz p2, :cond_2

    sget-object v9, Lcom/sec/android/app/camaftest/core/DecodeHintType;->TRY_HARDER:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    invoke-interface {p2, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v8, 0x1

    .line 60
    .local v8, "tryHarder":Z
    :goto_0
    if-eqz v8, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->isRotateSupported()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 61
    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->rotateCounterClockwise()Lcom/sec/android/app/camaftest/core/BinaryBitmap;

    move-result-object v7

    .line 62
    .local v7, "rotatedImage":Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    invoke-direct {p0, v7, p2}, Lcom/sec/android/app/camaftest/core/oned/OneDReader;->doDecode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v6

    .line 65
    .local v6, "result":Lcom/sec/android/app/camaftest/core/Result;
    invoke-virtual {v6}, Lcom/sec/android/app/camaftest/core/Result;->getResultMetadata()Ljava/util/Map;

    move-result-object v2

    .line 66
    .local v2, "metadata":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/ResultMetadataType;*>;"
    const/16 v4, 0x10e

    .line 67
    .local v4, "orientation":I
    if-eqz v2, :cond_1

    sget-object v9, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->ORIENTATION:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-interface {v2, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 70
    sget-object v9, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->ORIENTATION:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-interface {v2, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    add-int/2addr v9, v4

    rem-int/lit16 v4, v9, 0x168

    .line 72
    :cond_1
    sget-object v9, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->ORIENTATION:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Lcom/sec/android/app/camaftest/core/Result;->putMetadata(Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;)V

    .line 74
    invoke-virtual {v6}, Lcom/sec/android/app/camaftest/core/Result;->getResultPoints()[Lcom/sec/android/app/camaftest/core/ResultPoint;

    move-result-object v5

    .line 75
    .local v5, "points":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    if-eqz v5, :cond_0

    .line 76
    invoke-virtual {v7}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->getHeight()I

    move-result v0

    .line 77
    .local v0, "height":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v9, v5

    if-ge v1, v9, :cond_0

    .line 78
    new-instance v9, Lcom/sec/android/app/camaftest/core/ResultPoint;

    int-to-float v10, v0

    aget-object v11, v5, v1

    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/ResultPoint;->getY()F

    move-result v11

    sub-float/2addr v10, v11

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v10, v11

    aget-object v11, v5, v1

    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/ResultPoint;->getX()F

    move-result v11

    invoke-direct {v9, v10, v11}, Lcom/sec/android/app/camaftest/core/ResultPoint;-><init>(FF)V

    aput-object v9, v5, v1

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 59
    .end local v0    # "height":I
    .end local v1    # "i":I
    .end local v2    # "metadata":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/ResultMetadataType;*>;"
    .end local v4    # "orientation":I
    .end local v5    # "points":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    .end local v6    # "result":Lcom/sec/android/app/camaftest/core/Result;
    .end local v7    # "rotatedImage":Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .end local v8    # "tryHarder":Z
    :cond_2
    const/4 v8, 0x0

    goto :goto_0

    .line 83
    .restart local v8    # "tryHarder":Z
    :cond_3
    throw v3
.end method

.method public abstract decodeRow(ILcom/sec/android/app/camaftest/core/common/BitArray;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/app/camaftest/core/common/BitArray;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/ChecksumException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method
