.class public final Lcom/sec/android/app/camaftest/core/Result;
.super Ljava/lang/Object;
.source "Result.java"


# instance fields
.field private final format:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

.field private final rawBytes:[B

.field private resultMetadata:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/ResultMetadataType;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private resultPoints:[Lcom/sec/android/app/camaftest/core/ResultPoint;

.field private final text:Ljava/lang/String;

.field private final timestamp:J


# direct methods
.method public constructor <init>(Ljava/lang/String;[B[Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/BarcodeFormat;)V
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "rawBytes"    # [B
    .param p3, "resultPoints"    # [Lcom/sec/android/app/camaftest/core/ResultPoint;
    .param p4, "format"    # Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    .prologue
    .line 40
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/app/camaftest/core/Result;-><init>(Ljava/lang/String;[B[Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/BarcodeFormat;J)V

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B[Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/BarcodeFormat;J)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "rawBytes"    # [B
    .param p3, "resultPoints"    # [Lcom/sec/android/app/camaftest/core/ResultPoint;
    .param p4, "format"    # Lcom/sec/android/app/camaftest/core/BarcodeFormat;
    .param p5, "timestamp"    # J

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/camaftest/core/Result;->text:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lcom/sec/android/app/camaftest/core/Result;->rawBytes:[B

    .line 50
    iput-object p3, p0, Lcom/sec/android/app/camaftest/core/Result;->resultPoints:[Lcom/sec/android/app/camaftest/core/ResultPoint;

    .line 51
    iput-object p4, p0, Lcom/sec/android/app/camaftest/core/Result;->format:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->resultMetadata:Ljava/util/Map;

    .line 53
    iput-wide p5, p0, Lcom/sec/android/app/camaftest/core/Result;->timestamp:J

    .line 54
    return-void
.end method


# virtual methods
.method public addResultPoints([Lcom/sec/android/app/camaftest/core/ResultPoint;)V
    .locals 5
    .param p1, "newPoints"    # [Lcom/sec/android/app/camaftest/core/ResultPoint;

    .prologue
    const/4 v4, 0x0

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/camaftest/core/Result;->resultPoints:[Lcom/sec/android/app/camaftest/core/ResultPoint;

    .line 114
    .local v1, "oldPoints":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    if-nez v1, :cond_1

    .line 115
    iput-object p1, p0, Lcom/sec/android/app/camaftest/core/Result;->resultPoints:[Lcom/sec/android/app/camaftest/core/ResultPoint;

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    if-eqz p1, :cond_0

    array-length v2, p1

    if-lez v2, :cond_0

    .line 117
    array-length v2, v1

    array-length v3, p1

    add-int/2addr v2, v3

    new-array v0, v2, [Lcom/sec/android/app/camaftest/core/ResultPoint;

    .line 118
    .local v0, "allPoints":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    array-length v2, v1

    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 119
    array-length v2, v1

    array-length v3, p1

    invoke-static {p1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 120
    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->resultPoints:[Lcom/sec/android/app/camaftest/core/ResultPoint;

    goto :goto_0
.end method

.method public getBarcodeFormat()Lcom/sec/android/app/camaftest/core/BarcodeFormat;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->format:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    return-object v0
.end method

.method public getRawBytes()[B
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->rawBytes:[B

    return-object v0
.end method

.method public getResultMetadata()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/ResultMetadataType;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->resultMetadata:Ljava/util/Map;

    return-object v0
.end method

.method public getResultPoints()[Lcom/sec/android/app/camaftest/core/ResultPoint;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->resultPoints:[Lcom/sec/android/app/camaftest/core/ResultPoint;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 125
    iget-wide v0, p0, Lcom/sec/android/app/camaftest/core/Result;->timestamp:J

    return-wide v0
.end method

.method public putAllMetadata(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/ResultMetadataType;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p1, "metadata":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->resultMetadata:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 105
    iput-object p1, p0, Lcom/sec/android/app/camaftest/core/Result;->resultMetadata:Ljava/util/Map;

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->resultMetadata:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public putMetadata(Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;)V
    .locals 2
    .param p1, "type"    # Lcom/sec/android/app/camaftest/core/ResultMetadataType;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->resultMetadata:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 97
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->resultMetadata:Ljava/util/Map;

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->resultMetadata:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Result;->text:Ljava/lang/String;

    return-object v0
.end method
