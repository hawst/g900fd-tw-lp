.class public Lcom/sec/android/app/camera/CamcorderPreviewTest;
.super Landroid/app/Activity;
.source "CamcorderPreviewTest.java"

# interfaces
.implements Landroid/media/MediaRecorder$OnErrorListener;
.implements Landroid/media/MediaRecorder$OnInfoListener;
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/sec/android/app/camera/framework/ShutterButton$OnShutterButtonListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camera/CamcorderPreviewTest$PreviewCallback;,
        Lcom/sec/android/app/camera/CamcorderPreviewTest$MainHandler;,
        Lcom/sec/android/app/camera/CamcorderPreviewTest$ShutterCallback;,
        Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;
    }
.end annotation


# static fields
.field protected static final CAMERA_FIRMWARE_CHECK_FILE_REAR:Ljava/lang/String; = "/sys/class/camera/rear/rear_checkfw_factory"

.field protected static final CAMERA_FIRMWARE_INFO_FILE_REAR:Ljava/lang/String; = "/sys/class/camera/rear/rear_camfw"

.field protected static final CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

.field private static final DIRECTORY_CAMERA_AUTO_TEST:Ljava/lang/String;

.field public static final KEY_POWER:Ljava/lang/String; = "Hold"

.field public static final KEY_VOLUME_UP:Ljava/lang/String; = "Volume Up"

.field private static final MAX_VIDEO_FILE_SIZE:J = 0xffffffffL

.field private static final STATE_RECORD_INIT:I = -0x1

.field private static final STATE_RECORD_RECORDING:I = 0x3

.field private static final STATE_RECORD_START:I = 0x4

.field private static final STATE_RECORD_STOP:I = 0x1

.field private static final STATE_RECORD_STOPED:I

.field public static TAG:Ljava/lang/String;


# instance fields
.field public Feature:Lcom/sec/android/app/camera/Feature;

.field private final REQUEST_CAMCORDER_PREVIEW_TEST:I

.field private bIsUsbOrUartCommand:Z

.field private bSentAck:Z

.field private camcorderpreview:[[I

.field private cameraPicSize:Ljava/lang/String;

.field private cameraType:I

.field private isPreviewStarted:Z

.field private mCamcorderStop:Z

.field private mCameraDevice:Landroid/hardware/Camera;

.field private mCameraSettings:Lcom/sec/android/app/camera/framework/CameraSettings;

.field private mCameraVideoFileDescriptor:Ljava/io/FileDescriptor;

.field private mCameraVideoFilename:Ljava/lang/String;

.field private mCurResolution:I

.field private mCurrentTime:J

.field private mCurrentVideoFilename:Ljava/lang/String;

.field private mCurrentVideoValues:Landroid/content/ContentValues;

.field private mCurrentVolumeDownKeyFirshTime:J

.field private mDVFSHelper:Landroid/os/DVFSHelper;

.field private mErrorCallback:Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;

.field private mErrorPopup:Landroid/app/AlertDialog;

.field private mIsPressedBackkey:Z

.field private mKeyFailToast:Landroid/widget/Toast;

.field private mMainHandler:Landroid/os/Handler;

.field private mMediaRecorder:Landroid/media/MediaRecorder;

.field private mMediaRecorderRecording:Z

.field private mMovieIndexCount:I

.field private mParameters:Landroid/hardware/Camera$Parameters;

.field private mPausing:Z

.field private mPrepareRecordingThread:Ljava/lang/Thread;

.field private mPreviewCallback:Lcom/sec/android/app/camera/CamcorderPreviewTest$PreviewCallback;

.field private mPreviewing:Z

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRecordingState:I

.field private mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

.field private mShutterCallback:Lcom/sec/android/app/camera/CamcorderPreviewTest$ShutterCallback;

.field mStartCheck:Ljava/lang/Runnable;

.field private mStartRecordingThread:Ljava/lang/Thread;

.field private mStopButton:Lcom/sec/android/app/camera/framework/ShutterButton;

.field private mStopCamera:Z

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceView:Lcom/sec/android/app/camera/VideoPreview;

.field protected mTimerHandler:Landroid/os/Handler;

.field private mVideoRecordingTimeInMiliSecond:J

.field protected mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mchkopencamera:Z

.field private supportedCPUCoreTable:[I

.field private supportedCPUFreqTable:[I

.field private testType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 75
    const-string v0, "CamcorderPreviewTest"

    sput-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/DCIM/Camera"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/fImage"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->DIRECTORY_CAMERA_AUTO_TEST:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v3, -0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 77
    iput-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    .line 80
    iput-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    .line 82
    iput v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraType:I

    .line 83
    const-string v0, "3264x1836"

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraPicSize:Ljava/lang/String;

    .line 84
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->REQUEST_CAMCORDER_PREVIEW_TEST:I

    .line 88
    iput v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    .line 91
    new-instance v0, Lcom/sec/android/app/camera/CamcorderPreviewTest$MainHandler;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest$MainHandler;-><init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;Lcom/sec/android/app/camera/CamcorderPreviewTest$1;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;

    .line 94
    iput-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    .line 97
    iput-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mIsPressedBackkey:Z

    .line 98
    iput-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z

    .line 99
    iput-wide v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentTime:J

    .line 102
    iput-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mchkopencamera:Z

    .line 104
    new-instance v0, Lcom/sec/android/app/camera/CamcorderPreviewTest$ShutterCallback;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest$ShutterCallback;-><init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;Lcom/sec/android/app/camera/CamcorderPreviewTest$1;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mShutterCallback:Lcom/sec/android/app/camera/CamcorderPreviewTest$ShutterCallback;

    .line 108
    iput-wide v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mVideoRecordingTimeInMiliSecond:J

    .line 116
    iput-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPrepareRecordingThread:Ljava/lang/Thread;

    .line 117
    iput-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStartRecordingThread:Ljava/lang/Thread;

    .line 118
    iput-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorderRecording:Z

    .line 123
    iput-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPausing:Z

    .line 124
    iput-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bSentAck:Z

    .line 125
    iput-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopCamera:Z

    .line 126
    iput-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPreviewing:Z

    .line 127
    iput-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isPreviewStarted:Z

    .line 135
    iput v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    .line 137
    iput-wide v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVolumeDownKeyFirshTime:J

    .line 140
    new-instance v0, Lcom/sec/android/app/camera/CamcorderPreviewTest$PreviewCallback;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest$PreviewCallback;-><init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPreviewCallback:Lcom/sec/android/app/camera/CamcorderPreviewTest$PreviewCallback;

    .line 142
    new-instance v0, Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;-><init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorCallback:Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;

    .line 144
    iput-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 148
    iput-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mKeyFailToast:Landroid/widget/Toast;

    .line 157
    iput v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMovieIndexCount:I

    .line 158
    iput-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    .line 1039
    new-instance v0, Lcom/sec/android/app/camera/framework/CameraSettings;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/framework/CameraSettings;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraSettings:Lcom/sec/android/app/camera/framework/CameraSettings;

    .line 1300
    new-instance v0, Lcom/sec/android/app/camera/CamcorderPreviewTest$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest$4;-><init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStartCheck:Ljava/lang/Runnable;

    .line 1372
    new-instance v0, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest$6;-><init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1487
    new-instance v0, Lcom/sec/android/app/camera/CamcorderPreviewTest$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest$7;-><init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mTimerHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/camera/CamcorderPreviewTest;Landroid/hardware/Camera;)Landroid/hardware/Camera;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # Landroid/hardware/Camera;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPreviewing:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mchkopencamera:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera$Parameters;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/camera/CamcorderPreviewTest;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # Landroid/hardware/Camera$Parameters;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/camera/CamcorderPreviewTest;)[[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraPicSize:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorCallback:Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/camera/CamcorderPreviewTest;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadCastAck(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1802(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isPreviewStarted:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bSentAck:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/camera/CamcorderPreviewTest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # I

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V

    return-void
.end method

.method static synthetic access$2002(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopCamera:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/camera/CamcorderPreviewTest;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$2302(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mIsPressedBackkey:Z

    return v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mIsPressedBackkey:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/camera/CamcorderPreviewTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraType:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->initializeMediaRecorder()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/media/MediaRecorder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/camera/CamcorderPreviewTest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    return p1
.end method

.method static synthetic access$802(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorderRecording:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/CamcorderPreviewTest;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->releaseMediaRecorder()V

    return-void
.end method

.method private cleanupEmptyFile()V
    .locals 6

    .prologue
    .line 828
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFilename:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 829
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFilename:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 830
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 833
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFilename:Ljava/lang/String;

    .line 836
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private cleanupTempFile()V
    .locals 2

    .prologue
    .line 816
    new-instance v0, Ljava/io/File;

    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getTempFileName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 817
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 820
    :cond_0
    return-void
.end method

.method private closeCamera()V
    .locals 2

    .prologue
    .line 1279
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "closeCamera"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1280
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 1281
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 1282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    .line 1284
    :cond_0
    return-void
.end method

.method private createVideoPath()V
    .locals 25

    .prologue
    .line 735
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 739
    .local v8, "dateTaken":J
    const-string v6, ""

    .line 740
    .local v6, "cameraDirPath":Ljava/lang/String;
    const-string v16, ""

    .line 741
    .local v16, "filepart":Ljava/lang/String;
    const-string v13, ""

    .line 742
    .local v13, "filename":Ljava/lang/String;
    const-string v18, ""

    .line 743
    .local v18, "title":Ljava/lang/String;
    const-string v10, ""

    .line 745
    .local v10, "displayName":Ljava/lang/String;
    sget-object v20, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "createVideoPath() : testType : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    const-string v20, "autotest"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 747
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->DIRECTORY_CAMERA_AUTO_TEST:Ljava/lang/String;

    .line 752
    :goto_0
    const-string v11, ".mp4"

    .line 753
    .local v11, "extension":Ljava/lang/String;
    const-string v17, "video/mp4"

    .line 755
    .local v17, "mimeType":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 756
    .local v5, "cameraDir":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 757
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 759
    .local v7, "date":Ljava/util/Date;
    sget-object v20, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "createVideoPath() : testType : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    const-string v20, "autotest"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 761
    const-string v4, ""

    .line 763
    .local v4, "TempFileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraType:I

    move/from16 v20, v0

    if-nez v20, :cond_1

    .line 764
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "REARMOVIE"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "%02d"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMovieIndexCount:I

    move/from16 v24, v0

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "_"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 769
    :goto_1
    const-string v20, "yyyyMMddkkmmss"

    move-object/from16 v0, v20

    invoke-static {v0, v7}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v16

    .line 770
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 771
    move-object/from16 v18, v16

    .line 772
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 780
    .end local v4    # "TempFileName":Ljava/lang/String;
    :goto_2
    sget-object v20, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "createVideoPath filename : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 784
    .local v12, "f":Ljava/io/File;
    const/4 v14, 0x0

    .line 785
    .local v14, "filenumber":I
    :goto_3
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 786
    sget-object v20, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Duplicated file name found: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    invoke-virtual {v7, v8, v9}, Ljava/util/Date;->setTime(J)V

    .line 788
    const-string v20, "yyyyMMdd_kkmmss"

    move-object/from16 v0, v20

    invoke-static {v0, v7}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v16

    .line 789
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    add-int/lit8 v15, v14, 0x1

    .end local v14    # "filenumber":I
    .local v15, "filenumber":I
    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 790
    move-object/from16 v18, v16

    .line 791
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 793
    new-instance v12, Ljava/io/File;

    .end local v12    # "f":Ljava/io/File;
    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v12    # "f":Ljava/io/File;
    move v14, v15

    .end local v15    # "filenumber":I
    .restart local v14    # "filenumber":I
    goto/16 :goto_3

    .line 749
    .end local v5    # "cameraDir":Ljava/io/File;
    .end local v7    # "date":Ljava/util/Date;
    .end local v11    # "extension":Ljava/lang/String;
    .end local v12    # "f":Ljava/io/File;
    .end local v14    # "filenumber":I
    .end local v17    # "mimeType":Ljava/lang/String;
    :cond_0
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    goto/16 :goto_0

    .line 766
    .restart local v4    # "TempFileName":Ljava/lang/String;
    .restart local v5    # "cameraDir":Ljava/io/File;
    .restart local v7    # "date":Ljava/util/Date;
    .restart local v11    # "extension":Ljava/lang/String;
    .restart local v17    # "mimeType":Ljava/lang/String;
    :cond_1
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "FRONTMOVIE"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "%02d"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMovieIndexCount:I

    move/from16 v24, v0

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "_"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 774
    .end local v4    # "TempFileName":Ljava/lang/String;
    :cond_2
    const-string v20, "yyyyMMdd_kkmmss"

    move-object/from16 v0, v20

    invoke-static {v0, v7}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v16

    .line 775
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 776
    move-object/from16 v18, v16

    .line 777
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_2

    .line 796
    .restart local v12    # "f":Ljava/io/File;
    .restart local v14    # "filenumber":I
    :cond_3
    new-instance v19, Landroid/content/ContentValues;

    const/16 v20, 0x7

    invoke-direct/range {v19 .. v20}, Landroid/content/ContentValues;-><init>(I)V

    .line 798
    .local v19, "values":Landroid/content/ContentValues;
    const-string v20, "title"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    const-string v20, "_display_name"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    const-string v20, "datetaken"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 801
    const-string v20, "mime_type"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 802
    const-string v20, "_data"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFilename:Ljava/lang/String;

    .line 806
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVideoValues:Landroid/content/ContentValues;

    .line 807
    return-void
.end method

.method private dialogErrorPopup(I)V
    .locals 5
    .param p1, "messageId"    # I

    .prologue
    const/4 v4, 0x0

    .line 1338
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "dialogErrorPopup"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1340
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1341
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 1342
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStartCheck:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1344
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogErrorPopup() : testType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1345
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogErrorPopup() : bIsUsbOrUartCommand : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1346
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    if-eqz v1, :cond_2

    .line 1347
    const-string v1, "com.android.samsungtest.RECORDING_START_ACK"

    const/4 v2, 0x2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadCastAck(Ljava/lang/String;I)V

    .line 1369
    :cond_1
    :goto_0
    iput-boolean v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mchkopencamera:Z

    .line 1370
    return-void

    .line 1349
    :cond_2
    const-string v1, "com.android.samsungtest.CAMERA_BAD"

    invoke-direct {p0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadCastAck(Ljava/lang/String;)V

    .line 1350
    iget-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mchkopencamera:Z

    if-nez v1, :cond_1

    .line 1351
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1352
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a000d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1354
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1355
    const v1, 0x7f0a0001

    new-instance v2, Lcom/sec/android/app/camera/CamcorderPreviewTest$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest$5;-><init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1363
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1364
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    .line 1365
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private dialogNGPopup(I)V
    .locals 5
    .param p1, "messageId"    # I

    .prologue
    const/4 v4, 0x0

    .line 2013
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "CamcorderPreviewTest.dialogNGPopup"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2015
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2016
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 2017
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStartCheck:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2019
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogNGPopup() : testType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2020
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogNGPopup() : bIsUsbOrUartCommand : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2021
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    if-eqz v1, :cond_2

    .line 2022
    const-string v1, "com.android.samsungtest.RECORDING_START_ACK"

    const/4 v2, 0x4

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadCastAck(Ljava/lang/String;I)V

    .line 2046
    :cond_1
    :goto_0
    iput-boolean v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mchkopencamera:Z

    .line 2047
    return-void

    .line 2024
    :cond_2
    const-string v1, "com.android.samsungtest.CAMERA_BAD"

    invoke-direct {p0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadCastAck(Ljava/lang/String;)V

    .line 2025
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CamcorderPreviewTest.dialogNGPopup mchkopencamera : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mchkopencamera:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2026
    iget-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mchkopencamera:Z

    if-nez v1, :cond_1

    .line 2027
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2028
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a000d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2029
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 2031
    const v1, 0x7f0a0001

    new-instance v2, Lcom/sec/android/app/camera/CamcorderPreviewTest$8;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest$8;-><init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2040
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2041
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    .line 2042
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private finishCamcorderPreviewTest(I)V
    .locals 5
    .param p1, "result"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1415
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "finishCamcorderPreviewTest()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1416
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1417
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "camera_id"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1418
    const-string v1, "ommision_test"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1419
    const-string v1, "torch_on"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1420
    const-string v1, "camcorder_preview_test"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1421
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->setResult(ILandroid/content/Intent;)V

    .line 1422
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->finish()V

    .line 1423
    return-void
.end method

.method private getTempFileName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 823
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->CAMERA_IMAGE_BUCKET_NAME_PHONE:Ljava/lang/String;

    .line 824
    .local v0, "cameraDirPath":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "temp_video"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private initializeMediaRecorder()V
    .locals 13

    .prologue
    const v12, 0xbb80

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x2

    .line 293
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "initializeRecorder"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    if-eqz v4, :cond_0

    .line 435
    :goto_0
    return-void

    .line 300
    :cond_0
    new-instance v4, Landroid/media/MediaRecorder;

    invoke-direct {v4}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    .line 301
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->unlock()V

    .line 302
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setCamera(Landroid/hardware/Camera;)V

    .line 304
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, p0}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    .line 305
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, p0}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    .line 307
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 309
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v10}, Landroid/media/MediaRecorder;->setVideoSource(I)V

    .line 310
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v8}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 311
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const/16 v5, 0x7d0

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setMaxDuration(I)V

    .line 316
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFileDescriptor:Ljava/io/FileDescriptor;

    if-eqz v4, :cond_4

    .line 317
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFileDescriptor:Ljava/io/FileDescriptor;

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/io/FileDescriptor;)V

    .line 322
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->createVideoPath()V

    .line 323
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getTempFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 329
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const/16 v5, 0x1e

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setVideoFrameRate(I)V

    .line 330
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v5, v5, v6

    aget v5, v5, v9

    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v7, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v6, v6, v7

    aget v6, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/media/MediaRecorder;->setVideoSize(II)V

    .line 333
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v4, v4, Lcom/sec/android/app/camera/Feature;->CAMERA_FIXED_MEDIARECORDER_PROFILE:Z

    if-eqz v4, :cond_5

    .line 334
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v5, v5, Lcom/sec/android/app/camera/Feature;->FIXED_VIDEO_ENCODING_BITRATE:I

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setVideoEncodingBitRate(I)V

    .line 335
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v5, v5, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_ENCODING_BITRATE:I

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    .line 336
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v5, v5, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_CHANNELS:I

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    .line 337
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v5, v5, Lcom/sec/android/app/camera/Feature;->FIXED_AUDIO_SAMPLINGRATE:I

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    .line 338
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v8}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    .line 339
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v11}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 384
    :cond_1
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v5}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setPreviewDisplay(Landroid/view/Surface;)V

    .line 389
    const-wide v2, 0xffffffffL

    .line 395
    .local v2, "maxFileSize":J
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const-wide v6, 0xffffffffL

    invoke-virtual {v4, v6, v7}, Landroid/media/MediaRecorder;->setMaxFileSize(J)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 409
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    if-eqz v4, :cond_2

    .line 410
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v9}, Landroid/media/MediaRecorder;->setOrientationHint(I)V

    .line 412
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    if-eqz v4, :cond_3

    .line 414
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4}, Landroid/media/MediaRecorder;->prepare()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 434
    :cond_3
    :goto_4
    iput-boolean v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorderRecording:Z

    goto/16 :goto_0

    .line 319
    .end local v2    # "maxFileSize":J
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->createVideoPath()V

    .line 320
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getTempFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 341
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v4, v4, v5

    aget v4, v4, v9

    const/16 v5, 0x780

    if-ne v4, v5, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v4, v4, v5

    aget v4, v4, v10

    const/16 v5, 0x438

    if-ne v4, v5, :cond_6

    .line 343
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const v5, 0x1036640

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setVideoEncodingBitRate(I)V

    .line 344
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const v5, 0x1f400

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    .line 345
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v8}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    .line 346
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v12}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    .line 347
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v8}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    .line 348
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v11}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    goto :goto_2

    .line 349
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v4, v4, v5

    aget v4, v4, v9

    const/16 v5, 0x500

    if-ne v4, v5, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v4, v4, v5

    aget v4, v4, v10

    const/16 v5, 0x2d0

    if-ne v4, v5, :cond_7

    .line 351
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const v5, 0xb71b00

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setVideoEncodingBitRate(I)V

    .line 352
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const v5, 0x1f400

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    .line 353
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v8}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    .line 354
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v12}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    .line 355
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v8}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    .line 356
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v11}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    goto/16 :goto_2

    .line 357
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v4, v4, v5

    aget v4, v4, v9

    const/16 v5, 0x2d0

    if-ne v4, v5, :cond_8

    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v4, v4, v5

    aget v4, v4, v10

    const/16 v5, 0x1e0

    if-ne v4, v5, :cond_8

    .line 359
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const v5, 0x34a0a8

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setVideoEncodingBitRate(I)V

    .line 360
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const v5, 0x1f400

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    .line 361
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v8}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    .line 362
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v12}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    .line 363
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v8}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    .line 364
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v11}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    goto/16 :goto_2

    .line 365
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v4, v4, v5

    aget v4, v4, v9

    const/16 v5, 0x280

    if-ne v4, v5, :cond_9

    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v4, v4, v5

    aget v4, v4, v10

    const/16 v5, 0x1e0

    if-ne v4, v5, :cond_9

    .line 367
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const v5, 0x2ef770

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setVideoEncodingBitRate(I)V

    .line 368
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const v5, 0x1f400

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    .line 369
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v8}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    .line 370
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v12}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    .line 371
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v8}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    .line 372
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v11}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    goto/16 :goto_2

    .line 373
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v4, v4, v5

    aget v4, v4, v9

    const/16 v5, 0x140

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v4, v4, v5

    aget v4, v4, v10

    const/16 v5, 0xf0

    if-ne v4, v5, :cond_1

    .line 375
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const v5, 0xbb418

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setVideoEncodingBitRate(I)V

    .line 376
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const v5, 0x1f400

    invoke-virtual {v4, v5}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    .line 377
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v8}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    .line 378
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v12}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    .line 379
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v8}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    .line 380
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v11}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    goto/16 :goto_2

    .line 396
    .restart local v2    # "maxFileSize":J
    :catch_0
    move-exception v0

    .line 399
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->releaseMediaRecorder()V

    goto/16 :goto_3

    .line 415
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 417
    .local v0, "e":Ljava/io/IOException;
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->releaseMediaRecorder()V

    .line 421
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doStopVideoRecordingSync()V

    .line 423
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->startPreview()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 430
    iput-boolean v10, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPreviewing:Z

    goto/16 :goto_4

    .line 424
    :catch_2
    move-exception v1

    .line 425
    .local v1, "ex":Ljava/lang/Throwable;
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "exception while startPreview"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 426
    const v4, 0x7f0a000a

    invoke-direct {p0, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V

    goto/16 :goto_0
.end method

.method public static isSupported(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2
    .param p0, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "supported":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 1190
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private registerVideo()V
    .locals 9

    .prologue
    .line 487
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "registerVideo"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFileDescriptor:Ljava/io/FileDescriptor;

    if-nez v4, :cond_0

    .line 490
    const-string v4, "content://media/external/video/media"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 491
    .local v3, "videoTable":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVideoValues:Landroid/content/ContentValues;

    const-string v5, "_size"

    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVideoFilename:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 492
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVideoValues:Landroid/content/ContentValues;

    const-string v5, "duration"

    iget-wide v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mVideoRecordingTimeInMiliSecond:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 493
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVideoValues:Landroid/content/ContentValues;

    const-string v5, "resolution"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    aget v7, v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v7, v7, v8

    const/4 v8, 0x1

    aget v7, v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    const/4 v1, 0x0

    .line 500
    .local v1, "mCurrentVideoUri":Landroid/net/Uri;
    const-string v4, "autotest"

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 503
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVideoValues:Landroid/content/ContentValues;

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 504
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v4, v5, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3

    .line 515
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->gotoFactoryCameraTest()V

    .line 537
    .end local v1    # "mCurrentVideoUri":Landroid/net/Uri;
    .end local v3    # "videoTable":Landroid/net/Uri;
    :cond_0
    :goto_1
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVideoValues:Landroid/content/ContentValues;

    .line 538
    return-void

    .line 505
    .restart local v1    # "mCurrentVideoUri":Landroid/net/Uri;
    .restart local v3    # "videoTable":Landroid/net/Uri;
    :catch_0
    move-exception v2

    .line 506
    .local v2, "sfe":Landroid/database/sqlite/SQLiteFullException;
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "SQLiteFullException - Not enough space in database"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 507
    .end local v2    # "sfe":Landroid/database/sqlite/SQLiteFullException;
    :catch_1
    move-exception v0

    .line 508
    .local v0, "e":Ljava/lang/UnsupportedOperationException;
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "UnsupportedOperationException - insert failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 509
    .end local v0    # "e":Ljava/lang/UnsupportedOperationException;
    :catch_2
    move-exception v0

    .line 510
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "IllegalStateException - insert failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 511
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 512
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "NullPointerException - insert failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 517
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v4, v4, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    if-eqz v4, :cond_0

    .line 520
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVideoValues:Landroid/content/ContentValues;

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 521
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v4, v5, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_7

    .line 532
    :goto_2
    invoke-virtual {p0, p0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->playVideo(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_1

    .line 522
    :catch_4
    move-exception v2

    .line 523
    .restart local v2    # "sfe":Landroid/database/sqlite/SQLiteFullException;
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "SQLiteFullException - Not enough space in database"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 524
    .end local v2    # "sfe":Landroid/database/sqlite/SQLiteFullException;
    :catch_5
    move-exception v0

    .line 525
    .local v0, "e":Ljava/lang/UnsupportedOperationException;
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "UnsupportedOperationException - insert failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 526
    .end local v0    # "e":Ljava/lang/UnsupportedOperationException;
    :catch_6
    move-exception v0

    .line 527
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "IllegalStateException - insert failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 528
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_7
    move-exception v0

    .line 529
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "NullPointerException - insert failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private releaseMediaRecorder()V
    .locals 3

    .prologue
    .line 438
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    if-eqz v1, :cond_0

    .line 439
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cleanupEmptyFile()V

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->reset()V

    .line 441
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V

    .line 442
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    .line 444
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v1, :cond_1

    .line 446
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->reconnect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 452
    :cond_1
    :goto_0
    return-void

    .line 447
    :catch_0
    move-exception v0

    .line 449
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "mCameraDevice.reconnect() failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private renameTempFile()V
    .locals 3

    .prologue
    .line 810
    new-instance v1, Ljava/io/File;

    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getTempFileName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 811
    .local v1, "src":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFilename:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 812
    .local v0, "dest":Ljava/io/File;
    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 813
    return-void
.end method

.method private sendBroadCastAck(Ljava/lang/String;)V
    .locals 5
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 1287
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendBroadCastAck - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1288
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "TYPE"

    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "TYPE"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 1290
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bSentAck:Z

    .line 1291
    return-void
.end method

.method private sendBroadCastAck(Ljava/lang/String;I)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "result"    # I

    .prologue
    .line 1294
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendBroadCastAck - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1295
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "result"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 1297
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bSentAck:Z

    .line 1298
    return-void
.end method

.method public static setSystemKeyBlock(Landroid/content/ComponentName;I)V
    .locals 4
    .param p0, "componentName"    # Landroid/content/ComponentName;
    .param p1, "keyCode"    # I

    .prologue
    .line 1845
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 1848
    .local v1, "wm":Landroid/view/IWindowManager;
    const/4 v2, 0x1

    :try_start_0
    invoke-interface {v1, p1, p0, v2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1852
    :goto_0
    return-void

    .line 1849
    :catch_0
    move-exception v0

    .line 1850
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v3, "setSystemKeyBlock exception!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private stopPreview()V
    .locals 2

    .prologue
    .line 1477
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPreviewing:Z

    if-eqz v0, :cond_0

    .line 1479
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 1480
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1483
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPreviewing:Z

    .line 1484
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "stopPreview : mPreviewing set false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1485
    return-void
.end method


# virtual methods
.method public acquireDVFS(I)V
    .locals 5
    .param p1, "milisecond"    # I

    .prologue
    const/4 v4, 0x0

    .line 1855
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-nez v0, :cond_0

    .line 1856
    new-instance v0, Landroid/os/DVFSHelper;

    const/16 v1, 0xc

    invoke-direct {v0, p0, v1}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 1857
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->supportedCPUFreqTable:[I

    .line 1858
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->supportedCPUCoreTable:[I

    .line 1860
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->supportedCPUFreqTable:[I

    if-eqz v0, :cond_2

    .line 1861
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->supportedCPUFreqTable:[I

    aget v2, v2, v4

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 1862
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "supportedCPUFreqTable is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->supportedCPUFreqTable:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1867
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->supportedCPUCoreTable:[I

    if-eqz v0, :cond_3

    .line 1868
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->supportedCPUCoreTable:[I

    aget v2, v2, v4

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 1869
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "supportedCPUCoreTable is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->supportedCPUCoreTable:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1875
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    .line 1876
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0, p1}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 1877
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDVFSHelper.acquire : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1879
    :cond_1
    return-void

    .line 1864
    :cond_2
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "supportedCPUFreqTable is null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1871
    :cond_3
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "supportedCPUFreqTable is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public checkAutoRecording()V
    .locals 3

    .prologue
    .line 2087
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkAutoRecording isRecording : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isRecording()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", bIsUsbOrUartCommand : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2089
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    if-eqz v0, :cond_0

    .line 2090
    const-string v0, "com.android.samsungtest.RECORDING_START_ACK"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadCastAck(Ljava/lang/String;I)V

    .line 2092
    :cond_0
    return-void
.end method

.method public doPrepareVideoRecordingAsync()V
    .locals 2

    .prologue
    .line 467
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "doPrepareVideoRecordingAsync"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_0

    .line 470
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "mMediaRecorder is already initialized."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "Releasing mMediaRecorder..."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->releaseMediaRecorder()V

    .line 474
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_1

    .line 475
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/camera/CamcorderPreviewTest$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest$1;-><init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPrepareRecordingThread:Ljava/lang/Thread;

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPrepareRecordingThread:Ljava/lang/Thread;

    const-string v1, "PrepareRecordingThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPrepareRecordingThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 484
    :cond_1
    return-void
.end method

.method public doStartVideoRecordingAsync()V
    .locals 2

    .prologue
    .line 607
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "doStartVideoRecordingAsync"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mVideoRecordingTimeInMiliSecond:J

    .line 612
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPrepareRecordingThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPrepareRecordingThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 619
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    if-eqz v0, :cond_1

    const-string v0, "autotest"

    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/framework/ShutterButton;->setVisibility(I)V

    .line 622
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/framework/ShutterButton;->setVisibility(I)V

    .line 625
    :cond_1
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "start video recording"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    iget-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorderRecording:Z

    if-nez v0, :cond_2

    .line 628
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;-><init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStartRecordingThread:Ljava/lang/Thread;

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStartRecordingThread:Ljava/lang/Thread;

    const-string v1, "StartRecordingThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 667
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStartRecordingThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 669
    :cond_2
    return-void

    .line 615
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public doStopVideoRecordingSync()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 672
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "doStopVideoRecordingSync"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camera/framework/CameraSettings;->hasFlash()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 678
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v2, "flash-mode"

    const-string v3, "off"

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "flash-mode is FLASH_OFF"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 688
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    if-eqz v1, :cond_1

    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 690
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/camera/framework/ShutterButton;->setVisibility(I)V

    .line 691
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camera/framework/ShutterButton;->setVisibility(I)V

    .line 695
    :cond_1
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "Stopping VideoRecording..."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 696
    iget-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorderRecording:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    if-eqz v1, :cond_2

    .line 699
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    .line 700
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    .line 701
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->stop()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 716
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->renameTempFile()V

    .line 717
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFilename:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVideoFilename:Ljava/lang/String;

    .line 718
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting current video filename: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVideoFilename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    iput-boolean v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorderRecording:Z

    .line 721
    iput v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    .line 722
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->releaseMediaRecorder()V

    .line 723
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->registerVideo()V

    .line 726
    :cond_2
    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFilename:Ljava/lang/String;

    .line 727
    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFileDescriptor:Ljava/io/FileDescriptor;

    .line 731
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "Stopping VideoRecording is completed!"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    :goto_1
    return-void

    .line 683
    :catch_0
    move-exception v0

    .line 684
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "flash-mode IllegalArgumentException"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 702
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 703
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stop failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    iput-boolean v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorderRecording:Z

    .line 705
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->releaseMediaRecorder()V

    .line 707
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cleanupTempFile()V

    .line 708
    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFilename:Ljava/lang/String;

    .line 709
    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraVideoFileDescriptor:Ljava/io/FileDescriptor;

    .line 712
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    goto :goto_1
.end method

.method protected findBestFpsRange(Landroid/hardware/Camera$Parameters;II)[I
    .locals 10
    .param p1, "parameters"    # Landroid/hardware/Camera$Parameters;
    .param p2, "requestedMinFps"    # I
    .param p3, "requestedMaxFps"    # I

    .prologue
    .line 840
    sget-object v7, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Requsted fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    const/4 v1, 0x0

    .line 843
    .local v1, "MIN_IDX":I
    const/4 v0, 0x1

    .line 844
    .local v0, "MAX_IDX":I
    const/4 v7, 0x2

    new-array v3, v7, [I

    .line 845
    .local v3, "fpsRange":[I
    const/4 v7, 0x2

    new-array v2, v7, [I

    .line 847
    .local v2, "bestFpsRange":[I
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v4

    .line 849
    .local v4, "fpsRangeList":Ljava/util/List;, "Ljava/util/List<[I>;"
    if-nez v4, :cond_0

    .line 850
    sget-object v7, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v8, "supported preview fps range is null"

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    const/4 v3, 0x0

    .line 908
    .end local v3    # "fpsRange":[I
    :goto_0
    return-object v3

    .line 854
    .restart local v3    # "fpsRange":[I
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v5, v7, -0x1

    .local v5, "i":I
    :goto_1
    if-ltz v5, :cond_8

    .line 855
    const/4 v8, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    const/4 v9, 0x0

    aget v7, v7, v9

    aput v7, v3, v8

    .line 856
    const/4 v8, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    const/4 v9, 0x1

    aget v7, v7, v9

    aput v7, v3, v8

    .line 858
    const/4 v7, 0x1

    aget v7, v3, v7

    if-ne p3, v7, :cond_6

    .line 859
    const/4 v7, 0x0

    aget v7, v3, v7

    if-ne p2, v7, :cond_1

    .line 861
    move-object v2, v3

    .line 862
    sget-object v7, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 865
    :cond_1
    if-nez v5, :cond_2

    .line 867
    move-object v2, v3

    .line 868
    sget-object v7, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 873
    :cond_2
    move v6, v5

    .local v6, "j":I
    :goto_2
    if-ltz v6, :cond_5

    .line 874
    const/4 v8, 0x0

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    const/4 v9, 0x0

    aget v7, v7, v9

    aput v7, v3, v8

    .line 875
    const/4 v8, 0x1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    const/4 v9, 0x1

    aget v7, v7, v9

    aput v7, v3, v8

    .line 877
    const/4 v7, 0x0

    aget v7, v3, v7

    if-ne p2, v7, :cond_3

    .line 879
    move-object v2, v3

    .line 880
    sget-object v7, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 883
    :cond_3
    const/4 v7, 0x0

    aget v7, v3, v7

    if-le p2, v7, :cond_4

    .line 884
    move-object v2, v3

    .line 885
    sget-object v7, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 873
    :cond_4
    add-int/lit8 v6, v6, -0x1

    goto :goto_2

    .line 892
    :cond_5
    move-object v2, v3

    .line 893
    sget-object v7, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 897
    .end local v6    # "j":I
    :cond_6
    const/4 v7, 0x1

    aget v7, v3, v7

    if-le p3, v7, :cond_7

    .line 898
    move-object v2, v3

    .line 899
    sget-object v7, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 854
    :cond_7
    add-int/lit8 v5, v5, -0x1

    goto/16 :goto_1

    .line 906
    :cond_8
    move-object v2, v3

    .line 907
    sget-object v7, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find best fps range : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public findOptimalPreviewSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;
    .locals 16
    .param p2, "targetRatio"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;D)",
            "Landroid/hardware/Camera$Size;"
        }
    .end annotation

    .prologue
    .line 1883
    .local p1, "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    const-wide v0, 0x3f50624dd2f1a9fcL    # 0.001

    .line 1884
    .local v0, "ASPECT_TOLERANCE":D
    if-nez p1, :cond_1

    .line 1885
    const/4 v6, 0x0

    .line 1932
    :cond_0
    return-object v6

    .line 1887
    :cond_1
    const/4 v6, 0x0

    .line 1888
    .local v6, "optimalSize":Landroid/hardware/Camera$Size;
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 1896
    .local v4, "minDiff":D
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "window"

    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/WindowManager;

    invoke-interface {v11}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 1899
    .local v2, "display":Landroid/view/Display;
    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v11

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v12

    invoke-static {v11, v12}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 1901
    .local v10, "targetHeight":I
    sget-object v11, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "display.getHeight() = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " display.getWidth() = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1904
    if-gtz v10, :cond_2

    .line 1906
    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v10

    .line 1910
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/hardware/Camera$Size;

    .line 1911
    .local v7, "size":Landroid/hardware/Camera$Size;
    iget v11, v7, Landroid/hardware/Camera$Size;->width:I

    int-to-double v12, v11

    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    int-to-double v14, v11

    div-double v8, v12, v14

    .line 1912
    .local v8, "ratio":D
    sub-double v12, v8, p2

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    const-wide v14, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v11, v12, v14

    if-gtz v11, :cond_3

    .line 1914
    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v12, v11

    cmpg-double v11, v12, v4

    if-gez v11, :cond_3

    .line 1915
    move-object v6, v7

    .line 1916
    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v4, v11

    goto :goto_0

    .line 1922
    .end local v7    # "size":Landroid/hardware/Camera$Size;
    .end local v8    # "ratio":D
    :cond_4
    if-nez v6, :cond_0

    .line 1923
    sget-object v11, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v12, "No preview size match the aspect ratio"

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1924
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 1925
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/hardware/Camera$Size;

    .line 1926
    .restart local v7    # "size":Landroid/hardware/Camera$Size;
    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v12, v11

    cmpg-double v11, v12, v4

    if-gez v11, :cond_5

    .line 1927
    move-object v6, v7

    .line 1928
    iget v11, v7, Landroid/hardware/Camera$Size;->height:I

    sub-int/2addr v11, v10

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v4, v11

    goto :goto_1
.end method

.method public getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;
    .locals 1

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraSettings:Lcom/sec/android/app/camera/framework/CameraSettings;

    return-object v0
.end method

.method public gotoFactoryCameraTest()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2095
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "gotoFactoryCameraTest..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2097
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2098
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "camera_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2099
    const-string v1, "ommision_test"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2101
    const-string v1, "camcorder_preview_test"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2102
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->setResult(ILandroid/content/Intent;)V

    .line 2103
    return-void
.end method

.method public isCamcorderFirmwareGood()Z
    .locals 14

    .prologue
    .line 1940
    sget-object v11, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v12, "isCamcorderFirmwareGood..."

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1941
    const/4 v8, 0x0

    .line 1942
    .local v8, "mFWInfo":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1943
    .local v5, "fr":Ljava/io/FileReader;
    const/4 v1, 0x0

    .line 1944
    .local v1, "br":Ljava/io/BufferedReader;
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1947
    .local v0, "IsOldPath":Ljava/lang/Boolean;
    :try_start_0
    const-string v10, "/sys/class/camera/rear/rear_checkfw_factory"

    .line 1948
    .local v10, "sysFsPath":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1950
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_0

    .line 1951
    const-string v10, "/sys/class/camera/rear/rear_camfw"

    .line 1952
    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1955
    :cond_0
    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, v10}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1956
    .end local v5    # "fr":Ljava/io/FileReader;
    .local v6, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1957
    .end local v1    # "br":Ljava/io/BufferedReader;
    .local v2, "br":Ljava/io/BufferedReader;
    :try_start_2
    sget-object v11, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CamcorderPreviewTest.isCamcorderFirmwareGood() fr : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1958
    if-eqz v6, :cond_1

    if-eqz v2, :cond_1

    .line 1959
    sget-object v11, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CamcorderPreviewTest.isCamcorderFirmwareGood() br : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1960
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    .line 1961
    sget-object v11, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CamcorderPreviewTest.isCamcorderFirmwareGood - FW info["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1964
    :cond_1
    if-eqz v6, :cond_2

    .line 1965
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V

    .line 1968
    :cond_2
    if-eqz v2, :cond_3

    .line 1969
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1975
    :cond_3
    if-eqz v6, :cond_4

    .line 1976
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V

    .line 1979
    :cond_4
    if-eqz v2, :cond_5

    .line 1980
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_5
    move-object v1, v2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    move-object v5, v6

    .line 1988
    .end local v4    # "file":Ljava/io/File;
    .end local v6    # "fr":Ljava/io/FileReader;
    .end local v10    # "sysFsPath":Ljava/lang/String;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :cond_6
    :goto_0
    sget-object v11, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CamcorderPreviewTest.isCamcorderFirmwareGood() mFWInfo : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1989
    if-eqz v8, :cond_8

    .line 1990
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v8, v11}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 1991
    .local v7, "lastFWInfo":C
    const/16 v11, 0x4d

    if-ne v7, v11, :cond_c

    const/4 v9, 0x0

    .line 1992
    .local v9, "needToErrorPopup":Z
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    if-eqz v11, :cond_e

    .line 1993
    const-string v11, "NG_"

    invoke-virtual {v8, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_7

    if-eqz v9, :cond_d

    .line 1994
    :cond_7
    const v11, 0x7f0a0024

    invoke-direct {p0, v11}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogNGPopup(I)V

    .line 2009
    .end local v7    # "lastFWInfo":C
    .end local v9    # "needToErrorPopup":Z
    :cond_8
    :goto_2
    const/4 v11, 0x1

    :goto_3
    return v11

    .line 1982
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "file":Ljava/io/File;
    .restart local v6    # "fr":Ljava/io/FileReader;
    .restart local v10    # "sysFsPath":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 1983
    .local v3, "e":Ljava/io/IOException;
    sget-object v11, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v12, "CamcorderPreviewTest.isCamcorderFirmwareGood() exception!!"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    move-object v5, v6

    .line 1986
    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_0

    .line 1971
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "file":Ljava/io/File;
    .end local v10    # "sysFsPath":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 1972
    .local v3, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_4
    sget-object v11, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v12, "CamcorderPreviewTest.isCamcorderFirmwareGood Read FW info fail"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1975
    if-eqz v5, :cond_9

    .line 1976
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 1979
    :cond_9
    if-eqz v1, :cond_6

    .line 1980
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 1982
    :catch_2
    move-exception v3

    .line 1983
    .local v3, "e":Ljava/io/IOException;
    sget-object v11, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v12, "CamcorderPreviewTest.isCamcorderFirmwareGood() exception!!"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1974
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    .line 1975
    :goto_5
    if-eqz v5, :cond_a

    .line 1976
    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 1979
    :cond_a
    if-eqz v1, :cond_b

    .line 1980
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 1985
    :cond_b
    :goto_6
    throw v11

    .line 1982
    :catch_3
    move-exception v3

    .line 1983
    .restart local v3    # "e":Ljava/io/IOException;
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, "CamcorderPreviewTest.isCamcorderFirmwareGood() exception!!"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 1991
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v7    # "lastFWInfo":C
    :cond_c
    const/4 v9, 0x1

    goto :goto_1

    .line 1996
    .restart local v9    # "needToErrorPopup":Z
    :cond_d
    sget-object v11, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v12, "CamcorderPreviewTest.isCamcorderFirmwareGood() Ok."

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1997
    const/4 v11, 0x1

    goto :goto_3

    .line 2000
    :cond_e
    const-string v11, "NG"

    invoke-virtual {v8, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_f

    if-eqz v9, :cond_10

    .line 2001
    :cond_f
    const v11, 0x7f0a0024

    invoke-direct {p0, v11}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogNGPopup(I)V

    goto :goto_2

    .line 2003
    :cond_10
    sget-object v11, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v12, "Camera.isCameraFirmwareGood() Ok."

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2004
    const/4 v11, 0x1

    goto :goto_3

    .line 1974
    .end local v5    # "fr":Ljava/io/FileReader;
    .end local v7    # "lastFWInfo":C
    .end local v9    # "needToErrorPopup":Z
    .restart local v4    # "file":Ljava/io/File;
    .restart local v6    # "fr":Ljava/io/FileReader;
    .restart local v10    # "sysFsPath":Ljava/lang/String;
    :catchall_1
    move-exception v11

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_5

    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v11

    move-object v1, v2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_5

    .line 1971
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_4
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_5
    move-exception v3

    move-object v1, v2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_4
.end method

.method public isRecording()Z
    .locals 2

    .prologue
    .line 456
    iget v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 458
    const/4 v0, 0x1

    .line 461
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSupportedFocusModes(Ljava/lang/String;Ljava/util/List;)Z
    .locals 1
    .param p1, "checkfocusmode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1200
    .local p2, "focusmodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1, p2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isSupported(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1201
    const/4 v0, 0x1

    .line 1204
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 569
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 571
    const-string v3, "KYS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "111 onActivityResult() : requestCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    const-string v3, "KYS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "111 onActivityResult() : resultCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    packed-switch p1, :pswitch_data_0

    .line 605
    :cond_0
    :goto_0
    return-void

    .line 575
    :pswitch_0
    if-ne p2, v7, :cond_1

    .line 578
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 579
    .local v1, "intents":Landroid/content/Intent;
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/camera/Camera;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 580
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "camera_id"

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 581
    const-string v3, "ommision_test"

    invoke-virtual {v0, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 582
    const-string v3, "torch_on"

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 583
    const-string v3, "camcorder_preview_test"

    invoke-virtual {v0, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 585
    invoke-virtual {p0, v7, v0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->setResult(ILandroid/content/Intent;)V

    .line 586
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->finish()V

    goto :goto_0

    .line 587
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "intents":Landroid/content/Intent;
    :cond_1
    if-nez p2, :cond_0

    .line 590
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 591
    .restart local v1    # "intents":Landroid/content/Intent;
    if-eqz p3, :cond_2

    .line 592
    const-string v3, "CAMERA FAIL"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 593
    .local v2, "strFailMessage":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 594
    const-string v3, "CAMERA FAIL"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 597
    .end local v2    # "strFailMessage":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0, v6, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->setResult(ILandroid/content/Intent;)V

    .line 599
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->finish()V

    goto :goto_0

    .line 573
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x0

    .line 913
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 914
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "onCreate"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 916
    invoke-static {}, Lcom/sec/android/app/camera/CameraApp;->getAppFeature()Lcom/sec/android/app/camera/Feature;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    .line 918
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    invoke-static {v4, v8}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 919
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    const/16 v5, 0x1a

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 920
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    const/16 v5, 0xbb

    invoke-static {v4, v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 922
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "test_type"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    .line 923
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "camera_id"

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraType:I

    .line 925
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "usb_or_uart_command"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    .line 926
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "movie_index_count"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMovieIndexCount:I

    .line 927
    iget v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMovieIndexCount:I

    const/16 v5, 0x64

    if-lt v4, v5, :cond_0

    .line 928
    iput v7, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMovieIndexCount:I

    .line 931
    :cond_0
    iget v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraType:I

    if-nez v4, :cond_1

    .line 932
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isCamcorderFirmwareGood()Z

    .line 935
    :cond_1
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "testType = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", cameraType = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraType:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", bIsUsbOrUartCommand = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mMovieIndexCount = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMovieIndexCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 943
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-object v4, v4, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    array-length v4, v4

    const/4 v5, 0x2

    filled-new-array {v4, v5}, [I

    move-result-object v4

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[I

    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    .line 944
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-object v4, v4, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    .line 945
    iput v7, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    .line 947
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v4, v4, Lcom/sec/android/app/camera/Feature;->CAMERA_BOOST_ENABLE:Z

    if-eqz v4, :cond_2

    .line 948
    const/16 v4, 0x7d0

    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->acquireDVFS(I)V

    .line 951
    :cond_2
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "onCreate"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    new-instance v2, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;-><init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V

    invoke-direct {v2, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1010
    .local v2, "openCameraThread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 1012
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 1013
    .local v3, "win":Landroid/view/Window;
    const v4, 0x680080

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    .line 1014
    const/high16 v4, 0x7f030000

    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->setContentView(I)V

    .line 1016
    const v4, 0x7f09000e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1017
    .local v1, "mHelpText":Landroid/widget/TextView;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    aget-object v5, v5, v7

    aget v5, v5, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    aget-object v5, v5, v7

    const/4 v6, 0x1

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1019
    const v4, 0x7f09000b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/camera/VideoPreview;

    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceView:Lcom/sec/android/app/camera/VideoPreview;

    .line 1020
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceView:Lcom/sec/android/app/camera/VideoPreview;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/VideoPreview;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 1021
    .local v0, "holder":Landroid/view/SurfaceHolder;
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 1022
    invoke-interface {v0, v8}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 1024
    const v4, 0x7f09000c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/camera/framework/ShutterButton;

    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    .line 1025
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/camera/framework/ShutterButton;->setOnShutterButtonListener(Lcom/sec/android/app/camera/framework/ShutterButton$OnShutterButtonListener;)V

    .line 1027
    const v4, 0x7f09000d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/camera/framework/ShutterButton;

    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    .line 1028
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/camera/framework/ShutterButton;->setOnShutterButtonListener(Lcom/sec/android/app/camera/framework/ShutterButton$OnShutterButtonListener;)V

    .line 1030
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mShutterButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    invoke-virtual {v4, v9}, Lcom/sec/android/app/camera/framework/ShutterButton;->setVisibility(I)V

    .line 1031
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopButton:Lcom/sec/android/app/camera/framework/ShutterButton;

    invoke-virtual {v4, v9}, Lcom/sec/android/app/camera/framework/ShutterButton;->setVisibility(I)V

    .line 1034
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1037
    :goto_0
    return-void

    .line 1035
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public onError(Landroid/media/MediaRecorder;II)V
    .locals 3
    .param p1, "mr"    # Landroid/media/MediaRecorder;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 206
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaRecorder onError = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    const v0, 0x7f0a000b

    invoke-direct {p0, v0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V

    .line 209
    return-void
.end method

.method public onInfo(Landroid/media/MediaRecorder;II)V
    .locals 4
    .param p1, "mr"    # Landroid/media/MediaRecorder;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 212
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "onInfo...."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const/16 v1, 0x320

    if-ne p2, v1, :cond_3

    .line 214
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "onInfo - MEDIA_RECORDER_INFO_MAX_DURATION_REACHED"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doStopVideoRecordingSync()V

    .line 217
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onInfo - testType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopCamera:Z

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->finish()V

    .line 223
    :cond_0
    const-string v1, "com.android.samsungtest.CAMCORDER_GOOD"

    invoke-direct {p0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadCastAck(Ljava/lang/String;)V

    .line 225
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    if-eqz v1, :cond_2

    .line 235
    :cond_1
    :goto_0
    return-void

    .line 228
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 229
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->setResult(ILandroid/content/Intent;)V

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->finish()V

    goto :goto_0

    .line 232
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    const/16 v1, 0x321

    if-ne p2, v1, :cond_1

    .line 233
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "onInfo - MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 11
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const v7, 0x7f0a00a4

    const/4 v10, 0x0

    const/4 v3, 0x1

    .line 1426
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onKeyDown keyCode = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1427
    const/4 v1, 0x0

    .line 1429
    .local v1, "mKeyString":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 1461
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "CamcorderPreviewTest.onKeyDown() default return true"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    const/16 v4, 0x1a

    if-ne p1, v4, :cond_0

    .line 1463
    const-string v1, "Hold"

    .line 1465
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mKeyFailToast:Landroid/widget/Toast;

    if-nez v4, :cond_0

    .line 1466
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v10

    invoke-virtual {p0, v7, v3}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mKeyFailToast:Landroid/widget/Toast;

    .line 1467
    iget-object v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mKeyFailToast:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1473
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    :cond_1
    :goto_1
    :sswitch_0
    return v3

    .line 1434
    :sswitch_1
    const-string v1, "Volume Up"

    .line 1436
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mKeyFailToast:Landroid/widget/Toast;

    if-nez v4, :cond_1

    .line 1437
    new-array v4, v3, [Ljava/lang/Object;

    aput-object v1, v4, v10

    invoke-virtual {p0, v7, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mKeyFailToast:Landroid/widget/Toast;

    .line 1438
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mKeyFailToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1443
    :sswitch_2
    iget-boolean v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mIsPressedBackkey:Z

    if-nez v4, :cond_2

    .line 1444
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1445
    .local v2, "rightNow":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentTime:J

    .line 1446
    iput-boolean v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mIsPressedBackkey:Z

    .line 1447
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->startTimer()V

    goto :goto_1

    .line 1450
    .end local v2    # "rightNow":Ljava/util/Calendar;
    :cond_2
    iput-boolean v10, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mIsPressedBackkey:Z

    .line 1451
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1453
    .restart local v2    # "rightNow":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentTime:J

    const-wide/16 v8, 0x7d0

    add-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    .line 1454
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1455
    .local v0, "intents":Landroid/content/Intent;
    const-string v4, "camcorder_preview_test_backkey_canceled"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1456
    invoke-virtual {p0, v10, v0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 1429
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x18 -> :sswitch_1
        0x19 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 18
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1526
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "onKeyUp keyCode = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1527
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mKeyFailToast:Landroid/widget/Toast;

    .line 1529
    sparse-switch p1, :sswitch_data_0

    .line 1751
    invoke-super/range {p0 .. p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v12

    :goto_0
    return v12

    .line 1531
    :sswitch_0
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "onKeyUp - testType : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1532
    const-string v12, "autotest"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1533
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, "onKeyUp - Volume down key is not used in auto test mode."

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1534
    const/4 v12, 0x1

    goto :goto_0

    .line 1537
    :cond_0
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "KEYCODE_VOLUME_DOWN mRecordingState : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", isPreviewStarted : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isPreviewStarted:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1538
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isPreviewStarted:Z

    if-nez v12, :cond_1

    .line 1539
    const/4 v12, 0x1

    goto :goto_0

    .line 1542
    :cond_1
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    const/4 v13, 0x4

    if-eq v12, v13, :cond_2

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    const/4 v13, 0x1

    if-eq v12, v13, :cond_2

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    if-nez v12, :cond_3

    .line 1543
    :cond_2
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "volumedownkey return !!! mRecordingState : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1544
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 1547
    :cond_3
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    .line 1548
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-object v13, v13, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    array-length v13, v13

    add-int/lit8 v13, v13, -0x1

    if-le v12, v13, :cond_e

    .line 1549
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-object v12, v12, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    array-length v12, v12

    add-int/lit8 v12, v12, -0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    .line 1552
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v12, v12, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    if-eqz v12, :cond_a

    .line 1553
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVolumeDownKeyFirshTime:J

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-nez v12, :cond_5

    .line 1554
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 1555
    .local v9, "rightNow":Ljava/util/Calendar;
    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVolumeDownKeyFirshTime:J

    .line 1569
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isRecording()Z

    move-result v12

    if-nez v12, :cond_9

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    const/4 v13, 0x4

    if-eq v12, v13, :cond_9

    .line 1571
    const/4 v12, 0x4

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    .line 1573
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getCameraSettings()Lcom/sec/android/app/camera/framework/CameraSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/camera/framework/CameraSettings;->hasFlash()Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1574
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraType:I

    if-nez v12, :cond_8

    .line 1575
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v13, "flash-mode"

    const-string v14, "torch"

    invoke-virtual {v12, v13, v14}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1576
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, "flash-mode is FLASH_TORCH"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1583
    :goto_2
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v12, v13}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1589
    :cond_4
    :goto_3
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, "onKeyUp Camcorder Recoding "

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1590
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z

    .line 1591
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doPrepareVideoRecordingAsync()V

    .line 1592
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doStartVideoRecordingAsync()V

    .line 1593
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mTimerHandler:Landroid/os/Handler;

    const/16 v13, 0x1f

    const-wide/16 v14, 0xaf0

    invoke-virtual {v12, v13, v14, v15}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1595
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 1557
    .end local v9    # "rightNow":Ljava/util/Calendar;
    :cond_5
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 1559
    .restart local v9    # "rightNow":Ljava/util/Calendar;
    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVolumeDownKeyFirshTime:J

    const-wide/16 v16, 0x7d0

    add-long v14, v14, v16

    cmp-long v12, v12, v14

    if-lez v12, :cond_6

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z

    if-nez v12, :cond_7

    .line 1561
    :cond_6
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, " volumedownkey return !!! TIME "

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1562
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 1565
    :cond_7
    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurrentVolumeDownKeyFirshTime:J

    goto/16 :goto_1

    .line 1578
    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v13, "flash-mode"

    const-string v14, "off"

    invoke-virtual {v12, v13, v14}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1579
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, "flash-mode is FLASH_OFF"

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1584
    :catch_0
    move-exception v3

    .line 1585
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, "flash-mode IllegalArgumentException"

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 1597
    .end local v3    # "e":Ljava/lang/IllegalArgumentException;
    :cond_9
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "onKeyUp Camcorder Stop mCamcorderStop : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1598
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z

    if-eqz v12, :cond_c

    .line 1599
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    .line 1600
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doStopVideoRecordingSync()V

    .line 1608
    .end local v9    # "rightNow":Ljava/util/Calendar;
    :cond_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v12, :cond_b

    .line 1609
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->stopPreview()V

    .line 1610
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->closeCamera()V

    .line 1613
    :cond_b
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v12, v12, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    if-eqz v12, :cond_d

    .line 1614
    const-string v12, "KYS"

    const-string v13, "Nothing"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1632
    :goto_4
    const-string v12, "KYS"

    const-string v13, "RESULT_OK VOLUME_DOWN"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1633
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 1602
    .restart local v9    # "rightNow":Ljava/util/Calendar;
    :cond_c
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, "volumedownkey return TIME!!!"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1603
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 1619
    .end local v9    # "rightNow":Ljava/util/Calendar;
    :cond_d
    const-string v12, "KYS"

    const-string v13, "Nothing 111 "

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1621
    new-instance v5, Landroid/content/Intent;

    const-class v12, Lcom/sec/android/app/camera/Camera;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1622
    .local v5, "intent":Landroid/content/Intent;
    const-string v12, "camera_id"

    const/4 v13, 0x0

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1623
    const-string v12, "ommision_test"

    const/4 v13, 0x1

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1624
    const-string v12, "torch_on"

    const/4 v13, 0x0

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1625
    const-string v12, "camcorder_preview_test"

    const/4 v13, 0x1

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1628
    const/4 v12, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->setResult(ILandroid/content/Intent;)V

    .line 1629
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->finish()V

    goto :goto_4

    .line 1636
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_e
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v12, :cond_f

    .line 1637
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->stopPreview()V

    .line 1638
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->closeCamera()V

    .line 1641
    :cond_f
    const v12, 0x7f09000e

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1642
    .local v6, "mHelpText":Landroid/widget/TextView;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v13, v13, v14

    const/4 v14, 0x0

    aget v13, v13, v14

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "x"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v13, v13, v14

    const/4 v14, 0x1

    aget v13, v13, v14

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1645
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v12, v12, Lcom/sec/android/app/camera/Feature;->CAMERA_BOOST_ENABLE:Z

    if-eqz v12, :cond_10

    .line 1646
    const/16 v12, 0x7d0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->acquireDVFS(I)V

    .line 1649
    :cond_10
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraType:I

    invoke-static {v12}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1656
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 1657
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v12}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 1659
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v12, v12, Lcom/sec/android/app/camera/Feature;->CROP_CAMCORDER_PREVIEW_ENABLE:Z

    if-eqz v12, :cond_13

    .line 1660
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v12}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v10

    .line 1661
    .local v10, "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    if-nez v10, :cond_11

    .line 1662
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, "supported preview size is null"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1663
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 1650
    .end local v10    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :catch_1
    move-exception v3

    .line 1651
    .local v3, "e":Ljava/lang/Exception;
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, "service cannot connect. critical exception occured."

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1652
    const v12, 0x7f0a000a

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V

    .line 1653
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 1665
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v10    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_11
    const/4 v8, 0x0

    .line 1666
    .local v8, "previewsize":Landroid/hardware/Camera$Size;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v12}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v7

    .line 1667
    .local v7, "picturesize":Landroid/hardware/Camera$Size;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    const/4 v13, 0x0

    aget-object v12, v12, v13

    const/4 v13, 0x0

    aget v12, v12, v13

    int-to-double v12, v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    const/4 v15, 0x0

    aget-object v14, v14, v15

    const/4 v15, 0x1

    aget v14, v14, v15

    int-to-double v14, v14

    div-double/2addr v12, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v12, v13}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->findOptimalPreviewSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;

    move-result-object v8

    .line 1669
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "preview size is "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v8, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " x "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v8, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1670
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v13, v8, Landroid/hardware/Camera$Size;->width:I

    iget v14, v8, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v12, v13, v14}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 1676
    .end local v7    # "picturesize":Landroid/hardware/Camera$Size;
    .end local v8    # "previewsize":Landroid/hardware/Camera$Size;
    .end local v10    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :goto_5
    :try_start_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v12, v13}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1685
    :goto_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorCallback:Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;

    invoke-virtual {v12, v13}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    .line 1687
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v12, :cond_15

    .line 1689
    :try_start_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v12, v13}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1702
    :goto_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v12}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 1703
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v13, "cam_mode"

    const-string v14, "1"

    invoke-virtual {v12, v13, v14}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1706
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v12, v12, v13

    const/4 v13, 0x0

    aget v12, v12, v13

    const/16 v13, 0x780

    if-ne v12, v13, :cond_12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v12, v12, v13

    const/4 v13, 0x1

    aget v12, v12, v13

    const/16 v13, 0x438

    if-ne v12, v13, :cond_12

    .line 1708
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v13, "video-size"

    const-string v14, "1920x1080"

    invoke-virtual {v12, v13, v14}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1711
    :cond_12
    :try_start_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v12, v13}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_4

    .line 1721
    :goto_8
    :try_start_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v12}, Landroid/hardware/Camera;->startPreview()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_5

    .line 1728
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPreviewing:Z

    .line 1730
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 1672
    :cond_13
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v13, v13, v14

    const/4 v14, 0x0

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v14, v14, v15

    const/4 v15, 0x1

    aget v14, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    goto/16 :goto_5

    .line 1679
    :catch_2
    move-exception v3

    .line 1680
    .local v3, "e":Ljava/lang/RuntimeException;
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, "setParameter fail"

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1681
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;

    const/16 v13, 0x9

    invoke-virtual {v12, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1682
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 1690
    .end local v3    # "e":Ljava/lang/RuntimeException;
    :catch_3
    move-exception v4

    .line 1691
    .local v4, "exception":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v12, :cond_14

    .line 1692
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v12}, Landroid/hardware/Camera;->release()V

    .line 1693
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    .line 1695
    :cond_14
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 1698
    .end local v4    # "exception":Ljava/io/IOException;
    :cond_15
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceView:Lcom/sec/android/app/camera/VideoPreview;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v13, v13, v14

    const/4 v14, 0x0

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v14, v14, v15

    const/4 v15, 0x1

    aget v14, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/app/camera/VideoPreview;->setAspectRatio(II)V

    goto/16 :goto_7

    .line 1714
    :catch_4
    move-exception v3

    .line 1715
    .restart local v3    # "e":Ljava/lang/RuntimeException;
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, "setParameter fail"

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1716
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;

    const/16 v13, 0x9

    invoke-virtual {v12, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1717
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 1722
    .end local v3    # "e":Ljava/lang/RuntimeException;
    :catch_5
    move-exception v3

    .line 1723
    .local v3, "e":Ljava/lang/Throwable;
    sget-object v12, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v13, "exception while startPreview"

    invoke-static {v12, v13, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1724
    const v12, 0x7f0a000a

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V

    .line 1725
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 1732
    .end local v3    # "e":Ljava/lang/Throwable;
    .end local v6    # "mHelpText":Landroid/widget/TextView;
    :sswitch_1
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 1734
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->isShowFailDialog()Z

    move-result v12

    if-eqz v12, :cond_17

    .line 1736
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    const/4 v13, -0x1

    if-eq v12, v13, :cond_16

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    const/4 v13, 0x1

    if-eq v12, v13, :cond_16

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    if-nez v12, :cond_18

    .line 1739
    :cond_16
    const-string v11, "REAR CAMERA VIDEO PREVIEW"

    .line 1746
    .local v11, "strFailMessage":Ljava/lang/String;
    :goto_9
    invoke-static {v11}, Lcom/sec/android/app/camera/Camera$FailDialogFragment;->newInstance(Ljava/lang/String;)Lcom/sec/android/app/camera/Camera$FailDialogFragment;

    move-result-object v2

    .line 1747
    .local v2, "alertDialog":Landroid/app/DialogFragment;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v12

    const-string v13, "dialog"

    invoke-virtual {v2, v12, v13}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1749
    .end local v2    # "alertDialog":Landroid/app/DialogFragment;
    .end local v11    # "strFailMessage":Ljava/lang/String;
    :cond_17
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 1740
    :cond_18
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    const/4 v13, 0x3

    if-eq v12, v13, :cond_19

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    const/4 v13, 0x4

    if-ne v12, v13, :cond_1a

    .line 1742
    :cond_19
    const-string v11, "REAR CAMERA VIDEO RECORDING"

    .restart local v11    # "strFailMessage":Ljava/lang/String;
    goto :goto_9

    .line 1744
    .end local v11    # "strFailMessage":Ljava/lang/String;
    :cond_1a
    const-string v11, "REAR CAMERA"

    .restart local v11    # "strFailMessage":Ljava/lang/String;
    goto :goto_9

    .line 1712
    .end local v11    # "strFailMessage":Ljava/lang/String;
    .restart local v6    # "mHelpText":Landroid/widget/TextView;
    :catch_6
    move-exception v12

    goto/16 :goto_8

    .line 1677
    :catch_7
    move-exception v12

    goto/16 :goto_6

    .line 1529
    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_1
        0x19 -> :sswitch_0
        0xbb -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1210
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "onPause"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1212
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 1213
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1214
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "releaseWakelock"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1215
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1217
    :cond_0
    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1220
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPausing:Z

    .line 1222
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStartCheck:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1225
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    if-eqz v1, :cond_2

    .line 1227
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    .line 1228
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    .line 1229
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->stop()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1235
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cleanupEmptyFile()V

    .line 1236
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->reset()V

    .line 1237
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V

    .line 1238
    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;

    .line 1239
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v2, "onPause : mMediaRecorder stop"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1242
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v1, :cond_3

    .line 1243
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->stopPreview()V

    .line 1244
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    .line 1245
    iput-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    .line 1248
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-eqz v1, :cond_4

    .line 1249
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v1}, Landroid/os/DVFSHelper;->release()V

    .line 1253
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1257
    :goto_1
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    .line 1258
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPause - mStopCamera : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopCamera:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1259
    iget-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopCamera:Z

    if-eqz v1, :cond_5

    .line 1260
    const-string v1, "autotest"

    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1261
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPause - bIsUsbOrUartCommand : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1262
    iget-boolean v1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    if-eqz v1, :cond_6

    .line 1263
    const-string v1, "com.android.samsungtest.RECORDING_STOP_ACK"

    invoke-direct {p0, v1, v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadCastAck(Ljava/lang/String;I)V

    .line 1268
    :goto_2
    iput-boolean v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bIsUsbOrUartCommand:Z

    .line 1273
    :goto_3
    iput-boolean v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopCamera:Z

    .line 1275
    :cond_5
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1276
    return-void

    .line 1231
    :catch_0
    move-exception v0

    .line 1232
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v1, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPause : mMediaRecorder stop RuntimeException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1265
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_6
    const-string v1, "com.android.samsungtest.CAMERA_STOP_ACK"

    invoke-direct {p0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadCastAck(Ljava/lang/String;)V

    goto :goto_2

    .line 1270
    :cond_7
    const-string v1, "com.android.samsungtest.CAMERA_STOP_ACK"

    invoke-direct {p0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->sendBroadCastAck(Ljava/lang/String;)V

    goto :goto_3

    .line 1254
    :catch_1
    move-exception v1

    goto/16 :goto_1
.end method

.method public onResume()V
    .locals 15

    .prologue
    const/16 v14, 0x9

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 1047
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1048
    sget-object v8, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v9, "[WRG] onResume()"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1051
    iput-boolean v12, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->bSentAck:Z

    .line 1052
    iput-boolean v12, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mStopCamera:Z

    .line 1054
    const-string v8, "power"

    invoke-virtual {p0, v8}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/PowerManager;

    .line 1055
    .local v5, "pm":Landroid/os/PowerManager;
    const v8, 0x3000001a

    sget-object v9, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1057
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1059
    iput-boolean v12, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPausing:Z

    .line 1062
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 1063
    .local v3, "intentFilter":Landroid/content/IntentFilter;
    const-string v8, "com.android.samsungtest.CameraStop"

    invoke-virtual {v3, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1064
    const-string v8, "com.android.samsungtest.RecordingStart"

    invoke-virtual {v3, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1065
    const-string v8, "com.android.samsungtest.RecordingStop"

    invoke-virtual {v3, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1066
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v3}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1068
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    if-nez v8, :cond_0

    .line 1069
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-object v8, v8, Lcom/sec/android/app/camera/Feature;->TEST_RESOLUTION:[[I

    iput-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    .line 1073
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    if-nez v8, :cond_3

    .line 1075
    :try_start_0
    iget v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraType:I

    invoke-static {v8}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1081
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v8, v12}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 1082
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v8}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 1084
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v8, v8, Lcom/sec/android/app/camera/Feature;->CROP_CAMCORDER_PREVIEW_ENABLE:Z

    if-eqz v8, :cond_6

    .line 1085
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v7

    .line 1086
    .local v7, "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    if-nez v7, :cond_2

    .line 1087
    sget-object v8, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v9, "supported preview size is null"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1186
    .end local v7    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_1
    :goto_0
    return-void

    .line 1076
    :catch_0
    move-exception v0

    .line 1077
    .local v0, "e":Ljava/lang/Exception;
    sget-object v8, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v9, "service cannot connect. critical exception occured."

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1078
    const v8, 0x7f0a000a

    invoke-direct {p0, v8}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V

    goto :goto_0

    .line 1090
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v7    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_2
    const/4 v6, 0x0

    .line 1091
    .local v6, "previewsize":Landroid/hardware/Camera$Size;
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v4

    .line 1092
    .local v4, "picturesize":Landroid/hardware/Camera$Size;
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    aget-object v8, v8, v12

    aget v8, v8, v12

    int-to-double v8, v8

    iget-object v10, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    aget-object v10, v10, v12

    aget v10, v10, v13

    int-to-double v10, v10

    div-double/2addr v8, v10

    invoke-virtual {p0, v7, v8, v9}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->findOptimalPreviewSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;

    move-result-object v6

    .line 1094
    sget-object v8, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "preview size is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v6, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " x "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v6, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v9, v6, Landroid/hardware/Camera$Size;->width:I

    iget v10, v6, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v8, v9, v10}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 1101
    .end local v4    # "picturesize":Landroid/hardware/Camera$Size;
    .end local v6    # "previewsize":Landroid/hardware/Camera$Size;
    .end local v7    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :goto_1
    :try_start_1
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v8, v9}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1109
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorCallback:Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;

    invoke-virtual {v8, v9}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    .line 1112
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v8, :cond_7

    .line 1114
    :try_start_2
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v8, v9}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1128
    :goto_3
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v8}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 1129
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v9, "cam_mode"

    const-string v10, "1"

    invoke-virtual {v8, v9, v10}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1130
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v8, v8, Lcom/sec/android/app/camera/Feature;->SET_CAMCORDER_VIDEO_SIZE:Z

    if-eqz v8, :cond_4

    .line 1131
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v10, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v9, v9, v10

    aget v9, v9, v12

    iget-object v10, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v11, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v10, v10, v11

    aget v10, v10, v13

    invoke-virtual {p0, v8, v9, v10}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->setVideoSize(Landroid/hardware/Camera$Parameters;II)V

    .line 1135
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v9, v9, Lcom/sec/android/app/camera/Feature;->CAMCORDER_PREVIEW_FPS_MIN:I

    iget-object v10, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v10, v10, Lcom/sec/android/app/camera/Feature;->CAMCORDER_PREVIEW_FPS_MAX:I

    invoke-virtual {p0, v8, v9, v10}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->findBestFpsRange(Landroid/hardware/Camera$Parameters;II)[I

    move-result-object v2

    .line 1137
    .local v2, "fpsrange":[I
    if-eqz v2, :cond_5

    .line 1138
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    aget v9, v2, v12

    aget v10, v2, v13

    invoke-virtual {v8, v9, v10}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    .line 1141
    :cond_5
    const-string v8, "continuous-picture"

    iget-object v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isSupportedFocusModes(Ljava/lang/String;Ljava/util/List;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1142
    sget-object v8, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v9, "Focus - Camcorder SupportedFocusModes() : set continuous-picture mode"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1143
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v9, "continuous-picture"

    invoke-virtual {v8, v9}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 1150
    :goto_4
    :try_start_3
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v8, v9}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1179
    :goto_5
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPreviewCallback:Lcom/sec/android/app/camera/CamcorderPreviewTest$PreviewCallback;

    invoke-virtual {v8, v9}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1182
    sget-object v8, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onResume - testType : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mErrorPopUp : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1183
    const-string v8, "autotest"

    iget-object v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->testType:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorPopup:Landroid/app/AlertDialog;

    if-nez v8, :cond_1

    .line 1184
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->startTimerAutoRecording()V

    goto/16 :goto_0

    .line 1097
    .end local v2    # "fpsrange":[I
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v10, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v9, v9, v10

    aget v9, v9, v12

    iget-object v10, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v11, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v10, v10, v11

    aget v10, v10, v13

    invoke-virtual {v8, v9, v10}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    goto/16 :goto_1

    .line 1104
    :catch_1
    move-exception v0

    .line 1105
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v8, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v9, "setParameter fail"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1106
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v8, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1115
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_2
    move-exception v1

    .line 1116
    .local v1, "exception":Ljava/io/IOException;
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v8, :cond_1

    .line 1117
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v8}, Landroid/hardware/Camera;->release()V

    .line 1118
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    goto/16 :goto_0

    .line 1123
    .end local v1    # "exception":Ljava/io/IOException;
    :cond_7
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceView:Lcom/sec/android/app/camera/VideoPreview;

    iget-object v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v10, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v9, v9, v10

    aget v9, v9, v12

    iget-object v10, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v11, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v10, v10, v11

    aget v10, v10, v13

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/camera/VideoPreview;->setAspectRatio(II)V

    goto/16 :goto_3

    .line 1145
    .restart local v2    # "fpsrange":[I
    :cond_8
    sget-object v8, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v9, "not support focusmode"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1153
    :catch_3
    move-exception v0

    .line 1154
    .restart local v0    # "e":Ljava/lang/RuntimeException;
    sget-object v8, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v9, "setParameter fail"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1155
    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v8, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1151
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_4
    move-exception v8

    goto/16 :goto_5

    .line 1102
    .end local v2    # "fpsrange":[I
    :catch_5
    move-exception v8

    goto/16 :goto_2
.end method

.method public onShutterButtonClick(Lcom/sec/android/app/camera/framework/ShutterButton;)V
    .locals 4
    .param p1, "button"    # Lcom/sec/android/app/camera/framework/ShutterButton;

    .prologue
    const/4 v2, -0x1

    .line 250
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "call onShutterButtonClick"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPausing:Z

    if-eqz v0, :cond_1

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/camera/framework/ShutterButton;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 257
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "Camcorder shutter_button"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isRecording()Z

    move-result v0

    if-nez v0, :cond_2

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doPrepareVideoRecordingAsync()V

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doStartVideoRecordingAsync()V

    .line 261
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mTimerHandler:Landroid/os/Handler;

    const/16 v1, 0x1f

    const-wide/16 v2, 0xaf0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 265
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z

    if-eqz v0, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doStopVideoRecordingSync()V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    if-eqz v0, :cond_0

    .line 269
    invoke-direct {p0, v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->finishCamcorderPreviewTest(I)V

    goto :goto_0

    .line 276
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "Camcorder stop_button"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z

    if-eqz v0, :cond_4

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doStopVideoRecordingSync()V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->CHECK_CAMCORDER_RECORDING_TEST:Z

    if-eqz v0, :cond_3

    .line 281
    const-string v0, "KYS"

    const-string v1, "Nothing"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 283
    :cond_3
    invoke-direct {p0, v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->finishCamcorderPreviewTest(I)V

    goto :goto_0

    .line 286
    :cond_4
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "stop button is visible wrongly in recording state!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 255
    :pswitch_data_0
    .packed-switch 0x7f09000c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onShutterButtonFocus(Lcom/sec/android/app/camera/framework/ShutterButton;Z)V
    .locals 2
    .param p1, "button"    # Lcom/sec/android/app/camera/framework/ShutterButton;
    .param p2, "pressed"    # Z

    .prologue
    .line 238
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "call onShutterButtonFocus"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    iget-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPausing:Z

    if-eqz v0, :cond_0

    .line 247
    :goto_0
    return-void

    .line 242
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/camera/framework/ShutterButton;->getId()I

    goto :goto_0
.end method

.method public playVideo(Landroid/app/Activity;Landroid/net/Uri;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 541
    if-nez p2, :cond_0

    .line 542
    :try_start_0
    new-instance v2, Landroid/content/ActivityNotFoundException;

    invoke-direct {v2}, Landroid/content/ActivityNotFoundException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 558
    :catch_0
    move-exception v0

    .line 559
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v3, "exception playvideo"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :goto_0
    return-void

    .line 551
    :cond_0
    :try_start_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 552
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 554
    const/16 v2, 0x64

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public setVideoSize(Landroid/hardware/Camera$Parameters;II)V
    .locals 3
    .param p1, "p"    # Landroid/hardware/Camera$Parameters;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 1935
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1936
    .local v0, "v":Ljava/lang/String;
    const-string v1, "video-size"

    invoke-virtual {p1, v1, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1937
    return-void
.end method

.method public startRecordingForAutoTest()V
    .locals 6

    .prologue
    const/16 v5, 0x21

    const/16 v4, 0x1f

    const/4 v3, 0x4

    .line 2050
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "startRecordingForAutoTest..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2051
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startRecordingForAutoTest - isRecording : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isRecording()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mRecordingState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPreviewing : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPreviewing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2053
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isRecording()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    if-eq v0, v3, :cond_0

    .line 2054
    iput v3, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I

    .line 2073
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "startRecordingForAutoTest Camcorder Recoding..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2074
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCamcorderStop:Z

    .line 2075
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doPrepareVideoRecordingAsync()V

    .line 2076
    invoke-virtual {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doStartVideoRecordingAsync()V

    .line 2077
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2078
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 2079
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xaf0

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2081
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2084
    :cond_0
    return-void
.end method

.method protected startTimer()V
    .locals 4

    .prologue
    .line 1514
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mTimerHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1516
    return-void
.end method

.method protected startTimerAutoRecording()V
    .locals 5

    .prologue
    const/16 v4, 0x20

    .line 1519
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "startTimerAutoRecording..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1520
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1521
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1523
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 10
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 1766
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v7, "surfaceChanged"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1768
    if-ge p3, p4, :cond_0

    .line 1769
    move v5, p3

    .line 1770
    .local v5, "tmp_swp":I
    move p3, p4

    .line 1771
    move p4, v5

    .line 1773
    .end local v5    # "tmp_swp":I
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceView:Lcom/sec/android/app/camera/VideoPreview;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/camera/VideoPreview;->setVisibility(I)V

    .line 1775
    iget-boolean v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPausing:Z

    if-eqz v6, :cond_2

    .line 1776
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v7, "surfaceChanged - mPausing == true, return"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1842
    :cond_1
    :goto_0
    return-void

    .line 1780
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v6, :cond_6

    .line 1781
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 1782
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/hardware/Camera$Parameters;->setRecordingHint(Z)V

    .line 1784
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v6, v6, Lcom/sec/android/app/camera/Feature;->CROP_CAMCORDER_PREVIEW_ENABLE:Z

    if-eqz v6, :cond_5

    .line 1785
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v4

    .line 1786
    .local v4, "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    if-nez v4, :cond_3

    .line 1787
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v7, "supported preview size is null"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1790
    :cond_3
    const/4 v3, 0x0

    .line 1791
    .local v3, "previewsize":Landroid/hardware/Camera$Size;
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    .line 1792
    .local v2, "picturesize":Landroid/hardware/Camera$Size;
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    const/4 v7, 0x0

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aget v6, v6, v7

    int-to-double v6, v6

    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    const/4 v9, 0x0

    aget-object v8, v8, v9

    const/4 v9, 0x1

    aget v8, v8, v9

    int-to-double v8, v8

    div-double/2addr v6, v8

    invoke-virtual {p0, v4, v6, v7}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->findOptimalPreviewSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;

    move-result-object v3

    .line 1794
    if-eqz v3, :cond_4

    .line 1795
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "preview size is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " x "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1796
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    iget v7, v3, Landroid/hardware/Camera$Size;->width:I

    iget v8, v3, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v6, v7, v8}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 1805
    .end local v2    # "picturesize":Landroid/hardware/Camera$Size;
    .end local v3    # "previewsize":Landroid/hardware/Camera$Size;
    .end local v4    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :goto_1
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v7, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v6, v7}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1815
    :goto_2
    :try_start_1
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[WRG] surfaceChanged - mSurfaceHolder = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1816
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v7, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v6, v7}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1826
    :try_start_2
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v7, "[WRG] surfaceChanged startPreview"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1827
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->isPreviewStarted:Z

    .line 1828
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->startPreview()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 1834
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPreviewing:Z

    goto/16 :goto_0

    .line 1798
    .restart local v2    # "picturesize":Landroid/hardware/Camera$Size;
    .restart local v3    # "previewsize":Landroid/hardware/Camera$Size;
    .restart local v4    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_4
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v7, "optimal preview size is returned null"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1801
    .end local v2    # "picturesize":Landroid/hardware/Camera$Size;
    .end local v3    # "previewsize":Landroid/hardware/Camera$Size;
    .end local v4    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;

    iget-object v7, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    aget v7, v7, v8

    iget-object v8, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I

    iget v9, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCurResolution:I

    aget-object v8, v8, v9

    const/4 v9, 0x1

    aget v8, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    goto :goto_1

    .line 1808
    :catch_0
    move-exception v0

    .line 1809
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v7, "setParameter fail"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1810
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;

    const/16 v7, 0x9

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1817
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v1

    .line 1818
    .local v1, "exception":Ljava/io/IOException;
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v7, "mCameraDevice.setPreviewDisplay exception!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1819
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v6, :cond_1

    .line 1820
    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->release()V

    .line 1821
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;

    goto/16 :goto_0

    .line 1829
    .end local v1    # "exception":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 1830
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v7, "exception while startPreview"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1831
    const v6, 0x7f0a000a

    invoke-direct {p0, v6}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V

    goto/16 :goto_0

    .line 1838
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_6
    sget-object v6, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v7, "surfaceChanged - mCameraDevice NULL! "

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1839
    const v6, 0x7f0a000a

    invoke-direct {p0, v6}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V

    goto/16 :goto_0

    .line 1806
    :catch_3
    move-exception v6

    goto/16 :goto_2
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 1755
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "surfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1756
    iput-object p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 1757
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 1760
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "surfaceDestroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1761
    invoke-direct {p0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->stopPreview()V

    .line 1762
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 1763
    return-void
.end method
