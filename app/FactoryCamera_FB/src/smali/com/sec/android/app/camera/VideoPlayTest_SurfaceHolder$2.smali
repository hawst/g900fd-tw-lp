.class Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;
.super Ljava/lang/Object;
.source "VideoPlayTest_SurfaceHolder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v2, 0x7f080006

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->bPlayComplet:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$502(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;Z)Z

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    iget-object v0, v0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    iget-object v0, v0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # invokes: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->setProgress()I
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$600(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)I

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$000(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 210
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlay:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$200(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/Button;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    iget-object v0, v0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f020012

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPass:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$300(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mFail:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$400(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 213
    return-void

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    iget-object v0, v0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    goto :goto_0

    .line 210
    :cond_1
    const v0, 0x7f020013

    goto :goto_1
.end method
