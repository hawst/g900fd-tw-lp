.class Lcom/sec/android/app/camerafirmware/CameraFirmware$4;
.super Landroid/os/Handler;
.source "CameraFirmware.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camerafirmware/CameraFirmware;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camerafirmware/CameraFirmware;)V
    .locals 0

    .prologue
    .line 960
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 963
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 994
    :goto_0
    return-void

    .line 966
    :pswitch_0
    const-string v0, "CameraFirmware"

    const-string v1, "handleMessage : updateFirmware - finish"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 967
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$1200(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 968
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$1200(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 971
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->hideUpdateProgress()V

    .line 973
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3e9

    if-ne v0, v1, :cond_3

    .line 974
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    const-string v1, "F/W Update complete.\nNeed to reboot!"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 976
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->updateFirmwareUpdateCount()V

    .line 982
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/camerafirmware/CameraFirmware;->setBlockHold(Z)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$1300(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z

    .line 984
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z
    invoke-static {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$100(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 985
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->stopPreview()V

    .line 988
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->resetFWInfo()V

    goto :goto_0

    .line 978
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    const-string v1, "F/W dump complete"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 991
    :pswitch_1
    const-string v0, "CameraFirmware"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handle msg: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 963
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
