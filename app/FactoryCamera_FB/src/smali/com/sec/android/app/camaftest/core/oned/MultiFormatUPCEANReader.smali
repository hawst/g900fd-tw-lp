.class public final Lcom/sec/android/app/camaftest/core/oned/MultiFormatUPCEANReader;
.super Lcom/sec/android/app/camaftest/core/oned/OneDReader;
.source "MultiFormatUPCEANReader.java"


# instance fields
.field private final readers:[Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/oned/OneDReader;-><init>()V

    .line 45
    if-nez p1, :cond_4

    const/4 v0, 0x0

    .line 46
    .local v0, "possibleFormats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v1, "readers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;>;"
    if-eqz v0, :cond_2

    .line 48
    sget-object v2, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_13:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 49
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/EAN13Reader;

    invoke-direct {v2}, Lcom/sec/android/app/camaftest/core/oned/EAN13Reader;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 53
    :cond_0
    :goto_1
    sget-object v2, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_8:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 54
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/EAN8Reader;

    invoke-direct {v2}, Lcom/sec/android/app/camaftest/core/oned/EAN8Reader;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 56
    :cond_1
    sget-object v2, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_E:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 57
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/UPCEReader;

    invoke-direct {v2}, Lcom/sec/android/app/camaftest/core/oned/UPCEReader;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 60
    :cond_2
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 61
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/EAN13Reader;

    invoke-direct {v2}, Lcom/sec/android/app/camaftest/core/oned/EAN13Reader;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 63
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/EAN8Reader;

    invoke-direct {v2}, Lcom/sec/android/app/camaftest/core/oned/EAN8Reader;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 64
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/UPCEReader;

    invoke-direct {v2}, Lcom/sec/android/app/camaftest/core/oned/UPCEReader;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 66
    :cond_3
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v2, v2, [Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;

    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;

    iput-object v2, p0, Lcom/sec/android/app/camaftest/core/oned/MultiFormatUPCEANReader;->readers:[Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;

    .line 67
    return-void

    .line 45
    .end local v0    # "possibleFormats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    .end local v1    # "readers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;>;"
    :cond_4
    sget-object v2, Lcom/sec/android/app/camaftest/core/DecodeHintType;->POSSIBLE_FORMATS:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    move-object v0, v2

    goto :goto_0

    .line 50
    .restart local v0    # "possibleFormats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    .restart local v1    # "readers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;>;"
    :cond_5
    sget-object v2, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_A:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 51
    new-instance v2, Lcom/sec/android/app/camaftest/core/oned/UPCAReader;

    invoke-direct {v2}, Lcom/sec/android/app/camaftest/core/oned/UPCAReader;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public decodeRow(ILcom/sec/android/app/camaftest/core/common/BitArray;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 18
    .param p1, "rowNumber"    # I
    .param p2, "row"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/app/camaftest/core/common/BitArray;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 72
    .local p3, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    invoke-static/range {p2 .. p2}, Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;->findStartGuardPattern(Lcom/sec/android/app/camaftest/core/common/BitArray;)[I

    move-result-object v13

    .line 73
    .local v13, "startGuardPattern":[I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/camaftest/core/oned/MultiFormatUPCEANReader;->readers:[Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;

    .local v3, "arr$":[Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;
    array-length v7, v3

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_5

    aget-object v10, v3, v6

    .line 76
    .local v10, "reader":Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;
    :try_start_0
    move/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v10, v0, v1, v13, v2}, Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;->decodeRow(ILcom/sec/android/app/camaftest/core/common/BitArray;[ILjava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    :try_end_0
    .catch Lcom/sec/android/app/camaftest/core/ReaderException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 99
    .local v11, "result":Lcom/sec/android/app/camaftest/core/Result;
    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/Result;->getBarcodeFormat()Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    move-result-object v14

    sget-object v15, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_13:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    if-ne v14, v15, :cond_1

    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/Result;->getText()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Ljava/lang/String;->charAt(I)C

    move-result v14

    const/16 v15, 0x30

    if-ne v14, v15, :cond_1

    const/4 v5, 0x1

    .line 100
    .local v5, "ean13MayBeUPCA":Z
    :goto_1
    if-nez p3, :cond_2

    const/4 v8, 0x0

    .line 101
    .local v8, "possibleFormats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    :goto_2
    if-eqz v8, :cond_0

    sget-object v14, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_A:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v8, v14}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    :cond_0
    const/4 v4, 0x1

    .line 103
    .local v4, "canReturnUPCA":Z
    :goto_3
    if-eqz v5, :cond_4

    if-eqz v4, :cond_4

    .line 105
    new-instance v12, Lcom/sec/android/app/camaftest/core/Result;

    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/Result;->getText()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/Result;->getRawBytes()[B

    move-result-object v15

    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/Result;->getResultPoints()[Lcom/sec/android/app/camaftest/core/ResultPoint;

    move-result-object v16

    sget-object v17, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_A:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v12, v14, v15, v0, v1}, Lcom/sec/android/app/camaftest/core/Result;-><init>(Ljava/lang/String;[B[Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/BarcodeFormat;)V

    .line 106
    .local v12, "resultUPCA":Lcom/sec/android/app/camaftest/core/Result;
    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/Result;->getResultMetadata()Ljava/util/Map;

    move-result-object v14

    invoke-virtual {v12, v14}, Lcom/sec/android/app/camaftest/core/Result;->putAllMetadata(Ljava/util/Map;)V

    .line 109
    .end local v12    # "resultUPCA":Lcom/sec/android/app/camaftest/core/Result;
    :goto_4
    return-object v12

    .line 77
    .end local v4    # "canReturnUPCA":Z
    .end local v5    # "ean13MayBeUPCA":Z
    .end local v8    # "possibleFormats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    .end local v11    # "result":Lcom/sec/android/app/camaftest/core/Result;
    :catch_0
    move-exception v9

    .line 73
    .local v9, "re":Lcom/sec/android/app/camaftest/core/ReaderException;
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 99
    .end local v9    # "re":Lcom/sec/android/app/camaftest/core/ReaderException;
    .restart local v11    # "result":Lcom/sec/android/app/camaftest/core/Result;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 100
    .restart local v5    # "ean13MayBeUPCA":Z
    :cond_2
    sget-object v14, Lcom/sec/android/app/camaftest/core/DecodeHintType;->POSSIBLE_FORMATS:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Collection;

    move-object v8, v14

    goto :goto_2

    .line 101
    .restart local v8    # "possibleFormats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    :cond_3
    const/4 v4, 0x0

    goto :goto_3

    .restart local v4    # "canReturnUPCA":Z
    :cond_4
    move-object v12, v11

    .line 109
    goto :goto_4

    .line 112
    .end local v4    # "canReturnUPCA":Z
    .end local v5    # "ean13MayBeUPCA":Z
    .end local v8    # "possibleFormats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    .end local v10    # "reader":Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;
    .end local v11    # "result":Lcom/sec/android/app/camaftest/core/Result;
    :cond_5
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v14

    throw v14
.end method

.method public reset()V
    .locals 4

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/MultiFormatUPCEANReader;->readers:[Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;

    .local v0, "arr$":[Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 118
    .local v3, "reader":Lcom/sec/android/app/camaftest/core/Reader;
    invoke-interface {v3}, Lcom/sec/android/app/camaftest/core/Reader;->reset()V

    .line 117
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 120
    .end local v3    # "reader":Lcom/sec/android/app/camaftest/core/Reader;
    :cond_0
    return-void
.end method
