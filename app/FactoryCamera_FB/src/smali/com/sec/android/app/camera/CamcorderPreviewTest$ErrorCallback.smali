.class public final Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;
.super Ljava/lang/Object;
.source "CamcorderPreviewTest.java"

# interfaces
.implements Landroid/hardware/Camera$ErrorCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/CamcorderPreviewTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ErrorCallback"
.end annotation


# static fields
.field private static final CAMERA_ERROR_DATALINE_FAIL:I = 0x7d0

.field private static final CAMERA_ERROR_DATALINE_SUCCESS:I = 0x7d1

.field private static final CAMERA_ERROR_MSG_NO_ERROR:I = 0x0

.field private static final CAMERA_ERROR_WRONG_FW:I = 0x3e8


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILandroid/hardware/Camera;)V
    .locals 3
    .param p1, "error"    # I
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 167
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    sparse-switch p1, :sswitch_data_0

    .line 192
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorCallback - CAMERA BAD["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    const v1, 0x7f0a000a

    # invokes: Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$200(Lcom/sec/android/app/camera/CamcorderPreviewTest;I)V

    .line 196
    :goto_0
    :sswitch_0
    return-void

    .line 173
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "ErrorCallback - CAMERA_ERROR_WRONG_FW"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    const v1, 0x7f0a000c

    # invokes: Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$200(Lcom/sec/android/app/camera/CamcorderPreviewTest;I)V

    goto :goto_0

    .line 178
    :sswitch_2
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "ErrorCallback - CAMERA_ERROR_DATALINE_SUCCESS"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 183
    :sswitch_3
    sget-object v0, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v1, "ErrorCallback - CAMERA_ERROR_DATALINE_FAIL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraType:I
    invoke-static {v0}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$400(Lcom/sec/android/app/camera/CamcorderPreviewTest;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    const v1, 0x7f0a0009

    # invokes: Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$200(Lcom/sec/android/app/camera/CamcorderPreviewTest;I)V

    goto :goto_0

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    const v1, 0x7f0a0008

    # invokes: Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$200(Lcom/sec/android/app/camera/CamcorderPreviewTest;I)V

    goto :goto_0

    .line 169
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x3e8 -> :sswitch_1
        0x7d0 -> :sswitch_3
        0x7d1 -> :sswitch_2
    .end sparse-switch
.end method
