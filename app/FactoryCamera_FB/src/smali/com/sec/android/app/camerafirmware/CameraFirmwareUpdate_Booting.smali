.class public Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;
.super Landroid/app/Activity;
.source "CameraFirmwareUpdate_Booting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CameraFirmwareBootChecker"


# instance fields
.field public Feature:Lcom/sec/android/app/camera/Feature;

.field private final TIME_COUNT:I

.field private TIME_COUNT_DEL:I

.field mCameraDevice:Landroid/hardware/Camera;

.field mCheckFileName:Ljava/lang/String;

.field mOpenCameraThread:Ljava/lang/Thread;

.field private mParameters:Landroid/hardware/Camera$Parameters;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWindowManager:Landroid/view/IWindowManager;

.field private progressDialog:Landroid/app/ProgressDialog;

.field timerCount:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;

.field versionIsp:Ljava/lang/String;

.field versionPhone:Ljava/lang/String;

.field versionSensor:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mCameraDevice:Landroid/hardware/Camera;

    .line 38
    const v0, 0x15f90

    iput v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->TIME_COUNT:I

    .line 39
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->TIME_COUNT_DEL:I

    .line 41
    const-string v0, "/sys/class/camera/rear/rear_checkfw"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mCheckFileName:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionIsp:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionPhone:Ljava/lang/String;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionSensor:Ljava/lang/String;

    .line 176
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$2;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mOpenCameraThread:Ljava/lang/Thread;

    .line 262
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->setBlockHold(Z)Z

    move-result v0

    return v0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 65
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 66
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x2000000a

    const-string v2, "TouchFirmware"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 68
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mWindowManager:Landroid/view/IWindowManager;

    .line 69
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 72
    return-void
.end method

.method private setBlockHold(Z)Z
    .locals 5
    .param p1, "bBlock"    # Z

    .prologue
    .line 159
    const-string v2, "CameraFirmwareBootChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setBlockHold - bBlock: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const/4 v0, 0x0

    .line 161
    .local v0, "bResult":Z
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mWindowManager:Landroid/view/IWindowManager;

    if-nez v2, :cond_0

    .line 162
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mWindowManager:Landroid/view/IWindowManager;

    .line 165
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mWindowManager:Landroid/view/IWindowManager;

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    invoke-interface {v2, v3, v4, p1}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z

    move-result v2

    or-int/2addr v0, v2

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mWindowManager:Landroid/view/IWindowManager;

    const/16 v3, 0x1a

    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    invoke-interface {v2, v3, v4, p1}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z

    move-result v2

    or-int/2addr v0, v2

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mWindowManager:Landroid/view/IWindowManager;

    const/16 v3, 0x52

    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    invoke-interface {v2, v3, v4, p1}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    or-int/2addr v0, v2

    .line 172
    :goto_0
    const-string v2, "CameraFirmwareBootChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setBlockHold - bResult: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    return v0

    .line 169
    :catch_0
    move-exception v1

    .line 170
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public SendFWUpdateInfo(Z)V
    .locals 5
    .param p1, "isfwupdate"    # Z

    .prologue
    .line 243
    const/4 v1, 0x0

    .line 245
    .local v1, "sendvalue":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 247
    const-string v1, "FWUpdateOk"

    .line 254
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "IS_FACTORYCAMERA_FW_UPDATE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 255
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "isfactorycamerafwupdate"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 257
    const-string v2, "CameraFirmwareBootChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SendFWUpdateInfo() - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    invoke-virtual {p0, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->sendBroadcast(Landroid/content/Intent;)V

    .line 260
    return-void

    .line 251
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const-string v1, "FWUpdateFail"

    goto :goto_0
.end method

.method protected hideUpdateProgress()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 143
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    .line 144
    return-void
.end method

.method public isFileExists()Z
    .locals 3

    .prologue
    .line 317
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mCheckFileName:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 318
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 319
    const-string v1, "CameraFirmwareBootChecker"

    const-string v2, "User Firmware file exists"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    const/4 v1, 0x1

    .line 323
    :goto_0
    return v1

    .line 322
    :cond_0
    const-string v1, "CameraFirmwareBootChecker"

    const-string v2, "User Firmware file does not exists!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isIspVersionHigher()Z
    .locals 13

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 415
    const-string v10, "CameraFirmwareBootChecker"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "isIspVersionHigher() - FW versionIsp ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionIsp:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " FW versionPhone ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionPhone:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " FW versionSensor ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionSensor:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    const/4 v3, 0x0

    .line 418
    .local v3, "versionIspSubstart":Ljava/lang/String;
    const/4 v5, 0x0

    .line 419
    .local v5, "versionPhoneSubstart":Ljava/lang/String;
    const/4 v7, 0x0

    .line 421
    .local v7, "versionSensorSubstart":Ljava/lang/String;
    const/4 v2, 0x0

    .line 422
    .local v2, "versionIspSubend":Ljava/lang/String;
    const/4 v4, 0x0

    .line 423
    .local v4, "versionPhoneSubend":Ljava/lang/String;
    const/4 v6, 0x0

    .line 425
    .local v6, "versionSensorSubend":Ljava/lang/String;
    const/4 v1, 0x0

    .line 426
    .local v1, "startFwVersion":Ljava/lang/String;
    const/4 v0, 0x0

    .line 428
    .local v0, "endFwVersion":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionSensor:Ljava/lang/String;

    const-string v11, "Sensor"

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionSubStartInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 429
    iget-object v10, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionSensor:Ljava/lang/String;

    const-string v11, "Sensor"

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionSubEndInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 431
    iget-object v10, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionPhone:Ljava/lang/String;

    const-string v11, "Phone"

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionSubStartInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 432
    iget-object v10, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionPhone:Ljava/lang/String;

    const-string v11, "Phone"

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionSubEndInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 434
    iget-object v10, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionIsp:Ljava/lang/String;

    const-string v11, "Isp"

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionSubStartInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 435
    iget-object v10, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionIsp:Ljava/lang/String;

    const-string v11, "Isp"

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionSubEndInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 438
    if-eqz v3, :cond_8

    const-string v10, "S13F0S"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 439
    if-eqz v4, :cond_0

    if-nez v6, :cond_4

    .line 440
    :cond_0
    if-eqz v4, :cond_3

    .line 441
    move-object v1, v5

    .line 442
    move-object v0, v4

    .line 457
    :cond_1
    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 458
    const-string v9, "CameraFirmwareBootChecker"

    const-string v10, "Another sensor module is detected"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    :cond_2
    :goto_1
    return v8

    .line 443
    :cond_3
    if-eqz v6, :cond_1

    .line 444
    move-object v1, v7

    .line 445
    move-object v0, v6

    goto :goto_0

    .line 448
    :cond_4
    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-ltz v10, :cond_5

    .line 449
    move-object v1, v7

    .line 450
    move-object v0, v6

    goto :goto_0

    .line 452
    :cond_5
    move-object v1, v5

    .line 453
    move-object v0, v4

    goto :goto_0

    .line 462
    :cond_6
    if-eqz v2, :cond_7

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-ltz v10, :cond_7

    .line 463
    const-string v8, "CameraFirmwareBootChecker"

    const-string v10, "versionIspSubend >=  not need update "

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 464
    goto :goto_1

    .line 467
    :cond_7
    const-string v9, "CameraFirmwareBootChecker"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "versionIspSubend < - need update  versionIspSubend = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 472
    :cond_8
    if-eqz v3, :cond_10

    const-string v10, "O13F0S"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 473
    if-eqz v4, :cond_9

    if-nez v6, :cond_c

    .line 474
    :cond_9
    if-eqz v4, :cond_b

    .line 475
    move-object v1, v5

    .line 476
    move-object v0, v4

    .line 491
    :cond_a
    :goto_2
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_e

    .line 492
    const-string v9, "CameraFirmwareBootChecker"

    const-string v10, "Another sensor module is detected"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 477
    :cond_b
    if-eqz v6, :cond_a

    .line 478
    move-object v1, v7

    .line 479
    move-object v0, v6

    goto :goto_2

    .line 482
    :cond_c
    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-ltz v10, :cond_d

    .line 483
    move-object v1, v7

    .line 484
    move-object v0, v6

    goto :goto_2

    .line 486
    :cond_d
    move-object v1, v5

    .line 487
    move-object v0, v4

    goto :goto_2

    .line 496
    :cond_e
    if-eqz v2, :cond_f

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-ltz v10, :cond_f

    .line 497
    const-string v8, "CameraFirmwareBootChecker"

    const-string v10, "versionIspSubend >=  not need update "

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 498
    goto :goto_1

    .line 501
    :cond_f
    const-string v9, "CameraFirmwareBootChecker"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "versionIspSubend < - need update  versionIspSubend = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 506
    :cond_10
    if-eqz v3, :cond_18

    const-string v10, "S13F0L"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_18

    .line 507
    if-eqz v4, :cond_11

    if-nez v6, :cond_14

    .line 508
    :cond_11
    if-eqz v4, :cond_13

    .line 509
    move-object v1, v5

    .line 510
    move-object v0, v4

    .line 525
    :cond_12
    :goto_3
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_16

    .line 526
    const-string v9, "CameraFirmwareBootChecker"

    const-string v10, "Another sensor module is detected"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 511
    :cond_13
    if-eqz v6, :cond_12

    .line 512
    move-object v1, v7

    .line 513
    move-object v0, v6

    goto :goto_3

    .line 516
    :cond_14
    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-ltz v10, :cond_15

    .line 517
    move-object v1, v7

    .line 518
    move-object v0, v6

    goto :goto_3

    .line 520
    :cond_15
    move-object v1, v5

    .line 521
    move-object v0, v4

    goto :goto_3

    .line 530
    :cond_16
    if-eqz v2, :cond_17

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-ltz v10, :cond_17

    .line 531
    const-string v8, "CameraFirmwareBootChecker"

    const-string v10, "versionIspSubend >=  not need update "

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 532
    goto/16 :goto_1

    .line 535
    :cond_17
    const-string v9, "CameraFirmwareBootChecker"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "versionIspSubend < - need update  versionIspSubend = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 540
    :cond_18
    if-eqz v3, :cond_2

    const-string v10, "OxxxxL"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 541
    if-eqz v4, :cond_19

    if-nez v6, :cond_1c

    .line 542
    :cond_19
    if-eqz v4, :cond_1b

    .line 543
    move-object v1, v5

    .line 544
    move-object v0, v4

    .line 559
    :cond_1a
    :goto_4
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1e

    .line 560
    const-string v9, "CameraFirmwareBootChecker"

    const-string v10, "Another sensor module is detected"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 545
    :cond_1b
    if-eqz v6, :cond_1a

    .line 546
    move-object v1, v7

    .line 547
    move-object v0, v6

    goto :goto_4

    .line 550
    :cond_1c
    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-ltz v10, :cond_1d

    .line 551
    move-object v1, v7

    .line 552
    move-object v0, v6

    goto :goto_4

    .line 554
    :cond_1d
    move-object v1, v5

    .line 555
    move-object v0, v4

    goto :goto_4

    .line 564
    :cond_1e
    if-eqz v2, :cond_1f

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-ltz v10, :cond_1f

    .line 565
    const-string v8, "CameraFirmwareBootChecker"

    const-string v10, "versionIspSubend >=  not need update "

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 566
    goto/16 :goto_1

    .line 569
    :cond_1f
    const-string v9, "CameraFirmwareBootChecker"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "versionIspSubend < - need update  versionIspSubend = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public isNeedUpdate()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 302
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->isFileExists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 303
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->loadVersionsFromCheckFile()Z

    move-result v1

    if-nez v1, :cond_1

    .line 313
    :cond_0
    :goto_0
    return v0

    .line 306
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->isIspVersionHigher()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 313
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadVersionsFromCheckFile()Z
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 328
    const/4 v3, 0x0

    .line 329
    .local v3, "fr":Ljava/io/FileReader;
    const/4 v0, 0x0

    .line 332
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v4, Ljava/io/FileReader;

    iget-object v8, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mCheckFileName:Ljava/lang/String;

    invoke-direct {v4, v8}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 333
    .end local v3    # "fr":Ljava/io/FileReader;
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 335
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 337
    .local v5, "fwInfo":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 338
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 340
    if-nez v5, :cond_3

    .line 341
    const-string v8, "CameraFirmwareBootChecker"

    const-string v9, "loadVersionsFromCheckFile() - can not get the FW info"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 366
    if-eqz v4, :cond_0

    .line 367
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 368
    :cond_0
    if-eqz v1, :cond_1

    .line 369
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_1
    :goto_0
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 375
    .end local v4    # "fr":Ljava/io/FileReader;
    .end local v5    # "fwInfo":Ljava/lang/String;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :cond_2
    :goto_1
    return v7

    .line 370
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fwInfo":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 371
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 345
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_4
    const-string v8, "CameraFirmwareBootChecker"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "loadVersionsFromCheckFile() - FW info["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    new-instance v6, Ljava/util/StringTokenizer;

    const-string v8, " "

    const/4 v9, 0x0

    invoke-direct {v6, v5, v8, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 348
    .local v6, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v8

    const/4 v9, 0x2

    if-ge v8, v9, :cond_6

    .line 349
    const-string v8, "CameraFirmwareBootChecker"

    const-string v9, "invalid FW Info!"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 366
    if-eqz v4, :cond_4

    .line 367
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 368
    :cond_4
    if-eqz v1, :cond_5

    .line 369
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_5
    :goto_2
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 372
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_1

    .line 370
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_1
    move-exception v2

    .line 371
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 353
    .end local v2    # "e":Ljava/io/IOException;
    :cond_6
    :try_start_6
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionIsp:Ljava/lang/String;

    .line 354
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionPhone:Ljava/lang/String;

    .line 355
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->versionSensor:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 359
    const/4 v7, 0x1

    .line 366
    if-eqz v4, :cond_7

    .line 367
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 368
    :cond_7
    if-eqz v1, :cond_8

    .line 369
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    :cond_8
    :goto_3
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 372
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_1

    .line 370
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_2
    move-exception v2

    .line 371
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 361
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "fr":Ljava/io/FileReader;
    .end local v5    # "fwInfo":Ljava/lang/String;
    .end local v6    # "st":Ljava/util/StringTokenizer;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_3
    move-exception v2

    .line 362
    .local v2, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_8
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 363
    const-string v8, "CameraFirmwareBootChecker"

    const-string v9, "file reading error"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 366
    if-eqz v3, :cond_9

    .line 367
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 368
    :cond_9
    if-eqz v0, :cond_2

    .line 369
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_1

    .line 370
    :catch_4
    move-exception v2

    .line 371
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 365
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 366
    :goto_5
    if-eqz v3, :cond_a

    .line 367
    :try_start_a
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V

    .line 368
    :cond_a
    if-eqz v0, :cond_b

    .line 369
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 372
    :cond_b
    :goto_6
    throw v7

    .line 370
    :catch_5
    move-exception v2

    .line 371
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 365
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_5

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_5

    .line 361
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    goto :goto_4
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const-string v1, "CameraFirmwareBootChecker"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-static {}, Lcom/sec/android/app/camera/CameraApp;->getAppFeature()Lcom/sec/android/app/camera/Feature;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->Feature:Lcom/sec/android/app/camera/Feature;

    .line 56
    const v1, 0x7f030003

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->setContentView(I)V

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 59
    .local v0, "win":Landroid/view/Window;
    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->init()V

    .line 62
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 237
    const-string v0, "CameraFirmwareBootChecker"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 239
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 75
    const-string v0, "CameraFirmwareBootChecker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyDown()-keyCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 89
    const-string v0, "CameraFirmwareBootChecker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyDown()-keyCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 203
    const-string v1, "CameraFirmwareBootChecker"

    const-string v2, "onPause"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 208
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mOpenCameraThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mCameraDevice:Landroid/hardware/Camera;

    if-nez v1, :cond_1

    .line 210
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 211
    :catch_0
    move-exception v0

    .line 212
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "CameraFirmwareBootChecker"

    const-string v2, "onPause() sleep error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 217
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v1, :cond_2

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    .line 219
    iput-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mCameraDevice:Landroid/hardware/Camera;

    .line 220
    const-string v1, "CameraFirmwareBootChecker"

    const-string v2, "onPause() closeCamera"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->hideUpdateProgress()V

    .line 225
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->setBlockHold(Z)Z

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->timerCount:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;

    if-eqz v1, :cond_3

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->timerCount:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;->cancel()V

    .line 229
    iput-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->timerCount:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;

    .line 232
    :cond_3
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 233
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 190
    const-string v0, "CameraFirmwareBootChecker"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->mOpenCameraThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 194
    new-instance v0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;

    const-wide/32 v2, 0x15f90

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->timerCount:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->timerCount:Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$CountDown;->start()Landroid/os/CountDownTimer;

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->showUpdateProgress()V

    .line 198
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->setBlockHold(Z)Z

    .line 199
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 200
    return-void
.end method

.method protected showUpdateProgress()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting$1;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    const-string v1, "Camera ISP Firmware updating...\nIt will take maximum 90 secs.\nDon\'t Press any key. and Do not Remove Battery Pack."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 128
    iget v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->TIME_COUNT_DEL:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->TIME_COUNT_DEL:I

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 134
    :cond_1
    return-void
.end method

.method public versionSubEndInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "info"    # Ljava/lang/String;
    .param p2, "log"    # Ljava/lang/String;

    .prologue
    .line 397
    const/4 v1, 0x0

    .line 399
    .local v1, "subendinfo":Ljava/lang/String;
    if-eqz p1, :cond_0

    const-string v3, "[0-9|a-z|A-Z|]*"

    invoke-virtual {p1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 400
    :cond_0
    const-string v3, "CameraFirmwareBootChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "versionSubEndInfo() - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    .line 411
    .end local v1    # "subendinfo":Ljava/lang/String;
    .local v2, "subendinfo":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 405
    .end local v2    # "subendinfo":Ljava/lang/String;
    .restart local v1    # "subendinfo":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x7

    const/16 v4, 0xb

    :try_start_0
    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    move-object v2, v1

    .line 411
    .end local v1    # "subendinfo":Ljava/lang/String;
    .restart local v2    # "subendinfo":Ljava/lang/String;
    goto :goto_0

    .line 406
    .end local v2    # "subendinfo":Ljava/lang/String;
    .restart local v1    # "subendinfo":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 407
    .local v0, "StringIndexOutOfBoundsException":Ljava/lang/Exception;
    const-string v3, "CameraFirmwareBootChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "versionSubEndInfo() StringIndexOutOfBounds() , "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public versionSubStartInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "info"    # Ljava/lang/String;
    .param p2, "log"    # Ljava/lang/String;

    .prologue
    .line 379
    const/4 v1, 0x0

    .line 381
    .local v1, "substartinfo":Ljava/lang/String;
    if-eqz p1, :cond_0

    const-string v3, "[0-9|a-z|A-Z|]*"

    invoke-virtual {p1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 382
    :cond_0
    const-string v3, "CameraFirmwareBootChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "versionSubStartInfo() - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    .line 393
    .end local v1    # "substartinfo":Ljava/lang/String;
    .local v2, "substartinfo":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 387
    .end local v2    # "substartinfo":Ljava/lang/String;
    .restart local v1    # "substartinfo":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    const/4 v4, 0x6

    :try_start_0
    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    move-object v2, v1

    .line 393
    .end local v1    # "substartinfo":Ljava/lang/String;
    .restart local v2    # "substartinfo":Ljava/lang/String;
    goto :goto_0

    .line 388
    .end local v2    # "substartinfo":Ljava/lang/String;
    .restart local v1    # "substartinfo":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 389
    .local v0, "StringIndexOutOfBoundsException":Ljava/lang/Exception;
    const-string v3, "CameraFirmwareBootChecker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "versionSubStartInfo() StringIndexOutOfBounds() , "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    const/4 v1, 0x0

    goto :goto_1
.end method
