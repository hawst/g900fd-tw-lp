.class Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;
.super Ljava/lang/Object;
.source "VideoPlayTest_SurfaceHolder.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 8
    .param p1, "bar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromuser"    # Z

    .prologue
    .line 357
    if-nez p3, :cond_1

    .line 368
    :cond_0
    :goto_0
    return-void

    .line 363
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    iget-object v4, v4, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    int-to-long v0, v4

    .line 364
    .local v0, "duration":J
    int-to-long v4, p2

    mul-long/2addr v4, v0

    const-wide/16 v6, 0x3e8

    div-long v2, v4, v6

    .line 365
    .local v2, "newposition":J
    iget-object v4, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    iget-object v4, v4, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    long-to-int v5, v2

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 366
    iget-object v4, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mCurrentTime:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$800(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 367
    iget-object v4, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mCurrentTime:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$800(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    long-to-int v6, v2

    # invokes: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->stringForTime(I)Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$900(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "bar"    # Landroid/widget/SeekBar;

    .prologue
    const v2, 0x7f080006

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mDragging:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$702(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;Z)Z

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->bPlayComplet:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$502(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;Z)Z

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPass:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$300(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mFail:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$400(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$000(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 354
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "bar"    # Landroid/widget/SeekBar;

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mDragging:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$702(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;Z)Z

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # invokes: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->setProgress()I
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$600(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)I

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;->this$0:Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    # getter for: Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->access$000(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 378
    return-void
.end method
