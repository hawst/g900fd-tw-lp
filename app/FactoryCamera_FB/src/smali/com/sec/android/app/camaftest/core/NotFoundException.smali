.class public final Lcom/sec/android/app/camaftest/core/NotFoundException;
.super Lcom/sec/android/app/camaftest/core/ReaderException;
.source "NotFoundException.java"


# static fields
.field private static final instance:Lcom/sec/android/app/camaftest/core/NotFoundException;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/sec/android/app/camaftest/core/NotFoundException;

    invoke-direct {v0}, Lcom/sec/android/app/camaftest/core/NotFoundException;-><init>()V

    sput-object v0, Lcom/sec/android/app/camaftest/core/NotFoundException;->instance:Lcom/sec/android/app/camaftest/core/NotFoundException;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/ReaderException;-><init>()V

    .line 31
    return-void
.end method

.method public static getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/camaftest/core/NotFoundException;->instance:Lcom/sec/android/app/camaftest/core/NotFoundException;

    return-object v0
.end method
