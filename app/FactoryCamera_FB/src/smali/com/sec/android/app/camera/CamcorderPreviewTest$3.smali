.class Lcom/sec/android/app/camera/CamcorderPreviewTest$3;
.super Ljava/lang/Object;
.source "CamcorderPreviewTest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/camera/CamcorderPreviewTest;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V
    .locals 0

    .prologue
    .line 952
    iput-object p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/16 v10, 0x9

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 956
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraType:I
    invoke-static {v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$400(Lcom/sec/android/app/camera/CamcorderPreviewTest;)I

    move-result v5

    invoke-static {v5}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v5

    # setter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v4, v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1002(Lcom/sec/android/app/camera/CamcorderPreviewTest;Landroid/hardware/Camera;)Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 963
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1000(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 964
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1000(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera;

    move-result-object v5

    invoke-virtual {v5}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    # setter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v4, v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1302(Lcom/sec/android/app/camera/CamcorderPreviewTest;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;

    .line 966
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    iget-object v4, v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v4, v4, Lcom/sec/android/app/camera/Feature;->SUPPORT_PHASE_AF:Z

    if-eqz v4, :cond_0

    .line 967
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v4

    const-string v5, "phase-af"

    const-string v6, "on"

    invoke-virtual {v4, v5, v6}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v4

    const-string v5, "auto"

    invoke-virtual {v4, v5}, Landroid/hardware/Camera$Parameters;->setAntibanding(Ljava/lang/String;)V

    .line 972
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    iget-object v4, v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v4, v4, Lcom/sec/android/app/camera/Feature;->CROP_CAMCORDER_PREVIEW_ENABLE:Z

    if-eqz v4, :cond_4

    .line 973
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v3

    .line 974
    .local v3, "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    if-nez v3, :cond_1

    .line 975
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "supported preview size is null"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    .end local v3    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :goto_0
    return-void

    .line 957
    :catch_0
    move-exception v0

    .line 958
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # setter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mchkopencamera:Z
    invoke-static {v4, v9}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1202(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z

    .line 959
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 960
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-virtual {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->finish()V

    goto :goto_0

    .line 978
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v3    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_1
    const/4 v2, 0x0

    .line 979
    .local v2, "previewsize":Landroid/hardware/Camera$Size;
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    .line 980
    .local v1, "picturesize":Landroid/hardware/Camera$Size;
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I
    invoke-static {v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1400(Lcom/sec/android/app/camera/CamcorderPreviewTest;)[[I

    move-result-object v5

    aget-object v5, v5, v8

    aget v5, v5, v8

    int-to-double v6, v5

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I
    invoke-static {v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1400(Lcom/sec/android/app/camera/CamcorderPreviewTest;)[[I

    move-result-object v5

    aget-object v5, v5, v8

    aget v5, v5, v9

    int-to-double v8, v5

    div-double/2addr v6, v8

    invoke-virtual {v4, v3, v6, v7}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->findOptimalPreviewSize(Ljava/util/List;D)Landroid/hardware/Camera$Size;

    move-result-object v2

    .line 982
    if-eqz v2, :cond_3

    .line 983
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "preview size is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " x "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v4

    iget v5, v2, Landroid/hardware/Camera$Size;->width:I

    iget v6, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v4, v5, v6}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 994
    .end local v1    # "picturesize":Landroid/hardware/Camera$Size;
    .end local v2    # "previewsize":Landroid/hardware/Camera$Size;
    .end local v3    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    iget-object v4, v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v4, v4, Lcom/sec/android/app/camera/Feature;->CAMCORDER_SUPPORT_WIDE_RESOLUTION:Z

    if-eqz v4, :cond_2

    .line 995
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v4

    const-string v5, "picture-size"

    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->cameraPicSize:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1500(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1000(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1006
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1000(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mErrorCallback:Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;
    invoke-static {v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1600(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Lcom/sec/android/app/camera/CamcorderPreviewTest$ErrorCallback;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    goto/16 :goto_0

    .line 986
    .restart local v1    # "picturesize":Landroid/hardware/Camera$Size;
    .restart local v2    # "previewsize":Landroid/hardware/Camera$Size;
    .restart local v3    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_3
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "optimal preview size is returned null"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 990
    .end local v1    # "picturesize":Landroid/hardware/Camera$Size;
    .end local v2    # "previewsize":Landroid/hardware/Camera$Size;
    .end local v3    # "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I
    invoke-static {v5}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1400(Lcom/sec/android/app/camera/CamcorderPreviewTest;)[[I

    move-result-object v5

    aget-object v5, v5, v8

    aget v5, v5, v8

    iget-object v6, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->camcorderpreview:[[I
    invoke-static {v6}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1400(Lcom/sec/android/app/camera/CamcorderPreviewTest;)[[I

    move-result-object v6

    aget-object v6, v6, v8

    aget v6, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    goto :goto_1

    .line 1001
    :catch_1
    move-exception v0

    .line 1002
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v4, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v5, "setParameter fail"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1003
    iget-object v4, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$3;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMainHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$300(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 999
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_2
    move-exception v4

    goto :goto_2
.end method
