.class Lcom/sec/android/app/camera/OISTest$2;
.super Ljava/lang/Object;
.source "OISTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/camera/OISTest;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/OISTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/OISTest;)V
    .locals 0

    .prologue
    .line 431
    iput-object p1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 436
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mCheckingState:I
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$1100(Lcom/sec/android/app/camera/OISTest;)I

    move-result v1

    if-nez v1, :cond_1

    .line 437
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$500(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 438
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$500(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera;->stopPreview()V

    .line 442
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    iget-object v2, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/OISTest;->access$500(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    # setter for: Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v1, v2}, Lcom/sec/android/app/camera/OISTest;->access$702(Lcom/sec/android/app/camera/OISTest;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;

    .line 444
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$700(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    const-string v2, "focus-mode"

    const-string v3, "continuous-picture"

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$700(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    const-string v2, "ois"

    const-string v3, "sine_y"

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mHelpText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$1200(Lcom/sec/android/app/camera/OISTest;)Landroid/widget/TextView;

    move-result-object v1

    const-string v2, "y test"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 449
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$500(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v2}, Lcom/sec/android/app/camera/OISTest;->access$700(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 457
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # setter for: Lcom/sec/android/app/camera/OISTest;->mCheckingState:I
    invoke-static {v1, v4}, Lcom/sec/android/app/camera/OISTest;->access$1102(Lcom/sec/android/app/camera/OISTest;I)I

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$500(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera;->startPreview()V

    .line 465
    :cond_0
    :goto_1
    return-void

    .line 452
    :catch_0
    move-exception v0

    .line 453
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v1, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v2, "setParameter fail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$300(Lcom/sec/android/app/camera/OISTest;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 459
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mCheckingState:I
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$1100(Lcom/sec/android/app/camera/OISTest;)I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 460
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$2;->this$0:Lcom/sec/android/app/camera/OISTest;

    const/4 v2, -0x1

    # invokes: Lcom/sec/android/app/camera/OISTest;->finishOISTest(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/camera/OISTest;->access$1300(Lcom/sec/android/app/camera/OISTest;I)V

    goto :goto_1

    .line 450
    :catch_1
    move-exception v1

    goto :goto_0
.end method
