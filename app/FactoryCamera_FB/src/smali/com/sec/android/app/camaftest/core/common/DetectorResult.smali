.class public Lcom/sec/android/app/camaftest/core/common/DetectorResult;
.super Ljava/lang/Object;
.source "DetectorResult.java"


# instance fields
.field private final bits:Lcom/sec/android/app/camaftest/core/common/BitMatrix;

.field private final points:[Lcom/sec/android/app/camaftest/core/ResultPoint;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camaftest/core/common/BitMatrix;[Lcom/sec/android/app/camaftest/core/ResultPoint;)V
    .locals 0
    .param p1, "bits"    # Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .param p2, "points"    # [Lcom/sec/android/app/camaftest/core/ResultPoint;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/camaftest/core/common/DetectorResult;->bits:Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/camaftest/core/common/DetectorResult;->points:[Lcom/sec/android/app/camaftest/core/ResultPoint;

    .line 36
    return-void
.end method


# virtual methods
.method public getBits()Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/common/DetectorResult;->bits:Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    return-object v0
.end method

.method public getPoints()[Lcom/sec/android/app/camaftest/core/ResultPoint;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/common/DetectorResult;->points:[Lcom/sec/android/app/camaftest/core/ResultPoint;

    return-object v0
.end method
