.class Lcom/sec/android/app/camera/OISTest$1;
.super Ljava/lang/Object;
.source "OISTest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/camera/OISTest;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/OISTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/OISTest;)V
    .locals 0

    .prologue
    .line 375
    iput-object p1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/16 v6, 0x9

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 379
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    iget-object v2, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->cameraType:I
    invoke-static {v2}, Lcom/sec/android/app/camera/OISTest;->access$400(Lcom/sec/android/app/camera/OISTest;)I

    move-result v2

    invoke-static {v2}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v2

    # setter for: Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v1, v2}, Lcom/sec/android/app/camera/OISTest;->access$502(Lcom/sec/android/app/camera/OISTest;Landroid/hardware/Camera;)Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 386
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$500(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera;

    move-result-object v1

    const/16 v2, 0x5a

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    iget-object v2, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/OISTest;->access$500(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    # setter for: Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v1, v2}, Lcom/sec/android/app/camera/OISTest;->access$702(Lcom/sec/android/app/camera/OISTest;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;

    .line 389
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    iget-object v1, v1, Lcom/sec/android/app/camera/OISTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->SUPPORT_PHASE_AF:Z

    if-eqz v1, :cond_0

    .line 390
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$700(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    const-string v2, "phase-af"

    const-string v3, "on"

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$700(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    const-string v2, "auto"

    invoke-virtual {v1, v2}, Landroid/hardware/Camera$Parameters;->setAntibanding(Ljava/lang/String;)V

    .line 395
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$700(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->camcorderpreview:[[I
    invoke-static {v2}, Lcom/sec/android/app/camera/OISTest;->access$800(Lcom/sec/android/app/camera/OISTest;)[[I

    move-result-object v2

    aget-object v2, v2, v4

    aget v2, v2, v4

    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->camcorderpreview:[[I
    invoke-static {v3}, Lcom/sec/android/app/camera/OISTest;->access$800(Lcom/sec/android/app/camera/OISTest;)[[I

    move-result-object v3

    aget-object v3, v3, v4

    aget v3, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 398
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    iget-object v1, v1, Lcom/sec/android/app/camera/OISTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CAMCORDER_SUPPORT_WIDE_RESOLUTION:Z

    if-eqz v1, :cond_1

    .line 399
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$700(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v1

    const-string v2, "picture-size"

    iget-object v3, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->cameraPicSize:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/camera/OISTest;->access$900(Lcom/sec/android/app/camera/OISTest;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$500(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mParameters:Landroid/hardware/Camera$Parameters;
    invoke-static {v2}, Lcom/sec/android/app/camera/OISTest;->access$700(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 410
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$500(Lcom/sec/android/app/camera/OISTest;)Landroid/hardware/Camera;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mErrorCallback:Lcom/sec/android/app/camera/OISTest$ErrorCallback;
    invoke-static {v2}, Lcom/sec/android/app/camera/OISTest;->access$1000(Lcom/sec/android/app/camera/OISTest;)Lcom/sec/android/app/camera/OISTest$ErrorCallback;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    .line 411
    :goto_1
    return-void

    .line 380
    :catch_0
    move-exception v0

    .line 381
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # setter for: Lcom/sec/android/app/camera/OISTest;->mchkopencamera:Z
    invoke-static {v1, v5}, Lcom/sec/android/app/camera/OISTest;->access$602(Lcom/sec/android/app/camera/OISTest;Z)Z

    .line 382
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$300(Lcom/sec/android/app/camera/OISTest;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 383
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/OISTest;->finish()V

    goto :goto_1

    .line 405
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 406
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v1, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v2, "setParameter fail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$1;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/camera/OISTest;->access$300(Lcom/sec/android/app/camera/OISTest;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 403
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_2
    move-exception v1

    goto :goto_0
.end method
