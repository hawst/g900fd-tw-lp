.class public final Lcom/sec/android/app/camaftest/CaptureActivity;
.super Landroid/app/Activity;
.source "CaptureActivity.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camaftest/CaptureActivity$1;
    }
.end annotation


# static fields
.field private static final BULK_MODE_SCAN_DELAY_MS:J = 0x3e8L

.field private static final DEFAULT_INTENT_RESULT_DURATION_MS:J = 0x5dcL

.field private static final DIRECTORY_CAMERA_DATA:Ljava/lang/String;

.field private static final DIRECTORY_CAMERA_SDCARD:Ljava/lang/String;

.field private static final DISPLAYABLE_METADATA_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/app/camaftest/core/ResultMetadataType;",
            ">;"
        }
    .end annotation
.end field

.field private static final PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.camaftest"

.field private static final PREPARING:J = -0x2L

.field private static final PRODUCT_SEARCH_URL_PREFIX:Ljava/lang/String; = "http://www.google"

.field private static final PRODUCT_SEARCH_URL_SUFFIX:Ljava/lang/String; = "/m/products/scan"

.field private static final RETURN_CODE_PLACEHOLDER:Ljava/lang/String; = "{CODE}"

.field private static final RETURN_URL_PARAM:Ljava/lang/String; = "ret"

.field private static final TAG:Ljava/lang/String;

.field private static final UNAVAILABLE:J = -0x1L

.field private static final UNKNOWN_SIZE:J = -0x3L

.field private static final ZXING_URLS:[Ljava/lang/String;


# instance fields
.field private bUseSdcard:Z

.field private beepManager:Lcom/sec/android/app/camaftest/BeepManager;

.field private cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

.field private characterSet:Ljava/lang/String;

.field private decodeFormats:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/camaftest/core/BarcodeFormat;",
            ">;"
        }
    .end annotation
.end field

.field private handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

.field private hasSurface:Z

.field private inactivityTimer:Lcom/sec/android/app/camaftest/InactivityTimer;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mDoneButton:Landroid/widget/Button;

.field private mFailButton:Landroid/widget/Button;

.field private mIsShowResultView:Z

.field private mLastSucceededFilePath:Ljava/lang/String;

.field private mScanButton:Landroid/widget/Button;

.field private resultView:Landroid/view/View;

.field private returnUrlTemplate:Ljava/lang/String;

.field private source:Lcom/sec/android/app/camaftest/IntentSource;

.field private sourceUrl:Ljava/lang/String;

.field private statusView:Landroid/widget/TextView;

.field private viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 97
    const-class v0, Lcom/sec/android/app/camaftest/CaptureActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    .line 105
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "http://zxing.appspot.com/scan"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "zxing://scan/"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/camaftest/CaptureActivity;->ZXING_URLS:[Ljava/lang/String;

    .line 109
    sget-object v0, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->ISSUE_NUMBER:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    sget-object v1, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->SUGGESTED_PRICE:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    sget-object v2, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->ERROR_CORRECTION_LEVEL:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    sget-object v3, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->POSSIBLE_COUNTRY:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-static {v0, v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/CaptureActivity;->DISPLAYABLE_METADATA_TYPES:Ljava/util/Set;

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/log/DCIM/Camera"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/CaptureActivity;->DIRECTORY_CAMERA_DATA:Ljava/lang/String;

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Camera"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/CaptureActivity;->DIRECTORY_CAMERA_SDCARD:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 119
    iput-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mScanButton:Landroid/widget/Button;

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->bUseSdcard:Z

    .line 137
    iput-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mLastSucceededFilePath:Ljava/lang/String;

    return-void
.end method

.method private displayFrameworkBugMessageAndExit()V
    .locals 3

    .prologue
    .line 660
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 661
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const/high16 v1, 0x7f0a0000

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 662
    const v1, 0x7f0a0060

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 663
    const v1, 0x7f0a003b

    new-instance v2, Lcom/sec/android/app/camaftest/FinishListener;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camaftest/FinishListener;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 664
    new-instance v1, Lcom/sec/android/app/camaftest/FinishListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camaftest/FinishListener;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 665
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 666
    return-void
.end method

.method private static drawLine(Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/ResultPoint;)V
    .locals 6
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "paint"    # Landroid/graphics/Paint;
    .param p2, "a"    # Lcom/sec/android/app/camaftest/core/ResultPoint;
    .param p3, "b"    # Lcom/sec/android/app/camaftest/core/ResultPoint;

    .prologue
    .line 469
    invoke-virtual {p2}, Lcom/sec/android/app/camaftest/core/ResultPoint;->getX()F

    move-result v1

    invoke-virtual {p2}, Lcom/sec/android/app/camaftest/core/ResultPoint;->getY()F

    move-result v2

    invoke-virtual {p3}, Lcom/sec/android/app/camaftest/core/ResultPoint;->getX()F

    move-result v3

    invoke-virtual {p3}, Lcom/sec/android/app/camaftest/core/ResultPoint;->getY()F

    move-result v4

    move-object v0, p0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 470
    return-void
.end method

.method private drawResultPoints(Landroid/graphics/Bitmap;Lcom/sec/android/app/camaftest/core/Result;)V
    .locals 13
    .param p1, "barcode"    # Landroid/graphics/Bitmap;
    .param p2, "rawResult"    # Lcom/sec/android/app/camaftest/core/Result;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x2

    .line 439
    invoke-virtual {p2}, Lcom/sec/android/app/camaftest/core/Result;->getResultPoints()[Lcom/sec/android/app/camaftest/core/ResultPoint;

    move-result-object v7

    .line 440
    .local v7, "points":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    if-eqz v7, :cond_0

    array-length v8, v7

    if-lez v8, :cond_0

    .line 441
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 442
    .local v2, "canvas":Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 443
    .local v5, "paint":Landroid/graphics/Paint;
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080024

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 444
    const/high16 v8, 0x40400000    # 3.0f

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 445
    sget-object v8, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 446
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    add-int/lit8 v9, v9, -0x2

    invoke-direct {v1, v10, v10, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 447
    .local v1, "border":Landroid/graphics/Rect;
    invoke-virtual {v2, v1, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 449
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080026

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 450
    array-length v8, v7

    if-ne v8, v10, :cond_1

    .line 451
    const/high16 v8, 0x40800000    # 4.0f

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 452
    aget-object v8, v7, v11

    aget-object v9, v7, v12

    invoke-static {v2, v5, v8, v9}, Lcom/sec/android/app/camaftest/CaptureActivity;->drawLine(Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/ResultPoint;)V

    .line 466
    .end local v1    # "border":Landroid/graphics/Rect;
    .end local v2    # "canvas":Landroid/graphics/Canvas;
    .end local v5    # "paint":Landroid/graphics/Paint;
    :cond_0
    :goto_0
    return-void

    .line 453
    .restart local v1    # "border":Landroid/graphics/Rect;
    .restart local v2    # "canvas":Landroid/graphics/Canvas;
    .restart local v5    # "paint":Landroid/graphics/Paint;
    :cond_1
    array-length v8, v7

    const/4 v9, 0x4

    if-ne v8, v9, :cond_3

    invoke-virtual {p2}, Lcom/sec/android/app/camaftest/core/Result;->getBarcodeFormat()Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_A:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    if-eq v8, v9, :cond_2

    invoke-virtual {p2}, Lcom/sec/android/app/camaftest/core/Result;->getBarcodeFormat()Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_13:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    if-ne v8, v9, :cond_3

    .line 457
    :cond_2
    aget-object v8, v7, v11

    aget-object v9, v7, v12

    invoke-static {v2, v5, v8, v9}, Lcom/sec/android/app/camaftest/CaptureActivity;->drawLine(Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/ResultPoint;)V

    .line 458
    aget-object v8, v7, v10

    const/4 v9, 0x3

    aget-object v9, v7, v9

    invoke-static {v2, v5, v8, v9}, Lcom/sec/android/app/camaftest/CaptureActivity;->drawLine(Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/ResultPoint;)V

    goto :goto_0

    .line 460
    :cond_3
    const/high16 v8, 0x41200000    # 10.0f

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 461
    move-object v0, v7

    .local v0, "arr$":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v6, v0, v3

    .line 462
    .local v6, "point":Lcom/sec/android/app/camaftest/core/ResultPoint;
    invoke-virtual {v6}, Lcom/sec/android/app/camaftest/core/ResultPoint;->getX()F

    move-result v8

    invoke-virtual {v6}, Lcom/sec/android/app/camaftest/core/ResultPoint;->getY()F

    move-result v9

    invoke-virtual {v2, v8, v9, v5}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 461
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private static getAvailableSpaceSd()J
    .locals 10

    .prologue
    const-wide/16 v2, -0x1

    .line 748
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v5

    .line 749
    .local v5, "state":Ljava/lang/String;
    const-string v6, "checking"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 750
    const-wide/16 v2, -0x2

    .line 770
    :cond_0
    :goto_0
    return-wide v2

    .line 752
    :cond_1
    const-string v6, "mounted"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 756
    new-instance v0, Ljava/io/File;

    sget-object v6, Lcom/sec/android/app/camaftest/CaptureActivity;->DIRECTORY_CAMERA_SDCARD:Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 757
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 758
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 763
    :try_start_0
    new-instance v4, Landroid/os/StatFs;

    sget-object v6, Lcom/sec/android/app/camaftest/CaptureActivity;->DIRECTORY_CAMERA_SDCARD:Ljava/lang/String;

    invoke-direct {v4, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 764
    .local v4, "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v8

    int-to-long v8, v8

    mul-long v2, v6, v8

    .line 765
    .local v2, "space":J
    sget-object v6, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAvailableSpaceSd : space ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 767
    .end local v2    # "space":J
    .end local v4    # "stat":Landroid/os/StatFs;
    :catch_0
    move-exception v1

    .line 768
    .local v1, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    const-string v7, "Fail to access external storage"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 770
    const-wide/16 v2, -0x3

    goto :goto_0
.end method

.method private handleDecodeExternally(Lcom/sec/android/app/camaftest/core/Result;Lcom/sec/android/app/camaftest/result/ResultHandler;Landroid/graphics/Bitmap;)V
    .locals 16
    .param p1, "rawResult"    # Lcom/sec/android/app/camaftest/core/Result;
    .param p2, "resultHandler"    # Lcom/sec/android/app/camaftest/result/ResultHandler;
    .param p3, "barcode"    # Landroid/graphics/Bitmap;

    .prologue
    .line 562
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Lcom/sec/android/app/camaftest/ViewfinderView;->drawResultBitmap(Landroid/graphics/Bitmap;)V

    .line 567
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->statusView:Landroid/widget/TextView;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;->getDisplayTitle()I

    move-result v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/camaftest/CaptureActivity;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 569
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->source:Lcom/sec/android/app/camaftest/IntentSource;

    sget-object v14, Lcom/sec/android/app/camaftest/IntentSource;->NATIVE_APP_INTENT:Lcom/sec/android/app/camaftest/IntentSource;

    if-ne v13, v14, :cond_5

    .line 572
    new-instance v8, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v8, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 573
    .local v8, "intent":Landroid/content/Intent;
    const/high16 v13, 0x80000

    invoke-virtual {v8, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 574
    const-string v13, "SCAN_RESULT"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/camaftest/core/Result;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 575
    const-string v13, "SCAN_RESULT_FORMAT"

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/camaftest/core/Result;->getBarcodeFormat()Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 576
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/camaftest/core/Result;->getRawBytes()[B

    move-result-object v11

    .line 577
    .local v11, "rawBytes":[B
    if-eqz v11, :cond_0

    array-length v13, v11

    if-lez v13, :cond_0

    .line 578
    const-string v13, "SCAN_RESULT_BYTES"

    invoke-virtual {v8, v13, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 580
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/camaftest/core/Result;->getResultMetadata()Ljava/util/Map;

    move-result-object v9

    .line 581
    .local v9, "metadata":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/ResultMetadataType;*>;"
    if-eqz v9, :cond_3

    .line 582
    sget-object v13, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->ORIENTATION:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-interface {v9, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 583
    .local v10, "orientation":Ljava/lang/Integer;
    if-eqz v10, :cond_1

    .line 584
    const-string v13, "SCAN_RESULT_ORIENTATION"

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-virtual {v8, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 586
    :cond_1
    sget-object v13, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->ERROR_CORRECTION_LEVEL:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-interface {v9, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 587
    .local v4, "ecLevel":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 588
    const-string v13, "SCAN_RESULT_ERROR_CORRECTION_LEVEL"

    invoke-virtual {v8, v13, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 590
    :cond_2
    sget-object v13, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->BYTE_SEGMENTS:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-interface {v9, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 591
    .local v2, "byteSegments":Ljava/lang/Iterable;, "Ljava/lang/Iterable<[B>;"
    if-eqz v2, :cond_3

    .line 592
    const/4 v6, 0x0

    .line 593
    .local v6, "i":I
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 594
    .local v1, "byteSegment":[B
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "SCAN_RESULT_BYTE_SEGMENTS_"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 595
    add-int/lit8 v6, v6, 0x1

    .line 596
    goto :goto_0

    .line 599
    .end local v1    # "byteSegment":[B
    .end local v2    # "byteSegments":Ljava/lang/Iterable;, "Ljava/lang/Iterable<[B>;"
    .end local v4    # "ecLevel":Ljava/lang/String;
    .end local v6    # "i":I
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v10    # "orientation":Ljava/lang/Integer;
    :cond_3
    const v13, 0x7f090007

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v8}, Lcom/sec/android/app/camaftest/CaptureActivity;->sendReplyMessage(ILjava/lang/Object;)V

    .line 625
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v9    # "metadata":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/ResultMetadataType;*>;"
    .end local v11    # "rawBytes":[B
    :cond_4
    :goto_1
    return-void

    .line 601
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->source:Lcom/sec/android/app/camaftest/IntentSource;

    sget-object v14, Lcom/sec/android/app/camaftest/IntentSource;->PRODUCT_SEARCH_LINK:Lcom/sec/android/app/camaftest/IntentSource;

    if-ne v13, v14, :cond_6

    .line 605
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->sourceUrl:Ljava/lang/String;

    const-string v14, "/scan"

    invoke-virtual {v13, v14}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 606
    .local v5, "end":I
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->sourceUrl:Ljava/lang/String;

    const/4 v15, 0x0

    invoke-virtual {v14, v15, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "?q="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;->getDisplayContents()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "&source=zxing"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 607
    .local v12, "replyURL":Ljava/lang/String;
    const v13, 0x7f090004

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v12}, Lcom/sec/android/app/camaftest/CaptureActivity;->sendReplyMessage(ILjava/lang/Object;)V

    goto :goto_1

    .line 609
    .end local v5    # "end":I
    .end local v12    # "replyURL":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->source:Lcom/sec/android/app/camaftest/IntentSource;

    sget-object v14, Lcom/sec/android/app/camaftest/IntentSource;->ZXING_LINK:Lcom/sec/android/app/camaftest/IntentSource;

    if-ne v13, v14, :cond_4

    .line 613
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->returnUrlTemplate:Ljava/lang/String;

    if-eqz v13, :cond_4

    .line 614
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;->getDisplayContents()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 616
    .local v3, "codeReplacement":Ljava/lang/String;
    :try_start_0
    const-string v13, "UTF-8"

    invoke-static {v3, v13}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 620
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->returnUrlTemplate:Ljava/lang/String;

    const-string v14, "{CODE}"

    invoke-virtual {v13, v14, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 621
    .restart local v12    # "replyURL":Ljava/lang/String;
    const v13, 0x7f090004

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v12}, Lcom/sec/android/app/camaftest/CaptureActivity;->sendReplyMessage(ILjava/lang/Object;)V

    goto :goto_1

    .line 617
    .end local v12    # "replyURL":Ljava/lang/String;
    :catch_0
    move-exception v13

    goto :goto_2
.end method

.method private handleDecodeFailed()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 473
    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->statusView:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 474
    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/camaftest/ViewfinderView;->setVisibility(I)V

    .line 476
    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 477
    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mFailButton:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 478
    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->resultView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 480
    const v2, 0x7f09002a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 481
    .local v0, "decodeFailView":Landroid/view/ViewGroup;
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 483
    const v2, 0x7f09002b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 484
    .local v1, "decodeSucceedView":Landroid/view/ViewGroup;
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 485
    return-void
.end method

.method private handleDecodeInternally(Lcom/sec/android/app/camaftest/core/Result;Lcom/sec/android/app/camaftest/result/ResultHandler;Landroid/graphics/Bitmap;)V
    .locals 26
    .param p1, "rawResult"    # Lcom/sec/android/app/camaftest/core/Result;
    .param p2, "resultHandler"    # Lcom/sec/android/app/camaftest/result/ResultHandler;
    .param p3, "barcode"    # Landroid/graphics/Bitmap;

    .prologue
    .line 489
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->statusView:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setVisibility(I)V

    .line 490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Lcom/sec/android/app/camaftest/ViewfinderView;->setVisibility(I)V

    .line 492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->mDoneButton:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 493
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->mFailButton:Landroid/widget/Button;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setVisibility(I)V

    .line 494
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->resultView:Landroid/view/View;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setVisibility(I)V

    .line 496
    const v22, 0x7f09002a

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 497
    .local v6, "decodeFailView":Landroid/view/ViewGroup;
    const/16 v22, 0x8

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 499
    const v22, 0x7f09002b

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 500
    .local v7, "decodeSucceedView":Landroid/view/ViewGroup;
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 502
    const v22, 0x7f09002c

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 503
    .local v4, "barcodeImageView":Landroid/widget/ImageView;
    if-nez p3, :cond_1

    .line 504
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f020011

    invoke-static/range {v22 .. v23}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 510
    :goto_0
    const v22, 0x7f09002e

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 511
    .local v10, "formatTextView":Landroid/widget/TextView;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/camaftest/core/Result;->getBarcodeFormat()Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 513
    const v22, 0x7f090030

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 514
    .local v21, "typeTextView":Landroid/widget/TextView;
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;->getType()Lcom/sec/android/app/camaftest/core/client/result/ParsedResultType;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/camaftest/core/client/result/ParsedResultType;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 516
    const/16 v22, 0x3

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v12

    .line 517
    .local v12, "formatter":Ljava/text/DateFormat;
    new-instance v22, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/camaftest/core/Result;->getTimestamp()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    .line 518
    .local v11, "formattedTime":Ljava/lang/String;
    const v22, 0x7f090032

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 519
    .local v20, "timeTextView":Landroid/widget/TextView;
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 521
    const v22, 0x7f090034

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 522
    .local v14, "metaTextView":Landroid/widget/TextView;
    const v22, 0x7f090033

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .line 523
    .local v15, "metaTextViewLabel":Landroid/view/View;
    const/16 v22, 0x8

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 524
    const/16 v22, 0x8

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    .line 525
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/camaftest/core/Result;->getResultMetadata()Ljava/util/Map;

    move-result-object v16

    .line 526
    .local v16, "metadata":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;>;"
    if-eqz v16, :cond_3

    .line 527
    new-instance v17, Ljava/lang/StringBuilder;

    const/16 v22, 0x14

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 528
    .local v17, "metadataText":Ljava/lang/StringBuilder;
    invoke-interface/range {v16 .. v16}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_2

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 529
    .local v9, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;>;"
    sget-object v22, Lcom/sec/android/app/camaftest/CaptureActivity;->DISPLAYABLE_METADATA_TYPES:Ljava/util/Set;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v23

    invoke-interface/range {v22 .. v23}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 530
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0xa

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 507
    .end local v9    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;>;"
    .end local v10    # "formatTextView":Landroid/widget/TextView;
    .end local v11    # "formattedTime":Ljava/lang/String;
    .end local v12    # "formatter":Ljava/text/DateFormat;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "metaTextView":Landroid/widget/TextView;
    .end local v15    # "metaTextViewLabel":Landroid/view/View;
    .end local v16    # "metadata":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;>;"
    .end local v17    # "metadataText":Ljava/lang/StringBuilder;
    .end local v20    # "timeTextView":Landroid/widget/TextView;
    .end local v21    # "typeTextView":Landroid/widget/TextView;
    :cond_1
    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 533
    .restart local v10    # "formatTextView":Landroid/widget/TextView;
    .restart local v11    # "formattedTime":Ljava/lang/String;
    .restart local v12    # "formatter":Ljava/text/DateFormat;
    .restart local v13    # "i$":Ljava/util/Iterator;
    .restart local v14    # "metaTextView":Landroid/widget/TextView;
    .restart local v15    # "metaTextViewLabel":Landroid/view/View;
    .restart local v16    # "metadata":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;>;"
    .restart local v17    # "metadataText":Ljava/lang/StringBuilder;
    .restart local v20    # "timeTextView":Landroid/widget/TextView;
    .restart local v21    # "typeTextView":Landroid/widget/TextView;
    :cond_2
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->length()I

    move-result v22

    if-lez v22, :cond_3

    .line 534
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->length()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 535
    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 536
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 537
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    .line 541
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v17    # "metadataText":Ljava/lang/StringBuilder;
    :cond_3
    const v22, 0x7f090035

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 542
    .local v5, "contentsTextView":Landroid/widget/TextView;
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;->getDisplayContents()Ljava/lang/CharSequence;

    move-result-object v8

    .line 543
    .local v8, "displayContents":Ljava/lang/CharSequence;
    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 545
    const/16 v22, 0x16

    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    move-result v23

    div-int/lit8 v23, v23, 0x4

    rsub-int/lit8 v23, v23, 0x20

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 546
    .local v18, "scaledSize":I
    const/16 v22, 0x2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 548
    const v22, 0x7f090036

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 549
    .local v19, "supplementTextView":Landroid/widget/TextView;
    const-string v22, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 550
    const/16 v22, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 551
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v22

    const-string v23, "preferences_supplemental"

    const/16 v24, 0x1

    invoke-interface/range {v22 .. v24}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 553
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;->getResult()Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    move-object/from16 v23, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    move-object/from16 v3, p0

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/camaftest/result/supplement/SupplementalInfoRetriever;->maybeInvokeRetrieval(Landroid/widget/TextView;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;Landroid/os/Handler;Landroid/content/Context;)V

    .line 558
    :cond_4
    return-void
.end method

.method private initCamera(Landroid/view/SurfaceHolder;)V
    .locals 6
    .param p1, "surfaceHolder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 640
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/camaftest/camera/CameraManager;->openDriver(Landroid/view/SurfaceHolder;)V

    .line 642
    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    if-nez v2, :cond_0

    .line 643
    new-instance v2, Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    iget-object v3, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->decodeFormats:Ljava/util/Collection;

    iget-object v4, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->characterSet:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/sec/android/app/camaftest/CaptureActivityHandler;-><init>(Lcom/sec/android/app/camaftest/CaptureActivity;Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/camaftest/camera/CameraManager;)V

    iput-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 656
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->refreshScanButton()V

    .line 657
    return-void

    .line 646
    :catch_0
    move-exception v1

    .line 647
    .local v1, "ioe":Ljava/io/IOException;
    sget-object v2, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 648
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->displayFrameworkBugMessageAndExit()V

    goto :goto_0

    .line 649
    .end local v1    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 652
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v2, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    const-string v3, "Unexpected error initializing camera"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 653
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->displayFrameworkBugMessageAndExit()V

    goto :goto_0
.end method

.method private static isZXingURL(Ljava/lang/String;)Z
    .locals 6
    .param p0, "dataString"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 289
    if-nez p0, :cond_1

    .line 297
    :cond_0
    :goto_0
    return v4

    .line 292
    :cond_1
    sget-object v0, Lcom/sec/android/app/camaftest/CaptureActivity;->ZXING_URLS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 293
    .local v3, "url":Ljava/lang/String;
    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 294
    const/4 v4, 0x1

    goto :goto_0

    .line 292
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private refreshScanButton()V
    .locals 4

    .prologue
    const/16 v3, 0x14

    const/4 v2, 0x0

    .line 829
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mScanButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 830
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/camera/CameraManager;->setPreviewOrientation()V

    .line 831
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/camera/CameraManager;->getPreviewOrientation()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 849
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mScanButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 850
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/camera/CameraManager;->getFramingRectInPreview()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camaftest/ViewfinderView;->invalidate(Landroid/graphics/Rect;)V

    .line 851
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/camera/CameraManager;->getLastOrientation()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camaftest/camera/CameraManager;->setPreviousOrientation(I)V

    .line 852
    return-void

    .line 833
    :sswitch_0
    invoke-virtual {v0, v2, v2, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 834
    const/16 v1, 0x15

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0

    .line 837
    :sswitch_1
    invoke-virtual {v0, v2, v2, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 838
    const/16 v1, 0x51

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0

    .line 841
    :sswitch_2
    invoke-virtual {v0, v3, v2, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 842
    const/16 v1, 0x13

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0

    .line 845
    :sswitch_3
    invoke-virtual {v0, v2, v3, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 846
    const/16 v1, 0x31

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0

    .line 831
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method private resetStatusView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->resultView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->statusView:Landroid/widget/TextView;

    const v1, 0x7f0a0064

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->statusView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/camaftest/ViewfinderView;->setVisibility(I)V

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mScanButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 681
    iput-boolean v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mIsShowResultView:Z

    .line 682
    return-void
.end method

.method private sendReplyMessage(ILjava/lang/Object;)V
    .locals 8
    .param p1, "id"    # I
    .param p2, "arg"    # Ljava/lang/Object;

    .prologue
    .line 628
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    invoke-static {v1, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 629
    .local v0, "message":Landroid/os/Message;
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "RESULT_DISPLAY_DURATION_MS"

    const-wide/16 v6, 0x5dc

    invoke-virtual {v1, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 631
    .local v2, "resultDurationMS":J
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 632
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 636
    :goto_0
    return-void

    .line 634
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public static setSystemKeyBlock(Landroid/content/ComponentName;I)V
    .locals 4
    .param p0, "componentName"    # Landroid/content/ComponentName;
    .param p1, "keyCode"    # I

    .prologue
    .line 855
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 858
    .local v1, "wm":Landroid/view/IWindowManager;
    const/4 v2, 0x1

    :try_start_0
    invoke-interface {v1, p1, p0, v2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 862
    :goto_0
    return-void

    .line 859
    :catch_0
    move-exception v0

    .line 860
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    const-string v3, "setSystemKeyBlock exception!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private writeImage(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 8
    .param p1, "directory"    # Ljava/lang/String;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "data"    # [B

    .prologue
    .line 774
    const/4 v3, 0x0

    .line 776
    .local v3, "outputStream":Ljava/io/OutputStream;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mLastSucceededFilePath:Ljava/lang/String;

    .line 779
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 780
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 781
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 782
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 783
    .local v5, "redir":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 785
    .end local v5    # "redir":Ljava/io/File;
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_1

    .line 786
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 787
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    .local v2, "file":Ljava/io/File;
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 789
    .end local v3    # "outputStream":Ljava/io/OutputStream;
    .local v4, "outputStream":Ljava/io/OutputStream;
    :try_start_1
    invoke-virtual {v4, p3}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 796
    :try_start_2
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 801
    .end local v0    # "dir":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v3    # "outputStream":Ljava/io/OutputStream;
    :goto_0
    return-void

    .line 797
    .end local v3    # "outputStream":Ljava/io/OutputStream;
    .restart local v0    # "dir":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    :catch_0
    move-exception v6

    move-object v3, v4

    .line 800
    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v3    # "outputStream":Ljava/io/OutputStream;
    goto :goto_0

    .line 790
    .end local v0    # "dir":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    :catch_1
    move-exception v1

    .line 791
    .local v1, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    sget-object v6, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    invoke-static {v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 796
    :try_start_4
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 797
    :catch_2
    move-exception v6

    goto :goto_0

    .line 792
    .end local v1    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v1

    .line 793
    .local v1, "ex":Ljava/io/IOException;
    :goto_2
    :try_start_5
    sget-object v6, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    invoke-static {v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 796
    :try_start_6
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 797
    :catch_4
    move-exception v6

    goto :goto_0

    .line 795
    .end local v1    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 796
    :goto_3
    :try_start_7
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 799
    :goto_4
    throw v6

    .line 797
    :catch_5
    move-exception v7

    goto :goto_4

    .line 795
    .end local v3    # "outputStream":Ljava/io/OutputStream;
    .restart local v0    # "dir":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v3    # "outputStream":Ljava/io/OutputStream;
    goto :goto_3

    .line 792
    .end local v3    # "outputStream":Ljava/io/OutputStream;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    :catch_6
    move-exception v1

    move-object v3, v4

    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v3    # "outputStream":Ljava/io/OutputStream;
    goto :goto_2

    .line 790
    .end local v3    # "outputStream":Ljava/io/OutputStream;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    :catch_7
    move-exception v1

    move-object v3, v4

    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .restart local v3    # "outputStream":Ljava/io/OutputStream;
    goto :goto_1
.end method


# virtual methods
.method public addImage(Landroid/content/ContentResolver;Ljava/lang/String;J[BI)Landroid/net/Uri;
    .locals 7
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "date"    # J
    .param p5, "jpeg"    # [B
    .param p6, "orientation"    # I

    .prologue
    .line 804
    sget-object v3, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addImage title:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x6

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 807
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "title"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    const-string v3, "_display_name"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    const-string v3, "datetaken"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 810
    const-string v3, "mime_type"

    const-string v4, "image/jpeg"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    const-string v3, "_size"

    array-length v4, p5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 812
    const-string v3, "_data"

    iget-object v4, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mLastSucceededFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    const-string v3, "orientation"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 815
    const/4 v1, 0x0

    .line 817
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p1, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 821
    :goto_0
    sget-object v3, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addImage uri:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 823
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.camera.NEW_PICTURE"

    invoke-direct {v3, v4, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/camaftest/CaptureActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 825
    return-object v1

    .line 818
    :catch_0
    move-exception v0

    .line 819
    .local v0, "th":Ljava/lang/Throwable;
    sget-object v3, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to write MediaStore"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public drawViewfinder()V
    .locals 1

    .prologue
    .line 685
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/ViewfinderView;->drawViewfinder()V

    .line 686
    return-void
.end method

.method getCameraManager()Lcom/sec/android/app/camaftest/camera/CameraManager;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    return-object v0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    return-object v0
.end method

.method getViewfinderView()Lcom/sec/android/app/camaftest/ViewfinderView;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;

    return-object v0
.end method

.method public handleDecode(Lcom/sec/android/app/camaftest/core/Result;Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "rawResult"    # Lcom/sec/android/app/camaftest/core/Result;
    .param p2, "barcode"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v7, 0x0

    .line 369
    iget-object v4, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->inactivityTimer:Lcom/sec/android/app/camaftest/InactivityTimer;

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/InactivityTimer;->onActivity()V

    .line 370
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mIsShowResultView:Z

    .line 372
    iget-object v4, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->beepManager:Lcom/sec/android/app/camaftest/BeepManager;

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/BeepManager;->updatePrefsForVolume()V

    .line 374
    iget-object v4, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->beepManager:Lcom/sec/android/app/camaftest/BeepManager;

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/BeepManager;->playBeepSoundAndVibrate()V

    .line 376
    const/4 v3, 0x0

    .line 378
    .local v3, "volume":I
    const-string v4, "audio"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/camaftest/CaptureActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    iput-object v4, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 379
    iget-object v4, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mAudioManager:Landroid/media/AudioManager;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v3

    .line 381
    sget-object v4, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "STREAM_MUSIC volume "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    if-nez p1, :cond_0

    .line 385
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->handleDecodeFailed()V

    .line 430
    :goto_0
    return-void

    .line 389
    :cond_0
    invoke-static {p0, p1}, Lcom/sec/android/app/camaftest/result/ResultHandlerFactory;->makeResultHandler(Lcom/sec/android/app/camaftest/CaptureActivity;Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/result/ResultHandler;

    move-result-object v2

    .line 391
    .local v2, "resultHandler":Lcom/sec/android/app/camaftest/result/ResultHandler;
    if-nez p2, :cond_1

    .line 393
    const/4 v4, 0x0

    invoke-direct {p0, p1, v2, v4}, Lcom/sec/android/app/camaftest/CaptureActivity;->handleDecodeInternally(Lcom/sec/android/app/camaftest/core/Result;Lcom/sec/android/app/camaftest/result/ResultHandler;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 397
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/Result;->getResultPoints()[Lcom/sec/android/app/camaftest/core/ResultPoint;

    move-result-object v0

    .line 398
    .local v0, "points":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    if-eqz v0, :cond_2

    array-length v4, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 399
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->handleDecodeFailed()V

    .line 400
    sget-object v4, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    const-string v5, "detect barcode"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 405
    :cond_2
    invoke-direct {p0, p2, p1}, Lcom/sec/android/app/camaftest/CaptureActivity;->drawResultPoints(Landroid/graphics/Bitmap;Lcom/sec/android/app/camaftest/core/Result;)V

    .line 406
    sget-object v4, Lcom/sec/android/app/camaftest/CaptureActivity$1;->$SwitchMap$com$sec$android$app$camaftest$IntentSource:[I

    iget-object v5, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->source:Lcom/sec/android/app/camaftest/IntentSource;

    invoke-virtual {v5}, Lcom/sec/android/app/camaftest/IntentSource;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 409
    :pswitch_0
    invoke-direct {p0, p1, v2, p2}, Lcom/sec/android/app/camaftest/CaptureActivity;->handleDecodeExternally(Lcom/sec/android/app/camaftest/core/Result;Lcom/sec/android/app/camaftest/result/ResultHandler;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 412
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->returnUrlTemplate:Ljava/lang/String;

    if-nez v4, :cond_3

    .line 413
    invoke-direct {p0, p1, v2, p2}, Lcom/sec/android/app/camaftest/CaptureActivity;->handleDecodeInternally(Lcom/sec/android/app/camaftest/core/Result;Lcom/sec/android/app/camaftest/result/ResultHandler;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 415
    :cond_3
    invoke-direct {p0, p1, v2, p2}, Lcom/sec/android/app/camaftest/CaptureActivity;->handleDecodeExternally(Lcom/sec/android/app/camaftest/core/Result;Lcom/sec/android/app/camaftest/result/ResultHandler;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 419
    :pswitch_2
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 420
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v4, "preferences_bulk_mode"

    invoke-interface {v1, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 421
    const v4, 0x7f0a005f

    invoke-static {p0, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 423
    const-wide/16 v4, 0x3e8

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/camaftest/CaptureActivity;->restartPreviewAfterDelay(J)V

    goto :goto_0

    .line 425
    :cond_4
    invoke-direct {p0, p1, v2, p2}, Lcom/sec/android/app/camaftest/CaptureActivity;->handleDecodeInternally(Lcom/sec/android/app/camaftest/core/Result;Lcom/sec/android/app/camaftest/result/ResultHandler;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 406
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 690
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 692
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 716
    :goto_0
    :pswitch_0
    return-void

    .line 694
    :pswitch_1
    sget-object v1, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    const-string v2, "onClick() - scan_button"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mScanButton:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 696
    const-wide/16 v2, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/camaftest/CaptureActivity;->restartPreviewAfterDelay(J)V

    goto :goto_0

    .line 699
    :pswitch_2
    sget-object v1, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    const-string v2, "onClick() - done_button"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    const-string v1, "data_filepath"

    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mLastSucceededFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 701
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/camaftest/CaptureActivity;->setResult(ILandroid/content/Intent;)V

    .line 702
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->finish()V

    goto :goto_0

    .line 705
    :pswitch_3
    sget-object v1, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    const-string v2, "onClick() - fail_button"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/camaftest/CaptureActivity;->setResult(ILandroid/content/Intent;)V

    .line 707
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->finish()V

    goto :goto_0

    .line 710
    :pswitch_4
    sget-object v1, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    const-string v2, "onClick() - retry_button"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->resetStatusView()V

    goto :goto_0

    .line 692
    :pswitch_data_0
    .packed-switch 0x7f090038
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 157
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/sec/android/app/camaftest/CaptureActivity;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/16 v2, 0x1a

    invoke-static {v1, v2}, Lcom/sec/android/app/camaftest/CaptureActivity;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 178
    .local v0, "window":Landroid/view/Window;
    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 179
    const v1, 0x7f030005

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->setContentView(I)V

    .line 181
    iput-boolean v3, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->hasSurface:Z

    .line 182
    new-instance v1, Lcom/sec/android/app/camaftest/InactivityTimer;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camaftest/InactivityTimer;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->inactivityTimer:Lcom/sec/android/app/camaftest/InactivityTimer;

    .line 183
    new-instance v1, Lcom/sec/android/app/camaftest/BeepManager;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camaftest/BeepManager;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->beepManager:Lcom/sec/android/app/camaftest/BeepManager;

    .line 185
    const v1, 0x7f050001

    invoke-static {p0, v1, v3}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;IZ)V

    .line 189
    const v1, 0x7f09003c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mScanButton:Landroid/widget/Button;

    .line 190
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mScanButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->inactivityTimer:Lcom/sec/android/app/camaftest/InactivityTimer;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->inactivityTimer:Lcom/sec/android/app/camaftest/InactivityTimer;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/InactivityTimer;->shutdown()V

    .line 320
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 321
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 325
    const/4 v1, 0x4

    if-ne p1, v1, :cond_3

    .line 326
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->source:Lcom/sec/android/app/camaftest/IntentSource;

    sget-object v2, Lcom/sec/android/app/camaftest/IntentSource;->NATIVE_APP_INTENT:Lcom/sec/android/app/camaftest/IntentSource;

    if-ne v1, v2, :cond_1

    .line 327
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camaftest/CaptureActivity;->setResult(I)V

    .line 328
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->finish()V

    .line 338
    :cond_0
    :goto_0
    return v0

    .line 330
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->source:Lcom/sec/android/app/camaftest/IntentSource;

    sget-object v2, Lcom/sec/android/app/camaftest/IntentSource;->NONE:Lcom/sec/android/app/camaftest/IntentSource;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->source:Lcom/sec/android/app/camaftest/IntentSource;

    sget-object v2, Lcom/sec/android/app/camaftest/IntentSource;->ZXING_LINK:Lcom/sec/android/app/camaftest/IntentSource;

    if-ne v1, v2, :cond_4

    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mIsShowResultView:Z

    if-ne v1, v0, :cond_4

    .line 331
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->resetStatusView()V

    goto :goto_0

    .line 334
    :cond_3
    const/16 v1, 0x50

    if-eq p1, v1, :cond_0

    const/16 v1, 0x1b

    if-eq p1, v1, :cond_0

    .line 338
    :cond_4
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 302
    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    if-eqz v2, :cond_0

    .line 303
    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->quitSynchronously()V

    .line 304
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    .line 306
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->inactivityTimer:Lcom/sec/android/app/camaftest/InactivityTimer;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/InactivityTimer;->onPause()V

    .line 307
    iget-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/camera/CameraManager;->closeDriver()V

    .line 308
    iget-boolean v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->hasSurface:Z

    if-nez v2, :cond_1

    .line 309
    const v2, 0x7f090027

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceView;

    .line 310
    .local v1, "surfaceView":Landroid/view/SurfaceView;
    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 311
    .local v0, "surfaceHolder":Landroid/view/SurfaceHolder;
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 313
    .end local v0    # "surfaceHolder":Landroid/view/SurfaceHolder;
    .end local v1    # "surfaceView":Landroid/view/SurfaceView;
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 314
    return-void
.end method

.method protected onResume()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 195
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 202
    new-instance v9, Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->getApplication()Landroid/app/Application;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/android/app/camaftest/camera/CameraManager;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    .line 204
    const v9, 0x7f090028

    invoke-virtual {p0, v9}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/camaftest/ViewfinderView;

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;

    .line 205
    iget-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;

    iget-object v10, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v9, v10}, Lcom/sec/android/app/camaftest/ViewfinderView;->setCameraManager(Lcom/sec/android/app/camaftest/camera/CameraManager;)V

    .line 207
    const v9, 0x7f090029

    invoke-virtual {p0, v9}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->resultView:Landroid/view/View;

    .line 208
    const v9, 0x7f090038

    invoke-virtual {p0, v9}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mDoneButton:Landroid/widget/Button;

    .line 209
    const v9, 0x7f090039

    invoke-virtual {p0, v9}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mFailButton:Landroid/widget/Button;

    .line 211
    const v9, 0x7f09003b

    invoke-virtual {p0, v9}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->statusView:Landroid/widget/TextView;

    .line 213
    iput-object v12, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    .line 214
    iput-boolean v11, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mIsShowResultView:Z

    .line 216
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->resetStatusView()V

    .line 218
    const v9, 0x7f090027

    invoke-virtual {p0, v9}, Lcom/sec/android/app/camaftest/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/SurfaceView;

    .line 219
    .local v7, "surfaceView":Landroid/view/SurfaceView;
    invoke-virtual {v7}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v6

    .line 221
    .local v6, "surfaceHolder":Landroid/view/SurfaceHolder;
    iget-boolean v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->hasSurface:Z

    if-eqz v9, :cond_3

    .line 224
    invoke-direct {p0, v6}, Lcom/sec/android/app/camaftest/CaptureActivity;->initCamera(Landroid/view/SurfaceHolder;)V

    .line 231
    :goto_0
    iget-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->beepManager:Lcom/sec/android/app/camaftest/BeepManager;

    invoke-virtual {v9}, Lcom/sec/android/app/camaftest/BeepManager;->updatePrefs()V

    .line 233
    iget-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->inactivityTimer:Lcom/sec/android/app/camaftest/InactivityTimer;

    invoke-virtual {v9}, Lcom/sec/android/app/camaftest/InactivityTimer;->onResume()V

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 237
    .local v5, "intent":Landroid/content/Intent;
    sget-object v9, Lcom/sec/android/app/camaftest/IntentSource;->NONE:Lcom/sec/android/app/camaftest/IntentSource;

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->source:Lcom/sec/android/app/camaftest/IntentSource;

    .line 238
    iput-object v12, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->decodeFormats:Ljava/util/Collection;

    .line 239
    iput-object v12, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->characterSet:Ljava/lang/String;

    .line 241
    if-eqz v5, :cond_2

    .line 242
    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {v5}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    .line 245
    .local v2, "dataString":Ljava/lang/String;
    const-string v9, "com.sec.android.app.camaftest.SCAN"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 247
    sget-object v9, Lcom/sec/android/app/camaftest/IntentSource;->NATIVE_APP_INTENT:Lcom/sec/android/app/camaftest/IntentSource;

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->source:Lcom/sec/android/app/camaftest/IntentSource;

    .line 248
    invoke-static {v5}, Lcom/sec/android/app/camaftest/DecodeFormatManager;->parseDecodeFormats(Landroid/content/Intent;)Ljava/util/Collection;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->decodeFormats:Ljava/util/Collection;

    .line 250
    const-string v9, "SCAN_WIDTH"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "SCAN_HEIGHT"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 251
    const-string v9, "SCAN_WIDTH"

    invoke-virtual {v5, v9, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 252
    .local v8, "width":I
    const-string v9, "SCAN_HEIGHT"

    invoke-virtual {v5, v9, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 253
    .local v3, "height":I
    if-lez v8, :cond_0

    if-lez v3, :cond_0

    .line 254
    iget-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v9, v8, v3}, Lcom/sec/android/app/camaftest/camera/CameraManager;->setManualFramingRect(II)V

    .line 258
    .end local v3    # "height":I
    .end local v8    # "width":I
    :cond_0
    const-string v9, "PROMPT_MESSAGE"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 259
    .local v1, "customPromptMessage":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 260
    iget-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->statusView:Landroid/widget/TextView;

    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    .end local v1    # "customPromptMessage":Ljava/lang/String;
    :cond_1
    :goto_1
    const-string v9, "CHARACTER_SET"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->characterSet:Ljava/lang/String;

    .line 286
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "dataString":Ljava/lang/String;
    :cond_2
    return-void

    .line 227
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-interface {v6, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 228
    const/4 v9, 0x3

    invoke-interface {v6, v9}, Landroid/view/SurfaceHolder;->setType(I)V

    goto :goto_0

    .line 263
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v2    # "dataString":Ljava/lang/String;
    .restart local v5    # "intent":Landroid/content/Intent;
    :cond_4
    if-eqz v2, :cond_5

    const-string v9, "http://www.google"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    const-string v9, "/m/products/scan"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 268
    sget-object v9, Lcom/sec/android/app/camaftest/IntentSource;->PRODUCT_SEARCH_LINK:Lcom/sec/android/app/camaftest/IntentSource;

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->source:Lcom/sec/android/app/camaftest/IntentSource;

    .line 269
    iput-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->sourceUrl:Ljava/lang/String;

    .line 270
    sget-object v9, Lcom/sec/android/app/camaftest/DecodeFormatManager;->PRODUCT_FORMATS:Ljava/util/Collection;

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->decodeFormats:Ljava/util/Collection;

    goto :goto_1

    .line 272
    :cond_5
    invoke-static {v2}, Lcom/sec/android/app/camaftest/CaptureActivity;->isZXingURL(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 275
    sget-object v9, Lcom/sec/android/app/camaftest/IntentSource;->ZXING_LINK:Lcom/sec/android/app/camaftest/IntentSource;

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->source:Lcom/sec/android/app/camaftest/IntentSource;

    .line 276
    iput-object v2, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->sourceUrl:Ljava/lang/String;

    .line 277
    iget-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->sourceUrl:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 278
    .local v4, "inputUri":Landroid/net/Uri;
    const-string v9, "ret"

    invoke-virtual {v4, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->returnUrlTemplate:Ljava/lang/String;

    .line 279
    invoke-static {v4}, Lcom/sec/android/app/camaftest/DecodeFormatManager;->parseDecodeFormats(Landroid/net/Uri;)Ljava/util/Collection;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->decodeFormats:Ljava/util/Collection;

    goto :goto_1
.end method

.method public restartPreviewAfterDelay(J)V
    .locals 3
    .param p1, "delayMS"    # J

    .prologue
    .line 669
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    if-eqz v0, :cond_0

    .line 670
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->mLastSucceededFilePath:Ljava/lang/String;

    .line 671
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->handler:Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    const v1, 0x7f090006

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 673
    :cond_0
    return-void
.end method

.method public storeImage([BI)V
    .locals 12
    .param p1, "data"    # [B
    .param p2, "orientation"    # I

    .prologue
    .line 719
    if-nez p1, :cond_0

    .line 741
    :goto_0
    return-void

    .line 723
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 724
    .local v4, "dateTaken":J
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "yyyy-MM-dd kk.mm.ss"

    invoke-static {v2, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 727
    .local v3, "name":Ljava/lang/String;
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->bUseSdcard:Z

    if-eqz v1, :cond_2

    .line 728
    invoke-static {}, Lcom/sec/android/app/camaftest/CaptureActivity;->getAvailableSpaceSd()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-lez v1, :cond_1

    .line 729
    sget-object v1, Lcom/sec/android/app/camaftest/CaptureActivity;->DIRECTORY_CAMERA_SDCARD:Ljava/lang/String;

    invoke-direct {p0, v1, v3, p1}, Lcom/sec/android/app/camaftest/CaptureActivity;->writeImage(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 735
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/camaftest/camera/CameraManager;->calculateOrientationForPicture(I)I

    move-result v7

    .line 737
    .local v7, "orientationForPicture":I
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object v1, p0

    move-object v6, p1

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/camaftest/CaptureActivity;->addImage(Landroid/content/ContentResolver;Ljava/lang/String;J[BI)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 738
    .end local v7    # "orientationForPicture":I
    :catch_0
    move-exception v0

    .line 739
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    const-string v2, "Exception while compressing image."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 732
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    sget-object v1, Lcom/sec/android/app/camaftest/CaptureActivity;->DIRECTORY_CAMERA_DATA:Ljava/lang/String;

    invoke-direct {p0, v1, v3, p1}, Lcom/sec/android/app/camaftest/CaptureActivity;->writeImage(Ljava/lang/String;Ljava/lang/String;[B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 359
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/CaptureActivity;->refreshScanButton()V

    .line 360
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 343
    if-nez p1, :cond_0

    .line 344
    sget-object v0, Lcom/sec/android/app/camaftest/CaptureActivity;->TAG:Ljava/lang/String;

    const-string v1, "*** WARNING *** surfaceCreated() gave us a null surface!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->hasSurface:Z

    if-nez v0, :cond_1

    .line 347
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->hasSurface:Z

    .line 348
    invoke-direct {p0, p1}, Lcom/sec/android/app/camaftest/CaptureActivity;->initCamera(Landroid/view/SurfaceHolder;)V

    .line 350
    :cond_1
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 354
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camaftest/CaptureActivity;->hasSurface:Z

    .line 355
    return-void
.end method
