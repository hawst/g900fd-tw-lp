.class public final Lcom/sec/android/app/camaftest/result/ResultHandlerFactory;
.super Ljava/lang/Object;
.source "ResultHandlerFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camaftest/result/ResultHandlerFactory$1;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static makeResultHandler(Lcom/sec/android/app/camaftest/CaptureActivity;Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/result/ResultHandler;
    .locals 3
    .param p0, "activity"    # Lcom/sec/android/app/camaftest/CaptureActivity;
    .param p1, "rawResult"    # Lcom/sec/android/app/camaftest/core/Result;

    .prologue
    .line 34
    invoke-static {p1}, Lcom/sec/android/app/camaftest/result/ResultHandlerFactory;->parseResult(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    move-result-object v0

    .line 35
    .local v0, "result":Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;
    sget-object v1, Lcom/sec/android/app/camaftest/result/ResultHandlerFactory$1;->$SwitchMap$com$sec$android$app$camaftest$core$client$result$ParsedResultType:[I

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;->getType()Lcom/sec/android/app/camaftest/core/client/result/ParsedResultType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/camaftest/core/client/result/ParsedResultType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 49
    new-instance v1, Lcom/sec/android/app/camaftest/result/TextResultHandler;

    invoke-direct {v1, p0, v0, p1}, Lcom/sec/android/app/camaftest/result/TextResultHandler;-><init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;Lcom/sec/android/app/camaftest/core/Result;)V

    :goto_0
    return-object v1

    .line 37
    :pswitch_0
    new-instance v1, Lcom/sec/android/app/camaftest/result/AddressBookResultHandler;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/camaftest/result/AddressBookResultHandler;-><init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;)V

    goto :goto_0

    .line 39
    :pswitch_1
    new-instance v1, Lcom/sec/android/app/camaftest/result/EmailAddressResultHandler;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/camaftest/result/EmailAddressResultHandler;-><init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;)V

    goto :goto_0

    .line 41
    :pswitch_2
    new-instance v1, Lcom/sec/android/app/camaftest/result/URIResultHandler;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/camaftest/result/URIResultHandler;-><init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;)V

    goto :goto_0

    .line 43
    :pswitch_3
    new-instance v1, Lcom/sec/android/app/camaftest/result/TelResultHandler;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/camaftest/result/TelResultHandler;-><init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;)V

    goto :goto_0

    .line 45
    :pswitch_4
    new-instance v1, Lcom/sec/android/app/camaftest/result/SMSResultHandler;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/camaftest/result/SMSResultHandler;-><init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;)V

    goto :goto_0

    .line 47
    :pswitch_5
    new-instance v1, Lcom/sec/android/app/camaftest/result/CalendarResultHandler;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/camaftest/result/CalendarResultHandler;-><init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;)V

    goto :goto_0

    .line 35
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static parseResult(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;
    .locals 1
    .param p0, "rawResult"    # Lcom/sec/android/app/camaftest/core/Result;

    .prologue
    .line 54
    invoke-static {p0}, Lcom/sec/android/app/camaftest/core/client/result/ResultParser;->parseResult(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    move-result-object v0

    return-object v0
.end method
