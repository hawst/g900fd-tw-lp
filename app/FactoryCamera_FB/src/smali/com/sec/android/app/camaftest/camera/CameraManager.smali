.class public final Lcom/sec/android/app/camaftest/camera/CameraManager;
.super Ljava/lang/Object;
.source "CameraManager.java"


# static fields
.field private static final MAX_NUM_OF_AUTO_FOCUS:I = 0x3

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public Feature:Lcom/sec/android/app/camera/Feature;

.field private final autoFocusCallback:Lcom/sec/android/app/camaftest/camera/AutoFocusCallback;

.field private camera:Landroid/hardware/Camera;

.field private final configManager:Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;

.field private final context:Landroid/content/Context;

.field private framingRect:Landroid/graphics/Rect;

.field private framingRectInPreview:Landroid/graphics/Rect;

.field private initialized:Z

.field private mAutoFocusCount:I

.field private mCameraId:I

.field private mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

.field private mDVFSHelper:Landroid/os/DVFSHelper;

.field private mLastOrientation:I

.field private mNumberOfCameras:I

.field private mOrientationListener:Landroid/view/OrientationEventListener;

.field private mPreviewOrientation:I

.field private mPreviousOrientation:I

.field private final previewCallback:Lcom/sec/android/app/camaftest/camera/PreviewCallback;

.field private previewing:Z

.field private requestedFramingRectHeight:I

.field private requestedFramingRectWidth:I

.field private reverseImage:Z

.field private supportedCPUCoreTable:[I

.field private supportedCPUFreqTable:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/camera/CameraManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object v3, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 65
    iput v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mAutoFocusCount:I

    .line 70
    iput v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mCameraId:I

    .line 71
    iput v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mLastOrientation:I

    .line 72
    iput v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mPreviewOrientation:I

    .line 73
    iput v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mPreviousOrientation:I

    .line 75
    iput-object v3, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 88
    sget-object v1, Lcom/sec/android/app/camaftest/camera/CameraManager;->TAG:Ljava/lang/String;

    const-string v2, "CameraManager..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iput-object p1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->context:Landroid/content/Context;

    .line 91
    invoke-static {}, Lcom/sec/android/app/camera/CameraApp;->getAppFeature()Lcom/sec/android/app/camera/Feature;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->Feature:Lcom/sec/android/app/camera/Feature;

    .line 93
    new-instance v1, Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;

    invoke-direct {v1, p1}, Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->configManager:Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;

    .line 94
    new-instance v1, Lcom/sec/android/app/camaftest/camera/PreviewCallback;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->configManager:Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;

    invoke-direct {v1, v2}, Lcom/sec/android/app/camaftest/camera/PreviewCallback;-><init>(Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;)V

    iput-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->previewCallback:Lcom/sec/android/app/camaftest/camera/PreviewCallback;

    .line 95
    new-instance v1, Lcom/sec/android/app/camaftest/camera/AutoFocusCallback;

    invoke-direct {v1}, Lcom/sec/android/app/camaftest/camera/AutoFocusCallback;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->autoFocusCallback:Lcom/sec/android/app/camaftest/camera/AutoFocusCallback;

    .line 97
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mNumberOfCameras:I

    .line 98
    iget v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mNumberOfCameras:I

    new-array v1, v1, [Landroid/hardware/Camera$CameraInfo;

    iput-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    .line 100
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mNumberOfCameras:I

    if-ge v0, v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    aput-object v2, v1, v0

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    aget-object v1, v1, v0

    invoke-static {v0, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/camera/CameraManager;->setOrientationListener()V

    .line 106
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/app/camaftest/camera/CameraManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/camaftest/camera/CameraManager;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camaftest/camera/CameraManager;
    .param p1, "x1"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/camaftest/camera/CameraManager;->roundOrientation(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/camaftest/camera/CameraManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camaftest/camera/CameraManager;
    .param p1, "x1"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/camaftest/camera/CameraManager;->setLastOrientation(I)V

    return-void
.end method

.method private roundOrientation(I)I
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 457
    add-int/lit8 v0, p1, 0x2d

    div-int/lit8 v0, v0, 0x5a

    mul-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    return v0
.end method

.method private setLastOrientation(I)V
    .locals 0
    .param p1, "lastOrientation"    # I

    .prologue
    .line 461
    iput p1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mLastOrientation:I

    .line 462
    return-void
.end method

.method private setOrientationListener()V
    .locals 2

    .prologue
    .line 440
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-nez v0, :cond_0

    .line 441
    new-instance v0, Lcom/sec/android/app/camaftest/camera/CameraManager$1;

    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->context:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/camaftest/camera/CameraManager$1;-><init>(Lcom/sec/android/app/camaftest/camera/CameraManager;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 454
    return-void
.end method


# virtual methods
.method public acquireDVFS(I)V
    .locals 5
    .param p1, "milisecond"    # I

    .prologue
    const/4 v4, 0x0

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-nez v0, :cond_0

    .line 488
    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->context:Landroid/content/Context;

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->supportedCPUFreqTable:[I

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->supportedCPUCoreTable:[I

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->supportedCPUFreqTable:[I

    if-eqz v0, :cond_2

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mDVFSHelper:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->supportedCPUFreqTable:[I

    aget v2, v2, v4

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 494
    sget-object v0, Lcom/sec/android/app/camaftest/camera/CameraManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "supportedCPUFreqTable is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->supportedCPUFreqTable:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->supportedCPUCoreTable:[I

    if-eqz v0, :cond_3

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mDVFSHelper:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    iget-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->supportedCPUCoreTable:[I

    aget v2, v2, v4

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 501
    sget-object v0, Lcom/sec/android/app/camaftest/camera/CameraManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "supportedCPUCoreTable is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->supportedCPUCoreTable:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0, p1}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 509
    sget-object v0, Lcom/sec/android/app/camaftest/camera/CameraManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDVFSHelper.acquire : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    :cond_1
    return-void

    .line 496
    :cond_2
    sget-object v0, Lcom/sec/android/app/camaftest/camera/CameraManager;->TAG:Ljava/lang/String;

    const-string v1, "supportedCPUFreqTable is null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 503
    :cond_3
    sget-object v0, Lcom/sec/android/app/camaftest/camera/CameraManager;->TAG:Ljava/lang/String;

    const-string v1, "supportedCPUFreqTable is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public buildLuminanceSource([BII)Lcom/sec/android/app/camaftest/PlanarYUVLuminanceSource;
    .locals 14
    .param p1, "data"    # [B
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/camera/CameraManager;->getFramingRectInPreview()Landroid/graphics/Rect;

    move-result-object v11

    .line 386
    .local v11, "rect":Landroid/graphics/Rect;
    if-nez v11, :cond_0

    .line 387
    const/4 v1, 0x0

    .line 435
    :goto_0
    return-object v1

    .line 390
    :cond_0
    const/4 v12, 0x0

    .line 391
    .local v12, "tmp":I
    array-length v1, p1

    new-array v2, v1, [B

    .line 393
    .local v2, "convertData":[B
    iget v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mPreviewOrientation:I

    sparse-switch v1, :sswitch_data_0

    .line 435
    :cond_1
    :goto_1
    new-instance v1, Lcom/sec/android/app/camaftest/PlanarYUVLuminanceSource;

    iget v5, v11, Landroid/graphics/Rect;->left:I

    iget v6, v11, Landroid/graphics/Rect;->top:I

    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v8

    iget-boolean v9, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->reverseImage:Z

    move/from16 v3, p2

    move/from16 v4, p3

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/camaftest/PlanarYUVLuminanceSource;-><init>([BIIIIIIZ)V

    goto :goto_0

    .line 395
    :sswitch_0
    move-object v2, p1

    .line 396
    goto :goto_1

    .line 399
    :sswitch_1
    const/4 v10, 0x0

    .local v10, "h":I
    :goto_2
    move/from16 v0, p3

    if-ge v10, v0, :cond_3

    .line 400
    const/4 v13, 0x0

    .local v13, "w":I
    :goto_3
    move/from16 v0, p2

    if-ge v13, v0, :cond_2

    .line 401
    mul-int v1, v13, p3

    add-int v1, v1, p3

    sub-int/2addr v1, v10

    add-int/lit8 v1, v1, -0x1

    mul-int v3, v10, p2

    add-int/2addr v3, v13

    aget-byte v3, p1, v3

    aput-byte v3, v2, v1

    .line 400
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 399
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 405
    .end local v13    # "w":I
    :cond_3
    move/from16 v12, p2

    .line 406
    move/from16 p2, p3

    .line 407
    move/from16 p3, v12

    .line 408
    goto :goto_1

    .line 411
    .end local v10    # "h":I
    :sswitch_2
    const/4 v10, 0x0

    .restart local v10    # "h":I
    :goto_4
    move/from16 v0, p3

    if-ge v10, v0, :cond_1

    .line 412
    const/4 v13, 0x0

    .restart local v13    # "w":I
    :goto_5
    move/from16 v0, p2

    if-ge v13, v0, :cond_4

    .line 413
    mul-int v1, p2, p3

    mul-int v3, v10, p2

    add-int/2addr v3, v13

    add-int/lit8 v3, v3, 0x1

    sub-int/2addr v1, v3

    mul-int v3, v10, p2

    add-int/2addr v3, v13

    aget-byte v3, p1, v3

    aput-byte v3, v2, v1

    .line 412
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    .line 411
    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 419
    .end local v10    # "h":I
    .end local v13    # "w":I
    :sswitch_3
    const/4 v10, 0x0

    .restart local v10    # "h":I
    :goto_6
    move/from16 v0, p3

    if-ge v10, v0, :cond_6

    .line 420
    const/4 v13, 0x0

    .restart local v13    # "w":I
    :goto_7
    move/from16 v0, p2

    if-ge v13, v0, :cond_5

    .line 421
    sub-int v1, p2, v13

    add-int/lit8 v1, v1, -0x1

    mul-int v1, v1, p3

    add-int/2addr v1, v10

    mul-int v3, v10, p2

    add-int/2addr v3, v13

    aget-byte v3, p1, v3

    aput-byte v3, v2, v1

    .line 420
    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    .line 419
    :cond_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 425
    .end local v13    # "w":I
    :cond_6
    move/from16 v12, p2

    .line 426
    move/from16 p2, p3

    .line 427
    move/from16 p3, v12

    .line 428
    goto/16 :goto_1

    .line 393
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method public calculateOrientationForPicture(I)I
    .locals 4
    .param p1, "orientation"    # I

    .prologue
    .line 476
    const/4 v1, 0x0

    .line 478
    .local v1, "rotation":I
    const/4 v2, -0x1

    if-eq p1, v2, :cond_0

    .line 479
    iget-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mCameraInfo:[Landroid/hardware/Camera$CameraInfo;

    iget v3, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mCameraId:I

    aget-object v0, v2, v3

    .line 480
    .local v0, "info":Landroid/hardware/Camera$CameraInfo;
    iget v2, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v2, p1

    rem-int/lit16 v1, v2, 0x168

    .line 483
    .end local v0    # "info":Landroid/hardware/Camera$CameraInfo;
    :cond_0
    return v1
.end method

.method public closeDriver()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 151
    iput-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    .line 154
    iput-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->framingRect:Landroid/graphics/Rect;

    .line 155
    iput-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->framingRectInPreview:Landroid/graphics/Rect;

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 161
    :cond_1
    return-void
.end method

.method public getFramingRect()Landroid/graphics/Rect;
    .locals 8

    .prologue
    .line 268
    iget-object v5, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->framingRect:Landroid/graphics/Rect;

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/camera/CameraManager;->getPreviousOrientation()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/camera/CameraManager;->getLastOrientation()I

    move-result v6

    if-eq v5, v6, :cond_2

    .line 269
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    if-nez v5, :cond_1

    .line 270
    const/4 v5, 0x0

    .line 309
    :goto_0
    return-object v5

    .line 272
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->configManager:Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;

    invoke-virtual {v5}, Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;->getScreenResolution()Landroid/graphics/Point;

    move-result-object v2

    .line 273
    .local v2, "screenResolution":Landroid/graphics/Point;
    const/4 v4, 0x0

    .line 274
    .local v4, "width":I
    const/4 v0, 0x0

    .line 275
    .local v0, "height":I
    const/4 v1, 0x0

    .line 276
    .local v1, "leftOffset":I
    const/4 v3, 0x0

    .line 278
    .local v3, "topOffset":I
    iget v5, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mPreviewOrientation:I

    sparse-switch v5, :sswitch_data_0

    .line 288
    iget v4, v2, Landroid/graphics/Point;->y:I

    .line 289
    iget v0, v2, Landroid/graphics/Point;->x:I

    .line 290
    iget v1, v2, Landroid/graphics/Point;->y:I

    .line 291
    iget v3, v2, Landroid/graphics/Point;->x:I

    .line 295
    :goto_1
    mul-int/lit8 v5, v4, 0x3

    div-int/lit8 v4, v5, 0x4

    .line 296
    mul-int/lit8 v5, v0, 0x3

    div-int/lit8 v0, v5, 0x4

    .line 298
    if-le v4, v0, :cond_3

    .line 299
    move v4, v0

    .line 304
    :goto_2
    sub-int v5, v1, v4

    div-int/lit8 v1, v5, 0x2

    .line 305
    sub-int v5, v3, v0

    div-int/lit8 v3, v5, 0x2

    .line 306
    new-instance v5, Landroid/graphics/Rect;

    add-int v6, v1, v4

    add-int v7, v3, v0

    invoke-direct {v5, v1, v3, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->framingRect:Landroid/graphics/Rect;

    .line 307
    sget-object v5, Lcom/sec/android/app/camaftest/camera/CameraManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Calculated framing rect: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->framingRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    .end local v0    # "height":I
    .end local v1    # "leftOffset":I
    .end local v2    # "screenResolution":Landroid/graphics/Point;
    .end local v3    # "topOffset":I
    .end local v4    # "width":I
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->framingRect:Landroid/graphics/Rect;

    goto :goto_0

    .line 281
    .restart local v0    # "height":I
    .restart local v1    # "leftOffset":I
    .restart local v2    # "screenResolution":Landroid/graphics/Point;
    .restart local v3    # "topOffset":I
    .restart local v4    # "width":I
    :sswitch_0
    iget v4, v2, Landroid/graphics/Point;->x:I

    .line 282
    iget v0, v2, Landroid/graphics/Point;->y:I

    .line 283
    iget v1, v2, Landroid/graphics/Point;->x:I

    .line 284
    iget v3, v2, Landroid/graphics/Point;->y:I

    .line 285
    goto :goto_1

    .line 301
    :cond_3
    move v0, v4

    goto :goto_2

    .line 278
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xb4 -> :sswitch_0
    .end sparse-switch
.end method

.method public getFramingRectInPreview()Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 317
    iget-object v4, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->framingRectInPreview:Landroid/graphics/Rect;

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/camera/CameraManager;->getPreviousOrientation()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/camera/CameraManager;->getLastOrientation()I

    move-result v5

    if-eq v4, v5, :cond_2

    .line 318
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/camera/CameraManager;->getFramingRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 319
    .local v1, "framingRect":Landroid/graphics/Rect;
    if-nez v1, :cond_1

    .line 320
    const/4 v4, 0x0

    .line 345
    .end local v1    # "framingRect":Landroid/graphics/Rect;
    :goto_0
    return-object v4

    .line 322
    .restart local v1    # "framingRect":Landroid/graphics/Rect;
    :cond_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 323
    .local v2, "rect":Landroid/graphics/Rect;
    iget-object v4, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->configManager:Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;->getCameraResolution()Landroid/graphics/Point;

    move-result-object v0

    .line 324
    .local v0, "cameraResolution":Landroid/graphics/Point;
    iget-object v4, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->configManager:Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;

    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;->getScreenResolution()Landroid/graphics/Point;

    move-result-object v3

    .line 326
    .local v3, "screenResolution":Landroid/graphics/Point;
    iget v4, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mPreviewOrientation:I

    sparse-switch v4, :sswitch_data_0

    .line 336
    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget v5, v0, Landroid/graphics/Point;->y:I

    mul-int/2addr v4, v5

    iget v5, v3, Landroid/graphics/Point;->y:I

    div-int/2addr v4, v5

    iput v4, v2, Landroid/graphics/Rect;->left:I

    .line 337
    iget v4, v2, Landroid/graphics/Rect;->right:I

    iget v5, v0, Landroid/graphics/Point;->y:I

    mul-int/2addr v4, v5

    iget v5, v3, Landroid/graphics/Point;->y:I

    div-int/2addr v4, v5

    iput v4, v2, Landroid/graphics/Rect;->right:I

    .line 338
    iget v4, v2, Landroid/graphics/Rect;->top:I

    iget v5, v0, Landroid/graphics/Point;->x:I

    mul-int/2addr v4, v5

    iget v5, v3, Landroid/graphics/Point;->x:I

    div-int/2addr v4, v5

    iput v4, v2, Landroid/graphics/Rect;->top:I

    .line 339
    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    iget v5, v0, Landroid/graphics/Point;->x:I

    mul-int/2addr v4, v5

    iget v5, v3, Landroid/graphics/Point;->x:I

    div-int/2addr v4, v5

    iput v4, v2, Landroid/graphics/Rect;->bottom:I

    .line 343
    :goto_1
    iput-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->framingRectInPreview:Landroid/graphics/Rect;

    .line 345
    .end local v0    # "cameraResolution":Landroid/graphics/Point;
    .end local v1    # "framingRect":Landroid/graphics/Rect;
    .end local v2    # "rect":Landroid/graphics/Rect;
    .end local v3    # "screenResolution":Landroid/graphics/Point;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->framingRectInPreview:Landroid/graphics/Rect;

    goto :goto_0

    .line 329
    .restart local v0    # "cameraResolution":Landroid/graphics/Point;
    .restart local v1    # "framingRect":Landroid/graphics/Rect;
    .restart local v2    # "rect":Landroid/graphics/Rect;
    .restart local v3    # "screenResolution":Landroid/graphics/Point;
    :sswitch_0
    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget v5, v0, Landroid/graphics/Point;->x:I

    mul-int/2addr v4, v5

    iget v5, v3, Landroid/graphics/Point;->x:I

    div-int/2addr v4, v5

    iput v4, v2, Landroid/graphics/Rect;->left:I

    .line 330
    iget v4, v2, Landroid/graphics/Rect;->right:I

    iget v5, v0, Landroid/graphics/Point;->x:I

    mul-int/2addr v4, v5

    iget v5, v3, Landroid/graphics/Point;->x:I

    div-int/2addr v4, v5

    iput v4, v2, Landroid/graphics/Rect;->right:I

    .line 331
    iget v4, v2, Landroid/graphics/Rect;->top:I

    iget v5, v0, Landroid/graphics/Point;->y:I

    mul-int/2addr v4, v5

    iget v5, v3, Landroid/graphics/Point;->y:I

    div-int/2addr v4, v5

    iput v4, v2, Landroid/graphics/Rect;->top:I

    .line 332
    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    iget v5, v0, Landroid/graphics/Point;->y:I

    mul-int/2addr v4, v5

    iget v5, v3, Landroid/graphics/Point;->y:I

    div-int/2addr v4, v5

    iput v4, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 326
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xb4 -> :sswitch_0
    .end sparse-switch
.end method

.method public getLastOrientation()I
    .locals 1

    .prologue
    .line 465
    iget v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mLastOrientation:I

    return v0
.end method

.method public getPreviewOrientation()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mPreviewOrientation:I

    return v0
.end method

.method public getPreviousOrientation()I
    .locals 1

    .prologue
    .line 473
    iget v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mPreviousOrientation:I

    return v0
.end method

.method public isAutoFocusAvailable()Z
    .locals 2

    .prologue
    .line 253
    iget v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mAutoFocusCount:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openDriver(Landroid/view/SurfaceHolder;)V
    .locals 5
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    .line 116
    .local v1, "theCamera":Landroid/hardware/Camera;
    if-nez v1, :cond_2

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->CAMERA_BOOST_ENABLE:Z

    if-eqz v2, :cond_0

    .line 118
    const/16 v2, 0x7d0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camaftest/camera/CameraManager;->acquireDVFS(I)V

    .line 121
    :cond_0
    iget v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mCameraId:I

    invoke-static {v2}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v1

    .line 122
    if-nez v1, :cond_1

    .line 123
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2

    .line 125
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    .line 127
    :cond_2
    invoke-virtual {v1, p1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/camera/CameraManager;->setPreviewOrientation()V

    .line 130
    iget-boolean v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->initialized:Z

    if-nez v2, :cond_3

    .line 131
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->initialized:Z

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->configManager:Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;->initFromCameraParameters(Landroid/hardware/Camera;)V

    .line 133
    iget v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->requestedFramingRectWidth:I

    if-lez v2, :cond_3

    iget v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->requestedFramingRectHeight:I

    if-lez v2, :cond_3

    .line 134
    iget v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->requestedFramingRectWidth:I

    iget v3, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->requestedFramingRectHeight:I

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/camaftest/camera/CameraManager;->setManualFramingRect(II)V

    .line 135
    iput v4, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->requestedFramingRectWidth:I

    .line 136
    iput v4, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->requestedFramingRectHeight:I

    .line 139
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->configManager:Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;->setDesiredCameraParameters(Landroid/hardware/Camera;)V

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 142
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "preferences_reverse_image"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->reverseImage:Z

    .line 143
    return-void
.end method

.method public requestAutoFocus(Landroid/os/Handler;I)V
    .locals 3
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "message"    # I

    .prologue
    .line 239
    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->previewing:Z

    if-eqz v1, :cond_0

    .line 240
    iget v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mAutoFocusCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mAutoFocusCount:I

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->autoFocusCallback:Lcom/sec/android/app/camaftest/camera/AutoFocusCallback;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/camaftest/camera/AutoFocusCallback;->setHandler(Landroid/os/Handler;I)V

    .line 244
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->autoFocusCallback:Lcom/sec/android/app/camaftest/camera/AutoFocusCallback;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 245
    :catch_0
    move-exception v0

    .line 247
    .local v0, "re":Ljava/lang/RuntimeException;
    sget-object v1, Lcom/sec/android/app/camaftest/camera/CameraManager;->TAG:Ljava/lang/String;

    const-string v2, "Unexpected exception while focusing"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public requestPreviewFrame(Landroid/os/Handler;I)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "message"    # I

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    .line 226
    .local v0, "theCamera":Landroid/hardware/Camera;
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->previewing:Z

    if-eqz v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->previewCallback:Lcom/sec/android/app/camaftest/camera/PreviewCallback;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/camaftest/camera/PreviewCallback;->setHandler(Landroid/os/Handler;I)V

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->previewCallback:Lcom/sec/android/app/camaftest/camera/PreviewCallback;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setOneShotPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 230
    :cond_0
    return-void
.end method

.method public setAutoFocusAvailable()V
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mAutoFocusCount:I

    .line 258
    return-void
.end method

.method public setManualFramingRect(II)V
    .locals 6
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 356
    iget-boolean v3, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->initialized:Z

    if-eqz v3, :cond_2

    .line 357
    iget-object v3, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->configManager:Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/camera/CameraConfigurationManager;->getScreenResolution()Landroid/graphics/Point;

    move-result-object v1

    .line 358
    .local v1, "screenResolution":Landroid/graphics/Point;
    iget v3, v1, Landroid/graphics/Point;->x:I

    if-le p1, v3, :cond_0

    .line 359
    iget p1, v1, Landroid/graphics/Point;->x:I

    .line 361
    :cond_0
    iget v3, v1, Landroid/graphics/Point;->y:I

    if-le p2, v3, :cond_1

    .line 362
    iget p2, v1, Landroid/graphics/Point;->y:I

    .line 364
    :cond_1
    iget v3, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, p1

    div-int/lit8 v0, v3, 0x2

    .line 365
    .local v0, "leftOffset":I
    iget v3, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, p2

    div-int/lit8 v2, v3, 0x2

    .line 366
    .local v2, "topOffset":I
    new-instance v3, Landroid/graphics/Rect;

    add-int v4, v0, p1

    add-int v5, v2, p2

    invoke-direct {v3, v0, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v3, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->framingRect:Landroid/graphics/Rect;

    .line 367
    sget-object v3, Lcom/sec/android/app/camaftest/camera/CameraManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Calculated manual framing rect: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->framingRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->framingRectInPreview:Landroid/graphics/Rect;

    .line 373
    .end local v0    # "leftOffset":I
    .end local v1    # "screenResolution":Landroid/graphics/Point;
    .end local v2    # "topOffset":I
    :goto_0
    return-void

    .line 370
    :cond_2
    iput p1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->requestedFramingRectWidth:I

    .line 371
    iput p2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->requestedFramingRectHeight:I

    goto :goto_0
.end method

.method public setPreviewOrientation()V
    .locals 5

    .prologue
    .line 193
    iget-object v2, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    .line 194
    .local v2, "theCamera":Landroid/hardware/Camera;
    if-eqz v2, :cond_1

    .line 195
    iget-object v3, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->context:Landroid/content/Context;

    const-string v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 196
    .local v0, "manager":Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 198
    .local v1, "rotation":I
    const/4 v3, 0x1

    if-ne v1, v3, :cond_2

    .line 199
    const/4 v1, 0x3

    .line 204
    :cond_0
    :goto_0
    mul-int/lit8 v1, v1, 0x5a

    .line 206
    invoke-virtual {p0, v1}, Lcom/sec/android/app/camaftest/camera/CameraManager;->calculateOrientationForPicture(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mPreviewOrientation:I

    .line 208
    iget v3, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mPreviewOrientation:I

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 210
    .end local v0    # "manager":Landroid/view/WindowManager;
    .end local v1    # "rotation":I
    :cond_1
    return-void

    .line 200
    .restart local v0    # "manager":Landroid/view/WindowManager;
    .restart local v1    # "rotation":I
    :cond_2
    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    .line 201
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setPreviousOrientation(I)V
    .locals 0
    .param p1, "lastOrientation"    # I

    .prologue
    .line 469
    iput p1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mPreviousOrientation:I

    .line 470
    return-void
.end method

.method public startPreview()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    .line 168
    .local v0, "theCamera":Landroid/hardware/Camera;
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->previewing:Z

    if-nez v1, :cond_0

    .line 169
    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 170
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->previewing:Z

    .line 173
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v1, :cond_1

    .line 174
    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v1}, Landroid/view/OrientationEventListener;->enable()V

    .line 175
    :cond_1
    return-void
.end method

.method public stopPreview()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->previewing:Z

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->previewCallback:Lcom/sec/android/app/camaftest/camera/PreviewCallback;

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/camaftest/camera/PreviewCallback;->setHandler(Landroid/os/Handler;I)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->autoFocusCallback:Lcom/sec/android/app/camaftest/camera/AutoFocusCallback;

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/camaftest/camera/AutoFocusCallback;->setHandler(Landroid/os/Handler;I)V

    .line 185
    iput-boolean v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->previewing:Z

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_1

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager;->mOrientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 190
    :cond_1
    return-void
.end method
