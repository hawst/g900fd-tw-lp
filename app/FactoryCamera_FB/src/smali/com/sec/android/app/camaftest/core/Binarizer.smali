.class public abstract Lcom/sec/android/app/camaftest/core/Binarizer;
.super Ljava/lang/Object;
.source "Binarizer.java"


# instance fields
.field private final source:Lcom/sec/android/app/camaftest/core/LuminanceSource;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/camaftest/core/LuminanceSource;)V
    .locals 0
    .param p1, "source"    # Lcom/sec/android/app/camaftest/core/LuminanceSource;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/camaftest/core/Binarizer;->source:Lcom/sec/android/app/camaftest/core/LuminanceSource;

    .line 36
    return-void
.end method


# virtual methods
.method public abstract createBinarizer(Lcom/sec/android/app/camaftest/core/LuminanceSource;)Lcom/sec/android/app/camaftest/core/Binarizer;
.end method

.method public abstract getBlackMatrix()Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation
.end method

.method public abstract getBlackRow(ILcom/sec/android/app/camaftest/core/common/BitArray;)Lcom/sec/android/app/camaftest/core/common/BitArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Binarizer;->source:Lcom/sec/android/app/camaftest/core/LuminanceSource;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/LuminanceSource;->getHeight()I

    move-result v0

    return v0
.end method

.method public getLuminanceSource()Lcom/sec/android/app/camaftest/core/LuminanceSource;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Binarizer;->source:Lcom/sec/android/app/camaftest/core/LuminanceSource;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/Binarizer;->source:Lcom/sec/android/app/camaftest/core/LuminanceSource;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/LuminanceSource;->getWidth()I

    move-result v0

    return v0
.end method
