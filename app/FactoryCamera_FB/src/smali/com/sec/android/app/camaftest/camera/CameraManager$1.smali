.class Lcom/sec/android/app/camaftest/camera/CameraManager$1;
.super Landroid/view/OrientationEventListener;
.source "CameraManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/camaftest/camera/CameraManager;->setOrientationListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camaftest/camera/CameraManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camaftest/camera/CameraManager;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 441
    iput-object p1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager$1;->this$0:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    .line 443
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 444
    # getter for: Lcom/sec/android/app/camaftest/camera/CameraManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/camaftest/camera/CameraManager;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onOrientationChanged: orientation - unknown orientation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    :goto_0
    return-void

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager$1;->this$0:Lcom/sec/android/app/camaftest/camera/CameraManager;

    iget-object v1, p0, Lcom/sec/android/app/camaftest/camera/CameraManager$1;->this$0:Lcom/sec/android/app/camaftest/camera/CameraManager;

    # invokes: Lcom/sec/android/app/camaftest/camera/CameraManager;->roundOrientation(I)I
    invoke-static {v1, p1}, Lcom/sec/android/app/camaftest/camera/CameraManager;->access$100(Lcom/sec/android/app/camaftest/camera/CameraManager;I)I

    move-result v1

    # invokes: Lcom/sec/android/app/camaftest/camera/CameraManager;->setLastOrientation(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/camaftest/camera/CameraManager;->access$200(Lcom/sec/android/app/camaftest/camera/CameraManager;I)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/camaftest/camera/CameraManager$1;->this$0:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/camera/CameraManager;->setPreviewOrientation()V

    goto :goto_0
.end method
