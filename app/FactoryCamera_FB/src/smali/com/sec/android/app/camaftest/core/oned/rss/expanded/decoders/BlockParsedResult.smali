.class final Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;
.super Ljava/lang/Object;
.source "BlockParsedResult.java"


# instance fields
.field private final decodedInformation:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

.field private final finished:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;Z)V
    .locals 0
    .param p1, "information"    # Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    .param p2, "finished"    # Z

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-boolean p2, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;->finished:Z

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;->decodedInformation:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

    .line 46
    return-void
.end method

.method constructor <init>(Z)V
    .locals 1
    .param p1, "finished"    # Z

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;-><init>(Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;Z)V

    .line 41
    return-void
.end method


# virtual methods
.method getDecodedInformation()Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;->decodedInformation:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/DecodedInformation;

    return-object v0
.end method

.method isFinished()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/BlockParsedResult;->finished:Z

    return v0
.end method
