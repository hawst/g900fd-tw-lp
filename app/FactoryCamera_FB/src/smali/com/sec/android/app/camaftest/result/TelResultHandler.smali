.class public final Lcom/sec/android/app/camaftest/result/TelResultHandler;
.super Lcom/sec/android/app/camaftest/result/ResultHandler;
.source "TelResultHandler.java"


# static fields
.field private static final buttons:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/camaftest/result/TelResultHandler;->buttons:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0a0034
        0x7f0a002e
    .end array-data
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "result"    # Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;-><init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;)V

    .line 40
    return-void
.end method


# virtual methods
.method public getButtonCount()I
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/camaftest/result/TelResultHandler;->buttons:[I

    array-length v0, v0

    return v0
.end method

.method public getButtonText(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/app/camaftest/result/TelResultHandler;->buttons:[I

    aget v0, v0, p1

    return v0
.end method

.method public getDisplayContents()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/result/TelResultHandler;->getResult()Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;->getDisplayResult()Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "contents":Ljava/lang/String;
    const-string v1, "\r"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getDisplayTitle()I
    .locals 1

    .prologue
    .line 77
    const v0, 0x7f0a0097

    return v0
.end method

.method public handleButtonPress(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/result/TelResultHandler;->getResult()Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/camaftest/core/client/result/TelParsedResult;

    .line 55
    .local v1, "telResult":Lcom/sec/android/app/camaftest/core/client/result/TelParsedResult;
    packed-switch p1, :pswitch_data_0

    .line 65
    :goto_0
    return-void

    .line 57
    :pswitch_0
    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/client/result/TelParsedResult;->getTelURI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camaftest/result/TelResultHandler;->dialPhoneFromUri(Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :pswitch_1
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    .line 61
    .local v0, "numbers":[Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/client/result/TelParsedResult;->getNumber()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 62
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/camaftest/result/TelResultHandler;->addPhoneOnlyContact([Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
