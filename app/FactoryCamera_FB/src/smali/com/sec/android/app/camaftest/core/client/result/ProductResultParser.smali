.class public final Lcom/sec/android/app/camaftest/core/client/result/ProductResultParser;
.super Lcom/sec/android/app/camaftest/core/client/result/ResultParser;
.source "ProductResultParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/client/result/ResultParser;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic parse(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/camaftest/core/Result;

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/sec/android/app/camaftest/core/client/result/ProductResultParser;->parse(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/client/result/ProductParsedResult;

    move-result-object v0

    return-object v0
.end method

.method public parse(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/client/result/ProductParsedResult;
    .locals 8
    .param p1, "result"    # Lcom/sec/android/app/camaftest/core/Result;

    .prologue
    const/4 v6, 0x0

    .line 32
    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/Result;->getBarcodeFormat()Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    move-result-object v1

    .line 33
    .local v1, "format":Lcom/sec/android/app/camaftest/core/BarcodeFormat;
    sget-object v7, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_A:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    if-eq v1, v7, :cond_1

    sget-object v7, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_E:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    if-eq v1, v7, :cond_1

    sget-object v7, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_8:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    if-eq v1, v7, :cond_1

    sget-object v7, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_13:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    if-eq v1, v7, :cond_1

    .line 55
    :cond_0
    :goto_0
    return-object v6

    .line 37
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/Result;->getText()Ljava/lang/String;

    move-result-object v4

    .line 38
    .local v4, "rawText":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    .line 39
    .local v2, "length":I
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_1
    if-ge v5, v2, :cond_2

    .line 40
    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 41
    .local v0, "c":C
    const/16 v7, 0x30

    if-lt v0, v7, :cond_0

    const/16 v7, 0x39

    if-gt v0, v7, :cond_0

    .line 39
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 52
    .end local v0    # "c":C
    :cond_2
    move-object v3, v4

    .line 55
    .local v3, "normalizedProductID":Ljava/lang/String;
    new-instance v6, Lcom/sec/android/app/camaftest/core/client/result/ProductParsedResult;

    invoke-direct {v6, v4, v3}, Lcom/sec/android/app/camaftest/core/client/result/ProductParsedResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
