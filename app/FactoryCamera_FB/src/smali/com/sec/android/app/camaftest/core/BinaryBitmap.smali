.class public final Lcom/sec/android/app/camaftest/core/BinaryBitmap;
.super Ljava/lang/Object;
.source "BinaryBitmap.java"


# instance fields
.field private final binarizer:Lcom/sec/android/app/camaftest/core/Binarizer;

.field private matrix:Lcom/sec/android/app/camaftest/core/common/BitMatrix;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camaftest/core/Binarizer;)V
    .locals 2
    .param p1, "binarizer"    # Lcom/sec/android/app/camaftest/core/Binarizer;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Binarizer must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->binarizer:Lcom/sec/android/app/camaftest/core/Binarizer;

    .line 38
    return-void
.end method


# virtual methods
.method public crop(IIII)Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .locals 3
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 106
    iget-object v1, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->binarizer:Lcom/sec/android/app/camaftest/core/Binarizer;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/Binarizer;->getLuminanceSource()Lcom/sec/android/app/camaftest/core/LuminanceSource;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/sec/android/app/camaftest/core/LuminanceSource;->crop(IIII)Lcom/sec/android/app/camaftest/core/LuminanceSource;

    move-result-object v0

    .line 107
    .local v0, "newSource":Lcom/sec/android/app/camaftest/core/LuminanceSource;
    new-instance v1, Lcom/sec/android/app/camaftest/core/BinaryBitmap;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->binarizer:Lcom/sec/android/app/camaftest/core/Binarizer;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/camaftest/core/Binarizer;->createBinarizer(Lcom/sec/android/app/camaftest/core/LuminanceSource;)Lcom/sec/android/app/camaftest/core/Binarizer;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;-><init>(Lcom/sec/android/app/camaftest/core/Binarizer;)V

    return-object v1
.end method

.method public getBlackMatrix()Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->matrix:Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->binarizer:Lcom/sec/android/app/camaftest/core/Binarizer;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/Binarizer;->getBlackMatrix()Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->matrix:Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->matrix:Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    return-object v0
.end method

.method public getBlackRow(ILcom/sec/android/app/camaftest/core/common/BitArray;)Lcom/sec/android/app/camaftest/core/common/BitArray;
    .locals 1
    .param p1, "y"    # I
    .param p2, "row"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->binarizer:Lcom/sec/android/app/camaftest/core/Binarizer;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/camaftest/core/Binarizer;->getBlackRow(ILcom/sec/android/app/camaftest/core/common/BitArray;)Lcom/sec/android/app/camaftest/core/common/BitArray;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->binarizer:Lcom/sec/android/app/camaftest/core/Binarizer;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/Binarizer;->getHeight()I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->binarizer:Lcom/sec/android/app/camaftest/core/Binarizer;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/Binarizer;->getWidth()I

    move-result v0

    return v0
.end method

.method public isCropSupported()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->binarizer:Lcom/sec/android/app/camaftest/core/Binarizer;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/Binarizer;->getLuminanceSource()Lcom/sec/android/app/camaftest/core/LuminanceSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/LuminanceSource;->isCropSupported()Z

    move-result v0

    return v0
.end method

.method public isRotateSupported()Z
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->binarizer:Lcom/sec/android/app/camaftest/core/Binarizer;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/Binarizer;->getLuminanceSource()Lcom/sec/android/app/camaftest/core/LuminanceSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/core/LuminanceSource;->isRotateSupported()Z

    move-result v0

    return v0
.end method

.method public rotateCounterClockwise()Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .locals 3

    .prologue
    .line 123
    iget-object v1, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->binarizer:Lcom/sec/android/app/camaftest/core/Binarizer;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/Binarizer;->getLuminanceSource()Lcom/sec/android/app/camaftest/core/LuminanceSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/LuminanceSource;->rotateCounterClockwise()Lcom/sec/android/app/camaftest/core/LuminanceSource;

    move-result-object v0

    .line 124
    .local v0, "newSource":Lcom/sec/android/app/camaftest/core/LuminanceSource;
    new-instance v1, Lcom/sec/android/app/camaftest/core/BinaryBitmap;

    iget-object v2, p0, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->binarizer:Lcom/sec/android/app/camaftest/core/Binarizer;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/camaftest/core/Binarizer;->createBinarizer(Lcom/sec/android/app/camaftest/core/LuminanceSource;)Lcom/sec/android/app/camaftest/core/Binarizer;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;-><init>(Lcom/sec/android/app/camaftest/core/Binarizer;)V

    return-object v1
.end method
