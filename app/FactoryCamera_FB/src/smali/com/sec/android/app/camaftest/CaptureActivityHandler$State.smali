.class final enum Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;
.super Ljava/lang/Enum;
.source "CaptureActivityHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camaftest/CaptureActivityHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

.field public static final enum DONE:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

.field public static final enum PREVIEW:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

.field public static final enum SUCCESS:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    new-instance v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    const-string v1, "PREVIEW"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->PREVIEW:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    .line 52
    new-instance v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->SUCCESS:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    .line 53
    new-instance v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->DONE:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    .line 50
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    sget-object v1, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->PREVIEW:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->SUCCESS:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->DONE:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->$VALUES:[Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 50
    const-class v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->$VALUES:[Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    return-object v0
.end method
