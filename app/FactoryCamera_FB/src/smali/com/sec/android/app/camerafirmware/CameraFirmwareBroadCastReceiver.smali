.class public Lcom/sec/android/app/camerafirmware/CameraFirmwareBroadCastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CameraFirmwareBroadCastReceiver.java"


# static fields
.field private static final DEFAULT_IMEI_U1:Ljava/lang/String; = "004999010640000"

.field private static final REQUEST_INSERT:Ljava/lang/String; = "com.sec.android.app.samsungapps.una.REQUEST_INSERT_PACKAGE_INFO_DATA"

.field public static final SECRET_CODE_ACTION:Ljava/lang/String; = "android.provider.Telephony.SECRET_CODE"

.field private static final TAG:Ljava/lang/String; = "CameraFirmware_broadcast"

.field private static final UNA_PREPARED_DATABASE:Ljava/lang/String; = "com.sec.android.app.samsungapps.una.COMPLETED_FILL_DATA_IN_DB"


# instance fields
.field public Feature:Lcom/sec/android/app/camera/Feature;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 41
    const-string v0, "CameraFirmware_broadcast"

    const-string v1, "CameraFirmwareBroadCastReceiver..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    invoke-static {}, Lcom/sec/android/app/camera/CameraApp;->getAppFeature()Lcom/sec/android/app/camera/Feature;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmwareBroadCastReceiver;->Feature:Lcom/sec/android/app/camera/Feature;

    .line 43
    return-void
.end method

.method private GenerateCameraFirmwarePackageName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "vendor"    # Ljava/lang/String;

    .prologue
    .line 263
    const-string v1, "com.sec.android.app.camerafirmware"

    .line 266
    .local v1, "result":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 267
    :try_start_0
    const-string v2, "CameraFirmware_broadcast"

    const-string v3, "vendor string is null!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    const-string v2, "_unknown"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 280
    :goto_0
    return-object v1

    .line 270
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 272
    const-string v2, "CameraFirmware_broadcast"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CameraFW PackageName : ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 274
    :catch_0
    move-exception v0

    .line 275
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "_unknown"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 276
    const-string v2, "CameraFirmware_broadcast"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Exception]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private ParsingCameraFirmwareVendor(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "oemcamerafirmware"    # Ljava/lang/String;

    .prologue
    .line 285
    const/4 v1, 0x0

    .line 288
    .local v1, "vendor":Ljava/lang/String;
    if-nez p1, :cond_0

    move-object v2, v1

    .line 301
    .end local v1    # "vendor":Ljava/lang/String;
    .local v2, "vendor":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 292
    .end local v2    # "vendor":Ljava/lang/String;
    .restart local v1    # "vendor":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x2

    :try_start_0
    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 293
    const-string v3, "CameraFirmware_broadcast"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Camera Infomation - Vendor : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v2, v1

    .line 301
    .end local v1    # "vendor":Ljava/lang/String;
    .restart local v2    # "vendor":Ljava/lang/String;
    goto :goto_0

    .line 294
    .end local v2    # "vendor":Ljava/lang/String;
    .restart local v1    # "vendor":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 295
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "CameraFirmware_broadcast"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Exception]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private ParsingCameraFirmwareVersion(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "oemcamerafirmware"    # Ljava/lang/String;

    .prologue
    .line 306
    const/4 v3, 0x0

    .line 309
    .local v3, "result":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 310
    :try_start_0
    const-string v8, "CameraFirmware_broadcast"

    const-string v9, "oemcamerafirmware is NULL"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v3

    .line 337
    .end local v3    # "result":Ljava/lang/String;
    .local v4, "result":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 314
    .end local v4    # "result":Ljava/lang/String;
    .restart local v3    # "result":Ljava/lang/String;
    :cond_0
    const/4 v8, 0x2

    const/4 v9, 0x3

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 315
    .local v7, "versionYear":Ljava/lang/String;
    const/4 v8, 0x3

    const/4 v9, 0x4

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 316
    .local v5, "versionMonth":Ljava/lang/String;
    const/4 v8, 0x4

    const/4 v9, 0x6

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 318
    .local v6, "versionRelease":Ljava/lang/String;
    const-string v8, "CameraFirmware_broadcast"

    const-string v9, "Camera Infomation"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    const-string v8, "CameraFirmware_broadcast"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Year : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    const-string v8, "CameraFirmware_broadcast"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Month : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    const-string v8, "CameraFirmware_broadcast"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Release : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    invoke-direct {p0, v7}, Lcom/sec/android/app/camerafirmware/CameraFirmwareBroadCastReceiver;->checkCameraFWYear(Ljava/lang/String;)I

    move-result v2

    .line 324
    .local v2, "nVersionYear":I
    invoke-direct {p0, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmwareBroadCastReceiver;->checkCameraFWMonth(Ljava/lang/String;)I

    move-result v1

    .line 327
    .local v1, "nVersionMonth":I
    const-string v8, "%d.%d.%s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    aput-object v6, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 329
    const-string v8, "CameraFirmware_broadcast"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ParsingCameraFW RESULT : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "nVersionMonth":I
    .end local v2    # "nVersionYear":I
    .end local v5    # "versionMonth":Ljava/lang/String;
    .end local v6    # "versionRelease":Ljava/lang/String;
    .end local v7    # "versionYear":Ljava/lang/String;
    :goto_1
    move-object v4, v3

    .line 337
    .end local v3    # "result":Ljava/lang/String;
    .restart local v4    # "result":Ljava/lang/String;
    goto/16 :goto_0

    .line 331
    .end local v4    # "result":Ljava/lang/String;
    .restart local v3    # "result":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 332
    .local v0, "e":Ljava/lang/Exception;
    const-string v8, "CameraFirmware_broadcast"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Exception]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private checkCameraFWMonth(Ljava/lang/String;)I
    .locals 5
    .param p1, "month"    # Ljava/lang/String;

    .prologue
    .line 227
    const/4 v1, 0x0

    .line 228
    .local v1, "returnValue":I
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "inMonth":Ljava/lang/String;
    const-string v2, "A"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 231
    const/4 v1, 0x1

    .line 258
    :goto_0
    const-string v2, "CameraFirmware_broadcast"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkCameraFWMonth Return :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    return v1

    .line 232
    :cond_0
    const-string v2, "B"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 233
    const/4 v1, 0x2

    goto :goto_0

    .line 234
    :cond_1
    const-string v2, "C"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 235
    const/4 v1, 0x3

    goto :goto_0

    .line 236
    :cond_2
    const-string v2, "D"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 237
    const/4 v1, 0x4

    goto :goto_0

    .line 238
    :cond_3
    const-string v2, "E"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 239
    const/4 v1, 0x5

    goto :goto_0

    .line 240
    :cond_4
    const-string v2, "F"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 241
    const/4 v1, 0x6

    goto :goto_0

    .line 242
    :cond_5
    const-string v2, "G"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 243
    const/4 v1, 0x7

    goto :goto_0

    .line 244
    :cond_6
    const-string v2, "H"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 245
    const/16 v1, 0x8

    goto :goto_0

    .line 246
    :cond_7
    const-string v2, "I"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 247
    const/16 v1, 0x9

    goto :goto_0

    .line 248
    :cond_8
    const-string v2, "J"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 249
    const/16 v1, 0xa

    goto :goto_0

    .line 250
    :cond_9
    const-string v2, "K"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 251
    const/16 v1, 0xb

    goto :goto_0

    .line 252
    :cond_a
    const-string v2, "L"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 253
    const/16 v1, 0xc

    goto/16 :goto_0

    .line 255
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method private checkCameraFWYear(Ljava/lang/String;)I
    .locals 5
    .param p1, "year"    # Ljava/lang/String;

    .prologue
    .line 170
    const/4 v1, 0x0

    .line 171
    .local v1, "returnValue":I
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "inMonth":Ljava/lang/String;
    const-string v2, "C"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    const/16 v1, 0x9

    .line 221
    :cond_0
    :goto_0
    const-string v2, "CameraFirmware_broadcast"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkCameraFWYear Return :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    return v1

    .line 175
    :cond_1
    const-string v2, "D"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 176
    const/16 v1, 0xa

    goto :goto_0

    .line 177
    :cond_2
    const-string v2, "E"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 178
    const/16 v1, 0xb

    goto :goto_0

    .line 179
    :cond_3
    const-string v2, "F"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 180
    const/16 v1, 0xc

    goto :goto_0

    .line 181
    :cond_4
    const-string v2, "G"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 182
    const/16 v1, 0xd

    goto :goto_0

    .line 183
    :cond_5
    const-string v2, "H"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 184
    const/16 v1, 0xe

    goto :goto_0

    .line 185
    :cond_6
    const-string v2, "I"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 186
    const/16 v1, 0xf

    goto :goto_0

    .line 187
    :cond_7
    const-string v2, "J"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 188
    const/16 v1, 0x10

    goto :goto_0

    .line 189
    :cond_8
    const-string v2, "K"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 190
    const/16 v1, 0x11

    goto :goto_0

    .line 191
    :cond_9
    const-string v2, "L"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 192
    const/16 v1, 0x12

    goto :goto_0

    .line 193
    :cond_a
    const-string v2, "M"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 194
    const/16 v1, 0x13

    goto/16 :goto_0

    .line 195
    :cond_b
    const-string v2, "N"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 196
    const/16 v1, 0x14

    goto/16 :goto_0

    .line 197
    :cond_c
    const-string v2, "O"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 198
    const/16 v1, 0x15

    goto/16 :goto_0

    .line 199
    :cond_d
    const-string v2, "P"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 200
    const/16 v1, 0x16

    goto/16 :goto_0

    .line 201
    :cond_e
    const-string v2, "Q"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 202
    const/16 v1, 0x17

    goto/16 :goto_0

    .line 203
    :cond_f
    const-string v2, "R"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 204
    const/16 v1, 0x18

    goto/16 :goto_0

    .line 205
    :cond_10
    const-string v2, "S"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 206
    const/16 v1, 0x19

    goto/16 :goto_0

    .line 207
    :cond_11
    const-string v2, "T"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 208
    const/16 v1, 0x1a

    goto/16 :goto_0

    .line 209
    :cond_12
    const-string v2, "U"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 210
    const/16 v1, 0x1b

    goto/16 :goto_0

    .line 211
    :cond_13
    const-string v2, "V"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 212
    const/16 v1, 0x1c

    goto/16 :goto_0

    .line 213
    :cond_14
    const-string v2, "X"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 214
    const/16 v1, 0x1d

    goto/16 :goto_0

    .line 215
    :cond_15
    const-string v2, "Y"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 216
    const/16 v1, 0x1e

    goto/16 :goto_0

    .line 217
    :cond_16
    const-string v2, "Z"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 218
    const/16 v1, 0x1f

    goto/16 :goto_0
.end method

.method public static isBattery()Z
    .locals 8

    .prologue
    .line 341
    const-string v2, "/sys/class/power_supply/battery/present"

    .line 343
    .local v2, "path":Ljava/lang/String;
    const/4 v3, 0x0

    .line 344
    .local v3, "temp":Ljava/lang/String;
    const/4 v1, 0x0

    .line 347
    .local v1, "isBattery":Z
    :try_start_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 349
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 351
    .local v4, "value":I
    const-string v5, "CameraFirmware_broadcast"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[WRG] isBattery value = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    if-eqz v4, :cond_0

    .line 354
    const-string v5, "CameraFirmware_broadcast"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[WRG] isBattery Good State = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    const/4 v1, 0x1

    .line 366
    .end local v4    # "value":I
    :goto_0
    return v1

    .line 360
    .restart local v4    # "value":I
    :cond_0
    const-string v5, "CameraFirmware_broadcast"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[WRG] isBattery False State = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    const/4 v1, 0x0

    goto :goto_0

    .line 363
    .end local v4    # "value":I
    :catch_0
    move-exception v0

    .line 364
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "CameraFirmware_broadcast"

    const-string v6, "[WRG] Battery problem !!!!! "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 53
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camerafirmware/CameraFirmwareBroadCastReceiver;->Feature:Lcom/sec/android/app/camera/Feature;

    if-nez v14, :cond_0

    .line 54
    invoke-static {}, Lcom/sec/android/app/camera/CameraApp;->getAppFeature()Lcom/sec/android/app/camera/Feature;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/camerafirmware/CameraFirmwareBroadCastReceiver;->Feature:Lcom/sec/android/app/camera/Feature;

    .line 57
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "action":Ljava/lang/String;
    const-string v14, "com.sec.android.app.samsungapps.una.COMPLETED_FILL_DATA_IN_DB"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 60
    const-string v14, "CameraFirmware_broadcast"

    const-string v15, "Received UNA Prepared Database"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    const/4 v8, 0x0

    .line 65
    .local v8, "imei":Ljava/lang/String;
    if-eqz v8, :cond_4

    const-string v14, "004999010640000"

    invoke-virtual {v8, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 66
    const-string v14, "CameraFirmware_broadcast"

    const-string v15, "The Default U1\'s IMEI is detected. UNASERVICE_firmware_version_check is ignored..."

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    .end local v8    # "imei":Ljava/lang/String;
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camerafirmware/CameraFirmwareBroadCastReceiver;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v14, v14, Lcom/sec/android/app/camera/Feature;->CAMERA_USE_DI_CAMERAFIRMWARE:Z

    if-nez v14, :cond_3

    .line 146
    const-string v14, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 147
    new-instance v7, Landroid/content/Intent;

    const-string v14, "android.intent.action.MAIN"

    invoke-direct {v7, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 149
    .local v7, "i":Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v14

    invoke-virtual {v14}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v6

    .line 151
    .local v6, "host":Ljava/lang/String;
    if-eqz v6, :cond_3

    .line 152
    const-string v14, "34971539"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 153
    const-string v14, "CameraFirmware_broadcast"

    const-string v15, "Secret[34971539] Eng mode will be launched"

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    const-class v14, Lcom/sec/android/app/camerafirmware/CameraFirmware;

    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v14}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 162
    :cond_2
    :goto_1
    const/high16 v14, 0x10000000

    invoke-virtual {v7, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 163
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 167
    .end local v6    # "host":Ljava/lang/String;
    .end local v7    # "i":Landroid/content/Intent;
    :cond_3
    return-void

    .line 71
    .restart local v8    # "imei":Ljava/lang/String;
    :cond_4
    :try_start_0
    new-instance v10, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v10}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    .line 74
    .local v10, "mCamFirmMgr":Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;
    new-instance v11, Landroid/content/Intent;

    const-string v14, "com.sec.android.app.samsungapps.una.REQUEST_INSERT_PACKAGE_INFO_DATA"

    const-string v15, "request_for_samsungapps_una://com.sec.android.app.camerafirmware"

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-direct {v11, v14, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 78
    .local v11, "sendIntent":Landroid/content/Intent;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 80
    .local v3, "bundle":Landroid/os/Bundle;
    const/4 v4, 0x0

    .line 89
    .local v4, "cameraFW":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v4

    .line 91
    const-string v14, "CameraFirmware_broadcast"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Camera FW : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmwareBroadCastReceiver;->ParsingCameraFirmwareVendor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 94
    .local v12, "vendor":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/app/camerafirmware/CameraFirmwareBroadCastReceiver;->ParsingCameraFirmwareVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 96
    .local v13, "version":Ljava/lang/String;
    if-nez v13, :cond_5

    .line 97
    const-string v14, "CameraFirmware_broadcast"

    const-string v15, "ParsingCameraFirmwareVersion returned NULL value. So, CameraFWVersion set 0.0.0"

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    const-string v13, "0.0.0"

    .line 102
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/app/camerafirmware/CameraFirmwareBroadCastReceiver;->GenerateCameraFirmwarePackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "CameraFWPackageName":Ljava/lang/String;
    const-string v14, "AppID"

    invoke-virtual {v3, v14, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v14, "Version"

    invoke-virtual {v3, v14, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v14, "AppContentType"

    const-string v15, "3"

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v14, "AppLoadType"

    const-string v15, "0"

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {v11, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 111
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 120
    const-string v14, "CameraFirmware_broadcast"

    const-string v15, "Send data to UNA"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 122
    .end local v1    # "CameraFWPackageName":Ljava/lang/String;
    .end local v3    # "bundle":Landroid/os/Bundle;
    .end local v4    # "cameraFW":Ljava/lang/String;
    .end local v10    # "mCamFirmMgr":Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;
    .end local v11    # "sendIntent":Landroid/content/Intent;
    .end local v12    # "vendor":Ljava/lang/String;
    .end local v13    # "version":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 123
    .local v5, "e":Ljava/lang/Exception;
    const-string v14, "CameraFirmware_broadcast"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Exception :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 128
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v8    # "imei":Ljava/lang/String;
    :cond_6
    const-string v14, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/camerafirmware/CameraFirmwareBroadCastReceiver;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v14, v14, Lcom/sec/android/app/camera/Feature;->CAMERA_FIRMWARE_UPDATE_BOOTING:Z

    if-eqz v14, :cond_1

    .line 131
    new-instance v9, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    invoke-direct {v9}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;-><init>()V

    .line 134
    .local v9, "mBooting":Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;
    invoke-virtual {v9}, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;->isNeedUpdate()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-static {}, Lcom/sec/android/app/camerafirmware/CameraFirmwareBroadCastReceiver;->isBattery()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 136
    new-instance v7, Landroid/content/Intent;

    const-string v14, "android.intent.action.MAIN"

    invoke-direct {v7, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 137
    .restart local v7    # "i":Landroid/content/Intent;
    const-class v14, Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;

    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v14}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 138
    const/high16 v14, 0x10200000

    invoke-virtual {v7, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 140
    const-string v14, "CameraFirmware_broadcast"

    const-string v15, "CameraFirmwareBroadCastReceiver.java  ACTION_BOOT_COMPLETED !!!!!!!!!!!!!!"

    invoke-static {v14, v15}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 157
    .end local v9    # "mBooting":Lcom/sec/android/app/camerafirmware/CameraFirmwareUpdate_Booting;
    .restart local v6    # "host":Ljava/lang/String;
    :cond_7
    const-string v14, "7412365"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 158
    const-string v14, "CameraFirmware_broadcast"

    const-string v15, "SecretCode[7412365] Service mode will be launched"

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    const-class v14, Lcom/sec/android/app/camerafirmware/CameraFirmware_user;

    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v14}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_1
.end method
