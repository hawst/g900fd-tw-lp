.class public final Lcom/sec/android/app/camaftest/core/oned/UPCAWriter;
.super Ljava/lang/Object;
.source "UPCAWriter.java"

# interfaces
.implements Lcom/sec/android/app/camaftest/core/Writer;


# instance fields
.field private final subWriter:Lcom/sec/android/app/camaftest/core/oned/EAN13Writer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lcom/sec/android/app/camaftest/core/oned/EAN13Writer;

    invoke-direct {v0}, Lcom/sec/android/app/camaftest/core/oned/EAN13Writer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/UPCAWriter;->subWriter:Lcom/sec/android/app/camaftest/core/oned/EAN13Writer;

    return-void
.end method

.method private static preencode(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "contents"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0xb

    .line 54
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 55
    .local v1, "length":I
    if-ne v1, v5, :cond_3

    .line 57
    const/4 v2, 0x0

    .line 58
    .local v2, "sum":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_1

    .line 59
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/lit8 v4, v3, -0x30

    rem-int/lit8 v3, v0, 0x2

    if-nez v3, :cond_0

    const/4 v3, 0x3

    :goto_1
    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_0
    const/4 v3, 0x1

    goto :goto_1

    .line 61
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    rsub-int v4, v2, 0x3e8

    rem-int/lit8 v4, v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 65
    .end local v0    # "i":I
    .end local v2    # "sum":I
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v4, 0x30

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 62
    :cond_3
    const/16 v3, 0xc

    if-eq v1, v3, :cond_2

    .line 63
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Requested contents should be 11 or 12 digits long, but got "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method


# virtual methods
.method public encode(Ljava/lang/String;Lcom/sec/android/app/camaftest/core/BarcodeFormat;II)Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .locals 6
    .param p1, "contents"    # Ljava/lang/String;
    .param p2, "format"    # Lcom/sec/android/app/camaftest/core/BarcodeFormat;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/WriterException;
        }
    .end annotation

    .prologue
    .line 38
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/camaftest/core/oned/UPCAWriter;->encode(Ljava/lang/String;Lcom/sec/android/app/camaftest/core/BarcodeFormat;IILjava/util/Map;)Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    move-result-object v0

    return-object v0
.end method

.method public encode(Ljava/lang/String;Lcom/sec/android/app/camaftest/core/BarcodeFormat;IILjava/util/Map;)Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .locals 6
    .param p1, "contents"    # Ljava/lang/String;
    .param p2, "format"    # Lcom/sec/android/app/camaftest/core/BarcodeFormat;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/camaftest/core/BarcodeFormat;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/EncodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/common/BitMatrix;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/WriterException;
        }
    .end annotation

    .prologue
    .line 43
    .local p5, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/EncodeHintType;*>;"
    sget-object v0, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_A:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    if-eq p2, v0, :cond_0

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can only encode UPC-A, but got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/UPCAWriter;->subWriter:Lcom/sec/android/app/camaftest/core/oned/EAN13Writer;

    invoke-static {p1}, Lcom/sec/android/app/camaftest/core/oned/UPCAWriter;->preencode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_13:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/camaftest/core/oned/EAN13Writer;->encode(Ljava/lang/String;Lcom/sec/android/app/camaftest/core/BarcodeFormat;IILjava/util/Map;)Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    move-result-object v0

    return-object v0
.end method
