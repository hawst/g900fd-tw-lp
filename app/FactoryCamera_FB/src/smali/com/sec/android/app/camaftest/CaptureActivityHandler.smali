.class public final Lcom/sec/android/app/camaftest/CaptureActivityHandler;
.super Landroid/os/Handler;
.source "CaptureActivityHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final activity:Lcom/sec/android/app/camaftest/CaptureActivity;

.field private final cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

.field private final decodeThread:Lcom/sec/android/app/camaftest/DecodeThread;

.field private state:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/sec/android/app/camaftest/CaptureActivity;Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/camaftest/camera/CameraManager;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/app/camaftest/CaptureActivity;
    .param p3, "characterSet"    # Ljava/lang/String;
    .param p4, "cameraManager"    # Lcom/sec/android/app/camaftest/camera/CameraManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/camaftest/CaptureActivity;",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/app/camaftest/core/BarcodeFormat;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/camaftest/camera/CameraManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    .local p2, "decodeFormats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->activity:Lcom/sec/android/app/camaftest/CaptureActivity;

    .line 61
    new-instance v0, Lcom/sec/android/app/camaftest/DecodeThread;

    new-instance v1, Lcom/sec/android/app/camaftest/ViewfinderResultPointCallback;

    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/CaptureActivity;->getViewfinderView()Lcom/sec/android/app/camaftest/ViewfinderView;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/camaftest/ViewfinderResultPointCallback;-><init>(Lcom/sec/android/app/camaftest/ViewfinderView;)V

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/sec/android/app/camaftest/DecodeThread;-><init>(Lcom/sec/android/app/camaftest/CaptureActivity;Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/camaftest/core/ResultPointCallback;)V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->decodeThread:Lcom/sec/android/app/camaftest/DecodeThread;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->decodeThread:Lcom/sec/android/app/camaftest/DecodeThread;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/DecodeThread;->start()V

    .line 64
    sget-object v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->SUCCESS:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    iput-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->state:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    .line 67
    iput-object p4, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    .line 68
    invoke-virtual {p4}, Lcom/sec/android/app/camaftest/camera/CameraManager;->startPreview()V

    .line 69
    return-void
.end method

.method private handleDecode(Lcom/sec/android/app/camaftest/core/Result;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "rawResult"    # Lcom/sec/android/app/camaftest/core/Result;
    .param p2, "barcode"    # Landroid/graphics/Bitmap;

    .prologue
    .line 146
    sget-object v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->SUCCESS:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    iput-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->state:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/camera/CameraManager;->setAutoFocusAvailable()V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->activity:Lcom/sec/android/app/camaftest/CaptureActivity;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/camaftest/CaptureActivity;->handleDecode(Lcom/sec/android/app/camaftest/core/Result;Landroid/graphics/Bitmap;)V

    .line 149
    return-void
.end method

.method private restartPreviewAndDecode()V
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->state:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    sget-object v1, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->SUCCESS:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    if-ne v0, v1, :cond_0

    .line 138
    sget-object v0, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->PREVIEW:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    iput-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->state:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->decodeThread:Lcom/sec/android/app/camaftest/DecodeThread;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/DecodeThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    const v2, 0x7f090001

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camaftest/camera/CameraManager;->requestPreviewFrame(Landroid/os/Handler;I)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/camaftest/camera/CameraManager;->requestAutoFocus(Landroid/os/Handler;I)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->activity:Lcom/sec/android/app/camaftest/CaptureActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/camaftest/CaptureActivity;->drawViewfinder()V

    .line 143
    :cond_0
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x0

    .line 73
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 117
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 78
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->state:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    sget-object v6, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->PREVIEW:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    if-ne v5, v6, :cond_0

    .line 79
    iget-object v5, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v5}, Lcom/sec/android/app/camaftest/camera/CameraManager;->isAutoFocusAvailable()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 80
    iget-object v5, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    const/high16 v6, 0x7f090000

    invoke-virtual {v5, p0, v6}, Lcom/sec/android/app/camaftest/camera/CameraManager;->requestAutoFocus(Landroid/os/Handler;I)V

    goto :goto_0

    .line 82
    :cond_1
    invoke-direct {p0, v0, v0}, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->handleDecode(Lcom/sec/android/app/camaftest/core/Result;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 87
    :pswitch_2
    sget-object v5, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->TAG:Ljava/lang/String;

    const-string v6, "Got restart preview message"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->restartPreviewAndDecode()V

    goto :goto_0

    .line 91
    :pswitch_3
    sget-object v5, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->TAG:Ljava/lang/String;

    const-string v6, "Got decode succeeded message"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 93
    .local v1, "bundle":Landroid/os/Bundle;
    if-nez v1, :cond_2

    move-object v2, v0

    .line 94
    .local v2, "data":[B
    :goto_1
    if-nez v1, :cond_3

    .line 96
    .local v0, "barcode":Landroid/graphics/Bitmap;
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->activity:Lcom/sec/android/app/camaftest/CaptureActivity;

    iget-object v6, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v6}, Lcom/sec/android/app/camaftest/camera/CameraManager;->getLastOrientation()I

    move-result v6

    invoke-virtual {v5, v2, v6}, Lcom/sec/android/app/camaftest/CaptureActivity;->storeImage([BI)V

    .line 97
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lcom/sec/android/app/camaftest/core/Result;

    invoke-direct {p0, v5, v0}, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->handleDecode(Lcom/sec/android/app/camaftest/core/Result;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 93
    .end local v0    # "barcode":Landroid/graphics/Bitmap;
    .end local v2    # "data":[B
    :cond_2
    const-string v5, "preview_jpg_data"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    goto :goto_1

    .line 94
    .restart local v2    # "data":[B
    :cond_3
    const-string v5, "barcode_bitmap"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    move-object v0, v5

    goto :goto_2

    .line 100
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "data":[B
    :pswitch_4
    iget-object v5, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->state:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    sget-object v6, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->PREVIEW:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    if-ne v5, v6, :cond_0

    .line 101
    iget-object v5, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    iget-object v6, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->decodeThread:Lcom/sec/android/app/camaftest/DecodeThread;

    invoke-virtual {v6}, Lcom/sec/android/app/camaftest/DecodeThread;->getHandler()Landroid/os/Handler;

    move-result-object v6

    const v7, 0x7f090001

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/camaftest/camera/CameraManager;->requestPreviewFrame(Landroid/os/Handler;I)V

    goto :goto_0

    .line 105
    :pswitch_5
    sget-object v5, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->TAG:Ljava/lang/String;

    const-string v6, "Got return scan result message"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v6, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->activity:Lcom/sec/android/app/camaftest/CaptureActivity;

    const/4 v7, -0x1

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Landroid/content/Intent;

    invoke-virtual {v6, v7, v5}, Lcom/sec/android/app/camaftest/CaptureActivity;->setResult(ILandroid/content/Intent;)V

    .line 107
    iget-object v5, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->activity:Lcom/sec/android/app/camaftest/CaptureActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/camaftest/CaptureActivity;->finish()V

    goto/16 :goto_0

    .line 110
    :pswitch_6
    sget-object v5, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->TAG:Ljava/lang/String;

    const-string v6, "Got product query message"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    .line 112
    .local v4, "url":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 113
    .local v3, "intent":Landroid/content/Intent;
    const/high16 v5, 0x80000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 114
    iget-object v5, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->activity:Lcom/sec/android/app/camaftest/CaptureActivity;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/camaftest/CaptureActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x7f090000
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public quitSynchronously()V
    .locals 4

    .prologue
    .line 120
    sget-object v1, Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;->DONE:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    iput-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->state:Lcom/sec/android/app/camaftest/CaptureActivityHandler$State;

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->cameraManager:Lcom/sec/android/app/camaftest/camera/CameraManager;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/camera/CameraManager;->stopPreview()V

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->decodeThread:Lcom/sec/android/app/camaftest/DecodeThread;

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/DecodeThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    const v2, 0x7f090005

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 123
    .local v0, "quit":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 126
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->decodeThread:Lcom/sec/android/app/camaftest/DecodeThread;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/camaftest/DecodeThread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_0
    const v1, 0x7f090003

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->removeMessages(I)V

    .line 133
    const v1, 0x7f090002

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camaftest/CaptureActivityHandler;->removeMessages(I)V

    .line 134
    return-void

    .line 127
    :catch_0
    move-exception v1

    goto :goto_0
.end method
