.class final Lcom/sec/android/app/camaftest/ViewfinderResultPointCallback;
.super Ljava/lang/Object;
.source "ViewfinderResultPointCallback.java"

# interfaces
.implements Lcom/sec/android/app/camaftest/core/ResultPointCallback;


# instance fields
.field private final viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camaftest/ViewfinderView;)V
    .locals 0
    .param p1, "viewfinderView"    # Lcom/sec/android/app/camaftest/ViewfinderView;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/camaftest/ViewfinderResultPointCallback;->viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;

    .line 28
    return-void
.end method


# virtual methods
.method public foundPossibleResultPoint(Lcom/sec/android/app/camaftest/core/ResultPoint;)V
    .locals 1
    .param p1, "point"    # Lcom/sec/android/app/camaftest/core/ResultPoint;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/camaftest/ViewfinderResultPointCallback;->viewfinderView:Lcom/sec/android/app/camaftest/ViewfinderView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/camaftest/ViewfinderView;->addPossibleResultPoint(Lcom/sec/android/app/camaftest/core/ResultPoint;)V

    .line 33
    return-void
.end method
