.class Lcom/sec/android/app/camerafirmware/CameraFirmware$5;
.super Ljava/lang/Object;
.source "CameraFirmware.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/camerafirmware/CameraFirmware;->DialogPopup(I)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camerafirmware/CameraFirmware;)V
    .locals 0

    .prologue
    .line 1172
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog1"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v1, 0x0

    .line 1174
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mForceUpdate:Z
    invoke-static {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$900(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1175
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mForceUpdate:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$902(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z

    .line 1176
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    sget-object v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_FORCE:[B

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->setManageMode([B)V

    .line 1187
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->showUpdateProgress()V

    .line 1188
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->startUpdateThread()V

    .line 1189
    return-void

    .line 1177
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompForceUpdate:Z
    invoke-static {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$1000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1178
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompForceUpdate:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$1002(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z

    .line 1179
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    sget-object v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_COMP_FIRMWARE_FORCE:[B

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->setManageMode([B)V

    goto :goto_0

    .line 1180
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # getter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdate:Z
    invoke-static {v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$300(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1181
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    # setter for: Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdate:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->access$302(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z

    .line 1182
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    sget-object v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_COMP_FIRMWARE_UPDATE:[B

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->setManageMode([B)V

    goto :goto_0

    .line 1184
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;->this$0:Lcom/sec/android/app/camerafirmware/CameraFirmware;

    iget-object v0, v0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    sget-object v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_UPDATE:[B

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->setManageMode([B)V

    goto :goto_0
.end method
