.class Lcom/sec/android/app/camera/CamcorderPreviewTest$2;
.super Ljava/lang/Object;
.source "CamcorderPreviewTest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/camera/CamcorderPreviewTest;->doStartVideoRecordingAsync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V
    .locals 0

    .prologue
    .line 628
    iput-object p1, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 632
    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;
    invoke-static {v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$600(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/media/MediaRecorder;

    move-result-object v2

    if-nez v2, :cond_0

    .line 633
    sget-object v2, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v3, "MediaRecorder is not initialized."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    const/4 v3, -0x1

    # setter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I
    invoke-static {v2, v3}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$702(Lcom/sec/android/app/camera/CamcorderPreviewTest;I)I

    .line 664
    :goto_0
    return-void

    .line 639
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # setter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorderRecording:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$802(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z

    .line 643
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mMediaRecorder:Landroid/media/MediaRecorder;
    invoke-static {v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$600(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/media/MediaRecorder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->start()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 662
    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    const/4 v3, 0x3

    # setter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mRecordingState:I
    invoke-static {v2, v3}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$702(Lcom/sec/android/app/camera/CamcorderPreviewTest;I)I

    goto :goto_0

    .line 644
    :catch_0
    move-exception v0

    .line 645
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v2, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v3, "Could not start media recorder. "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 646
    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # invokes: Lcom/sec/android/app/camera/CamcorderPreviewTest;->releaseMediaRecorder()V
    invoke-static {v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$900(Lcom/sec/android/app/camera/CamcorderPreviewTest;)V

    .line 649
    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    invoke-virtual {v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->doStopVideoRecordingSync()V

    .line 651
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # getter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1000(Lcom/sec/android/app/camera/CamcorderPreviewTest;)Landroid/hardware/Camera;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera;->startPreview()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 657
    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    # setter for: Lcom/sec/android/app/camera/CamcorderPreviewTest;->mPreviewing:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$1102(Lcom/sec/android/app/camera/CamcorderPreviewTest;Z)Z

    goto :goto_0

    .line 652
    :catch_1
    move-exception v1

    .line 653
    .local v1, "ex":Ljava/lang/Throwable;
    sget-object v2, Lcom/sec/android/app/camera/CamcorderPreviewTest;->TAG:Ljava/lang/String;

    const-string v3, "exception while startPreview"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 654
    iget-object v2, p0, Lcom/sec/android/app/camera/CamcorderPreviewTest$2;->this$0:Lcom/sec/android/app/camera/CamcorderPreviewTest;

    const v3, 0x7f0a000a

    # invokes: Lcom/sec/android/app/camera/CamcorderPreviewTest;->dialogErrorPopup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/camera/CamcorderPreviewTest;->access$200(Lcom/sec/android/app/camera/CamcorderPreviewTest;I)V

    goto :goto_0
.end method
