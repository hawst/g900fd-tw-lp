.class public abstract Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AbstractExpandedDecoder;
.super Ljava/lang/Object;
.source "AbstractExpandedDecoder.java"


# instance fields
.field private final generalDecoder:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;

.field private final information:Lcom/sec/android/app/camaftest/core/common/BitArray;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camaftest/core/common/BitArray;)V
    .locals 1
    .param p1, "information"    # Lcom/sec/android/app/camaftest/core/common/BitArray;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AbstractExpandedDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    .line 44
    new-instance v0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;

    invoke-direct {v0, p1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;)V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AbstractExpandedDecoder;->generalDecoder:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;

    .line 45
    return-void
.end method

.method public static createDecoder(Lcom/sec/android/app/camaftest/core/common/BitArray;)Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AbstractExpandedDecoder;
    .locals 6
    .param p0, "information"    # Lcom/sec/android/app/camaftest/core/common/BitArray;

    .prologue
    const/4 v4, 0x1

    .line 58
    invoke-virtual {p0, v4}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 59
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI01AndOtherAIs;

    invoke-direct {v3, p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI01AndOtherAIs;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;)V

    .line 99
    :goto_0
    return-object v3

    .line 61
    :cond_0
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/camaftest/core/common/BitArray;->get(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 62
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AnyAIDecoder;

    invoke-direct {v3, p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AnyAIDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;)V

    goto :goto_0

    .line 65
    :cond_1
    const/4 v3, 0x4

    invoke-static {p0, v4, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(Lcom/sec/android/app/camaftest/core/common/BitArray;II)I

    move-result v1

    .line 67
    .local v1, "fourBitEncodationMethod":I
    packed-switch v1, :pswitch_data_0

    .line 74
    const/4 v3, 0x5

    invoke-static {p0, v4, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(Lcom/sec/android/app/camaftest/core/common/BitArray;II)I

    move-result v0

    .line 75
    .local v0, "fiveBitEncodationMethod":I
    packed-switch v0, :pswitch_data_1

    .line 82
    const/4 v3, 0x7

    invoke-static {p0, v4, v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;->extractNumericValueFromBitArray(Lcom/sec/android/app/camaftest/core/common/BitArray;II)I

    move-result v2

    .line 83
    .local v2, "sevenBitEncodationMethod":I
    packed-switch v2, :pswitch_data_2

    .line 102
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unknown decoder: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 69
    .end local v0    # "fiveBitEncodationMethod":I
    .end local v2    # "sevenBitEncodationMethod":I
    :pswitch_0
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013103decoder;

    invoke-direct {v3, p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013103decoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;)V

    goto :goto_0

    .line 71
    :pswitch_1
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI01320xDecoder;

    invoke-direct {v3, p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI01320xDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;)V

    goto :goto_0

    .line 77
    .restart local v0    # "fiveBitEncodationMethod":I
    :pswitch_2
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI01392xDecoder;

    invoke-direct {v3, p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI01392xDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;)V

    goto :goto_0

    .line 79
    :pswitch_3
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI01393xDecoder;

    invoke-direct {v3, p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI01393xDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;)V

    goto :goto_0

    .line 85
    .restart local v2    # "sevenBitEncodationMethod":I
    :pswitch_4
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;

    const-string v4, "310"

    const-string v5, "11"

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 87
    :pswitch_5
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;

    const-string v4, "320"

    const-string v5, "11"

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :pswitch_6
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;

    const-string v4, "310"

    const-string v5, "13"

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :pswitch_7
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;

    const-string v4, "320"

    const-string v5, "13"

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :pswitch_8
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;

    const-string v4, "310"

    const-string v5, "15"

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 95
    :pswitch_9
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;

    const-string v4, "320"

    const-string v5, "15"

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 97
    :pswitch_a
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;

    const-string v4, "310"

    const-string v5, "17"

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 99
    :pswitch_b
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;

    const-string v4, "320"

    const-string v5, "17"

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0x1xDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 67
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 75
    :pswitch_data_1
    .packed-switch 0xc
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 83
    :pswitch_data_2
    .packed-switch 0x38
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method


# virtual methods
.method protected final getGeneralDecoder()Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AbstractExpandedDecoder;->generalDecoder:Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/GeneralAppIdDecoder;

    return-object v0
.end method

.method protected final getInformation()Lcom/sec/android/app/camaftest/core/common/BitArray;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AbstractExpandedDecoder;->information:Lcom/sec/android/app/camaftest/core/common/BitArray;

    return-object v0
.end method

.method public abstract parseInformation()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation
.end method
