.class public final Lcom/sec/android/app/camaftest/core/client/result/ISBNResultParser;
.super Lcom/sec/android/app/camaftest/core/client/result/ResultParser;
.source "ISBNResultParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/client/result/ResultParser;-><init>()V

    return-void
.end method


# virtual methods
.method public parse(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/client/result/ISBNParsedResult;
    .locals 5
    .param p1, "result"    # Lcom/sec/android/app/camaftest/core/Result;

    .prologue
    const/4 v3, 0x0

    .line 34
    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/Result;->getBarcodeFormat()Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    move-result-object v0

    .line 35
    .local v0, "format":Lcom/sec/android/app/camaftest/core/BarcodeFormat;
    sget-object v4, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_13:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    if-eq v0, v4, :cond_1

    .line 47
    :cond_0
    :goto_0
    return-object v3

    .line 38
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/Result;->getText()Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "rawText":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 40
    .local v1, "length":I
    const/16 v4, 0xd

    if-ne v1, v4, :cond_0

    .line 43
    const-string v4, "978"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "979"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 47
    :cond_2
    new-instance v3, Lcom/sec/android/app/camaftest/core/client/result/ISBNParsedResult;

    invoke-direct {v3, v2}, Lcom/sec/android/app/camaftest/core/client/result/ISBNParsedResult;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic parse(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/camaftest/core/Result;

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/sec/android/app/camaftest/core/client/result/ISBNResultParser;->parse(Lcom/sec/android/app/camaftest/core/Result;)Lcom/sec/android/app/camaftest/core/client/result/ISBNParsedResult;

    move-result-object v0

    return-object v0
.end method
