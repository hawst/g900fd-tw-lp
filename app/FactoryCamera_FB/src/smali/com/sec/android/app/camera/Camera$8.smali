.class Lcom/sec/android/app/camera/Camera$8;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/camera/Camera;->setViewFinder(IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/Camera;

.field final synthetic val$wallTimeStart:J

.field final synthetic val$watchDogSync:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/Camera;Ljava/lang/Object;J)V
    .locals 1

    .prologue
    .line 2623
    iput-object p1, p0, Lcom/sec/android/app/camera/Camera$8;->this$0:Lcom/sec/android/app/camera/Camera;

    iput-object p2, p0, Lcom/sec/android/app/camera/Camera$8;->val$watchDogSync:Ljava/lang/Object;

    iput-wide p3, p0, Lcom/sec/android/app/camera/Camera$8;->val$wallTimeStart:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 2625
    const-string v2, "FactoryCamera"

    const-string v3, "watchDog thread run()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2626
    const/4 v1, 0x1

    .line 2629
    .local v1, "next_warning":I
    :cond_0
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/camera/Camera$8;->val$watchDogSync:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2630
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$8;->val$watchDogSync:Ljava/lang/Object;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 2631
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/camera/Camera$8;->this$0:Lcom/sec/android/app/camera/Camera;

    # getter for: Lcom/sec/android/app/camera/Camera;->mPreviewing:Z
    invoke-static {v2}, Lcom/sec/android/app/camera/Camera;->access$5100(Lcom/sec/android/app/camera/Camera;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2655
    return-void

    .line 2631
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 2632
    :catch_0
    move-exception v2

    goto :goto_1

    .line 2638
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/app/camera/Camera$8;->val$wallTimeStart:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    div-int/lit16 v0, v2, 0x3e8

    .line 2639
    .local v0, "delay":I
    if-lt v0, v1, :cond_0

    .line 2640
    const/16 v2, 0x78

    if-ge v0, v2, :cond_2

    .line 2641
    const-string v2, "FactoryCamera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "preview hasn\'t started yet in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " seconds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2645
    :goto_2
    const/16 v2, 0x3c

    if-ge v1, v2, :cond_3

    .line 2646
    shl-int/lit8 v1, v1, 0x1

    .line 2647
    const/16 v2, 0x10

    if-ne v1, v2, :cond_0

    .line 2648
    const/16 v1, 0xf

    goto :goto_0

    .line 2643
    :cond_2
    const-string v2, "FactoryCamera"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "preview hasn\'t started yet in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    div-int/lit8 v4, v0, 0x3c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " minutes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2651
    :cond_3
    add-int/lit8 v1, v1, 0x3c

    goto :goto_0
.end method
