.class public Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;
.super Landroid/app/DialogFragment;
.source "PostViewTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/PostViewTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "keyAlertDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 355
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;
    .locals 1
    .param p0, "title"    # I

    .prologue
    .line 357
    new-instance v0, Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;

    invoke-direct {v0}, Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;-><init>()V

    .line 358
    .local v0, "alertDialogFragment":Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 363
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Side Key Test Fail\nRe-press to key"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Check"

    new-instance v2, Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment$2;-><init>(Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment$1;-><init>(Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
