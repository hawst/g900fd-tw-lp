.class public Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;
.super Landroid/app/Activity;
.source "VideoPlayTest_SurfaceHolder.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final SHOW_PROGRESS:I = 0x2

.field public static TAG:Ljava/lang/String; = null

.field public static VOLUME_DEFAULT:I = 0x0

.field public static final VOLUME_MAX:I = 0xf


# instance fields
.field private bPlayComplet:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private mButtonsLayout:Landroid/widget/RelativeLayout;

.field mComplete:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mCurrentTime:Landroid/widget/TextView;

.field private mDragging:Z

.field private mEndTime:Landroid/widget/TextView;

.field private mFail:Landroid/widget/Button;

.field mFormatBuilder:Ljava/lang/StringBuilder;

.field mFormatter:Ljava/util/Formatter;

.field private mHandler:Landroid/os/Handler;

.field private mHolder:Landroid/view/SurfaceHolder;

.field private mPass:Landroid/widget/Button;

.field private mPlay:Landroid/widget/Button;

.field mPlayer:Landroid/media/MediaPlayer;

.field private mPreview:Landroid/view/SurfaceView;

.field private mProgress:Landroid/widget/ProgressBar;

.field private mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mUri:Landroid/net/Uri;

.field protected mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "VideoPlayTest_SurfaceHolder"

    sput-object v0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->TAG:Ljava/lang/String;

    .line 63
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->VOLUME_DEFAULT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->bPlayComplet:Z

    .line 174
    new-instance v0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$1;-><init>(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mComplete:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 321
    new-instance v0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$3;-><init>(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mHandler:Landroid/os/Handler;

    .line 341
    new-instance v0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$4;-><init>(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)V

    iput-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mProgress:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlay:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPass:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mFail:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->bPlayComplet:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->setProgress()I

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mDragging:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mDragging:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mCurrentTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;
    .param p1, "x1"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->stringForTime(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setProgress()I
    .locals 10

    .prologue
    .line 284
    iget-object v5, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mDragging:Z

    if-eqz v5, :cond_2

    .line 285
    :cond_0
    const/4 v4, 0x0

    .line 308
    :cond_1
    :goto_0
    return v4

    .line 287
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v4

    .line 288
    .local v4, "position":I
    iget-object v5, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    .line 289
    .local v0, "duration":I
    const/4 v1, 0x0

    .line 291
    .local v1, "percent":I
    iget-object v5, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mProgress:Landroid/widget/ProgressBar;

    if-eqz v5, :cond_4

    .line 292
    if-lez v0, :cond_3

    .line 294
    const-wide/16 v6, 0x3e8

    int-to-long v8, v4

    mul-long/2addr v6, v8

    int-to-long v8, v0

    div-long v2, v6, v8

    .line 295
    .local v2, "pos":J
    iget-object v5, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mProgress:Landroid/widget/ProgressBar;

    long-to-int v6, v2

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 297
    .end local v2    # "pos":J
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    .line 300
    :cond_4
    sget-object v5, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setProgress, position : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", duration: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", percent : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    iget-object v5, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mEndTime:Landroid/widget/TextView;

    if-eqz v5, :cond_5

    .line 303
    iget-object v5, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->stringForTime(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v5, :cond_1

    .line 306
    iget-object v5, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v4}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->stringForTime(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static setSystemKeyBlock(Landroid/content/ComponentName;I)V
    .locals 4
    .param p0, "componentName"    # Landroid/content/ComponentName;
    .param p1, "keyCode"    # I

    .prologue
    .line 382
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 384
    .local v1, "wm":Landroid/view/IWindowManager;
    const/4 v2, 0x1

    :try_start_0
    invoke-interface {v1, p1, p0, v2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    :goto_0
    return-void

    .line 385
    :catch_0
    move-exception v0

    .line 386
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->TAG:Ljava/lang/String;

    const-string v3, "setSystemKeyBlock exception!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private stringForTime(I)Ljava/lang/String;
    .locals 8
    .param p1, "timeMs"    # I

    .prologue
    const/4 v7, 0x0

    .line 312
    div-int/lit16 v2, p1, 0x3e8

    .line 313
    .local v2, "totalSeconds":I
    rem-int/lit8 v1, v2, 0x3c

    .line 314
    .local v1, "seconds":I
    div-int/lit8 v3, v2, 0x3c

    rem-int/lit8 v0, v3, 0x3c

    .line 316
    .local v0, "minutes":I
    iget-object v3, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 318
    iget-object v3, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mFormatter:Ljava/util/Formatter;

    const-string v4, "%02d:%02d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 251
    sget-object v2, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->TAG:Ljava/lang/String;

    const-string v3, "onClick()"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    iget-boolean v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->bPlayComplet:Z

    if-nez v2, :cond_0

    .line 253
    const-string v2, "Play is not complete."

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 281
    :goto_0
    return-void

    .line 257
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 259
    :pswitch_1
    sget-object v2, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->TAG:Ljava/lang/String;

    const-string v3, "onClick() - Pass"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 262
    .local v1, "intent1":Landroid/content/Intent;
    const-string v2, "camera_id"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 263
    const-string v2, "ommision_test"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 264
    const-string v2, "torch_on"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 265
    const-string v2, "camcorder_preview_test"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 266
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->setResult(ILandroid/content/Intent;)V

    .line 267
    iput-boolean v4, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->bPlayComplet:Z

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->finish()V

    goto :goto_0

    .line 271
    .end local v1    # "intent1":Landroid/content/Intent;
    :pswitch_2
    sget-object v2, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->TAG:Ljava/lang/String;

    const-string v3, "onClick() - Fail"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 273
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "CAMERA FAIL"

    const-string v3, "FRONT CAMERA VIDEO PLAYBACK"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 274
    invoke-virtual {p0, v4, v0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->setResult(ILandroid/content/Intent;)V

    .line 275
    iput-boolean v4, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->bPlayComplet:Z

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->finish()V

    goto :goto_0

    .line 257
    nop

    :pswitch_data_0
    .packed-switch 0x7f090042
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x3

    .line 70
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    const v1, 0x7f030007

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->setContentView(I)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/16 v2, 0x1a

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/16 v2, 0xbb

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 78
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mUri:Landroid/net/Uri;

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mUri:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 81
    sget-object v1, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->TAG:Ljava/lang/String;

    const-string v2, "VideoPlayer onCreate() fail mUri = NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->finish()V

    .line 85
    :cond_0
    sget-object v1, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VideoPlayer mUri = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    const v1, 0x7f090040

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceView;

    iput-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPreview:Landroid/view/SurfaceView;

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPreview:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mHolder:Landroid/view/SurfaceHolder;

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v4}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 92
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mAudioManager:Landroid/media/AudioManager;

    .line 94
    const v1, 0x7f090041

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mButtonsLayout:Landroid/widget/RelativeLayout;

    .line 95
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 96
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "orientation_reverse"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mButtonsLayout:Landroid/widget/RelativeLayout;

    const/high16 v2, 0x43340000    # 180.0f

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setRotation(F)V

    .line 100
    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 166
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    .line 172
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 234
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 240
    packed-switch p1, :pswitch_data_0

    .line 248
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_0
    return v1

    .line 242
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->isShowFailDialog()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    const-string v1, "FRONT CAMERA VIDEO PLAYBACK"

    invoke-static {v1}, Lcom/sec/android/app/camera/Camera$FailDialogFragment;->newInstance(Ljava/lang/String;)Lcom/sec/android/app/camera/Camera$FailDialogFragment;

    move-result-object v0

    .line 244
    .local v0, "alertDialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 246
    .end local v0    # "alertDialog":Landroid/app/DialogFragment;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0xbb
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 146
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    sget-object v0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->TAG:Ljava/lang/String;

    const-string v1, "releaseWakelock"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 153
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 162
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    sget v2, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->VOLUME_DEFAULT:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 163
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 188
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 190
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 191
    .local v0, "pm":Landroid/os/PowerManager;
    const v2, 0x3000001a

    sget-object v3, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 193
    iget-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 195
    const v2, 0x7f090043

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlay:Landroid/widget/Button;

    .line 196
    const v2, 0x7f090042

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPass:Landroid/widget/Button;

    .line 197
    const v2, 0x7f090044

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mFail:Landroid/widget/Button;

    .line 198
    iget-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlay:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder$2;-><init>(Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    const v2, 0x7f090046

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mProgress:Landroid/widget/ProgressBar;

    .line 217
    iget-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mProgress:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_1

    .line 218
    iget-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mProgress:Landroid/widget/ProgressBar;

    instance-of v2, v2, Landroid/widget/SeekBar;

    if-eqz v2, :cond_0

    .line 219
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mProgress:Landroid/widget/ProgressBar;

    check-cast v1, Landroid/widget/SeekBar;

    .line 220
    .local v1, "seeker":Landroid/widget/SeekBar;
    iget-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 222
    .end local v1    # "seeker":Landroid/widget/SeekBar;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mProgress:Landroid/widget/ProgressBar;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 225
    :cond_1
    const v2, 0x7f090047

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mEndTime:Landroid/widget/TextView;

    .line 226
    const v2, 0x7f090045

    invoke-virtual {p0, v2}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mCurrentTime:Landroid/widget/TextView;

    .line 228
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mFormatBuilder:Ljava/lang/StringBuilder;

    .line 229
    new-instance v2, Ljava/util/Formatter;

    iget-object v3, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mFormatter:Ljava/util/Formatter;

    .line 230
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->setProgress()I

    .line 105
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 6
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v5, 0x0

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_0

    .line 110
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    .line 115
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, p0, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 117
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mComplete:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->VOLUME_DEFAULT:I

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_1

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    const/16 v3, 0xf

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 127
    invoke-direct {p0}, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->setProgress()I

    .line 128
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 133
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlay:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f020012

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setBackgroundResource(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_3
    return-void

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    goto :goto_0

    .line 130
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/camera/VideoPlayTest_SurfaceHolder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_3

    .line 133
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const v1, 0x7f020013

    goto :goto_2
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 142
    return-void
.end method
