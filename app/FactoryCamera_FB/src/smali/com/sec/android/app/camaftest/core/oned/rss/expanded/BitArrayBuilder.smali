.class final Lcom/sec/android/app/camaftest/core/oned/rss/expanded/BitArrayBuilder;
.super Ljava/lang/Object;
.source "BitArrayBuilder.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method

.method static buildBitArray(Ljava/util/List;)Lcom/sec/android/app/camaftest/core/common/BitArray;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;",
            ">;)",
            "Lcom/sec/android/app/camaftest/core/common/BitArray;"
        }
    .end annotation

    .prologue
    .local p0, "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;>;"
    const/4 v12, 0x1

    .line 44
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v11

    shl-int/lit8 v11, v11, 0x1

    add-int/lit8 v2, v11, -0x1

    .line 45
    .local v2, "charNumber":I
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-interface {p0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;

    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;->getRightChar()Lcom/sec/android/app/camaftest/core/oned/rss/DataCharacter;

    move-result-object v11

    if-nez v11, :cond_0

    .line 46
    add-int/lit8 v2, v2, -0x1

    .line 49
    :cond_0
    mul-int/lit8 v10, v2, 0xc

    .line 51
    .local v10, "size":I
    new-instance v1, Lcom/sec/android/app/camaftest/core/common/BitArray;

    invoke-direct {v1, v10}, Lcom/sec/android/app/camaftest/core/common/BitArray;-><init>(I)V

    .line 52
    .local v1, "binary":Lcom/sec/android/app/camaftest/core/common/BitArray;
    const/4 v0, 0x0

    .line 54
    .local v0, "accPos":I
    const/4 v11, 0x0

    invoke-interface {p0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;

    .line 55
    .local v4, "firstPair":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;
    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;->getRightChar()Lcom/sec/android/app/camaftest/core/oned/rss/DataCharacter;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/oned/rss/DataCharacter;->getValue()I

    move-result v5

    .line 56
    .local v5, "firstValue":I
    const/16 v6, 0xb

    .local v6, "i":I
    :goto_0
    if-ltz v6, :cond_2

    .line 57
    shl-int v11, v12, v6

    and-int/2addr v11, v5

    if-eqz v11, :cond_1

    .line 58
    invoke-virtual {v1, v0}, Lcom/sec/android/app/camaftest/core/common/BitArray;->set(I)V

    .line 60
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 56
    add-int/lit8 v6, v6, -0x1

    goto :goto_0

    .line 63
    :cond_2
    const/4 v6, 0x1

    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v11

    if-ge v6, v11, :cond_7

    .line 64
    invoke-interface {p0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;

    .line 66
    .local v3, "currentPair":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;
    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;->getLeftChar()Lcom/sec/android/app/camaftest/core/oned/rss/DataCharacter;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/oned/rss/DataCharacter;->getValue()I

    move-result v8

    .line 67
    .local v8, "leftValue":I
    const/16 v7, 0xb

    .local v7, "j":I
    :goto_2
    if-ltz v7, :cond_4

    .line 68
    shl-int v11, v12, v7

    and-int/2addr v11, v8

    if-eqz v11, :cond_3

    .line 69
    invoke-virtual {v1, v0}, Lcom/sec/android/app/camaftest/core/common/BitArray;->set(I)V

    .line 71
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 67
    add-int/lit8 v7, v7, -0x1

    goto :goto_2

    .line 74
    :cond_4
    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;->getRightChar()Lcom/sec/android/app/camaftest/core/oned/rss/DataCharacter;

    move-result-object v11

    if-eqz v11, :cond_6

    .line 75
    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;->getRightChar()Lcom/sec/android/app/camaftest/core/oned/rss/DataCharacter;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/camaftest/core/oned/rss/DataCharacter;->getValue()I

    move-result v9

    .line 76
    .local v9, "rightValue":I
    const/16 v7, 0xb

    :goto_3
    if-ltz v7, :cond_6

    .line 77
    shl-int v11, v12, v7

    and-int/2addr v11, v9

    if-eqz v11, :cond_5

    .line 78
    invoke-virtual {v1, v0}, Lcom/sec/android/app/camaftest/core/common/BitArray;->set(I)V

    .line 80
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 76
    add-int/lit8 v7, v7, -0x1

    goto :goto_3

    .line 63
    .end local v9    # "rightValue":I
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 84
    .end local v3    # "currentPair":Lcom/sec/android/app/camaftest/core/oned/rss/expanded/ExpandedPair;
    .end local v7    # "j":I
    .end local v8    # "leftValue":I
    :cond_7
    return-object v1
.end method
