.class public Lcom/sec/android/app/camerafirmware/CameraFirmware;
.super Landroid/app/Activity;
.source "CameraFirmware.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CameraFirmware"

.field private static final TOAST_FINISH_DUMP:I = 0x3ea

.field private static final TOAST_FINISH_UPDATE:I = 0x3e9

.field private static final TOAST_PROCESS:I = 0x3e8

.field private static final mSalesCode:Ljava/lang/String;


# instance fields
.field Feature:Lcom/sec/android/app/camera/Feature;

.field protected PREF_KEY_UPCOUNT_73CX_ENG:Ljava/lang/String;

.field protected PREF_KEY_UPCOUNT_73CX_USER:Ljava/lang/String;

.field protected PREF_KEY_UPCOUNT_ENG:Ljava/lang/String;

.field protected PREF_KEY_UPCOUNT_ENG_FRONT:Ljava/lang/String;

.field protected PREF_KEY_UPCOUNT_USER:Ljava/lang/String;

.field protected PREF_KEY_UPCOUNT_USER_FRONT:Ljava/lang/String;

.field mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

.field mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

.field mClickListener:Landroid/view/View$OnClickListener;

.field private mCompForceUpdate:Z

.field private mCompUpdate:Z

.field private mCompUpdateCountCheck:Z

.field private mCurrentToast:Landroid/widget/Toast;

.field private mDate:Ljava/util/Date;

.field private mErrorPopup:Landroid/app/AlertDialog;

.field private mForceUpdate:Z

.field mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

.field private mHandler:Landroid/os/Handler;

.field private mKeystringBlockFlag:Z

.field private mPopup:Landroid/app/AlertDialog;

.field private final mProductShip:Ljava/lang/String;

.field mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWindowManager:Landroid/view/IWindowManager;

.field private misCompanionFile:Z

.field private misOISFile:Z

.field private progressDialog:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 99
    const-string v0, "ro.csc.sales_code"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mSalesCode:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 56
    iput-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 57
    iput-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 58
    iput-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    .line 60
    new-instance v0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getApplication()Landroid/app/Application;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    .line 64
    iput-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;

    .line 76
    const-string v0, "pref_firmware_upcount_eng_key"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_ENG:Ljava/lang/String;

    .line 77
    const-string v0, "pref_firmware_upcount_user_key"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_USER:Ljava/lang/String;

    .line 79
    const-string v0, "pref_firmware_upcount_73CX_eng_key"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_73CX_ENG:Ljava/lang/String;

    .line 80
    const-string v0, "pref_firmware_upcount_73CX_user_key"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_73CX_USER:Ljava/lang/String;

    .line 82
    const-string v0, "pref_firmware_upcount_eng_front_key"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_ENG_FRONT:Ljava/lang/String;

    .line 83
    const-string v0, "pref_firmware_upcount_user_front_key"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_USER_FRONT:Ljava/lang/String;

    .line 85
    iput-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mPopup:Landroid/app/AlertDialog;

    .line 87
    iput-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mErrorPopup:Landroid/app/AlertDialog;

    .line 89
    iput-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mDate:Ljava/util/Date;

    .line 91
    iput-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mForceUpdate:Z

    .line 93
    iput-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompForceUpdate:Z

    .line 94
    iput-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdate:Z

    .line 95
    iput-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdateCountCheck:Z

    .line 102
    const-string v0, "ro.product_ship"

    const-string v2, "FALSE"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mProductShip:Ljava/lang/String;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mProductShip:Ljava/lang/String;

    const-string v2, "TRUE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->isKeyStringBlocked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mKeystringBlockFlag:Z

    .line 108
    iput-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    .line 110
    iput-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misOISFile:Z

    .line 165
    new-instance v0, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware$1;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmware;)V

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    .line 960
    new-instance v0, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware$4;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmware;)V

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mHandler:Landroid/os/Handler;

    return-void

    :cond_0
    move v0, v1

    .line 105
    goto :goto_0
.end method

.method private CheckCamFWDate()J
    .locals 12

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    .line 1129
    const/4 v0, 0x0

    .line 1130
    .local v0, "CamFW":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v7}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v0

    .line 1131
    const/4 v7, 0x2

    invoke-virtual {v0, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1132
    .local v2, "CamFWVersionY":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    .line 1133
    .local v5, "ch":[C
    aget-char v7, v5, v8

    add-int/lit8 v7, v7, -0x43

    add-int/lit16 v4, v7, 0x7d9

    .line 1134
    .local v4, "Year":I
    const/4 v7, 0x4

    invoke-virtual {v0, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1135
    .local v1, "CamFWVersionM":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 1136
    .local v6, "ch1":[C
    aget-char v7, v6, v8

    add-int/lit8 v7, v7, -0x41

    add-int/lit8 v3, v7, 0x1

    .line 1138
    .local v3, "Month":I
    new-instance v7, Ljava/util/Date;

    const/4 v8, 0x1

    invoke-direct {v7, v4, v3, v8}, Ljava/util/Date;-><init>(III)V

    iput-object v7, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mDate:Ljava/util/Date;

    .line 1139
    const-string v7, "CameraFirmware"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CamFW Date = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mDate:Ljava/util/Date;

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1141
    iget-object v7, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mDate:Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    return-wide v8
.end method

.method private CheckCamFWVersion()I
    .locals 7

    .prologue
    .line 1145
    const/4 v0, 0x0

    .line 1146
    .local v0, "CamFW":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v0

    .line 1147
    const/4 v4, 0x4

    const/4 v5, 0x6

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1148
    .local v1, "CamFWVersionD":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 1149
    .local v3, "ch2":[C
    const/4 v4, 0x0

    aget-char v4, v3, v4

    mul-int/lit8 v4, v4, 0x7b

    const/4 v5, 0x1

    aget-char v5, v3, v5

    add-int v2, v4, v5

    .line 1150
    .local v2, "Version":I
    const-string v4, "CameraFirmware"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CheckCamFWVersion = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1151
    return v2
.end method

.method private CheckPhoneFWDate()J
    .locals 12

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    .line 1102
    const/4 v4, 0x0

    .line 1104
    .local v4, "phoneFW":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v7}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v4

    .line 1105
    const/4 v7, 0x2

    invoke-virtual {v4, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 1106
    .local v6, "phoneFWVersionY":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 1107
    .local v2, "ch":[C
    aget-char v7, v2, v8

    add-int/lit8 v7, v7, -0x43

    add-int/lit16 v1, v7, 0x7d9

    .line 1108
    .local v1, "Year":I
    const/4 v7, 0x4

    invoke-virtual {v4, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1109
    .local v5, "phoneFWVersionM":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 1110
    .local v3, "ch1":[C
    aget-char v7, v3, v8

    add-int/lit8 v7, v7, -0x41

    add-int/lit8 v0, v7, 0x1

    .line 1112
    .local v0, "Month":I
    new-instance v7, Ljava/util/Date;

    const/4 v8, 0x1

    invoke-direct {v7, v1, v0, v8}, Ljava/util/Date;-><init>(III)V

    iput-object v7, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mDate:Ljava/util/Date;

    .line 1113
    const-string v7, "CameraFirmware"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "PhoneFW Date = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mDate:Ljava/util/Date;

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1115
    iget-object v7, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mDate:Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    return-wide v8
.end method

.method private CheckPhoneFWVersion()I
    .locals 7

    .prologue
    .line 1119
    const/4 v2, 0x0

    .line 1120
    .local v2, "phoneFW":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v2

    .line 1121
    const/4 v4, 0x4

    const/4 v5, 0x6

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1122
    .local v3, "phoneFWVersionD":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 1123
    .local v1, "ch2":[C
    const/4 v4, 0x0

    aget-char v4, v1, v4

    mul-int/lit8 v4, v4, 0x7b

    const/4 v5, 0x1

    aget-char v5, v1, v5

    add-int v0, v4, v5

    .line 1124
    .local v0, "Version":I
    const-string v4, "CameraFirmware"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CheckPhoneFWVersion = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1125
    return v0
.end method

.method private DialogPopup(I)Landroid/app/AlertDialog;
    .locals 3
    .param p1, "messageId"    # I

    .prologue
    .line 1155
    const-string v1, "CameraFirmware"

    const-string v2, "DialogPopup"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1157
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1158
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 1161
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1162
    .local v0, "Dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1164
    iget-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdate:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompForceUpdate:Z

    if-eqz v1, :cond_2

    .line 1165
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cam FW Ver (73CX) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamCompanionFWVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nPhone FW Ver (73CX) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneCompanionFWVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1172
    :goto_0
    const v1, 0x7f0a0002

    new-instance v2, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware$5;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmware;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1191
    const-string v1, "No"

    new-instance v2, Lcom/sec/android/app/camerafirmware/CameraFirmware$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware$6;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmware;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1212
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1

    .line 1168
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cam FW Ver : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nPhone FW Ver : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/camerafirmware/CameraFirmware;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCurrentToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompForceUpdate:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompForceUpdate:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/camerafirmware/CameraFirmware;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->manageFirmware()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->setBlockHold(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misOISFile:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdate:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdate:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdateCountCheck:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdateCountCheck:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/camerafirmware/CameraFirmware;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mPopup:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/camerafirmware/CameraFirmware;I)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->DialogPopup(I)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->isSameVendor()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/camerafirmware/CameraFirmware;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->dialogErrorPopup(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/camerafirmware/CameraFirmware;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mForceUpdate:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/camerafirmware/CameraFirmware;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/camerafirmware/CameraFirmware;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mForceUpdate:Z

    return p1
.end method

.method private dialogErrorPopup(I)V
    .locals 3
    .param p1, "messageId"    # I

    .prologue
    .line 1216
    const-string v1, "CameraFirmware"

    const-string v2, "dialogErrorPopup"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1218
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1219
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 1221
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1222
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1224
    iget-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    if-eqz v1, :cond_1

    .line 1225
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cam FW Ver (73CX) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamCompanionFWVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Phone FW Ver (73CX) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneCompanionFWVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1232
    :goto_0
    const v1, 0x7f0a0001

    new-instance v2, Lcom/sec/android/app/camerafirmware/CameraFirmware$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware$7;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmware;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1237
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1238
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mErrorPopup:Landroid/app/AlertDialog;

    .line 1239
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1240
    return-void

    .line 1228
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cam FW Ver : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Phone FW Ver : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private init()V
    .locals 8

    .prologue
    const v7, 0x7f090016

    const v6, 0x7f090015

    const v5, 0x7f090019

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 631
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 632
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x2000000a

    const-string v2, "TouchFirmware"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 635
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mWindowManager:Landroid/view/IWindowManager;

    .line 637
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    .line 638
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 639
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 641
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->ChkSOCTypeModel()Z

    move-result v1

    if-nez v1, :cond_17

    .line 642
    const v1, 0x7f090021

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 643
    const v1, 0x7f090014

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 646
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->JF_FIRMWARE_FORCE_UPDATE:Z

    if-eqz v1, :cond_3

    .line 647
    const v1, 0x7f090017

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 672
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    if-eqz v1, :cond_d

    .line 673
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->ChkFirmwareTestKeyFile()Z

    move-result v1

    if-nez v1, :cond_c

    .line 674
    invoke-virtual {p0, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 682
    :goto_1
    iget-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    if-eqz v1, :cond_f

    .line 683
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->ChkFirmwareTestKeyFile()Z

    move-result v1

    if-nez v1, :cond_e

    .line 684
    invoke-virtual {p0, v7}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 692
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->FRONT_INTERNAL_ISP:Z

    if-nez v1, :cond_10

    .line 693
    const v1, 0x7f090018

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 698
    :goto_3
    const v1, 0x7f09001a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 700
    const v1, 0x7f09001b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 702
    const v1, 0x7f09001c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 704
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    if-eqz v1, :cond_12

    .line 705
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->ChkFirmwareTestKeyFile()Z

    move-result v1

    if-nez v1, :cond_11

    .line 706
    const v1, 0x7f09001d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 714
    :goto_4
    iget-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    if-eqz v1, :cond_14

    .line 715
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->ChkFirmwareTestKeyFile()Z

    move-result v1

    if-nez v1, :cond_13

    .line 716
    const v1, 0x7f09001f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 724
    :goto_5
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->FRONT_INTERNAL_ISP:Z

    if-nez v1, :cond_15

    .line 725
    const v1, 0x7f09001e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 730
    :goto_6
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_ISP_CORE_VOLTAGE:Z

    if-eqz v1, :cond_16

    .line 731
    const v1, 0x7f090020

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 782
    :cond_0
    :goto_7
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    if-eqz v1, :cond_1

    .line 783
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mFront:Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;->resetFWInfo()V

    .line 785
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-eqz v1, :cond_2

    .line 786
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->resetFWInfo()V

    .line 788
    :cond_2
    return-void

    .line 649
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->INTERNAL_ISP:Z

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->ChkUserFirmwareFile()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    if-eqz v1, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->ChkFirmwareTestKeyFile()Z

    move-result v1

    if-nez v1, :cond_8

    .line 651
    :cond_5
    const v1, 0x7f090017

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 656
    :goto_8
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->INTERNAL_ISP:Z

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->ChkUserCompanionFirmwareFile()Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    iget-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    if-eqz v1, :cond_a

    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->ChkFirmwareTestKeyFile()Z

    move-result v1

    if-nez v1, :cond_a

    .line 658
    :cond_7
    iget-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    if-eqz v1, :cond_9

    .line 659
    invoke-virtual {p0, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    .line 653
    :cond_8
    const v1, 0x7f090017

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_8

    .line 661
    :cond_9
    invoke-virtual {p0, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 664
    :cond_a
    iget-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    if-eqz v1, :cond_b

    .line 665
    invoke-virtual {p0, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 667
    :cond_b
    invoke-virtual {p0, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 676
    :cond_c
    invoke-virtual {p0, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 679
    :cond_d
    invoke-virtual {p0, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 686
    :cond_e
    invoke-virtual {p0, v7}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 689
    :cond_f
    invoke-virtual {p0, v7}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 695
    :cond_10
    const v1, 0x7f090018

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 708
    :cond_11
    const v1, 0x7f09001d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 711
    :cond_12
    const v1, 0x7f09001d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 718
    :cond_13
    const v1, 0x7f09001f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_5

    .line 721
    :cond_14
    const v1, 0x7f09001f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 727
    :cond_15
    const v1, 0x7f09001e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    .line 733
    :cond_16
    const v1, 0x7f090020

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    .line 736
    :cond_17
    const v1, 0x7f090014

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 737
    invoke-virtual {p0, v6}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 738
    invoke-virtual {p0, v7}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 739
    const v1, 0x7f090017

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 744
    invoke-virtual {p0, v5}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 747
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->FRONT_INTERNAL_ISP:Z

    if-nez v1, :cond_18

    .line 748
    const v1, 0x7f090018

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 753
    :goto_9
    const v1, 0x7f09001a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 754
    const v1, 0x7f09001b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 755
    const v1, 0x7f09001c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 756
    const v1, 0x7f09001d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 761
    const v1, 0x7f09001f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 764
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->FRONT_INTERNAL_ISP:Z

    if-nez v1, :cond_19

    .line 765
    const v1, 0x7f09001e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 770
    :goto_a
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CHECK_ISP_CORE_VOLTAGE:Z

    if-eqz v1, :cond_1a

    .line 771
    const v1, 0x7f090020

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 776
    :goto_b
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->NO_VENDOR_ID:Z

    if-nez v1, :cond_0

    .line 777
    const v1, 0x7f090021

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 778
    const v1, 0x7f090021

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_7

    .line 750
    :cond_18
    const v1, 0x7f090018

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_9

    .line 767
    :cond_19
    const v1, 0x7f09001e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_a

    .line 773
    :cond_1a
    const v1, 0x7f090020

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_b
.end method

.method public static isKeyStringBlocked()Z
    .locals 5

    .prologue
    .line 113
    const/4 v1, 0x0

    .line 115
    .local v1, "imeiBlocked":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v3, "/efs/FactoryApp/keystr"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x20

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 116
    const-string v2, "CameraFirmware"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    const-string v2, "ON"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    const-string v2, "CameraFirmware"

    const-string v3, "return true"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const/4 v2, 0x1

    .line 126
    :goto_1
    return v2

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e1":Ljava/io/IOException;
    const-string v1, "OFF"

    .line 119
    const-string v2, "CameraFirmware"

    const-string v3, "cannot open file : /efs/FactoryApp/keystr "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 125
    .end local v0    # "e1":Ljava/io/IOException;
    :cond_0
    const-string v2, "CameraFirmware"

    const-string v3, "return false"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private isSameVendor()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getCamFWVendor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getPhoneFWVendor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 162
    :cond_0
    :goto_0
    return v0

    .line 154
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getCamFWVendor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "O"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getCamFWVendor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "G"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getPhoneFWVendor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "O"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getPhoneFWVendor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "G"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 158
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getCamFWVendor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getPhoneFWVendor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    .line 162
    goto :goto_0
.end method

.method private manageFirmware()V
    .locals 6

    .prologue
    const/16 v5, 0x3e9

    const/16 v4, 0x3ea

    .line 886
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 888
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    .line 889
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 892
    :cond_0
    const-string v1, "Start the firmware update"

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 893
    const/16 v1, 0x3e8

    iput v1, v0, Landroid/os/Message;->what:I

    .line 894
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 896
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getManageMode()[B

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_UPDATE:[B

    if-ne v1, v2, :cond_3

    .line 897
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v2, v2, Lcom/sec/android/app/camera/Feature;->DURATION_FIRMWARE_UPDATE:I

    int-to-long v2, v2

    invoke-virtual {v1, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 910
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    if-eqz v1, :cond_2

    .line 911
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getManageMode()[B

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_UPDATE:[B

    if-ne v1, v2, :cond_8

    .line 912
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V

    .line 924
    :cond_2
    :goto_1
    return-void

    .line 898
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getManageMode()[B

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_DUMP:[B

    if-ne v1, v2, :cond_4

    .line 899
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v2, v2, Lcom/sec/android/app/camera/Feature;->DURATION_DUMP:I

    int-to-long v2, v2

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 900
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getManageMode()[B

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_FORCE:[B

    if-ne v1, v2, :cond_5

    .line 901
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v2, v2, Lcom/sec/android/app/camera/Feature;->DURATION_FIRMWARE_UPDATE:I

    int-to-long v2, v2

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 902
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getManageMode()[B

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_COMP_FIRMWARE_FORCE:[B

    if-ne v1, v2, :cond_6

    .line 903
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v2, v2, Lcom/sec/android/app/camera/Feature;->DURATION_COMPANION_FIRMWARE_UPDATE:I

    int-to-long v2, v2

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 904
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getManageMode()[B

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_COMP_FIRMWARE_UPDATE:[B

    if-ne v1, v2, :cond_7

    .line 905
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v2, v2, Lcom/sec/android/app/camera/Feature;->DURATION_COMPANION_FIRMWARE_UPDATE:I

    int-to-long v2, v2

    invoke-virtual {v1, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 906
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getManageMode()[B

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_COMP_FIRMWARE_DUMP:[B

    if-ne v1, v2, :cond_1

    .line 907
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget v2, v2, Lcom/sec/android/app/camera/Feature;->DURATION_DUMP:I

    int-to-long v2, v2

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 913
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getManageMode()[B

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_FORCE:[B

    if-ne v1, v2, :cond_9

    .line 914
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V

    goto :goto_1

    .line 915
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getManageMode()[B

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_FIRMWARE_DUMP:[B

    if-ne v1, v2, :cond_a

    .line 916
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V

    goto/16 :goto_1

    .line 917
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getManageMode()[B

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_COMP_FIRMWARE_FORCE:[B

    if-ne v1, v2, :cond_b

    .line 918
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V

    goto/16 :goto_1

    .line 919
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getManageMode()[B

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_COMP_FIRMWARE_UPDATE:[B

    if-ne v1, v2, :cond_c

    .line 920
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V

    goto/16 :goto_1

    .line 921
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getManageMode()[B

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAM_FLAG_COMP_FIRMWARE_DUMP:[B

    if-ne v1, v2, :cond_2

    .line 922
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->setFirmwareMode(I)V

    goto/16 :goto_1
.end method

.method private setBlockHold(Z)Z
    .locals 4
    .param p1, "bBlock"    # Z

    .prologue
    .line 804
    const-string v1, "CameraFirmware"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setBlockHold - bBlock: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    const/4 v0, 0x0

    .line 807
    .local v0, "bResult":Z
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mWindowManager:Landroid/view/IWindowManager;

    if-nez v1, :cond_0

    .line 808
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mWindowManager:Landroid/view/IWindowManager;

    .line 820
    :cond_0
    const-string v1, "CameraFirmware"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setBlockHold - bResult: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 821
    return v0
.end method


# virtual methods
.method protected ChkFirmwareTestKeyFile()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1045
    const/4 v0, 0x0

    .line 1047
    .local v0, "FirmwareTestKeyFile":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v3, v3, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    if-eqz v3, :cond_1

    .line 1048
    :cond_0
    const-string v0, "/sdcard/1q2w3e4r.key"

    .line 1054
    const-string v3, "CameraFirmware"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ChkFirmwareTestKeyFile : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1057
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1058
    const-string v2, "CameraFirmware"

    const-string v3, "User Firmware file exists"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1059
    const/4 v2, 0x1

    .line 1062
    .end local v1    # "dir":Ljava/io/File;
    :goto_0
    return v2

    .line 1050
    :cond_1
    const-string v3, "CameraFirmware"

    const-string v4, "DEFAULT_FIRMWARE_UPDATE feature disable"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1061
    .restart local v1    # "dir":Ljava/io/File;
    :cond_2
    const-string v3, "CameraFirmware"

    const-string v4, "User Firmware file doesn\'t exist"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected ChkSOCTypeModel()Z
    .locals 2

    .prologue
    .line 1243
    const-string v0, "CameraFirmware"

    const-string v1, "ChkSOCTypeModel..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1244
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v0, :cond_0

    .line 1245
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 1248
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v1, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->ChkFirmwareFile(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v1, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->ChkFirmwareFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1250
    :cond_1
    const-string v0, "CameraFirmware"

    const-string v1, "ChkSOCTypeModel false..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1251
    const/4 v0, 0x0

    .line 1254
    :goto_0
    return v0

    .line 1253
    :cond_2
    const-string v0, "CameraFirmware"

    const-string v1, "ChkSOCTypeModel true..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1254
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected ChkUserCompanionFirmwareFile()Z
    .locals 5

    .prologue
    .line 1023
    const/4 v0, 0x0

    .line 1025
    .local v0, "UserCompFirmwareFile":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    if-eqz v2, :cond_0

    .line 1026
    const-string v0, "/sdcard/CamFW_Companion.bin"

    .line 1033
    :goto_0
    const-string v2, "CameraFirmware"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ChkUserCompFirmwareFile : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1034
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1035
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1036
    const-string v2, "CameraFirmware"

    const-string v3, "User Comp Firmware file exists"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1037
    const/4 v2, 0x1

    .line 1040
    :goto_1
    return v2

    .line 1029
    .end local v1    # "dir":Ljava/io/File;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_USER_FILE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "RS_M10MO.bin"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1039
    .restart local v1    # "dir":Ljava/io/File;
    :cond_1
    const-string v2, "CameraFirmware"

    const-string v3, "User Comp Firmware file doesn\'t exist"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1040
    const/4 v2, 0x0

    goto :goto_1
.end method

.method protected ChkUserFirmwareFile()Z
    .locals 5

    .prologue
    .line 998
    const/4 v0, 0x0

    .line 1000
    .local v0, "UserFirmwareFile":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    if-eqz v2, :cond_0

    .line 1001
    const-string v0, "/sdcard/CamFW_Main.bin"

    .line 1010
    :goto_0
    const-string v2, "CameraFirmware"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ChkUserFirmwareFile : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1012
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1013
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1014
    const-string v2, "CameraFirmware"

    const-string v3, "User Firmware file exists"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    const/4 v2, 0x1

    .line 1018
    :goto_1
    return v2

    .line 1002
    .end local v1    # "dir":Ljava/io/File;
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    if-eqz v2, :cond_1

    .line 1003
    const-string v0, "/sdcard/CamFW_Companion.bin"

    goto :goto_0

    .line 1006
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_FIRMWARE_INFO_USER_FILE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "RS_M10MO.bin"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1017
    .restart local v1    # "dir":Ljava/io/File;
    :cond_2
    const-string v2, "CameraFirmware"

    const-string v3, "User Firmware file doesn\'t exist"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1018
    const/4 v2, 0x0

    goto :goto_1
.end method

.method protected IsNewFirmwareDate()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1086
    const-string v0, "CameraFirmware"

    const-string v1, "IsNewFirmwareDate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1087
    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->CheckPhoneFWDate()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->CheckCamFWDate()J

    move-result-wide v2

    sub-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 1088
    const/4 v0, 0x1

    .line 1092
    :goto_0
    return v0

    .line 1089
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->CheckPhoneFWDate()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->CheckCamFWDate()J

    move-result-wide v2

    sub-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 1090
    const/4 v0, 0x0

    goto :goto_0

    .line 1092
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected IsNewFirmwareVersion()Z
    .locals 2

    .prologue
    .line 1097
    const-string v0, "CameraFirmware"

    const-string v1, "IsNewFirmwareVersion"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1098
    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->CheckPhoneFWVersion()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->CheckCamFWVersion()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkCameraStartCondition_Security()Z
    .locals 2

    .prologue
    .line 825
    const-string v1, "persist.sys.camera_lock"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 826
    .local v0, "dev_camera_lock_state":Ljava/lang/String;
    const-string v1, "camera_lock.enabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 827
    const/4 v1, 0x1

    .line 828
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getCamFWVendor()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1067
    const/4 v0, 0x0

    .line 1068
    .local v0, "cameraFW":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1069
    .local v1, "cameraFWvendor":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getCamFWVer()Ljava/lang/String;

    move-result-object v0

    .line 1070
    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1071
    const-string v2, "CameraFirmware"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCamFWVendor() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1072
    return-object v1
.end method

.method protected getPhoneFWVendor()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1076
    const/4 v0, 0x0

    .line 1077
    .local v0, "phoneFW":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1078
    .local v1, "phoneFWvendor":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->getPhoneFWVer()Ljava/lang/String;

    move-result-object v0

    .line 1079
    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1081
    const-string v2, "CameraFirmware"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getPhoneFWVendor() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1082
    return-object v1
.end method

.method protected getUpdateCount(Ljava/lang/String;)I
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 955
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 957
    .local v0, "pref":Landroid/content/SharedPreferences;
    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method protected hideUpdateProgress()V
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 626
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 627
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    .line 628
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 131
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 132
    const-string v0, "CameraFirmware"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-static {}, Lcom/sec/android/app/camera/CameraApp;->getAppFeature()Lcom/sec/android/app/camera/Feature;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->Feature:Lcom/sec/android/app/camera/Feature;

    .line 136
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->setContentView(I)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-nez v0, :cond_0

    .line 139
    const-string v0, "CameraFirmware"

    const-string v1, "mRear is null, rear FW info will be created"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    new-instance v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    invoke-direct {v0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v1, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_COMPANION_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->ChkFirmwareFile(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v1, v1, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->CAMERA_OIS_FIRMWARE_INFO_FILE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;->ChkFirmwareFile(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misOISFile:Z

    .line 145
    const-string v0, "CameraFirmware"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "misCompanionFile : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misCompanionFile:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", misOISFile : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->misOISFile:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->init()V

    .line 148
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 881
    const-string v0, "CameraFirmware"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 883
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 563
    const-string v0, "CameraFirmware"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyDown()-keyCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    sparse-switch p1, :sswitch_data_0

    .line 572
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 567
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 564
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x1a -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 576
    const-string v0, "CameraFirmware"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyDown()-keyCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    sparse-switch p1, :sswitch_data_0

    .line 585
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 580
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 577
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x1a -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 867
    const-string v0, "CameraFirmware"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 869
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 872
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {v0}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->releaseCamera()V

    .line 874
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->setBlockHold(Z)Z

    .line 876
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 877
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const v5, 0x7f0a0013

    const/4 v4, 0x1

    .line 832
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 833
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Camera_SecurityMdmService"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 834
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->checkCameraStartCondition_Security()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 835
    invoke-static {p0, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 836
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->finish()V

    .line 864
    :cond_0
    :goto_0
    return-void

    .line 842
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "enterprise_policy"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 843
    .local v0, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/enterprise/RestrictionPolicy;->isCameraEnabled(Z)Z

    move-result v2

    if-nez v2, :cond_2

    .line 844
    invoke-static {p0, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 845
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->finish()V

    goto :goto_0

    .line 850
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "device_policy"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/admin/DevicePolicyManager;

    .line 851
    .local v1, "mDPM":Landroid/app/admin/DevicePolicyManager;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/admin/DevicePolicyManager;->getAllowCamera(Landroid/content/ComponentName;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 852
    invoke-static {p0, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 853
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->finish()V

    goto :goto_0

    .line 856
    :cond_3
    const-string v2, "CameraFirmware"

    const-string v3, "onResume"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCameraDevice:Lcom/sec/android/app/camerafirmware/CameraDeviceController;

    invoke-virtual {v2}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->openCamera()Z

    move-result v2

    if-nez v2, :cond_0

    .line 858
    const-string v2, "CameraFirmware"

    const-string v3, "onResume - camera is not opened"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    const v2, 0x7f0a0012

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected setUpdateCount(Ljava/lang/String;I)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "count"    # I

    .prologue
    .line 946
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 949
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 951
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 952
    return-void
.end method

.method protected showUpdateProgress()V
    .locals 1

    .prologue
    .line 589
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->showUpdateProgress(Z)V

    .line 590
    return-void
.end method

.method protected showUpdateProgress(Z)V
    .locals 2
    .param p1, "isDump"    # Z

    .prologue
    .line 593
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 594
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    .line 595
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/android/app/camerafirmware/CameraFirmware$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware$2;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmware;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 609
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 610
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 613
    :cond_0
    if-nez p1, :cond_2

    .line 614
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    const-string v1, "Now Updating... Warning!\nDo not turn off!\nIt will take sometime."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 620
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 622
    :cond_1
    return-void

    .line 617
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->progressDialog:Landroid/app/ProgressDialog;

    const-string v1, "Now Dumping... Warning!\nDo not turn off!\nIt will take sometime."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected startUpdateThread()V
    .locals 2

    .prologue
    .line 791
    const/4 v0, 0x0

    .line 793
    .local v0, "thread":Ljava/lang/Thread;
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->setBlockHold(Z)Z

    .line 795
    new-instance v0, Ljava/lang/Thread;

    .end local v0    # "thread":Ljava/lang/Thread;
    new-instance v1, Lcom/sec/android/app/camerafirmware/CameraFirmware$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/camerafirmware/CameraFirmware$3;-><init>(Lcom/sec/android/app/camerafirmware/CameraFirmware;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 800
    .restart local v0    # "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 801
    return-void
.end method

.method protected updateFirmwareUpdateCount()V
    .locals 4

    .prologue
    .line 929
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCamFirmMgr:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mRear:Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;

    if-ne v1, v2, :cond_1

    .line 930
    iget-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdateCountCheck:Z

    if-eqz v1, :cond_0

    .line 931
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_73CX_ENG:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v0

    .line 932
    .local v0, "updateCount":I
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_73CX_ENG:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->setUpdateCount(Ljava/lang/String;I)V

    .line 933
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->mCompUpdateCountCheck:Z

    .line 942
    :goto_0
    const-string v1, "CameraFirmware"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateFirmwareUpdateCount - PREF_KEY_UPCOUNT_ENG:["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 943
    return-void

    .line 935
    .end local v0    # "updateCount":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_ENG:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v0

    .line 936
    .restart local v0    # "updateCount":I
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_ENG:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->setUpdateCount(Ljava/lang/String;I)V

    goto :goto_0

    .line 939
    .end local v0    # "updateCount":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_ENG_FRONT:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->getUpdateCount(Ljava/lang/String;)I

    move-result v0

    .line 940
    .restart local v0    # "updateCount":I
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraFirmware;->PREF_KEY_UPCOUNT_ENG_FRONT:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/camerafirmware/CameraFirmware;->setUpdateCount(Ljava/lang/String;I)V

    goto :goto_0
.end method
