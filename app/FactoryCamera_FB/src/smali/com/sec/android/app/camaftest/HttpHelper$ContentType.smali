.class public final enum Lcom/sec/android/app/camaftest/HttpHelper$ContentType;
.super Ljava/lang/Enum;
.source "HttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camaftest/HttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/camaftest/HttpHelper$ContentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

.field public static final enum HTML:Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

.field public static final enum JSON:Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

.field public static final enum TEXT:Lcom/sec/android/app/camaftest/HttpHelper$ContentType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    new-instance v0, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    const-string v1, "HTML"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;->HTML:Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    .line 50
    new-instance v0, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    const-string v1, "JSON"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;->JSON:Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    .line 52
    new-instance v0, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;->TEXT:Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    .line 46
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    sget-object v1, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;->HTML:Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;->JSON:Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;->TEXT:Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;->$VALUES:[Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/camaftest/HttpHelper$ContentType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 46
    const-class v0, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/camaftest/HttpHelper$ContentType;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/camaftest/HttpHelper$ContentType;->$VALUES:[Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    invoke-virtual {v0}, [Lcom/sec/android/app/camaftest/HttpHelper$ContentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/camaftest/HttpHelper$ContentType;

    return-object v0
.end method
