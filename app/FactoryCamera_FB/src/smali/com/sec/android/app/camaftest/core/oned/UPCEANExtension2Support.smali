.class final Lcom/sec/android/app/camaftest/core/oned/UPCEANExtension2Support;
.super Ljava/lang/Object;
.source "UPCEANExtension2Support.java"


# instance fields
.field private final decodeMiddleCounters:[I

.field private final decodeRowStringBuffer:Ljava/lang/StringBuilder;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/UPCEANExtension2Support;->decodeMiddleCounters:[I

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/UPCEANExtension2Support;->decodeRowStringBuffer:Ljava/lang/StringBuilder;

    return-void
.end method

.method private static parseExtensionString(Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .param p0, "raw"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/ResultMetadataType;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 100
    const/4 v0, 0x0

    .line 104
    :goto_0
    return-object v0

    .line 102
    :cond_0
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 103
    .local v0, "result":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;>;"
    sget-object v1, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->ISSUE_NUMBER:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method decodeMiddle(Lcom/sec/android/app/camaftest/core/common/BitArray;[ILjava/lang/StringBuilder;)I
    .locals 12
    .param p1, "row"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .param p2, "startRange"    # [I
    .param p3, "resultString"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v4, p0, Lcom/sec/android/app/camaftest/core/oned/UPCEANExtension2Support;->decodeMiddleCounters:[I

    .line 55
    .local v4, "counters":[I
    const/4 v10, 0x0

    const/4 v11, 0x0

    aput v11, v4, v10

    .line 56
    const/4 v10, 0x1

    const/4 v11, 0x0

    aput v11, v4, v10

    .line 57
    const/4 v10, 0x2

    const/4 v11, 0x0

    aput v11, v4, v10

    .line 58
    const/4 v10, 0x3

    const/4 v11, 0x0

    aput v11, v4, v10

    .line 59
    invoke-virtual {p1}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v5

    .line 60
    .local v5, "end":I
    const/4 v10, 0x1

    aget v8, p2, v10

    .line 62
    .local v8, "rowOffset":I
    const/4 v2, 0x0

    .line 64
    .local v2, "checkParity":I
    const/4 v9, 0x0

    .local v9, "x":I
    :goto_0
    const/4 v10, 0x2

    if-ge v9, v10, :cond_3

    if-ge v8, v5, :cond_3

    .line 65
    sget-object v10, Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;->L_AND_G_PATTERNS:[[I

    invoke-static {p1, v4, v8, v10}, Lcom/sec/android/app/camaftest/core/oned/UPCEANReader;->decodeDigit(Lcom/sec/android/app/camaftest/core/common/BitArray;[II[[I)I

    move-result v1

    .line 66
    .local v1, "bestMatch":I
    rem-int/lit8 v10, v1, 0xa

    add-int/lit8 v10, v10, 0x30

    int-to-char v10, v10

    invoke-virtual {p3, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 67
    move-object v0, v4

    .local v0, "arr$":[I
    array-length v7, v0

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v7, :cond_0

    aget v3, v0, v6

    .line 68
    .local v3, "counter":I
    add-int/2addr v8, v3

    .line 67
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 70
    .end local v3    # "counter":I
    :cond_0
    const/16 v10, 0xa

    if-lt v1, v10, :cond_1

    .line 71
    const/4 v10, 0x1

    rsub-int/lit8 v11, v9, 0x1

    shl-int/2addr v10, v11

    or-int/2addr v2, v10

    .line 73
    :cond_1
    const/4 v10, 0x1

    if-eq v9, v10, :cond_2

    .line 75
    invoke-virtual {p1, v8}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getNextSet(I)I

    move-result v8

    .line 76
    invoke-virtual {p1, v8}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getNextUnset(I)I

    move-result v8

    .line 64
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 80
    .end local v0    # "arr$":[I
    .end local v1    # "bestMatch":I
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    :cond_3
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    const/4 v11, 0x2

    if-eq v10, v11, :cond_4

    .line 81
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v10

    throw v10

    .line 84
    :cond_4
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    rem-int/lit8 v10, v10, 0x4

    if-eq v10, v2, :cond_5

    .line 85
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v10

    throw v10

    .line 88
    :cond_5
    return v8
.end method

.method decodeRow(ILcom/sec/android/app/camaftest/core/common/BitArray;[I)Lcom/sec/android/app/camaftest/core/Result;
    .locals 12
    .param p1, "rowNumber"    # I
    .param p2, "row"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .param p3, "extensionStartRange"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 39
    iget-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/UPCEANExtension2Support;->decodeRowStringBuffer:Ljava/lang/StringBuilder;

    .line 40
    .local v3, "result":Ljava/lang/StringBuilder;
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 41
    invoke-virtual {p0, p2, p3, v3}, Lcom/sec/android/app/camaftest/core/oned/UPCEANExtension2Support;->decodeMiddle(Lcom/sec/android/app/camaftest/core/common/BitArray;[ILjava/lang/StringBuilder;)I

    move-result v0

    .line 43
    .local v0, "end":I
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 44
    .local v4, "resultString":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/camaftest/core/oned/UPCEANExtension2Support;->parseExtensionString(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 46
    .local v1, "extensionData":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;>;"
    new-instance v2, Lcom/sec/android/app/camaftest/core/Result;

    const/4 v5, 0x0

    const/4 v6, 0x2

    new-array v6, v6, [Lcom/sec/android/app/camaftest/core/ResultPoint;

    new-instance v7, Lcom/sec/android/app/camaftest/core/ResultPoint;

    aget v8, p3, v10

    aget v9, p3, v11

    add-int/2addr v8, v9

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    int-to-float v9, p1

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/camaftest/core/ResultPoint;-><init>(FF)V

    aput-object v7, v6, v10

    new-instance v7, Lcom/sec/android/app/camaftest/core/ResultPoint;

    int-to-float v8, v0

    int-to-float v9, p1

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/camaftest/core/ResultPoint;-><init>(FF)V

    aput-object v7, v6, v11

    sget-object v7, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_EAN_EXTENSION:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/sec/android/app/camaftest/core/Result;-><init>(Ljava/lang/String;[B[Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/BarcodeFormat;)V

    .line 47
    .local v2, "extensionResult":Lcom/sec/android/app/camaftest/core/Result;
    if-eqz v1, :cond_0

    .line 48
    invoke-virtual {v2, v1}, Lcom/sec/android/app/camaftest/core/Result;->putAllMetadata(Ljava/util/Map;)V

    .line 50
    :cond_0
    return-object v2
.end method
