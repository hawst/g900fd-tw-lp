.class public final Lcom/sec/android/app/camaftest/core/aztec/AztecReader;
.super Ljava/lang/Object;
.source "AztecReader.java"

# interfaces
.implements Lcom/sec/android/app/camaftest/core/Reader;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 1
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/camaftest/core/aztec/AztecReader;->decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;

    move-result-object v0

    return-object v0
.end method

.method public decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 15
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/BinaryBitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/camaftest/core/BinaryBitmap;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation

    .prologue
    .line 61
    .local p2, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    new-instance v12, Lcom/sec/android/app/camaftest/core/aztec/detector/Detector;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/camaftest/core/BinaryBitmap;->getBlackMatrix()Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    move-result-object v13

    invoke-direct {v12, v13}, Lcom/sec/android/app/camaftest/core/aztec/detector/Detector;-><init>(Lcom/sec/android/app/camaftest/core/common/BitMatrix;)V

    invoke-virtual {v12}, Lcom/sec/android/app/camaftest/core/aztec/detector/Detector;->detect()Lcom/sec/android/app/camaftest/core/aztec/AztecDetectorResult;

    move-result-object v4

    .line 62
    .local v4, "detectorResult":Lcom/sec/android/app/camaftest/core/aztec/AztecDetectorResult;
    invoke-virtual {v4}, Lcom/sec/android/app/camaftest/core/aztec/AztecDetectorResult;->getPoints()[Lcom/sec/android/app/camaftest/core/ResultPoint;

    move-result-object v9

    .line 64
    .local v9, "points":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    if-eqz p2, :cond_0

    .line 65
    sget-object v12, Lcom/sec/android/app/camaftest/core/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/camaftest/core/ResultPointCallback;

    .line 66
    .local v11, "rpcb":Lcom/sec/android/app/camaftest/core/ResultPointCallback;
    if-eqz v11, :cond_0

    .line 67
    move-object v1, v9

    .local v1, "arr$":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v8, v1, v6

    .line 68
    .local v8, "point":Lcom/sec/android/app/camaftest/core/ResultPoint;
    invoke-interface {v11, v8}, Lcom/sec/android/app/camaftest/core/ResultPointCallback;->foundPossibleResultPoint(Lcom/sec/android/app/camaftest/core/ResultPoint;)V

    .line 67
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 73
    .end local v1    # "arr$":[Lcom/sec/android/app/camaftest/core/ResultPoint;
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    .end local v8    # "point":Lcom/sec/android/app/camaftest/core/ResultPoint;
    .end local v11    # "rpcb":Lcom/sec/android/app/camaftest/core/ResultPointCallback;
    :cond_0
    new-instance v12, Lcom/sec/android/app/camaftest/core/aztec/decoder/Decoder;

    invoke-direct {v12}, Lcom/sec/android/app/camaftest/core/aztec/decoder/Decoder;-><init>()V

    invoke-virtual {v12, v4}, Lcom/sec/android/app/camaftest/core/aztec/decoder/Decoder;->decode(Lcom/sec/android/app/camaftest/core/aztec/AztecDetectorResult;)Lcom/sec/android/app/camaftest/core/common/DecoderResult;

    move-result-object v3

    .line 75
    .local v3, "decoderResult":Lcom/sec/android/app/camaftest/core/common/DecoderResult;
    new-instance v10, Lcom/sec/android/app/camaftest/core/Result;

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/DecoderResult;->getText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/DecoderResult;->getRawBytes()[B

    move-result-object v13

    sget-object v14, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->AZTEC:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-direct {v10, v12, v13, v9, v14}, Lcom/sec/android/app/camaftest/core/Result;-><init>(Ljava/lang/String;[B[Lcom/sec/android/app/camaftest/core/ResultPoint;Lcom/sec/android/app/camaftest/core/BarcodeFormat;)V

    .line 77
    .local v10, "result":Lcom/sec/android/app/camaftest/core/Result;
    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/DecoderResult;->getByteSegments()Ljava/util/List;

    move-result-object v2

    .line 78
    .local v2, "byteSegments":Ljava/util/List;, "Ljava/util/List<[B>;"
    if-eqz v2, :cond_1

    .line 79
    sget-object v12, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->BYTE_SEGMENTS:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-virtual {v10, v12, v2}, Lcom/sec/android/app/camaftest/core/Result;->putMetadata(Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;)V

    .line 81
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/app/camaftest/core/common/DecoderResult;->getECLevel()Ljava/lang/String;

    move-result-object v5

    .line 82
    .local v5, "ecLevel":Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 83
    sget-object v12, Lcom/sec/android/app/camaftest/core/ResultMetadataType;->ERROR_CORRECTION_LEVEL:Lcom/sec/android/app/camaftest/core/ResultMetadataType;

    invoke-virtual {v10, v12, v5}, Lcom/sec/android/app/camaftest/core/Result;->putMetadata(Lcom/sec/android/app/camaftest/core/ResultMetadataType;Ljava/lang/Object;)V

    .line 86
    :cond_2
    return-object v10
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 92
    return-void
.end method
