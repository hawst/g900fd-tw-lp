.class public Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;
.super Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;
.source "FirmwareFileMgrFront.java"


# static fields
.field protected static final CAMERA_FIRMWARE_INFO_FILE_FULL_FRONT:Ljava/lang/String; = "/sys/class/camera/front/front_camfw_full"

.field protected static final CAMERA_FIRMWARE_INFO_USER_FILE:Ljava/lang/String;

.field protected static final CAMERA_FIRMWARE_TYPE_FILE_FRONT:Ljava/lang/String; = "/sys/class/camera/front/front_camtype"

.field protected static final CAMERA_NO_FIRMWARE_TYPE_FILE_FRONT:Ljava/lang/String; = "/sys/class/camera/front/front_type"

.field protected static final RS_M5LS:Ljava/lang/String; = "RS_M5LS.bin"

.field protected static final TAG:Ljava/lang/String; = "FirmwareFileMgrFront"


# instance fields
.field protected CAMERA_FIRMWARE_INFO_FILE_FRONT:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;->CAMERA_FIRMWARE_INFO_USER_FILE:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgr;-><init>()V

    .line 20
    const-string v0, "/sys/class/camera/front/front_camfw"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;->CAMERA_FIRMWARE_INFO_FILE_FRONT:Ljava/lang/String;

    .line 31
    const-string v0, "FirmwareFileMgrFront"

    const-string v1, "FirmwareFileMgrFront"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;->resetFWInfo()V

    .line 35
    const-string v0, "/sys/class/camera/front/front_camfw_full"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;->ChkFirmwareFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const-string v0, "/sys/class/camera/front/front_camfw_full"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;->CAMERA_FIRMWARE_INFO_FILE_FRONT:Ljava/lang/String;

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;->CAMERA_FIRMWARE_INFO_FILE_FRONT:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;->CAMERA_FIRMWARE_INFO_FILE:Ljava/lang/String;

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v0, v0, Lcom/sec/android/app/camera/Feature;->NO_FIRMWARE_TYPE_FILE:Z

    if-eqz v0, :cond_1

    .line 40
    const-string v0, "/sys/class/camera/front/front_type"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;->CAMERA_FIRMWARE_TYPE_FILE:Ljava/lang/String;

    .line 43
    :goto_0
    return-void

    .line 42
    :cond_1
    const-string v0, "/sys/class/camera/front/front_camtype"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/FirmwareFileMgrFront;->CAMERA_FIRMWARE_TYPE_FILE:Ljava/lang/String;

    goto :goto_0
.end method
