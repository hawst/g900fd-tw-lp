.class public interface abstract Lcom/sec/android/app/camaftest/core/multi/MultipleBarcodeReader;
.super Ljava/lang/Object;
.source "MultipleBarcodeReader.java"


# virtual methods
.method public abstract decodeMultiple(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)[Lcom/sec/android/app/camaftest/core/Result;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation
.end method

.method public abstract decodeMultiple(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)[Lcom/sec/android/app/camaftest/core/Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/camaftest/core/BinaryBitmap;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)[",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation
.end method
