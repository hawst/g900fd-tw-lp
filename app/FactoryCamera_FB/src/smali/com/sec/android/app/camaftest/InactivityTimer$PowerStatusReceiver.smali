.class final Lcom/sec/android/app/camaftest/InactivityTimer$PowerStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "InactivityTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camaftest/InactivityTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PowerStatusReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camaftest/InactivityTimer;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/camaftest/InactivityTimer;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/app/camaftest/InactivityTimer$PowerStatusReceiver;->this$0:Lcom/sec/android/app/camaftest/InactivityTimer;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/camaftest/InactivityTimer;Lcom/sec/android/app/camaftest/InactivityTimer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/camaftest/InactivityTimer;
    .param p2, "x1"    # Lcom/sec/android/app/camaftest/InactivityTimer$1;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/app/camaftest/InactivityTimer$PowerStatusReceiver;-><init>(Lcom/sec/android/app/camaftest/InactivityTimer;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 99
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    const-string v1, "plugged"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 103
    .local v0, "batteryPlugged":I
    if-lez v0, :cond_0

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/camaftest/InactivityTimer$PowerStatusReceiver;->this$0:Lcom/sec/android/app/camaftest/InactivityTimer;

    # invokes: Lcom/sec/android/app/camaftest/InactivityTimer;->cancel()V
    invoke-static {v1}, Lcom/sec/android/app/camaftest/InactivityTimer;->access$200(Lcom/sec/android/app/camaftest/InactivityTimer;)V

    .line 107
    .end local v0    # "batteryPlugged":I
    :cond_0
    return-void
.end method
