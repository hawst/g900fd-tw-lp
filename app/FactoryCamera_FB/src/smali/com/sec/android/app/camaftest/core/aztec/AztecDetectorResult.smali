.class public final Lcom/sec/android/app/camaftest/core/aztec/AztecDetectorResult;
.super Lcom/sec/android/app/camaftest/core/common/DetectorResult;
.source "AztecDetectorResult.java"


# instance fields
.field private final compact:Z

.field private final nbDatablocks:I

.field private final nbLayers:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camaftest/core/common/BitMatrix;[Lcom/sec/android/app/camaftest/core/ResultPoint;ZII)V
    .locals 0
    .param p1, "bits"    # Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .param p2, "points"    # [Lcom/sec/android/app/camaftest/core/ResultPoint;
    .param p3, "compact"    # Z
    .param p4, "nbDatablocks"    # I
    .param p5, "nbLayers"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/camaftest/core/common/DetectorResult;-><init>(Lcom/sec/android/app/camaftest/core/common/BitMatrix;[Lcom/sec/android/app/camaftest/core/ResultPoint;)V

    .line 35
    iput-boolean p3, p0, Lcom/sec/android/app/camaftest/core/aztec/AztecDetectorResult;->compact:Z

    .line 36
    iput p4, p0, Lcom/sec/android/app/camaftest/core/aztec/AztecDetectorResult;->nbDatablocks:I

    .line 37
    iput p5, p0, Lcom/sec/android/app/camaftest/core/aztec/AztecDetectorResult;->nbLayers:I

    .line 38
    return-void
.end method


# virtual methods
.method public getNbDatablocks()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/camaftest/core/aztec/AztecDetectorResult;->nbDatablocks:I

    return v0
.end method

.method public getNbLayers()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/camaftest/core/aztec/AztecDetectorResult;->nbLayers:I

    return v0
.end method

.method public isCompact()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/app/camaftest/core/aztec/AztecDetectorResult;->compact:Z

    return v0
.end method
