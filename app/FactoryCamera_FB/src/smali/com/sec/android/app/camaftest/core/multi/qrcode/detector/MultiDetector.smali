.class public final Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiDetector;
.super Lcom/sec/android/app/camaftest/core/qrcode/detector/Detector;
.source "MultiDetector.java"


# static fields
.field private static final EMPTY_DETECTOR_RESULTS:[Lcom/sec/android/app/camaftest/core/common/DetectorResult;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/sec/android/app/camaftest/core/common/DetectorResult;

    sput-object v0, Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiDetector;->EMPTY_DETECTOR_RESULTS:[Lcom/sec/android/app/camaftest/core/common/DetectorResult;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/camaftest/core/common/BitMatrix;)V
    .locals 0
    .param p1, "image"    # Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/camaftest/core/qrcode/detector/Detector;-><init>(Lcom/sec/android/app/camaftest/core/common/BitMatrix;)V

    .line 45
    return-void
.end method


# virtual methods
.method public detectMulti(Ljava/util/Map;)[Lcom/sec/android/app/camaftest/core/common/DetectorResult;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)[",
            "Lcom/sec/android/app/camaftest/core/common/DetectorResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiDetector;->getImage()Lcom/sec/android/app/camaftest/core/common/BitMatrix;

    move-result-object v3

    .line 49
    .local v3, "image":Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    if-nez p1, :cond_0

    const/4 v8, 0x0

    .line 51
    .local v8, "resultPointCallback":Lcom/sec/android/app/camaftest/core/ResultPointCallback;
    :goto_0
    new-instance v1, Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiFinderPatternFinder;

    invoke-direct {v1, v3, v8}, Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiFinderPatternFinder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitMatrix;Lcom/sec/android/app/camaftest/core/ResultPointCallback;)V

    .line 52
    .local v1, "finder":Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiFinderPatternFinder;
    invoke-virtual {v1, p1}, Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiFinderPatternFinder;->findMulti(Ljava/util/Map;)[Lcom/sec/android/app/camaftest/core/qrcode/detector/FinderPatternInfo;

    move-result-object v5

    .line 54
    .local v5, "infos":[Lcom/sec/android/app/camaftest/core/qrcode/detector/FinderPatternInfo;
    array-length v9, v5

    if-nez v9, :cond_1

    .line 55
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v9

    throw v9

    .line 49
    .end local v1    # "finder":Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiFinderPatternFinder;
    .end local v5    # "infos":[Lcom/sec/android/app/camaftest/core/qrcode/detector/FinderPatternInfo;
    .end local v8    # "resultPointCallback":Lcom/sec/android/app/camaftest/core/ResultPointCallback;
    :cond_0
    sget-object v9, Lcom/sec/android/app/camaftest/core/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    invoke-interface {p1, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/camaftest/core/ResultPointCallback;

    move-object v8, v9

    goto :goto_0

    .line 58
    .restart local v1    # "finder":Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiFinderPatternFinder;
    .restart local v5    # "infos":[Lcom/sec/android/app/camaftest/core/qrcode/detector/FinderPatternInfo;
    .restart local v8    # "resultPointCallback":Lcom/sec/android/app/camaftest/core/ResultPointCallback;
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 59
    .local v7, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/camaftest/core/common/DetectorResult;>;"
    move-object v0, v5

    .local v0, "arr$":[Lcom/sec/android/app/camaftest/core/qrcode/detector/FinderPatternInfo;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v6, :cond_2

    aget-object v4, v0, v2

    .line 61
    .local v4, "info":Lcom/sec/android/app/camaftest/core/qrcode/detector/FinderPatternInfo;
    :try_start_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiDetector;->processFinderPatternInfo(Lcom/sec/android/app/camaftest/core/qrcode/detector/FinderPatternInfo;)Lcom/sec/android/app/camaftest/core/common/DetectorResult;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/sec/android/app/camaftest/core/ReaderException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 66
    .end local v4    # "info":Lcom/sec/android/app/camaftest/core/qrcode/detector/FinderPatternInfo;
    :cond_2
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 67
    sget-object v9, Lcom/sec/android/app/camaftest/core/multi/qrcode/detector/MultiDetector;->EMPTY_DETECTOR_RESULTS:[Lcom/sec/android/app/camaftest/core/common/DetectorResult;

    .line 69
    :goto_3
    return-object v9

    :cond_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Lcom/sec/android/app/camaftest/core/common/DetectorResult;

    invoke-interface {v7, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/sec/android/app/camaftest/core/common/DetectorResult;

    goto :goto_3

    .line 62
    .restart local v4    # "info":Lcom/sec/android/app/camaftest/core/qrcode/detector/FinderPatternInfo;
    :catch_0
    move-exception v9

    goto :goto_2
.end method
