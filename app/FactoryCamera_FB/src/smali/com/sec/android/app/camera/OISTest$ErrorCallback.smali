.class public final Lcom/sec/android/app/camera/OISTest$ErrorCallback;
.super Ljava/lang/Object;
.source "OISTest.java"

# interfaces
.implements Landroid/hardware/Camera$ErrorCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/OISTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ErrorCallback"
.end annotation


# static fields
.field private static final CAMERA_ERROR_DATALINE_FAIL:I = 0x7d0

.field private static final CAMERA_ERROR_DATALINE_SUCCESS:I = 0x7d1

.field private static final CAMERA_ERROR_MSG_NO_ERROR:I = 0x0

.field private static final CAMERA_ERROR_WRONG_FW:I = 0x3e8


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/OISTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/camera/OISTest;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/camera/OISTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/OISTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILandroid/hardware/Camera;)V
    .locals 3
    .param p1, "error"    # I
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 155
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    sparse-switch p1, :sswitch_data_0

    .line 180
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorCallback - CAMERA BAD["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/OISTest;

    const v1, 0x7f0a000a

    # invokes: Lcom/sec/android/app/camera/OISTest;->dialogErrorPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/OISTest;->access$200(Lcom/sec/android/app/camera/OISTest;I)V

    .line 184
    :goto_0
    :sswitch_0
    return-void

    .line 161
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "ErrorCallback - CAMERA_ERROR_WRONG_FW"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/OISTest;

    const v1, 0x7f0a000c

    # invokes: Lcom/sec/android/app/camera/OISTest;->dialogErrorPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/OISTest;->access$200(Lcom/sec/android/app/camera/OISTest;I)V

    goto :goto_0

    .line 166
    :sswitch_2
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "ErrorCallback - CAMERA_ERROR_DATALINE_SUCCESS"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/camera/OISTest;->access$300(Lcom/sec/android/app/camera/OISTest;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 171
    :sswitch_3
    sget-object v0, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v1, "ErrorCallback - CAMERA_ERROR_DATALINE_FAIL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/OISTest;

    # getter for: Lcom/sec/android/app/camera/OISTest;->cameraType:I
    invoke-static {v0}, Lcom/sec/android/app/camera/OISTest;->access$400(Lcom/sec/android/app/camera/OISTest;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/OISTest;

    const v1, 0x7f0a0009

    # invokes: Lcom/sec/android/app/camera/OISTest;->dialogErrorPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/OISTest;->access$200(Lcom/sec/android/app/camera/OISTest;I)V

    goto :goto_0

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/OISTest$ErrorCallback;->this$0:Lcom/sec/android/app/camera/OISTest;

    const v1, 0x7f0a0008

    # invokes: Lcom/sec/android/app/camera/OISTest;->dialogErrorPopup(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/camera/OISTest;->access$200(Lcom/sec/android/app/camera/OISTest;I)V

    goto :goto_0

    .line 157
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x3e8 -> :sswitch_1
        0x7d0 -> :sswitch_3
        0x7d1 -> :sswitch_2
    .end sparse-switch
.end method
