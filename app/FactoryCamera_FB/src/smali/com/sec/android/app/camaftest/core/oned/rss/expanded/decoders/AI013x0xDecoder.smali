.class abstract Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0xDecoder;
.super Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI01weightDecoder;
.source "AI013x0xDecoder.java"


# static fields
.field private static final HEADER_SIZE:I = 0x5

.field private static final WEIGHT_SIZE:I = 0xf


# direct methods
.method constructor <init>(Lcom/sec/android/app/camaftest/core/common/BitArray;)V
    .locals 0
    .param p1, "information"    # Lcom/sec/android/app/camaftest/core/common/BitArray;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI01weightDecoder;-><init>(Lcom/sec/android/app/camaftest/core/common/BitArray;)V

    .line 42
    return-void
.end method


# virtual methods
.method public parseInformation()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0xDecoder;->getInformation()Lcom/sec/android/app/camaftest/core/common/BitArray;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/common/BitArray;->getSize()I

    move-result v1

    const/16 v2, 0x3c

    if-eq v1, v2, :cond_0

    .line 47
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v1

    throw v1

    .line 50
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .local v0, "buf":Ljava/lang/StringBuilder;
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0xDecoder;->encodeCompressedGtin(Ljava/lang/StringBuilder;I)V

    .line 53
    const/16 v1, 0x2d

    const/16 v2, 0xf

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/decoders/AI013x0xDecoder;->encodeCompressedWeight(Ljava/lang/StringBuilder;II)V

    .line 55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
