.class public interface abstract Lcom/sec/android/app/camaftest/core/Reader;
.super Ljava/lang/Object;
.source "Reader.java"


# virtual methods
.method public abstract decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;)Lcom/sec/android/app/camaftest/core/Result;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/ChecksumException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation
.end method

.method public abstract decode(Lcom/sec/android/app/camaftest/core/BinaryBitmap;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/camaftest/core/BinaryBitmap;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;,
            Lcom/sec/android/app/camaftest/core/ChecksumException;,
            Lcom/sec/android/app/camaftest/core/FormatException;
        }
    .end annotation
.end method

.method public abstract reset()V
.end method
