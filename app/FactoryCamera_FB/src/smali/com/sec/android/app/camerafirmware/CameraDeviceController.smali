.class public Lcom/sec/android/app/camerafirmware/CameraDeviceController;
.super Ljava/lang/Object;
.source "CameraDeviceController.java"


# static fields
.field public static final FIRMWAREMODE_COMP_DUMP:I = 0x7

.field public static final FIRMWAREMODE_COMP_FORCE:I = 0x5

.field public static final FIRMWAREMODE_COMP_WRITE:I = 0x6

.field public static final FIRMWAREMODE_DUMP:I = 0x3

.field public static final FIRMWAREMODE_FORCE:I = 0x4

.field public static final FIRMWAREMODE_NONE:I = 0x0

.field public static final FIRMWAREMODE_UPDATE:I = 0x2

.field public static final FIRMWAREMODE_VERSION:I = 0x1

.field private static final PARAM_KEY_FIRMWAREMODE:Ljava/lang/String; = "firmware-mode"

.field private static final PARAM_VALUE_COMP_DUMP:Ljava/lang/String; = "comp_dump"

.field private static final PARAM_VALUE_COMP_FORCE:Ljava/lang/String; = "comp_force"

.field private static final PARAM_VALUE_COMP_WRITE:Ljava/lang/String; = "comp_write"

.field private static final PARAM_VALUE_DUMP:Ljava/lang/String; = "dump"

.field private static final PARAM_VALUE_FORCE:Ljava/lang/String; = "force"

.field private static final PARAM_VALUE_NONE:Ljava/lang/String; = "none"

.field private static final PARAM_VALUE_UPDATE:Ljava/lang/String; = "update"

.field private static final PARAM_VALUE_VERSION:Ljava/lang/String; = "version"

.field private static final TAG:Ljava/lang/String; = "CameraDeviceController"


# instance fields
.field protected CAMERA_COMPANION_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

.field public Feature:Lcom/sec/android/app/camera/Feature;

.field private final context:Landroid/content/Context;

.field private mCameraDevice:Landroid/hardware/Camera;

.field private mDVFSHelper:Landroid/os/DVFSHelper;

.field private mParameters:Landroid/hardware/Camera$Parameters;

.field private supportedCPUCoreTable:[I

.field private supportedCPUFreqTable:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 62
    const-string v0, "/sys/class/camera/rear/rear_companionfw_full"

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->CAMERA_COMPANION_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    .line 65
    const-string v0, "CameraDeviceController"

    const-string v1, "CameraDeviceController..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iput-object p1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->context:Landroid/content/Context;

    .line 67
    invoke-static {}, Lcom/sec/android/app/camera/CameraApp;->getAppFeature()Lcom/sec/android/app/camera/Feature;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->Feature:Lcom/sec/android/app/camera/Feature;

    .line 68
    return-void
.end method


# virtual methods
.method protected ChkFirmwareFile(Ljava/lang/String;)Z
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 194
    const-string v2, "CameraDeviceController"

    const-string v3, "ChkFirmwareFile"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const/4 v0, 0x0

    .line 197
    .local v0, "FirmwareFile":Ljava/lang/String;
    move-object v0, p1

    .line 199
    const-string v2, "CameraDeviceController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FirmwareFile : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 201
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 202
    const-string v2, "CameraDeviceController"

    const-string v3, "Firmware info data file exists"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    const/4 v2, 0x1

    .line 206
    :goto_0
    return v2

    .line 205
    :cond_0
    const-string v2, "CameraDeviceController"

    const-string v3, "Firmware info data file doesn\'t exist"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public acquireDVFS(I)V
    .locals 5
    .param p1, "milisecond"    # I

    .prologue
    const/4 v4, 0x0

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-nez v0, :cond_0

    .line 168
    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->context:Landroid/content/Context;

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mDVFSHelper:Landroid/os/DVFSHelper;

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->supportedCPUFreqTable:[I

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->supportedCPUCoreTable:[I

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->supportedCPUFreqTable:[I

    if-eqz v0, :cond_2

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mDVFSHelper:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->supportedCPUFreqTable:[I

    aget v2, v2, v4

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 174
    const-string v0, "CameraDeviceController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "supportedCPUFreqTable is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->supportedCPUFreqTable:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->supportedCPUCoreTable:[I

    if-eqz v0, :cond_3

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mDVFSHelper:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->supportedCPUCoreTable:[I

    aget v2, v2, v4

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 181
    const-string v0, "CameraDeviceController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "supportedCPUCoreTable is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->supportedCPUCoreTable:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0, p1}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 189
    const-string v0, "CameraDeviceController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDVFSHelper.acquire : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :cond_1
    return-void

    .line 176
    :cond_2
    const-string v0, "CameraDeviceController"

    const-string v1, "supportedCPUFreqTable is null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 183
    :cond_3
    const-string v0, "CameraDeviceController"

    const-string v1, "supportedCPUFreqTable is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public openCamera()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 71
    const-string v1, "CameraDeviceController"

    const-string v2, "openCameraDevice"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    if-nez v1, :cond_1

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->CAMERA_BOOST_ENABLE:Z

    if-eqz v1, :cond_0

    .line 74
    const/16 v1, 0x7d0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->acquireDVFS(I)V

    .line 77
    :cond_0
    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    .line 80
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public releaseCamera()V
    .locals 2

    .prologue
    .line 84
    const-string v0, "CameraDeviceController"

    const-string v1, "releaseCameraDevice"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mDVFSHelper:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mDVFSHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 93
    :cond_1
    return-void
.end method

.method public setFirmwareMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 96
    const-string v1, "CameraDeviceController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setFirmwareMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    if-nez v1, :cond_1

    .line 99
    const-string v1, "CameraDeviceController"

    const-string v2, "setFirmwareMode - mCameraDevice is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mParameters:Landroid/hardware/Camera$Parameters;

    .line 105
    const-string v0, "version"

    .line 106
    .local v0, "valueFwMode":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 129
    const-string v0, "none"

    .line 133
    :goto_1
    const-string v1, "CameraDeviceController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setFirmwareMode mode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", valueFwMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mParameters:Landroid/hardware/Camera$Parameters;

    const-string v2, "firmware-mode"

    invoke-virtual {v1, v2, v0}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v1, v1, Lcom/sec/android/app/camera/Feature;->DEFAULT_FIRMWARE_UPDATE:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->CAMERA_COMPANION_FIRMWARE_INFO_FILE_FULL_REAR:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->ChkFirmwareFile(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->startPreview()Z

    goto :goto_0

    .line 108
    :pswitch_0
    const-string v0, "version"

    .line 109
    goto :goto_1

    .line 111
    :pswitch_1
    const-string v0, "update"

    .line 112
    goto :goto_1

    .line 114
    :pswitch_2
    const-string v0, "force"

    .line 115
    goto :goto_1

    .line 117
    :pswitch_3
    const-string v0, "dump"

    .line 118
    goto :goto_1

    .line 120
    :pswitch_4
    const-string v0, "comp_force"

    .line 121
    goto :goto_1

    .line 123
    :pswitch_5
    const-string v0, "comp_write"

    .line 124
    goto :goto_1

    .line 126
    :pswitch_6
    const-string v0, "comp_dump"

    .line 127
    goto :goto_1

    .line 106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public startPreview()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 143
    const-string v2, "CameraDeviceController"

    const-string v3, "startCamerPreview"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    if-nez v2, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->openCamera()Z

    move-result v2

    if-nez v2, :cond_0

    .line 156
    :goto_0
    return v1

    .line 150
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->startPreview()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    const/4 v1, 0x1

    goto :goto_0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Ljava/lang/Throwable;
    const-string v2, "CameraDeviceController"

    const-string v3, "exception while startPreview"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public stopPreview()V
    .locals 2

    .prologue
    .line 160
    const-string v0, "CameraDeviceController"

    const-string v1, "stopCameraPreview"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/camerafirmware/CameraDeviceController;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 164
    :cond_0
    return-void
.end method
