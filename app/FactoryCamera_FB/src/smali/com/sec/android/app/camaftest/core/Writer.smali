.class public interface abstract Lcom/sec/android/app/camaftest/core/Writer;
.super Ljava/lang/Object;
.source "Writer.java"


# virtual methods
.method public abstract encode(Ljava/lang/String;Lcom/sec/android/app/camaftest/core/BarcodeFormat;II)Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/WriterException;
        }
    .end annotation
.end method

.method public abstract encode(Ljava/lang/String;Lcom/sec/android/app/camaftest/core/BarcodeFormat;IILjava/util/Map;)Lcom/sec/android/app/camaftest/core/common/BitMatrix;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/camaftest/core/BarcodeFormat;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/EncodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/common/BitMatrix;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/WriterException;
        }
    .end annotation
.end method
