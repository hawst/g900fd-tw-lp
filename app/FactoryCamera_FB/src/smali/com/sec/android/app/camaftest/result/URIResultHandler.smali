.class public final Lcom/sec/android/app/camaftest/result/URIResultHandler;
.super Lcom/sec/android/app/camaftest/result/ResultHandler;
.source "URIResultHandler.java"


# static fields
.field private static final SECURE_PROTOCOLS:[Ljava/lang/String;

.field private static final buttons:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "otpauth:"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/camaftest/result/URIResultHandler;->SECURE_PROTOCOLS:[Ljava/lang/String;

    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/camaftest/result/URIResultHandler;->buttons:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0a002f
        0x7f0a003b
    .end array-data
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "result"    # Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/camaftest/result/ResultHandler;-><init>(Landroid/app/Activity;Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;)V

    .line 47
    return-void
.end method


# virtual methods
.method public areContentsSecure()Z
    .locals 8

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/result/URIResultHandler;->getResult()Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/camaftest/core/client/result/URIParsedResult;

    .line 84
    .local v5, "uriResult":Lcom/sec/android/app/camaftest/core/client/result/URIParsedResult;
    invoke-virtual {v5}, Lcom/sec/android/app/camaftest/core/client/result/URIParsedResult;->getURI()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 85
    .local v4, "uri":Ljava/lang/String;
    sget-object v0, Lcom/sec/android/app/camaftest/result/URIResultHandler;->SECURE_PROTOCOLS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 86
    .local v3, "secure":Ljava/lang/String;
    invoke-virtual {v4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 87
    const/4 v6, 0x1

    .line 90
    .end local v3    # "secure":Ljava/lang/String;
    :goto_1
    return v6

    .line 85
    .restart local v3    # "secure":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 90
    .end local v3    # "secure":Ljava/lang/String;
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public getButtonCount()I
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/app/camaftest/result/URIResultHandler;->buttons:[I

    array-length v0, v0

    return v0
.end method

.method public getButtonText(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 59
    sget-object v0, Lcom/sec/android/app/camaftest/result/URIResultHandler;->buttons:[I

    aget v0, v0, p1

    return v0
.end method

.method public getDisplayTitle()I
    .locals 1

    .prologue
    .line 78
    const v0, 0x7f0a0099

    return v0
.end method

.method public handleButtonPress(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/camaftest/result/URIResultHandler;->getResult()Lcom/sec/android/app/camaftest/core/client/result/ParsedResult;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/camaftest/core/client/result/URIParsedResult;

    .line 65
    .local v1, "uriResult":Lcom/sec/android/app/camaftest/core/client/result/URIParsedResult;
    invoke-virtual {v1}, Lcom/sec/android/app/camaftest/core/client/result/URIParsedResult;->getURI()Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "uri":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 74
    :goto_0
    return-void

    .line 68
    :pswitch_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/camaftest/result/URIResultHandler;->openURL(Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :pswitch_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/camaftest/result/URIResultHandler;->shareByEmail(Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
