.class public Lcom/sec/android/app/camera/PostViewTest;
.super Landroid/app/Activity;
.source "PostViewTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;
    }
.end annotation


# static fields
.field public static final INPUT_OTHER_KEY_INTERVAL_TIME:J = 0x32L

.field public static final KEY_POWER:Ljava/lang/String; = "Hold"

.field public static final KEY_VOLUME_UP:Ljava/lang/String; = "Volume Up"

.field private static final TAG:Ljava/lang/String; = "PostViewTest"

.field private static mIsDialogShown:Z

.field private static mKeyPressTime:J

.field private static mKeyReleaseTime:J

.field private static mOldKeyPressTime:J


# instance fields
.field public Feature:Lcom/sec/android/app/camera/Feature;

.field private mCameraBitmap:[Landroid/graphics/Bitmap;

.field private mFilePathMainCam:Ljava/lang/String;

.field private mFilePathSubCam:Ljava/lang/String;

.field private mFrontDisplayed:Z

.field private mImageView:Landroid/widget/ImageView;

.field private mIsLongPress:Z

.field private mIsMultiKeyPress:Z

.field private mKeyFailToast:Landroid/widget/Toast;

.field private mLowLightCaptureDisplayed:I

.field private mRearDisplayed:Z

.field private oldKeyCode:I

.field private onKeyDownCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 55
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/camera/PostViewTest;->mIsDialogShown:Z

    .line 56
    sput-wide v2, Lcom/sec/android/app/camera/PostViewTest;->mKeyPressTime:J

    .line 57
    sput-wide v2, Lcom/sec/android/app/camera/PostViewTest;->mKeyReleaseTime:J

    .line 58
    sput-wide v2, Lcom/sec/android/app/camera/PostViewTest;->mOldKeyPressTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/app/camera/PostViewTest;->mCameraBitmap:[Landroid/graphics/Bitmap;

    .line 43
    iput-boolean v1, p0, Lcom/sec/android/app/camera/PostViewTest;->mRearDisplayed:Z

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/app/camera/PostViewTest;->mFrontDisplayed:Z

    .line 46
    iput v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mLowLightCaptureDisplayed:I

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsLongPress:Z

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsMultiKeyPress:Z

    .line 53
    iput v2, p0, Lcom/sec/android/app/camera/PostViewTest;->oldKeyCode:I

    .line 54
    iput v1, p0, Lcom/sec/android/app/camera/PostViewTest;->onKeyDownCount:I

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/PostViewTest;->mKeyFailToast:Landroid/widget/Toast;

    .line 66
    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 33
    sput-boolean p0, Lcom/sec/android/app/camera/PostViewTest;->mIsDialogShown:Z

    return p0
.end method

.method private displayImage(I)V
    .locals 4
    .param p1, "cameraType"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 318
    if-nez p1, :cond_1

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/camera/PostViewTest;->mCameraBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v3

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/camera/PostViewTest;->mImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/PostViewTest;->mCameraBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 325
    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mRearDisplayed:Z

    .line 335
    :goto_1
    return-void

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/camera/PostViewTest;->mImageView:Landroid/widget/ImageView;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 328
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/camera/PostViewTest;->mCameraBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v2

    if-eqz v0, :cond_2

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/camera/PostViewTest;->mImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/camera/PostViewTest;->mCameraBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 333
    :goto_2
    iput-boolean v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mFrontDisplayed:Z

    goto :goto_1

    .line 331
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/camera/PostViewTest;->mImageView:Landroid/widget/ImageView;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_2
.end method

.method private getImage(I)V
    .locals 11
    .param p1, "cameraType"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 281
    invoke-static {}, Lcom/sec/android/app/camera/CameraStorage;->getInstance()Lcom/sec/android/app/camera/CameraStorage;

    move-result-object v7

    .line 282
    .local v7, "cs":Lcom/sec/android/app/camera/CameraStorage;
    if-nez p1, :cond_1

    .line 284
    invoke-virtual {v7, v1}, Lcom/sec/android/app/camera/CameraStorage;->getFilePath(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mFilePathMainCam:Ljava/lang/String;

    .line 285
    const-string v2, "PostViewTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PostViewTest.getImage() mFilePathMainCam : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camera/PostViewTest;->mFilePathMainCam:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mFilePathMainCam:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/app/camera/PostViewTest;->mFilePathMainCam:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 288
    const/4 v9, 0x4

    .line 292
    .local v9, "sampleSize":I
    new-instance v8, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v8}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 293
    .local v8, "options":Landroid/graphics/BitmapFactory$Options;
    iput v9, v8, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 294
    iget-object v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mFilePathMainCam:Ljava/lang/String;

    invoke-static {v2, v8}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 295
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 296
    .local v5, "matrix":Landroid/graphics/Matrix;
    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 297
    iget-object v10, p0, Lcom/sec/android/app/camera/PostViewTest;->mCameraBitmap:[Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v10, v1

    .line 315
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    .end local v8    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v9    # "sampleSize":I
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    invoke-virtual {v7, v6}, Lcom/sec/android/app/camera/CameraStorage;->getFilePath(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mFilePathSubCam:Ljava/lang/String;

    .line 304
    const-string v2, "PostViewTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PostViewTest.getImage() mFilePathSubCam : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/camera/PostViewTest;->mFilePathSubCam:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    iget-object v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mFilePathSubCam:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/app/camera/PostViewTest;->mFilePathSubCam:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 307
    iget-object v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mFilePathSubCam:Ljava/lang/String;

    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 308
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 309
    .restart local v5    # "matrix":Landroid/graphics/Matrix;
    const/high16 v2, 0x43870000    # 270.0f

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 310
    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v5, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 311
    iget-object v10, p0, Lcom/sec/android/app/camera/PostViewTest;->mCameraBitmap:[Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v10, v6

    goto :goto_0
.end method

.method private initialize()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 266
    const v0, 0x7f09004c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/camera/PostViewTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/camera/PostViewTest;->mImageView:Landroid/widget/ImageView;

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "low_light_capture_display"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/camera/PostViewTest;->mLowLightCaptureDisplayed:I

    .line 269
    const-string v0, "PostViewTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PostViewTest.initialize() mLowLightCaptureDisplayed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mLowLightCaptureDisplayed:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    iget v0, p0, Lcom/sec/android/app/camera/PostViewTest;->mLowLightCaptureDisplayed:I

    if-ne v0, v3, :cond_0

    .line 272
    invoke-direct {p0, v3}, Lcom/sec/android/app/camera/PostViewTest;->getImage(I)V

    .line 273
    invoke-direct {p0, v3}, Lcom/sec/android/app/camera/PostViewTest;->displayImage(I)V

    .line 278
    :goto_0
    return-void

    .line 275
    :cond_0
    invoke-direct {p0, v4}, Lcom/sec/android/app/camera/PostViewTest;->getImage(I)V

    .line 276
    invoke-direct {p0, v4}, Lcom/sec/android/app/camera/PostViewTest;->displayImage(I)V

    goto :goto_0
.end method

.method public static setSystemKeyBlock(Landroid/content/ComponentName;I)V
    .locals 4
    .param p0, "componentName"    # Landroid/content/ComponentName;
    .param p1, "keyCode"    # I

    .prologue
    .line 338
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 341
    .local v1, "wm":Landroid/view/IWindowManager;
    const/4 v2, 0x1

    :try_start_0
    invoke-interface {v1, p1, p0, v2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 345
    :goto_0
    return-void

    .line 342
    :catch_0
    move-exception v0

    .line 343
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "PostViewTest"

    const-string v3, "setSystemKeyBlock exception!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    const-string v1, "PostViewTest"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-static {}, Lcom/sec/android/app/camera/CameraApp;->getAppFeature()Lcom/sec/android/app/camera/Feature;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/PostViewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/PostViewTest;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/16 v2, 0x1a

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/PostViewTest;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/16 v2, 0xbb

    invoke-static {v1, v2}, Lcom/sec/android/app/camera/PostViewTest;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 79
    .local v0, "win":Landroid/view/Window;
    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 81
    const v1, 0x7f030009

    invoke-virtual {p0, v1}, Lcom/sec/android/app/camera/PostViewTest;->setContentView(I)V

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/camera/PostViewTest;->initialize()V

    .line 84
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 106
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 107
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const v7, 0x7f0a00a4

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 110
    const-string v2, "PostViewTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onKeyDown keyCode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v0, 0x0

    .line 113
    .local v0, "mKeyString":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/sec/android/app/camera/PostViewTest;->mKeyPressTime:J

    .line 115
    iget-boolean v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsLongPress:Z

    if-nez v2, :cond_0

    .line 116
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsLongPress:Z

    .line 118
    iget-boolean v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsLongPress:Z

    if-eqz v2, :cond_0

    .line 119
    iput p1, p0, Lcom/sec/android/app/camera/PostViewTest;->oldKeyCode:I

    .line 121
    iget v2, p0, Lcom/sec/android/app/camera/PostViewTest;->onKeyDownCount:I

    if-lez v2, :cond_0

    .line 122
    sget-wide v2, Lcom/sec/android/app/camera/PostViewTest;->mKeyPressTime:J

    sput-wide v2, Lcom/sec/android/app/camera/PostViewTest;->mOldKeyPressTime:J

    .line 127
    :cond_0
    sget-wide v2, Lcom/sec/android/app/camera/PostViewTest;->mKeyPressTime:J

    sget-wide v4, Lcom/sec/android/app/camera/PostViewTest;->mOldKeyPressTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsLongPress:Z

    if-eqz v2, :cond_1

    .line 128
    const-string v2, "PostViewTest"

    const-string v3, "onKeyDown LongKeyPress 1000ms..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    sget-boolean v2, Lcom/sec/android/app/camera/PostViewTest;->mIsDialogShown:Z

    if-nez v2, :cond_1

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->showKeyFailDialog()V

    .line 134
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsLongPress:Z

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/camera/PostViewTest;->oldKeyCode:I

    if-ne v2, p1, :cond_3

    :cond_2
    iget v2, p0, Lcom/sec/android/app/camera/PostViewTest;->onKeyDownCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/camera/PostViewTest;->onKeyDownCount:I

    if-le v2, v1, :cond_4

    .line 135
    :cond_3
    iput-boolean v1, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsMultiKeyPress:Z

    .line 138
    :cond_4
    sget-wide v2, Lcom/sec/android/app/camera/PostViewTest;->mKeyReleaseTime:J

    const-wide/16 v4, 0x32

    add-long/2addr v2, v4

    sget-wide v4, Lcom/sec/android/app/camera/PostViewTest;->mKeyPressTime:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    sget-wide v2, Lcom/sec/android/app/camera/PostViewTest;->mKeyReleaseTime:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    .line 139
    sget-boolean v2, Lcom/sec/android/app/camera/PostViewTest;->mIsDialogShown:Z

    if-nez v2, :cond_5

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->showKeyFailDialog()V

    .line 142
    :cond_5
    iput-boolean v1, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsMultiKeyPress:Z

    .line 145
    :cond_6
    sparse-switch p1, :sswitch_data_0

    .line 168
    const-string v2, "PostViewTest"

    const-string v3, "PostViewTest.onKeyDown() default return true"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    const/16 v2, 0x1a

    if-ne p1, v2, :cond_7

    .line 170
    const-string v0, "Hold"

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mKeyFailToast:Landroid/widget/Toast;

    if-nez v2, :cond_7

    .line 173
    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v6

    invoke-virtual {p0, v7, v1}, Lcom/sec/android/app/camera/PostViewTest;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/camera/PostViewTest;->mKeyFailToast:Landroid/widget/Toast;

    .line 174
    iget-object v1, p0, Lcom/sec/android/app/camera/PostViewTest;->mKeyFailToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 179
    :cond_7
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :cond_8
    :goto_0
    return v1

    .line 147
    :sswitch_0
    const-string v2, "PostViewTest"

    const-string v3, "PostViewTest.onKeyDown() VOLUME DOWN return true"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 151
    :sswitch_1
    const-string v2, "PostViewTest"

    const-string v3, "PostViewTest.onKeyDown() VOLUME UP return true"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    const-string v0, "Volume Up"

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mKeyFailToast:Landroid/widget/Toast;

    if-nez v2, :cond_8

    .line 155
    new-array v2, v1, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-virtual {p0, v7, v2}, Lcom/sec/android/app/camera/PostViewTest;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mKeyFailToast:Landroid/widget/Toast;

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mKeyFailToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 161
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/app/camera/PostViewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/sec/android/app/camera/PostViewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v2, v2, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    if-eqz v2, :cond_7

    :cond_9
    iget v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mLowLightCaptureDisplayed:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_7

    .line 163
    const-string v2, "PostViewTest"

    const-string v3, "PostViewTest.onKeyDown() KEYCODE_BACK return true"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 145
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x18 -> :sswitch_1
        0x19 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 183
    const-string v4, "PostViewTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onKeyUp keyCode = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sput-wide v4, Lcom/sec/android/app/camera/PostViewTest;->mKeyReleaseTime:J

    .line 185
    iput v7, p0, Lcom/sec/android/app/camera/PostViewTest;->onKeyDownCount:I

    .line 186
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/camera/PostViewTest;->mKeyFailToast:Landroid/widget/Toast;

    .line 188
    const-string v4, "PostViewTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onKeyUp mIsLongPress : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsLongPress:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mIsMultiKeyPress : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsMultiKeyPress:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iget-boolean v4, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsLongPress:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsMultiKeyPress:Z

    if-eqz v4, :cond_3

    .line 190
    :cond_0
    sget-boolean v4, Lcom/sec/android/app/camera/PostViewTest;->mIsDialogShown:Z

    if-nez v4, :cond_1

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->showKeyFailDialog()V

    .line 193
    :cond_1
    iput-boolean v7, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsLongPress:Z

    .line 194
    iput-boolean v7, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsMultiKeyPress:Z

    .line 262
    :cond_2
    :goto_0
    :sswitch_0
    return v3

    .line 197
    :cond_3
    sget-boolean v4, Lcom/sec/android/app/camera/PostViewTest;->mIsDialogShown:Z

    if-nez v4, :cond_2

    .line 201
    sparse-switch p1, :sswitch_data_0

    .line 262
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_0

    .line 203
    :sswitch_1
    const-string v4, "PostViewTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "PostViewTest.onKeyUp() mRearDisplayed : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/sec/android/app/camera/PostViewTest;->mRearDisplayed:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mFrontDisplayed : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/sec/android/app/camera/PostViewTest;->mFrontDisplayed:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-boolean v4, p0, Lcom/sec/android/app/camera/PostViewTest;->mRearDisplayed:Z

    if-eqz v4, :cond_6

    .line 205
    iget-object v4, p0, Lcom/sec/android/app/camera/PostViewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v4, v4, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST:Z

    if-eqz v4, :cond_4

    iget v4, p0, Lcom/sec/android/app/camera/PostViewTest;->mLowLightCaptureDisplayed:I

    if-nez v4, :cond_4

    .line 206
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/camera/Camera;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 207
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "camera_id"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 208
    const-string v4, "ommision_test"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 209
    const-string v4, "torch_on"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 210
    const-string v4, "camcorder_preview_test"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 211
    invoke-virtual {p0, v8, v1}, Lcom/sec/android/app/camera/PostViewTest;->setResult(ILandroid/content/Intent;)V

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->finish()V

    goto :goto_0

    .line 216
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/camera/PostViewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v4, v4, Lcom/sec/android/app/camera/Feature;->SKIP_POSTVIEW_FRONT:Z

    if-eqz v4, :cond_5

    .line 217
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 218
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v8, v1}, Lcom/sec/android/app/camera/PostViewTest;->setResult(ILandroid/content/Intent;)V

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->finish()V

    goto :goto_0

    .line 221
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_5
    invoke-direct {p0, v3}, Lcom/sec/android/app/camera/PostViewTest;->getImage(I)V

    .line 222
    invoke-direct {p0, v3}, Lcom/sec/android/app/camera/PostViewTest;->displayImage(I)V

    .line 223
    iput-boolean v7, p0, Lcom/sec/android/app/camera/PostViewTest;->mRearDisplayed:Z

    goto :goto_0

    .line 225
    :cond_6
    iget-boolean v4, p0, Lcom/sec/android/app/camera/PostViewTest;->mFrontDisplayed:Z

    if-eqz v4, :cond_2

    .line 226
    iget-object v4, p0, Lcom/sec/android/app/camera/PostViewTest;->Feature:Lcom/sec/android/app/camera/Feature;

    iget-boolean v4, v4, Lcom/sec/android/app/camera/Feature;->CHECK_LOW_LIGHT_CAPTURE_TEST_FRONT:Z

    if-eqz v4, :cond_7

    iget v4, p0, Lcom/sec/android/app/camera/PostViewTest;->mLowLightCaptureDisplayed:I

    if-ne v4, v3, :cond_7

    .line 227
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/camera/Camera;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 228
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v4, "camera_id"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 229
    const-string v4, "ommision_test"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 230
    const-string v4, "torch_on"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 231
    const-string v4, "camcorder_preview_test"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 232
    invoke-virtual {p0, v8, v1}, Lcom/sec/android/app/camera/PostViewTest;->setResult(ILandroid/content/Intent;)V

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->finish()V

    goto/16 :goto_0

    .line 237
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 238
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v8, v1}, Lcom/sec/android/app/camera/PostViewTest;->setResult(ILandroid/content/Intent;)V

    .line 239
    iput-boolean v7, p0, Lcom/sec/android/app/camera/PostViewTest;->mFrontDisplayed:Z

    .line 240
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->finish()V

    goto/16 :goto_0

    .line 246
    .end local v1    # "intent":Landroid/content/Intent;
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/camera/Camera;->isShowFailDialog()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 248
    iget-boolean v4, p0, Lcom/sec/android/app/camera/PostViewTest;->mRearDisplayed:Z

    if-eqz v4, :cond_8

    .line 249
    const-string v2, "REAR CAMERA"

    .line 256
    .local v2, "strFailMessage":Ljava/lang/String;
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " POST VIEW"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/camera/Camera$FailDialogFragment;->newInstance(Ljava/lang/String;)Lcom/sec/android/app/camera/Camera$FailDialogFragment;

    move-result-object v0

    .line 257
    .local v0, "alertDialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "dialog"

    invoke-virtual {v0, v4, v5}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 251
    .end local v0    # "alertDialog":Landroid/app/DialogFragment;
    .end local v2    # "strFailMessage":Ljava/lang/String;
    :cond_8
    iget-boolean v4, p0, Lcom/sec/android/app/camera/PostViewTest;->mFrontDisplayed:Z

    if-eqz v4, :cond_9

    .line 252
    const-string v2, "FRONT CAMERA"

    .restart local v2    # "strFailMessage":Ljava/lang/String;
    goto :goto_1

    .line 254
    .end local v2    # "strFailMessage":Ljava/lang/String;
    :cond_9
    const-string v2, "UNKOWN"

    .restart local v2    # "strFailMessage":Ljava/lang/String;
    goto :goto_1

    .line 201
    nop

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_1
        0xbb -> :sswitch_2
    .end sparse-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 101
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 102
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    .line 87
    const-string v0, "PostViewTest"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iput-boolean v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsLongPress:Z

    .line 89
    iput-boolean v2, p0, Lcom/sec/android/app/camera/PostViewTest;->mIsMultiKeyPress:Z

    .line 90
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/camera/PostViewTest;->oldKeyCode:I

    .line 91
    iput v2, p0, Lcom/sec/android/app/camera/PostViewTest;->onKeyDownCount:I

    .line 92
    sput-boolean v2, Lcom/sec/android/app/camera/PostViewTest;->mIsDialogShown:Z

    .line 93
    sput-wide v4, Lcom/sec/android/app/camera/PostViewTest;->mKeyPressTime:J

    .line 94
    sput-wide v4, Lcom/sec/android/app/camera/PostViewTest;->mKeyReleaseTime:J

    .line 95
    sput-wide v4, Lcom/sec/android/app/camera/PostViewTest;->mOldKeyPressTime:J

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/camera/PostViewTest;->mKeyFailToast:Landroid/widget/Toast;

    .line 97
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 98
    return-void
.end method

.method public showKeyFailDialog()V
    .locals 3

    .prologue
    .line 348
    const-string v1, "PostViewTest"

    const-string v2, "showKeyFailDialog"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    const/16 v1, 0x2710

    invoke-static {v1}, Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;->newInstance(I)Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;

    move-result-object v0

    .line 350
    .local v0, "dialog":Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;->setCancelable(Z)V

    .line 351
    invoke-virtual {p0}, Lcom/sec/android/app/camera/PostViewTest;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/camera/PostViewTest$keyAlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 352
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/camera/PostViewTest;->mIsDialogShown:Z

    .line 353
    return-void
.end method
