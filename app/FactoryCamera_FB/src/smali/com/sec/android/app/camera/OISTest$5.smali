.class Lcom/sec/android/app/camera/OISTest$5;
.super Landroid/content/BroadcastReceiver;
.source "OISTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/OISTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/camera/OISTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/camera/OISTest;)V
    .locals 0

    .prologue
    .line 785
    iput-object p1, p0, Lcom/sec/android/app/camera/OISTest$5;->this$0:Lcom/sec/android/app/camera/OISTest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 788
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 789
    .local v0, "action":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    const-string v1, "com.android.samsungtest.CameraStop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 791
    sget-object v1, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    const-string v2, "onReceive - get Stop Camera"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$5;->this$0:Lcom/sec/android/app/camera/OISTest;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/camera/OISTest;->mStopCamera:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/camera/OISTest;->access$1702(Lcom/sec/android/app/camera/OISTest;Z)Z

    .line 793
    iget-object v1, p0, Lcom/sec/android/app/camera/OISTest$5;->this$0:Lcom/sec/android/app/camera/OISTest;

    invoke-virtual {v1}, Lcom/sec/android/app/camera/OISTest;->finish()V

    .line 797
    :goto_0
    return-void

    .line 795
    :cond_0
    sget-object v1, Lcom/sec/android/app/camera/OISTest;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive - this action["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] is not defined"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
