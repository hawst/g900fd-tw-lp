.class public final Lcom/sec/android/app/camaftest/core/oned/MultiFormatOneDReader;
.super Lcom/sec/android/app/camaftest/core/oned/OneDReader;
.source "MultiFormatOneDReader.java"


# instance fields
.field private final readers:[Lcom/sec/android/app/camaftest/core/oned/OneDReader;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    invoke-direct {p0}, Lcom/sec/android/app/camaftest/core/oned/OneDReader;-><init>()V

    .line 42
    if-nez p1, :cond_a

    const/4 v0, 0x0

    .line 43
    .local v0, "possibleFormats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    :goto_0
    if-eqz p1, :cond_b

    sget-object v3, Lcom/sec/android/app/camaftest/core/DecodeHintType;->ASSUME_CODE_39_CHECK_DIGIT:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_b

    const/4 v2, 0x1

    .line 44
    .local v2, "useCode39CheckDigit":Z
    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .local v1, "readers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/oned/OneDReader;>;"
    if-eqz v0, :cond_8

    .line 46
    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_13:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_A:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->EAN_8:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->UPC_E:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 47
    :cond_0
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/MultiFormatUPCEANReader;

    invoke-direct {v3, p1}, Lcom/sec/android/app/camaftest/core/oned/MultiFormatUPCEANReader;-><init>(Ljava/util/Map;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 49
    :cond_1
    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODE_39:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 50
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/Code39Reader;

    invoke-direct {v3, v2}, Lcom/sec/android/app/camaftest/core/oned/Code39Reader;-><init>(Z)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_2
    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODE_93:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 53
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_3
    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODE_128:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 56
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/Code128Reader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/Code128Reader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 58
    :cond_4
    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->ITF:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 59
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/ITFReader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/ITFReader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 61
    :cond_5
    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->CODABAR:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 62
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/CodaBarReader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/CodaBarReader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_6
    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->RSS_14:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 65
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/RSS14Reader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/rss/RSS14Reader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_7
    sget-object v3, Lcom/sec/android/app/camaftest/core/BarcodeFormat;->RSS_EXPANDED:Lcom/sec/android/app/camaftest/core/BarcodeFormat;

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 68
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/RSSExpandedReader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/RSSExpandedReader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_8
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 72
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/MultiFormatUPCEANReader;

    invoke-direct {v3, p1}, Lcom/sec/android/app/camaftest/core/oned/MultiFormatUPCEANReader;-><init>(Ljava/util/Map;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 73
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/Code39Reader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/Code39Reader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 74
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/CodaBarReader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/CodaBarReader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 75
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/Code93Reader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 76
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/Code128Reader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/Code128Reader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 77
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/ITFReader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/ITFReader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 78
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/RSS14Reader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/rss/RSS14Reader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 79
    new-instance v3, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/RSSExpandedReader;

    invoke-direct {v3}, Lcom/sec/android/app/camaftest/core/oned/rss/expanded/RSSExpandedReader;-><init>()V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_9
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v3

    new-array v3, v3, [Lcom/sec/android/app/camaftest/core/oned/OneDReader;

    invoke-interface {v1, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/sec/android/app/camaftest/core/oned/OneDReader;

    iput-object v3, p0, Lcom/sec/android/app/camaftest/core/oned/MultiFormatOneDReader;->readers:[Lcom/sec/android/app/camaftest/core/oned/OneDReader;

    .line 82
    return-void

    .line 42
    .end local v0    # "possibleFormats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    .end local v1    # "readers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/oned/OneDReader;>;"
    .end local v2    # "useCode39CheckDigit":Z
    :cond_a
    sget-object v3, Lcom/sec/android/app/camaftest/core/DecodeHintType;->POSSIBLE_FORMATS:Lcom/sec/android/app/camaftest/core/DecodeHintType;

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Collection;

    move-object v0, v3

    goto/16 :goto_0

    .line 43
    .restart local v0    # "possibleFormats":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/camaftest/core/BarcodeFormat;>;"
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_1
.end method


# virtual methods
.method public decodeRow(ILcom/sec/android/app/camaftest/core/common/BitArray;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    .locals 5
    .param p1, "rowNumber"    # I
    .param p2, "row"    # Lcom/sec/android/app/camaftest/core/common/BitArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/app/camaftest/core/common/BitArray;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/camaftest/core/DecodeHintType;",
            "*>;)",
            "Lcom/sec/android/app/camaftest/core/Result;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/camaftest/core/NotFoundException;
        }
    .end annotation

    .prologue
    .line 86
    .local p3, "hints":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/app/camaftest/core/DecodeHintType;*>;"
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/MultiFormatOneDReader;->readers:[Lcom/sec/android/app/camaftest/core/oned/OneDReader;

    .local v0, "arr$":[Lcom/sec/android/app/camaftest/core/oned/OneDReader;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 88
    .local v3, "reader":Lcom/sec/android/app/camaftest/core/oned/OneDReader;
    :try_start_0
    invoke-virtual {v3, p1, p2, p3}, Lcom/sec/android/app/camaftest/core/oned/OneDReader;->decodeRow(ILcom/sec/android/app/camaftest/core/common/BitArray;Ljava/util/Map;)Lcom/sec/android/app/camaftest/core/Result;
    :try_end_0
    .catch Lcom/sec/android/app/camaftest/core/ReaderException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    .line 89
    :catch_0
    move-exception v4

    .line 86
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 94
    .end local v3    # "reader":Lcom/sec/android/app/camaftest/core/oned/OneDReader;
    :cond_0
    invoke-static {}, Lcom/sec/android/app/camaftest/core/NotFoundException;->getNotFoundInstance()Lcom/sec/android/app/camaftest/core/NotFoundException;

    move-result-object v4

    throw v4
.end method

.method public reset()V
    .locals 4

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/camaftest/core/oned/MultiFormatOneDReader;->readers:[Lcom/sec/android/app/camaftest/core/oned/OneDReader;

    .local v0, "arr$":[Lcom/sec/android/app/camaftest/core/oned/OneDReader;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 100
    .local v3, "reader":Lcom/sec/android/app/camaftest/core/Reader;
    invoke-interface {v3}, Lcom/sec/android/app/camaftest/core/Reader;->reset()V

    .line 99
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 102
    .end local v3    # "reader":Lcom/sec/android/app/camaftest/core/Reader;
    :cond_0
    return-void
.end method
