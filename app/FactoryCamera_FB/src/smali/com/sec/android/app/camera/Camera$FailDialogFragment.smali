.class public Lcom/sec/android/app/camera/Camera$FailDialogFragment;
.super Landroid/app/DialogFragment;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/camera/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FailDialogFragment"
.end annotation


# instance fields
.field message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3293
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/sec/android/app/camera/Camera$FailDialogFragment;
    .locals 3
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 3297
    new-instance v1, Lcom/sec/android/app/camera/Camera$FailDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/camera/Camera$FailDialogFragment;-><init>()V

    .line 3298
    .local v1, "dialog":Lcom/sec/android/app/camera/Camera$FailDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3299
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "message"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3300
    invoke-virtual {v1, v0}, Lcom/sec/android/app/camera/Camera$FailDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 3301
    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 3306
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 3307
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera$FailDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/camera/Camera$FailDialogFragment;->message:Ljava/lang/String;

    .line 3308
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 3314
    const v6, 0x7f030006

    const/4 v7, 0x0

    invoke-virtual {p1, v6, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 3315
    .local v4, "v":Landroid/view/View;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 3316
    .local v2, "displayRectangle":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera$FailDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    .line 3317
    .local v5, "window":Landroid/view/Window;
    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 3318
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    const v7, 0x3f333333    # 0.7f

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setMinimumWidth(I)V

    .line 3319
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x3f000000    # 0.5f

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setMinimumHeight(I)V

    .line 3320
    const v6, 0x7f09003d

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 3321
    .local v3, "tvMessage":Landroid/widget/TextView;
    const v6, 0x7f09003e

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 3322
    .local v1, "btYes":Landroid/widget/Button;
    const v6, 0x7f0a0002

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setText(I)V

    .line 3323
    const v6, 0x7f09003f

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 3324
    .local v0, "btNo":Landroid/widget/Button;
    const v6, 0x7f0a0003

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setText(I)V

    .line 3325
    invoke-virtual {p0}, Lcom/sec/android/app/camera/Camera$FailDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/view/Window;->requestFeature(I)Z

    .line 3326
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/android/app/camera/Camera$FailDialogFragment;->message:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " FAIL?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3327
    new-instance v6, Lcom/sec/android/app/camera/Camera$FailDialogFragment$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/camera/Camera$FailDialogFragment$1;-><init>(Lcom/sec/android/app/camera/Camera$FailDialogFragment;)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3342
    new-instance v6, Lcom/sec/android/app/camera/Camera$FailDialogFragment$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/camera/Camera$FailDialogFragment$2;-><init>(Lcom/sec/android/app/camera/Camera$FailDialogFragment;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3351
    return-object v4
.end method
