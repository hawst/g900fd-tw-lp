.class public final Lcom/sec/factory/camera/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/camera/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final CurrentTime:I = 0x7f090045

.field public static final EndTime:I = 0x7f090047

.field public static final Fail:I = 0x7f090044

.field public static final OIS_FAIL:I = 0x7f09004a

.field public static final OIS_PASS:I = 0x7f090049

.field public static final Pass:I = 0x7f090042

.field public static final Play:I = 0x7f090043

.field public static final auto_focus:I = 0x7f090000

.field public static final barcode_image_view:I = 0x7f09002c

.field public static final btNo:I = 0x7f09003f

.field public static final btYes:I = 0x7f09003e

.field public static final buttons_layout:I = 0x7f090041

.field public static final camera:I = 0x7f09000a

.field public static final camera_preview:I = 0x7f09000b

.field public static final camera_preview_layout:I = 0x7f090048

.field public static final contents_supplement_text_view:I = 0x7f090036

.field public static final contents_text_view:I = 0x7f090035

.field public static final decode:I = 0x7f090001

.field public static final decode_fail_view:I = 0x7f09002a

.field public static final decode_failed:I = 0x7f090002

.field public static final decode_succeeded:I = 0x7f090003

.field public static final decode_success_view:I = 0x7f09002b

.field public static final dumpCompanionFirmware:I = 0x7f09001f

.field public static final dumpFirmware:I = 0x7f09001d

.field public static final dumpFirmwareFront:I = 0x7f09001e

.field public static final fail_button:I = 0x7f090039

.field public static final focus_fail_indicator:I = 0x7f090012

.field public static final focus_rectangle:I = 0x7f090010

.field public static final focus_success_indicator:I = 0x7f090011

.field public static final format_text_view:I = 0x7f09002e

.field public static final format_text_view_label:I = 0x7f09002d

.field public static final getCheckCal:I = 0x7f09001c

.field public static final getISPVersion:I = 0x7f09001a

.field public static final getISPVoltageInfo:I = 0x7f090020

.field public static final getUpdateCount:I = 0x7f09001b

.field public static final getVendorId:I = 0x7f090021

.field public static final helptext:I = 0x7f09000e

.field public static final launch_product_query:I = 0x7f090004

.field public static final lowlightcapturetext:I = 0x7f090013

.field public static final message:I = 0x7f09003d

.field public static final meta_text_view:I = 0x7f090034

.field public static final meta_text_view_label:I = 0x7f090033

.field public static final ok_button:I = 0x7f090038

.field public static final phoneCAMFWVerCheck:I = 0x7f090014

.field public static final postimage:I = 0x7f09004c

.field public static final postview:I = 0x7f09004b

.field public static final preview_view:I = 0x7f090027

.field public static final quit:I = 0x7f090005

.field public static final restart_preview:I = 0x7f090006

.field public static final result_button_view:I = 0x7f090037

.field public static final result_view:I = 0x7f090029

.field public static final retry_button:I = 0x7f09003a

.field public static final return_scan_result:I = 0x7f090007

.field public static final scan_button:I = 0x7f09003c

.field public static final search_book_contents_failed:I = 0x7f090008

.field public static final search_book_contents_succeeded:I = 0x7f090009

.field public static final seekBar:I = 0x7f090046

.field public static final shutter_button:I = 0x7f09000c

.field public static final status_view:I = 0x7f09003b

.field public static final stop_button:I = 0x7f09000d

.field public static final surface:I = 0x7f090040

.field public static final sutter_button_layout:I = 0x7f09000f

.field public static final time_text_view:I = 0x7f090032

.field public static final time_text_view_label:I = 0x7f090031

.field public static final type_text_view:I = 0x7f090030

.field public static final type_text_view_label:I = 0x7f09002f

.field public static final updateCompanionFirmwareForce:I = 0x7f090016

.field public static final updateCompanionFirmwareInImage:I = 0x7f090019

.field public static final updateFirmwareForce:I = 0x7f090015

.field public static final updateFirmwareInImage:I = 0x7f090017

.field public static final updateFirmwareInImageFront:I = 0x7f090018

.field public static final user_getUpdateCount:I = 0x7f090026

.field public static final user_phoneCAMFWVerCheck:I = 0x7f090022

.field public static final user_phoneFWVerCheck:I = 0x7f090025

.field public static final user_updateFirmwareInImage:I = 0x7f090023

.field public static final user_updateFirmwareInImageFront:I = 0x7f090024

.field public static final viewfinder_view:I = 0x7f090028


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
