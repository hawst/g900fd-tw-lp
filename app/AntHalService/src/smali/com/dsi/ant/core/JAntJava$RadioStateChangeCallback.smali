.class Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;
.super Ljava/lang/Object;
.source "JAntJava.java"

# interfaces
.implements Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/core/JAntJava;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RadioStateChangeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/core/JAntJava;


# direct methods
.method private constructor <init>(Lcom/dsi/ant/core/JAntJava;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;->this$0:Lcom/dsi/ant/core/JAntJava;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dsi/ant/core/JAntJava;Lcom/dsi/ant/core/JAntJava$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/core/JAntJava;
    .param p2, "x1"    # Lcom/dsi/ant/core/JAntJava$1;

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;-><init>(Lcom/dsi/ant/core/JAntJava;)V

    return-void
.end method


# virtual methods
.method public onRadioStateChange(Z)V
    .locals 5
    .param p1, "up"    # Z

    .prologue
    const/4 v3, 0x1

    .line 121
    const-string v0, "JAntJava"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Radio State Changed: up = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mWaitforRadioState:Z
    invoke-static {v0}, Lcom/dsi/ant/core/JAntJava;->access$300(Lcom/dsi/ant/core/JAntJava;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-ne p1, v3, :cond_2

    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mState:I
    invoke-static {v0}, Lcom/dsi/ant/core/JAntJava;->access$400(Lcom/dsi/ant/core/JAntJava;)I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 124
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;->this$0:Lcom/dsi/ant/core/JAntJava;

    const/4 v1, 0x0

    # setter for: Lcom/dsi/ant/core/JAntJava;->mWaitforRadioState:Z
    invoke-static {v0, v1}, Lcom/dsi/ant/core/JAntJava;->access$302(Lcom/dsi/ant/core/JAntJava;Z)Z

    .line 125
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;->this$0:Lcom/dsi/ant/core/JAntJava;

    # setter for: Lcom/dsi/ant/core/JAntJava;->mAntRadioEnabled:Z
    invoke-static {v0, v3}, Lcom/dsi/ant/core/JAntJava;->access$102(Lcom/dsi/ant/core/JAntJava;Z)Z

    .line 126
    const-string v0, "JAntJava"

    const-string v1, "bt radio enabled while ant is enabling"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;->this$0:Lcom/dsi/ant/core/JAntJava;

    monitor-enter v1

    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mConnectedThread:Lcom/dsi/ant/core/JAntJava$ConnectedThread;
    invoke-static {v0}, Lcom/dsi/ant/core/JAntJava;->access$500(Lcom/dsi/ant/core/JAntJava;)Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    move-result-object v0

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;->this$0:Lcom/dsi/ant/core/JAntJava;

    new-instance v2, Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    iget-object v3, p0, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;->this$0:Lcom/dsi/ant/core/JAntJava;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/dsi/ant/core/JAntJava$ConnectedThread;-><init>(Lcom/dsi/ant/core/JAntJava;Lcom/dsi/ant/core/JAntJava$1;)V

    # setter for: Lcom/dsi/ant/core/JAntJava;->mConnectedThread:Lcom/dsi/ant/core/JAntJava$ConnectedThread;
    invoke-static {v0, v2}, Lcom/dsi/ant/core/JAntJava;->access$502(Lcom/dsi/ant/core/JAntJava;Lcom/dsi/ant/core/JAntJava$ConnectedThread;)Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    .line 130
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mConnectedThread:Lcom/dsi/ant/core/JAntJava$ConnectedThread;
    invoke-static {v0}, Lcom/dsi/ant/core/JAntJava;->access$500(Lcom/dsi/ant/core/JAntJava;)Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->start()V

    .line 132
    :cond_0
    monitor-exit v1

    .line 136
    :cond_1
    :goto_0
    return-void

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 133
    :cond_2
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mState:I
    invoke-static {v0}, Lcom/dsi/ant/core/JAntJava;->access$400(Lcom/dsi/ant/core/JAntJava;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 134
    const-string v0, "JAntJava"

    const-string v1, "bt radio disabled while ant is enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
