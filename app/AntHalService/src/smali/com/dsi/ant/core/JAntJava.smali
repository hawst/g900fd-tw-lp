.class public Lcom/dsi/ant/core/JAntJava;
.super Ljava/lang/Object;
.source "JAntJava.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/core/JAntJava$ConnectedThread;,
        Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;,
        Lcom/dsi/ant/core/JAntJava$ICallback;
    }
.end annotation


# static fields
.field private static mCallback:Lcom/dsi/ant/core/JAntJava$ICallback;


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mAntRadioEnabled:Z

.field private mConnectedThread:Lcom/dsi/ant/core/JAntJava$ConnectedThread;

.field private mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mHciSock:Landroid/bluetooth/BluetoothSocket;

.field private mIntentAirplaneMode:Landroid/content/BroadcastReceiver;

.field mRadioManager:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

.field private mRadioStCallback:Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;

.field private mState:I

.field private mWaitforRadioState:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/dsi/ant/core/JAntJava;->mCallback:Lcom/dsi/ant/core/JAntJava$ICallback;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-boolean v0, p0, Lcom/dsi/ant/core/JAntJava;->mAntRadioEnabled:Z

    .line 50
    iput-boolean v0, p0, Lcom/dsi/ant/core/JAntJava;->mWaitforRadioState:Z

    .line 52
    iput v0, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    .line 93
    new-instance v0, Lcom/dsi/ant/core/JAntJava$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/core/JAntJava$1;-><init>(Lcom/dsi/ant/core/JAntJava;)V

    iput-object v0, p0, Lcom/dsi/ant/core/JAntJava;->mIntentAirplaneMode:Landroid/content/BroadcastReceiver;

    .line 241
    new-instance v0, Lcom/dsi/ant/core/JAntJava$2;

    invoke-direct {v0, p0}, Lcom/dsi/ant/core/JAntJava$2;-><init>(Lcom/dsi/ant/core/JAntJava;)V

    iput-object v0, p0, Lcom/dsi/ant/core/JAntJava;->mHandler:Landroid/os/Handler;

    .line 276
    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/core/JAntJava;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/core/JAntJava;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/dsi/ant/core/JAntJava;->mAntRadioEnabled:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/dsi/ant/core/JAntJava;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$102(Lcom/dsi/ant/core/JAntJava;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/dsi/ant/core/JAntJava;->mAntRadioEnabled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/dsi/ant/core/JAntJava;II)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/core/JAntJava;->sendMessageDelayed(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/dsi/ant/core/JAntJava;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/dsi/ant/core/JAntJava;->mWaitforRadioState:Z

    return v0
.end method

.method static synthetic access$302(Lcom/dsi/ant/core/JAntJava;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/dsi/ant/core/JAntJava;->mWaitforRadioState:Z

    return p1
.end method

.method static synthetic access$400(Lcom/dsi/ant/core/JAntJava;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    return v0
.end method

.method static synthetic access$402(Lcom/dsi/ant/core/JAntJava;I)I
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    return p1
.end method

.method static synthetic access$500(Lcom/dsi/ant/core/JAntJava;)Lcom/dsi/ant/core/JAntJava$ConnectedThread;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava;->mConnectedThread:Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    return-object v0
.end method

.method static synthetic access$502(Lcom/dsi/ant/core/JAntJava;Lcom/dsi/ant/core/JAntJava$ConnectedThread;)Lcom/dsi/ant/core/JAntJava$ConnectedThread;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;
    .param p1, "x1"    # Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/dsi/ant/core/JAntJava;->mConnectedThread:Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    return-object p1
.end method

.method static synthetic access$800(Lcom/dsi/ant/core/JAntJava;)Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava;->mRadioStCallback:Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;

    return-object v0
.end method

.method static synthetic access$900(Lcom/dsi/ant/core/JAntJava;)Landroid/bluetooth/BluetoothSocket;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;

    return-object v0
.end method

.method static synthetic access$902(Lcom/dsi/ant/core/JAntJava;Landroid/bluetooth/BluetoothSocket;)Landroid/bluetooth/BluetoothSocket;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/core/JAntJava;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothSocket;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;

    return-object p1
.end method

.method private sendAntStateChange(I)V
    .locals 2
    .param p1, "NewState"    # I

    .prologue
    .line 381
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 382
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 383
    return-void
.end method

.method private sendMessageDelayed(II)V
    .locals 4
    .param p1, "messageId"    # I
    .param p2, "milisec"    # I

    .prologue
    .line 233
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 234
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 235
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mHandler:Landroid/os/Handler;

    int-to-long v2, p2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 236
    return-void
.end method


# virtual methods
.method public declared-synchronized ANTTxMessage([B)Lcom/dsi/ant/core/JAntStatus;
    .locals 4
    .param p1, "message"    # [B

    .prologue
    .line 334
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;

    if-nez v1, :cond_0

    .line 335
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/dsi/ant/core/JAntJava;->sendAntStateChange(I)V

    .line 336
    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->FAILED:Lcom/dsi/ant/core/JAntStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    :goto_0
    monitor-exit p0

    return-object v1

    .line 339
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345
    :try_start_2
    const-string v1, "JAntJava"

    const-string v2, "ANTTxMessage: exiting"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    goto :goto_0

    .line 340
    :catch_0
    move-exception v0

    .line 341
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "JAntJava"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ANTTxMessage: failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->FAILED:Lcom/dsi/ant/core/JAntStatus;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 334
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public create(Lcom/dsi/ant/core/JAntJava$ICallback;Landroid/content/Context;)Lcom/dsi/ant/core/JAntStatus;
    .locals 4
    .param p1, "callback"    # Lcom/dsi/ant/core/JAntJava$ICallback;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    const-string v1, "JAntJava"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Calling JAntJava:create, callback: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 72
    iget v1, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    if-nez v1, :cond_0

    .line 73
    const/4 v1, 0x4

    iput v1, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    .line 74
    :cond_0
    sput-object p1, Lcom/dsi/ant/core/JAntJava;->mCallback:Lcom/dsi/ant/core/JAntJava$ICallback;

    .line 75
    iput-object p2, p0, Lcom/dsi/ant/core/JAntJava;->mContext:Landroid/content/Context;

    .line 77
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 78
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 79
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/dsi/ant/core/JAntJava;->mIntentAirplaneMode:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 81
    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    return-object v1
.end method

.method public destroy()Lcom/dsi/ant/core/JAntStatus;
    .locals 2

    .prologue
    .line 85
    const-string v0, "JAntJava"

    const-string v1, "destroy: entered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-virtual {p0}, Lcom/dsi/ant/core/JAntJava;->disable()Lcom/dsi/ant/core/JAntStatus;

    .line 87
    sget-object v0, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    return-object v0
.end method

.method public disable()Lcom/dsi/ant/core/JAntStatus;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    .line 190
    const-string v2, "JAntJava"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disable: entered, mState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget v2, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    if-nez v2, :cond_0

    .line 192
    const-string v2, "JAntJava"

    const-string v3, "JAntJava is not initialized"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->FAILED:Lcom/dsi/ant/core/JAntStatus;

    .line 228
    :goto_0
    return-object v2

    .line 195
    :cond_0
    const/4 v0, 0x0

    .line 196
    .local v0, "s":Landroid/bluetooth/BluetoothSocket;
    const/4 v1, 0x0

    .line 197
    .local v1, "t":Ljava/lang/Thread;
    monitor-enter p0

    .line 198
    :try_start_0
    iget v2, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    if-eq v2, v6, :cond_1

    iget v2, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    if-eq v2, v5, :cond_1

    .line 200
    iget-object v2, p0, Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;

    if-eqz v2, :cond_3

    .line 201
    const/4 v2, 0x3

    iput v2, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    .line 202
    iget v2, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    invoke-direct {p0, v2}, Lcom/dsi/ant/core/JAntJava;->sendAntStateChange(I)V

    .line 203
    iget-object v2, p0, Lcom/dsi/ant/core/JAntJava;->mRadioManager:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/dsi/ant/core/JAntJava;->mRadioStCallback:Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;

    invoke-virtual {v2, v3, v4}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->releaseRadioReference(ILcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;)Z

    .line 204
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/core/JAntJava;->mAntRadioEnabled:Z

    .line 205
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;

    .line 206
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mConnectedThread:Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    .line 207
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;

    .line 208
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dsi/ant/core/JAntJava;->mConnectedThread:Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    .line 214
    :cond_1
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 217
    iget v2, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    invoke-direct {p0, v2}, Lcom/dsi/ant/core/JAntJava;->sendAntStateChange(I)V

    .line 218
    :try_start_1
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 221
    :goto_2
    const-string v2, "JAntJava"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "waiting thread exit: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 225
    :goto_3
    const-string v2, "JAntJava"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "thread exited: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_2
    const-string v2, "JAntJava"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disable exit: mState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    invoke-static {v4}, Lcom/dsi/ant/server/AntHalDefine;->getAntHalStateString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    goto/16 :goto_0

    .line 210
    :cond_3
    const/4 v2, 0x4

    :try_start_3
    iput v2, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    .line 211
    iget v2, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    invoke-direct {p0, v2}, Lcom/dsi/ant/core/JAntJava;->sendAntStateChange(I)V

    goto :goto_1

    .line 214
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 219
    :catch_0
    move-exception v2

    goto :goto_2

    .line 223
    :catch_1
    move-exception v2

    goto :goto_3
.end method

.method public declared-synchronized enable()Lcom/dsi/ant/core/JAntStatus;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 141
    monitor-enter p0

    :try_start_0
    const-string v1, "JAntJava"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable: entered, mState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget v1, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    if-nez v1, :cond_0

    .line 150
    const-string v1, "JAntJava"

    const-string v2, "JAntJava is not initialized"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->FAILED:Lcom/dsi/ant/core/JAntStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    :goto_0
    monitor-exit p0

    return-object v1

    .line 153
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    if-eq v1, v5, :cond_3

    iget v1, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    if-eq v1, v4, :cond_3

    .line 155
    const/4 v1, 0x1

    iput v1, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    .line 156
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 158
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mRadioManager:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    if-nez v1, :cond_1

    .line 159
    invoke-static {}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->getRadioManager()Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    move-result-object v1

    iput-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mRadioManager:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .line 162
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mRadioStCallback:Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;

    if-nez v1, :cond_2

    .line 163
    new-instance v1, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;-><init>(Lcom/dsi/ant/core/JAntJava;Lcom/dsi/ant/core/JAntJava$1;)V

    iput-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mRadioStCallback:Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;

    .line 166
    :cond_2
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/dsi/ant/core/JAntJava;->sendAntStateChange(I)V

    .line 168
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mRadioManager:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    invoke-virtual {v1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->isRadioEnabled()Z

    move-result v0

    .line 170
    .local v0, "enabled":Z
    const-string v1, "JAntJava"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable: isRadioEnabled, enabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const-string v1, "JAntJava"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable: addRadioReference, RADIO_TYPE_ANT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dsi/ant/core/JAntJava;->mRadioStCallback:Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mRadioManager:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/dsi/ant/core/JAntJava;->mRadioStCallback:Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;

    invoke-virtual {v1, v2, v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->addRadioReference(ILcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;)Z

    .line 173
    if-nez v0, :cond_4

    .line 174
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/core/JAntJava;->mWaitforRadioState:Z

    .line 180
    :goto_1
    if-eqz v0, :cond_3

    .line 181
    new-instance v1, Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/dsi/ant/core/JAntJava$ConnectedThread;-><init>(Lcom/dsi/ant/core/JAntJava;Lcom/dsi/ant/core/JAntJava$1;)V

    iput-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mConnectedThread:Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    .line 182
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava;->mConnectedThread:Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    invoke-virtual {v1}, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->start()V

    .line 185
    .end local v0    # "enabled":Z
    :cond_3
    const-string v1, "JAntJava"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable exit: mState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    invoke-static {v3}, Lcom/dsi/ant/server/AntHalDefine;->getAntHalStateString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    goto/16 :goto_0

    .line 177
    .restart local v0    # "enabled":Z
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/core/JAntJava;->mAntRadioEnabled:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 141
    .end local v0    # "enabled":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getRadioEnabledStatus()I
    .locals 3

    .prologue
    .line 327
    monitor-enter p0

    :try_start_0
    const-string v0, "JAntJava"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getRadioEnabledStatus: entered, mState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/core/JAntJava;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    iget v0, p0, Lcom/dsi/ant/core/JAntJava;->mState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 327
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public hardReset()Lcom/dsi/ant/core/JAntStatus;
    .locals 2

    .prologue
    .line 365
    const-string v0, "JAntJava"

    const-string v1, "hardReset: entered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    sget-object v0, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    return-object v0
.end method

.method public onAntRxMessage([B)V
    .locals 2
    .param p1, "RxMessage"    # [B

    .prologue
    .line 371
    sget-object v0, Lcom/dsi/ant/core/JAntJava;->mCallback:Lcom/dsi/ant/core/JAntJava$ICallback;

    if-eqz v0, :cond_0

    .line 373
    sget-object v0, Lcom/dsi/ant/core/JAntJava;->mCallback:Lcom/dsi/ant/core/JAntJava$ICallback;

    invoke-interface {v0, p1}, Lcom/dsi/ant/core/JAntJava$ICallback;->ANTRxMessage([B)V

    .line 379
    :goto_0
    return-void

    .line 377
    :cond_0
    const-string v0, "JAntJava"

    const-string v1, "onAntRxMessage: callback is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onAntStateChange(I)V
    .locals 3
    .param p1, "NewState"    # I

    .prologue
    .line 386
    const-string v0, "JAntJava"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAntStateChange: NewState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    sget-object v0, Lcom/dsi/ant/core/JAntJava;->mCallback:Lcom/dsi/ant/core/JAntJava$ICallback;

    if-eqz v0, :cond_0

    .line 390
    sget-object v0, Lcom/dsi/ant/core/JAntJava;->mCallback:Lcom/dsi/ant/core/JAntJava$ICallback;

    invoke-interface {v0, p1}, Lcom/dsi/ant/core/JAntJava$ICallback;->ANTStateChange(I)V

    .line 396
    :goto_0
    return-void

    .line 394
    :cond_0
    const-string v0, "JAntJava"

    const-string v1, "nativeCb_AntStateChange: callback is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
