.class Lcom/dsi/ant/core/JAntJava$1;
.super Landroid/content/BroadcastReceiver;
.source "JAntJava.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/core/JAntJava;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/core/JAntJava;


# direct methods
.method constructor <init>(Lcom/dsi/ant/core/JAntJava;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/dsi/ant/core/JAntJava$1;->this$0:Lcom/dsi/ant/core/JAntJava;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 96
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "action":Ljava/lang/String;
    const-string v2, "JAntJava"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mAirplaneModeReceiver action: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 99
    iget-object v2, p0, Lcom/dsi/ant/core/JAntJava$1;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/dsi/ant/core/JAntJava;->access$000(Lcom/dsi/ant/core/JAntJava;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 101
    .local v1, "airplaneModeOn":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 102
    iget-object v3, p0, Lcom/dsi/ant/core/JAntJava$1;->this$0:Lcom/dsi/ant/core/JAntJava;

    monitor-enter v3

    .line 103
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/core/JAntJava$1;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mAntRadioEnabled:Z
    invoke-static {v2}, Lcom/dsi/ant/core/JAntJava;->access$100(Lcom/dsi/ant/core/JAntJava;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    const-string v2, "JAntJava"

    const-string v4, "sendMessageDelayed MESSAGE_DISABLE_RADIO: 2000"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v2, p0, Lcom/dsi/ant/core/JAntJava$1;->this$0:Lcom/dsi/ant/core/JAntJava;

    const/16 v4, 0x3e8

    const/16 v5, 0x7d0

    # invokes: Lcom/dsi/ant/core/JAntJava;->sendMessageDelayed(II)V
    invoke-static {v2, v4, v5}, Lcom/dsi/ant/core/JAntJava;->access$200(Lcom/dsi/ant/core/JAntJava;II)V

    .line 112
    :cond_0
    monitor-exit v3

    .line 115
    .end local v1    # "airplaneModeOn":I
    :cond_1
    return-void

    .line 112
    .restart local v1    # "airplaneModeOn":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
