.class Lcom/dsi/ant/core/JAntJava$2;
.super Landroid/os/Handler;
.source "JAntJava.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/core/JAntJava;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/core/JAntJava;


# direct methods
.method constructor <init>(Lcom/dsi/ant/core/JAntJava;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x2

    .line 244
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 267
    iget v0, p1, Landroid/os/Message;->what:I

    if-ltz v0, :cond_0

    iget v0, p1, Landroid/os/Message;->what:I

    if-gt v0, v2, :cond_0

    .line 269
    const-string v0, "JAntJava"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "callback the ant state change: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Lcom/dsi/ant/core/JAntJava;->onAntStateChange(I)V

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 246
    :sswitch_0
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    monitor-enter v1

    .line 248
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mAntRadioEnabled:Z
    invoke-static {v0}, Lcom/dsi/ant/core/JAntJava;->access$100(Lcom/dsi/ant/core/JAntJava;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    iget-object v0, v0, Lcom/dsi/ant/core/JAntJava;->mRadioManager:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mRadioStCallback:Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;
    invoke-static {v3}, Lcom/dsi/ant/core/JAntJava;->access$800(Lcom/dsi/ant/core/JAntJava;)Lcom/dsi/ant/core/JAntJava$RadioStateChangeCallback;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->releaseRadioReference(ILcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;)Z

    .line 250
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    const/4 v2, 0x0

    # setter for: Lcom/dsi/ant/core/JAntJava;->mAntRadioEnabled:Z
    invoke-static {v0, v2}, Lcom/dsi/ant/core/JAntJava;->access$102(Lcom/dsi/ant/core/JAntJava;Z)Z

    .line 252
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 256
    :sswitch_1
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    # setter for: Lcom/dsi/ant/core/JAntJava;->mState:I
    invoke-static {v0, v2}, Lcom/dsi/ant/core/JAntJava;->access$402(Lcom/dsi/ant/core/JAntJava;I)I

    .line 257
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mState:I
    invoke-static {v1}, Lcom/dsi/ant/core/JAntJava;->access$400(Lcom/dsi/ant/core/JAntJava;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/core/JAntJava;->onAntStateChange(I)V

    goto :goto_0

    .line 260
    :sswitch_2
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    # setter for: Lcom/dsi/ant/core/JAntJava;->mState:I
    invoke-static {v0, v1}, Lcom/dsi/ant/core/JAntJava;->access$402(Lcom/dsi/ant/core/JAntJava;I)I

    .line 261
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mState:I
    invoke-static {v1}, Lcom/dsi/ant/core/JAntJava;->access$400(Lcom/dsi/ant/core/JAntJava;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/core/JAntJava;->onAntStateChange(I)V

    goto :goto_0

    .line 264
    :sswitch_3
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava$2;->this$0:Lcom/dsi/ant/core/JAntJava;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    invoke-virtual {v1, v0}, Lcom/dsi/ant/core/JAntJava;->onAntRxMessage([B)V

    goto :goto_0

    .line 244
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_1
        0xc -> :sswitch_2
        0x3e8 -> :sswitch_0
        0x3e9 -> :sswitch_3
    .end sparse-switch
.end method
