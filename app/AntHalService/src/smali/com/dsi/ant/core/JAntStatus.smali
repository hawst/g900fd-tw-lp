.class public final enum Lcom/dsi/ant/core/JAntStatus;
.super Ljava/lang/Enum;
.source "JAntStatus.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/core/JAntStatus;

.field public static final enum AUDIO_OPERATION_UNAVAILIBLE_RESOURCES:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum COMMAND_WRITE_FAILED:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum CONTEXT_DOESNT_EXIST:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum CONTEXT_NOT_DESTROYED:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum CONTEXT_NOT_DISABLED:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum CONTEXT_NOT_ENABLED:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum DISABLING_IN_PROGRESS:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum FAILED:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum FAILED_BT_NOT_INITIALIZED:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum HARDWARE_ERR:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum INTERNAL_ERROR:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum INVALID_PARM:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum IN_PROGRESS:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum NOT_APPLICABLE:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum NOT_DE_INITIALIZED:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum NOT_INITIALIZED:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum NOT_SUPPORTED:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum NO_VALUE:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum NO_VALUE_AVAILABLE:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum PENDING:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum SCRIPT_EXEC_FAILED:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum SUCCESS:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum TOO_MANY_PENDING_CMDS:Lcom/dsi/ant/core/JAntStatus;

.field public static final enum TRANSPORT_INIT_ERR:Lcom/dsi/ant/core/JAntStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 22
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    .line 23
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5, v5}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->FAILED:Lcom/dsi/ant/core/JAntStatus;

    .line 24
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v6, v6}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->PENDING:Lcom/dsi/ant/core/JAntStatus;

    .line 25
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "INVALID_PARM"

    invoke-direct {v0, v1, v7, v7}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->INVALID_PARM:Lcom/dsi/ant/core/JAntStatus;

    .line 26
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v8, v8}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->IN_PROGRESS:Lcom/dsi/ant/core/JAntStatus;

    .line 27
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "NOT_APPLICABLE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->NOT_APPLICABLE:Lcom/dsi/ant/core/JAntStatus;

    .line 28
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "NOT_SUPPORTED"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->NOT_SUPPORTED:Lcom/dsi/ant/core/JAntStatus;

    .line 29
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "INTERNAL_ERROR"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->INTERNAL_ERROR:Lcom/dsi/ant/core/JAntStatus;

    .line 30
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "TRANSPORT_INIT_ERR"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->TRANSPORT_INIT_ERR:Lcom/dsi/ant/core/JAntStatus;

    .line 31
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "HARDWARE_ERR"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->HARDWARE_ERR:Lcom/dsi/ant/core/JAntStatus;

    .line 32
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "NO_VALUE_AVAILABLE"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->NO_VALUE_AVAILABLE:Lcom/dsi/ant/core/JAntStatus;

    .line 33
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "CONTEXT_DOESNT_EXIST"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->CONTEXT_DOESNT_EXIST:Lcom/dsi/ant/core/JAntStatus;

    .line 34
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "CONTEXT_NOT_DESTROYED"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->CONTEXT_NOT_DESTROYED:Lcom/dsi/ant/core/JAntStatus;

    .line 35
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "CONTEXT_NOT_ENABLED"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->CONTEXT_NOT_ENABLED:Lcom/dsi/ant/core/JAntStatus;

    .line 36
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "CONTEXT_NOT_DISABLED"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->CONTEXT_NOT_DISABLED:Lcom/dsi/ant/core/JAntStatus;

    .line 37
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "NOT_DE_INITIALIZED"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->NOT_DE_INITIALIZED:Lcom/dsi/ant/core/JAntStatus;

    .line 38
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "NOT_INITIALIZED"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->NOT_INITIALIZED:Lcom/dsi/ant/core/JAntStatus;

    .line 39
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "TOO_MANY_PENDING_CMDS"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->TOO_MANY_PENDING_CMDS:Lcom/dsi/ant/core/JAntStatus;

    .line 40
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "DISABLING_IN_PROGRESS"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->DISABLING_IN_PROGRESS:Lcom/dsi/ant/core/JAntStatus;

    .line 41
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "COMMAND_WRITE_FAILED"

    const/16 v2, 0x13

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->COMMAND_WRITE_FAILED:Lcom/dsi/ant/core/JAntStatus;

    .line 42
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "SCRIPT_EXEC_FAILED"

    const/16 v2, 0x14

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->SCRIPT_EXEC_FAILED:Lcom/dsi/ant/core/JAntStatus;

    .line 44
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "FAILED_BT_NOT_INITIALIZED"

    const/16 v2, 0x15

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->FAILED_BT_NOT_INITIALIZED:Lcom/dsi/ant/core/JAntStatus;

    .line 45
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "AUDIO_OPERATION_UNAVAILIBLE_RESOURCES"

    const/16 v2, 0x16

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->AUDIO_OPERATION_UNAVAILIBLE_RESOURCES:Lcom/dsi/ant/core/JAntStatus;

    .line 47
    new-instance v0, Lcom/dsi/ant/core/JAntStatus;

    const-string v1, "NO_VALUE"

    const/16 v2, 0x17

    const/16 v3, 0x64

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/core/JAntStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->NO_VALUE:Lcom/dsi/ant/core/JAntStatus;

    .line 20
    const/16 v0, 0x18

    new-array v0, v0, [Lcom/dsi/ant/core/JAntStatus;

    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->FAILED:Lcom/dsi/ant/core/JAntStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->PENDING:Lcom/dsi/ant/core/JAntStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->INVALID_PARM:Lcom/dsi/ant/core/JAntStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->IN_PROGRESS:Lcom/dsi/ant/core/JAntStatus;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->NOT_APPLICABLE:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->NOT_SUPPORTED:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->INTERNAL_ERROR:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->TRANSPORT_INIT_ERR:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->HARDWARE_ERR:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->NO_VALUE_AVAILABLE:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->CONTEXT_DOESNT_EXIST:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->CONTEXT_NOT_DESTROYED:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->CONTEXT_NOT_ENABLED:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->CONTEXT_NOT_DISABLED:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->NOT_DE_INITIALIZED:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->NOT_INITIALIZED:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->TOO_MANY_PENDING_CMDS:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->DISABLING_IN_PROGRESS:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->COMMAND_WRITE_FAILED:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->SCRIPT_EXEC_FAILED:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->FAILED_BT_NOT_INITIALIZED:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->AUDIO_OPERATION_UNAVAILIBLE_RESOURCES:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->NO_VALUE:Lcom/dsi/ant/core/JAntStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/core/JAntStatus;->$VALUES:[Lcom/dsi/ant/core/JAntStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    iput p3, p0, Lcom/dsi/ant/core/JAntStatus;->value:I

    .line 54
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/core/JAntStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/dsi/ant/core/JAntStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/core/JAntStatus;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/core/JAntStatus;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/dsi/ant/core/JAntStatus;->$VALUES:[Lcom/dsi/ant/core/JAntStatus;

    invoke-virtual {v0}, [Lcom/dsi/ant/core/JAntStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/core/JAntStatus;

    return-object v0
.end method
