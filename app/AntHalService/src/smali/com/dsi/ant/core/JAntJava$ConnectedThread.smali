.class Lcom/dsi/ant/core/JAntJava$ConnectedThread;
.super Ljava/lang/Thread;
.source "JAntJava.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/core/JAntJava;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectedThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/core/JAntJava;


# direct methods
.method private constructor <init>(Lcom/dsi/ant/core/JAntJava;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dsi/ant/core/JAntJava;Lcom/dsi/ant/core/JAntJava$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/core/JAntJava;
    .param p2, "x1"    # Lcom/dsi/ant/core/JAntJava$1;

    .prologue
    .line 276
    invoke-direct {p0, p1}, Lcom/dsi/ant/core/JAntJava$ConnectedThread;-><init>(Lcom/dsi/ant/core/JAntJava;)V

    return-void
.end method

.method private final init()Ljava/io/InputStream;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 278
    iget-object v1, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    monitor-enter v1

    .line 279
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;
    invoke-static {v0}, Lcom/dsi/ant/core/JAntJava;->access$900(Lcom/dsi/ant/core/JAntJava;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;
    invoke-static {v0}, Lcom/dsi/ant/core/JAntJava;->access$900(Lcom/dsi/ant/core/JAntJava;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    iget-object v2, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v2}, Lcom/dsi/ant/core/JAntJava;->access$1000(Lcom/dsi/ant/core/JAntJava;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    const/16 v3, 0xec

    const/16 v4, 0x2d

    const/16 v5, 0xff

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/bluetooth/BluetoothAdapter;->createVendorHciSocket(IIII)Landroid/bluetooth/BluetoothSocket;

    move-result-object v2

    # setter for: Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;
    invoke-static {v0, v2}, Lcom/dsi/ant/core/JAntJava;->access$902(Lcom/dsi/ant/core/JAntJava;Landroid/bluetooth/BluetoothSocket;)Landroid/bluetooth/BluetoothSocket;

    .line 282
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;
    invoke-static {v0}, Lcom/dsi/ant/core/JAntJava;->access$900(Lcom/dsi/ant/core/JAntJava;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->connect()V

    .line 283
    iget-object v0, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;
    invoke-static {v0}, Lcom/dsi/ant/core/JAntJava;->access$900(Lcom/dsi/ant/core/JAntJava;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/16 v6, 0xc8

    .line 289
    const-string v7, "JAntJava"

    const-string v8, "mConnectedThread, enter run()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    :try_start_0
    invoke-direct {p0}, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->init()Ljava/io/InputStream;

    move-result-object v5

    .line 293
    .local v5, "is":Ljava/io/InputStream;
    iget-object v7, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    const/16 v8, 0xc

    const/16 v9, 0xc8

    # invokes: Lcom/dsi/ant/core/JAntJava;->sendMessageDelayed(II)V
    invoke-static {v7, v8, v9}, Lcom/dsi/ant/core/JAntJava;->access$200(Lcom/dsi/ant/core/JAntJava;II)V

    .line 294
    const/16 v7, 0x400

    new-array v0, v7, [B

    .line 297
    .local v0, "buffer":[B
    const/4 v4, 0x0

    .line 300
    .local v4, "enable_state_updated":Z
    :cond_0
    :goto_0
    invoke-virtual {v5, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 301
    .local v1, "bytes":I
    if-lez v1, :cond_0

    .line 302
    iget-object v7, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/dsi/ant/core/JAntJava;->onAntRxMessage([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 305
    .end local v0    # "buffer":[B
    .end local v1    # "bytes":I
    .end local v4    # "enable_state_updated":Z
    .end local v5    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v2

    .line 306
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "JAntJava"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "run(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    .end local v2    # "e":Ljava/io/IOException;
    :goto_1
    iget-object v7, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    monitor-enter v7

    .line 313
    :try_start_1
    iget-object v8, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;
    invoke-static {v8}, Lcom/dsi/ant/core/JAntJava;->access$900(Lcom/dsi/ant/core/JAntJava;)Landroid/bluetooth/BluetoothSocket;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    if-eqz v8, :cond_1

    .line 314
    :try_start_2
    iget-object v8, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;
    invoke-static {v8}, Lcom/dsi/ant/core/JAntJava;->access$900(Lcom/dsi/ant/core/JAntJava;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v8

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 317
    :goto_2
    :try_start_3
    iget-object v8, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    const/4 v9, 0x0

    # setter for: Lcom/dsi/ant/core/JAntJava;->mHciSock:Landroid/bluetooth/BluetoothSocket;
    invoke-static {v8, v9}, Lcom/dsi/ant/core/JAntJava;->access$902(Lcom/dsi/ant/core/JAntJava;Landroid/bluetooth/BluetoothSocket;)Landroid/bluetooth/BluetoothSocket;

    .line 319
    :cond_1
    iget-object v8, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    const/4 v9, 0x0

    # setter for: Lcom/dsi/ant/core/JAntJava;->mConnectedThread:Lcom/dsi/ant/core/JAntJava$ConnectedThread;
    invoke-static {v8, v9}, Lcom/dsi/ant/core/JAntJava;->access$502(Lcom/dsi/ant/core/JAntJava;Lcom/dsi/ant/core/JAntJava$ConnectedThread;)Lcom/dsi/ant/core/JAntJava$ConnectedThread;

    .line 320
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 321
    iget-object v7, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    const/16 v8, 0xa

    iget-object v9, p0, Lcom/dsi/ant/core/JAntJava$ConnectedThread;->this$0:Lcom/dsi/ant/core/JAntJava;

    # getter for: Lcom/dsi/ant/core/JAntJava;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v9}, Lcom/dsi/ant/core/JAntJava;->access$1000(Lcom/dsi/ant/core/JAntJava;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v9

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v9

    if-eqz v9, :cond_2

    :goto_3
    # invokes: Lcom/dsi/ant/core/JAntJava;->sendMessageDelayed(II)V
    invoke-static {v7, v8, v6}, Lcom/dsi/ant/core/JAntJava;->access$200(Lcom/dsi/ant/core/JAntJava;II)V

    .line 322
    const-string v6, "JAntJava"

    const-string v7, "mConnectedThread, exit run()"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    return-void

    .line 307
    :catch_1
    move-exception v3

    .line 308
    .local v3, "ee":Ljava/lang/NullPointerException;
    const-string v7, "JAntJava"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "run(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 320
    .end local v3    # "ee":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v6

    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v6

    .line 321
    :cond_2
    const/16 v6, 0x7d0

    goto :goto_3

    .line 315
    :catch_2
    move-exception v8

    goto :goto_2
.end method
