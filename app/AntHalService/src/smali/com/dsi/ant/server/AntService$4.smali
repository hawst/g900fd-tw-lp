.class Lcom/dsi/ant/server/AntService$4;
.super Ljava/lang/Object;
.source "AntService.java"

# interfaces
.implements Lcom/dsi/ant/core/JAntJava$ICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/server/AntService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/server/AntService;


# direct methods
.method constructor <init>(Lcom/dsi/ant/server/AntService;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/dsi/ant/server/AntService$4;->this$0:Lcom/dsi/ant/server/AntService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized ANTRxMessage([B)V
    .locals 3
    .param p1, "message"    # [B

    .prologue
    .line 526
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/server/AntService$4;->this$0:Lcom/dsi/ant/server/AntService;

    iget-object v1, v1, Lcom/dsi/ant/server/AntService;->mCallback:Lcom/dsi/ant/server/IAntHalCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 530
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/server/AntService$4;->this$0:Lcom/dsi/ant/server/AntService;

    iget-object v1, v1, Lcom/dsi/ant/server/AntService;->mCallback:Lcom/dsi/ant/server/IAntHalCallback;

    invoke-interface {v1, p1}, Lcom/dsi/ant/server/IAntHalCallback;->antHalRxMessage([B)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 542
    :goto_0
    monitor-exit p0

    return-void

    .line 532
    :catch_0
    move-exception v0

    .line 535
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v1, "AntHalService"

    const-string v2, "ANT HAL Rx Message callback failure in application"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 526
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 540
    :cond_0
    :try_start_3
    const-string v1, "AntHalService"

    const-string v2, "JAnt callback called after service has been destroyed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized ANTStateChange(I)V
    .locals 3
    .param p1, "NewState"    # I

    .prologue
    .line 546
    monitor-enter p0

    :try_start_0
    const-string v0, "AntHalService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ANTStateChange callback to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    iget-object v0, p0, Lcom/dsi/ant/server/AntService$4;->this$0:Lcom/dsi/ant/server/AntService;

    # invokes: Lcom/dsi/ant/server/AntService;->setState(I)V
    invoke-static {v0, p1}, Lcom/dsi/ant/server/AntService;->access$900(Lcom/dsi/ant/server/AntService;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    monitor-exit p0

    return-void

    .line 546
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
