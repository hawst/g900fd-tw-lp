.class public Lcom/dsi/ant/server/AntService;
.super Landroid/app/Service;
.source "AntService.java"


# static fields
.field private static sAntHalServiceDestroy_LOCK:Ljava/lang/Object;


# instance fields
.field mCallback:Lcom/dsi/ant/server/IAntHalCallback;

.field private mChangeAntPowerState_LOCK:Ljava/lang/Object;

.field private final mHalBinder:Lcom/dsi/ant/server/IAntHal$Stub;

.field private mInitialized:Z

.field private mJAnt:Lcom/dsi/ant/core/JAntJava;

.field private mJAntCallback:Lcom/dsi/ant/core/JAntJava$ICallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dsi/ant/server/AntService;->sAntHalServiceDestroy_LOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/server/AntService;->mInitialized:Z

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/server/AntService;->mChangeAntPowerState_LOCK:Ljava/lang/Object;

    .line 375
    new-instance v0, Lcom/dsi/ant/server/AntService$3;

    invoke-direct {v0, p0}, Lcom/dsi/ant/server/AntService$3;-><init>(Lcom/dsi/ant/server/AntService;)V

    iput-object v0, p0, Lcom/dsi/ant/server/AntService;->mHalBinder:Lcom/dsi/ant/server/IAntHal$Stub;

    .line 522
    new-instance v0, Lcom/dsi/ant/server/AntService$4;

    invoke-direct {v0, p0}, Lcom/dsi/ant/server/AntService$4;-><init>(Lcom/dsi/ant/server/AntService;)V

    iput-object v0, p0, Lcom/dsi/ant/server/AntService;->mJAntCallback:Lcom/dsi/ant/core/JAntJava$ICallback;

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/server/AntService;I)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/server/AntService;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/dsi/ant/server/AntService;->doSetAntState(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/dsi/ant/server/AntService;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/server/AntService;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/dsi/ant/server/AntService;->doGetAntState()I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/dsi/ant/server/AntService;[B)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/server/AntService;
    .param p1, "x1"    # [B

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/dsi/ant/server/AntService;->doANTTxMessage([B)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/dsi/ant/server/AntService;Lcom/dsi/ant/server/IAntHalCallback;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/server/AntService;
    .param p1, "x1"    # Lcom/dsi/ant/server/IAntHalCallback;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/dsi/ant/server/AntService;->doRegisterAntHalCallback(Lcom/dsi/ant/server/IAntHalCallback;)I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/dsi/ant/server/AntService;Lcom/dsi/ant/server/IAntHalCallback;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/server/AntService;
    .param p1, "x1"    # Lcom/dsi/ant/server/IAntHalCallback;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/dsi/ant/server/AntService;->doUnregisterAntHalCallback(Lcom/dsi/ant/server/IAntHalCallback;)I

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/dsi/ant/server/AntService;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/server/AntService;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/dsi/ant/server/AntService;->doGetServiceLibraryVersionCode()I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/dsi/ant/server/AntService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/server/AntService;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/dsi/ant/server/AntService;->doGetServiceLibraryVersionName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/dsi/ant/server/AntService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/server/AntService;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/dsi/ant/server/AntService;->setState(I)V

    return-void
.end method

.method private asyncSetAntPowerState(Z)I
    .locals 7
    .param p1, "state"    # Z

    .prologue
    const/4 v6, 0x3

    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 144
    const/4 v1, -0x1

    .line 146
    .local v1, "result":I
    iget-object v4, p0, Lcom/dsi/ant/server/AntService;->mChangeAntPowerState_LOCK:Ljava/lang/Object;

    monitor-enter v4

    .line 148
    :try_start_0
    invoke-direct {p0}, Lcom/dsi/ant/server/AntService;->doGetAntState()I

    move-result v0

    .line 150
    .local v0, "currentState":I
    if-eqz p1, :cond_2

    .line 151
    const/4 v5, 0x2

    if-eq v5, v0, :cond_0

    if-ne v3, v0, :cond_1

    .line 153
    :cond_0
    const-string v2, "AntHalService"

    const-string v5, "Enable request ignored as already enabled/enabling"

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    monitor-exit v4

    move v2, v3

    .line 180
    :goto_0
    return v2

    .line 156
    :cond_1
    if-ne v6, v0, :cond_5

    .line 157
    const-string v3, "AntHalService"

    const-string v5, "Enable request ignored as already disabling"

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    monitor-exit v4

    goto :goto_0

    .line 178
    .end local v0    # "currentState":I
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 162
    .restart local v0    # "currentState":I
    :cond_2
    const/4 v5, 0x4

    if-eq v5, v0, :cond_3

    if-ne v6, v0, :cond_4

    .line 164
    :cond_3
    :try_start_1
    const-string v2, "AntHalService"

    const-string v5, "Disable request ignored as already disabled/disabling"

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    monitor-exit v4

    move v2, v3

    goto :goto_0

    .line 167
    :cond_4
    if-ne v3, v0, :cond_5

    .line 168
    const-string v3, "AntHalService"

    const-string v5, "Disable request ignored as already enabling"

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    monitor-exit v4

    goto :goto_0

    .line 173
    :cond_5
    if-eqz p1, :cond_6

    .line 174
    invoke-direct {p0}, Lcom/dsi/ant/server/AntService;->enableBlocking()I

    move-result v1

    .line 178
    :goto_1
    monitor-exit v4

    move v2, v1

    .line 180
    goto :goto_0

    .line 176
    :cond_6
    invoke-direct {p0}, Lcom/dsi/ant/server/AntService;->disableBlocking()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_1
.end method

.method private disableBlocking()I
    .locals 4

    .prologue
    .line 216
    const/4 v0, -0x1

    .line 217
    .local v0, "ret":I
    sget-object v2, Lcom/dsi/ant/server/AntService;->sAntHalServiceDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 219
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    if-eqz v1, :cond_0

    .line 221
    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    iget-object v3, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    invoke-virtual {v3}, Lcom/dsi/ant/core/JAntJava;->disable()Lcom/dsi/ant/core/JAntStatus;

    move-result-object v3

    if-ne v1, v3, :cond_1

    .line 223
    const-string v1, "AntHalService"

    const-string v3, "Disable callback end: Success"

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    const/4 v0, 0x1

    .line 231
    :cond_0
    :goto_0
    monitor-exit v2

    .line 232
    return v0

    .line 228
    :cond_1
    const-string v1, "AntHalService"

    const-string v3, "Disable callback end: Failure"

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 231
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private doANTTxMessage([B)I
    .locals 5
    .param p1, "message"    # [B

    .prologue
    .line 275
    const-string v2, "AntHalService"

    const-string v3, "ANT Tx Message start"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    if-nez p1, :cond_0

    .line 279
    const-string v2, "AntHalService"

    const-string v3, "ANTTxMessage invalid message: message is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    const/4 v0, -0x2

    .line 315
    :goto_0
    return v0

    .line 283
    :cond_0
    const/4 v0, -0x1

    .line 285
    .local v0, "result":I
    iget-object v2, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    invoke-virtual {v2, p1}, Lcom/dsi/ant/core/JAntJava;->ANTTxMessage([B)Lcom/dsi/ant/core/JAntStatus;

    move-result-object v1

    .line 287
    .local v1, "status":Lcom/dsi/ant/core/JAntStatus;
    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    if-ne v2, v1, :cond_2

    .line 289
    const-string v2, "AntHalService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mJAnt.ANTTxMessage returned status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/dsi/ant/core/JAntStatus;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    const/4 v0, 0x1

    .line 311
    :cond_1
    :goto_1
    const-string v2, "AntHalService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ANTTxMessage: Result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    const-string v2, "AntHalService"

    const-string v3, "ANT Tx Message end"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 295
    :cond_2
    const-string v2, "AntHalService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mJAnt.ANTTxMessage returned status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/dsi/ant/core/JAntStatus;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->FAILED_BT_NOT_INITIALIZED:Lcom/dsi/ant/core/JAntStatus;

    if-ne v2, v1, :cond_3

    .line 299
    const/4 v0, -0x3

    goto :goto_1

    .line 301
    :cond_3
    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->NOT_SUPPORTED:Lcom/dsi/ant/core/JAntStatus;

    if-ne v2, v1, :cond_4

    .line 303
    const/4 v0, -0x4

    goto :goto_1

    .line 305
    :cond_4
    sget-object v2, Lcom/dsi/ant/core/JAntStatus;->INVALID_PARM:Lcom/dsi/ant/core/JAntStatus;

    if-ne v2, v1, :cond_1

    .line 307
    const/4 v0, -0x2

    goto :goto_1
.end method

.method private doGetAntState()I
    .locals 4

    .prologue
    .line 125
    const-string v1, "AntHalService"

    const-string v2, "doGetAntState start"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v1, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    invoke-virtual {v1}, Lcom/dsi/ant/core/JAntJava;->getRadioEnabledStatus()I

    move-result v0

    .line 129
    .local v0, "retState":I
    const-string v1, "AntHalService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Get ANT State = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/dsi/ant/server/AntHalDefine;->getAntHalStateString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    return v0
.end method

.method private doGetServiceLibraryVersionCode()I
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x1

    return v0
.end method

.method private doGetServiceLibraryVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 349
    sget-object v0, Lcom/dsi/ant/server/Version;->ANT_HAL_LIBRARY_VERSION_NAME:Ljava/lang/String;

    return-object v0
.end method

.method private doHardReset()I
    .locals 4

    .prologue
    .line 354
    const/4 v0, -0x1

    .line 355
    .local v0, "ret":I
    sget-object v2, Lcom/dsi/ant/server/AntService;->sAntHalServiceDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 357
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    if-eqz v1, :cond_0

    .line 359
    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    iget-object v3, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    invoke-virtual {v3}, Lcom/dsi/ant/core/JAntJava;->hardReset()Lcom/dsi/ant/core/JAntStatus;

    move-result-object v3

    if-ne v1, v3, :cond_1

    .line 361
    const-string v1, "AntHalService"

    const-string v3, "Hard Reset end: Success"

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    const/4 v0, 0x1

    .line 369
    :cond_0
    :goto_0
    monitor-exit v2

    .line 370
    return v0

    .line 366
    :cond_1
    const-string v1, "AntHalService"

    const-string v3, "Hard Reset end: Failure"

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 369
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private doRegisterAntHalCallback(Lcom/dsi/ant/server/IAntHalCallback;)I
    .locals 3
    .param p1, "callback"    # Lcom/dsi/ant/server/IAntHalCallback;

    .prologue
    .line 320
    const-string v0, "AntHalService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Registering callback: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    iput-object p1, p0, Lcom/dsi/ant/server/AntService;->mCallback:Lcom/dsi/ant/server/IAntHalCallback;

    .line 324
    const/4 v0, 0x1

    return v0
.end method

.method private doSetAntState(I)I
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 93
    iget-object v2, p0, Lcom/dsi/ant/server/AntService;->mChangeAntPowerState_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 94
    const/4 v0, -0x2

    .line 96
    .local v0, "result":I
    sparse-switch p1, :sswitch_data_0

    .line 115
    :goto_0
    :try_start_0
    monitor-exit v2

    return v0

    .line 100
    :sswitch_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/dsi/ant/server/AntService;->asyncSetAntPowerState(Z)I

    move-result v0

    .line 101
    goto :goto_0

    .line 105
    :sswitch_1
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/dsi/ant/server/AntService;->asyncSetAntPowerState(Z)I

    move-result v0

    .line 106
    goto :goto_0

    .line 110
    :sswitch_2
    invoke-direct {p0}, Lcom/dsi/ant/server/AntService;->doHardReset()I

    move-result v0

    goto :goto_0

    .line 116
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 96
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x9 -> :sswitch_2
    .end sparse-switch
.end method

.method private doUnregisterAntHalCallback(Lcom/dsi/ant/server/IAntHalCallback;)I
    .locals 4
    .param p1, "callback"    # Lcom/dsi/ant/server/IAntHalCallback;

    .prologue
    .line 329
    const-string v1, "AntHalService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UNRegistering callback: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    const/4 v0, -0x1

    .line 333
    .local v0, "result":I
    iget-object v1, p0, Lcom/dsi/ant/server/AntService;->mCallback:Lcom/dsi/ant/server/IAntHalCallback;

    invoke-interface {v1}, Lcom/dsi/ant/server/IAntHalCallback;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-interface {p1}, Lcom/dsi/ant/server/IAntHalCallback;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 335
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/server/AntService;->mCallback:Lcom/dsi/ant/server/IAntHalCallback;

    .line 336
    const/4 v0, 0x1

    .line 339
    :cond_0
    return v0
.end method

.method private enableBlocking()I
    .locals 4

    .prologue
    .line 190
    const/4 v0, -0x1

    .line 191
    .local v0, "ret":I
    sget-object v2, Lcom/dsi/ant/server/AntService;->sAntHalServiceDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 193
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    if-eqz v1, :cond_0

    .line 195
    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    iget-object v3, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    invoke-virtual {v3}, Lcom/dsi/ant/core/JAntJava;->enable()Lcom/dsi/ant/core/JAntStatus;

    move-result-object v3

    if-ne v1, v3, :cond_1

    .line 197
    const-string v1, "AntHalService"

    const-string v3, "Enable call: Success"

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    const/4 v0, 0x1

    .line 205
    :cond_0
    :goto_0
    monitor-exit v2

    .line 206
    return v0

    .line 202
    :cond_1
    const-string v1, "AntHalService"

    const-string v3, "Enable call: Failure"

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 205
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private setState(I)V
    .locals 5
    .param p1, "state"    # I

    .prologue
    .line 64
    iget-object v2, p0, Lcom/dsi/ant/server/AntService;->mChangeAntPowerState_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 65
    :try_start_0
    const-string v1, "AntHalService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Setting ANT State = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/dsi/ant/server/AntHalDefine;->getAntHalStateString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    :try_start_1
    const-string v1, "AntHalService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Calling status changed callback "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dsi/ant/server/AntService;->mCallback:Lcom/dsi/ant/server/IAntHalCallback;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v1, p0, Lcom/dsi/ant/server/AntService;->mCallback:Lcom/dsi/ant/server/IAntHalCallback;

    invoke-interface {v1, p1}, Lcom/dsi/ant/server/IAntHalCallback;->antHalStateChanged(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83
    :goto_0
    :try_start_2
    monitor-exit v2

    .line 84
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AntHalService"

    const-string v3, "ANT HAL State Changed callback failure in application"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 83
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 79
    :catch_1
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/NullPointerException;
    :try_start_3
    const-string v1, "AntHalService"

    const-string v3, "Calling status changed callback is null"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public static startService(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dsi/ant/server/IAntHal;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 476
    const-string v1, "AntHalService"

    const-string v2, "onBind() entered"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    const/4 v0, 0x0

    .line 480
    .local v0, "binder":Landroid/os/IBinder;
    iget-boolean v1, p0, Lcom/dsi/ant/server/AntService;->mInitialized:Z

    if-eqz v1, :cond_0

    .line 482
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/dsi/ant/server/IAntHal;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 484
    const-string v1, "AntHalService"

    const-string v2, "Bind: IAntHal"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    iget-object v0, p0, Lcom/dsi/ant/server/AntService;->mHalBinder:Lcom/dsi/ant/server/IAntHal$Stub;

    .line 492
    :cond_0
    invoke-static {p0}, Lcom/dsi/ant/server/AntService;->startService(Landroid/content/Context;)Z

    .line 494
    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 419
    const-string v1, "AntHalService"

    const-string v2, "onCreate() entered"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 423
    iget-object v1, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    if-eqz v1, :cond_0

    .line 426
    const-string v1, "AntHalService"

    const-string v2, "LAST JAnt HCI Interface object not destroyed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    :cond_0
    new-instance v1, Lcom/dsi/ant/core/JAntJava;

    invoke-direct {v1}, Lcom/dsi/ant/core/JAntJava;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    .line 430
    iget-object v1, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    iget-object v2, p0, Lcom/dsi/ant/server/AntService;->mJAntCallback:Lcom/dsi/ant/core/JAntJava$ICallback;

    invoke-virtual {v1, v2, p0}, Lcom/dsi/ant/core/JAntJava;->create(Lcom/dsi/ant/core/JAntJava$ICallback;Landroid/content/Context;)Lcom/dsi/ant/core/JAntStatus;

    move-result-object v0

    .line 432
    .local v0, "createResult":Lcom/dsi/ant/core/JAntStatus;
    sget-object v1, Lcom/dsi/ant/core/JAntStatus;->SUCCESS:Lcom/dsi/ant/core/JAntStatus;

    if-ne v0, v1, :cond_1

    .line 434
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/server/AntService;->mInitialized:Z

    .line 436
    const-string v1, "AntHalService"

    const-string v2, "JAntJava create success"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :goto_0
    return-void

    .line 440
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dsi/ant/server/AntService;->mInitialized:Z

    .line 442
    const-string v1, "AntHalService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "JAntJava create failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    .line 449
    const-string v1, "AntHalService"

    const-string v2, "onDestroy() entered"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    :try_start_0
    sget-object v2, Lcom/dsi/ant/server/AntService;->sAntHalServiceDestroy_LOCK:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 455
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    if-eqz v1, :cond_0

    .line 457
    invoke-direct {p0}, Lcom/dsi/ant/server/AntService;->disableBlocking()I

    move-result v0

    .line 458
    .local v0, "result":I
    const-string v1, "AntHalService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDestroy: disable result is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/dsi/ant/server/AntHalDefine;->getAntHalResultString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    iget-object v1, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    invoke-virtual {v1}, Lcom/dsi/ant/core/JAntJava;->destroy()Lcom/dsi/ant/core/JAntStatus;

    .line 461
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/server/AntService;->mJAnt:Lcom/dsi/ant/core/JAntJava;

    .line 463
    .end local v0    # "result":I
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 465
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/dsi/ant/server/AntService;->mCallback:Lcom/dsi/ant/server/IAntHalCallback;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 469
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 471
    return-void

    .line 463
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 469
    :catchall_1
    move-exception v1

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    throw v1
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 510
    const-string v0, "AntHalService"

    const-string v1, "onStartCommand() entered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    iget-boolean v0, p0, Lcom/dsi/ant/server/AntService;->mInitialized:Z

    if-nez v0, :cond_0

    .line 514
    const-string v0, "AntHalService"

    const-string v1, "not initialized, stopping self"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    invoke-virtual {p0}, Lcom/dsi/ant/server/AntService;->stopSelf()V

    .line 517
    :cond_0
    const/4 v0, 0x2

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 500
    const-string v0, "AntHalService"

    const-string v1, "onUnbind() entered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/server/AntService;->mCallback:Lcom/dsi/ant/server/IAntHalCallback;

    .line 504
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
