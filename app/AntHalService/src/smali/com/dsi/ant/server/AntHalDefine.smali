.class public Lcom/dsi/ant/server/AntHalDefine;
.super Ljava/lang/Object;
.source "AntHalDefine.java"


# direct methods
.method public static getAntHalResultString(I)Ljava/lang/String;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 55
    .local v0, "string":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 76
    :pswitch_0
    const-string v0, "FAIL: UNKNOWN"

    .line 80
    :goto_0
    return-object v0

    .line 58
    :pswitch_1
    const-string v0, "SUCCESS"

    .line 59
    goto :goto_0

    .line 61
    :pswitch_2
    const-string v0, "FAIL: UNKNOWN"

    .line 62
    goto :goto_0

    .line 64
    :pswitch_3
    const-string v0, "FAIL: INVALID REQUEST"

    .line 65
    goto :goto_0

    .line 67
    :pswitch_4
    const-string v0, "FAIL: NOT ENABLED"

    .line 68
    goto :goto_0

    .line 70
    :pswitch_5
    const-string v0, "FAIL: NOT SUPPORTED"

    .line 71
    goto :goto_0

    .line 73
    :pswitch_6
    const-string v0, "FAIL: RESOURCE IN USE"

    .line 74
    goto :goto_0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch -0x5
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getAntHalStateString(I)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # I

    .prologue
    .line 124
    const/4 v0, 0x0

    .line 126
    .local v0, "string":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 159
    const-string v0, "STATE UNKNOWN"

    .line 163
    :goto_0
    return-object v0

    .line 129
    :pswitch_0
    const-string v0, "STATE UNKNOWN"

    .line 130
    goto :goto_0

    .line 132
    :pswitch_1
    const-string v0, "ENABLING"

    .line 133
    goto :goto_0

    .line 135
    :pswitch_2
    const-string v0, "ENABLED"

    .line 136
    goto :goto_0

    .line 138
    :pswitch_3
    const-string v0, "DISABLING"

    .line 139
    goto :goto_0

    .line 141
    :pswitch_4
    const-string v0, "DISABLED"

    .line 142
    goto :goto_0

    .line 144
    :pswitch_5
    const-string v0, "ANT NOT SUPPORTED"

    .line 145
    goto :goto_0

    .line 147
    :pswitch_6
    const-string v0, "ANT HAL SERVICE NOT INSTALLED"

    .line 148
    goto :goto_0

    .line 150
    :pswitch_7
    const-string v0, "ANT HAL SERVICE NOT CONNECTED"

    .line 151
    goto :goto_0

    .line 153
    :pswitch_8
    const-string v0, "RESETTING"

    .line 154
    goto :goto_0

    .line 156
    :pswitch_9
    const-string v0, "RESET"

    .line 157
    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
