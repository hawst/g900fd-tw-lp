.class public abstract Lcom/dsi/ant/server/IAntHal$Stub;
.super Landroid/os/Binder;
.source "IAntHal.java"

# interfaces
.implements Lcom/dsi/ant/server/IAntHal;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/server/IAntHal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 20
    const-string v0, "com.dsi.ant.server.IAntHal"

    invoke-virtual {p0, p0, v0}, Lcom/dsi/ant/server/IAntHal$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 43
    sparse-switch p1, :sswitch_data_0

    .line 115
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    .line 47
    :sswitch_0
    const-string v3, "com.dsi.ant.server.IAntHal"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 52
    :sswitch_1
    const-string v3, "com.dsi.ant.server.IAntHal"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 55
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/dsi/ant/server/IAntHal$Stub;->setAntState(I)I

    move-result v1

    .line 56
    .local v1, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 57
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 62
    .end local v0    # "_arg0":I
    .end local v1    # "_result":I
    :sswitch_2
    const-string v3, "com.dsi.ant.server.IAntHal"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p0}, Lcom/dsi/ant/server/IAntHal$Stub;->getAntState()I

    move-result v1

    .line 64
    .restart local v1    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 65
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 70
    .end local v1    # "_result":I
    :sswitch_3
    const-string v3, "com.dsi.ant.server.IAntHal"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 73
    .local v0, "_arg0":[B
    invoke-virtual {p0, v0}, Lcom/dsi/ant/server/IAntHal$Stub;->ANTTxMessage([B)I

    move-result v1

    .line 74
    .restart local v1    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 75
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 80
    .end local v0    # "_arg0":[B
    .end local v1    # "_result":I
    :sswitch_4
    const-string v3, "com.dsi.ant.server.IAntHal"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/dsi/ant/server/IAntHalCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/server/IAntHalCallback;

    move-result-object v0

    .line 83
    .local v0, "_arg0":Lcom/dsi/ant/server/IAntHalCallback;
    invoke-virtual {p0, v0}, Lcom/dsi/ant/server/IAntHal$Stub;->registerAntHalCallback(Lcom/dsi/ant/server/IAntHalCallback;)I

    move-result v1

    .line 84
    .restart local v1    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 85
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 90
    .end local v0    # "_arg0":Lcom/dsi/ant/server/IAntHalCallback;
    .end local v1    # "_result":I
    :sswitch_5
    const-string v3, "com.dsi.ant.server.IAntHal"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/dsi/ant/server/IAntHalCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/dsi/ant/server/IAntHalCallback;

    move-result-object v0

    .line 93
    .restart local v0    # "_arg0":Lcom/dsi/ant/server/IAntHalCallback;
    invoke-virtual {p0, v0}, Lcom/dsi/ant/server/IAntHal$Stub;->unregisterAntHalCallback(Lcom/dsi/ant/server/IAntHalCallback;)I

    move-result v1

    .line 94
    .restart local v1    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 95
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 100
    .end local v0    # "_arg0":Lcom/dsi/ant/server/IAntHalCallback;
    .end local v1    # "_result":I
    :sswitch_6
    const-string v3, "com.dsi.ant.server.IAntHal"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0}, Lcom/dsi/ant/server/IAntHal$Stub;->getServiceLibraryVersionCode()I

    move-result v1

    .line 102
    .restart local v1    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 103
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 108
    .end local v1    # "_result":I
    :sswitch_7
    const-string v3, "com.dsi.ant.server.IAntHal"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lcom/dsi/ant/server/IAntHal$Stub;->getServiceLibraryVersionName()Ljava/lang/String;

    move-result-object v1

    .line 110
    .local v1, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 111
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
