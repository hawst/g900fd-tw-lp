.class Lcom/dsi/ant/server/AntService$3;
.super Lcom/dsi/ant/server/IAntHal$Stub;
.source "AntService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/server/AntService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/server/AntService;


# direct methods
.method constructor <init>(Lcom/dsi/ant/server/AntService;)V
    .locals 0

    .prologue
    .line 376
    iput-object p1, p0, Lcom/dsi/ant/server/AntService$3;->this$0:Lcom/dsi/ant/server/AntService;

    invoke-direct {p0}, Lcom/dsi/ant/server/IAntHal$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public ANTTxMessage([B)I
    .locals 1
    .param p1, "message"    # [B

    .prologue
    .line 389
    iget-object v0, p0, Lcom/dsi/ant/server/AntService$3;->this$0:Lcom/dsi/ant/server/AntService;

    # invokes: Lcom/dsi/ant/server/AntService;->doANTTxMessage([B)I
    invoke-static {v0, p1}, Lcom/dsi/ant/server/AntService;->access$400(Lcom/dsi/ant/server/AntService;[B)I

    move-result v0

    return v0
.end method

.method public getAntState()I
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/dsi/ant/server/AntService$3;->this$0:Lcom/dsi/ant/server/AntService;

    # invokes: Lcom/dsi/ant/server/AntService;->doGetAntState()I
    invoke-static {v0}, Lcom/dsi/ant/server/AntService;->access$300(Lcom/dsi/ant/server/AntService;)I

    move-result v0

    return v0
.end method

.method public getServiceLibraryVersionCode()I
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/dsi/ant/server/AntService$3;->this$0:Lcom/dsi/ant/server/AntService;

    # invokes: Lcom/dsi/ant/server/AntService;->doGetServiceLibraryVersionCode()I
    invoke-static {v0}, Lcom/dsi/ant/server/AntService;->access$700(Lcom/dsi/ant/server/AntService;)I

    move-result v0

    return v0
.end method

.method public getServiceLibraryVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/dsi/ant/server/AntService$3;->this$0:Lcom/dsi/ant/server/AntService;

    # invokes: Lcom/dsi/ant/server/AntService;->doGetServiceLibraryVersionName()Ljava/lang/String;
    invoke-static {v0}, Lcom/dsi/ant/server/AntService;->access$800(Lcom/dsi/ant/server/AntService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public registerAntHalCallback(Lcom/dsi/ant/server/IAntHalCallback;)I
    .locals 1
    .param p1, "callback"    # Lcom/dsi/ant/server/IAntHalCallback;

    .prologue
    .line 395
    iget-object v0, p0, Lcom/dsi/ant/server/AntService$3;->this$0:Lcom/dsi/ant/server/AntService;

    # invokes: Lcom/dsi/ant/server/AntService;->doRegisterAntHalCallback(Lcom/dsi/ant/server/IAntHalCallback;)I
    invoke-static {v0, p1}, Lcom/dsi/ant/server/AntService;->access$500(Lcom/dsi/ant/server/AntService;Lcom/dsi/ant/server/IAntHalCallback;)I

    move-result v0

    return v0
.end method

.method public setAntState(I)I
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 379
    iget-object v0, p0, Lcom/dsi/ant/server/AntService$3;->this$0:Lcom/dsi/ant/server/AntService;

    # invokes: Lcom/dsi/ant/server/AntService;->doSetAntState(I)I
    invoke-static {v0, p1}, Lcom/dsi/ant/server/AntService;->access$200(Lcom/dsi/ant/server/AntService;I)I

    move-result v0

    return v0
.end method

.method public unregisterAntHalCallback(Lcom/dsi/ant/server/IAntHalCallback;)I
    .locals 1
    .param p1, "callback"    # Lcom/dsi/ant/server/IAntHalCallback;

    .prologue
    .line 400
    iget-object v0, p0, Lcom/dsi/ant/server/AntService$3;->this$0:Lcom/dsi/ant/server/AntService;

    # invokes: Lcom/dsi/ant/server/AntService;->doUnregisterAntHalCallback(Lcom/dsi/ant/server/IAntHalCallback;)I
    invoke-static {v0, p1}, Lcom/dsi/ant/server/AntService;->access$600(Lcom/dsi/ant/server/AntService;Lcom/dsi/ant/server/IAntHalCallback;)I

    move-result v0

    return v0
.end method
