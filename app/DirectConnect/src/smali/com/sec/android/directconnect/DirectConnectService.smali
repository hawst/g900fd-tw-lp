.class public Lcom/sec/android/directconnect/DirectConnectService;
.super Landroid/app/Service;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/directconnect/DirectConnectService$Clients;,
        Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    }
.end annotation


# instance fields
.field private cStartTime:J

.field private connTime:J

.field private final findPeers:Z

.field private isOwnerWfdOn:Z

.field private mCStateMachine:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

.field private mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private mCurrentSM:Lcom/android/internal/util/StateMachine;

.field private mManager:Landroid/net/wifi/p2p/WifiP2pManager;

.field private mNfcState:I

.field private mNotification:Landroid/app/Notification;

.field private mOStateMachine:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

.field private mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

.field private mServInfo:Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceInfo;

.field private mServReq:Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceRequest;

.field private mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

.field private ownerFoundT:J

.field private ownerMac:Ljava/lang/String;

.field private ownerName:Ljava/lang/String;

.field private final useServDisc:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 137
    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 138
    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 139
    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    .line 140
    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mServReq:Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceRequest;

    .line 141
    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mServInfo:Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceInfo;

    .line 144
    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mOStateMachine:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .line 145
    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCStateMachine:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .line 146
    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    .line 149
    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    .line 158
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->ownerName:Ljava/lang/String;

    .line 159
    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 160
    iput-boolean v4, p0, Lcom/sec/android/directconnect/DirectConnectService;->isOwnerWfdOn:Z

    .line 161
    iput-boolean v4, p0, Lcom/sec/android/directconnect/DirectConnectService;->useServDisc:Z

    .line 162
    iput-boolean v5, p0, Lcom/sec/android/directconnect/DirectConnectService;->findPeers:Z

    .line 167
    iput v5, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNfcState:I

    .line 170
    iput-wide v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->connTime:J

    .line 171
    iput-wide v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->cStartTime:J

    .line 172
    iput-wide v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->ownerFoundT:J

    .line 2833
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/directconnect/DirectConnectService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->getHotSpotState()I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/directconnect/DirectConnectService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->isWFDisplayEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->autoConnect()V

    return-void
.end method

.method static synthetic access$13400(Lcom/sec/android/directconnect/DirectConnectService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->ownerMac:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/directconnect/DirectConnectService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService;->stopSeparateUi(Z)V

    return-void
.end method

.method static synthetic access$16100(Lcom/sec/android/directconnect/DirectConnectService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->connTime:J

    return-wide v0
.end method

.method static synthetic access$16102(Lcom/sec/android/directconnect/DirectConnectService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;
    .param p1, "x1"    # J

    .prologue
    .line 51
    iput-wide p1, p0, Lcom/sec/android/directconnect/DirectConnectService;->connTime:J

    return-wide p1
.end method

.method static synthetic access$16200(Lcom/sec/android/directconnect/DirectConnectService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->cStartTime:J

    return-wide v0
.end method

.method static synthetic access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    return-object v0
.end method

.method static synthetic access$16800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pDevice;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    return-object v0
.end method

.method static synthetic access$16802(Lcom/sec/android/directconnect/DirectConnectService;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pDevice;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;
    .param p1, "x1"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    return-object p1
.end method

.method static synthetic access$16900(Lcom/sec/android/directconnect/DirectConnectService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService;->sendBroadcastIsGroupOwner(Z)V

    return-void
.end method

.method static synthetic access$17002(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService;->ownerName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$17100(Lcom/sec/android/directconnect/DirectConnectService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->isOwnerWfdOn:Z

    return v0
.end method

.method static synthetic access$17200(Lcom/sec/android/directconnect/DirectConnectService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->ownerFoundT:J

    return-wide v0
.end method

.method static synthetic access$17202(Lcom/sec/android/directconnect/DirectConnectService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;
    .param p1, "x1"    # J

    .prologue
    .line 51
    iput-wide p1, p0, Lcom/sec/android/directconnect/DirectConnectService;->ownerFoundT:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/sec/android/directconnect/DirectConnectService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->isWFDEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/directconnect/DirectConnectService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->isWFDConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->wFDInit()V

    return-void
.end method

.method static synthetic access$3400(Lcom/sec/android/directconnect/DirectConnectService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNfcState:I

    return v0
.end method

.method static synthetic access$3800(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->disableHotSpot()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->cleanup()V

    return-void
.end method

.method static synthetic access$4100(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService;->stopPopUp(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4700(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->disableWFDisplay()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->cleanup2()V

    return-void
.end method

.method static synthetic access$5700(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    return-object v0
.end method

.method static synthetic access$5800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->stopDiscovery()V

    return-void
.end method

.method static synthetic access$7300(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$7400(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->callRemoveGroup()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->startSeparateUi()V

    return-void
.end method

.method static synthetic access$8100(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->addService()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->stopSeparateUi()V

    return-void
.end method

.method static synthetic access$9500(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->discoverOwner()V

    return-void
.end method

.method private addService()V
    .locals 4

    .prologue
    .line 3070
    const-string v0, "DConnect"

    const-string v1, "_presence._tcp"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceInfo;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mServInfo:Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceInfo;

    .line 3071
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mServInfo:Landroid/net/wifi/p2p/nsd/WifiP2pDnsSdServiceInfo;

    new-instance v3, Lcom/sec/android/directconnect/DirectConnectService$11;

    invoke-direct {v3, p0}, Lcom/sec/android/directconnect/DirectConnectService$11;-><init>(Lcom/sec/android/directconnect/DirectConnectService;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->addLocalService(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/nsd/WifiP2pServiceInfo;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 3083
    return-void
.end method

.method private autoConnect()V
    .locals 3

    .prologue
    .line 2986
    const-string v0, "DirectConnectService"

    const-string v1, "autoConnect"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3032
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 3033
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/sec/android/directconnect/DirectConnectService$9;

    invoke-direct {v2, p0}, Lcom/sec/android/directconnect/DirectConnectService$9;-><init>(Lcom/sec/android/directconnect/DirectConnectService;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->requestP2pListen(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 3047
    const-string v0, "DirectConnectService"

    const-string v1, "calling requestNfcConnect"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3048
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/sec/android/directconnect/DirectConnectService$10;

    invoke-direct {v2, p0}, Lcom/sec/android/directconnect/DirectConnectService$10;-><init>(Lcom/sec/android/directconnect/DirectConnectService;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->requestNfcConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 3066
    :cond_0
    return-void
.end method

.method private callRemoveGroup()V
    .locals 3

    .prologue
    .line 2555
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 2556
    const-string v0, "DirectConnectService"

    const-string v1, "callRemoveGroup"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2557
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/sec/android/directconnect/DirectConnectService$1;

    invoke-direct {v2, p0}, Lcom/sec/android/directconnect/DirectConnectService$1;-><init>(Lcom/sec/android/directconnect/DirectConnectService;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 2570
    :cond_0
    return-void
.end method

.method private cleanup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2574
    const-string v0, "DirectConnectService"

    const-string v1, "cleanup"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2579
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    if-eqz v0, :cond_2

    .line 2580
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mOStateMachine:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    if-eqz v0, :cond_0

    .line 2581
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mOStateMachine:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->exit()V

    .line 2582
    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mOStateMachine:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .line 2584
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    .line 2586
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->setDialogListener(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$DialogListener;)V

    .line 2590
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCStateMachine:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    if-eqz v0, :cond_1

    .line 2591
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCStateMachine:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->exit()V

    .line 2592
    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCStateMachine:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .line 2594
    :cond_1
    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    .line 2597
    :cond_2
    const v0, 0xfeed

    invoke-direct {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService;->stopNotification(I)V

    .line 2598
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService;->stopForeground(Z)V

    .line 2599
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService;->stopSelf()V

    .line 2600
    return-void
.end method

.method private cleanup2()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2603
    const-string v0, "DirectConnectService"

    const-string v1, "cleanup2"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2604
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    if-eqz v0, :cond_2

    .line 2605
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mOStateMachine:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    if-eqz v0, :cond_0

    .line 2606
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mOStateMachine:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->exit()V

    .line 2607
    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mOStateMachine:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .line 2610
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCStateMachine:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    if-eqz v0, :cond_1

    .line 2611
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCStateMachine:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->exit()V

    .line 2612
    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCStateMachine:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .line 2614
    :cond_1
    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    .line 2616
    :cond_2
    const v0, 0xfeed

    invoke-direct {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService;->stopNotification(I)V

    .line 2617
    invoke-virtual {p0, v3}, Lcom/sec/android/directconnect/DirectConnectService;->stopForeground(Z)V

    .line 2619
    invoke-direct {p0, v3}, Lcom/sec/android/directconnect/DirectConnectService;->stopSeparateUi(Z)V

    .line 2620
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService;->stopSelf()V

    .line 2621
    return-void
.end method

.method private disableHotSpot()V
    .locals 5

    .prologue
    .line 3179
    const-string v2, "DirectConnectService"

    const-string v3, "disableWifiAp"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3180
    const-string v2, "wifi"

    invoke-virtual {p0, v2}, Lcom/sec/android/directconnect/DirectConnectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    .line 3182
    .local v1, "wifi":Landroid/net/wifi/WifiManager;
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3186
    :goto_0
    return-void

    .line 3183
    :catch_0
    move-exception v0

    .line 3184
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "DirectConnectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disableWifiAp: errmsg=["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private disableWFDirect()V
    .locals 3

    .prologue
    .line 2519
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v0, :cond_0

    .line 2520
    const-string v0, "wifip2p"

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 2521
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 2524
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_1

    .line 2525
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1}, Landroid/net/wifi/p2p/WifiP2pManager;->disableP2p(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)V

    .line 2527
    :cond_1
    return-void
.end method

.method private disableWFDisplay()V
    .locals 3

    .prologue
    .line 3198
    const-string v1, "DirectConnectService"

    const-string v2, "disableWFDisplay"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3199
    const-string v1, "display"

    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 3200
    .local v0, "mDisplayManager":Landroid/hardware/display/DisplayManager;
    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    .line 3201
    return-void
.end method

.method private discoverOwner()V
    .locals 2

    .prologue
    .line 2891
    const-string v0, "DirectConnectService"

    const-string v1, "discoverOwner"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2894
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->discoverPeers()V

    .line 2899
    return-void
.end method

.method private discoverPeers()V
    .locals 4

    .prologue
    .line 2867
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 2868
    const-string v0, "DirectConnectService"

    const-string v1, "discoverPeers"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2872
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/directconnect/DirectConnectService$4;

    invoke-direct {v3, p0}, Lcom/sec/android/directconnect/DirectConnectService$4;-><init>(Lcom/sec/android/directconnect/DirectConnectService;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->discoverPeersWithFlush(Landroid/net/wifi/p2p/WifiP2pManager$Channel;ILandroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 2888
    :cond_0
    return-void
.end method

.method private getHotSpotState()I
    .locals 7

    .prologue
    .line 3167
    const/16 v2, 0xb

    .line 3168
    .local v2, "state":I
    const-string v4, "wifi"

    invoke-virtual {p0, v4}, Lcom/sec/android/directconnect/DirectConnectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    .line 3170
    .local v3, "wifi":Landroid/net/wifi/WifiManager;
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "getWifiApState"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 3171
    .local v1, "method":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 3175
    .end local v1    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return v2

    .line 3172
    :catch_0
    move-exception v0

    .line 3173
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "DirectConnectService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getWifiApState: errmsg=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getVersion()Ljava/lang/String;
    .locals 5

    .prologue
    .line 3222
    const-string v1, ""

    .line 3225
    .local v1, "version":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v1, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3230
    :goto_0
    return-object v1

    .line 3226
    :catch_0
    move-exception v0

    .line 3227
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "tag"

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isWFDConnected()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 3152
    :try_start_0
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Lcom/sec/android/directconnect/DirectConnectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 3153
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/16 v4, 0xd

    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 3155
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v4, v5, :cond_0

    .line 3156
    const/4 v3, 0x1

    .line 3163
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "netInfo":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return v3

    .line 3160
    :catch_0
    move-exception v1

    .line 3161
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v4, "DirectConnectService"

    const-string v5, "isWFDConnected - NullPointerException"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isWFDEnabled()Z
    .locals 5

    .prologue
    .line 3137
    :try_start_0
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 3138
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/16 v3, 0xd

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 3140
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    .line 3141
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 3146
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "netInfo":Landroid/net/NetworkInfo;
    :goto_0
    return v3

    .line 3143
    :catch_0
    move-exception v1

    .line 3144
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v3, "DirectConnectService"

    const-string v4, "isWFDEnabled - NullPointerException"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3146
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private isWFDisplayEnabled()Z
    .locals 3

    .prologue
    .line 3190
    const-string v1, "display"

    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 3191
    .local v0, "mDisplayManager":Landroid/hardware/display/DisplayManager;
    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 3192
    const/4 v1, 0x1

    .line 3194
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private sendBroadcastIsGroupOwner(Z)V
    .locals 4
    .param p1, "IsGroupOwner"    # Z

    .prologue
    .line 3209
    const-string v2, "DirectConnectService"

    const-string v3, " sendBroadcastIsGroupOwner "

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3210
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.directconnect.DIRECT_CONNECT_IS_GROUP_OWNER"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3211
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "wifidirect_is_group_owner"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3212
    const-string v2, "com.sec.android.app.camera"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3213
    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService;->sendBroadcast(Landroid/content/Intent;)V

    .line 3215
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.directconnect.DIRECT_CONNECT_IS_GROUP_OWNER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3216
    .local v1, "intent2":Landroid/content/Intent;
    const-string v2, "wifidirect_is_group_owner"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3217
    const-string v2, "com.samsung.shareshot"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3218
    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->sendBroadcast(Landroid/content/Intent;)V

    .line 3219
    return-void
.end method

.method private startPopUp(Ljava/lang/String;)V
    .locals 4
    .param p1, "popupType"    # Ljava/lang/String;

    .prologue
    .line 3094
    const-string v1, "DirectConnectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startPopUp type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3095
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_POPUP_START_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3096
    .local v0, "popup_intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3097
    const-string v1, "POPUP_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3098
    const-string v1, "com.sec.android.directconnect"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3099
    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService;->startActivity(Landroid/content/Intent;)V

    .line 3100
    return-void
.end method

.method private startSeparateUi()V
    .locals 3

    .prologue
    .line 3112
    const-string v1, "DirectConnectService"

    const-string v2, "startSeparateUi"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3113
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_SEP_UI_START_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3114
    .local v0, "popup_intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3115
    const-string v1, "POPUP_TYPE"

    const-string v2, "separate"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3116
    const-string v1, "com.sec.android.directconnect"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3117
    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService;->startActivity(Landroid/content/Intent;)V

    .line 3118
    return-void
.end method

.method private stopDiscovery()V
    .locals 3

    .prologue
    .line 2904
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 2905
    const-string v0, "DirectConnectService"

    const-string v1, "stopDiscovery"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2906
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/sec/android/directconnect/DirectConnectService$5;

    invoke-direct {v2, p0}, Lcom/sec/android/directconnect/DirectConnectService$5;-><init>(Lcom/sec/android/directconnect/DirectConnectService;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->stopPeerDiscovery(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 2920
    :cond_0
    return-void
.end method

.method private stopNotification(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 3088
    const-string v1, "DirectConnectService"

    const-string v2, "stopNotification"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3089
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 3090
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 3091
    return-void
.end method

.method private stopPopUp(Ljava/lang/String;)V
    .locals 3
    .param p1, "popupType"    # Ljava/lang/String;

    .prologue
    .line 3103
    const-string v1, "DirectConnectService"

    const-string v2, "stopPopUp"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3104
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_POPUP_STOP_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3105
    .local v0, "popup_intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3106
    const-string v1, "POPUP_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3107
    const-string v1, "com.sec.android.directconnect"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3108
    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService;->startActivity(Landroid/content/Intent;)V

    .line 3109
    return-void
.end method

.method private stopSeparateUi()V
    .locals 1

    .prologue
    .line 3121
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService;->stopSeparateUi(Z)V

    .line 3122
    return-void
.end method

.method private stopSeparateUi(Z)V
    .locals 3
    .param p1, "stopActivity"    # Z

    .prologue
    .line 3125
    const-string v1, "DirectConnectService"

    const-string v2, "stopSeparateUi"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3126
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_SEP_UI_STOP_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3127
    .local v0, "popup_intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3128
    const-string v1, "POPUP_TYPE"

    const-string v2, "separate"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3129
    const-string v1, "STOP_ACTIVITY"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3130
    const-string v1, "com.sec.android.directconnect"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3131
    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService;->startActivity(Landroid/content/Intent;)V

    .line 3132
    return-void
.end method

.method private wFDInit()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2531
    const-string v1, "DirectConnectService"

    const-string v2, "wiFiDi_init"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2533
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2534
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2535
    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2536
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2537
    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2545
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    if-nez v1, :cond_0

    .line 2546
    const-string v1, "wifip2p"

    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 2547
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v1, p0, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 2548
    new-instance v1, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;-><init>(Lcom/sec/android/directconnect/DirectConnectService;Lcom/sec/android/directconnect/DirectConnectService$1;)V

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    .line 2549
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/directconnect/DirectConnectService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2551
    :cond_0
    return-void
.end method


# virtual methods
.method getMsgName(I)Ljava/lang/String;
    .locals 2
    .param p1, "msg"    # I

    .prologue
    .line 3249
    packed-switch p1, :pswitch_data_0

    .line 3356
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 3251
    :pswitch_1
    const-string v0, "START_O"

    goto :goto_0

    .line 3254
    :pswitch_2
    const-string v0, "START_C"

    goto :goto_0

    .line 3257
    :pswitch_3
    const-string v0, "MSG_WFD_DISABLED"

    goto :goto_0

    .line 3260
    :pswitch_4
    const-string v0, "MSG_WFD_ENABLED"

    goto :goto_0

    .line 3263
    :pswitch_5
    const-string v0, "MSG_WFD_DISCONNECTED"

    goto :goto_0

    .line 3266
    :pswitch_6
    const-string v0, "MSG_WFD_CONNECTED"

    goto :goto_0

    .line 3269
    :pswitch_7
    const-string v0, "MSG_WFD_REQ_TIMEOUT"

    goto :goto_0

    .line 3272
    :pswitch_8
    const-string v0, "MSG_WFD_FOUND_OWNER"

    goto :goto_0

    .line 3275
    :pswitch_9
    const-string v0, "MSG_WFD_FOUND_PEERS"

    goto :goto_0

    .line 3278
    :pswitch_a
    const-string v0, "MSG_HS_USER_ACTION"

    goto :goto_0

    .line 3281
    :pswitch_b
    const-string v0, "MSG_HS_DISABLE_TIMER"

    goto :goto_0

    .line 3284
    :pswitch_c
    const-string v0, "MSG_HS_DISABLED"

    goto :goto_0

    .line 3287
    :pswitch_d
    const-string v0, "MSG_WFDISPLAY_USER_ACTION"

    goto :goto_0

    .line 3290
    :pswitch_e
    const-string v0, "MSG_WFDISPLAY_DISABLED"

    goto :goto_0

    .line 3293
    :pswitch_f
    const-string v0, "MSG_CONNECT_TIMEOUT"

    goto :goto_0

    .line 3296
    :pswitch_10
    const-string v0, "MSG_REQ_NFC_TIMER"

    goto :goto_0

    .line 3299
    :pswitch_11
    const-string v0, "MSG_DISCOVER_OWNER_TIMEOUT"

    goto :goto_0

    .line 3302
    :pswitch_12
    const-string v0, "MSG_CANCEL"

    goto :goto_0

    .line 3305
    :pswitch_13
    const-string v0, "MSG_FINISH"

    goto :goto_0

    .line 3308
    :pswitch_14
    const-string v0, "MSG_CONNECT_FAIL"

    goto :goto_0

    .line 3311
    :pswitch_15
    const-string v0, "MSG_FINISH_CONNECTED"

    goto :goto_0

    .line 3314
    :pswitch_16
    const-string v0, "MSG_NEXT_REQ"

    goto :goto_0

    .line 3317
    :pswitch_17
    const-string v0, "MSG_CONNECTED_PEER"

    goto :goto_0

    .line 3320
    :pswitch_18
    const-string v0, "MSG_WFD_OWNER_INFO"

    goto :goto_0

    .line 3323
    :pswitch_19
    const-string v0, "MSG_CONNECT_RETRY"

    goto :goto_0

    .line 3326
    :pswitch_1a
    const-string v0, "MSG_CLIENT_INFO"

    goto :goto_0

    .line 3329
    :pswitch_1b
    const-string v0, "MSG_MAX_CLIENTS_REACHED"

    goto :goto_0

    .line 3332
    :pswitch_1c
    const-string v0, "MSG_CANCEL_CONNECT_USER_ACTION"

    goto :goto_0

    .line 3335
    :pswitch_1d
    const-string v0, "MSG_NFC_DISCONNECTED"

    goto :goto_0

    .line 3338
    :pswitch_1e
    const-string v0, "MSG_NFC_DISC_WAIT_TIMEOUT"

    goto :goto_0

    .line 3341
    :pswitch_1f
    const-string v0, "MSG_TCP_SERVER_CON_SUCCESS"

    goto :goto_0

    .line 3344
    :pswitch_20
    const-string v0, "MSG_TCP_CLIENT_FAIL"

    goto :goto_0

    .line 3347
    :pswitch_21
    const-string v0, "MSG_DISCOVER_OWNER_RETRY"

    goto :goto_0

    .line 3350
    :pswitch_22
    const-string v0, "MSG_SERVICE_DISCOVERY_TIMEOUT"

    goto :goto_0

    .line 3353
    :pswitch_23
    const-string v0, "MSG_WFD_DISABLE_TIMEOUT"

    goto :goto_0

    .line 3249
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
    .end packed-switch
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 177
    const-string v0, "DirectConnectService"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 184
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 187
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->getVersion()Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "version":Ljava/lang/String;
    const-string v1, "DirectConnectService"

    const-string v2, "++++++++++++++++++++++++++++++++++++++++"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    const-string v1, "DirectConnectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "+++    DirectConnect : VER=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]     +++"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const-string v1, "DirectConnectService"

    const-string v2, "+++    Update Date : 2013-09-24      +++"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    const-string v1, "DirectConnectService"

    const-string v2, "++++++++++++++++++++++++++++++++++++++++"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const-string v1, "DirectConnectService"

    const-string v2, " $$Entering onCreate"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 3235
    const-string v0, "DirectConnectService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3237
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    if-eqz v0, :cond_0

    .line 3238
    const-string v0, "DirectConnectService"

    const-string v1, "unregistering... "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3239
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 3240
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    .line 3243
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 3244
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const v9, 0xfeed

    const/16 v8, 0x3e9

    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 200
    if-eqz p1, :cond_1

    .line 201
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 202
    .local v0, "action":Ljava/lang/String;
    const-string v2, "DirectConnectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStartCommand: action= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    if-eqz v0, :cond_1

    .line 209
    const-string v2, "com.sec.android.directconnect.DIRECTCONNECT_C_START_ACTION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 213
    const-string v2, "wfdStatus"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 215
    const-string v2, "DirectConnectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStartCommand: wfd:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "wfdStatus"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iput-boolean v5, p0, Lcom/sec/android/directconnect/DirectConnectService;->isOwnerWfdOn:Z

    .line 223
    :cond_0
    const-string v2, "DirectConnectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WFD state:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->isWFDEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    if-eqz v2, :cond_2

    .line 229
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    invoke-virtual {v2, v6}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    .line 351
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    :goto_0
    return v5

    .line 233
    .restart local v0    # "action":Ljava/lang/String;
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->cStartTime:J

    .line 235
    const-string v2, "mac"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->ownerMac:Ljava/lang/String;

    .line 237
    const-string v2, "DirectConnectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStartCommand: mac:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/directconnect/DirectConnectService;->ownerMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService;->startCNotification()V

    .line 243
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    invoke-virtual {p0, v9, v2}, Lcom/sec/android/directconnect/DirectConnectService;->startForeground(ILandroid/app/Notification;)V

    .line 247
    new-instance v2, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    const-string v3, "DirectConnectService"

    invoke-direct {v2, p0, v3}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;-><init>(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCStateMachine:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .line 249
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCStateMachine:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    .line 251
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCStateMachine:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v2}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->start()V

    .line 253
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCStateMachine:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v2, v6}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessage(I)V

    goto :goto_0

    .line 259
    :cond_3
    const-string v2, "com.sec.android.directconnect.DIRECTCONNECT_O_START_ACTION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 263
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    if-eqz v2, :cond_4

    .line 265
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    invoke-virtual {v2, v5}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    goto :goto_0

    .line 269
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService;->startONotification()V

    .line 271
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    invoke-virtual {p0, v9, v2}, Lcom/sec/android/directconnect/DirectConnectService;->startForeground(ILandroid/app/Notification;)V

    .line 275
    new-instance v2, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    const-string v3, "DirectConnectService"

    invoke-direct {v2, p0, v3}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;-><init>(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mOStateMachine:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .line 277
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mOStateMachine:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    .line 279
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mOStateMachine:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-virtual {v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->start()V

    .line 281
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mOStateMachine:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-virtual {v2, v5}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessage(I)V

    goto :goto_0

    .line 287
    :cond_5
    const-string v2, "com.sec.android.directconnect.DIRECTCONNECT_POPUP_RESULT_HOTSPOT_ACTION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 289
    const-string v2, "result"

    invoke-virtual {p1, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 293
    .local v1, "result":I
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    if-eqz v2, :cond_1

    .line 295
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    const/16 v4, 0xa

    invoke-virtual {v3, v4, v1, v7}, Lcom/android/internal/util/StateMachine;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 301
    .end local v1    # "result":I
    :cond_6
    const-string v2, "com.sec.android.directconnect.DIRECTCONNECT_POPUP_RESULT_WFDISPLAY_ACTION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 303
    const-string v2, "result"

    invoke-virtual {p1, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 307
    .restart local v1    # "result":I
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    if-eqz v2, :cond_1

    .line 309
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    const/16 v4, 0xd

    invoke-virtual {v3, v4, v1, v7}, Lcom/android/internal/util/StateMachine;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 313
    .end local v1    # "result":I
    :cond_7
    const-string v2, "com.sec.android.directconnect.DIRECTCONNECT_POPUP_RESULT_CONNECTING_ACTION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 315
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    if-eqz v2, :cond_1

    .line 317
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    const/16 v3, 0x1d

    invoke-virtual {v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    goto/16 :goto_0

    .line 321
    :cond_8
    const-string v2, "com.sec.android.directconnect.DIRECTCONNECT_NFC_DISCONNECT_ACTION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 323
    iput v6, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNfcState:I

    .line 325
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    if-eqz v2, :cond_1

    .line 327
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    goto/16 :goto_0

    .line 331
    :cond_9
    const-string v2, "com.sec.android.directconnect.DIRECTCONNECT_NFC_CONNECT_ACTION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 333
    iput v5, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNfcState:I

    goto/16 :goto_0

    .line 335
    :cond_a
    const-string v2, "com.sec.android.directconnect.DIRECTCONNECT_DISABLE_WIFI_DIRECT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 337
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService;->disableWFDirect()V

    .line 339
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    if-eqz v2, :cond_b

    .line 341
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    goto/16 :goto_0

    .line 345
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService;->stopSelf()V

    goto/16 :goto_0
.end method

.method startCNotification()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f050002

    .line 2838
    const-string v1, "DirectConnectService"

    const-string v2, "startCNotification"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2841
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 2842
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    .line 2843
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    const v2, 0x7f02000b

    iput v2, v1, Landroid/app/Notification;->icon:I

    .line 2844
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    invoke-virtual {p0, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 2845
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    const/4 v2, -0x1

    iput v2, v1, Landroid/app/Notification;->defaults:I

    .line 2846
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    invoke-virtual {p0, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p0, v2, v4, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 2848
    const v1, 0xfeed

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 2849
    return-void
.end method

.method startONotification()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f050002

    .line 2852
    const-string v1, "DirectConnectService"

    const-string v2, "startONotification"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2855
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 2856
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    .line 2857
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    const v2, 0x7f02000b

    iput v2, v1, Landroid/app/Notification;->icon:I

    .line 2858
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    invoke-virtual {p0, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 2859
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    const/4 v2, -0x1

    iput v2, v1, Landroid/app/Notification;->defaults:I

    .line 2860
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    invoke-virtual {p0, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p0, v2, v4, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 2862
    const v1, 0xfeed

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 2863
    return-void
.end method
