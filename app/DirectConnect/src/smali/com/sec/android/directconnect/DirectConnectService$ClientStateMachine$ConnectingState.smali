.class Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConnectingState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V
    .locals 0

    .prologue
    .line 2347
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 5

    .prologue
    const/16 v4, 0xf

    .line 2351
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2352
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V
    invoke-static {v0, v4}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$16000(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V

    .line 2354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v4, v2, v3}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessageDelayed(IJ)V

    .line 2355
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 9
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x1

    const/16 v8, 0x14

    .line 2359
    const-string v1, "DirectConnectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handling message "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v3, v3, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2361
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 2406
    const/4 v0, 0x0

    .line 2409
    :goto_0
    :sswitch_0
    return v0

    .line 2364
    :sswitch_1
    const-string v1, "DirectConnectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Connection time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v3, v3, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->connTime:J
    invoke-static {v3}, Lcom/sec/android/directconnect/DirectConnectService;->access$16100(Lcom/sec/android/directconnect/DirectConnectService;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2365
    const-string v1, "DirectConnectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Total time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v3, v3, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->cStartTime:J
    invoke-static {v3}, Lcom/sec/android/directconnect/DirectConnectService;->access$16200(Lcom/sec/android/directconnect/DirectConnectService;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2367
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-ne v1, v0, :cond_0

    .line 2368
    const-string v1, "DirectConnectService"

    const-string v2, "this should be client but owner, disconnect"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2370
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v2, "connecterror"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 2374
    :goto_1
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    const/16 v2, 0xf

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V
    invoke-static {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$16300(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V

    .line 2375
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    const/16 v2, 0x1a

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V
    invoke-static {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$16400(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V

    .line 2376
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v1, v8}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessage(I)V

    goto :goto_0

    .line 2372
    :cond_0
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v2, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopPopUp(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$4100(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    goto :goto_1

    .line 2381
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v2, "connecterror"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 2382
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->WFDCancelConnect()V
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$16500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    .line 2383
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v1, v8}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessage(I)V

    goto/16 :goto_0

    .line 2393
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->WFDCancelConnect()V
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$16500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    .line 2394
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v2, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopPopUp(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$4100(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 2396
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v1, v8}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessage(I)V

    goto/16 :goto_0

    .line 2401
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->WFDCancelConnect()V
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$16500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    .line 2402
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v1, v8}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessage(I)V

    goto/16 :goto_0

    .line 2361
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0xf -> :sswitch_2
        0x13 -> :sswitch_3
        0x15 -> :sswitch_2
        0x1a -> :sswitch_0
        0x1d -> :sswitch_4
    .end sparse-switch
.end method
