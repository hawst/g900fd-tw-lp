.class Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;
.super Ljava/lang/Thread;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TCPClientThread"
.end annotation


# instance fields
.field clientSock:Ljava/net/Socket;

.field serverAddr:Ljava/net/SocketAddress;

.field sm:Lcom/android/internal/util/StateMachine;

.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/StateMachine;)V
    .locals 3
    .param p2, "s"    # Lcom/android/internal/util/StateMachine;

    .prologue
    .line 2468
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2464
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->clientSock:Ljava/net/Socket;

    .line 2465
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mOIPAddress:Ljava/net/InetAddress;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$13300(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Ljava/net/InetAddress;

    move-result-object v1

    const v2, 0xc351

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->serverAddr:Ljava/net/SocketAddress;

    .line 2469
    iput-object p2, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->sm:Lcom/android/internal/util/StateMachine;

    .line 2470
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 2474
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 2476
    const-string v1, "DirectConnectService"

    const-string v2, "Start TCP client"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2480
    :try_start_0
    const-string v1, "DirectConnectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TCP connecting to owner "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->serverAddr:Ljava/net/SocketAddress;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2482
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->clientSock:Ljava/net/Socket;

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->serverAddr:Ljava/net/SocketAddress;

    const/16 v3, 0xfa0

    invoke-virtual {v1, v2, v3}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2499
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->clientSock:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2500
    const-string v1, "DirectConnectService"

    const-string v2, "Connected!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2501
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->sm:Lcom/android/internal/util/StateMachine;

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    .line 2504
    :cond_0
    const-string v1, "DirectConnectService"

    const-string v2, "Closing Client Connection Socket"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2505
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->clientSock:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2512
    :goto_0
    return-void

    .line 2508
    :catch_0
    move-exception v0

    .line 2509
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "DirectConnectService"

    const-string v2, "Could not close client socket"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2485
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 2486
    .local v0, "e":Ljava/net/SocketTimeoutException;
    :try_start_2
    const-string v1, "DirectConnectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "client socket timeout, local Addr:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->clientSock:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2488
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->sm:Lcom/android/internal/util/StateMachine;

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2499
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->clientSock:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2500
    const-string v1, "DirectConnectService"

    const-string v2, "Connected!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2501
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->sm:Lcom/android/internal/util/StateMachine;

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    .line 2504
    :cond_1
    const-string v1, "DirectConnectService"

    const-string v2, "Closing Client Connection Socket"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2505
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->clientSock:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 2508
    :catch_2
    move-exception v0

    .line 2509
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "DirectConnectService"

    const-string v2, "Could not close client socket"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2492
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 2493
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_4
    const-string v1, "DirectConnectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "client socket IO exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2494
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->sm:Lcom/android/internal/util/StateMachine;

    const/16 v2, 0x21

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Lcom/android/internal/util/StateMachine;->sendMessageDelayed(IJ)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2499
    :try_start_5
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->clientSock:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2500
    const-string v1, "DirectConnectService"

    const-string v2, "Connected!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2501
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->sm:Lcom/android/internal/util/StateMachine;

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    .line 2504
    :cond_2
    const-string v1, "DirectConnectService"

    const-string v2, "Closing Client Connection Socket"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2505
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->clientSock:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 2508
    :catch_4
    move-exception v0

    .line 2509
    const-string v1, "DirectConnectService"

    const-string v2, "Could not close client socket"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2498
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    .line 2499
    :try_start_6
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->clientSock:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2500
    const-string v2, "DirectConnectService"

    const-string v3, "Connected!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2501
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->sm:Lcom/android/internal/util/StateMachine;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    .line 2504
    :cond_3
    const-string v2, "DirectConnectService"

    const-string v3, "Closing Client Connection Socket"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2505
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->clientSock:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 2510
    :goto_1
    throw v1

    .line 2508
    :catch_5
    move-exception v0

    .line 2509
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v2, "DirectConnectService"

    const-string v3, "Could not close client socket"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
