.class Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
.super Lcom/android/internal/util/StateMachine;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClientStateMachine"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcDisConState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcConState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$RemovingGrpState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitWFDisplayResultState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledWFDisplayState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitHSResultState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscConState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscWFDisplayState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscHSState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledState;,
        Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;
    }
.end annotation


# instance fields
.field private final mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

.field private final mConnectedState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;

.field private final mConnectingState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;

.field private final mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;

.field private final mDisablingHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;

.field private final mDisablingWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;

.field private final mDiscoveringOwnerState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;

.field private final mEnabledHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;

.field private final mEnabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledState;

.field private final mEnabledWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledWFDisplayState;

.field private final mEnablingNfcConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcConState;

.field private final mEnablingNfcDisConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcDisConState;

.field private mOIPAddress:Ljava/net/InetAddress;

.field private final mRemovingGrpState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$RemovingGrpState;

.field private final mTCPClientState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;

.field private mTCPClientThread:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;

.field private final mWaitHSResultState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitHSResultState;

.field private final mWaitNfcDiscConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscConState;

.field private final mWaitNfcDiscEnabledSepUiState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;

.field private final mWaitNfcDiscEnabledState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledState;

.field private final mWaitNfcDiscHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscHSState;

.field private final mWaitNfcDiscWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscWFDisplayState;

.field private final mWaitWFDisplayResultState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitWFDisplayResultState;

.field final synthetic this$0:Lcom/sec/android/directconnect/DirectConnectService;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V
    .locals 2
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 1481
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    .line 1482
    invoke-direct {p0, p2}, Lcom/android/internal/util/StateMachine;-><init>(Ljava/lang/String;)V

    .line 1450
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    .line 1452
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;

    .line 1453
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcConState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcConState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnablingNfcConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcConState;

    .line 1454
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcDisConState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcDisConState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnablingNfcDisConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcDisConState;

    .line 1455
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledState;

    .line 1456
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDiscoveringOwnerState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;

    .line 1457
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mConnectingState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;

    .line 1459
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnabledHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;

    .line 1460
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitHSResultState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitHSResultState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitHSResultState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitHSResultState;

    .line 1461
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDisablingHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;

    .line 1463
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledWFDisplayState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledWFDisplayState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnabledWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledWFDisplayState;

    .line 1464
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitWFDisplayResultState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitWFDisplayResultState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitWFDisplayResultState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitWFDisplayResultState;

    .line 1465
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDisablingWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;

    .line 1467
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$RemovingGrpState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$RemovingGrpState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mRemovingGrpState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$RemovingGrpState;

    .line 1468
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mConnectedState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;

    .line 1470
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscEnabledState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledState;

    .line 1471
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscEnabledSepUiState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;

    .line 1472
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscHSState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscHSState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscHSState;

    .line 1473
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscWFDisplayState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscWFDisplayState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscWFDisplayState;

    .line 1474
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscConState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscConState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscConState;

    .line 1476
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mTCPClientState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;

    .line 1478
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mTCPClientThread:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;

    .line 1484
    const-string v0, "DirectConnectService"

    const-string v1, "Creating State Machine"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1485
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1486
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnablingNfcConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcConState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1487
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnablingNfcDisConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcDisConState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1488
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1489
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDiscoveringOwnerState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1490
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mConnectingState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1491
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnabledHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1492
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitHSResultState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitHSResultState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1493
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDisablingHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1494
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnabledWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledWFDisplayState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1495
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitWFDisplayResultState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitWFDisplayResultState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1496
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDisablingWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1497
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mRemovingGrpState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$RemovingGrpState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1498
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mConnectedState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1499
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscEnabledState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1500
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscEnabledSepUiState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1501
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscHSState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1502
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscWFDisplayState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1503
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscConState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1504
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mTCPClientState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 1507
    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->getHotSpotState()I
    invoke-static {p1}, Lcom/sec/android/directconnect/DirectConnectService;->access$000(Lcom/sec/android/directconnect/DirectConnectService;)I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    .line 1508
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnabledHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    .line 1523
    :goto_0
    return-void

    .line 1509
    :cond_0
    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->isWFDisplayEnabled()Z
    invoke-static {p1}, Lcom/sec/android/directconnect/DirectConnectService;->access$100(Lcom/sec/android/directconnect/DirectConnectService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1510
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnabledWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledWFDisplayState;

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    goto :goto_0

    .line 1512
    :cond_1
    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->isWFDEnabled()Z
    invoke-static {p1}, Lcom/sec/android/directconnect/DirectConnectService;->access$200(Lcom/sec/android/directconnect/DirectConnectService;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1513
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    goto :goto_0

    .line 1515
    :cond_2
    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->isWFDEnabled()Z
    invoke-static {p1}, Lcom/sec/android/directconnect/DirectConnectService;->access$200(Lcom/sec/android/directconnect/DirectConnectService;)Z

    move-result v0

    if-eqz v0, :cond_3

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->isWFDConnected()Z
    invoke-static {p1}, Lcom/sec/android/directconnect/DirectConnectService;->access$300(Lcom/sec/android/directconnect/DirectConnectService;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1516
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledState;

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    goto :goto_0

    .line 1520
    :cond_3
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mConnectedState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    goto :goto_0
.end method

.method private WFDCancelConnect()V
    .locals 3

    .prologue
    .line 2415
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$5800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$5700(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2416
    const-string v0, "DirectConnectService"

    const-string v1, "cancelConnect"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2417
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$5800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$5700(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    new-instance v2, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$1;

    invoke-direct {v2, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$1;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->cancelConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 2430
    :cond_0
    return-void
.end method

.method private WFDConnect()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2434
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$5800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$5700(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2435
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pConfig;

    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pConfig;-><init>()V

    .line 2436
    .local v0, "config":Landroid/net/wifi/p2p/WifiP2pConfig;
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->ownerMac:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$13400(Lcom/sec/android/directconnect/DirectConnectService;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    .line 2437
    iput v2, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    .line 2438
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iput v2, v1, Landroid/net/wifi/WpsInfo;->setup:I

    .line 2439
    const-string v1, "DirectConnectService"

    const-string v2, "connecting to owner"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2440
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/sec/android/directconnect/DirectConnectService;->connTime:J
    invoke-static {v1, v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->access$16102(Lcom/sec/android/directconnect/DirectConnectService;J)J

    .line 2441
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$5800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$5700(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v2

    new-instance v3, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$2;

    invoke-direct {v3, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$2;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pConfig;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 2455
    .end local v0    # "config":Landroid/net/wifi/p2p/WifiP2pConfig;
    :cond_0
    return-void
.end method

.method static synthetic access$10000(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$10100(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$10200(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$10300(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitHSResultState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitHSResultState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitHSResultState;

    return-object v0
.end method

.method static synthetic access$10400(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$10500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$10600(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$10700(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitWFDisplayResultState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitWFDisplayResultState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitWFDisplayResultState;

    return-object v0
.end method

.method static synthetic access$10800(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$10900(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$11000(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$11100(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$11200(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mConnectedState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;

    return-object v0
.end method

.method static synthetic access$11300(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$11400(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$11500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$11600(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscHSState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscHSState;

    return-object v0
.end method

.method static synthetic access$11700(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$11800(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDisablingHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;

    return-object v0
.end method

.method static synthetic access$11900(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$12000(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;

    return-object v0
.end method

.method static synthetic access$12100(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$12200(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$12300(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscWFDisplayState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscWFDisplayState;

    return-object v0
.end method

.method static synthetic access$12400(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$12500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDisablingWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;

    return-object v0
.end method

.method static synthetic access$12600(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$12700(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$12800(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledState;

    return-object v0
.end method

.method static synthetic access$12900(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$13000(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$13100(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscConState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscConState;

    return-object v0
.end method

.method static synthetic access$13200(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$13300(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Ljava/net/InetAddress;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mOIPAddress:Ljava/net/InetAddress;

    return-object v0
.end method

.method static synthetic access$13302(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Ljava/net/InetAddress;)Ljava/net/InetAddress;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Ljava/net/InetAddress;

    .prologue
    .line 1448
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mOIPAddress:Ljava/net/InetAddress;

    return-object p1
.end method

.method static synthetic access$13500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mTCPClientState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;

    return-object v0
.end method

.method static synthetic access$13600(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$13700(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$RemovingGrpState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mRemovingGrpState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$RemovingGrpState;

    return-object v0
.end method

.method static synthetic access$13800(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$13900(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$14000(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$14100(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscEnabledState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledState;

    return-object v0
.end method

.method static synthetic access$14200(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$14300(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcDisConState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnablingNfcDisConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcDisConState;

    return-object v0
.end method

.method static synthetic access$14400(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$14500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcConState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnablingNfcConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnablingNfcConState;

    return-object v0
.end method

.method static synthetic access$14600(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$14700(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$14800(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscEnabledSepUiState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;

    return-object v0
.end method

.method static synthetic access$14900(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$15000(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$15100(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$15200(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$15300(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$15400(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$15500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$15600(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->WFDConnect()V

    return-void
.end method

.method static synthetic access$15700(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mConnectingState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;

    return-object v0
.end method

.method static synthetic access$15800(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$15900(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$16000(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$16300(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$16400(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$16500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    invoke-direct {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->WFDCancelConnect()V

    return-void
.end method

.method static synthetic access$9300(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$9400(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$9600(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDiscoveringOwnerState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;

    return-object v0
.end method

.method static synthetic access$9700(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$9800(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$9900(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 1448
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V

    return-void
.end method


# virtual methods
.method public exit()V
    .locals 0

    .prologue
    .line 1526
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->quit()V

    .line 1527
    return-void
.end method

.method startTCPClient()V
    .locals 1

    .prologue
    .line 2458
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;-><init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/StateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mTCPClientThread:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;

    .line 2459
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mTCPClientThread:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientThread;->start()V

    .line 2460
    return-void
.end method
