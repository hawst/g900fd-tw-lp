.class public Lcom/sec/android/directconnect/DirectConnectPopUp;
.super Landroid/app/Activity;
.source "DirectConnectPopUp.java"


# instance fields
.field private mAlert_Message:Ljava/lang/String;

.field private mCount_down:I

.field mPop_up_Handler:Landroid/os/Handler;

.field private mPopup:Landroid/app/AlertDialog$Builder;

.field private mPopup_type:Ljava/lang/String;

.field private m_PopUp:Landroid/app/AlertDialog;

.field private pairing_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

.field private pairing_progressbar_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

.field private test:I

.field private textView_Message:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    .line 38
    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup:Landroid/app/AlertDialog$Builder;

    .line 39
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mCount_down:I

    .line 42
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->test:I

    .line 239
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectPopUp$7;-><init>(Lcom/sec/android/directconnect/DirectConnectPopUp;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPop_up_Handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/directconnect/DirectConnectPopUp;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectPopUp;
    .param p1, "x1"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/directconnect/DirectConnectPopUp;->sendAction(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/directconnect/DirectConnectPopUp;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectPopUp;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mCount_down:I

    return v0
.end method

.method static synthetic access$110(Lcom/sec/android/directconnect/DirectConnectPopUp;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectPopUp;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mCount_down:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mCount_down:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/directconnect/DirectConnectPopUp;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectPopUp;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/directconnect/DirectConnectPopUp;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectPopUp;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->textView_Message:Landroid/widget/TextView;

    return-object v0
.end method

.method private sendAction(I)V
    .locals 4
    .param p1, "result"    # I

    .prologue
    .line 273
    const-string v1, "wfdisplay"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 274
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_POPUP_RESULT_WFDISPLAY_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 275
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.directconnect"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 276
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 277
    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 291
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    const-string v1, "hotspot"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 279
    const-string v1, "DirectConnectPopUp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendAction result:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_POPUP_RESULT_HOTSPOT_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 281
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.directconnect"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 283
    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 284
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    const-string v1, "connecting"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 285
    const-string v1, "DirectConnectPopUp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendAction result:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_POPUP_RESULT_CONNECTING_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 287
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.directconnect"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 288
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 289
    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private setAnimationDrawable(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/LinearLayout;
    .locals 9
    .param p1, "Check"    # Ljava/lang/String;
    .param p2, "meg"    # Ljava/lang/String;

    .prologue
    const v8, 0x7f070003

    const/high16 v7, 0x7f070000

    const/high16 v6, 0x7f030000

    const/4 v5, 0x0

    .line 294
    const-string v4, "connecting"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 296
    invoke-static {p0, v6, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 297
    .local v0, "linear":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 300
    .local v3, "top_ImageView":Landroid/widget/ImageView;
    const/high16 v4, 0x7f040000

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 301
    invoke-virtual {v3}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v4, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 302
    iget-object v4, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 304
    const v4, 0x7f070002

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 305
    .local v2, "paring_progressbar":Landroid/widget/ImageView;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 306
    const v4, 0x7f040001

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 307
    invoke-virtual {v2}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v4, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_progressbar_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 308
    iget-object v4, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_progressbar_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    move-object v1, v0

    .line 330
    .end local v0    # "linear":Landroid/widget/LinearLayout;
    .end local v2    # "paring_progressbar":Landroid/widget/ImageView;
    .end local v3    # "top_ImageView":Landroid/widget/ImageView;
    .local v1, "linear":Landroid/widget/LinearLayout;
    :goto_0
    return-object v1

    .line 312
    .end local v1    # "linear":Landroid/widget/LinearLayout;
    :cond_0
    const-string v4, "separate"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 314
    invoke-static {p0, v6, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 315
    .restart local v0    # "linear":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 318
    .restart local v3    # "top_ImageView":Landroid/widget/ImageView;
    const v4, 0x7f040002

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 319
    invoke-virtual {v3}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v4, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 320
    iget-object v4, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    move-object v1, v0

    .line 322
    .end local v0    # "linear":Landroid/widget/LinearLayout;
    .restart local v1    # "linear":Landroid/widget/LinearLayout;
    goto :goto_0

    .line 326
    .end local v1    # "linear":Landroid/widget/LinearLayout;
    .end local v3    # "top_ImageView":Landroid/widget/ImageView;
    :cond_1
    const v4, 0x7f030002

    invoke-static {p0, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 327
    .restart local v0    # "linear":Landroid/widget/LinearLayout;
    const v4, 0x7f070006

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->textView_Message:Landroid/widget/TextView;

    .line 328
    iget-object v4, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->textView_Message:Landroid/widget/TextView;

    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v1, v0

    .line 330
    .end local v0    # "linear":Landroid/widget/LinearLayout;
    .restart local v1    # "linear":Landroid/widget/LinearLayout;
    goto :goto_0
.end method

.method private startPopUp(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const v7, 0x7f050007

    const v4, 0x7f050005

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 66
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    .line 68
    .local v0, "oldPopupType":Ljava/lang/String;
    const-string v1, "POPUP_TYPE"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    .line 69
    const-string v1, "DirectConnectPopUp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " mPopup_type  = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", old:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->finish()V

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-ne v1, v5, :cond_2

    .line 75
    const-string v1, "DirectConnectPopUp"

    const-string v2, "pop up already shown"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 79
    :cond_2
    const-string v1, "wfdisplay"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 80
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f050003

    invoke-virtual {p0, v2}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v4}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mCount_down:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mAlert_Message:Ljava/lang/String;

    .line 97
    :goto_1
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-ne v1, v5, :cond_3

    .line 98
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 101
    :cond_3
    const v1, 0x103012e

    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectPopUp;->setTheme(I)V

    .line 102
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup:Landroid/app/AlertDialog$Builder;

    .line 103
    const v1, 0x103000f

    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectPopUp;->setTheme(I)V

    .line 105
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f050006

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 106
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup:Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mAlert_Message:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/directconnect/DirectConnectPopUp;->setAnimationDrawable(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 108
    const-string v1, "wfdisplay"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "hotspot"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 109
    :cond_4
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup:Landroid/app/AlertDialog$Builder;

    const/high16 v2, 0x1040000

    new-instance v3, Lcom/sec/android/directconnect/DirectConnectPopUp$1;

    invoke-direct {v3, p0}, Lcom/sec/android/directconnect/DirectConnectPopUp$1;-><init>(Lcom/sec/android/directconnect/DirectConnectPopUp;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 116
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup:Landroid/app/AlertDialog$Builder;

    const v2, 0x104000a

    new-instance v3, Lcom/sec/android/directconnect/DirectConnectPopUp$2;

    invoke-direct {v3, p0}, Lcom/sec/android/directconnect/DirectConnectPopUp$2;-><init>(Lcom/sec/android/directconnect/DirectConnectPopUp;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 147
    :cond_5
    :goto_2
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    .line 148
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 149
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    new-instance v2, Lcom/sec/android/directconnect/DirectConnectPopUp$6;

    invoke-direct {v2, p0}, Lcom/sec/android/directconnect/DirectConnectPopUp$6;-><init>(Lcom/sec/android/directconnect/DirectConnectPopUp;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 161
    :try_start_0
    const-string v1, "DirectConnectPopUp"

    const-string v2, " PopUp show "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 166
    const-string v1, "wfdisplay"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 167
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPop_up_Handler:Landroid/os/Handler;

    const/4 v2, 0x1

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 172
    :catch_0
    move-exception v1

    goto/16 :goto_0

    .line 81
    :cond_6
    const-string v1, "hotspot"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 82
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f050004

    invoke-virtual {p0, v2}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v4}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mCount_down:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mAlert_Message:Ljava/lang/String;

    goto/16 :goto_1

    .line 83
    :cond_7
    const-string v1, "connecterror"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 84
    const v1, 0x7f050009

    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mAlert_Message:Ljava/lang/String;

    goto/16 :goto_1

    .line 85
    :cond_8
    const-string v1, "busy"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 86
    const v1, 0x7f05000a

    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mAlert_Message:Ljava/lang/String;

    goto/16 :goto_1

    .line 87
    :cond_9
    const-string v1, "connecting"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 88
    const v1, 0x7f05000b

    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mAlert_Message:Ljava/lang/String;

    goto/16 :goto_1

    .line 89
    :cond_a
    const-string v1, "separate"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 90
    const v1, 0x7f05000c

    invoke-virtual {p0, v1}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mAlert_Message:Ljava/lang/String;

    goto/16 :goto_1

    .line 92
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->finish()V

    goto/16 :goto_0

    .line 123
    :cond_c
    const-string v1, "connecterror"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, "busy"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 124
    :cond_d
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup:Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/sec/android/directconnect/DirectConnectPopUp$3;

    invoke-direct {v2, p0}, Lcom/sec/android/directconnect/DirectConnectPopUp$3;-><init>(Lcom/sec/android/directconnect/DirectConnectPopUp;)V

    invoke-virtual {v1, v7, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_2

    .line 130
    :cond_e
    const-string v1, "connecting"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 131
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f050008

    new-instance v3, Lcom/sec/android/directconnect/DirectConnectPopUp$4;

    invoke-direct {v3, p0}, Lcom/sec/android/directconnect/DirectConnectPopUp$4;-><init>(Lcom/sec/android/directconnect/DirectConnectPopUp;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_2

    .line 138
    :cond_f
    const-string v1, "separate"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 139
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup:Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/sec/android/directconnect/DirectConnectPopUp$5;

    invoke-direct {v2, p0}, Lcom/sec/android/directconnect/DirectConnectPopUp$5;-><init>(Lcom/sec/android/directconnect/DirectConnectPopUp;)V

    invoke-virtual {v1, v7, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_2

    .line 168
    :cond_10
    :try_start_1
    const-string v1, "hotspot"

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPop_up_Handler:Landroid/os/Handler;

    const/4 v2, 0x2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 179
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_progressbar_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_progressbar_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_progressbar_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 188
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0xc8

    .line 46
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const-string v0, "DirectConnectPopUp"

    const-string v1, " onCreate "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->requestWindowFeature(I)Z

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_POPUP_START_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->startPopUp(Landroid/content/Intent;)V

    .line 53
    iput v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->test:I

    .line 63
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_SEP_UI_START_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->startPopUp(Landroid/content/Intent;)V

    .line 56
    iput v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->test:I

    goto :goto_0

    .line 58
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->finish()V

    goto :goto_0

    .line 61
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 217
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 218
    const-string v0, "DirectConnectPopUp"

    const-string v1, " onDestroy "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 224
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 226
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 227
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 228
    iput-object v3, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_progressbar_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_progressbar_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-ne v0, v2, :cond_2

    .line 232
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_progressbar_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 233
    iput-object v3, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->pairing_progressbar_frameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 237
    :cond_2
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 192
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 193
    const-string v0, "DirectConnectPopUp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNewIntent = [ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_POPUP_STOP_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    const-string v0, "DirectConnectPopUp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop... test:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->test:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->finish()V

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_POPUP_START_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 199
    invoke-direct {p0, p1}, Lcom/sec/android/directconnect/DirectConnectPopUp;->startPopUp(Landroid/content/Intent;)V

    goto :goto_0

    .line 200
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_SEP_UI_START_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 201
    invoke-direct {p0, p1}, Lcom/sec/android/directconnect/DirectConnectPopUp;->startPopUp(Landroid/content/Intent;)V

    goto :goto_0

    .line 202
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.directconnect.DIRECTCONNECT_SEP_UI_STOP_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPopup_type:Ljava/lang/String;

    .line 204
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    if-eqz v0, :cond_4

    .line 205
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 208
    :cond_4
    const-string v0, "STOP_ACTIVITY"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    const-string v0, "DirectConnectPopUp"

    const-string v1, "Finishing activity"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->finish()V

    goto :goto_0
.end method
