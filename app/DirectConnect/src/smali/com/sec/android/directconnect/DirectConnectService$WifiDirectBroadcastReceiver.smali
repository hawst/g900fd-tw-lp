.class Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DirectConnectService.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;
.implements Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WifiDirectBroadcastReceiver"
.end annotation


# instance fields
.field private final clientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

.field private oAddress:Ljava/lang/String;

.field private oIPAddress:Ljava/net/InetAddress;

.field final synthetic this$0:Lcom/sec/android/directconnect/DirectConnectService;


# direct methods
.method private constructor <init>(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 1

    .prologue
    .line 2656
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 2659
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$Clients;

    invoke-direct {v0}, Lcom/sec/android/directconnect/DirectConnectService$Clients;-><init>()V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->clientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/directconnect/DirectConnectService;Lcom/sec/android/directconnect/DirectConnectService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/directconnect/DirectConnectService;
    .param p2, "x1"    # Lcom/sec/android/directconnect/DirectConnectService$1;

    .prologue
    .line 2656
    invoke-direct {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;-><init>(Lcom/sec/android/directconnect/DirectConnectService;)V

    return-void
.end method

.method private sendMessage(I)V
    .locals 2
    .param p1, "msg"    # I

    .prologue
    .line 2670
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2671
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/internal/util/StateMachine;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    .line 2673
    :cond_0
    return-void
.end method

.method private sendMessage(IIILjava/lang/Object;)V
    .locals 2
    .param p1, "msg"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 2664
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2665
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/android/internal/util/StateMachine;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    .line 2667
    :cond_0
    return-void
.end method


# virtual methods
.method public onGroupInfoAvailable(Landroid/net/wifi/p2p/WifiP2pGroup;)V
    .locals 6
    .param p1, "arg0"    # Landroid/net/wifi/p2p/WifiP2pGroup;

    .prologue
    .line 2803
    const-string v2, "DirectConnectService"

    const-string v3, "onGroupInfoAvailable"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2805
    if-nez p1, :cond_1

    .line 2806
    const-string v2, "DirectConnectService"

    const-string v3, "onGroupInfoAvailable: arg0 is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2829
    :cond_0
    :goto_0
    return-void

    .line 2810
    :cond_1
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v2

    if-nez v2, :cond_2

    .line 2811
    const-string v2, "DirectConnectService"

    const-string v3, "onGroupInfoAvailable: mCurrentSM is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2816
    :cond_2
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v2

    iget-object v2, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->oAddress:Ljava/lang/String;

    .line 2817
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v3}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v3

    const/16 v4, 0x19

    iget-object v5, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->oAddress:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/android/internal/util/StateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    .line 2819
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 2820
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->clientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2821
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2822
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2823
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 2824
    .local v0, "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->clientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2827
    .end local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v3}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v3

    const/16 v4, 0x1b

    iget-object v5, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->clientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

    invoke-virtual {v3, v4, v5}, Lcom/android/internal/util/StateMachine;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(Landroid/os/Message;)V

    goto :goto_0
.end method

.method public onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 10
    .param p1, "peerList"    # Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2754
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2756
    .local v2, "peers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    const-string v5, "DirectConnectService"

    const-string v6, "onPeersAvailable"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2757
    const-string v5, "DirectConnectService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "in onPeersAvailable ownerMac:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->ownerMac:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/directconnect/DirectConnectService;->access$13400(Lcom/sec/android/directconnect/DirectConnectService;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2759
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2760
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2761
    const-string v5, "DirectConnectService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "in onPeersAvailable peer size:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2762
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 2763
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 2764
    .local v1, "peerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    const-string v5, "DirectConnectService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "in onPeersAvailable peer:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2765
    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->ownerMac:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/directconnect/DirectConnectService;->access$13400(Lcom/sec/android/directconnect/DirectConnectService;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2769
    const-string v5, "DirectConnectService"

    const-string v6, "found onwerMac!"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2770
    iget-object v5, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget-object v6, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    # setter for: Lcom/sec/android/directconnect/DirectConnectService;->ownerName:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/directconnect/DirectConnectService;->access$17002(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)Ljava/lang/String;

    .line 2774
    iget-object v5, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->isOwnerWfdOn:Z
    invoke-static {v5}, Lcom/sec/android/directconnect/DirectConnectService;->access$17100(Lcom/sec/android/directconnect/DirectConnectService;)Z

    move-result v5

    if-ne v5, v3, :cond_2

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupOwner()Z

    move-result v5

    if-eq v5, v3, :cond_0

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupClient()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2776
    :cond_0
    const-string v3, "DirectConnectService"

    const-string v4, "WAIT FOUND OWNER"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2791
    .end local v1    # "peerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_1
    :goto_1
    return-void

    .line 2777
    .restart local v1    # "peerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_2
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupClient()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    .line 2779
    const-string v5, "DirectConnectService"

    const-string v6, "FOUND OWNER"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2780
    iget-object v5, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->ownerFoundT:J
    invoke-static {v5}, Lcom/sec/android/directconnect/DirectConnectService;->access$17200(Lcom/sec/android/directconnect/DirectConnectService;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_3

    .line 2781
    iget-object v5, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    # setter for: Lcom/sec/android/directconnect/DirectConnectService;->ownerFoundT:J
    invoke-static {v5, v6, v7}, Lcom/sec/android/directconnect/DirectConnectService;->access$17202(Lcom/sec/android/directconnect/DirectConnectService;J)J

    .line 2785
    :cond_3
    iget-object v5, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v5}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v6}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupOwner()Z

    move-result v8

    if-ne v8, v3, :cond_4

    :goto_2
    invoke-virtual {v6, v7, v3, v4}, Lcom/android/internal/util/StateMachine;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v6, 0x3e8

    invoke-virtual {v5, v3, v6, v7}, Lcom/android/internal/util/StateMachine;->sendMessageDelayed(Landroid/os/Message;J)V

    goto :goto_1

    :cond_4
    move v3, v4

    goto :goto_2

    .line 2762
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2678
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2679
    .local v0, "action":Ljava/lang/String;
    const-string v9, "DirectConnectService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " onReceive action:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2681
    const-string v9, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2683
    const-string v7, "wifi_p2p_state"

    const/4 v8, -0x1

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 2685
    .local v6, "wifiP2pState":I
    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 2686
    const-string v7, "DirectConnectService"

    const-string v8, "state enabled"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2687
    const/4 v7, 0x4

    invoke-direct {p0, v7}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->sendMessage(I)V

    .line 2740
    .end local v6    # "wifiP2pState":I
    :cond_0
    :goto_0
    return-void

    .line 2689
    .restart local v6    # "wifiP2pState":I
    :cond_1
    const-string v7, "DirectConnectService"

    const-string v8, "state disabled"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2690
    const/4 v7, 0x3

    invoke-direct {p0, v7}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->sendMessage(I)V

    goto :goto_0

    .line 2693
    .end local v6    # "wifiP2pState":I
    :cond_2
    const-string v9, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2694
    iget-object v8, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v7, "wifiP2pDevice"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/net/wifi/p2p/WifiP2pDevice;

    # setter for: Lcom/sec/android/directconnect/DirectConnectService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v8, v7}, Lcom/sec/android/directconnect/DirectConnectService;->access$16802(Lcom/sec/android/directconnect/DirectConnectService;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 2695
    const-string v7, "DirectConnectService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    da=["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v9}, Lcom/sec/android/directconnect/DirectConnectService;->access$16800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v9

    iget-object v9, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2696
    const-string v7, "DirectConnectService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    deviceName=["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v9}, Lcom/sec/android/directconnect/DirectConnectService;->access$16800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v9

    iget-object v9, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2697
    const-string v7, "DirectConnectService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    primaryDeviceType=["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v9}, Lcom/sec/android/directconnect/DirectConnectService;->access$16800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v9

    iget-object v9, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2698
    const-string v7, "DirectConnectService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    secondaryDeviceType=["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v9}, Lcom/sec/android/directconnect/DirectConnectService;->access$16800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v9

    iget-object v9, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->secondaryDeviceType:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2699
    const-string v7, "DirectConnectService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    status=["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mThisDevice:Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v9}, Lcom/sec/android/directconnect/DirectConnectService;->access$16800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v9

    iget v9, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2700
    :cond_3
    const-string v9, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2702
    const/16 v8, 0x9

    invoke-direct {p0, v8}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->sendMessage(I)V

    .line 2704
    const-string v8, "connectedDevAddress"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2705
    .local v1, "connectedDevAddr":Ljava/lang/String;
    const-string v8, "p2pGroupInfo"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 2707
    .local v3, "group":Landroid/net/wifi/p2p/WifiP2pGroup;
    const-string v8, "DirectConnectService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "conn:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " grp:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2708
    if-eqz v1, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v8

    if-ne v8, v7, :cond_0

    .line 2709
    const-string v7, "DirectConnectService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "connectedDevAddr:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2710
    const/16 v7, 0x18

    invoke-direct {p0, v7}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->sendMessage(I)V

    goto/16 :goto_0

    .line 2713
    .end local v1    # "connectedDevAddr":Ljava/lang/String;
    .end local v3    # "group":Landroid/net/wifi/p2p/WifiP2pGroup;
    :cond_4
    const-string v9, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2720
    const-string v9, "wifiP2pInfo"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/net/wifi/p2p/WifiP2pInfo;

    .line 2721
    .local v5, "wifiP2pInfo":Landroid/net/wifi/p2p/WifiP2pInfo;
    const-string v9, "networkInfo"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/net/NetworkInfo;

    .line 2722
    .local v4, "networkInfo":Landroid/net/NetworkInfo;
    const-string v9, "connectedDevAddress"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2723
    .restart local v1    # "connectedDevAddr":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v2

    .line 2725
    .local v2, "detailState":Landroid/net/NetworkInfo$DetailedState;
    iget-object v9, v5, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    iput-object v9, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->oIPAddress:Ljava/net/InetAddress;

    .line 2727
    sget-object v9, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v2, v9, :cond_6

    iget-object v9, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->isWFDEnabled()Z
    invoke-static {v9}, Lcom/sec/android/directconnect/DirectConnectService;->access$200(Lcom/sec/android/directconnect/DirectConnectService;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 2728
    const-string v9, "DirectConnectService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "connectedDevAddr:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2730
    const/4 v9, 0x6

    iget-boolean v10, v5, Landroid/net/wifi/p2p/WifiP2pInfo;->isGroupOwner:Z

    if-ne v10, v7, :cond_5

    :goto_1
    iget-object v10, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->oIPAddress:Ljava/net/InetAddress;

    invoke-direct {p0, v9, v7, v8, v10}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->sendMessage(IIILjava/lang/Object;)V

    .line 2733
    iget-object v7, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget-boolean v8, v5, Landroid/net/wifi/p2p/WifiP2pInfo;->isGroupOwner:Z

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->sendBroadcastIsGroupOwner(Z)V
    invoke-static {v7, v8}, Lcom/sec/android/directconnect/DirectConnectService;->access$16900(Lcom/sec/android/directconnect/DirectConnectService;Z)V

    goto/16 :goto_0

    :cond_5
    move v7, v8

    .line 2730
    goto :goto_1

    .line 2735
    :cond_6
    const-string v7, "DirectConnectService"

    const-string v8, "Not connected"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2736
    const/4 v7, 0x5

    invoke-direct {p0, v7}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->sendMessage(I)V

    goto/16 :goto_0
.end method

.method public wFDRequestGroupInfo()V
    .locals 2

    .prologue
    .line 2794
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$5800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$5700(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2795
    const-string v0, "DirectConnectService"

    const-string v1, "wFDRequestGroupInfo"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2796
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$5800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$5700(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Landroid/net/wifi/p2p/WifiP2pManager;->requestGroupInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;)V

    .line 2798
    :cond_0
    return-void
.end method

.method public wFDRequestPeers()V
    .locals 2

    .prologue
    .line 2744
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$5800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$5700(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2745
    const-string v0, "DirectConnectService"

    const-string v1, "requestPeers"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2746
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$5800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$5700(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Landroid/net/wifi/p2p/WifiP2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    .line 2748
    :cond_0
    return-void
.end method
