.class Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WaitNfcDiscEnabledSepUiState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V
    .locals 0

    .prologue
    .line 1633
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 3

    .prologue
    .line 1637
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1638
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/16 v5, 0x1f

    const/4 v0, 0x1

    .line 1643
    const-string v1, "DirectConnectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handling message "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v3, v3, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1645
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 1665
    const/4 v0, 0x0

    .line 1668
    :goto_0
    return v0

    .line 1649
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopSeparateUi()V
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$900(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 1650
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V
    invoke-static {v1, v5}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$9900(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V

    .line 1651
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->discoverOwner()V
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$9500(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 1652
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDiscoveringOwnerState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$9600(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;

    move-result-object v2

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$10000(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V

    .line 1654
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v2, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    goto :goto_0

    .line 1659
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopSeparateUi(Z)V
    invoke-static {v1, v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$1500(Lcom/sec/android/directconnect/DirectConnectService;Z)V

    .line 1660
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V
    invoke-static {v1, v5}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$10100(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V

    .line 1661
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessage(I)V

    goto :goto_0

    .line 1645
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_1
        0x1e -> :sswitch_0
    .end sparse-switch
.end method
