.class Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
.super Lcom/android/internal/util/StateMachine;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OwnerStateMachine"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$RemovingGrpState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcDisConState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcConState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitWFDisplayResultState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingHSState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitHSResultState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledHSState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscConState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscWFDisplayState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscHSState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledState;,
        Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;
    }
.end annotation


# instance fields
.field private final mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

.field private final mAutoConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;

.field private final mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

.field private final mConnectedWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;

.field private final mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;

.field private final mDisablingHSState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingHSState;

.field private final mDisablingWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;

.field private final mEnabledHSState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledHSState;

.field private final mEnabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDState;

.field private final mEnabledWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;

.field private final mEnablingWFDNfcConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcConState;

.field private final mEnablingWFDNfcDisConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcDisConState;

.field private final mMaxClientsReachedState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;

.field private mOIPAddress:Ljava/net/InetAddress;

.field private final mRemovingGrpState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$RemovingGrpState;

.field private mTCPServerThread:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;

.field private final mWaitHSResultState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitHSResultState;

.field private final mWaitNfcDiscConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscConState;

.field private final mWaitNfcDiscEnabledSepUiState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;

.field private final mWaitNfcDiscEnabledState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledState;

.field private final mWaitNfcDiscHSState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscHSState;

.field private final mWaitNfcDiscWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscWFDisplayState;

.field private final mWaitWFDisplayResultState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitWFDisplayResultState;

.field final synthetic this$0:Lcom/sec/android/directconnect/DirectConnectService;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V
    .locals 2
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 387
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    .line 388
    invoke-direct {p0, p2}, Lcom/android/internal/util/StateMachine;-><init>(Ljava/lang/String;)V

    .line 356
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    .line 358
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDState;

    .line 359
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;

    .line 360
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcConState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcConState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnablingWFDNfcConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcConState;

    .line 361
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcDisConState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcDisConState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnablingWFDNfcDisConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcDisConState;

    .line 362
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mConnectedWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;

    .line 363
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAutoConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;

    .line 365
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledHSState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledHSState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnabledHSState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledHSState;

    .line 366
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitHSResultState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitHSResultState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitHSResultState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitHSResultState;

    .line 367
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingHSState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingHSState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mDisablingHSState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingHSState;

    .line 369
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnabledWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;

    .line 370
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitWFDisplayResultState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitWFDisplayResultState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitWFDisplayResultState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitWFDisplayResultState;

    .line 371
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mDisablingWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;

    .line 373
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$RemovingGrpState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$RemovingGrpState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mRemovingGrpState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$RemovingGrpState;

    .line 375
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscEnabledState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledState;

    .line 376
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscEnabledSepUiState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;

    .line 377
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscHSState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscHSState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscHSState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscHSState;

    .line 378
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscWFDisplayState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscWFDisplayState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscWFDisplayState;

    .line 379
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscConState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscConState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscConState;

    .line 381
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;

    invoke-direct {v0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mMaxClientsReachedState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;

    .line 383
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$Clients;

    invoke-direct {v0}, Lcom/sec/android/directconnect/DirectConnectService$Clients;-><init>()V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

    .line 384
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mTCPServerThread:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;

    .line 390
    const-string v0, "DirectConnectService"

    const-string v1, "Creating State Machine"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 392
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 393
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnablingWFDNfcConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcConState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 394
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnablingWFDNfcDisConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcDisConState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 395
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mConnectedWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAutoConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnabledHSState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledHSState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitHSResultState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitHSResultState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mDisablingHSState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingHSState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 400
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnabledWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 401
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitWFDisplayResultState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitWFDisplayResultState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mDisablingWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mRemovingGrpState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$RemovingGrpState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 404
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscEnabledState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscEnabledSepUiState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 406
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscHSState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscHSState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscWFDisplayState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 408
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscConState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 409
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mMaxClientsReachedState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAllState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AllState;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->addState(Lcom/android/internal/util/State;Lcom/android/internal/util/State;)V

    .line 412
    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->getHotSpotState()I
    invoke-static {p1}, Lcom/sec/android/directconnect/DirectConnectService;->access$000(Lcom/sec/android/directconnect/DirectConnectService;)I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    .line 413
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnabledHSState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledHSState;

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    .line 427
    :goto_0
    return-void

    .line 414
    :cond_0
    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->isWFDisplayEnabled()Z
    invoke-static {p1}, Lcom/sec/android/directconnect/DirectConnectService;->access$100(Lcom/sec/android/directconnect/DirectConnectService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 415
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnabledWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    goto :goto_0

    .line 417
    :cond_1
    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->isWFDEnabled()Z
    invoke-static {p1}, Lcom/sec/android/directconnect/DirectConnectService;->access$200(Lcom/sec/android/directconnect/DirectConnectService;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 418
    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->isWFDConnected()Z
    invoke-static {p1}, Lcom/sec/android/directconnect/DirectConnectService;->access$300(Lcom/sec/android/directconnect/DirectConnectService;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 419
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mConnectedWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    goto :goto_0

    .line 421
    :cond_2
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDState;

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    goto :goto_0

    .line 424
    :cond_3
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;

    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    goto :goto_0
.end method

.method static synthetic access$1000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAutoConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitHSResultState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitHSResultState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitHSResultState;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitWFDisplayResultState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitWFDisplayResultState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitWFDisplayResultState;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$2800(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mConnectedWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$3500(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$3600(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscHSState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscHSState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscHSState;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingHSState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mDisablingHSState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingHSState;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$4400(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$4500(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscWFDisplayState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscWFDisplayState;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$4800(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mDisablingWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$5000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$5100(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDState;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$5300(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$5400(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$5500(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscEnabledState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledState;

    return-object v0
.end method

.method static synthetic access$5600(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$5900(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcDisConState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnablingWFDNfcDisConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcDisConState;

    return-object v0
.end method

.method static synthetic access$6000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$6100(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcConState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnablingWFDNfcConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcConState;

    return-object v0
.end method

.method static synthetic access$6200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$6300(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$6400(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscEnabledSepUiState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;

    return-object v0
.end method

.method static synthetic access$6500(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$6600(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$6700(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$6800(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$6900(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$7000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscConState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscConState;

    return-object v0
.end method

.method static synthetic access$7100(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$7200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Ljava/net/InetAddress;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mOIPAddress:Ljava/net/InetAddress;

    return-object v0
.end method

.method static synthetic access$7202(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Ljava/net/InetAddress;)Ljava/net/InetAddress;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Ljava/net/InetAddress;

    .prologue
    .line 354
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mOIPAddress:Ljava/net/InetAddress;

    return-object p1
.end method

.method static synthetic access$7500(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$RemovingGrpState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mRemovingGrpState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$RemovingGrpState;

    return-object v0
.end method

.method static synthetic access$7600(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$7700(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/sec/android/directconnect/DirectConnectService$Clients;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/sec/android/directconnect/DirectConnectService$Clients;

    .prologue
    .line 354
    invoke-direct {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->copyList(Lcom/sec/android/directconnect/DirectConnectService$Clients;)V

    return-void
.end method

.method static synthetic access$7800(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$Clients;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

    return-object v0
.end method

.method static synthetic access$7900(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$8000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$8200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$8300(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$8400(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$8500(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$8600(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$8700(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$8800(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Ljava/util/ArrayList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 354
    invoke-direct {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->isNewDeviceFound(Ljava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$8900(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # Lcom/android/internal/util/IState;

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    return-void
.end method

.method static synthetic access$9000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$9100(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method static synthetic access$9200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
    .param p1, "x1"    # I

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V

    return-void
.end method

.method private copyList(Lcom/sec/android/directconnect/DirectConnectService$Clients;)V
    .locals 3
    .param p1, "clientNewList"    # Lcom/sec/android/directconnect/DirectConnectService$Clients;

    .prologue
    .line 1344
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1346
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p1, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1347
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1346
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1349
    :cond_0
    return-void
.end method

.method private findDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)Z
    .locals 5
    .param p1, "dev"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 1371
    const-string v2, "DirectConnectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Existing clientList size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

    iget-object v4, v4, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1373
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1374
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 1375
    .local v0, "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    const-string v2, "DirectConnectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dev address:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1376
    iget-object v2, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v3, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1377
    const/4 v2, 0x1

    .line 1382
    .end local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :goto_1
    return v2

    .line 1373
    .restart local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1381
    .end local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_1
    const-string v2, "DirectConnectService"

    const-string v3, "findDevice false"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1382
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private isNewDeviceFound(Ljava/util/ArrayList;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/wifi/p2p/WifiP2pDevice;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "clientNewList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    const/4 v2, 0x1

    .line 1352
    const-string v3, "DirectConnectService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "New list size:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1354
    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;

    iget-object v3, v3, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 1355
    const-string v3, "DirectConnectService"

    const-string v4, "isNewDeviceFound zero so true"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1367
    :goto_0
    return v2

    .line 1359
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 1360
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 1361
    .local v0, "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-direct {p0, v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->findDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1362
    const-string v3, "DirectConnectService"

    const-string v4, "isNewDeviceFound true"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1359
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1366
    .end local v0    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_2
    const-string v2, "DirectConnectService"

    const-string v3, "isNewDeviceFound false"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1367
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public exit()V
    .locals 0

    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->quit()V

    .line 431
    return-void
.end method

.method startTCPServer()V
    .locals 1

    .prologue
    .line 1386
    new-instance v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;-><init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/StateMachine;)V

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mTCPServerThread:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;

    .line 1387
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mTCPServerThread:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->start()V

    .line 1388
    return-void
.end method
