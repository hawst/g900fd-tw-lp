.class public Lcom/sec/android/directconnect/DirectConnectActivity;
.super Landroid/app/Activity;
.source "DirectConnectActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 88
    return-void
.end method

.method private parseNdefMsg(Landroid/content/Intent;)Ljava/lang/String;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 78
    const-string v1, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 80
    .local v0, "rawMsgs":[Landroid/os/Parcelable;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 82
    new-instance v2, Ljava/lang/String;

    aget-object v1, v0, v3

    check-cast v1, Landroid/nfc/NdefMessage;

    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v1

    aget-object v1, v1, v3

    invoke-virtual {v1}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    move-object v1, v2

    .line 84
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private processIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 61
    const-string v3, "DirectConnectActivity"

    const-string v4, "processIntent"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-direct {p0, p1}, Lcom/sec/android/directconnect/DirectConnectActivity;->parseNdefMsg(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    .line 64
    .local v2, "payload":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;

    invoke-direct {v3, v2}, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;->mac:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;->access$000(Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 66
    new-instance v1, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;

    invoke-direct {v1, v2}, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;-><init>(Ljava/lang/String;)V

    .line 68
    .local v1, "msgFormat":Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.android.directconnect.DIRECTCONNECT_C_START_ACTION"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 69
    .local v0, "i":Landroid/content/Intent;
    const-string v3, "mac"

    invoke-virtual {v1}, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;->getMac()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    const-string v3, "wfdStatus"

    invoke-virtual {v1}, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;->getWfdStatus()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v3, "com.sec.android.directconnect"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    invoke-virtual {p0, v0}, Lcom/sec/android/directconnect/DirectConnectActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 75
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "msgFormat":Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const-string v1, "DirectConnectActivity"

    const-string v2, " onCreate "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 27
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29
    invoke-direct {p0, v0}, Lcom/sec/android/directconnect/DirectConnectActivity;->processIntent(Landroid/content/Intent;)V

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectActivity;->finish()V

    .line 32
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 37
    const-string v0, "DirectConnectActivity"

    const-string v1, " onDestroy "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 57
    const-string v0, "DirectConnectActivity"

    const-string v1, "onNewIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 51
    const-string v0, "DirectConnectActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 44
    const-string v0, "DirectConnectActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    return-void
.end method
