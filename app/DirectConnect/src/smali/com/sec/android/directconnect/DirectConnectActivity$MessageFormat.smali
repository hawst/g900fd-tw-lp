.class Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;
.super Ljava/lang/Object;
.source "DirectConnectActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageFormat"
.end annotation


# instance fields
.field private mac:Ljava/lang/String;

.field private mimeType:Ljava/lang/String;

.field private wfdStatus:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "json"    # Ljava/lang/String;

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 98
    .local v1, "obj":Lorg/json/JSONObject;
    const-string v2, "mac"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;->mac:Ljava/lang/String;

    .line 99
    const-string v2, "mimeType"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;->mimeType:Ljava/lang/String;

    .line 100
    const-string v2, "wfdStatus"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;->wfdStatus:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    .end local v1    # "obj":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;->mac:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getMac()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;->mac:Ljava/lang/String;

    return-object v0
.end method

.method public getWfdStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectActivity$MessageFormat;->wfdStatus:Ljava/lang/String;

    return-object v0
.end method
