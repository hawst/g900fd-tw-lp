.class Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EnabledWFDisplayState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V
    .locals 0

    .prologue
    .line 823
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 3

    .prologue
    .line 827
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 833
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handling message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 835
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 847
    const/4 v0, 0x0

    .line 850
    :goto_0
    return v0

    .line 837
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mNfcState:I
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$3400(Lcom/sec/android/directconnect/DirectConnectService;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 839
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v1, "wfdisplay"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 840
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitWFDisplayResultState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitWFDisplayResultState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$2500(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitWFDisplayResultState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$4400(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    .line 850
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 842
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscWFDisplayState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscWFDisplayState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$4500(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscWFDisplayState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$4600(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    goto :goto_1

    .line 835
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
