.class Lcom/sec/android/directconnect/DirectConnectService$4;
.super Ljava/lang/Object;
.source "DirectConnectService.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/directconnect/DirectConnectService;->discoverPeers()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directconnect/DirectConnectService;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService;)V
    .locals 0

    .prologue
    .line 2872
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$4;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 4
    .param p1, "arg0"    # I

    .prologue
    .line 2880
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFailure: discoverPeer:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2881
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 2883
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$4;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mCurrentSM:Lcom/android/internal/util/StateMachine;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$16700(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/android/internal/util/StateMachine;

    move-result-object v0

    const/16 v1, 0x22

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessageDelayed(IJ)V

    .line 2885
    :cond_0
    return-void
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 2876
    const-string v0, "DirectConnectService"

    const-string v1, "onSuccess: discoverPeers"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2877
    return-void
.end method
