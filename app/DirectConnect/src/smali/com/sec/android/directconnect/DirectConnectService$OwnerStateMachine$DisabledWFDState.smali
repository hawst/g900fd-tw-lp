.class Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DisabledWFDState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V
    .locals 0

    .prologue
    .line 974
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 3

    .prologue
    .line 978
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 979
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->wFDInit()V
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$3200(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 980
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 984
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handling message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 986
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 999
    const/4 v0, 0x0

    .line 1002
    :goto_0
    return v0

    .line 990
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$5800(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$5700(Lcom/sec/android/directconnect/DirectConnectService;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/wifi/p2p/WifiP2pManager;->enableP2p(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)V

    .line 991
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mNfcState:I
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$3400(Lcom/sec/android/directconnect/DirectConnectService;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 992
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnablingWFDNfcDisConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcDisConState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$5900(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcDisConState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$6000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    .line 1002
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 994
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnablingWFDNfcConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcConState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$6100(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnablingWFDNfcConState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$6200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    goto :goto_1

    .line 986
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xc -> :sswitch_0
        0xe -> :sswitch_0
    .end sparse-switch
.end method
