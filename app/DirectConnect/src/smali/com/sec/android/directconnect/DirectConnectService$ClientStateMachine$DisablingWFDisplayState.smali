.class Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DisablingWFDisplayState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V
    .locals 0

    .prologue
    .line 1959
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 3

    .prologue
    .line 1963
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1966
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v1, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 1967
    return-void
.end method

.method public exit()V
    .locals 3

    .prologue
    .line 2000
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exiting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2001
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    const/16 v1, 0x24

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$13000(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V

    .line 2002
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/16 v4, 0xe

    .line 1972
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handling message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1974
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1992
    const/4 v0, 0x0

    .line 1995
    :goto_0
    return v0

    .line 1976
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$12000(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$12700(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V

    .line 1977
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v0, v4}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessage(I)V

    .line 1995
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1982
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    const/16 v1, 0x24

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessageDelayed(IJ)V

    goto :goto_1

    .line 1986
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mEnabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$12800(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$12900(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V

    .line 1987
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v0, v4}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessage(I)V

    goto :goto_1

    .line 1974
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x5 -> :sswitch_1
        0x24 -> :sswitch_2
    .end sparse-switch
.end method
