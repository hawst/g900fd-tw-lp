.class Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConnectedWFDState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V
    .locals 0

    .prologue
    .line 1090
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 3

    .prologue
    .line 1094
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x1

    .line 1099
    const-string v0, "DirectConnectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handling message "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v3, v3, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1162
    const/4 v0, 0x0

    .line 1165
    :goto_0
    return v0

    .line 1103
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mNfcState:I
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$3400(Lcom/sec/android/directconnect/DirectConnectService;)I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 1105
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->wFDInit()V
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$3200(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 1109
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v2, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v0, v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    :goto_1
    :sswitch_1
    move v0, v1

    .line 1165
    goto :goto_0

    .line 1111
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mWaitNfcDiscConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscConState;
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscConState;

    move-result-object v2

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7100(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    goto :goto_1

    .line 1116
    :sswitch_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v1, :cond_1

    .line 1117
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/net/InetAddress;

    # setter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mOIPAddress:Ljava/net/InetAddress;
    invoke-static {v2, v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7202(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Ljava/net/InetAddress;)Ljava/net/InetAddress;

    .line 1119
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$7300(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->wFDRequestGroupInfo()V

    goto :goto_1

    .line 1125
    :cond_1
    const-string v0, "DirectConnectService"

    const-string v2, "Device connected but client. So disconnecting..."

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->callRemoveGroup()V
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$7400(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 1127
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mRemovingGrpState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$RemovingGrpState;
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7500(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$RemovingGrpState;

    move-result-object v2

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7600(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    goto :goto_1

    .line 1132
    :sswitch_3
    const-string v0, "DirectConnectService"

    const-string v2, "Updating initial client list"

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1133
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/directconnect/DirectConnectService$Clients;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->copyList(Lcom/sec/android/directconnect/DirectConnectService$Clients;)V
    invoke-static {v2, v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7700(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/sec/android/directconnect/DirectConnectService$Clients;)V

    .line 1135
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7800(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$Clients;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x5

    if-lt v0, v2, :cond_2

    .line 1136
    const-string v0, "DirectConnectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "max clients "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;
    invoke-static {v3}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7800(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$Clients;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " reached"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1137
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v2, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopPopUp(Ljava/lang/String;)V
    invoke-static {v0, v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$4100(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 1143
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    const/16 v2, 0x14

    invoke-virtual {v0, v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessage(I)V

    goto/16 :goto_1

    .line 1147
    :cond_2
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->autoConnect()V
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$1100(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 1148
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAutoConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$1200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;

    move-result-object v2

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7900(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    goto/16 :goto_1

    .line 1153
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->autoConnect()V
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$1100(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 1154
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAutoConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$1200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;

    move-result-object v2

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$8000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    goto/16 :goto_1

    .line 1101
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x6 -> :sswitch_2
        0x17 -> :sswitch_4
        0x19 -> :sswitch_1
        0x1b -> :sswitch_3
    .end sparse-switch
.end method
