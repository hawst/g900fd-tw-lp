.class Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EnabledHSState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V
    .locals 0

    .prologue
    .line 1788
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 3

    .prologue
    .line 1792
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1793
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 1798
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handling message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1800
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1812
    const/4 v0, 0x0

    .line 1815
    :goto_0
    return v0

    .line 1802
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mNfcState:I
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$3400(Lcom/sec/android/directconnect/DirectConnectService;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1804
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v1, "hotspot"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 1805
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitHSResultState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitHSResultState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$10300(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitHSResultState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$11500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V

    .line 1815
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1807
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$EnabledHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscHSState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscHSState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$11600(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscHSState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$11700(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V

    goto :goto_1

    .line 1800
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
