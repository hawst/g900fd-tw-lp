.class Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AutoConState"
.end annotation


# instance fields
.field firstTimeout:Z

.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V
    .locals 1

    .prologue
    .line 1245
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    .line 1246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->firstTimeout:Z

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 5

    .prologue
    const/4 v4, 0x7

    .line 1250
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1251
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V
    invoke-static {v0, v4}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$8600(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V

    .line 1252
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v4, v2, v3}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessageDelayed(IJ)V

    .line 1255
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->isWFDConnected()Z
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$300(Lcom/sec/android/directconnect/DirectConnectService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1256
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->startTCPServer()V

    .line 1258
    :cond_0
    return-void
.end method

.method public exit()V
    .locals 3

    .prologue
    .line 1338
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exiting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    const/4 v1, 0x7

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$9200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V

    .line 1340
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 8
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x1

    const/16 v7, 0x14

    const/4 v6, 0x7

    .line 1262
    const-string v2, "DirectConnectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Handling message "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v4, v4, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1264
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 1330
    const/4 v1, 0x0

    .line 1333
    :cond_0
    :goto_0
    return v1

    .line 1268
    :sswitch_0
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-nez v2, :cond_0

    .line 1269
    const-string v2, "DirectConnectService"

    const-string v3, "this device should be owner but client."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1271
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V
    invoke-static {v2, v6}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$8700(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V

    .line 1272
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v3, "connecterror"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 1273
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-virtual {v2, v7}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessage(I)V

    goto :goto_0

    .line 1286
    :sswitch_1
    iget-boolean v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->firstTimeout:Z

    if-nez v2, :cond_1

    .line 1287
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    const-wide/16 v4, 0x3a98

    invoke-virtual {v2, v6, v4, v5}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessageDelayed(IJ)V

    .line 1288
    iput-boolean v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->firstTimeout:Z

    .line 1289
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->autoConnect()V
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$1100(Lcom/sec/android/directconnect/DirectConnectService;)V

    goto :goto_0

    .line 1291
    :cond_1
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v3, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopPopUp(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->access$4100(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 1292
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-virtual {v2, v7}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessage(I)V

    goto :goto_0

    .line 1298
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$7300(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->wFDRequestGroupInfo()V

    goto :goto_0

    .line 1306
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/directconnect/DirectConnectService$Clients;

    .line 1307
    .local v0, "clientNewList":Lcom/sec/android/directconnect/DirectConnectService$Clients;
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v3, v0, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->isNewDeviceFound(Ljava/util/ArrayList;)Z
    invoke-static {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$8800(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Ljava/util/ArrayList;)Z

    move-result v2

    if-ne v2, v1, :cond_0

    .line 1308
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v3, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopPopUp(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->access$4100(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 1309
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    const/16 v3, 0x16

    invoke-virtual {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessage(I)V

    .line 1310
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mConnectedWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;
    invoke-static {v3}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$3000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$ConnectedWFDState;

    move-result-object v3

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$8900(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    .line 1311
    const-string v2, "DirectConnectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updating initial client list, size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1312
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->copyList(Lcom/sec/android/directconnect/DirectConnectService$Clients;)V
    invoke-static {v2, v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7700(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/sec/android/directconnect/DirectConnectService$Clients;)V

    goto/16 :goto_0

    .line 1317
    .end local v0    # "clientNewList":Lcom/sec/android/directconnect/DirectConnectService$Clients;
    :sswitch_4
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V
    invoke-static {v2, v6}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$9000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V

    .line 1318
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v3, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopPopUp(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->access$4100(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 1319
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-virtual {v2, v7}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessage(I)V

    goto/16 :goto_0

    .line 1323
    :sswitch_5
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V
    invoke-static {v2, v6}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$9100(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V

    .line 1325
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v3, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopPopUp(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->access$4100(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 1326
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-virtual {v2, v7}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessage(I)V

    goto/16 :goto_0

    .line 1264
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x7 -> :sswitch_1
        0x13 -> :sswitch_4
        0x18 -> :sswitch_2
        0x1b -> :sswitch_3
        0x20 -> :sswitch_5
    .end sparse-switch
.end method
