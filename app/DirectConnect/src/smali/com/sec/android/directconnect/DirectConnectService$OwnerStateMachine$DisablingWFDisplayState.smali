.class Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DisablingWFDisplayState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V
    .locals 0

    .prologue
    .line 892
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 3

    .prologue
    .line 896
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 899
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v1, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 900
    return-void
.end method

.method public exit()V
    .locals 3

    .prologue
    .line 932
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exiting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 933
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    const/16 v1, 0x24

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$5300(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V

    .line 934
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/16 v4, 0xe

    .line 905
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handling message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 924
    const/4 v0, 0x0

    .line 927
    :goto_0
    return v0

    .line 909
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$4200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisabledWFDState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$5000(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    .line 910
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-virtual {v0, v4}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessage(I)V

    .line 927
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 915
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    const/16 v1, 0x24

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessageDelayed(IJ)V

    goto :goto_1

    .line 919
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mEnabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$5100(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$EnabledWFDState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$5200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    .line 920
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$DisablingWFDisplayState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-virtual {v0, v4}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessage(I)V

    goto :goto_1

    .line 907
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x5 -> :sswitch_1
        0x24 -> :sswitch_2
    .end sparse-switch
.end method
