.class Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TCPClientState"
.end annotation


# instance fields
.field private tcp_client_retry:I

.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V
    .locals 1

    .prologue
    .line 2068
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    .line 2069
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;->tcp_client_retry:I

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 3

    .prologue
    .line 2073
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2075
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->startTCPClient()V

    .line 2076
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/16 v4, 0x14

    .line 2081
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handling message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2083
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 2096
    const/4 v0, 0x0

    .line 2099
    :goto_0
    return v0

    .line 2086
    :pswitch_0
    iget v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;->tcp_client_retry:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;->tcp_client_retry:I

    .line 2087
    iget v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;->tcp_client_retry:I

    if-ge v0, v4, :cond_0

    .line 2088
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->startTCPClient()V

    .line 2099
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2090
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v0, v4}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessage(I)V

    goto :goto_1

    .line 2083
    :pswitch_data_0
    .packed-switch 0x21
        :pswitch_0
    .end packed-switch
.end method
