.class Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WaitNfcDiscEnabledSepUiState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V
    .locals 0

    .prologue
    .line 556
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 3

    .prologue
    .line 560
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/16 v5, 0x1f

    const/4 v0, 0x1

    .line 566
    const-string v1, "DirectConnectService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handling message "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v3, v3, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 589
    const/4 v0, 0x0

    .line 592
    :goto_0
    return v0

    .line 571
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopSeparateUi()V
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$900(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 572
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V
    invoke-static {v1, v5}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$1700(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V

    .line 574
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->autoConnect()V
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$1100(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 575
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAutoConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$1200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;

    move-result-object v2

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$1800(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    .line 578
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v2, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    goto :goto_0

    .line 583
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopSeparateUi(Z)V
    invoke-static {v1, v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$1500(Lcom/sec/android/directconnect/DirectConnectService;Z)V

    .line 584
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V
    invoke-static {v1, v5}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$1900(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V

    .line 585
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$WaitNfcDiscEnabledSepUiState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessage(I)V

    goto :goto_0

    .line 568
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_1
        0x1e -> :sswitch_0
    .end sparse-switch
.end method
