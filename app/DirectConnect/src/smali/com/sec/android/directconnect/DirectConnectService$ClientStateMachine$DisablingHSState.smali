.class Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DisablingHSState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V
    .locals 0

    .prologue
    .line 1856
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 4

    .prologue
    .line 1860
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1861
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    const/16 v1, 0xb

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessageDelayed(IJ)V

    .line 1864
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v1, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 1865
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/16 v4, 0xb

    .line 1870
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handling message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1872
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1883
    const/4 v0, 0x0

    .line 1886
    :goto_0
    return v0

    .line 1874
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->getHotSpotState()I
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$000(Lcom/sec/android/directconnect/DirectConnectService;)I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 1875
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mDisabledWFDState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$12000(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisabledState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$12100(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V

    .line 1876
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessage(I)V

    .line 1886
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1878
    :cond_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DisablingHSState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessageDelayed(IJ)V

    goto :goto_1

    .line 1872
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
    .end packed-switch
.end method
