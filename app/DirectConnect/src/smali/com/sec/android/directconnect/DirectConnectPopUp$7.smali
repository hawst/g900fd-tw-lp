.class Lcom/sec/android/directconnect/DirectConnectPopUp$7;
.super Landroid/os/Handler;
.source "DirectConnectPopUp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectPopUp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectPopUp;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v4, 0x7f050005

    const/16 v1, 0x3e9

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 243
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 244
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v5, :cond_2

    .line 245
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # getter for: Lcom/sec/android/directconnect/DirectConnectPopUp;->mCount_down:I
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$100(Lcom/sec/android/directconnect/DirectConnectPopUp;)I

    move-result v0

    if-le v0, v5, :cond_1

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # getter for: Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$200(Lcom/sec/android/directconnect/DirectConnectPopUp;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # operator-- for: Lcom/sec/android/directconnect/DirectConnectPopUp;->mCount_down:I
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$110(Lcom/sec/android/directconnect/DirectConnectPopUp;)I

    .line 247
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # getter for: Lcom/sec/android/directconnect/DirectConnectPopUp;->textView_Message:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$300(Lcom/sec/android/directconnect/DirectConnectPopUp;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    const v3, 0x7f050003

    invoke-virtual {v2, v3}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    invoke-virtual {v2, v4}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # getter for: Lcom/sec/android/directconnect/DirectConnectPopUp;->mCount_down:I
    invoke-static {v4}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$100(Lcom/sec/android/directconnect/DirectConnectPopUp;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPop_up_Handler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # getter for: Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$200(Lcom/sec/android/directconnect/DirectConnectPopUp;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # getter for: Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$200(Lcom/sec/android/directconnect/DirectConnectPopUp;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # invokes: Lcom/sec/android/directconnect/DirectConnectPopUp;->sendAction(I)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$000(Lcom/sec/android/directconnect/DirectConnectPopUp;I)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->finish()V

    goto :goto_0

    .line 255
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v7, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # getter for: Lcom/sec/android/directconnect/DirectConnectPopUp;->mCount_down:I
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$100(Lcom/sec/android/directconnect/DirectConnectPopUp;)I

    move-result v0

    if-le v0, v5, :cond_3

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # getter for: Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$200(Lcom/sec/android/directconnect/DirectConnectPopUp;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 257
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # operator-- for: Lcom/sec/android/directconnect/DirectConnectPopUp;->mCount_down:I
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$110(Lcom/sec/android/directconnect/DirectConnectPopUp;)I

    .line 258
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # getter for: Lcom/sec/android/directconnect/DirectConnectPopUp;->textView_Message:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$300(Lcom/sec/android/directconnect/DirectConnectPopUp;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    const v3, 0x7f050004

    invoke-virtual {v2, v3}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    invoke-virtual {v2, v4}, Lcom/sec/android/directconnect/DirectConnectPopUp;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # getter for: Lcom/sec/android/directconnect/DirectConnectPopUp;->mCount_down:I
    invoke-static {v4}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$100(Lcom/sec/android/directconnect/DirectConnectPopUp;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectPopUp;->mPop_up_Handler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v7, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 261
    :cond_3
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # getter for: Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$200(Lcom/sec/android/directconnect/DirectConnectPopUp;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # getter for: Lcom/sec/android/directconnect/DirectConnectPopUp;->m_PopUp:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$200(Lcom/sec/android/directconnect/DirectConnectPopUp;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    # invokes: Lcom/sec/android/directconnect/DirectConnectPopUp;->sendAction(I)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectPopUp;->access$000(Lcom/sec/android/directconnect/DirectConnectPopUp;I)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectPopUp$7;->this$0:Lcom/sec/android/directconnect/DirectConnectPopUp;

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectPopUp;->finish()V

    goto/16 :goto_0
.end method
