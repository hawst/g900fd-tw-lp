.class Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MaxClientsReachedState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)V
    .locals 0

    .prologue
    .line 1169
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 5

    .prologue
    const/16 v4, 0x23

    .line 1173
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1174
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->addService()V
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$8100(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 1175
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V
    invoke-static {v0, v4}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$8200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V

    .line 1176
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v4, v2, v3}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessageDelayed(IJ)V

    .line 1177
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/16 v4, 0x23

    .line 1182
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handling message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1184
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1212
    const/4 v0, 0x0

    .line 1215
    :goto_0
    return v0

    .line 1186
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessage(I)V

    .line 1215
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1191
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->removeMessages(I)V
    invoke-static {v0, v4}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$8300(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;I)V

    .line 1192
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$7300(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->wFDRequestGroupInfo()V

    goto :goto_1

    .line 1196
    :sswitch_2
    const-string v0, "DirectConnectService"

    const-string v1, "Updating initial client list"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/directconnect/DirectConnectService$Clients;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->copyList(Lcom/sec/android/directconnect/DirectConnectService$Clients;)V
    invoke-static {v1, v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7700(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/sec/android/directconnect/DirectConnectService$Clients;)V

    .line 1199
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7800(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$Clients;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    .line 1200
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "max clients "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mClientList:Lcom/sec/android/directconnect/DirectConnectService$Clients;
    invoke-static {v2}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7800(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$Clients;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$Clients;->list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " reached"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1202
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v4, v2, v3}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->sendMessageDelayed(IJ)V

    goto :goto_1

    .line 1205
    :cond_0
    const-string v0, "DirectConnectService"

    const-string v1, "oh, one of the clients disconnected"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1206
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->autoConnect()V
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$1100(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 1207
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$MaxClientsReachedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mAutoConState:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$1200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$AutoConState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$8400(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/IState;)V

    goto :goto_1

    .line 1184
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x1b -> :sswitch_2
        0x23 -> :sswitch_0
    .end sparse-switch
.end method
