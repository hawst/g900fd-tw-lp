.class Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;
.super Ljava/lang/Thread;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TCPServerThread"
.end annotation


# instance fields
.field clientSock:Ljava/net/Socket;

.field serverSock:Ljava/net/ServerSocket;

.field sm:Lcom/android/internal/util/StateMachine;

.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;Lcom/android/internal/util/StateMachine;)V
    .locals 1
    .param p2, "s"    # Lcom/android/internal/util/StateMachine;

    .prologue
    .line 1396
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1393
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->clientSock:Ljava/net/Socket;

    .line 1397
    iput-object p2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->sm:Lcom/android/internal/util/StateMachine;

    .line 1398
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1402
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 1404
    const-string v2, "DirectConnectService"

    const-string v3, "Start TCP server"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1406
    :try_start_0
    const-string v2, "DirectConnectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Open server socket on "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mOIPAddress:Ljava/net/InetAddress;
    invoke-static {v4}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Ljava/net/InetAddress;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1407
    new-instance v2, Ljava/net/ServerSocket;

    const v3, 0xc351

    const/4 v4, 0x5

    iget-object v5, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->this$1:Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->mOIPAddress:Ljava/net/InetAddress;
    invoke-static {v5}, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;->access$7200(Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine;)Ljava/net/InetAddress;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->serverSock:Ljava/net/ServerSocket;

    .line 1408
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->serverSock:Ljava/net/ServerSocket;

    const/16 v3, 0xfa0

    invoke-virtual {v2, v3}, Ljava/net/ServerSocket;->setSoTimeout(I)V

    .line 1410
    const-string v2, "DirectConnectService"

    const-string v3, "Waiting for TCP connection"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1412
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->serverSock:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->clientSock:Ljava/net/Socket;

    .line 1413
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->clientSock:Ljava/net/Socket;

    if-eqz v2, :cond_0

    .line 1415
    const-string v2, "DirectConnectService"

    const-string v3, "TCP connection success"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1416
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->sm:Lcom/android/internal/util/StateMachine;

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Lcom/android/internal/util/StateMachine;->sendMessage(I)V

    .line 1417
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->clientSock:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1432
    :cond_0
    :goto_0
    :try_start_2
    const-string v2, "DirectConnectService"

    const-string v3, "Closing Server Socket"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1433
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->serverSock:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 1440
    :goto_1
    return-void

    .line 1421
    :catch_0
    move-exception v1

    .line 1422
    .local v1, "ste":Ljava/net/SocketTimeoutException;
    :try_start_3
    const-string v2, "DirectConnectService"

    const-string v3, "Server wait timeout"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1426
    .end local v1    # "ste":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v0

    .line 1427
    .local v0, "e":Ljava/io/IOException;
    :try_start_4
    const-string v2, "DirectConnectService"

    const-string v3, "Accept failed"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1432
    :try_start_5
    const-string v2, "DirectConnectService"

    const-string v3, "Closing Server Socket"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1433
    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->serverSock:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 1436
    :catch_2
    move-exception v0

    .line 1437
    const-string v2, "DirectConnectService"

    const-string v3, "Could not close socket"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1436
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 1437
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v2, "DirectConnectService"

    const-string v3, "Could not close socket"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1431
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 1432
    :try_start_6
    const-string v3, "DirectConnectService"

    const-string v4, "Closing Server Socket"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1433
    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$OwnerStateMachine$TCPServerThread;->serverSock:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 1438
    :goto_2
    throw v2

    .line 1436
    :catch_4
    move-exception v0

    .line 1437
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v3, "DirectConnectService"

    const-string v4, "Could not close socket"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
