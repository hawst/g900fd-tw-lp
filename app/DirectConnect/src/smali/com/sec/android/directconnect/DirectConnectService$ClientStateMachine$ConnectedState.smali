.class Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConnectedState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V
    .locals 0

    .prologue
    .line 2005
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 3

    .prologue
    .line 2009
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2010
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 2015
    const-string v1, "DirectConnectService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Handling message "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v4, v4, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2017
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 2061
    const/4 v1, 0x0

    .line 2064
    :goto_0
    return v1

    .line 2022
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mNfcState:I
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$3400(Lcom/sec/android/directconnect/DirectConnectService;)I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2025
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->wFDInit()V
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$3200(Lcom/sec/android/directconnect/DirectConnectService;)V

    :goto_1
    move v1, v2

    .line 2064
    goto :goto_0

    .line 2027
    :cond_0
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mWaitNfcDiscConState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscConState;
    invoke-static {v3}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$13100(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$WaitNfcDiscConState;

    move-result-object v3

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v1, v3}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$13200(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V

    goto :goto_1

    .line 2032
    :sswitch_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-nez v1, :cond_1

    .line 2033
    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/net/InetAddress;

    # setter for: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mOIPAddress:Ljava/net/InetAddress;
    invoke-static {v3, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$13302(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Ljava/net/InetAddress;)Ljava/net/InetAddress;

    .line 2035
    :cond_1
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$7300(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->wFDRequestGroupInfo()V

    goto :goto_1

    .line 2040
    :sswitch_2
    const-string v1, "DirectConnectService"

    const-string v3, "unusual case"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2044
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 2046
    .local v0, "oAddress":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->ownerMac:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$13400(Lcom/sec/android/directconnect/DirectConnectService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2048
    const-string v1, "DirectConnectService"

    const-string v3, "client already connected. No action"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2049
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mTCPClientState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;
    invoke-static {v3}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$13500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$TCPClientState;

    move-result-object v3

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v1, v3}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$13600(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V

    .line 2050
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopSeparateUi(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/directconnect/DirectConnectService;->access$1500(Lcom/sec/android/directconnect/DirectConnectService;Z)V

    goto :goto_1

    .line 2053
    :cond_2
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v3, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v1, v3}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 2055
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, v1, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->callRemoveGroup()V
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$7400(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 2056
    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v3, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectedState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mRemovingGrpState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$RemovingGrpState;
    invoke-static {v3}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$13700(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$RemovingGrpState;

    move-result-object v3

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v1, v3}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$13800(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V

    goto :goto_1

    .line 2017
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x5 -> :sswitch_2
        0x6 -> :sswitch_1
        0x19 -> :sswitch_3
    .end sparse-switch
.end method
