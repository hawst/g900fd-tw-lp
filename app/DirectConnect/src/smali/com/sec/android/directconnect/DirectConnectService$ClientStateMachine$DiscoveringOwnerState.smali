.class Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;
.super Lcom/android/internal/util/State;
.source "DirectConnectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DiscoveringOwnerState"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;


# direct methods
.method constructor <init>(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V
    .locals 0

    .prologue
    .line 2296
    iput-object p1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    return-void
.end method


# virtual methods
.method public enter()V
    .locals 5

    .prologue
    const/16 v4, 0x11

    .line 2300
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2301
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V
    invoke-static {v0, v4}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$15400(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V

    .line 2302
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v4, v2, v3}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessageDelayed(IJ)V

    .line 2303
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/16 v5, 0x14

    const/16 v4, 0x11

    .line 2307
    const-string v0, "DirectConnectService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handling message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v2, v2, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Lcom/sec/android/directconnect/DirectConnectService;->getMsgName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2309
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 2340
    const/4 v0, 0x0

    .line 2343
    :goto_0
    return v0

    .line 2311
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService;->mReceiver:Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$7300(Lcom/sec/android/directconnect/DirectConnectService;)Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/directconnect/DirectConnectService$WifiDirectBroadcastReceiver;->wFDRequestPeers()V

    .line 2343
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2316
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V
    invoke-static {v0, v4}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$15500(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V

    .line 2317
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->WFDConnect()V
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$15600(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)V

    .line 2318
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v1, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # getter for: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->mConnectingState:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;
    invoke-static {v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$15700(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;)Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$ConnectingState;

    move-result-object v1

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$15800(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;Lcom/android/internal/util/IState;)V

    goto :goto_1

    .line 2322
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v1, "connecterror"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->startPopUp(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$1400(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 2323
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopDiscovery()V
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$600(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 2324
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v0, v5}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessage(I)V

    goto :goto_1

    .line 2328
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    const-string v1, "connecting"

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopPopUp(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/directconnect/DirectConnectService;->access$4100(Lcom/sec/android/directconnect/DirectConnectService;Ljava/lang/String;)V

    .line 2329
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->removeMessages(I)V
    invoke-static {v0, v4}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->access$15900(Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;I)V

    .line 2330
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->stopDiscovery()V
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$600(Lcom/sec/android/directconnect/DirectConnectService;)V

    .line 2331
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    invoke-virtual {v0, v5}, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->sendMessage(I)V

    goto :goto_1

    .line 2336
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine$DiscoveringOwnerState;->this$1:Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;

    iget-object v0, v0, Lcom/sec/android/directconnect/DirectConnectService$ClientStateMachine;->this$0:Lcom/sec/android/directconnect/DirectConnectService;

    # invokes: Lcom/sec/android/directconnect/DirectConnectService;->discoverOwner()V
    invoke-static {v0}, Lcom/sec/android/directconnect/DirectConnectService;->access$9500(Lcom/sec/android/directconnect/DirectConnectService;)V

    goto :goto_1

    .line 2309
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0x9 -> :sswitch_0
        0x11 -> :sswitch_2
        0x13 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method
