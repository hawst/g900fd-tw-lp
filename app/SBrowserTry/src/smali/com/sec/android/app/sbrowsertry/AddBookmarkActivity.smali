.class public Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;
.super Landroid/app/Activity;
.source "AddBookmarkActivity.java"


# instance fields
.field private doneButton:Landroid/widget/LinearLayout;

.field private mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;)Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    return-object v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 109
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setHandPointerMarginEndValue(I)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const v1, 0x7f03000a

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setContentView(I)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    sget-object v1, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setTouchTransparencyMode(Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setShowWrongInputToast(Z)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->show()V

    .line 117
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->invalidateOptionsMenu()V

    .line 118
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 26
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v2, 0x7f030003

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->setContentView(I)V

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 34
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/high16 v2, 0x7f030000

    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 36
    .local v0, "actionBarButtons":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 43
    .end local v0    # "actionBarButtons":Landroid/view/View;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x7f080009

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->doneButton:Landroid/widget/LinearLayout;

    .line 44
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->doneButton:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity$1;-><init>(Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    new-instance v2, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-direct {v2, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 60
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/lit8 v3, v3, 0x1

    div-int/lit8 v3, v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setHandPointerMarginEndValue(I)V

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const v3, 0x7f03000a

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setContentView(I)V

    .line 62
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    sget-object v3, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setTouchTransparencyMode(Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;)V

    .line 63
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setShowWrongInputToast(Z)V

    .line 64
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v2, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 65
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v2}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->show()V

    .line 66
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 70
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 76
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->setResult(I)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->finish()V

    .line 79
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 89
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0800ac

    if-ne v0, v1, :cond_1

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 94
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->setResult(I)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->finish()V

    .line 100
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->showWrongInputToast()V

    goto :goto_0
.end method
