.class Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;
.super Ljava/lang/Object;
.source "SmartScreenActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const v6, 0x7f080041

    const v5, 0x7f030020

    const/16 v4, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$200(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v0, v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$302(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;Landroid/view/View;)Landroid/view/View;

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$300(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f09003d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$300(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->showDialog(Landroid/view/View;Z)V

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$200(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v0, v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$302(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;Landroid/view/View;)Landroid/view/View;

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$300(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f09003c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$300(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->showDialog(Landroid/view/View;Z)V

    goto :goto_0

    .line 290
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$200(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$000(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->checkVisualFeedbackEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$400(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    const v1, 0x7f08004b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 297
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    const v1, 0x7f08004c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
