.class Lcom/sec/android/app/sbrowsertry/MainActivity$1;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sbrowsertry/MainActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sbrowsertry/MainActivity;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const v6, 0x7f08000e

    .line 501
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    iget v3, v3, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v4, 0x20

    if-ne v3, v4, :cond_1

    .line 503
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    iget-object v3, v3, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "finger_air_view_sound_and_haptic_feedback"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 505
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    iget-object v3, v3, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const v4, 0x7f080037

    invoke-virtual {v3, v4}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 506
    .local v2, "view":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/MainActivity;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v3}, Lcom/sec/android/app/sbrowsertry/MainActivity;->access$000(Lcom/sec/android/app/sbrowsertry/MainActivity;)Landroid/media/AudioManager;

    move-result-object v3

    const/16 v4, 0xd

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 507
    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 510
    .end local v2    # "view":Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    const v4, 0x7f020064

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 512
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 514
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 515
    .local v2, "view":Landroid/widget/ImageView;
    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 516
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v3, 0x7f070040

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 517
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 519
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/sbrowsertry/MainActivity;->handleStateChange()V

    .line 530
    .end local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "res":Landroid/content/res/Resources;
    .end local v2    # "view":Landroid/widget/ImageView;
    :goto_0
    return-void

    .line 521
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    iget v3, v3, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v4, 0x21

    if-ne v3, v4, :cond_2

    .line 523
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/sbrowsertry/MainActivity;->handleStateChange()V

    goto :goto_0

    .line 527
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 528
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/sbrowsertry/MainActivity;->finish()V

    goto :goto_0
.end method
