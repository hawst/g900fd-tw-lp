.class public Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;
.super Landroid/app/Activity;
.source "TbAddBookmarkActivity.java"


# instance fields
.field private mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V
    .locals 2
    .param p1, "resID"    # I
    .param p2, "mode"    # Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;
    .param p3, "bIgnoreHover"    # Z

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->dismissHelpDialog()V

    .line 79
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setContentView(I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p2}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setTouchTransparencyMode(Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setShowWrongInputToast(Z)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p3}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setIgnoreHoverEvt(Z)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->show()V

    .line 86
    return-void
.end method


# virtual methods
.method public dismissHelpDialog()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 93
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->dismissHelpDialog()V

    .line 69
    const v0, 0x7f03000a

    sget-object v1, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 74
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 26
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->setContentView(I)V

    .line 30
    const v0, 0x7f03000a

    sget-object v1, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 35
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 99
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 100
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 53
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 57
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->setResult(I)V

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->finish()V

    .line 60
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 43
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 46
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->setResult(I)V

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;->finish()V

    .line 48
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
