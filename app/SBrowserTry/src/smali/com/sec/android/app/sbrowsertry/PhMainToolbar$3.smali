.class Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;
.super Ljava/lang/Object;
.source "PhMainToolbar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    .line 149
    const v0, 0x103006e

    .line 152
    .local v0, "i":I
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->access$100(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->access$100(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->dismiss()V

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    invoke-static {v1, v2}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->access$102(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    new-instance v2, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;
    invoke-static {v3}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->access$200(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)Lcom/sec/android/app/sbrowsertry/MainActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;-><init>(Lcom/sec/android/app/sbrowsertry/MainActivity;)V

    # setter for: Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    invoke-static {v1, v2}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->access$102(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->access$100(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->access$000(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    const v4, 0x7f080081

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->createPopup(Landroid/content/Context;Landroid/view/View;I)V

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->access$100(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    move-result-object v1

    invoke-virtual {v1, v5, v5}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->show(ZZ)V

    .line 160
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->access$000(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/sbrowsertry/MainActivity;->handleStateChange()V

    .line 161
    return-void
.end method
