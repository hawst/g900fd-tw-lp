.class public Lcom/sec/android/app/sbrowsertry/SBrowserMainToolbar;
.super Landroid/widget/FrameLayout;
.source "SBrowserMainToolbar.java"

# interfaces
.implements Lcom/sec/android/app/sbrowsertry/LocationBar$StateListener;


# instance fields
.field protected mLocationBar:Lcom/sec/android/app/sbrowsertry/LocationBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "paramAttributeSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method


# virtual methods
.method public doClick()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserMainToolbar;->mLocationBar:Lcom/sec/android/app/sbrowsertry/LocationBar;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->doClick()V

    .line 56
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 36
    const v0, 0x7f080084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/SBrowserMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sbrowsertry/LocationBar;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserMainToolbar;->mLocationBar:Lcom/sec/android/app/sbrowsertry/LocationBar;

    .line 41
    return-void
.end method

.method public onStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 48
    return-void
.end method
