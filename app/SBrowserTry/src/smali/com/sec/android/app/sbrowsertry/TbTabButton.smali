.class public Lcom/sec/android/app/sbrowsertry/TbTabButton;
.super Landroid/widget/LinearLayout;
.source "TbTabButton.java"


# instance fields
.field mClose:Landroid/widget/ImageView;

.field mFaviconIconView:Landroid/widget/ImageView;

.field private final mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

.field mPressed:Z

.field mSelected:Z

.field mTabContent:Landroid/view/View;

.field private final mTabWidth:I

.field mTitle:Landroid/widget/TextView;

.field tab_bg:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 46
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 47
    check-cast p1, Lcom/sec/android/app/sbrowsertry/MainActivity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    .line 48
    invoke-virtual {p0, v3}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->setWillNotDraw(Z)V

    .line 50
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 51
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f0700d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mTabWidth:I

    .line 53
    const/16 v2, 0x10

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->setGravity(I)V

    .line 54
    invoke-virtual {p0, v4}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->setOrientation(I)V

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 57
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030029

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mTabContent:Landroid/view/View;

    .line 58
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mTabContent:Landroid/view/View;

    const v3, 0x7f08007a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mTitle:Landroid/widget/TextView;

    .line 59
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mTabContent:Landroid/view/View;

    const v3, 0x7f080079

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mFaviconIconView:Landroid/widget/ImageView;

    .line 60
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mTabContent:Landroid/view/View;

    const v3, 0x7f08007b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mClose:Landroid/widget/ImageView;

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mTabContent:Landroid/view/View;

    const v3, 0x7f080077

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->tab_bg:Landroid/widget/LinearLayout;

    .line 63
    iput-boolean v4, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mSelected:Z

    .line 64
    iput-boolean v4, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mPressed:Z

    .line 65
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mSelected:Z

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->tab_bg:Landroid/widget/LinearLayout;

    const v1, 0x7f02000a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 121
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mPressed:Z

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->tab_bg:Landroid/widget/LinearLayout;

    const v1, 0x7f02000d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 124
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 125
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->tab_bg:Landroid/widget/LinearLayout;

    const v1, 0x7f02000b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 109
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 110
    return-void
.end method

.method public setActivated(Z)V
    .locals 5
    .param p1, "selected"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    iput-boolean p1, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mSelected:Z

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mFaviconIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 72
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mTitle:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mSelected:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0a0013

    :goto_0
    invoke-virtual {v3, v4, v0}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 73
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mSelected:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->setHorizontalFadingEdgeEnabled(Z)V

    .line 74
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setActivated(Z)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->updateLayoutParams()V

    .line 76
    if-nez p1, :cond_2

    :goto_2
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->setFocusable(Z)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->postInvalidate()V

    .line 78
    return-void

    .line 72
    :cond_0
    const v0, 0x7f0a0014

    goto :goto_0

    :cond_1
    move v0, v2

    .line 73
    goto :goto_1

    :cond_2
    move v1, v2

    .line 76
    goto :goto_2
.end method

.method setDisplayTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    return-void
.end method

.method setFavicon(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 98
    if-nez p1, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200a4

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 100
    .local v0, "bm":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mFaviconIconView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 104
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mFaviconIconView:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public updateFaviconIcon(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mFaviconIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 83
    return-void
.end method

.method public updateLayoutParams()V
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 87
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v1, p0, Lcom/sec/android/app/sbrowsertry/TbTabButton;->mTabWidth:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 89
    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 90
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    return-void
.end method
