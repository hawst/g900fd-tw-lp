.class public Lcom/sec/android/app/sbrowsertry/SmartFaceListener;
.super Ljava/lang/Object;
.source "SmartFaceListener.java"


# static fields
.field private static final DETECT_IN_PROGRESS:I = 0x0

.field private static final NO_DETECTION:I = 0x2

.field private static final SHAKING:I = 0x3

.field private static final SMART_FACE_DEBUG:Z = true

.field private static final SMART_FACE_ENABLE:Z = true

.field public static final SMART_SCROLL:I = 0x0

.field private static final SMART_SCROLL_ANI:I = 0x1f4

.field private static final SMART_SCROLL_DOWN:I = 0x2

.field private static final SMART_SCROLL_INIT:I = 0x5

.field private static final SMART_SCROLL_LEFT:I = 0x3

.field private static final SMART_SCROLL_MOVE:I = 0x1

.field private static final SMART_SCROLL_MOVE_STOP:I = 0x2

.field private static final SMART_SCROLL_NONE:I = 0x0

.field private static final SMART_SCROLL_RESUME:I = 0x6

.field private static final SMART_SCROLL_RIGHT:I = 0x4

.field private static final SMART_SCROLL_UP:I = 0x1

.field private static TAG:Ljava/lang/String; = null

.field private static final TRACKING:I = 0x1


# instance fields
.field private mCheckFirstShaking:Z

.field private mContext:Landroid/content/Context;

.field private mDoScrollMove:Z

.field private mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

.field private final mPrivateEventHandler:Landroid/os/Handler;

.field private mProgressStatus:I

.field private mSmartScrollDirection:I

.field private mSmartScrollOn:Z

.field private mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

.field private mSmartScrollSpeed:I

.field private mSmartScrollSpeedFastX:I

.field private mSmartScrollSpeedFastY:I

.field private mSmartScrollSpeedNormalX:I

.field private mSmartScrollSpeedNormalY:I

.field protected mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, "TrySmartFaceListener"

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/touchwiz/widget/TwHelpScrollView;Landroid/content/Context;I)V
    .locals 3
    .param p1, "scrollView"    # Lcom/sec/android/touchwiz/widget/TwHelpScrollView;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "motionType"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v0, 0xe

    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    .line 37
    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    .line 71
    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedNormalX:I

    .line 72
    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedFastX:I

    .line 73
    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedNormalY:I

    .line 74
    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedFastY:I

    .line 78
    iput v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollDirection:I

    .line 79
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeed:I

    .line 80
    iput v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mProgressStatus:I

    .line 81
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollOn:Z

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mCheckFirstShaking:Z

    .line 84
    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mContext:Landroid/content/Context;

    .line 88
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mDoScrollMove:Z

    .line 314
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener$2;-><init>(Lcom/sec/android/app/sbrowsertry/SmartFaceListener;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    .line 92
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    .line 95
    iput-object p2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mContext:Landroid/content/Context;

    .line 96
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p3}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    .line 98
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sbrowsertry/SmartFaceListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollOn:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/sbrowsertry/SmartFaceListener;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartFaceListener;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->checkSmartScrollStatus(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/sbrowsertry/SmartFaceListener;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartFaceListener;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->checkSmartScrollDirection(II)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/sbrowsertry/SmartFaceListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->getSmartScrollSpeed()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/sbrowsertry/SmartFaceListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->doSmartScroll()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/sbrowsertry/SmartFaceListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mProgressStatus:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/sbrowsertry/SmartFaceListener;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private checkSmartScrollDirection(II)V
    .locals 6
    .param p1, "deltaX"    # I
    .param p2, "deltaY"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 461
    if-eqz p2, :cond_1

    .line 462
    packed-switch p2, :pswitch_data_0

    .line 510
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 464
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedFastY:I

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->handleSmartScrolling(II)V

    goto :goto_0

    .line 468
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedNormalY:I

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->handleSmartScrolling(II)V

    goto :goto_0

    .line 472
    :pswitch_3
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedNormalY:I

    invoke-direct {p0, v3, v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->handleSmartScrolling(II)V

    goto :goto_0

    .line 475
    :pswitch_4
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedFastY:I

    invoke-direct {p0, v3, v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->handleSmartScrolling(II)V

    goto :goto_0

    .line 478
    :cond_1
    if-eqz p1, :cond_2

    .line 479
    packed-switch p1, :pswitch_data_1

    :pswitch_5
    goto :goto_0

    .line 481
    :pswitch_6
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedFastX:I

    invoke-direct {p0, v4, v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->handleSmartScrolling(II)V

    goto :goto_0

    .line 485
    :pswitch_7
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedNormalX:I

    invoke-direct {p0, v4, v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->handleSmartScrolling(II)V

    goto :goto_0

    .line 489
    :pswitch_8
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedNormalX:I

    invoke-direct {p0, v5, v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->handleSmartScrolling(II)V

    goto :goto_0

    .line 493
    :pswitch_9
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedFastX:I

    invoke-direct {p0, v5, v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->handleSmartScrolling(II)V

    goto :goto_0

    .line 498
    :cond_2
    iput-boolean v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mDoScrollMove:Z

    .line 499
    iput v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollDirection:I

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 504
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setEnableVSync(Z)V

    goto :goto_0

    .line 462
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 479
    :pswitch_data_1
    .packed-switch -0x2
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private checkSmartScrollStatus(II)Z
    .locals 5
    .param p1, "processStatus"    # I
    .param p2, "guideDirction"    # I

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 377
    const/4 v2, 0x2

    if-ne p1, v2, :cond_4

    .line 378
    iget v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mProgressStatus:I

    if-ne v2, p1, :cond_0

    .line 453
    :goto_0
    return v0

    .line 382
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mCheckFirstShaking:Z

    .line 383
    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollDirection:I

    .line 384
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v2, :cond_1

    .line 385
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v2, v0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setEnableVSync(Z)V

    .line 388
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 389
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 390
    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mDoScrollMove:Z

    .line 393
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 394
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 397
    :cond_3
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mProgressStatus:I

    goto :goto_0

    .line 399
    :cond_4
    if-nez p1, :cond_8

    .line 400
    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mCheckFirstShaking:Z

    .line 401
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v2, :cond_5

    .line 402
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v2, v0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setEnableVSync(Z)V

    .line 405
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 406
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 407
    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mDoScrollMove:Z

    .line 410
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 411
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 414
    :cond_7
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mProgressStatus:I

    goto :goto_0

    .line 416
    :cond_8
    const/4 v2, 0x3

    if-ne p1, v2, :cond_c

    .line 417
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v2, :cond_9

    .line 418
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v2, v0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setEnableVSync(Z)V

    .line 421
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 422
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 423
    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mDoScrollMove:Z

    .line 426
    :cond_a
    iget-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mCheckFirstShaking:Z

    if-eqz v1, :cond_b

    .line 427
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 428
    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mCheckFirstShaking:Z

    .line 431
    :cond_b
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mProgressStatus:I

    goto/16 :goto_0

    .line 433
    :cond_c
    if-ne p1, v1, :cond_10

    .line 434
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mCheckFirstShaking:Z

    .line 435
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mProgressStatus:I

    if-ne v0, p1, :cond_d

    move v0, v1

    .line 436
    goto/16 :goto_0

    .line 438
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 442
    :cond_e
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mProgressStatus:I

    .line 444
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->TAG:Ljava/lang/String;

    const-string v2, "TRACKING - onSmartScrollReady"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v0, :cond_f

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->smartScrollReady()V

    :cond_f
    move v0, v1

    .line 449
    goto/16 :goto_0

    .line 452
    :cond_10
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mProgressStatus:I

    goto/16 :goto_0
.end method

.method private declared-synchronized doSmartScroll()V
    .locals 6

    .prologue
    .line 517
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollOn:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_1

    .line 571
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 520
    :cond_1
    const/4 v1, 0x0

    .line 521
    .local v1, "offset":I
    :try_start_1
    iget v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeed:I

    .line 522
    .local v2, "smartScrollSpead":I
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollDirection:I

    .line 525
    .local v0, "checkDirection":I
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 526
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 529
    :cond_2
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 531
    :pswitch_0
    mul-int/lit8 v1, v2, -0x1

    .line 533
    sget-object v3, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doSmartScroll : SMART_SCROLL_UP - offset="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v3, :cond_0

    .line 536
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v3, v1}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->smartScrollBy(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 517
    .end local v0    # "checkDirection":I
    .end local v1    # "offset":I
    .end local v2    # "smartScrollSpead":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 541
    .restart local v0    # "checkDirection":I
    .restart local v1    # "offset":I
    .restart local v2    # "smartScrollSpead":I
    :pswitch_1
    move v1, v2

    .line 543
    :try_start_2
    sget-object v3, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doSmartScroll : SMART_SCROLL_DOWN - offset="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v3, :cond_0

    .line 546
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v3, v1}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->smartScrollBy(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 529
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getSmartScrollSpeed()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->getSpeed()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeedNormalY:I

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->getSpeed()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeed:I

    .line 182
    :cond_0
    return-void
.end method

.method private handleSmartScrolling(II)V
    .locals 2
    .param p1, "direction"    # I
    .param p2, "speed"    # I

    .prologue
    const/4 v1, 0x1

    .line 340
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollDirection:I

    if-ne v0, p1, :cond_1

    .line 341
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeed:I

    if-ne v0, p2, :cond_0

    .line 363
    :goto_0
    return-void

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 353
    :cond_1
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollDirection:I

    .line 354
    iput p2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSpeed:I

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v0, :cond_2

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setInitVSync()V

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setEnableVSync(Z)V

    .line 361
    :cond_2
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mDoScrollMove:Z

    goto :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollOn:Z

    if-nez v0, :cond_0

    .line 188
    invoke-direct {p0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->getSmartScrollSpeed()V

    .line 191
    invoke-static {}, Lcom/samsung/android/smartface/SmartFaceManager;->getSmartFaceManager()Lcom/samsung/android/smartface/SmartFaceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    if-nez v0, :cond_1

    .line 194
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->TAG:Ljava/lang/String;

    const-string v1, "init: mFaceManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    new-instance v1, Lcom/sec/android/app/sbrowsertry/SmartFaceListener$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener$1;-><init>(Lcom/sec/android/app/sbrowsertry/SmartFaceListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartface/SmartFaceManager;->setListener(Lcom/samsung/android/smartface/SmartFaceManager$SmartFaceInfoListener;)V

    goto :goto_0
.end method


# virtual methods
.method public delayResumeSmartScroll()V
    .locals 4

    .prologue
    const/4 v1, 0x6

    .line 574
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 577
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 578
    return-void
.end method

.method public doSmartScrollStop()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x10

    const/4 v2, 0x1

    .line 589
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollDirection:I

    .line 590
    .local v0, "checkDirection":I
    packed-switch v0, :pswitch_data_0

    .line 601
    :goto_0
    return-void

    .line 593
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 597
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 590
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public isEnableFaceView()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->checkSmartScroll()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x1

    .line 104
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pauseSmartScroll()V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->isEnableFaceView()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 270
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pauseSmartScroll this:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mFaceManager:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    iput-boolean v4, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollOn:Z

    .line 273
    iput-boolean v3, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mDoScrollMove:Z

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0, v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setEnableVSync(Z)V

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 287
    :cond_2
    iput v3, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollDirection:I

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    if-eqz v0, :cond_3

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    invoke-virtual {v0}, Lcom/samsung/android/smartface/SmartFaceManager;->pause()V

    .line 294
    :cond_3
    return-void
.end method

.method public resumeSmartScroll()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->isEnableFaceView()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resumeSmartScroll this:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mFaceManager:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    iput-boolean v3, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollOn:Z

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    if-eqz v0, :cond_1

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    invoke-virtual {v0}, Lcom/samsung/android/smartface/SmartFaceManager;->resume()V

    .line 312
    :cond_1
    return-void
.end method

.method public setStartSmartScroll()V
    .locals 3

    .prologue
    .line 582
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MOTION - setStartSmartScroll : mDoScrollMove="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mDoScrollMove:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mDoScrollMove:Z

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 586
    :cond_0
    return-void
.end method

.method public startSmartScroll()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->isEnableFaceView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startSmartScroll- this:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    invoke-direct {p0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->init()V

    .line 231
    iput-boolean v3, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollOn:Z

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    invoke-virtual {v0, v3}, Lcom/samsung/android/smartface/SmartFaceManager;->start(I)Z

    .line 236
    :cond_0
    return-void
.end method

.method public stopSmartScroll()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x5

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->isEnableFaceView()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 242
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unregisterSmartScroll this:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0, v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setEnableVSync(Z)V

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 253
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 257
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    if-eqz v0, :cond_3

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    invoke-virtual {v0, v6}, Lcom/samsung/android/smartface/SmartFaceManager;->setListener(Lcom/samsung/android/smartface/SmartFaceManager$SmartFaceInfoListener;)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    invoke-virtual {v0}, Lcom/samsung/android/smartface/SmartFaceManager;->stop()V

    .line 260
    iput-object v6, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mFaceManager:Lcom/samsung/android/smartface/SmartFaceManager;

    .line 263
    :cond_3
    iput-boolean v3, p0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->mSmartScrollOn:Z

    .line 265
    :cond_4
    return-void
.end method
