.class Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;
.super Landroid/widget/ListPopupWindow;
.source "LocationBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/LocationBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SuggestionsPopupWindow"
.end annotation


# instance fields
.field private mAnchorPosition:[I

.field private mListItemCount:I

.field private mPreviousAnchorYPosition:I

.field private mPreviousAppSize:Landroid/graphics/Rect;

.field private mSuggestionHeight:I

.field private mTempAppSize:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/LocationBar;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sbrowsertry/LocationBar;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->this$0:Lcom/sec/android/app/sbrowsertry/LocationBar;

    .line 229
    const/4 v0, 0x0

    const v1, 0x101006b

    invoke-direct {p0, p2, v0, v1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 219
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mAnchorPosition:[I

    .line 221
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mPreviousAppSize:Landroid/graphics/Rect;

    .line 222
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mTempAppSize:Landroid/graphics/Rect;

    .line 230
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070078

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mSuggestionHeight:I

    .line 232
    return-void
.end method


# virtual methods
.method public invalidateSuggestionViews()V
    .locals 3

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v2

    if-nez v2, :cond_1

    .line 294
    :cond_0
    return-void

    .line 288
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->this$0:Lcom/sec/android/app/sbrowsertry/LocationBar;

    iget-object v2, v2, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 289
    .local v1, "suggestionsList":Landroid/widget/ListView;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 291
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    .line 289
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public show()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 236
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v2

    if-nez v2, :cond_2

    .line 237
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mListItemCount:I

    .line 268
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->this$0:Lcom/sec/android/app/sbrowsertry/LocationBar;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->positionSuggestionPopup(Landroid/widget/ListPopupWindow;)V

    .line 272
    invoke-virtual {p0, v4}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->setInputMethodMode(I)V

    .line 273
    invoke-super {p0}, Landroid/widget/ListPopupWindow;->show()V

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mListItemCount:I

    .line 275
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->this$0:Lcom/sec/android/app/sbrowsertry/LocationBar;

    invoke-virtual {v2}, Lcom/sec/android/app/sbrowsertry/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mPreviousAppSize:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mAnchorPosition:[I

    invoke-virtual {v2, v3}, Landroid/view/View;->getLocationInWindow([I)V

    .line 279
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mAnchorPosition:[I

    aget v2, v2, v4

    iput v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mPreviousAnchorYPosition:I

    .line 280
    :cond_1
    :goto_0
    return-void

    .line 238
    :cond_2
    iget v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mListItemCount:I

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getCount()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 245
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->this$0:Lcom/sec/android/app/sbrowsertry/LocationBar;

    invoke-virtual {v2}, Lcom/sec/android/app/sbrowsertry/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mTempAppSize:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 247
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mAnchorPosition:[I

    invoke-virtual {v2, v3}, Landroid/view/View;->getLocationInWindow([I)V

    .line 248
    iget v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mPreviousAnchorYPosition:I

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mAnchorPosition:[I

    aget v3, v3, v4

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mTempAppSize:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mPreviousAppSize:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 258
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mTempAppSize:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mAnchorPosition:[I

    aget v3, v3, v4

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v0, v2, v3

    .line 260
    .local v0, "availableViewport":I
    iget v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mListItemCount:I

    iget v3, p0, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->mSuggestionHeight:I

    mul-int v1, v2, v3

    .line 261
    .local v1, "minimumFullListSize":I
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeight()I

    move-result v2

    if-ge v2, v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeight()I

    move-result v2

    if-lt v2, v0, :cond_0

    goto :goto_0
.end method
