.class public Lcom/sec/android/app/sbrowsertry/SmartMotionListener;
.super Ljava/lang/Object;
.source "SmartMotionListener.java"


# static fields
.field private static final SMARTMOTION_DEBUG:Z = true

.field private static final SMARTMOTION_ENABLE:Z = true

.field public static final SMART_SCROLL:I = 0x0

.field private static final SMART_SCROLL_DOWN:I = 0x2

.field private static final SMART_SCROLL_MOVE:I = 0x1

.field private static final SMART_SCROLL_NONE:I = 0x0

.field private static final SMART_SCROLL_UP:I = 0x1

.field private static TAG:Ljava/lang/String;

.field private static mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;


# instance fields
.field private mCheckFaceDetected:Z

.field private mCheckSendResetEvent:Z

.field private mCheckStartMotionListener:Z

.field private mContext:Landroid/content/Context;

.field private mDoScrollMove:Z

.field private mMotionListener:Lcom/samsung/android/motion/MRListener;

.field private final mPrivateEventHandler:Landroid/os/Handler;

.field private mSmartScrollDirection:I

.field private mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

.field private mSmartScrollSpeed:I

.field private mSmartScrollSpeedNormalY:I

.field protected mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "TrySmartMotionListener"

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;

    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/touchwiz/widget/TwHelpScrollView;Landroid/content/Context;I)V
    .locals 3
    .param p1, "scrollView"    # Lcom/sec/android/touchwiz/widget/TwHelpScrollView;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "motionType"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    .line 34
    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    .line 35
    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    .line 55
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSpeedNormalY:I

    .line 59
    iput v1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollDirection:I

    .line 60
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSpeed:I

    .line 62
    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mContext:Landroid/content/Context;

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckFaceDetected:Z

    .line 64
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckStartMotionListener:Z

    .line 65
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckSendResetEvent:Z

    .line 68
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mDoScrollMove:Z

    .line 262
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$2;-><init>(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;

    .line 72
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    .line 75
    iput-object p2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mContext:Landroid/content/Context;

    .line 76
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p3}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    .line 82
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSpeedNormalY:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->doSmartMotionScroll()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartMotionListener;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->handleSmartMotionScrolling(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckStartMotionListener:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckFaceDetected:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartMotionListener;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckFaceDetected:Z

    return p1
.end method

.method static synthetic access$502(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartMotionListener;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollDirection:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->getSmartScrollSpeed()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartMotionListener;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mDoScrollMove:Z

    return p1
.end method

.method static synthetic access$902(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartMotionListener;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckSendResetEvent:Z

    return p1
.end method

.method private declared-synchronized doSmartMotionScroll()V
    .locals 6

    .prologue
    .line 331
    monitor-enter p0

    const/4 v1, 0x0

    .line 332
    .local v1, "offset":I
    :try_start_0
    iget v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSpeed:I

    .line 333
    .local v2, "smartScrollSpead":I
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollDirection:I

    .line 335
    .local v0, "checkDirection":I
    iget v3, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollDirection:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->isEdgeScrolling(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 336
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->setSmartMotionScrollAngle(I)V

    .line 339
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 340
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 343
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 365
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    .line 345
    :pswitch_0
    mul-int/lit8 v1, v2, -0x1

    .line 347
    :try_start_1
    sget-object v3, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MOTION - doSmartMotionScroll : SMART_MOTION_SCROLL_UP - offset="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v3, :cond_2

    .line 349
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v3, v1}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->smartScrollBy(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 331
    .end local v0    # "checkDirection":I
    .end local v2    # "smartScrollSpead":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 354
    .restart local v0    # "checkDirection":I
    .restart local v2    # "smartScrollSpead":I
    :pswitch_1
    move v1, v2

    .line 356
    :try_start_2
    sget-object v3, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MOTION - doSmartMotionScroll : SMART_MOTION_SCROLL_DOWN - offset="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v3, :cond_2

    .line 358
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v3, v1}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->smartScrollBy(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 343
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getSmartScrollSpeed()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->getSpeed()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSpeedNormalY:I

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->getSpeed()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSpeed:I

    .line 105
    :cond_0
    return-void
.end method

.method private handleSmartMotionScrolling(II)V
    .locals 2
    .param p1, "direction"    # I
    .param p2, "speed"    # I

    .prologue
    const/4 v1, 0x1

    .line 278
    invoke-direct {p0, p1}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->isEdgeScrolling(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->setSmartMotionScrollAngle(I)V

    .line 282
    :cond_0
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollDirection:I

    if-ne v0, p1, :cond_2

    .line 283
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSpeed:I

    if-ne v0, p2, :cond_1

    .line 300
    :goto_0
    return-void

    .line 286
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 288
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mDoScrollMove:Z

    .line 296
    :cond_2
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollDirection:I

    .line 297
    iput p2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSpeed:I

    .line 298
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mDoScrollMove:Z

    goto :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 109
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-nez v0, :cond_0

    .line 111
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;

    const-string v1, "SmartMotionListener : init entered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->getSmartScrollSpeed()V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mContext:Landroid/content/Context;

    const-string v1, "motion_recognition"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/motion/MotionRecognitionManager;

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    .line 118
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;-><init>(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    .line 196
    :cond_0
    return-void
.end method

.method private isEdgeScrolling(I)Z
    .locals 5
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 306
    sget-object v2, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isEdgeScrolling direction:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    if-ne p1, v0, :cond_1

    .line 310
    iget-boolean v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckSendResetEvent:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v2}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->getScrollY()I

    move-result v2

    if-nez v2, :cond_0

    .line 311
    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckSendResetEvent:Z

    .line 324
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 314
    goto :goto_0

    .line 316
    :cond_1
    const/4 v2, 0x2

    if-ne p1, v2, :cond_3

    .line 317
    iget-boolean v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckSendResetEvent:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v2}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->getScrollY()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->getMaxScrollContentSize()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 318
    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckSendResetEvent:Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 321
    goto :goto_0

    :cond_3
    move v0, v1

    .line 324
    goto :goto_0
.end method


# virtual methods
.method public isEnableSmartMotionListener()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->checkSmartScroll()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const/4 v0, 0x1

    .line 89
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSmartMotionScrollAngle(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->isEnableSmartMotionListener()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSmartMotionScrollAngle this:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mMotionSensorManager:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    if-eqz v0, :cond_0

    .line 257
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/motion/MotionRecognitionManager;->setSmartMotionAngle(Lcom/samsung/android/motion/MRListener;I)V

    .line 260
    :cond_0
    return-void
.end method

.method public setStartSmartScroll()V
    .locals 3

    .prologue
    .line 369
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MOTION - setStartSmartScroll : mDoScrollMove="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mDoScrollMove:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mDoScrollMove:Z

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 373
    :cond_0
    return-void
.end method

.method public declared-synchronized startSmartMotionScroll()V
    .locals 3

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->isEnableSmartMotionListener()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerSmartMotionScroll this:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mMotionSensorManager:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setInitVSync()V

    .line 207
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckStartMotionListener:Z

    .line 209
    invoke-direct {p0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->init()V

    .line 211
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-nez v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mContext:Landroid/content/Context;

    const-string v1, "motion_recognition"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/motion/MotionRecognitionManager;

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    .line 216
    :cond_1
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    if-eqz v0, :cond_2

    .line 217
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    const/high16 v2, 0x80000

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/motion/MotionRecognitionManager;->registerListenerEvent(Lcom/samsung/android/motion/MRListener;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    :cond_2
    monitor-exit p0

    return-void

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public stopSmartMotionScroll()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 222
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->isEnableSmartMotionListener()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 224
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unregisterSmartMotionScroll this:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mMotionSensorManager:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0, v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setEnableVSync(Z)V

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 233
    iput-boolean v3, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mDoScrollMove:Z

    .line 236
    :cond_1
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    if-eqz v0, :cond_2

    .line 237
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/motion/MotionRecognitionManager;->unregisterListener(Lcom/samsung/android/motion/MRListener;)V

    .line 239
    :cond_2
    sput-object v4, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    .line 240
    iput-object v4, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    if-eqz v0, :cond_3

    .line 243
    iput-object v4, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    .line 245
    :cond_3
    iput-boolean v3, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckFaceDetected:Z

    .line 246
    iput-boolean v3, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckStartMotionListener:Z

    .line 248
    :cond_4
    return-void
.end method
