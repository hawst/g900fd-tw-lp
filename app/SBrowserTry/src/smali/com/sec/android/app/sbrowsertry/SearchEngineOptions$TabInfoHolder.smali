.class Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;
.super Ljava/lang/Object;
.source "SearchEngineOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TabInfoHolder"
.end annotation


# instance fields
.field private favIcon:Landroid/graphics/drawable/Drawable;

.field mtitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .locals 0
    .param p1, "b"    # Landroid/graphics/drawable/Drawable;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    if-eqz p1, :cond_0

    .line 54
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;->favIcon:Landroid/graphics/drawable/Drawable;

    .line 55
    :cond_0
    iput-object p2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;->mtitle:Ljava/lang/String;

    .line 56
    return-void
.end method


# virtual methods
.method public getFavIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;->favIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;->mtitle:Ljava/lang/String;

    return-object v0
.end method
