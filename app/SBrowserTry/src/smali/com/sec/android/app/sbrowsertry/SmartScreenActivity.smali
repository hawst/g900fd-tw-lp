.class public Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;
.super Landroid/app/Activity;
.source "SmartScreenActivity.java"


# static fields
.field private static final GUIDE_SCROLL_SENSITIVITY_STATE:I = 0x15

.field private static final GUIDE_SMART_ROTATION_STATE:I = 0x2

.field private static final GUIDE_SMART_SCROLL_STATE_1:I = 0xb

.field private static final GUIDE_SMART_SCROLL_STATE_2:I = 0xc

.field private static final GUIDE_SMART_SCROLL_STATE_3:I = 0xd

.field private static final GUIDE_SMART_STAY_STATE:I = 0x1

.field private static final SCROLL_DEBUG:Z = true

.field private static final SMART_SCROLL_FACE:I = 0x1

.field private static final SMART_SCROLL_MOVE:I = 0x2

.field private static final SMART_SCROLL_NONE:I = 0x0

.field private static final SMART_SCROLL_READY:I = 0x1

.field private static final SMART_SCROLL_TILT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SmartScrollTry"


# instance fields
.field private FACE_DETECT_NOTI_DELAY:I

.field private actionBar:Landroid/app/ActionBar;

.field private guideTextView:Landroid/view/View;

.field private mCurrentGuide:I

.field private mFaceRecogRunnable:Ljava/lang/Runnable;

.field private mHandler:Landroid/os/Handler;

.field protected mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

.field protected mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

.field private mScrollMode:I

.field private mScrollReady:I

.field private mScrollViewListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

.field private mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

.field protected mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

.field private negative:Landroid/widget/ImageButton;

.field private positive:Landroid/widget/ImageButton;

.field private seekBar:Landroid/widget/SeekBar;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->negative:Landroid/widget/ImageButton;

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->positive:Landroid/widget/ImageButton;

    .line 57
    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollReady:I

    .line 58
    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 63
    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I

    .line 64
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->FACE_DETECT_NOTI_DELAY:I

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHandler:Landroid/os/Handler;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mFaceRecogRunnable:Ljava/lang/Runnable;

    .line 69
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;-><init>(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollViewListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollReady:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollReady:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mFaceRecogRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->FACE_DETECT_NOTI_DELAY:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->seekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method public static isTablet(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 264
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected getMaxScrollOffset()I
    .locals 8

    .prologue
    .line 200
    const/4 v2, 0x0

    .line 201
    .local v2, "mScrollMaxOffset":I
    const v5, 0x7f08003f

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    .line 202
    .local v0, "mRscImageHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Display;->getHeight()I

    move-result v1

    .line 205
    .local v1, "mScreenHeight":I
    const-string v5, "SmartScrollTry"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SCROLL - getMaxScrollOffset : mRscImageHeight="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mScreenHeight="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget v5, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I

    const/16 v6, 0x15

    if-ne v5, v6, :cond_1

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 211
    .local v4, "res":Landroid/content/res/Resources;
    const v5, 0x7f070097

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v3, v5

    .line 213
    .local v3, "mSeekbarHeight":I
    add-int/lit8 v5, v1, -0x4b

    sub-int/2addr v5, v3

    if-le v0, v5, :cond_0

    .line 215
    add-int/lit8 v5, v1, -0x4b

    sub-int/2addr v5, v3

    sub-int v2, v0, v5

    .line 226
    .end local v3    # "mSeekbarHeight":I
    .end local v4    # "res":Landroid/content/res/Resources;
    :cond_0
    :goto_0
    return v2

    .line 220
    :cond_1
    add-int/lit8 v5, v1, -0x4b

    if-le v0, v5, :cond_0

    .line 222
    add-int/lit8 v5, v1, -0x4b

    sub-int v2, v0, v5

    goto :goto_0
.end method

.method public getSmartScrollMode()I
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->checkSmartScrollMode()I

    move-result v0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 11
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 499
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 501
    iget v9, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I

    sparse-switch v9, :sswitch_data_0

    .line 554
    :goto_0
    return-void

    .line 505
    :sswitch_0
    const v9, 0x7f08003d

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 506
    .local v8, "rotStayView":Landroid/widget/ImageView;
    if-eqz v8, :cond_0

    .line 507
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 509
    :cond_0
    const v9, 0x7f08003d

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 511
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 512
    .local v4, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v9, 0x1

    iput-boolean v9, v4, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 513
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020001

    invoke-static {v9, v10, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 514
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const v9, 0x7f08003d

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 520
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v4    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v8    # "rotStayView":Landroid/widget/ImageView;
    :sswitch_1
    const v9, 0x7f08003f

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 521
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 522
    .local v6, "options2":Landroid/graphics/BitmapFactory$Options;
    const/4 v9, 0x1

    iput-boolean v9, v6, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 523
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020001

    invoke-static {v9, v10, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 524
    .local v2, "bitmap_scroll1":Landroid/graphics/Bitmap;
    const v9, 0x7f08003f

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    invoke-virtual {v9, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 528
    .end local v2    # "bitmap_scroll1":Landroid/graphics/Bitmap;
    .end local v6    # "options2":Landroid/graphics/BitmapFactory$Options;
    :sswitch_2
    const v9, 0x7f08003f

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 529
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 530
    .local v7, "options3":Landroid/graphics/BitmapFactory$Options;
    const/4 v9, 0x1

    iput-boolean v9, v7, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 531
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020001

    invoke-static {v9, v10, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 532
    .local v3, "bitmap_scroll2":Landroid/graphics/Bitmap;
    const v9, 0x7f08003f

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    invoke-virtual {v9, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 533
    iget v9, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_1

    .line 535
    iget-object v9, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    const v10, 0x7f08004d

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 536
    iget-object v9, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    const v10, 0x7f08004d

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const v10, 0x7f020040

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 540
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    const v10, 0x7f08004d

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 541
    iget-object v9, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    const v10, 0x7f08004d

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const v10, 0x7f02009f

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 546
    .end local v3    # "bitmap_scroll2":Landroid/graphics/Bitmap;
    .end local v7    # "options3":Landroid/graphics/BitmapFactory$Options;
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->showSensitivityText()V

    .line 547
    const v9, 0x7f08003f

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 548
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 549
    .local v5, "options1":Landroid/graphics/BitmapFactory$Options;
    const/4 v9, 0x1

    iput-boolean v9, v5, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 550
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020001

    invoke-static {v9, v10, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 551
    .local v1, "bitmap_air_scroll":Landroid/graphics/Bitmap;
    const v9, 0x7f08003f

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    invoke-virtual {v9, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 501
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0xb -> :sswitch_1
        0xc -> :sswitch_2
        0xd -> :sswitch_1
        0x15 -> :sswitch_3
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const v7, 0x7f03001f

    const v5, 0x7f03001e

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 270
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 272
    new-instance v2, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    .line 274
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHandler:Landroid/os/Handler;

    .line 275
    new-instance v2, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$2;-><init>(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)V

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mFaceRecogRunnable:Ljava/lang/Runnable;

    .line 303
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 305
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->actionBar:Landroid/app/ActionBar;

    .line 306
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->hide()V

    .line 309
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 310
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 312
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.sec.android.app.sbrowsertry.GUIDE_SMART_STAY"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 314
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->setContentView(I)V

    .line 315
    const v2, 0x7f08003d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 317
    iput v6, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I

    .line 319
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->showDialog(Landroid/view/View;Z)V

    .line 322
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mFaceRecogRunnable:Ljava/lang/Runnable;

    iget v4, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->FACE_DETECT_NOTI_DELAY:I

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 440
    :cond_1
    :goto_0
    return-void

    .line 324
    :cond_2
    const-string v2, "com.sec.android.app.sbrowsertry.GUIDE_SMART_ROTATION"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 326
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->setContentView(I)V

    .line 327
    const v2, 0x7f08003d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 329
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    .line 332
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->showDialog(Landroid/view/View;Z)V

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mFaceRecogRunnable:Ljava/lang/Runnable;

    iget v4, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->FACE_DETECT_NOTI_DELAY:I

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 337
    :cond_3
    const-string v2, "com.sec.android.app.sbrowsertry.GUIDE_SMART_SCROLL_FACE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 339
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->setContentView(I)V

    .line 341
    const/16 v2, 0xb

    iput v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I

    .line 344
    const v2, 0x7f08003f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 345
    const v2, 0x7f08003e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    .line 349
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->showDialog(Landroid/view/View;Z)V

    .line 352
    iput v6, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I

    .line 353
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollViewListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setScrollLintener(Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;)V

    .line 354
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    iget v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setSmartScroll(I)V

    goto :goto_0

    .line 356
    :cond_4
    const-string v2, "com.sec.android.app.sbrowsertry.GUIDE_SMART_SCROLL_TILT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 358
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->setContentView(I)V

    .line 360
    const/16 v2, 0xb

    iput v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I

    .line 363
    const v2, 0x7f08003f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 364
    const v2, 0x7f08003e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    .line 367
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    .line 368
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->showDialog(Landroid/view/View;Z)V

    .line 371
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I

    .line 372
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollViewListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setScrollLintener(Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;)V

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    iget v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setSmartScroll(I)V

    goto/16 :goto_0

    .line 375
    :cond_5
    const-string v2, "com.sec.android.app.sbrowsertry.SETTING_SCROLL_SENSITIVITY"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 377
    const v2, 0x7f030021

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->setContentView(I)V

    .line 379
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->showSensitivityText()V

    .line 380
    const/16 v2, 0x15

    iput v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I

    .line 382
    const v2, 0x7f080048

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->seekBar:Landroid/widget/SeekBar;

    .line 383
    const v2, 0x7f080047

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->negative:Landroid/widget/ImageButton;

    .line 384
    const v2, 0x7f080049

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->positive:Landroid/widget/ImageButton;

    .line 385
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->negative:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$3;-><init>(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 394
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->positive:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$4;-><init>(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 403
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->seekBar:Landroid/widget/SeekBar;

    const/16 v3, 0x1d

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 405
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "smart_scroll_sensitivity"

    const/16 v5, 0xf

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 406
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->seekBar:Landroid/widget/SeekBar;

    new-instance v3, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$5;-><init>(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 424
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getSmartScrollMode()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I

    .line 426
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090039

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 427
    const v2, 0x7f08003e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    .line 428
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollViewListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setScrollLintener(Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;)V

    .line 429
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    iget v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setSmartScroll(I)V

    .line 431
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 433
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 434
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 435
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->actionBar:Landroid/app/ActionBar;

    const v3, 0x7f090078

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(I)V

    .line 436
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->actionBar:Landroid/app/ActionBar;

    const v3, 0x7f020039

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setIcon(I)V

    .line 437
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->show()V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 460
    const-string v3, "SmartScrollTry"

    const-string v4, "SCROLL - onDestroy() "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mFaceRecogRunnable:Ljava/lang/Runnable;

    if-eqz v3, :cond_0

    .line 463
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mFaceRecogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 464
    iput-object v5, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHandler:Landroid/os/Handler;

    .line 467
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v3, :cond_1

    .line 468
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v3}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 469
    iput-object v5, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 472
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v3, :cond_2

    .line 473
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v3}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 474
    iput-object v5, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 477
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v3, :cond_3

    .line 478
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->stopSmartScroll()V

    .line 479
    iput-object v5, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mVerticalScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    .line 483
    :cond_3
    const v3, 0x7f08003f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 484
    .local v2, "scrollTryView":Landroid/view/View;
    if-eqz v2, :cond_4

    .line 485
    invoke-virtual {v2}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 486
    .local v0, "bitmap_air_scroll":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_4

    .line 487
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 490
    .end local v0    # "bitmap_air_scroll":Landroid/graphics/Bitmap;
    :cond_4
    const v3, 0x7f08003d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 491
    .local v1, "rotStayView":Landroid/widget/ImageView;
    if-eqz v1, :cond_5

    .line 492
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 493
    :cond_5
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 494
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 445
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 450
    const/4 v0, 0x0

    .line 453
    :goto_0
    return v0

    .line 447
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->finish()V

    .line 453
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 445
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public setSensitivity(I)V
    .locals 2
    .param p1, "sensitivity"    # I

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "smart_scroll_sensitivity"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 195
    return-void
.end method

.method protected showDialog(Landroid/view/View;Z)V
    .locals 3
    .param p1, "guide_view"    # Landroid/view/View;
    .param p2, "secondDialog"    # Z

    .prologue
    const/4 v2, 0x1

    .line 157
    if-eqz p2, :cond_1

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 163
    :cond_0
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setContentView(Landroid/view/View;)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    sget-object v1, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setTouchTransparencyMode(Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setShowWrongInputToast(Z)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->show()V

    .line 190
    :goto_0
    return-void

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_2

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 177
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_3

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog2:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 181
    :cond_3
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setContentView(Landroid/view/View;)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    sget-object v1, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setTouchTransparencyMode(Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setShowWrongInputToast(Z)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->show()V

    goto :goto_0
.end method

.method protected showSensitivityText()V
    .locals 10

    .prologue
    const v9, 0x7f080042

    const/4 v8, 0x3

    const/4 v7, 0x1

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Display;->getOrientation()I

    move-result v2

    .line 231
    .local v2, "orien":I
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->isTablet(Landroid/content/Context;)Z

    move-result v0

    .line 232
    .local v0, "isTablet":Z
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 234
    .local v4, "res":Landroid/content/res/Resources;
    const/4 v1, 0x0

    .line 235
    .local v1, "linearMarginRight":I
    if-eqz v0, :cond_4

    .line 236
    if-eq v2, v7, :cond_0

    if-ne v2, v8, :cond_3

    .line 237
    :cond_0
    const v6, 0x7f0700dd

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v1, v6

    .line 245
    :goto_0
    const v6, 0x7f080043

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 246
    .local v5, "view":Landroid/widget/LinearLayout;
    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 247
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v1, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 248
    iput v1, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 249
    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 251
    if-nez v0, :cond_2

    .line 252
    if-eq v2, v7, :cond_1

    if-ne v2, v8, :cond_5

    .line 254
    :cond_1
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 261
    :cond_2
    :goto_1
    return-void

    .line 239
    .end local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v5    # "view":Landroid/widget/LinearLayout;
    :cond_3
    const v6, 0x7f0700dc

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v1, v6

    goto :goto_0

    .line 242
    :cond_4
    const v6, 0x7f070099

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v1, v6

    goto :goto_0

    .line 258
    .restart local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v5    # "view":Landroid/widget/LinearLayout;
    :cond_5
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
