.class public Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;
.super Landroid/app/Activity;
.source "QuickLaunchActivity.java"


# instance fields
.field private mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 70
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const v1, 0x7f03000a

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setContentView(I)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    sget-object v1, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setTouchTransparencyMode(Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setShowWrongInputToast(Z)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->show()V

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->invalidateOptionsMenu()V

    .line 78
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x1

    .line 19
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->setContentView(I)V

    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 23
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 24
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 26
    :cond_0
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const v1, 0x7f03000a

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setContentView(I)V

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    sget-object v1, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setTouchTransparencyMode(Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;)V

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setShowWrongInputToast(Z)V

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->show()V

    .line 32
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 48
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 35
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 40
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->setResult(I)V

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->finish()V

    .line 43
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 53
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0800ac

    if-ne v0, v1, :cond_1

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 57
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->setResult(I)V

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->finish()V

    .line 62
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/QuickLaunchActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->showWrongInputToast()V

    goto :goto_0
.end method
