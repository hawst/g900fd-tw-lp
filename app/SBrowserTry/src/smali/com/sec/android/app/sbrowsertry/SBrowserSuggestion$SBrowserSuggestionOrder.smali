.class public final enum Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;
.super Ljava/lang/Enum;
.source "SBrowserSuggestion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SBrowserSuggestionOrder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

.field public static final enum BOOKMARK:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

.field public static final enum DIRECT:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

.field public static final enum HISTORY:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

.field public static final enum SCRAP:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;


# instance fields
.field private final mOrder:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 155
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    const-string v1, "BOOKMARK"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->BOOKMARK:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    const-string v1, "SCRAP"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->SCRAP:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    const-string v1, "HISTORY"

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->HISTORY:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    const-string v1, "DIRECT"

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->DIRECT:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    .line 154
    new-array v0, v6, [Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    sget-object v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->BOOKMARK:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->SCRAP:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->HISTORY:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->DIRECT:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->$VALUES:[Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "order"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 157
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 158
    iput p3, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->mOrder:I

    .line 159
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 154
    const-class v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;
    .locals 1

    .prologue
    .line 154
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->$VALUES:[Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    invoke-virtual {v0}, [Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    return-object v0
.end method


# virtual methods
.method public getOrder()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->mOrder:I

    return v0
.end method
