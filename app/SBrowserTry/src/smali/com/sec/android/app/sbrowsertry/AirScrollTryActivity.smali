.class public Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;
.super Landroid/app/Activity;
.source "AirScrollTryActivity.java"


# static fields
.field private static final SCROLL_DURATION:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "AirScrollTryActivity"


# instance fields
.field private mAirMotionForAirScroll:Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

.field private mAirScrollListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field private mImageView:Landroid/widget/ImageView;

.field private mLinearLayout:Landroid/widget/LinearLayout;

.field private mPopup:Landroid/widget/TextView;

.field private mScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

.field mViewY:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mPopup:Landroid/widget/TextView;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mViewY:I

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirMotionForAirScroll:Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    .line 50
    new-instance v0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;-><init>(Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirScrollListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;)Lcom/sec/android/touchwiz/widget/TwHelpScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 12
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v11, 0x0

    const v10, 0x7f020001

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 123
    const-string v5, "AirScrollTryActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onConfigurationChanged  newConfig = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 125
    iget v5, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 126
    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    .line 127
    .local v4, "rotStayView":Landroid/widget/ImageView;
    if-eqz v4, :cond_0

    .line 128
    invoke-virtual {v4, v11}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 129
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    if-eqz v5, :cond_1

    .line 130
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 131
    :cond_1
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 132
    .local v2, "options1":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v9, v2, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, v10, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 134
    .local v0, "bitmap1":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    if-eqz v5, :cond_2

    .line 135
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 136
    :cond_2
    invoke-virtual {p0, v8}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->setRequestedOrientation(I)V

    .line 150
    .end local v0    # "bitmap1":Landroid/graphics/Bitmap;
    .end local v2    # "options1":Landroid/graphics/BitmapFactory$Options;
    :goto_0
    return-void

    .line 138
    .end local v4    # "rotStayView":Landroid/widget/ImageView;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    .line 139
    .restart local v4    # "rotStayView":Landroid/widget/ImageView;
    if-eqz v4, :cond_4

    .line 140
    invoke-virtual {v4, v11}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 141
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    if-eqz v5, :cond_5

    .line 142
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 143
    :cond_5
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 144
    .local v3, "options2":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v9, v3, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, v10, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 146
    .local v1, "bitmap2":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    if-eqz v5, :cond_6

    .line 147
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 148
    :cond_6
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 81
    const-string v1, "AirScrollTryActivity"

    const-string v2, "Create AirScroll Tutorial"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    const v1, 0x7f030024

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->setContentView(I)V

    .line 93
    const v1, 0x7f080050

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    .line 95
    const v1, 0x7f080052

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x7f040000

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mBubbleAnimation:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x2bc

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mLinearLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 100
    const v1, 0x7f080051

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    .line 101
    const v1, 0x7f080053

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mPopup:Landroid/widget/TextView;

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mPopup:Landroid/widget/TextView;

    const v2, 0x7f09007b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 106
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mViewY:I

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirMotionForAirScroll:Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    if-eqz v1, :cond_0

    .line 110
    const-string v1, "AirScrollTryActivity"

    const-string v2, " AirMotionLintener register in showSelectActionBar"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirMotionForAirScroll:Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirScrollListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->setAirMotionLintener(Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;I)V

    .line 119
    :goto_0
    return-void

    .line 115
    :cond_0
    new-instance v1, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirMotionForAirScroll:Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirMotionForAirScroll:Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirScrollListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->setAirMotionLintener(Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirMotionForAirScroll:Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    const/4 v2, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->setAirMotionLintener(Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;I)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mImageView:Landroid/widget/ImageView;

    .line 158
    .local v0, "rotStayView":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 159
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 160
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 161
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirMotionForAirScroll:Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->setAirMotionLintener(Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;I)V

    .line 167
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 168
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirMotionForAirScroll:Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    if-eqz v0, :cond_0

    .line 175
    const-string v0, "AirScrollTryActivity"

    const-string v1, " AirMotionLintener register in showSelectActionBar"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirMotionForAirScroll:Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirScrollListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->setAirMotionLintener(Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;I)V

    .line 183
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 185
    return-void

    .line 180
    :cond_0
    new-instance v0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirMotionForAirScroll:Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirMotionForAirScroll:Lcom/sec/android/app/sbrowsertry/AirMotionDetector;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mAirScrollListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->setAirMotionLintener(Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;I)V

    goto :goto_0
.end method
