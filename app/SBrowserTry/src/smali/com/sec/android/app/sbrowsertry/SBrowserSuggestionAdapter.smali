.class public Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;
.super Landroid/widget/BaseAdapter;
.source "SBrowserSuggestionAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$3;,
        Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;,
        Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionSelectionHandler;,
        Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mLocationBar:Lcom/sec/android/app/sbrowsertry/LocationBar;

.field private final mSuggestionItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSuggestionSelectionHandler:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionSelectionHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/sbrowsertry/LocationBar;Ljava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "locationbar"    # Lcom/sec/android/app/sbrowsertry/LocationBar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/app/sbrowsertry/LocationBar;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p3, "suggestionlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->mContext:Landroid/content/Context;

    .line 73
    iput-object p3, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->mSuggestionItems:Ljava/util/List;

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;)Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionSelectionHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->mSuggestionSelectionHandler:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionSelectionHandler;

    return-object v0
.end method

.method private bindView(Landroid/view/View;Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "item"    # Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;

    .prologue
    .line 103
    const/4 v5, 0x0

    .line 104
    .local v5, "setRefineDrawable":Z
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 105
    const v9, 0x1020014

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 106
    .local v7, "tv1":Landroid/widget/TextView;
    const v9, 0x1020015

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 107
    .local v8, "tv2":Landroid/widget/TextView;
    const v9, 0x7f08009a

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 108
    .local v1, "ic1":Landroid/widget/ImageView;
    const v9, 0x7f08009c

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 109
    .local v2, "ic2":Landroid/view/View;
    const v9, 0x7f08009b

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 110
    .local v0, "div":Landroid/view/View;
    invoke-virtual {p2}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;->getSuggestion()Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;

    move-result-object v6

    .line 111
    .local v6, "suggestionItem":Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;
    invoke-virtual {v6}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    invoke-virtual {v6}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->getType()Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->SEARCH_WHAT_YOU_TYPED:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    if-ne v9, v10, :cond_3

    invoke-virtual {v6}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->getsuggestionUrl()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 114
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    invoke-virtual {v6}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->getsuggestionUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 128
    :goto_0
    const/4 v4, -0x1

    .line 129
    .local v4, "id":I
    invoke-direct {p0, v6}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->getSuggestionIconType(Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;)Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    move-result-object v3

    .line 130
    .local v3, "iconType":Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;
    sget-object v9, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$3;->$SwitchMap$com$sec$android$app$sbrowsertry$SBrowserSuggestionAdapter$SuggestionIconType:[I

    invoke-virtual {v3}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 156
    const v4, 0x7f020023

    .line 159
    :goto_1
    sget-object v9, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->TAG:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    if-eq v3, v9, :cond_0

    sget-object v9, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->BOOKMARK:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    if-eq v3, v9, :cond_0

    sget-object v9, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->OPERATOR:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    if-eq v3, v9, :cond_0

    sget-object v9, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->SCRAP:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    if-eq v3, v9, :cond_0

    sget-object v9, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->HISTORY:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    if-ne v3, v9, :cond_1

    .line 164
    :cond_0
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 165
    invoke-virtual {v6}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->getsuggestionUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 169
    :cond_1
    const/4 v9, -0x1

    if-eq v4, v9, :cond_2

    .line 170
    iget-object v9, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 172
    :cond_2
    if-eqz v5, :cond_4

    .line 173
    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    .line 176
    :goto_2
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v9

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 177
    new-instance v9, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$1;

    invoke-direct {v9, p0}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$1;-><init>(Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;)V

    invoke-virtual {v2, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    const v9, 0x7f080099

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    new-instance v10, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$2;

    invoke-direct {v10, p0}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$2;-><init>(Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;)V

    invoke-virtual {v9, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    return-void

    .line 118
    .end local v3    # "iconType":Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;
    .end local v4    # "id":I
    :cond_3
    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    const/4 v9, 0x2

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setMaxLines(I)V

    goto :goto_0

    .line 132
    .restart local v3    # "iconType":Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;
    .restart local v4    # "id":I
    :pswitch_0
    const v4, 0x7f020021

    .line 133
    const/4 v5, 0x1

    .line 134
    goto :goto_1

    .line 139
    :pswitch_1
    const v4, 0x7f02001f

    .line 140
    goto :goto_1

    .line 142
    :pswitch_2
    const v4, 0x7f020020

    .line 143
    goto :goto_1

    .line 145
    :pswitch_3
    const v4, 0x7f020021

    .line 146
    const/4 v5, 0x1

    .line 147
    goto :goto_1

    .line 150
    :pswitch_4
    const v4, 0x7f020022

    .line 151
    goto :goto_1

    .line 153
    :pswitch_5
    const v4, 0x7f02001f

    .line 154
    goto :goto_1

    .line 175
    :cond_4
    const/16 v9, 0x8

    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 130
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private getSuggestionIconType(Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;)Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;
    .locals 2
    .param p1, "suggestionItem"    # Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;

    .prologue
    .line 193
    invoke-virtual {p1}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->getType()Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->nativeType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 225
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->DEFAULT:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    :goto_0
    return-object v0

    .line 203
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->isStarred()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->BOOKMARK:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    goto :goto_0

    .line 205
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->getType()Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->HISTORY_URL:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    if-ne v0, v1, :cond_1

    .line 206
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->HISTORY:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    goto :goto_0

    .line 208
    :cond_1
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->GLOBE:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    goto :goto_0

    .line 214
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->getType()Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->VOICE_SUGGEST:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    if-eq v0, v1, :cond_2

    .line 215
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->MAGNIFIER:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    goto :goto_0

    .line 217
    :cond_2
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->VOICE:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    goto :goto_0

    .line 221
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->SCRAP:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    goto :goto_0

    .line 223
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->OPERATOR:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    goto :goto_0

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 85
    int-to-long v0, p1

    return-wide v0
.end method

.method public getSuggestionSelectionHandler()Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionSelectionHandler;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->mSuggestionSelectionHandler:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionSelectionHandler;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 91
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 92
    .local v0, "inflater":Landroid/view/LayoutInflater;
    move-object v2, p2

    .line 93
    .local v2, "view":Landroid/view/View;
    if-nez v2, :cond_0

    .line 94
    const v3, 0x7f03002c

    const/4 v4, 0x0

    invoke-virtual {v0, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 96
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;

    .line 97
    .local v1, "item":Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;
    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->bindView(Landroid/view/View;Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;)V

    .line 98
    return-object v2
.end method

.method public notifySuggestionsChanged()V
    .locals 0

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->notifyDataSetChanged()V

    .line 231
    return-void
.end method

.method public setSuggestionSelectionHandler(Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionSelectionHandler;)V
    .locals 0
    .param p1, "suggestionselectionhandler"    # Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionSelectionHandler;

    .prologue
    .line 235
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;->mSuggestionSelectionHandler:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionSelectionHandler;

    .line 236
    return-void
.end method
