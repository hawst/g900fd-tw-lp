.class public final Lcom/sec/android/app/sbrowsertry/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final active_tab_width:I = 0x7f0700bd

.field public static final add_bookmark_account_name_height:I = 0x7f070000

.field public static final add_bookmark_from_bookmarks_page_title:I = 0x7f070001

.field public static final add_bookmark_from_bookmarks_page_url:I = 0x7f070002

.field public static final add_bookmark_from_bookmarks_row2_margin_top:I = 0x7f0700be

.field public static final add_bookmark_from_bookmarks_title_margin_left:I = 0x7f0700bf

.field public static final add_bookmark_header_account_name_text_size:I = 0x7f070003

.field public static final add_bookmark_header_folder_icon_size:I = 0x7f070004

.field public static final add_bookmark_header_folder_layout_height:I = 0x7f070005

.field public static final add_bookmark_header_folder_name_text_padding_left:I = 0x7f070006

.field public static final add_bookmark_header_folder_name_text_size:I = 0x7f070007

.field public static final add_bookmark_header_icon_margin_vertical:I = 0x7f070008

.field public static final add_bookmark_header_icon_size:I = 0x7f070009

.field public static final add_bookmark_header_text_layout_margin_vertical:I = 0x7f07000a

.field public static final add_bookmark_header_text_layout_padding_left:I = 0x7f07000b

.field public static final add_bookmark_more_padding_start:I = 0x7f07000c

.field public static final add_bookmark_width:I = 0x7f0700c0

.field public static final bookmark_icon_height:I = 0x7f07000d

.field public static final bookmark_icon_paddingBottom:I = 0x7f07000e

.field public static final bookmark_icon_paddingleft:I = 0x7f07000f

.field public static final bookmark_icon_paddingright:I = 0x7f070010

.field public static final bookmark_icon_paddingtop:I = 0x7f070011

.field public static final bookmark_icon_width:I = 0x7f070012

.field public static final create_new_tab_tooltip_margin_top:I = 0x7f070013

.field public static final easymode_toolbar_button_height:I = 0x7f070014

.field public static final easymode_toolbar_button_width:I = 0x7f070015

.field public static final guide_bookmark_help_tap_height:I = 0x7f070016

.field public static final guide_bookmark_help_tap_step_12_margin_right:I = 0x7f070017

.field public static final guide_bookmark_help_tap_step_1_margin_left:I = 0x7f070018

.field public static final guide_bookmark_help_tap_step_1_margin_left_reverse:I = 0x7f070019

.field public static final guide_bookmark_help_tap_step_1_margin_right:I = 0x7f07001a

.field public static final guide_bookmark_help_tap_step_1_margin_top:I = 0x7f07001b

.field public static final guide_bookmark_help_tap_step_1_margin_top_att:I = 0x7f07001c

.field public static final guide_bookmark_help_tap_step_2_margin_right:I = 0x7f07001d

.field public static final guide_bookmark_help_tap_step_2_margin_right_reverse:I = 0x7f0700c1

.field public static final guide_bookmark_help_tap_step_2_margin_top:I = 0x7f07001e

.field public static final guide_bookmark_help_tap_step_2_margin_top_reverse:I = 0x7f0700c2

.field public static final guide_bookmark_help_tap_step_4_margin_left:I = 0x7f07001f

.field public static final guide_bookmark_help_tap_width:I = 0x7f070020

.field public static final guide_bookmark_initial_margin_left:I = 0x7f070021

.field public static final guide_bookmark_initial_margin_left_reverse:I = 0x7f070022

.field public static final guide_bookmark_initial_margin_right:I = 0x7f070023

.field public static final guide_bookmark_initial_margin_top:I = 0x7f070024

.field public static final guide_bookmark_initial_margin_top_reverse:I = 0x7f070025

.field public static final guide_bookmark_picker_height:I = 0x7f070026

.field public static final guide_bookmark_picker_step_1_margin_left:I = 0x7f070027

.field public static final guide_bookmark_picker_step_1_margin_left_reverse:I = 0x7f070028

.field public static final guide_bookmark_picker_step_1_margin_right:I = 0x7f0700c3

.field public static final guide_bookmark_picker_step_1_margin_top:I = 0x7f0700b9

.field public static final guide_bookmark_picker_step_2_margin_right:I = 0x7f070029

.field public static final guide_bookmark_picker_step_2_margin_right_reverse:I = 0x7f0700c4

.field public static final guide_bookmark_picker_width:I = 0x7f07002a

.field public static final guide_bookmark_punch_step_1_margin_left:I = 0x7f07002b

.field public static final guide_bookmark_punch_step_1_margin_left_reverse:I = 0x7f07002c

.field public static final guide_bookmark_punch_step_1_margin_top:I = 0x7f07002d

.field public static final guide_bookmark_punch_step_1_margin_top_att:I = 0x7f07002e

.field public static final guide_bookmark_punch_step_1_width:I = 0x7f07002f

.field public static final guide_bookmark_punch_step_2_width:I = 0x7f070030

.field public static final guide_bookmark_save_picker_step_12_margin_right:I = 0x7f070031

.field public static final guide_bookmark_save_picker_step_2_margin_right:I = 0x7f070032

.field public static final guide_bookmark_summary_step_1_margin_left:I = 0x7f0700c5

.field public static final guide_bookmark_summary_step_2_margin_right:I = 0x7f0700c6

.field public static final guide_bookmark_summary_step_2_margin_right_reverse:I = 0x7f0700c7

.field public static final guide_bubble_summary_step_1_margin_left:I = 0x7f070033

.field public static final guide_bubble_summary_step_1_margin_left_reverse:I = 0x7f070034

.field public static final guide_bubble_summary_step_1_margin_top:I = 0x7f070035

.field public static final guide_bubble_summary_step_3_margin_bottom:I = 0x7f070036

.field public static final guide_complete_bubbletext_margin_bottom:I = 0x7f070037

.field public static final guide_head_icon_picker_margin_top:I = 0x7f070038

.field public static final guide_helptap_height:I = 0x7f070039

.field public static final guide_helptap_width:I = 0x7f07003a

.field public static final guide_icon_text_margin_left:I = 0x7f0700ba

.field public static final guide_icon_text_margin_right:I = 0x7f0700bb

.field public static final guide_magnifier_PunchView_margin_Top:I = 0x7f07003b

.field public static final guide_magnifier_PunchView_margin_left:I = 0x7f07003c

.field public static final guide_magnifier_bubble_margin_left:I = 0x7f07003d

.field public static final guide_magnifier_popup_height:I = 0x7f07003e

.field public static final guide_magnifier_popup_margin_Bottom:I = 0x7f07003f

.field public static final guide_magnifier_popup_margin_left:I = 0x7f070040

.field public static final guide_magnifier_popup_width:I = 0x7f070041

.field public static final guide_magnifier_tip_picker_margin_left:I = 0x7f070042

.field public static final guide_magnifier_tip_picker_margin_top:I = 0x7f070043

.field public static final guide_newtab_PunchView_step_1_margin_left:I = 0x7f0700c8

.field public static final guide_newtab_PunchView_step_1_margin_right:I = 0x7f070044

.field public static final guide_newtab_PunchView_step_1_margin_top:I = 0x7f070045

.field public static final guide_newtab_tipbubble_step_1_margin_left:I = 0x7f0700c9

.field public static final guide_newtab_tipbubble_step_1_margin_right:I = 0x7f070046

.field public static final guide_punch_height:I = 0x7f070047

.field public static final guide_punch_width:I = 0x7f070048

.field public static final guide_quick_access_summary_side_margin_right:I = 0x7f070049

.field public static final guide_quick_access_tipbubble_step_1_margin_right:I = 0x7f07004a

.field public static final guide_reader_help_tap_step_2_margin_top:I = 0x7f07004b

.field public static final guide_reader_punch_step_2_height:I = 0x7f07004c

.field public static final guide_reader_punch_step_2_margin_top:I = 0x7f07004d

.field public static final guide_reading_PunchView_step_1_margin_right:I = 0x7f07004e

.field public static final guide_readinglist_tipbubble_step_1_margin_right:I = 0x7f07004f

.field public static final guide_searchEngine_help_tap_step_2_margin_left:I = 0x7f070050

.field public static final guide_searchEngine_help_tap_step_2_margin_top:I = 0x7f070051

.field public static final guide_searchEngine_help_tap_step_3_margin_left:I = 0x7f070052

.field public static final guide_searchEngine_help_tap_step_3_margin_top:I = 0x7f070053

.field public static final guide_searchEngine_picker_step_2_margin_left:I = 0x7f070054

.field public static final guide_searchEngine_picker_step_3_margin_left:I = 0x7f070055

.field public static final guide_searchEngine_punch_step_3_margin_left:I = 0x7f070056

.field public static final guide_searchEngine_punch_step_3_margin_top:I = 0x7f070057

.field public static final guide_searchEngine_punch_step_3_width:I = 0x7f070058

.field public static final guide_search_bar_help_tap_step_1_margin_top:I = 0x7f070059

.field public static final guide_search_bar_help_tap_step_2_margin_right:I = 0x7f07005a

.field public static final guide_search_bar_help_tap_step_2_margin_top:I = 0x7f07005b

.field public static final guide_search_bar_help_tap_step_3_margin_right:I = 0x7f07005c

.field public static final guide_search_bar_help_tap_step_3_margin_top:I = 0x7f07005d

.field public static final guide_search_bar_picker_step_2_margin_right:I = 0x7f07005e

.field public static final guide_search_bar_punch_step_1_margin_left:I = 0x7f07005f

.field public static final guide_search_bar_punch_step_1_margin_top:I = 0x7f070060

.field public static final guide_search_bar_punch_step_1_width:I = 0x7f070061

.field public static final guide_search_bar_punch_step_3_margin_left:I = 0x7f070062

.field public static final guide_search_bar_punch_step_3_margin_top:I = 0x7f070063

.field public static final guide_search_bar_punch_step_3_width:I = 0x7f070064

.field public static final guide_summary_picker_margin:I = 0x7f070065

.field public static final guide_summary_picker_margin_bottom:I = 0x7f070066

.field public static final guide_summary_picker_margin_right:I = 0x7f0700bc

.field public static final guide_summary_side_margin:I = 0x7f070067

.field public static final guide_summary_side_margin_left:I = 0x7f070068

.field public static final guide_summary_side_margin_right:I = 0x7f070069

.field public static final guide_summary_text_size:I = 0x7f07006a

.field public static final guide_text_margin_left:I = 0x7f07006b

.field public static final guide_text_max_width:I = 0x7f07006c

.field public static final guide_text_padding_bottom:I = 0x7f07006d

.field public static final guide_text_padding_left:I = 0x7f07006e

.field public static final guide_text_padding_top:I = 0x7f07006f

.field public static final guide_tip_picker_height:I = 0x7f070070

.field public static final guide_tip_picker_margin_normal:I = 0x7f070071

.field public static final guide_tip_picker_width:I = 0x7f070072

.field public static final helphub_icon_in_text_offset_huge_font:I = 0x7f070073

.field public static final helphub_icon_in_text_offset_norm_font:I = 0x7f070074

.field public static final helphub_icon_in_text_offset_tiny_font:I = 0x7f070075

.field public static final internet_star_location_bar_left_margin:I = 0x7f070076

.field public static final internet_star_location_bar_right_margin:I = 0x7f070077

.field public static final omnibox_suggestion_height:I = 0x7f070078

.field public static final ph_toolbar_backward_forward_btn_height:I = 0x7f070079

.field public static final ph_toolbar_backward_forward_btn_padding_bottom:I = 0x7f07007a

.field public static final ph_toolbar_backward_forward_btn_padding_left:I = 0x7f07007b

.field public static final ph_toolbar_backward_forward_btn_padding_right:I = 0x7f07007c

.field public static final ph_toolbar_backward_forward_btn_padding_top:I = 0x7f07007d

.field public static final ph_toolbar_backward_forward_btn_width:I = 0x7f07007e

.field public static final ph_toolbar_delete_height:I = 0x7f07007f

.field public static final ph_toolbar_delete_width:I = 0x7f070080

.field public static final ph_toolbar_divider_height:I = 0x7f070081

.field public static final ph_toolbar_divider_width:I = 0x7f070082

.field public static final ph_toolbar_mic_height:I = 0x7f070083

.field public static final ph_toolbar_mic_width:I = 0x7f070084

.field public static final ph_toolbar_multiwindow_count_margin_left:I = 0x7f070085

.field public static final ph_toolbar_multiwindow_count_margin_top:I = 0x7f070086

.field public static final ph_toolbar_multiwindow_count_text_size:I = 0x7f070087

.field public static final ph_toolbar_reload_height:I = 0x7f070088

.field public static final ph_toolbar_reload_width:I = 0x7f070089

.field public static final ph_toolbar_url_height:I = 0x7f07008a

.field public static final ph_toolbar_url_left_margin:I = 0x7f07008b

.field public static final sbrowser_actionbar_scroll_view_strip_height:I = 0x7f0700ca

.field public static final sbrowser_bookmark_folder_checkbox_height:I = 0x7f07008c

.field public static final sbrowser_bookmark_folder_checkbox_width:I = 0x7f07008d

.field public static final sbrowser_control_container_height:I = 0x7f0700cb

.field public static final sbrowser_favicon_Right_margin:I = 0x7f0700cc

.field public static final sbrowser_favicon_left_margin:I = 0x7f0700cd

.field public static final sbrowser_favicon_size:I = 0x7f0700ce

.field public static final sbrowser_location_bar_margin:I = 0x7f07008e

.field public static final sbrowser_location_bar_marginRight:I = 0x7f07008f

.field public static final sbrowser_locationbar_refresh_height:I = 0x7f070090

.field public static final sbrowser_locationbar_refresh_marginRight:I = 0x7f070091

.field public static final sbrowser_locationbar_refresh_width:I = 0x7f070092

.field public static final sbrowser_suggestion_list_margin_right:I = 0x7f070093

.field public static final sbrowser_tab_addoverlap:I = 0x7f0700cf

.field public static final sbrowser_tab_overlap:I = 0x7f0700d0

.field public static final sbrowser_tab_padding_top:I = 0x7f0700d1

.field public static final sbrowser_tab_slice:I = 0x7f0700d2

.field public static final sbrowser_tab_strip_and_toolbar_height:I = 0x7f0700d3

.field public static final sbrowser_tab_strip_height:I = 0x7f0700d4

.field public static final sbrowser_tab_width:I = 0x7f0700d5

.field public static final sbrowser_toolbar_delete_height:I = 0x7f0700d6

.field public static final sbrowser_toolbar_delete_width:I = 0x7f0700d7

.field public static final sbrowser_toolbar_height_no_shadow:I = 0x7f0700d8

.field public static final sbrowser_toolbar_homepage_text_size_easymode:I = 0x7f070094

.field public static final sbrowser_toolbar_progressbar_height:I = 0x7f0700d9

.field public static final sbrowser_toolbar_text_size_easymode:I = 0x7f070095

.field public static final sbrowser_toolbar_url_height:I = 0x7f0700da

.field public static final sbrowser_toolbar_url_left_margin:I = 0x7f0700db

.field public static final sbrowser_toolbar_urlbar_margin_easymode:I = 0x7f070096

.field public static final scroll_seekbar_linear_height:I = 0x7f070097

.field public static final scroll_seekbar_linear_margin_right_land:I = 0x7f0700dc

.field public static final scroll_seekbar_linear_margin_right_port:I = 0x7f0700dd

.field public static final scroll_seekbar_linear_margin_top:I = 0x7f0700de

.field public static final scroll_seekbar_linear_padding:I = 0x7f0700df

.field public static final scroll_seekbar_linear_width:I = 0x7f070098

.field public static final scroll_seekbar_margin_right:I = 0x7f070099

.field public static final scroll_seekbar_progress_margin:I = 0x7f0700e0

.field public static final scroll_seekbar_text_size:I = 0x7f0700e1

.field public static final seekbar_width:I = 0x7f07009a

.field public static final suggest_item_padding_left:I = 0x7f07009b

.field public static final suggest_item_padding_right:I = 0x7f07009c

.field public static final suggest_item_padding_top:I = 0x7f07009d

.field public static final tab_first_padding_left:I = 0x7f0700e2

.field public static final tab_focus_stroke:I = 0x7f0700e3

.field public static final tab_focus_stroke_inactive:I = 0x7f0700e4

.field public static final tab_manager_add_new_text_right_margin:I = 0x7f07009e

.field public static final tab_manager_back_height:I = 0x7f07009f

.field public static final tab_manager_back_margin_left:I = 0x7f0700a0

.field public static final tab_manager_back_width:I = 0x7f0700a1

.field public static final tab_manager_divider_height:I = 0x7f0700a2

.field public static final tab_manager_divider_width:I = 0x7f0700a3

.field public static final tab_manager_new_tab_height:I = 0x7f0700a4

.field public static final tab_manager_new_tab_left_margin:I = 0x7f0700a5

.field public static final tab_manager_new_tab_margin_bottom:I = 0x7f0700a6

.field public static final tab_manager_new_tab_margin_top:I = 0x7f0700a7

.field public static final tab_manager_new_tab_right_margin:I = 0x7f0700a8

.field public static final tab_manager_new_tab_width:I = 0x7f0700a9

.field public static final tab_manager_sbrowser_icon_height:I = 0x7f0700aa

.field public static final tab_manager_sbrowser_icon_margin_left:I = 0x7f0700ab

.field public static final tab_manager_sbrowser_icon_margin_right:I = 0x7f0700ac

.field public static final tab_manager_sbrowser_icon_padding_bottom:I = 0x7f0700ad

.field public static final tab_manager_sbrowser_icon_padding_top:I = 0x7f0700ae

.field public static final tab_manager_sbrowser_icon_width:I = 0x7f0700af

.field public static final tab_manager_toolbar_height:I = 0x7f0700b0

.field public static final tab_manager_window_manager_margin_left:I = 0x7f0700b1

.field public static final tag_confirm_button_height:I = 0x7f0700b2

.field public static final tag_confirm_button_width:I = 0x7f0700b3

.field public static final toolbar_magnify_icon_size:I = 0x7f0700e5

.field public static final toolbar_navigation_left_margin:I = 0x7f0700e6

.field public static final toolbar_navigation_right_margin:I = 0x7f0700e7

.field public static final toolbar_text_size_normal:I = 0x7f0700e8

.field public static final toolbar_url_left_margin:I = 0x7f0700e9

.field public static final toolbar_url_left_margin_adjust_right:I = 0x7f0700ea

.field public static final toolbar_url_right_margin_adjust_right:I = 0x7f0700eb

.field public static final visual_icon_height:I = 0x7f0700b4

.field public static final visual_icon_padding_bottom:I = 0x7f0700b5

.field public static final visual_icon_padding_left:I = 0x7f0700b6

.field public static final visual_icon_padding_top:I = 0x7f0700b7

.field public static final visual_icon_width:I = 0x7f0700b8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
