.class public Lcom/sec/android/app/sbrowsertry/ReaderActivity;
.super Landroid/app/Activity;
.source "ReaderActivity.java"


# instance fields
.field private mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sbrowsertry/ReaderActivity;)Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/ReaderActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/sbrowsertry/ReaderActivity;Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/ReaderActivity;
    .param p1, "x1"    # Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    return-object p1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v2, 0x7f030002

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->setContentView(I)V

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 49
    new-instance v2, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-direct {v2, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 50
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const v3, 0x7f03000b

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setContentView(I)V

    .line 51
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    sget-object v3, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setTouchTransparencyMode(Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;)V

    .line 52
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v2, v4}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setShowWrongInputToast(Z)V

    .line 53
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v2, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 54
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v2}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->show()V

    .line 56
    new-instance v1, Lcom/sec/android/app/sbrowsertry/ReaderActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sbrowsertry/ReaderActivity$1;-><init>(Lcom/sec/android/app/sbrowsertry/ReaderActivity;)V

    .line 68
    .local v1, "runnable":Ljava/lang/Runnable;
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 69
    .local v0, "handler":Landroid/os/Handler;
    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 70
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0b0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 75
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/ReaderActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 82
    :cond_0
    return-void
.end method
