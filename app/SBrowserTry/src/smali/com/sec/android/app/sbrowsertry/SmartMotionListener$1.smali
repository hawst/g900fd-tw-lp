.class Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;
.super Ljava/lang/Object;
.source "SmartMotionListener.java"

# interfaces
.implements Lcom/samsung/android/motion/MRListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMotionListener(Lcom/samsung/android/motion/MREvent;)V
    .locals 5
    .param p1, "motionEvent"    # Lcom/samsung/android/motion/MREvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 120
    invoke-virtual {p1}, Lcom/samsung/android/motion/MREvent;->getMotion()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 123
    :pswitch_0
    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MOTION - SMART_SCROLL_TILT_UP_START"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    iget-object v0, v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    iget-object v0, v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0, v4}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setEnableVSync(Z)V

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSpeedNormalY:I
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$100(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)I

    move-result v1

    # invokes: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->handleSmartMotionScrolling(II)V
    invoke-static {v0, v4, v1}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$200(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;II)V

    goto :goto_0

    .line 134
    :pswitch_1
    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MOTION - SMART_SCROLL_TILT_DOWN_START"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    iget-object v0, v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v0, :cond_2

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    iget-object v0, v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0, v4}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setEnableVSync(Z)V

    .line 140
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollSpeedNormalY:I
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$100(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)I

    move-result v2

    # invokes: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->handleSmartMotionScrolling(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$200(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;II)V

    goto :goto_0

    .line 145
    :pswitch_2
    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MOTION - SMART_SCROLL_TILT_FACE_IN_STOP : listener="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckStartMotionListener:Z
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$300(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCheckFaceDetected="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckFaceDetected:Z
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$400(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    iget-object v0, v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v0, :cond_3

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    iget-object v0, v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0, v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setEnableVSync(Z)V

    .line 151
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollDirection:I
    invoke-static {v0, v3}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$502(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;I)I

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # invokes: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->getSmartScrollSpeed()V
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$600(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$700(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$700(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mDoScrollMove:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$802(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;Z)Z

    .line 159
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckSendResetEvent:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$902(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;Z)Z

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckStartMotionListener:Z
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$300(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckFaceDetected:Z
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$400(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckFaceDetected:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$402(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;Z)Z

    .line 165
    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MOTION - SMART_SCROLL_TILT_FACE_IN_STOP - onSmartScrollReady"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    iget-object v0, v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    iget-object v0, v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->smartScrollReady()V

    goto/16 :goto_0

    .line 175
    :pswitch_3
    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MOTION - SMART_SCROLL_TILT_FACE_OUT_STOP"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    iget-object v0, v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    if-eqz v0, :cond_5

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    iget-object v0, v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mVScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    invoke-virtual {v0, v3}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setEnableVSync(Z)V

    .line 181
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mSmartScrollDirection:I
    invoke-static {v0, v3}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$502(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;I)I

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$700(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mPrivateEventHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$700(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mDoScrollMove:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$802(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;Z)Z

    .line 186
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckFaceDetected:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$402(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;Z)Z

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->mCheckSendResetEvent:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$902(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;Z)Z

    goto/16 :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
