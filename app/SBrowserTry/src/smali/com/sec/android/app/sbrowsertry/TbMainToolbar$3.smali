.class Lcom/sec/android/app/sbrowsertry/TbMainToolbar$3;
.super Ljava/lang/Object;
.source "TbMainToolbar.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    .line 135
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "i"    # I
    .param p3, "i2"    # I
    .param p4, "i3"    # I

    .prologue
    .line 122
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "i"    # I
    .param p3, "i2"    # I
    .param p4, "i3"    # I

    .prologue
    .line 126
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mNavigationFakeHint:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->access$300(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$3;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mNavigationFakeHint:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->access$300(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
