.class Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$3;
.super Ljava/lang/Object;
.source "SearchEngineOptions.java"

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->createPopup(Landroid/content/Context;Landroid/view/View;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$3;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$3;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    # getter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->isClicked:Z
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$100(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$3;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    # setter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->isClicked:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$102(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;Z)Z

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$3;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    # getter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$300(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Lcom/sec/android/app/sbrowsertry/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->finish()V

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$3;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    # getter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$000(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Landroid/widget/ListPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 107
    :goto_0
    return-void

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$3;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    # getter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$000(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Landroid/widget/ListPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0
.end method
