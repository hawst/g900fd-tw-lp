.class Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;
.super Ljava/lang/Object;
.source "AirMotionDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/AirMotionDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AirMotionSettings"
.end annotation


# instance fields
.field private mMotionType:I

.field private mSettingsContentResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "motionType"    # I

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;->mSettingsContentResolver:Landroid/content/ContentResolver;

    .line 162
    iput p2, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;->mMotionType:I

    .line 163
    return-void
.end method


# virtual methods
.method public checkAIRPINEnalbed()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;->mSettingsContentResolver:Landroid/content/ContentResolver;

    const-string v3, "air_motion_clip"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 190
    const-string v1, "AirMotionDetector"

    const-string v2, " Air Pin Enables  in Settings"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :goto_0
    return v0

    .line 193
    :cond_0
    const-string v0, "AirMotionDetector"

    const-string v2, " Air Pin disables in Settings"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 194
    goto :goto_0
.end method

.method public checkAirMotionEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;->mSettingsContentResolver:Landroid/content/ContentResolver;

    const-string v2, "air_motion_engine"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    .line 175
    :goto_0
    return v0

    .line 169
    :cond_0
    iget v1, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;->mMotionType:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 171
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;->checkAirScrollEnalbed()Z

    move-result v0

    goto :goto_0

    .line 173
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;->checkAIRPINEnalbed()Z

    move-result v0

    goto :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public checkAirScrollEnalbed()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 180
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;->mSettingsContentResolver:Landroid/content/ContentResolver;

    const-string v3, "air_motion_scroll"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 184
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
