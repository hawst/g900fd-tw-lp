.class public Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
.super Ljava/lang/Object;
.source "SearchEngineOptions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;,
        Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;
    }
.end annotation


# instance fields
.field private final infoHolders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;",
            ">;"
        }
    .end annotation
.end field

.field private isClicked:Z

.field private localResources:Landroid/content/res/Resources;

.field private mAdapter:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;

.field mBingInfo:Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

.field mOnClickListener:Landroid/view/View$OnClickListener;

.field private mPopup:Landroid/widget/ListPopupWindow;

.field private mScreenWidth:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sbrowsertry/MainActivity;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/app/sbrowsertry/MainActivity;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->isClicked:Z

    .line 33
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$1;-><init>(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 69
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->localResources:Landroid/content/res/Resources;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->infoHolders:Ljava/util/ArrayList;

    .line 72
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    const-string v2, "bing"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mBingInfo:Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->infoHolders:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mBingInfo:Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Landroid/widget/ListPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mScreenWidth:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mScreenWidth:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Lcom/sec/android/app/sbrowsertry/MainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->localResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->infoHolders:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public createPopup(Landroid/content/Context;Landroid/view/View;I)V
    .locals 5
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "paramView"    # Landroid/view/View;
    .param p3, "orientation"    # I

    .prologue
    .line 77
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->isClicked:Z

    .line 78
    new-instance v2, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$2;

    const/4 v3, 0x0

    const v4, 0x101006b

    invoke-direct {v2, p0, p1, v3, v4}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$2;-><init>(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    .line 90
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 91
    .local v1, "localLayoutInflater":Landroid/view/LayoutInflater;
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mInflater:Landroid/view/LayoutInflater;

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2, p2}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 94
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setInputMethodMode(I)V

    .line 95
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$3;-><init>(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)V

    .line 109
    .local v0, "local1":Landroid/widget/PopupWindow$OnDismissListener;
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2, v0}, Landroid/widget/ListPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 110
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->isClicked:Z

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 266
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->dismiss()V

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->postShow()V

    goto :goto_0
.end method

.method public onOrientationChanged()V
    .locals 4

    .prologue
    .line 278
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    if-eqz v2, :cond_0

    .line 279
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v2

    if-nez v2, :cond_1

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 282
    .local v1, "searchEngineDelayHandler":Landroid/os/Handler;
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$4;-><init>(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)V

    .line 288
    .local v0, "run":Ljava/lang/Runnable;
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->dismiss()V

    .line 289
    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public show(ZZ)V
    .locals 4
    .param p1, "paramBoolean1"    # Z
    .param p2, "paramBoolean3"    # Z

    .prologue
    .line 113
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->isClicked:Z

    .line 115
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v1, "localArrayList1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;>;"
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mBingInfo:Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;->getFavIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mBingInfo:Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 118
    .local v0, "info":Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    new-instance v2, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mInflater:Landroid/view/LayoutInflater;

    invoke-direct {v2, p0, v1, v3}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;-><init>(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;Ljava/util/List;Landroid/view/LayoutInflater;)V

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mAdapter:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mAdapter:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mAdapter:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->show()V

    .line 135
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->localResources:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 177
    return-void
.end method
