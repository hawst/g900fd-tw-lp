.class public Lcom/sec/android/app/sbrowsertry/LocationBar;
.super Landroid/widget/LinearLayout;
.source "LocationBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;,
        Lcom/sec/android/app/sbrowsertry/LocationBar$StateListener;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentState:I

.field protected mDeleteButton:Landroid/widget/ImageView;

.field protected mMic_button:Landroid/widget/ImageButton;

.field protected mPh_toolbar_bookmark:Landroid/widget/ImageView;

.field private mPh_toolbar_search:Landroid/widget/ImageButton;

.field private mPh_toolbar_star:Landroid/widget/ImageButton;

.field private mStateListener:Lcom/sec/android/app/sbrowsertry/LocationBar$StateListener;

.field private final mSuggestionItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mSuggestionListAdapter:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;

.field protected mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

.field protected mUrlBar:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mCurrentState:I

    .line 54
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mContext:Landroid/content/Context;

    .line 55
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030027

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 56
    .local v0, "mView":Landroid/view/View;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionItems:Ljava/util/List;

    .line 57
    new-instance v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionItems:Ljava/util/List;

    invoke-direct {v1, v2, p0, v3}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/sbrowsertry/LocationBar;Ljava/util/List;)V

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListAdapter:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;

    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->initSuggestionList()V

    .line 59
    const v1, 0x7f08006c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mUrlBar:Landroid/widget/EditText;

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mUrlBar:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mUrlBar:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/android/app/sbrowsertry/LocationBar$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$1;-><init>(Lcom/sec/android/app/sbrowsertry/LocationBar;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mUrlBar:Landroid/widget/EditText;

    new-instance v2, Lcom/sec/android/app/sbrowsertry/LocationBar$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$2;-><init>(Lcom/sec/android/app/sbrowsertry/LocationBar;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 88
    const v1, 0x7f08006e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mPh_toolbar_bookmark:Landroid/widget/ImageView;

    .line 89
    const v1, 0x7f080068

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mPh_toolbar_search:Landroid/widget/ImageButton;

    .line 90
    const v1, 0x7f08006a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mPh_toolbar_star:Landroid/widget/ImageButton;

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mPh_toolbar_star:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/android/app/sbrowsertry/LocationBar$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$3;-><init>(Lcom/sec/android/app/sbrowsertry/LocationBar;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    const v1, 0x7f08006f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mDeleteButton:Landroid/widget/ImageView;

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mDeleteButton:Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/android/app/sbrowsertry/LocationBar$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$4;-><init>(Lcom/sec/android/app/sbrowsertry/LocationBar;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    const v1, 0x7f08006d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mMic_button:Landroid/widget/ImageButton;

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/sbrowsertry/MainActivity;

    iget v1, v1, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v2, 0xb

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/sbrowsertry/MainActivity;

    iget v1, v1, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v2, 0x16

    if-gt v1, v2, :cond_0

    .line 126
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mCurrentState:I

    .line 127
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sbrowsertry/LocationBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/LocationBar;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mCurrentState:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/sbrowsertry/LocationBar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/LocationBar;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mCurrentState:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/sbrowsertry/LocationBar;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/LocationBar;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private initSuggestionList()V
    .locals 22

    .prologue
    .line 179
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 180
    .local v12, "query_string":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 181
    .local v17, "query_string2":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 182
    .local v7, "query_string3":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionItems:Ljava/util/List;

    new-instance v11, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;

    new-instance v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;

    sget-object v2, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->HISTORY_URL:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    invoke-virtual {v2}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->nativeType()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090024

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v1 .. v9}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-direct {v11, v1, v12}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;-><init>(Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;Ljava/lang/String;)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionItems:Ljava/util/List;

    new-instance v2, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;

    new-instance v8, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;

    sget-object v3, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->SEARCH_HISTORY:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    invoke-virtual {v3}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->nativeType()I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-string v13, ""

    const-string v14, ""

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-direct/range {v8 .. v16}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-direct {v2, v8, v12}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;-><init>(Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionItems:Ljava/util/List;

    new-instance v2, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;

    new-instance v13, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;

    sget-object v3, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->SEARCH_HISTORY:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    invoke-virtual {v3}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->nativeType()I

    move-result v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    const-string v18, ""

    const-string v19, ""

    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-direct/range {v13 .. v21}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-direct {v2, v13, v12}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionItem;-><init>(Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    if-nez v1, :cond_0

    .line 187
    new-instance v1, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;-><init>(Lcom/sec/android/app/sbrowsertry/LocationBar;Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    .line 188
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->updateSuggestionPopupPosition()V

    .line 189
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListAdapter:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 191
    :cond_0
    return-void
.end method

.method private updateSuggestionPopupPosition()V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->show()V

    .line 201
    :cond_0
    return-void
.end method


# virtual methods
.method changeState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 153
    packed-switch p1, :pswitch_data_0

    .line 170
    :goto_0
    return-void

    .line 157
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mPh_toolbar_bookmark:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mDeleteButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mMic_button:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mPh_toolbar_star:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mPh_toolbar_search:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mStateListener:Lcom/sec/android/app/sbrowsertry/LocationBar$StateListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/sbrowsertry/LocationBar$StateListener;->onStateChanged(I)V

    goto :goto_0

    .line 166
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mStateListener:Lcom/sec/android/app/sbrowsertry/LocationBar$StateListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/sbrowsertry/LocationBar$StateListener;->onStateChanged(I)V

    goto :goto_0

    .line 153
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public dismissSuggestion()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->setSuggestionsListVisibility(Z)V

    .line 141
    :cond_0
    return-void
.end method

.method public doClick()V
    .locals 4

    .prologue
    .line 309
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 310
    .local v0, "fadeInAnim":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 311
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 312
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mUrlBar:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->startAnimation(Landroid/view/animation/Animation;)V

    .line 313
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mCurrentState:I

    .line 314
    new-instance v1, Lcom/sec/android/app/sbrowsertry/LocationBar$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$5;-><init>(Lcom/sec/android/app/sbrowsertry/LocationBar;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/LocationBar;->post(Ljava/lang/Runnable;)Z

    .line 321
    return-void
.end method

.method protected positionSuggestionPopup(Landroid/widget/ListPopupWindow;)V
    .locals 4
    .param p1, "listpopupwindow"    # Landroid/widget/ListPopupWindow;

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->getWidth()I

    move-result v0

    .line 299
    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 300
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 301
    .local v1, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 302
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 303
    iget v2, v1, Landroid/graphics/Rect;->left:I

    neg-int v2, v2

    invoke-virtual {p1, v2}, Landroid/widget/ListPopupWindow;->setHorizontalOffset(I)V

    .line 305
    .end local v1    # "rect":Landroid/graphics/Rect;
    :cond_0
    invoke-virtual {p1, v0}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 306
    return-void
.end method

.method public setStateListener(Lcom/sec/android/app/sbrowsertry/LocationBar$StateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/sbrowsertry/LocationBar$StateListener;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mStateListener:Lcom/sec/android/app/sbrowsertry/LocationBar$StateListener;

    .line 175
    return-void
.end method

.method protected setSuggestionsListVisibility(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 204
    if-nez p1, :cond_1

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->dismiss()V

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 208
    invoke-direct {p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->updateSuggestionPopupPosition()V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/LocationBar;->mSuggestionListPopup:Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/LocationBar$SuggestionsPopupWindow;->show()V

    goto :goto_0
.end method

.method public showSuggestion()V
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->setSuggestionsListVisibility(Z)V

    .line 132
    return-void
.end method
