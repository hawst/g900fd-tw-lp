.class public Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;
.super Landroid/widget/LinearLayout;
.source "TbTabButtonContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private isTwoTab:Z

.field private mActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

.field private mAddTabOverlap:I

.field private mContext:Landroid/content/Context;

.field private mNewTabButton:Landroid/widget/ImageButton;

.field private mTabScrollView:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sbrowsertry/MainActivity;Landroid/content/Context;)V
    .locals 5
    .param p1, "activity"    # Lcom/sec/android/app/sbrowsertry/MainActivity;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 31
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    iput-boolean v4, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->isTwoTab:Z

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    .line 33
    iput-object p2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mContext:Landroid/content/Context;

    .line 35
    invoke-virtual {p1}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 36
    .local v1, "res":Landroid/content/res/Resources;
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 37
    .local v0, "factory":Landroid/view/LayoutInflater;
    const v2, 0x7f030028

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 38
    const v2, 0x7f0700d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0, v4, v2, v4, v4}, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->setPadding(IIII)V

    .line 39
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->setMinimumWidth(I)V

    .line 41
    const v2, 0x7f080072

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mNewTabButton:Landroid/widget/ImageButton;

    .line 42
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mNewTabButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    const v2, 0x7f0700cf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mAddTabOverlap:I

    .line 46
    const v2, 0x7f080071

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    iput-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mTabScrollView:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    .line 47
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mTabScrollView:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 48
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mTabScrollView:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->buildTabView()Lcom/sec/android/app/sbrowsertry/TbTabButton;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->addTab(Landroid/view/View;)V

    .line 49
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mTabScrollView:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->setSelectedTab(I)V

    .line 50
    return-void
.end method


# virtual methods
.method public buildTabView()Lcom/sec/android/app/sbrowsertry/TbTabButton;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 53
    new-instance v0, Lcom/sec/android/app/sbrowsertry/TbTabButton;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-direct {v0, v1}, Lcom/sec/android/app/sbrowsertry/TbTabButton;-><init>(Landroid/content/Context;)V

    .line 54
    .local v0, "tabview":Lcom/sec/android/app/sbrowsertry/TbTabButton;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->setDisplayTitle(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->setFavicon(Landroid/graphics/Bitmap;)V

    .line 57
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->setActivated(Z)V

    .line 59
    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mNewTabButton:Landroid/widget/ImageButton;

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->isTwoTab:Z

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mTabScrollView:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->buildTabView()Lcom/sec/android/app/sbrowsertry/TbTabButton;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->addTab(Landroid/view/View;)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mTabScrollView:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->setSelectedTab(I)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->handleStateChange()V

    .line 70
    iput-boolean v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->isTwoTab:Z

    .line 72
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->getPaddingTop()I

    move-result v0

    .line 91
    .local v0, "pt":I
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mTabScrollView:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    invoke-virtual {v2}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->getMeasuredWidth()I

    move-result v1

    .line 93
    .local v1, "sw":I
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mTabScrollView:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    const/4 v3, 0x0

    sub-int v4, p5, p3

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->layout(IIII)V

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mNewTabButton:Landroid/widget/ImageButton;

    iget v3, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mAddTabOverlap:I

    sub-int v3, v1, v3

    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mNewTabButton:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget v5, p0, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;->mAddTabOverlap:I

    sub-int/2addr v4, v5

    sub-int v5, p5, p3

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/widget/ImageButton;->layout(IIII)V

    .line 96
    return-void
.end method
