.class public final Lcom/sec/android/app/sbrowsertry/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ChromeViewHolder:[I

.field public static final ChromeViewHolder_gutterPercentage:I = 0x1

.field public static final ChromeViewHolder_location_allowed:I = 0x2

.field public static final ChromeViewHolder_popups_allowed:I = 0x3

.field public static final ChromeViewHolder_spaceBetweenTabs:I = 0x0

.field public static final SflScrollView:[I

.field public static final SflScrollView_android_galleryItemBackground:I = 0x0

.field public static final TwHelpTextView:[I

.field public static final TwHelpTextView_image:I = 0xa

.field public static final TwHelpTextView_images:I = 0x9

.field public static final TwHelpTextView_insideImg_gravity:I = 0x8

.field public static final TwHelpTextView_insideImg_height:I = 0x6

.field public static final TwHelpTextView_insideImg_padding:I = 0x5

.field public static final TwHelpTextView_insideImg_paddingBottom:I = 0x4

.field public static final TwHelpTextView_insideImg_paddingLeft:I = 0x1

.field public static final TwHelpTextView_insideImg_paddingRight:I = 0x2

.field public static final TwHelpTextView_insideImg_paddingTop:I = 0x3

.field public static final TwHelpTextView_insideImg_width:I = 0x7

.field public static final TwHelpTextView_text:I = 0x0

.field public static final TwTouchPunchView:[I

.field public static final TwTouchPunchView_punchshape:I = 0x0

.field public static final TwTouchPunchView_punchshow:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1166
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/sbrowsertry/R$styleable;->ChromeViewHolder:[I

    .line 1237
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101004c

    aput v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sbrowsertry/R$styleable;->SflScrollView:[I

    .line 1276
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/sbrowsertry/R$styleable;->TwHelpTextView:[I

    .line 1456
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/sbrowsertry/R$styleable;->TwTouchPunchView:[I

    return-void

    .line 1166
    nop

    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
    .end array-data

    .line 1276
    :array_1
    .array-data 4
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
    .end array-data

    .line 1456
    :array_2
    .array-data 4
        0x7f01000f
        0x7f010010
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
