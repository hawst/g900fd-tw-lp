.class public Lcom/sec/android/app/sbrowsertry/AirMotionDetector;
.super Ljava/lang/Object;
.source "AirMotionDetector.java"

# interfaces
.implements Lcom/samsung/android/service/gesture/GestureListener;
.implements Lcom/samsung/android/service/gesture/GestureManager$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;,
        Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;,
        Lcom/sec/android/app/sbrowsertry/AirMotionDetector$IAirMotionListener;
    }
.end annotation


# static fields
.field public static final AIR_MOTION_AIRPIN:I = 0x1

.field public static final AIR_MOTION_AIRSCROLL:I = 0x0

.field public static final AIR_MOTION_NONE:I = 0x3

.field private static final AIR_MOTION_PROVIDER:Ljava/lang/String; = "ir_provider"

.field private static final TAG:Ljava/lang/String; = "AirMotionDetector"


# instance fields
.field private mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

.field private mAirMotionSettings:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;

.field private mConnected:Z

.field private mContext:Landroid/content/Context;

.field private mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

.field private mMotionType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "motionType"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    .line 26
    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mConnected:Z

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mContext:Landroid/content/Context;

    .line 28
    iput v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mMotionType:I

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mContext:Landroid/content/Context;

    .line 52
    new-instance v0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p2}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionSettings:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;

    .line 53
    return-void
.end method


# virtual methods
.method public destoryAirMotionDetector()V
    .locals 2

    .prologue
    .line 86
    const-string v0, "AirMotionDetector"

    const-string v1, "Destory AirMotionDetector."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    if-nez v0, :cond_0

    .line 91
    :goto_0
    return-void

    .line 90
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->setAirMotionLintener(Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;I)V

    goto :goto_0
.end method

.method public onGestureEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/samsung/android/service/gesture/GestureEvent;

    .prologue
    .line 121
    invoke-virtual {p1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 153
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 123
    :pswitch_1
    const-string v0, "AirMotionDetector"

    const-string v1, " GESTURE_EVENT_SWEEP_LEFT"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;->onLeft()V

    goto :goto_0

    .line 129
    :pswitch_2
    const-string v0, "AirMotionDetector"

    const-string v1, " GESTURE_EVENT_SWEEP_RIGHT"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;->onRight()V

    goto :goto_0

    .line 135
    :pswitch_3
    const-string v0, "AirMotionDetector"

    const-string v1, " GESTURE_EVENT_SWEEP_UP"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;->onUp()V

    goto :goto_0

    .line 141
    :pswitch_4
    const-string v0, "AirMotionDetector"

    const-string v1, " GESTURE_EVENT_SWEEP_DOWN"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;->onDown()V

    goto :goto_0

    .line 147
    :pswitch_5
    const-string v0, "AirMotionDetector"

    const-string v1, " GESTURE_EVENT_HOVER"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onServiceConnected()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 95
    const-string v0, "AirMotionDetector"

    const-string v1, " Service is connected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iput-boolean v2, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mConnected:Z

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    if-eqz v0, :cond_0

    .line 98
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mMotionType:I

    if-nez v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    const-string v2, "air_motion_scroll"

    invoke-virtual {v0, p0, v1, v2}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mMotionType:I

    if-ne v0, v2, :cond_2

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    const-string v2, "air_motion_clip"

    invoke-virtual {v0, p0, v1, v2}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    invoke-virtual {v0, p0, v1}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onServiceDisconnected()V
    .locals 2

    .prologue
    .line 112
    const-string v0, "AirMotionDetector"

    const-string v1, " Service is disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    invoke-virtual {v0, p0, v1}, Lcom/samsung/android/service/gesture/GestureManager;->unregisterListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;)V

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mConnected:Z

    .line 117
    return-void
.end method

.method public setAirMotionLintener(Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;I)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;
    .param p2, "motionType"    # I

    .prologue
    .line 57
    const-string v0, "AirMotionDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " setAirMotionLintener. listner "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",motionType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    if-nez p1, :cond_1

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    .line 60
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mConnected:Z

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    invoke-virtual {v0, p0, v1}, Lcom/samsung/android/service/gesture/GestureManager;->unregisterListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;)V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionSettings:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionSettings;->checkAirMotionEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    if-nez v0, :cond_2

    .line 65
    new-instance v0, Lcom/samsung/android/service/gesture/GestureManager;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/samsung/android/service/gesture/GestureManager;-><init>(Landroid/content/Context;Lcom/samsung/android/service/gesture/GestureManager$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    .line 67
    :cond_2
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mAirMotionListener:Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;

    .line 68
    iput p2, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mMotionType:I

    .line 70
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mConnected:Z

    if-eqz v0, :cond_0

    .line 71
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mMotionType:I

    if-nez v0, :cond_3

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    const-string v2, "air_motion_scroll"

    invoke-virtual {v0, p0, v1, v2}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :cond_3
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mMotionType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    const-string v2, "air_motion_clip"

    invoke-virtual {v0, p0, v1, v2}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 76
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    invoke-virtual {v0, p0, v1}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector;->destoryAirMotionDetector()V

    goto :goto_0
.end method
