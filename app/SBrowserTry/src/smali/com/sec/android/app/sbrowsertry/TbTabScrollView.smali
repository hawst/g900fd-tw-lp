.class public Lcom/sec/android/app/sbrowsertry/TbTabScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "TbTabScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;
    }
.end annotation


# instance fields
.field private mContentView:Landroid/widget/LinearLayout;

.field private mSelected:I

.field private mTabOverlap:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->init(Landroid/content/Context;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->init(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->init(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sbrowsertry/TbTabScrollView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mContentView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sbrowsertry/TbTabScrollView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mTabOverlap:I

    return v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mTabOverlap:I

    .line 58
    invoke-virtual {p0, v3}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 59
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->setOverScrollMode(I)V

    .line 60
    new-instance v0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;-><init>(Lcom/sec/android/app/sbrowsertry/TbTabScrollView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mContentView:Landroid/widget/LinearLayout;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700e2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->addView(Landroid/view/View;)V

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mSelected:I

    .line 65
    return-void
.end method


# virtual methods
.method addTab(Landroid/view/View;)V
    .locals 1
    .param p1, "tab"    # Landroid/view/View;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 104
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setActivated(Z)V

    .line 105
    return-void
.end method

.method getSelectedTab()Landroid/view/View;
    .locals 2

    .prologue
    .line 95
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mSelected:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mSelected:I

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mContentView:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mSelected:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 69
    invoke-super/range {p0 .. p5}, Landroid/widget/HorizontalScrollView;->onLayout(ZIIII)V

    .line 70
    return-void
.end method

.method setSelectedTab(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->getSelectedTab()Landroid/view/View;

    move-result-object v0

    .line 83
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 84
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    .line 86
    :cond_0
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mSelected:I

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->getSelectedTab()Landroid/view/View;

    move-result-object v0

    .line 88
    if-eqz v0, :cond_1

    .line 89
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    .line 91
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->requestLayout()V

    .line 92
    return-void
.end method

.method protected updateLayout()V
    .locals 4

    .prologue
    .line 74
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    .line 75
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 76
    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/sbrowsertry/TbTabButton;

    .line 77
    .local v2, "tv":Lcom/sec/android/app/sbrowsertry/TbTabButton;
    invoke-virtual {v2}, Lcom/sec/android/app/sbrowsertry/TbTabButton;->updateLayoutParams()V

    .line 75
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 79
    .end local v2    # "tv":Lcom/sec/android/app/sbrowsertry/TbTabButton;
    :cond_0
    return-void
.end method
