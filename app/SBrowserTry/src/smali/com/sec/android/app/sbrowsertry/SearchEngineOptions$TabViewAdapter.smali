.class Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "SearchEngineOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TabViewAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mNumTabItems:I

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private final mTabItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;Ljava/util/List;Landroid/view/LayoutInflater;)V
    .locals 1
    .param p3, "paramLayoutInflater"    # Landroid/view/LayoutInflater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;",
            ">;",
            "Landroid/view/LayoutInflater;",
            ")V"
        }
    .end annotation

    .prologue
    .line 186
    .local p2, "paramList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;>;"
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 187
    iput-object p2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->mTabItems:Ljava/util/List;

    .line 188
    iput-object p3, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 189
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->mNumTabItems:I

    .line 190
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->mNumTabItems:I

    return v0
.end method

.method public getItem(I)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;
    .locals 1
    .param p1, "paramInt"    # I

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->mTabItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 179
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->getItem(I)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabInfoHolder;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "paramInt"    # I

    .prologue
    .line 212
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "paramInt"    # I

    .prologue
    .line 217
    const/4 v0, 0x0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "paramInt"    # I
    .param p2, "paramView"    # Landroid/view/View;
    .param p3, "paramViewGroup"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x1

    .line 222
    move-object v0, p2

    .line 223
    .local v0, "localView":Landroid/view/View;
    const/4 v1, 0x0

    .line 224
    .local v1, "localViewHolder":Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;
    if-nez v0, :cond_0

    .line 225
    new-instance v1, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;

    .end local v1    # "localViewHolder":Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;
    invoke-direct {v1, p0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;-><init>(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;)V

    .line 226
    .restart local v1    # "localViewHolder":Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f03002b

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 227
    const v5, 0x7f080098

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;->text:Landroid/widget/TextView;

    .line 228
    const v5, 0x7f080097

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, v1, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;->icon:Landroid/widget/ImageButton;

    .line 229
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 232
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    # getter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->infoHolders:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$500(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;

    invoke-virtual {v5}, Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;->getName()Ljava/lang/String;

    move-result-object v2

    .line 233
    .local v2, "searchEnginesName":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual {v2, v5, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 234
    .local v3, "temp1":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 236
    .local v4, "temp2":Ljava/lang/String;
    iget-object v5, v1, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;->text:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    iget-object v5, v1, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;->text:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    iget-object v5, v1, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;->icon:Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    iget-object v6, v1, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;->icon:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    # getter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->infoHolders:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$500(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;

    invoke-virtual {v5}, Lcom/sec/android/app/sbrowsertry/SearchEngineInfo;->getFavIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 241
    iget-object v5, v1, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;->text:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 242
    iget-object v5, v1, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;->icon:Landroid/widget/ImageButton;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 243
    return-object v0

    .line 231
    .end local v2    # "searchEnginesName":Ljava/lang/String;
    .end local v3    # "temp1":Ljava/lang/String;
    .end local v4    # "temp2":Ljava/lang/String;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "localViewHolder":Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;
    check-cast v1, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;

    .restart local v1    # "localViewHolder":Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter$ViewHolder;
    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x1

    return v0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$TabViewAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 203
    return-void
.end method
