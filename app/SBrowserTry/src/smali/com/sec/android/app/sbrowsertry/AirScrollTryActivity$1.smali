.class Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;
.super Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;
.source "AirScrollTryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    invoke-direct {p0}, Lcom/sec/android/app/sbrowsertry/AirMotionDetector$AirMotionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown()V
    .locals 6

    .prologue
    const-wide v4, 0x3feccccccccccccdL    # 0.9

    .line 66
    const-string v0, "AirScrollTryActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GESTURE_EVENT_SWEEP_Down in Tutorial    mScrollView.getBottom()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->access$000(Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;)Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->getBottom()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->access$000(Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;)Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    iget v2, v2, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mViewY:I

    int-to-float v2, v2

    float-to-double v2, v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->smoothScrollBy(II)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->access$000(Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;)Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    iget v1, v1, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mViewY:I

    int-to-float v1, v1

    float-to-double v2, v1

    mul-double/2addr v2, v4

    double-to-int v1, v2

    neg-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->showEdgeEffect(I)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mLinearLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->access$100(Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 74
    return-void
.end method

.method public onUp()V
    .locals 6

    .prologue
    const-wide v4, 0x3feccccccccccccdL    # 0.9

    .line 54
    const-string v0, "AirScrollTryActivity"

    const-string v1, "GESTURE_EVENT_SWEEP_UP in Tutorial"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->access$000(Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;)Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    iget v2, v2, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mViewY:I

    int-to-float v2, v2

    float-to-double v2, v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->smoothScrollBy(II)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mScrollView:Lcom/sec/android/touchwiz/widget/TwHelpScrollView;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->access$000(Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;)Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    iget v1, v1, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mViewY:I

    int-to-float v1, v1

    float-to-double v2, v1

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->showEdgeEffect(I)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->mLinearLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;->access$100(Lcom/sec/android/app/sbrowsertry/AirScrollTryActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 62
    return-void
.end method
