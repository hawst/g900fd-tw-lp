.class public final Lcom/sec/android/app/sbrowsertry/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final actionbar_create_bookmark_folder:I = 0x7f030000

.field public static final activity_main:I = 0x7f030001

.field public static final activity_reader_list:I = 0x7f030002

.field public static final add_bookmark_activity:I = 0x7f030003

.field public static final add_bookmark_activity_tablet:I = 0x7f030004

.field public static final add_quick_access_activity:I = 0x7f030005

.field public static final bottombar:I = 0x7f030006

.field public static final empty_help_dialog:I = 0x7f030007

.field public static final guide_bookmark_step_1:I = 0x7f030008

.field public static final guide_bookmark_step_12:I = 0x7f030009

.field public static final guide_bookmark_step_2:I = 0x7f03000a

.field public static final guide_bookmark_step_3:I = 0x7f03000b

.field public static final guide_bookmark_step_4:I = 0x7f03000c

.field public static final guide_bookmark_step_initial:I = 0x7f03000d

.field public static final guide_magnifier_step_1:I = 0x7f03000e

.field public static final guide_magnifier_step_2:I = 0x7f03000f

.field public static final guide_newtab_step_1:I = 0x7f030010

.field public static final guide_newtab_step_2:I = 0x7f030011

.field public static final guide_quick_access_step_1:I = 0x7f030012

.field public static final guide_reader_step_1:I = 0x7f030013

.field public static final guide_reader_step_2:I = 0x7f030014

.field public static final guide_reading_list_step_1:I = 0x7f030015

.field public static final guide_search_bar_step_1:I = 0x7f030016

.field public static final guide_search_bar_step_2:I = 0x7f030017

.field public static final guide_search_bar_step_3:I = 0x7f030018

.field public static final guide_search_bar_step_4:I = 0x7f030019

.field public static final guide_search_engine_step_1:I = 0x7f03001a

.field public static final guide_search_engine_step_2:I = 0x7f03001b

.field public static final guide_search_engine_step_3:I = 0x7f03001c

.field public static final guide_search_engine_step_4:I = 0x7f03001d

.field public static final guide_smart_screen:I = 0x7f03001e

.field public static final guide_smart_screen_step_1:I = 0x7f03001f

.field public static final guide_smart_screen_step_2:I = 0x7f030020

.field public static final guide_smart_scroll_sensitivity:I = 0x7f030021

.field public static final guide_smart_scroll_step_face:I = 0x7f030022

.field public static final guide_smart_scroll_step_tilt:I = 0x7f030023

.field public static final main_air_scroll:I = 0x7f030024

.field public static final no_tab_layout:I = 0x7f030025

.field public static final quick_new:I = 0x7f030026

.field public static final sbrowser_location_bar:I = 0x7f030027

.field public static final sbrowser_tab_bar:I = 0x7f030028

.field public static final sbrowser_tab_title:I = 0x7f030029

.field public static final sbrowser_toolbar:I = 0x7f03002a

.field public static final search_engine_row:I = 0x7f03002b

.field public static final suggestion_item:I = 0x7f03002c

.field public static final tab_manager_layout:I = 0x7f03002d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
