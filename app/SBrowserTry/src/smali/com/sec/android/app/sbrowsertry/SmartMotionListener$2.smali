.class Lcom/sec/android/app/sbrowsertry/SmartMotionListener$2;
.super Landroid/os/Handler;
.source "SmartMotionListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/SmartMotionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 265
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 271
    # getter for: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :goto_0
    return-void

    .line 267
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # invokes: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->getSmartScrollSpeed()V
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$600(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener$2;->this$0:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    # invokes: Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->doSmartMotionScroll()V
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->access$1000(Lcom/sec/android/app/sbrowsertry/SmartMotionListener;)V

    goto :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
