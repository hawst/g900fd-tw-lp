.class Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;
.super Ljava/lang/Object;
.source "TbMainToolbar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    .line 81
    const v0, 0x103006e

    .line 84
    .local v0, "i":I
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->access$000(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->access$000(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->dismiss()V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    invoke-static {v1, v2}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->access$002(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    new-instance v2, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;
    invoke-static {v3}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->access$100(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Lcom/sec/android/app/sbrowsertry/MainActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;-><init>(Lcom/sec/android/app/sbrowsertry/MainActivity;)V

    # setter for: Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    invoke-static {v1, v2}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->access$002(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->access$000(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->access$200(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    const v4, 0x7f080092

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->createPopup(Landroid/content/Context;Landroid/view/View;I)V

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->access$000(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    move-result-object v1

    invoke-virtual {v1, v5, v5}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->show(ZZ)V

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;->this$0:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->access$200(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/sbrowsertry/MainActivity;->handleStateChange()V

    .line 93
    return-void
.end method
