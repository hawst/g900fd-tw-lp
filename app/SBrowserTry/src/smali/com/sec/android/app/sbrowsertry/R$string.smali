.class public final Lcom/sec/android/app/sbrowsertry/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Indicator_popup:I = 0x7f090000

.field public static final Tap_to_enter_tag:I = 0x7f090001

.field public static final accessibility_button_addbookmark:I = 0x7f090002

.field public static final accessibility_button_bookmarks:I = 0x7f090003

.field public static final accessibility_button_clear:I = 0x7f090004

.field public static final accessibility_button_closetab:I = 0x7f090005

.field public static final accessibility_button_dropdown:I = 0x7f090006

.field public static final accessibility_button_refresh:I = 0x7f090007

.field public static final accessibility_button_voice:I = 0x7f090008

.field public static final accessibility_state_incognito:I = 0x7f090009

.field public static final accessibility_transition_navscreen:I = 0x7f09000a

.field public static final account_name_string:I = 0x7f09000b

.field public static final action_bar_cancel:I = 0x7f09000c

.field public static final action_bar_done:I = 0x7f09000d

.field public static final action_bar_title:I = 0x7f09000e

.field public static final add_bookmark_add_to_quick_access:I = 0x7f09000f

.field public static final addbookmark_cancel_button:I = 0x7f090010

.field public static final addbookmark_page_title:I = 0x7f090011

.field public static final addbookmark_page_url:I = 0x7f090012

.field public static final addbookmark_save_button:I = 0x7f090013

.field public static final addbookmark_tags_title:I = 0x7f090014

.field public static final airscroll_tutorial:I = 0x7f090015

.field public static final app_name:I = 0x7f090016

.field public static final backward:I = 0x7f090017

.field public static final bing:I = 0x7f090018

.field public static final bookmark_cancel:I = 0x7f090019

.field public static final bookmark_done:I = 0x7f09001a

.field public static final bookmark_edit_save:I = 0x7f09001b

.field public static final bookmark_select_folder_my_device:I = 0x7f09001c

.field public static final bookmark_this_page:I = 0x7f09001d

.field public static final bookmark_title:I = 0x7f09001e

.field public static final bookmarks_options_menu_delete:I = 0x7f09001f

.field public static final clear:I = 0x7f090020

.field public static final close_all:I = 0x7f090021

.field public static final create_new_tab_text:I = 0x7f090022

.field public static final create_new_tab_text_tablet:I = 0x7f090023

.field public static final default_title:I = 0x7f090024

.field public static final default_title_samsung:I = 0x7f090025

.field public static final default_url:I = 0x7f090026

.field public static final do_not_save:I = 0x7f090027

.field public static final easy_mode_bookmark_this_page:I = 0x7f090028

.field public static final easy_mode_bookmarks:I = 0x7f090029

.field public static final face_orientation_for_scroll:I = 0x7f09002a

.field public static final folder:I = 0x7f09002b

.field public static final forward:I = 0x7f09002c

.field public static final guide_add_to_bookmarks:I = 0x7f09008a

.field public static final guide_add_to_bookmarks_completed:I = 0x7f09002d

.field public static final guide_change_search_engine:I = 0x7f09002e

.field public static final guide_completed:I = 0x7f09002f

.field public static final guide_enter_tag:I = 0x7f090030

.field public static final guide_enter_the_keyword:I = 0x7f090031

.field public static final guide_hover_here:I = 0x7f090032

.field public static final guide_magnifier:I = 0x7f090033

.field public static final guide_newtab_completed:I = 0x7f09008b

.field public static final guide_open_bookmark:I = 0x7f090034

.field public static final guide_reader_open_article:I = 0x7f090035

.field public static final guide_reader_select_article:I = 0x7f090036

.field public static final guide_scroll_icon_face:I = 0x7f090037

.field public static final guide_scroll_icon_tilt:I = 0x7f090038

.field public static final guide_scroll_sensitivity:I = 0x7f090039

.field public static final guide_select_add_bookmark:I = 0x7f09003a

.field public static final guide_select_search_engine:I = 0x7f09003b

.field public static final guide_smart_rotation:I = 0x7f09003c

.field public static final guide_smart_stay:I = 0x7f09003d

.field public static final guide_tap_here:I = 0x7f09003e

.field public static final guide_tap_quick_access_button:I = 0x7f09003f

.field public static final guide_tap_to_add_bookmark:I = 0x7f090040

.field public static final guide_tap_to_change:I = 0x7f090041

.field public static final guide_tap_to_change_new_new:I = 0x7f090042

.field public static final guide_tap_to_delete:I = 0x7f090043

.field public static final guide_tap_to_open_optionmenu:I = 0x7f090044

.field public static final guide_tap_to_save:I = 0x7f090045

.field public static final guide_tap_to_searchbar_new_new:I = 0x7f090046

.field public static final guide_using_searchbar_completed:I = 0x7f090047

.field public static final hello_world:I = 0x7f090048

.field public static final incognitotab:I = 0x7f090049

.field public static final local_bookmarks:I = 0x7f09004a

.field public static final main_url:I = 0x7f09004b

.field public static final main_url_samsung:I = 0x7f09004c

.field public static final menu_settings:I = 0x7f09004d

.field public static final newtab:I = 0x7f09004e

.field public static final no_tab:I = 0x7f09004f

.field public static final none:I = 0x7f090050

.field public static final options_menu_add_shortcut:I = 0x7f090051

.field public static final options_menu_desktop_view:I = 0x7f090052

.field public static final options_menu_find_on_page:I = 0x7f090053

.field public static final options_menu_history:I = 0x7f090054

.field public static final options_menu_new_window:I = 0x7f090055

.field public static final options_menu_print:I = 0x7f090056

.field public static final options_menu_private_mode:I = 0x7f090057

.field public static final options_menu_refresh:I = 0x7f090058

.field public static final options_menu_scrap:I = 0x7f090059

.field public static final options_menu_scrap_list:I = 0x7f09005a

.field public static final options_menu_settings:I = 0x7f09005b

.field public static final options_menu_share:I = 0x7f09005c

.field public static final options_menu_web_clip:I = 0x7f09005d

.field public static final query_string:I = 0x7f09005e

.field public static final query_string2:I = 0x7f09005f

.field public static final query_string3:I = 0x7f090060

.field public static final quick_access_title:I = 0x7f090061

.field public static final quick_text1:I = 0x7f090062

.field public static final quick_text10:I = 0x7f090063

.field public static final quick_text11:I = 0x7f090064

.field public static final quick_text2:I = 0x7f090065

.field public static final quick_text3:I = 0x7f090066

.field public static final quick_text4:I = 0x7f090067

.field public static final quick_text5:I = 0x7f090068

.field public static final quick_text6:I = 0x7f090069

.field public static final quick_text7:I = 0x7f09006a

.field public static final quick_text8:I = 0x7f09006b

.field public static final quick_text9:I = 0x7f09006c

.field public static final reader:I = 0x7f09006d

.field public static final reading_list_title:I = 0x7f09006e

.field public static final save:I = 0x7f09006f

.field public static final sbrowser_add_bookmark_open_internet_2_new1:I = 0x7f090070

.field public static final sbrowser_add_bookmark_step_1_int_tutorial:I = 0x7f090071

.field public static final sbrowser_locationbar_hint:I = 0x7f090072

.field public static final sbrowser_multiwindow_add_new_tab:I = 0x7f090073

.field public static final scroll_speed_fast:I = 0x7f090074

.field public static final scroll_speed_slow:I = 0x7f090075

.field public static final search_engine_title:I = 0x7f090076

.field public static final sensitivity:I = 0x7f090077

.field public static final smart_scroll_sensitivity:I = 0x7f090078

.field public static final stms_appgroup:I = 0x7f090079

.field public static final stms_version:I = 0x7f09007a

.field public static final sweep:I = 0x7f09007b

.field public static final sync_tabs:I = 0x7f09007c

.field public static final tab_counter:I = 0x7f09007d

.field public static final tab_counter_new:I = 0x7f09007e

.field public static final tab_manager_no_window:I = 0x7f09007f

.field public static final tag_content_string:I = 0x7f090080

.field public static final tag_string:I = 0x7f090081

.field public static final tilt_device_for_scroll:I = 0x7f090082

.field public static final title_activity_main:I = 0x7f090083

.field public static final title_activity_sbrowser_search_bar_try:I = 0x7f090084

.field public static final title_content_string:I = 0x7f090085

.field public static final title_homepage:I = 0x7f090086

.field public static final title_string:I = 0x7f090087

.field public static final window_manager:I = 0x7f090088

.field public static final wrong_input_toast:I = 0x7f090089


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
