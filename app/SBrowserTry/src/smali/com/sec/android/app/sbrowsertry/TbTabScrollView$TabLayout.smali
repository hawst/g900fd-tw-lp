.class Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;
.super Landroid/widget/LinearLayout;
.source "TbTabScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/TbTabScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TabLayout"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sbrowsertry/TbTabScrollView;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->this$0:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    .line 109
    invoke-direct {p0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 110
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->setChildrenDrawingOrderEnabled(Z)V

    .line 111
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 7
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 123
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->getChildCount()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_0

    .line 125
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->this$0:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mTabOverlap:I
    invoke-static {v5}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->access$100(Lcom/sec/android/app/sbrowsertry/TbTabScrollView;)I

    move-result v5

    sub-int v1, v4, v5

    .line 126
    .local v1, "nextLeft":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 127
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 128
    .local v2, "tab":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v5

    sub-int v3, v4, v5

    .line 129
    .local v3, "w":I
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    add-int v5, v1, v3

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v6

    invoke-virtual {v2, v1, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 130
    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->this$0:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mTabOverlap:I
    invoke-static {v4}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->access$100(Lcom/sec/android/app/sbrowsertry/TbTabScrollView;)I

    move-result v4

    sub-int v4, v3, v4

    add-int/2addr v1, v4

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    .end local v0    # "i":I
    .end local v1    # "nextLeft":I
    .end local v2    # "tab":Landroid/view/View;
    .end local v3    # "w":I
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "hspec"    # I
    .param p2, "vspec"    # I

    .prologue
    .line 115
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->getMeasuredWidth()I

    move-result v0

    .line 117
    .local v0, "w":I
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->this$0:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mContentView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->access$000(Lcom/sec/android/app/sbrowsertry/TbTabScrollView;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->this$0:Lcom/sec/android/app/sbrowsertry/TbTabScrollView;

    # getter for: Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->mTabOverlap:I
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView;->access$100(Lcom/sec/android/app/sbrowsertry/TbTabScrollView;)I

    move-result v2

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sbrowsertry/TbTabScrollView$TabLayout;->setMeasuredDimension(II)V

    .line 119
    return-void
.end method
