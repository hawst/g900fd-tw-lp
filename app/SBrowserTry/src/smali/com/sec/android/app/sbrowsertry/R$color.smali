.class public final Lcom/sec/android/app/sbrowsertry/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final addbookmark_account:I = 0x7f060000

.field public static final addbookmark_blueline:I = 0x7f060001

.field public static final addbookmark_foldername:I = 0x7f060002

.field public static final addbookmark_input_field_text_color:I = 0x7f060003

.field public static final alert_infobar_bg_separator:I = 0x7f060004

.field public static final alert_infobar_text:I = 0x7f060005

.field public static final create_new_tab_text_color:I = 0x7f060006

.field public static final expandable_group_background:I = 0x7f060007

.field public static final find_in_page_text_color:I = 0x7f060008

.field public static final find_result_bar_find_results_text_color:I = 0x7f060009

.field public static final footer_seperator:I = 0x7f06000a

.field public static final fre_text_color:I = 0x7f06000b

.field public static final guide_bubble_text_color:I = 0x7f06000c

.field public static final item_color:I = 0x7f06000d

.field public static final js_dialog_title_separator:I = 0x7f06000e

.field public static final light_background_color:I = 0x7f06000f

.field public static final locationbar_domain_and_registry:I = 0x7f060010

.field public static final locationbar_scheme_to_domain:I = 0x7f060011

.field public static final locationbar_start_scheme_default:I = 0x7f060012

.field public static final locationbar_start_scheme_ev_secure:I = 0x7f060013

.field public static final locationbar_start_scheme_secure:I = 0x7f060014

.field public static final locationbar_start_scheme_security_error:I = 0x7f060015

.field public static final locationbar_start_scheme_security_warning:I = 0x7f060016

.field public static final locationbar_trailing_url:I = 0x7f060017

.field public static final magnifier_color:I = 0x7f060018

.field public static final menu_item_text_color:I = 0x7f060019

.field public static final omnibox_divider:I = 0x7f06001a

.field public static final profile_edit_text_color:I = 0x7f06001b

.field public static final sbrowser_searchbar_url_text_color:I = 0x7f06001c

.field public static final sbrowser_tablet_model_switcher_text_color:I = 0x7f06001d

.field public static final sbrowser_toolbar_multi_window_text_color:I = 0x7f06001e

.field public static final sbrowser_toolbar_url_text_color:I = 0x7f06001f

.field public static final sbrowser_toolbar_url_text_color_incog:I = 0x7f060020

.field public static final sbrowser_toolbar_url_text_color_reader:I = 0x7f060021

.field public static final scrap_unread_text_color:I = 0x7f060022

.field public static final scraplist_background:I = 0x7f060023

.field public static final scraplist_child_divider:I = 0x7f060024

.field public static final scraplist_item_content:I = 0x7f060025

.field public static final scraplist_item_title:I = 0x7f060026

.field public static final search_bar_text_color:I = 0x7f060027

.field public static final settings_bg_color:I = 0x7f060028

.field public static final settings_headers_item_text_color:I = 0x7f060029

.field public static final settings_headers_text_color:I = 0x7f06002a

.field public static final tabBorder:I = 0x7f06002b

.field public static final tabFocusHighlight:I = 0x7f06002c

.field public static final tab_manager_background:I = 0x7f06002d

.field public static final tag_edittext_text_color:I = 0x7f06002e

.field public static final tag_name_pressed:I = 0x7f06002f

.field public static final tag_name_unpressed:I = 0x7f060030

.field public static final tag_underline_pressed:I = 0x7f060031

.field public static final tag_underline_unpressed:I = 0x7f060032

.field public static final text_size_view:I = 0x7f060033

.field public static final textsize_change:I = 0x7f060034

.field public static final title_color:I = 0x7f060035

.field public static final toolbar_bg_color:I = 0x7f060036

.field public static final toolbar_switcher_tab_count_color:I = 0x7f060037

.field public static final toolbar_tab_count_color:I = 0x7f060038

.field public static final toolbar_url_text_hintcolor:I = 0x7f06003a

.field public static final white:I = 0x7f060039

.field public static final whitetheme_secaddbookmark_bg:I = 0x7f06003b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
