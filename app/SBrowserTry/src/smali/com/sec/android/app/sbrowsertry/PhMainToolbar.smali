.class public Lcom/sec/android/app/sbrowsertry/PhMainToolbar;
.super Lcom/sec/android/app/sbrowsertry/SBrowserMainToolbar;
.source "PhMainToolbar.java"


# static fields
.field public static final CHINA:Ljava/lang/Boolean;


# instance fields
.field private bottomBar:Landroid/view/View;

.field private forwardBackwardLayout:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

.field private mPh_toolbar_Search_engine_favicon:Landroid/widget/ImageButton;

.field private mPh_toolbar_searchEngine:Landroid/widget/ImageButton;

.field private mph_toolbar_option_menu:Landroid/widget/ImageButton;

.field private multitabBookmarkLayout:Landroid/widget/FrameLayout;

.field private searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

.field private searchEngineLayout:Landroid/widget/FrameLayout;

.field private toolbarLayout:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    const-string v0, "China"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CHINA"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "china"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->CHINA:Ljava/lang/Boolean;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sbrowsertry/SBrowserMainToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mContext:Landroid/content/Context;

    .line 82
    check-cast p1, Lcom/sec/android/app/sbrowsertry/MainActivity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/PhMainToolbar;
    .param p1, "x1"    # Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)Lcom/sec/android/app/sbrowsertry/MainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->multitabBookmarkLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private translateAnimationFocusMultiTabBookmark()V
    .locals 4

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->multitabBookmarkLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->toolbarLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$4;-><init>(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 227
    return-void
.end method


# virtual methods
.method public dismissSearchEngineOptions()V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->dismiss()V

    .line 232
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 87
    invoke-super {p0}, Lcom/sec/android/app/sbrowsertry/SBrowserMainToolbar;->onFinishInflate()V

    .line 89
    const v0, 0x7f08008d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->toolbarLayout:Landroid/view/View;

    .line 102
    const v0, 0x7f080081

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngineLayout:Landroid/widget/FrameLayout;

    .line 103
    const v0, 0x7f080085

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->multitabBookmarkLayout:Landroid/widget/FrameLayout;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mLocationBar:Lcom/sec/android/app/sbrowsertry/LocationBar;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->setStateListener(Lcom/sec/android/app/sbrowsertry/LocationBar$StateListener;)V

    .line 106
    const v0, 0x7f080088

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mph_toolbar_option_menu:Landroid/widget/ImageButton;

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mph_toolbar_option_menu:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mph_toolbar_option_menu:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$1;-><init>(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    const v0, 0x7f080082

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mPh_toolbar_searchEngine:Landroid/widget/ImageButton;

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mPh_toolbar_searchEngine:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mPh_toolbar_searchEngine:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$2;-><init>(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    const v0, 0x7f080083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mPh_toolbar_Search_engine_favicon:Landroid/widget/ImageButton;

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mPh_toolbar_Search_engine_favicon:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mPh_toolbar_Search_engine_favicon:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar$3;-><init>(Lcom/sec/android/app/sbrowsertry/PhMainToolbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    return-void
.end method

.method public onOrientationChanged(I)V
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngine:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->onOrientationChanged()V

    .line 238
    :cond_0
    return-void
.end method

.method public onStateChanged(I)V
    .locals 7
    .param p1, "state"    # I

    .prologue
    const v6, 0x7f080081

    const v5, 0x7f08006d

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 30
    packed-switch p1, :pswitch_data_0

    .line 61
    :goto_0
    return-void

    .line 34
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mLocationBar:Lcom/sec/android/app/sbrowsertry/LocationBar;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07008f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->searchEngineLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->handleStateChange()V

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->multitabBookmarkLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 41
    sget-object v0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->CHINA:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 46
    :goto_1
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 47
    const v0, 0x7f08006f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 48
    const v0, 0x7f080068

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 49
    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 50
    const v0, 0x7f080082

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 51
    const v0, 0x7f080083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 52
    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    .line 44
    :cond_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 56
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->handleStateChange()V

    goto/16 :goto_0

    .line 30
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
