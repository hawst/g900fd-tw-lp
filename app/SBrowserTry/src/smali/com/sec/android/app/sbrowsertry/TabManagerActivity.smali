.class public Lcom/sec/android/app/sbrowsertry/TabManagerActivity;
.super Landroid/app/Activity;
.source "TabManagerActivity.java"


# instance fields
.field private mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const v0, 0x7f03002d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->setContentView(I)V

    .line 34
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const v1, 0x7f030010

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setContentView(I)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    sget-object v1, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setTouchTransparencyMode(Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;)V

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setShowWrongInputToast(Z)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->show()V

    .line 41
    const v0, 0x7f0800a2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sbrowsertry/TabManagerActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sbrowsertry/TabManagerActivity$1;-><init>(Lcom/sec/android/app/sbrowsertry/TabManagerActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 16
    const/4 v0, 0x4

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 19
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 21
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->setResult(I)V

    .line 22
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/TabManagerActivity;->finish()V

    .line 24
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method
