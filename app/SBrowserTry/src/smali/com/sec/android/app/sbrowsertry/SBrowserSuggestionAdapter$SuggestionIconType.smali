.class public final enum Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;
.super Ljava/lang/Enum;
.source "SBrowserSuggestionAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SuggestionIconType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

.field public static final enum BOOKMARK:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

.field public static final enum DEFAULT:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

.field public static final enum GLOBE:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

.field public static final enum HISTORY:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

.field public static final enum MAGNIFIER:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

.field public static final enum OPERATOR:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

.field public static final enum SCRAP:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

.field public static final enum TAG:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

.field public static final enum VOICE:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 244
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    const-string v1, "BOOKMARK"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->BOOKMARK:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    const-string v1, "HISTORY"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->HISTORY:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    const-string v1, "GLOBE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->GLOBE:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    const-string v1, "MAGNIFIER"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->MAGNIFIER:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    const-string v1, "TAG"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->TAG:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    const-string v1, "VOICE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->VOICE:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    .line 246
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    const-string v1, "SCRAP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->SCRAP:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    const-string v1, "OPERATOR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->OPERATOR:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    .line 247
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    const-string v1, "DEFAULT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->DEFAULT:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    .line 243
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    sget-object v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->BOOKMARK:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->HISTORY:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->GLOBE:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->MAGNIFIER:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->TAG:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->VOICE:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->SCRAP:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->OPERATOR:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->DEFAULT:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->$VALUES:[Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 243
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 243
    const-class v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;
    .locals 1

    .prologue
    .line 243
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->$VALUES:[Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    invoke-virtual {v0}, [Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sbrowsertry/SBrowserSuggestionAdapter$SuggestionIconType;

    return-object v0
.end method
