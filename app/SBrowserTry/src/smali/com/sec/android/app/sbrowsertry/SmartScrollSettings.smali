.class public Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;
.super Ljava/lang/Object;
.source "SmartScrollSettings.java"


# static fields
.field public static final SMART_SCROLL:I = 0x0

.field public static final SMART_SCROLL_DELAY:I = 0x10

.field public static final SMART_SCROLL_FACE:I = 0x1

.field public static final SMART_SCROLL_SPEED:I = 0xf

.field public static final SMART_SCROLL_TILT:I = 0x2


# instance fields
.field private mScrollType:I

.field private mSettingsContentResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "scrollType"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->mSettingsContentResolver:Landroid/content/ContentResolver;

    .line 44
    iput p2, p0, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->mScrollType:I

    .line 45
    return-void
.end method


# virtual methods
.method public checkSmartScroll()Z
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->mScrollType:I

    packed-switch v0, :pswitch_data_0

    .line 52
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 50
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->checkSmartScrollEnalbed()Z

    move-result v0

    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public checkSmartScrollEnalbed()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 57
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->mSettingsContentResolver:Landroid/content/ContentResolver;

    const-string v3, "smart_scroll"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 62
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public checkSmartScrollMode()I
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->checkSmartScrollEnalbed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 68
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->mSettingsContentResolver:Landroid/content/ContentResolver;

    const-string v3, "face_smart_scroll"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 74
    :goto_0
    return v0

    .line 71
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    move v0, v1

    .line 74
    goto :goto_0
.end method

.method public checkVisualFeedbackEnabled()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 86
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->mSettingsContentResolver:Landroid/content/ContentResolver;

    const-string v3, "smart_scroll"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->mSettingsContentResolver:Landroid/content/ContentResolver;

    const-string v3, "smart_scroll_visual_feedback_icon"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 91
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public getSpeed()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->mSettingsContentResolver:Landroid/content/ContentResolver;

    const-string v1, "smart_scroll"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->mSettingsContentResolver:Landroid/content/ContentResolver;

    const-string v1, "smart_scroll_sensitivity"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 81
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xf

    goto :goto_0
.end method
