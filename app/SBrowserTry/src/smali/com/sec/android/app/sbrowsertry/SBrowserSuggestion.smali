.class public Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;
.super Ljava/lang/Object;
.source "SBrowserSuggestion.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$1;,
        Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;,
        Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;
    }
.end annotation


# static fields
.field public static final URL_WHAT_YOU_TYPED:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;


# instance fields
.field private final mDescription:Ljava/lang/String;

.field private final mDisplayText:Ljava/lang/String;

.field private final mIsScrap:Z

.field private final mIsScrapRead:Z

.field private final mIsStarred:Z

.field private final mIsTagged:Z

.field private final mRelevance:I

.field private final mScrapId:I

.field private final mScrapURL:Ljava/lang/String;

.field private final mTransition:I

.field private final mType:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

.field private final mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->URL_WHAT_YOU_TYPED:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    return-void
.end method

.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "relevance"    # I
    .param p3, "page_transition"    # I
    .param p4, "content"    # Ljava/lang/String;
    .param p5, "description"    # Ljava/lang/String;
    .param p6, "url"    # Ljava/lang/String;
    .param p7, "flag"    # Z
    .param p8, "tagged"    # Z

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {p1}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->GetNativeType(I)Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mType:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    .line 40
    iput p2, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mRelevance:I

    .line 41
    iput p3, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mTransition:I

    .line 42
    iput-object p4, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mDisplayText:Ljava/lang/String;

    .line 43
    iput-object p5, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mDescription:Ljava/lang/String;

    .line 44
    iput-object p6, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mUrl:Ljava/lang/String;

    .line 45
    iput-boolean p7, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsStarred:Z

    .line 46
    iput-boolean p8, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsTagged:Z

    .line 48
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsScrap:Z

    .line 49
    iput v1, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mScrapId:I

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mScrapURL:Ljava/lang/String;

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsScrapRead:Z

    .line 52
    return-void
.end method

.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZILjava/lang/String;Z)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "relevance"    # I
    .param p3, "page_transition"    # I
    .param p4, "content"    # Ljava/lang/String;
    .param p5, "description"    # Ljava/lang/String;
    .param p6, "url"    # Ljava/lang/String;
    .param p7, "flag"    # Z
    .param p8, "tagged"    # Z
    .param p9, "isScrap"    # Z
    .param p10, "scrapId"    # I
    .param p11, "scrapURL"    # Ljava/lang/String;
    .param p12, "isScrapRead"    # Z

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {p1}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->GetNativeType(I)Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mType:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    .line 59
    iput p2, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mRelevance:I

    .line 60
    iput p3, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mTransition:I

    .line 61
    iput-object p4, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mDisplayText:Ljava/lang/String;

    .line 62
    iput-object p5, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mDescription:Ljava/lang/String;

    .line 63
    iput-object p6, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mUrl:Ljava/lang/String;

    .line 64
    iput-boolean p7, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsStarred:Z

    .line 65
    iput-boolean p8, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsTagged:Z

    .line 66
    iput-boolean p9, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsScrap:Z

    .line 67
    iput p10, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mScrapId:I

    .line 68
    iput-object p11, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mScrapURL:Ljava/lang/String;

    .line 69
    iput-boolean p12, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsScrapRead:Z

    .line 70
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 73
    const/4 v0, 0x0

    .line 74
    .local v0, "flag":Z
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 75
    check-cast v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;

    .line 76
    .local v1, "suggestion":Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mType:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    iget-object v3, v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mType:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mDisplayText:Ljava/lang/String;

    iget-object v3, v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mDisplayText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsStarred:Z

    iget-boolean v3, v1, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsStarred:Z

    if-ne v2, v3, :cond_0

    .line 79
    const/4 v0, 0x1

    .line 81
    .end local v1    # "suggestion":Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;
    :cond_0
    return v0
.end method

.method public getDisplayText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mDisplayText:Ljava/lang/String;

    return-object v0
.end method

.method public getPageTransition()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mTransition:I

    return v0
.end method

.method public getRelevance()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mRelevance:I

    return v0
.end method

.method public getSBrowserSuggestionOrder()Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;
    .locals 2

    .prologue
    .line 167
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$1;->$SwitchMap$com$sec$android$app$sbrowsertry$SBrowserSuggestion$Type:[I

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mType:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    invoke-virtual {v1}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 196
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->DIRECT:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    :goto_0
    return-object v0

    .line 169
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->BOOKMARK:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    goto :goto_0

    .line 171
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->SCRAP:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    goto :goto_0

    .line 184
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsStarred:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsTagged:Z

    if-eqz v0, :cond_1

    .line 185
    :cond_0
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->BOOKMARK:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    goto :goto_0

    .line 187
    :cond_1
    sget-object v0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;->HISTORY:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$SBrowserSuggestionOrder;

    goto :goto_0

    .line 167
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public getScrapId()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mScrapId:I

    return v0
.end method

.method public getScrapURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mScrapURL:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mType:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getsuggestionUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 109
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mType:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    # getter for: Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->mNativeType:I
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->access$000(Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;)I

    move-result v2

    mul-int/lit8 v0, v2, 0x25

    .line 110
    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mDisplayText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 111
    .local v1, "j":I
    add-int v2, v0, v1

    return v2
.end method

.method public isScrap()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsScrap:Z

    return v0
.end method

.method public isScrapRead()Z
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsScrapRead:Z

    return v0
.end method

.method public isStarred()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsStarred:Z

    return v0
.end method

.method public isTagged()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mIsTagged:Z

    return v0
.end method

.method public isUrlSuggestion()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mType:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;->isUrl()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mType:Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " relevance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mRelevance:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mDisplayText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SBrowserSuggestion;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
