.class public final Lcom/sec/android/app/sbrowsertry/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final OK:I = 0x7f080022

.field public static final PAGE_MENU:I = 0x7f0800ad

.field public static final TAB_MANAGER_MENU:I = 0x7f0800bc

.field public static final account_name:I = 0x7f080015

.field public static final action_button_add_bookmark:I = 0x7f0800c2

.field public static final add_bookmark_activity_header_icon:I = 0x7f080014

.field public static final add_bookmark_container:I = 0x7f08001d

.field public static final add_bookmark_header_icon:I = 0x7f08001e

.field public static final add_bookmark_id:I = 0x7f0800b0

.field public static final add_bookmark_page_title:I = 0x7f08001b

.field public static final add_bookmark_page_title_input:I = 0x7f08001c

.field public static final add_bookmark_page_url:I = 0x7f080019

.field public static final add_bookmark_page_url_input:I = 0x7f08001a

.field public static final add_bookmark_url_layout:I = 0x7f080018

.field public static final add_shortcut_id:I = 0x7f0800b2

.field public static final arrow:I = 0x7f08001f

.field public static final basic_bottom_bar:I = 0x7f080024

.field public static final basic_toolbar:I = 0x7f08007c

.field public static final bookmark_cancel_id:I = 0x7f0800ab

.field public static final bookmark_cancel_id_action:I = 0x7f080007

.field public static final bookmark_done_id:I = 0x7f0800ac

.field public static final bookmark_done_id_action:I = 0x7f080009

.field public static final bookmarks_id:I = 0x7f0800b1

.field public static final bottom:I = 0x7f080001

.field public static final bubble_text:I = 0x7f080041

.field public static final cancel:I = 0x7f080020

.field public static final cancel_textview_bookmark:I = 0x7f080008

.field public static final center:I = 0x7f080002

.field public static final close:I = 0x7f08007b

.field public static final complete:I = 0x7f080039

.field public static final create_tab:I = 0x7f080076

.field public static final desktop_view_id:I = 0x7f0800ba

.field public static final divider:I = 0x7f08009b

.field public static final dividerBeforeNewTab:I = 0x7f0800a1

.field public static final divider_before_ok:I = 0x7f080021

.field public static final done_textview_bookmark:I = 0x7f08000a

.field public static final easy_mode_layout:I = 0x7f080089

.field public static final easymode_toolbar_backward:I = 0x7f08008a

.field public static final easymode_toolbar_backward_horizontal:I = 0x7f08007e

.field public static final easymode_toolbar_forward:I = 0x7f08008b

.field public static final easymode_toolbar_forward_horizontal:I = 0x7f08007f

.field public static final easymode_toolbar_homepage:I = 0x7f08008c

.field public static final easymode_toolbar_homepage_horizontal:I = 0x7f080080

.field public static final eyerecog_image:I = 0x7f08004b

.field public static final fast_text:I = 0x7f080045

.field public static final favicon:I = 0x7f080079

.field public static final fill:I = 0x7f080003

.field public static final find_on_page_id:I = 0x7f0800b5

.field public static final folder:I = 0x7f080017

.field public static final folderImage:I = 0x7f080016

.field public static final folderLine:I = 0x7f080012

.field public static final forward_backward_layout:I = 0x7f08008e

.field public static final frameLayout1:I = 0x7f080056

.field public static final frameLayout2:I = 0x7f080058

.field public static final frameLayout3:I = 0x7f08005b

.field public static final frameLayout4:I = 0x7f08005e

.field public static final frameLayout5:I = 0x7f080061

.field public static final goto_homepage_id:I = 0x7f0800ae

.field public static final guide_bubble_create_new_tab:I = 0x7f08002e

.field public static final guide_bubble_summary:I = 0x7f08002d

.field public static final guide_bubble_summary_step_1:I = 0x7f080031

.field public static final guide_bubble_summary_step_2:I = 0x7f080033

.field public static final guide_bubble_summary_step_3:I = 0x7f08003b

.field public static final guide_bubble_summary_step_initial:I = 0x7f080035

.field public static final guide_hand_pointer:I = 0x7f080000

.field public static final guide_icon_text:I = 0x7f08004e

.field public static final guide_picker_step_1:I = 0x7f08002f

.field public static final guide_picker_step_2:I = 0x7f080034

.field public static final guide_picker_step_3:I = 0x7f08003c

.field public static final guide_punch:I = 0x7f08002b

.field public static final guide_punch_step_1:I = 0x7f080030

.field public static final guide_punch_step_2:I = 0x7f080032

.field public static final guide_punch_step_3:I = 0x7f08003a

.field public static final header_container:I = 0x7f080054

.field public static final history_id:I = 0x7f0800b8

.field public static final hover_here:I = 0x7f080037

.field public static final icon1:I = 0x7f08009a

.field public static final icon2:I = 0x7f08009c

.field public static final imageList:I = 0x7f0800a5

.field public static final imageView1:I = 0x7f080051

.field public static final imageView2:I = 0x7f080059

.field public static final imageView3:I = 0x7f08005c

.field public static final imageView4:I = 0x7f08005f

.field public static final incognito:I = 0x7f080078

.field public static final left:I = 0x7f080004

.field public static final magnifierPopup:I = 0x7f08000e

.field public static final main_frame:I = 0x7f08004f

.field public static final main_frame_air_scroll:I = 0x7f080050

.field public static final main_id_lay:I = 0x7f08000c

.field public static final main_image:I = 0x7f08000d

.field public static final main_toolbar:I = 0x7f08000b

.field public static final more:I = 0x7f080013

.field public static final multi_window_screen:I = 0x7f08009d

.field public static final multiwindow_add_new_tab_text:I = 0x7f0800a4

.field public static final navigation_button:I = 0x7f08006b

.field public static final navigation_fake_hint:I = 0x7f080069

.field public static final negbuton:I = 0x7f080047

.field public static final new_window_id:I = 0x7f0800af

.field public static final newtab:I = 0x7f080072

.field public static final no_tab_icon:I = 0x7f0800a7

.field public static final no_tab_title:I = 0x7f080075

.field public static final no_tab_title_icon:I = 0x7f080074

.field public static final no_tab_title_layout:I = 0x7f080073

.field public static final no_tabs_layout:I = 0x7f0800a6

.field public static final ph_bottombar_home:I = 0x7f080027

.field public static final ph_reader_list:I = 0x7f080029

.field public static final ph_reader_list_layout:I = 0x7f080028

.field public static final ph_toolbar_backward:I = 0x7f080025

.field public static final ph_toolbar_bookmarks:I = 0x7f08002a

.field public static final ph_toolbar_dividerBefore_forward:I = 0x7f08007d

.field public static final ph_toolbar_forward:I = 0x7f080026

.field public static final ph_toolbar_multiwindow:I = 0x7f080086

.field public static final ph_toolbar_multiwindow_count:I = 0x7f080087

.field public static final ph_toolbar_multiwindow_layout:I = 0x7f080085

.field public static final ph_toolbar_option_menu:I = 0x7f080088

.field public static final ph_toolbar_search_engine:I = 0x7f080082

.field public static final ph_toolbar_search_engine_favicon:I = 0x7f080083

.field public static final ph_toolbar_search_engine_layout:I = 0x7f080081

.field public static final posbuton:I = 0x7f080049

.field public static final print_id:I = 0x7f0800b9

.field public static final private_mode_id:I = 0x7f0800b6

.field public static final quicklaunch_title:I = 0x7f080057

.field public static final quicklaunch_title2:I = 0x7f08005a

.field public static final quicklaunch_title3:I = 0x7f08005d

.field public static final quicklaunch_title4:I = 0x7f080060

.field public static final reader_list_image:I = 0x7f080011

.field public static final reader_more:I = 0x7f0800aa

.field public static final right:I = 0x7f080005

.field public static final rot_stay_image:I = 0x7f08003d

.field public static final sbrowser_bottom_container:I = 0x7f08000f

.field public static final sbrowser_bottombar:I = 0x7f080010

.field public static final sbrowser_location_bar:I = 0x7f080084

.field public static final sbrowser_location_bar_container:I = 0x7f080067

.field public static final sbrowser_mic_button:I = 0x7f08006d

.field public static final sbrowser_toolbar_deleteurl:I = 0x7f08006f

.field public static final sbrowser_toolbar_layout:I = 0x7f08008d

.field public static final sbrowser_toolbar_main_layout:I = 0x7f080023

.field public static final sbrowser_toolbar_reload:I = 0x7f08006e

.field public static final sbrowser_toolbar_search:I = 0x7f080068

.field public static final sbrowser_toolbar_star:I = 0x7f08006a

.field public static final sbrowser_url_bar:I = 0x7f08006c

.field public static final scrap_id:I = 0x7f0800b3

.field public static final scrap_list_id:I = 0x7f0800b7

.field public static final scroll_view:I = 0x7f08004a

.field public static final scrollque_detected:I = 0x7f08004d

.field public static final scrollque_image:I = 0x7f08004c

.field public static final scrolltry:I = 0x7f08003f

.field public static final search_engine_row_icon:I = 0x7f080097

.field public static final search_engine_row_layout:I = 0x7f080096

.field public static final search_engine_row_title:I = 0x7f080098

.field public static final seekbar:I = 0x7f080046

.field public static final seekbar_set_linear:I = 0x7f080043

.field public static final sensitivity_seekbar:I = 0x7f080048

.field public static final settings_id:I = 0x7f0800bb

.field public static final share_id:I = 0x7f0800b4

.field public static final slow_text:I = 0x7f080044

.field public static final speed_text:I = 0x7f080042

.field public static final suggestion:I = 0x7f080099

.field public static final sweep:I = 0x7f080053

.field public static final sweep_layout:I = 0x7f080052

.field public static final tab_header:I = 0x7f080055

.field public static final tab_manager_close_all:I = 0x7f0800be

.field public static final tab_manager_menu_spinner:I = 0x7f0800a9

.field public static final tab_manager_new_incognito_tab:I = 0x7f0800bd

.field public static final tab_manager_newtab:I = 0x7f0800a3

.field public static final tab_manager_sync_tabs:I = 0x7f0800bf

.field public static final tab_manager_toolbar:I = 0x7f08009e

.field public static final tabbar_bg:I = 0x7f080077

.field public static final tabbarcontent:I = 0x7f080070

.field public static final tabmanager_back_layout_parent:I = 0x7f08009f

.field public static final tabmanager_new_tab_layout:I = 0x7f0800a2

.field public static final tabmanager_options_layout:I = 0x7f0800a8

.field public static final tabs:I = 0x7f080071

.field public static final tb_toolbar_backward:I = 0x7f08008f

.field public static final tb_toolbar_bookmarks:I = 0x7f080095

.field public static final tb_toolbar_forward:I = 0x7f080090

.field public static final tb_toolbar_home:I = 0x7f080091

.field public static final tb_toolbar_search_engine:I = 0x7f080093

.field public static final tb_toolbar_search_engine_favicon:I = 0x7f080094

.field public static final tb_toolbar_search_engine_layout:I = 0x7f080092

.field public static final textView1:I = 0x7f080062

.field public static final textView2:I = 0x7f080063

.field public static final textView3:I = 0x7f080064

.field public static final textView4:I = 0x7f080065

.field public static final textView5:I = 0x7f080066

.field public static final tip_picker:I = 0x7f080038

.field public static final title:I = 0x7f08007a

.field public static final top:I = 0x7f080006

.field public static final twTouchPunchView:I = 0x7f080036

.field public static final twTouchPunchView1:I = 0x7f08002c

.field public static final vScreenView:I = 0x7f08003e

.field public static final visual_que_text:I = 0x7f080040

.field public static final window_manager:I = 0x7f0800a0

.field public static final zoomin:I = 0x7f0800c0

.field public static final zoomout:I = 0x7f0800c1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
