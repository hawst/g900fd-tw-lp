.class public final Lcom/sec/android/app/sbrowsertry/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final Black:I = 0x7f0200ff

.field public static final add_bookmark_menu:I = 0x7f020000

.field public static final air_scroll_image:I = 0x7f020001

.field public static final bing:I = 0x7f020002

.field public static final bottombar_reading_list_selector:I = 0x7f020003

.field public static final bottombar_selector:I = 0x7f020004

.field public static final browser_help_default:I = 0x7f020005

.field public static final browser_icon_favorite_activate:I = 0x7f020006

.field public static final browser_icon_favorite_clear:I = 0x7f020007

.field public static final browser_spinner_default_holo_light:I = 0x7f020008

.field public static final browser_spinner_pressed_holo_light:I = 0x7f020009

.field public static final browser_tab_active:I = 0x7f02000a

.field public static final browser_tab_inactive:I = 0x7f02000b

.field public static final browser_tab_press:I = 0x7f02000c

.field public static final browser_tab_pressed:I = 0x7f02000d

.field public static final browser_textfield_default_holo_light:I = 0x7f02000e

.field public static final browser_textfield_focused_holo_light:I = 0x7f02000f

.field public static final browsertab_active:I = 0x7f020010

.field public static final browsertab_add:I = 0x7f020011

.field public static final browsertab_add_focused:I = 0x7f020012

.field public static final browsertab_add_press:I = 0x7f020013

.field public static final browsertab_inactive:I = 0x7f020014

.field public static final browsertab_pressed:I = 0x7f020015

.field public static final bt_fast_default:I = 0x7f020016

.field public static final bt_fast_default_selector:I = 0x7f020017

.field public static final bt_fast_pressed:I = 0x7f020018

.field public static final bt_slow_default:I = 0x7f020019

.field public static final bt_slow_default_selector:I = 0x7f02001a

.field public static final bt_slow_pressed:I = 0x7f02001b

.field public static final btn_imageview_star:I = 0x7f02001c

.field public static final delete_stop_url_toggle_selector:I = 0x7f02001d

.field public static final dropdown_arrow:I = 0x7f02001e

.field public static final dropdown_bookmark:I = 0x7f02001f

.field public static final dropdown_history:I = 0x7f020020

.field public static final dropdown_keyword:I = 0x7f020021

.field public static final dropdown_scrap:I = 0x7f020022

.field public static final dropdown_url:I = 0x7f020023

.field public static final google:I = 0x7f020024

.field public static final help_hover:I = 0x7f020025

.field public static final help_popup_picker_bg_w_01:I = 0x7f020026

.field public static final help_popup_picker_t_c:I = 0x7f020027

.field public static final help_tap:I = 0x7f020028

.field public static final home_menu:I = 0x7f020029

.field public static final ic_back_disabled_holo_dark:I = 0x7f02002a

.field public static final ic_back_holo_dark:I = 0x7f02002b

.field public static final ic_bookmark_folder_list_small:I = 0x7f02002c

.field public static final ic_bookmark_holo_dark:I = 0x7f02002d

.field public static final ic_bookmark_holo_dark_disable:I = 0x7f02002e

.field public static final ic_browser_reader:I = 0x7f02002f

.field public static final ic_browser_reader_focus_press:I = 0x7f020030

.field public static final ic_btn_round_more:I = 0x7f020031

.field public static final ic_cancel_holo_dark:I = 0x7f020032

.field public static final ic_forward_disabled_holo_dark:I = 0x7f020033

.field public static final ic_forward_holo_dark:I = 0x7f020034

.field public static final ic_home_holo_dark:I = 0x7f020035

.field public static final ic_incognito_holo_dark:I = 0x7f020036

.field public static final ic_refresh_holo_dark:I = 0x7f020037

.field public static final ic_search_holo_dark:I = 0x7f020038

.field public static final ic_settings:I = 0x7f020039

.field public static final ic_tab_add:I = 0x7f02003a

.field public static final ic_tab_close:I = 0x7f02003b

.field public static final ic_voice_holo_dark:I = 0x7f02003c

.field public static final imternet_dropdown_icon_bg:I = 0x7f02003d

.field public static final imternet_progress:I = 0x7f02003e

.field public static final imternet_progress_bg:I = 0x7f02003f

.field public static final initializing_04:I = 0x7f020040

.field public static final internet_actionbar_edit_mode_bg:I = 0x7f020041

.field public static final internet_add_multi_windows:I = 0x7f020042

.field public static final internet_book_mark:I = 0x7f020043

.field public static final internet_book_mark_disable:I = 0x7f020044

.field public static final internet_dropdown_dv:I = 0x7f020045

.field public static final internet_dropdown_ic_arrow:I = 0x7f020046

.field public static final internet_dropdown_ic_browser:I = 0x7f020047

.field public static final internet_dropdown_ic_clock:I = 0x7f020048

.field public static final internet_dropdown_ic_favorite:I = 0x7f020049

.field public static final internet_dropdown_ic_scrap:I = 0x7f02004a

.field public static final internet_dropdown_ic_search:I = 0x7f02004b

.field public static final internet_easy_back_btn:I = 0x7f02004c

.field public static final internet_easy_back_btn_disabled:I = 0x7f02004d

.field public static final internet_easy_forward_btn:I = 0x7f02004e

.field public static final internet_easy_forward_btn_disabled:I = 0x7f02004f

.field public static final internet_favicon_default:I = 0x7f020050

.field public static final internet_ic_cancel:I = 0x7f020051

.field public static final internet_ic_cancel_disable:I = 0x7f020052

.field public static final internet_ic_cancel_selector:I = 0x7f020053

.field public static final internet_ic_refresh_disabled:I = 0x7f020054

.field public static final internet_ic_search:I = 0x7f020055

.field public static final internet_ic_voice:I = 0x7f020056

.field public static final internet_ic_voice_disable:I = 0x7f020057

.field public static final internet_icon_device:I = 0x7f020058

.field public static final internet_icon_device_small:I = 0x7f020059

.field public static final internet_icon_focus:I = 0x7f02005a

.field public static final internet_lock_icon:I = 0x7f02005b

.field public static final internet_lock_icon_on:I = 0x7f02005c

.field public static final internet_manager_01_perspective_gradient:I = 0x7f02005d

.field public static final internet_multi_windows:I = 0x7f02005e

.field public static final internet_multi_windows_disable:I = 0x7f02005f

.field public static final internet_star_off:I = 0x7f020060

.field public static final internet_star_on:I = 0x7f020061

.field public static final location_bar_item_bg:I = 0x7f020062

.field public static final magnifier_try_bg:I = 0x7f020063

.field public static final magnifier_try_popup:I = 0x7f020064

.field public static final magnifying_glass:I = 0x7f020065

.field public static final mic_selector:I = 0x7f020066

.field public static final motion_sensitivity_bg:I = 0x7f020067

.field public static final noitems_browser_synctab:I = 0x7f020068

.field public static final optionmenu_icon_add:I = 0x7f020069

.field public static final optionmenu_icon_menu:I = 0x7f02006a

.field public static final optionmenu_icon_menu_dim:I = 0x7f02006b

.field public static final optionmenu_icon_multi_windows:I = 0x7f02006c

.field public static final optionmenu_icon_share:I = 0x7f02006d

.field public static final optionmenu_icon_text_l:I = 0x7f02006e

.field public static final optionmenu_icon_text_s:I = 0x7f02006f

.field public static final optionmenu_secret_icon_menu:I = 0x7f020070

.field public static final options_menu_add_shortcut:I = 0x7f020071

.field public static final options_menu_desktop_view:I = 0x7f020072

.field public static final options_menu_find_on_page:I = 0x7f020073

.field public static final options_menu_history:I = 0x7f020074

.field public static final options_menu_new_window:I = 0x7f020075

.field public static final options_menu_print:I = 0x7f020076

.field public static final options_menu_private_mode:I = 0x7f020077

.field public static final options_menu_scrap:I = 0x7f020078

.field public static final options_menu_scrap_list:I = 0x7f020079

.field public static final options_menu_settings:I = 0x7f02007a

.field public static final options_menu_share:I = 0x7f02007b

.field public static final ph_toolbar_backward_selector:I = 0x7f02007c

.field public static final ph_toolbar_bookmark_selector:I = 0x7f02007d

.field public static final ph_toolbar_forward_selector:I = 0x7f02007e

.field public static final ph_toolbar_multiwindow_selector:I = 0x7f02007f

.field public static final popup_nor:I = 0x7f020080

.field public static final popup_sel:I = 0x7f020081

.field public static final popup_selector:I = 0x7f020082

.field public static final progressdrawable:I = 0x7f020083

.field public static final quick_1_image:I = 0x7f020084

.field public static final quick_2_image:I = 0x7f020085

.field public static final quick_3_image:I = 0x7f020086

.field public static final quick_4_image:I = 0x7f020087

.field public static final quick_5_image:I = 0x7f020088

.field public static final reader_selector:I = 0x7f020089

.field public static final refresh_button_selector:I = 0x7f02008a

.field public static final s_browser_ab_drop_down_default:I = 0x7f02008b

.field public static final s_browser_action_bar_divider:I = 0x7f02008c

.field public static final s_browser_action_bar_secret_divider:I = 0x7f02008d

.field public static final s_browser_actionbar_bg:I = 0x7f02008e

.field public static final s_browser_actionbar_bottom_bg:I = 0x7f02008f

.field public static final s_browser_actionbar_bottom_press:I = 0x7f020090

.field public static final s_browser_backward:I = 0x7f020091

.field public static final s_browser_backward_disable:I = 0x7f020092

.field public static final s_browser_forward:I = 0x7f020093

.field public static final s_browser_forward_disable:I = 0x7f020094

.field public static final s_browser_home:I = 0x7f020095

.field public static final s_browser_home_disable:I = 0x7f020096

.field public static final s_browser_ic_refresh:I = 0x7f020097

.field public static final s_browser_list_section_divider_holo_light:I = 0x7f020098

.field public static final s_browser_multi_windows:I = 0x7f020099

.field public static final s_browser_multi_windows_disable:I = 0x7f02009a

.field public static final s_browser_tab_divider:I = 0x7f02009b

.field public static final sbrowser_actionbar_bg:I = 0x7f02009c

.field public static final sbrowser_add_tab_selector:I = 0x7f02009d

.field public static final sbrowser_icon:I = 0x7f02009e

.field public static final scroll_que:I = 0x7f02009f

.field public static final selectable_item_bg:I = 0x7f0200a0

.field public static final shape_title:I = 0x7f0200a1

.field public static final shape_title2:I = 0x7f0200a2

.field public static final tab_bg:I = 0x7f0200a3

.field public static final tab_browser_logo:I = 0x7f0200a4

.field public static final tablet_new_tab:I = 0x7f0200a5

.field public static final tag_add:I = 0x7f0200a6

.field public static final tag_add_focused:I = 0x7f0200a7

.field public static final tag_add_pressed:I = 0x7f0200a8

.field public static final tag_button_add_pressed:I = 0x7f0200a9

.field public static final tag_button_delete_pressed:I = 0x7f0200aa

.field public static final tag_cancel:I = 0x7f0200ab

.field public static final tag_cancel_on:I = 0x7f0200ac

.field public static final tb_toolbar_backward_selector:I = 0x7f0200ad

.field public static final tb_toolbar_bookmark_selector:I = 0x7f0200ae

.field public static final tb_toolbar_forward_selector:I = 0x7f0200af

.field public static final tb_toolbar_mic_selector:I = 0x7f0200b0

.field public static final tb_toolbar_reload_selector:I = 0x7f0200b1

.field public static final tb_toolbar_search_engine_selector:I = 0x7f0200b2

.field public static final tb_toolbar_stop_selector:I = 0x7f0200b3

.field public static final toobar_menu_icon_selector:I = 0x7f0200b4

.field public static final toolbar_bg_normal:I = 0x7f0200b5

.field public static final toolbar_easymode_backward_selector:I = 0x7f0200b6

.field public static final toolbar_easymode_forward_selector:I = 0x7f0200b7

.field public static final toolbar_easymode_homepage_selector:I = 0x7f0200b8

.field public static final toolbar_magnify:I = 0x7f0200b9

.field public static final toolbar_search_engine_bg_selector:I = 0x7f0200ba

.field public static final toolbar_search_engine_spinner:I = 0x7f0200bb

.field public static final toolbar_textfield_bg_default:I = 0x7f0200bc

.field public static final tw_ab_transparent_dark_holo:I = 0x7f0200bd

.field public static final tw_ab_transparent_dark_holo_background:I = 0x7f0200be

.field public static final tw_action_bar_add_bookmark:I = 0x7f0200bf

.field public static final tw_action_bar_add_bookmark_disable:I = 0x7f0200c0

.field public static final tw_action_bar_clear_history:I = 0x7f0200c1

.field public static final tw_action_bar_clear_history_disable:I = 0x7f0200c2

.field public static final tw_action_bar_desktop_view:I = 0x7f0200c3

.field public static final tw_action_bar_desktop_view_disable:I = 0x7f0200c4

.field public static final tw_action_bar_find_page:I = 0x7f0200c5

.field public static final tw_action_bar_find_page_disable:I = 0x7f0200c6

.field public static final tw_action_bar_icon_cancel_02_holo_light:I = 0x7f0200c7

.field public static final tw_action_bar_icon_check_holo_light:I = 0x7f0200c8

.field public static final tw_action_bar_icon_home_disabled_holo_dark:I = 0x7f0200c9

.field public static final tw_action_bar_icon_home_holo_dark:I = 0x7f0200ca

.field public static final tw_action_bar_icon_new_add_holo_light:I = 0x7f0200cb

.field public static final tw_action_bar_icon_shortcut:I = 0x7f0200cc

.field public static final tw_action_bar_icon_shortcut_disable:I = 0x7f0200cd

.field public static final tw_action_bar_new_icognito:I = 0x7f0200ce

.field public static final tw_action_bar_new_icognito_disable:I = 0x7f0200cf

.field public static final tw_action_bar_newwindow:I = 0x7f0200d0

.field public static final tw_action_bar_newwindow_disable:I = 0x7f0200d1

.field public static final tw_action_bar_print:I = 0x7f0200d2

.field public static final tw_action_bar_print_disable:I = 0x7f0200d3

.field public static final tw_action_bar_save_for_later:I = 0x7f0200d4

.field public static final tw_action_bar_save_for_later_disable:I = 0x7f0200d5

.field public static final tw_action_bar_saved_page:I = 0x7f0200d6

.field public static final tw_action_bar_saved_page_disable:I = 0x7f0200d7

.field public static final tw_action_bar_settings:I = 0x7f0200d8

.field public static final tw_action_bar_settings_disable:I = 0x7f0200d9

.field public static final tw_action_bar_share_page:I = 0x7f0200da

.field public static final tw_action_bar_share_page_disable:I = 0x7f0200db

.field public static final tw_btn_default_focused_holo_light:I = 0x7f0200dc

.field public static final tw_btn_default_normal_holo_light:I = 0x7f0200dd

.field public static final tw_btn_default_pressed_holo_light:I = 0x7f0200de

.field public static final tw_btn_default_selected_holo_light:I = 0x7f0200df

.field public static final tw_btn_next_depth_disabled_focused_holo_dark:I = 0x7f0200e0

.field public static final tw_btn_next_depth_disabled_focused_holo_light:I = 0x7f0200e1

.field public static final tw_btn_next_depth_disabled_holo_dark:I = 0x7f0200e2

.field public static final tw_btn_next_depth_disabled_holo_light:I = 0x7f0200e3

.field public static final tw_btn_next_depth_focused_holo_dark:I = 0x7f0200e4

.field public static final tw_btn_next_depth_focused_holo_light:I = 0x7f0200e5

.field public static final tw_btn_next_depth_holo_dark:I = 0x7f0200e6

.field public static final tw_btn_next_depth_holo_light:I = 0x7f0200e7

.field public static final tw_btn_next_depth_pressed_holo_dark:I = 0x7f0200e8

.field public static final tw_btn_next_depth_pressed_holo_light:I = 0x7f0200e9

.field public static final tw_btn_selector_holo_light:I = 0x7f0200ea

.field public static final tw_dialog_bottom_bold_holo_light:I = 0x7f0200eb

.field public static final tw_dialog_bottom_medium_holo_light:I = 0x7f0200ec

.field public static final tw_dialog_button_line_light:I = 0x7f0200ed

.field public static final tw_divider_vertical_holo_dark:I = 0x7f0200ee

.field public static final tw_ic_menu_list_menu_holo_light:I = 0x7f0200ef

.field public static final tw_no_item_popup_bg:I = 0x7f0200f0

.field public static final tw_spinner_ab_default_holo_dark:I = 0x7f0200f1

.field public static final tw_spinner_ab_default_holo_light:I = 0x7f0200f2

.field public static final tw_spinner_ab_disabled_holo_light:I = 0x7f0200f3

.field public static final tw_spinner_ab_focused_holo_light:I = 0x7f0200f4

.field public static final tw_spinner_ab_holo_light:I = 0x7f0200f5

.field public static final tw_spinner_ab_pressed_holo_light:I = 0x7f0200f6

.field public static final tw_spinner_internet_ab_default_holo_light:I = 0x7f0200f7

.field public static final tw_tab_selected_focused_holo_dark:I = 0x7f0200f8

.field public static final tw_tab_selected_focused_holo_light:I = 0x7f0200f9

.field public static final tw_tab_selected_pressed_holo_dark:I = 0x7f0200fa

.field public static final tw_textfield_search_default_holo_light:I = 0x7f0200fb

.field public static final tw_textfield_search_focused_holo_light:I = 0x7f0200fc

.field public static final visual_que_icon:I = 0x7f0200fd

.field public static final webpage_bg:I = 0x7f0200fe


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
