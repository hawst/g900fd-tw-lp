.class Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;
.super Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;
.source "SmartScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;-><init>()V

    return-void
.end method


# virtual methods
.method public getMaxScrollSize()I
    .locals 3

    .prologue
    .line 141
    const/4 v0, 0x0

    .line 142
    .local v0, "mMaxScroll":I
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollReady:I
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$100(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getMaxScrollOffset()I

    move-result v0

    .line 146
    :cond_0
    return v0
.end method

.method public isSmartScrollEnabled()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$000(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$000(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->checkSmartScroll()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showScrollGuide()V
    .locals 6

    .prologue
    const v5, 0x7f080041

    const/4 v4, 0x1

    .line 79
    const-string v0, "SmartScrollTry"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SCROLL - showScrollGuide() : mScrollReady="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollReady:I
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$100(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$200(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollReady:I
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$100(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030020

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v0, v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$302(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;Landroid/view/View;)Landroid/view/View;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$400(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$300(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f09002a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 93
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$300(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->showDialog(Landroid/view/View;Z)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    const/16 v1, 0xd

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I
    invoke-static {v0, v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$202(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;I)I

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    const/4 v1, 0x2

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollReady:I
    invoke-static {v0, v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$102(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;I)I

    .line 98
    return-void

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$300(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090082

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public showScrollReadyGuide()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f08004c

    const v5, 0x7f08004b

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 103
    const-string v0, "SmartScrollTry"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SCROLL - SmartScrollReady() in Tutorial : mScrollReady="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollReady:I
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$100(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCurrentGuide="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$200(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$200(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_2

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$400(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030022

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v0, v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$302(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;Landroid/view/View;)Landroid/view/View;

    .line 113
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$300(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->showDialog(Landroid/view/View;Z)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    const/16 v1, 0xc

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I
    invoke-static {v0, v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$202(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;I)I

    .line 136
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollReady:I
    invoke-static {v0, v4}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$102(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;I)I

    .line 137
    return-void

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030023

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    # setter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->guideTextView:Landroid/view/View;
    invoke-static {v0, v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$302(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;Landroid/view/View;)Landroid/view/View;

    goto :goto_0

    .line 117
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mCurrentGuide:I
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$200(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mSmartScrollSettings:Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$000(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartScrollSettings;->checkVisualFeedbackEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mScrollMode:I
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$400(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020040

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 132
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$700(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->mFaceRecogRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$500(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)Ljava/lang/Runnable;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    # getter for: Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->FACE_DETECT_NOTI_DELAY:I
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->access$600(Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 128
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity$1;->this$0:Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/sbrowsertry/SmartScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f02009f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method
