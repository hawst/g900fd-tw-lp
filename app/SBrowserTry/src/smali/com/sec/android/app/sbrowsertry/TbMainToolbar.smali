.class public Lcom/sec/android/app/sbrowsertry/TbMainToolbar;
.super Lcom/sec/android/app/sbrowsertry/SBrowserMainToolbar;
.source "TbMainToolbar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

.field private mBookmarkButton:Landroid/widget/ImageButton;

.field private mContext:Landroid/content/Context;

.field private mCurrentState:I

.field private mDeleteButton:Landroid/widget/ImageButton;

.field private mHomeButton:Landroid/widget/ImageButton;

.field private mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

.field private mNavigationFakeHint:Landroid/view/View;

.field private mReloadButton:Landroid/widget/ImageButton;

.field private mTablet_toolbar_Search_engine:Landroid/widget/ImageButton;

.field private mTablet_toolbar_Search_engine_favicon:Landroid/widget/ImageButton;

.field private mUrlBar:Landroid/widget/EditText;

.field private mUrlBarContainer:Landroid/view/View;

.field private mUrlOptionsBar:Lcom/sec/android/app/sbrowsertry/TbUrlOptionsBar;

.field private mtablet_toolbar_search_engine_layout:Landroid/view/View;

.field private toolbarLayout:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "paramAttributeSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sbrowsertry/SBrowserMainToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mContext:Landroid/content/Context;

    .line 53
    check-cast p1, Lcom/sec/android/app/sbrowsertry/MainActivity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/TbMainToolbar;
    .param p1, "x1"    # Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Lcom/sec/android/app/sbrowsertry/MainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mNavigationFakeHint:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public dismissSearchEngineOptions()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->dismiss()V

    .line 142
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x2

    .line 199
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 215
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 201
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mCurrentState:I

    if-nez v0, :cond_1

    .line 202
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->onStateChanged(I)V

    goto :goto_0

    .line 203
    :cond_1
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mCurrentState:I

    if-ne v0, v1, :cond_0

    .line 204
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->onStateChanged(I)V

    goto :goto_0

    .line 207
    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->onStateChanged(I)V

    goto :goto_0

    .line 199
    nop

    :pswitch_data_0
    .packed-switch 0x7f08006c
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 193
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/sbrowsertry/SBrowserMainToolbar;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 194
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 147
    invoke-super {p0}, Lcom/sec/android/app/sbrowsertry/SBrowserMainToolbar;->onDetachedFromWindow()V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mUrlOptionsBar:Lcom/sec/android/app/sbrowsertry/TbUrlOptionsBar;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mUrlOptionsBar:Lcom/sec/android/app/sbrowsertry/TbUrlOptionsBar;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/TbUrlOptionsBar;->dismissSuggestion()V

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->engineOptions:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->dismiss()V

    .line 153
    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Lcom/sec/android/app/sbrowsertry/SBrowserMainToolbar;->onFinishInflate()V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mLocationBar:Lcom/sec/android/app/sbrowsertry/LocationBar;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mLocationBar:Lcom/sec/android/app/sbrowsertry/LocationBar;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/sbrowsertry/LocationBar;->setStateListener(Lcom/sec/android/app/sbrowsertry/LocationBar$StateListener;)V

    .line 63
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mCurrentState:I

    .line 64
    const v0, 0x7f08006c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mUrlBar:Landroid/widget/EditText;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mUrlBar:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    const v0, 0x7f08006a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mBookmarkButton:Landroid/widget/ImageButton;

    .line 68
    const v0, 0x7f08006f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mDeleteButton:Landroid/widget/ImageButton;

    .line 69
    const v0, 0x7f080067

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mUrlBarContainer:Landroid/view/View;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    const v0, 0x7f08006e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mReloadButton:Landroid/widget/ImageButton;

    .line 72
    const v0, 0x7f080091

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mHomeButton:Landroid/widget/ImageButton;

    .line 73
    const v0, 0x7f080069

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mNavigationFakeHint:Landroid/view/View;

    .line 75
    const v0, 0x7f080092

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mtablet_toolbar_search_engine_layout:Landroid/view/View;

    .line 76
    const v0, 0x7f080093

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mTablet_toolbar_Search_engine:Landroid/widget/ImageButton;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mTablet_toolbar_Search_engine:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$1;-><init>(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    const v0, 0x7f080094

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mTablet_toolbar_Search_engine_favicon:Landroid/widget/ImageButton;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mTablet_toolbar_Search_engine_favicon:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$2;-><init>(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    const v0, 0x7f08008d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->toolbarLayout:Landroid/view/View;

    .line 117
    const v0, 0x7f080084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sbrowsertry/TbUrlOptionsBar;

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mUrlOptionsBar:Lcom/sec/android/app/sbrowsertry/TbUrlOptionsBar;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mUrlBar:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar$3;-><init>(Lcom/sec/android/app/sbrowsertry/TbMainToolbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 137
    return-void
.end method

.method public onStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 157
    packed-switch p1, :pswitch_data_0

    .line 186
    :goto_0
    :pswitch_0
    iput p1, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mCurrentState:I

    .line 187
    return-void

    .line 161
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mReloadButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mtablet_toolbar_search_engine_layout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mBookmarkButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mHomeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mUrlBarContainer:Landroid/view/View;

    const v1, 0x7f02000f

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->handleStateChange()V

    goto :goto_0

    .line 172
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mUrlBar:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->handleStateChange()V

    goto :goto_0

    .line 178
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mUrlBar:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mUrlOptionsBar:Lcom/sec/android/app/sbrowsertry/TbUrlOptionsBar;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/TbUrlOptionsBar;->showSuggestion()V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sbrowsertry/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->handleStateChange()V

    goto :goto_0

    .line 157
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
