.class Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$2;
.super Landroid/widget/ListPopupWindow;
.source "SearchEngineOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->createPopup(Landroid/content/Context;Landroid/view/View;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # Landroid/util/AttributeSet;
    .param p4, "x2"    # I

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$2;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public show()V
    .locals 4

    .prologue
    .line 81
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$2;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->isClicked:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$102(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;Z)Z

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$2;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$2;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    # getter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mMainActivity:Lcom/sec/android/app/sbrowsertry/MainActivity;
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$300(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Lcom/sec/android/app/sbrowsertry/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    # setter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mScreenWidth:I
    invoke-static {v1, v2}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$202(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;I)I

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$2;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    # getter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->localResources:Landroid/content/res/Resources;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$400(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07008f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 84
    .local v0, "rightMargin":I
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$2;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    # getter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;
    invoke-static {v1}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$000(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Landroid/widget/ListPopupWindow;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$2;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    # getter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mScreenWidth:I
    invoke-static {v2}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$200(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions$2;->this$0:Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;

    # getter for: Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->mPopup:Landroid/widget/ListPopupWindow;
    invoke-static {v3}, Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;->access$000(Lcom/sec/android/app/sbrowsertry/SearchEngineOptions;)Landroid/widget/ListPopupWindow;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListPopupWindow;->getAnchorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 86
    invoke-super {p0}, Landroid/widget/ListPopupWindow;->show()V

    .line 87
    return-void
.end method
