.class public Lcom/sec/android/app/sbrowsertry/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sbrowsertry/MainActivity$EditTextTest;
    }
.end annotation


# static fields
.field public static final CHINA:Ljava/lang/Boolean;

.field public static final GUIDE_BOOKMARK_STEP_1:I = 0x1

.field public static final GUIDE_BOOKMARK_STEP_2:I = 0x2

.field public static final GUIDE_BOOKMARK_STEP_3:I = 0x3

.field public static final GUIDE_BOOKMARK_STEP_4:I = 0x4

.field public static final GUIDE_BOOKMARK_STEP_5:I = 0x5

.field public static final GUIDE_BOOKMARK_STEP_6:I = 0x6

.field public static final GUIDE_BOOKMARK_TABLET_STEP_1:I = 0x47

.field public static final GUIDE_BOOKMARK_TABLET_STEP_2:I = 0x48

.field public static final GUIDE_BOOKMARK_TABLET_STEP_3:I = 0x49

.field public static final GUIDE_MAGNIFIER_STEP_1:I = 0x1f

.field public static final GUIDE_MAGNIFIER_STEP_2:I = 0x20

.field public static final GUIDE_MAGNIFIER_STEP_3:I = 0x21

.field public static final GUIDE_MAGNIFIER_STEP_4:I = 0x22

.field public static final GUIDE_NEW_TAB_STEP_1:I = 0x15

.field public static final GUIDE_NEW_TAB_STEP_2:I = 0x16

.field public static final GUIDE_QUICK_ACCESS_STEP_1:I = 0x33

.field public static final GUIDE_QUICK_ACCESS_STEP_2:I = 0x34

.field public static final GUIDE_QUICK_ACCESS_STEP_3:I = 0x35

.field public static final GUIDE_READING_LIST_STEP_1:I = 0x3d

.field public static final GUIDE_READING_LIST_STEP_2:I = 0x3e

.field public static final GUIDE_SEARCH_BAR_STEP_1:I = 0xb

.field public static final GUIDE_SEARCH_BAR_STEP_2:I = 0xc

.field public static final GUIDE_SEARCH_BAR_STEP_3:I = 0xd

.field public static final GUIDE_SEARCH_BAR_STEP_4:I = 0xe

.field public static final GUIDE_SEARCH_ENGINE_STEP_1:I = 0x29

.field public static final GUIDE_SEARCH_ENGINE_STEP_2:I = 0x2a

.field public static final GUIDE_SEARCH_ENGINE_STEP_3:I = 0x2b

.field public static final GUIDE_SEARCH_ENGINE_STEP_4:I = 0x2c


# instance fields
.field private HELP_EXIT_NOTI_DELAY:I

.field public cscHome:Ljava/lang/String;

.field private isOptionMenuUp:Z

.field private mActionBar:Landroid/app/ActionBar;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentOrientation:I

.field public mCurrentStep:I

.field private mHandler:Landroid/os/Handler;

.field protected mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

.field private mHelpExitRunnable:Ljava/lang/Runnable;

.field protected mMagnifierPopupShowed:Z

.field private mMainToolbar:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

.field private mOnPauseCalled:Z

.field private mTbMainToolbar:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

.field private mWrongInputToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 102
    const-string v0, "China"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CHINA"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "china"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sbrowsertry/MainActivity;->CHINA:Ljava/lang/Boolean;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 79
    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mMagnifierPopupShowed:Z

    .line 80
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->cscHome:Ljava/lang/String;

    .line 84
    iput v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    .line 93
    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mOnPauseCalled:Z

    .line 94
    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->isOptionMenuUp:Z

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    .line 98
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->HELP_EXIT_NOTI_DELAY:I

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mWrongInputToast:Landroid/widget/Toast;

    .line 1089
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sbrowsertry/MainActivity;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sbrowsertry/MainActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method private createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V
    .locals 3
    .param p1, "resID"    # I
    .param p2, "mode"    # Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;
    .param p3, "bIgnoreHover"    # Z

    .prologue
    .line 1101
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 1102
    new-instance v1, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-direct {v1, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .line 1103
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v1, p1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setContentView(I)V

    .line 1104
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v1, p2}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setTouchTransparencyMode(Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;)V

    .line 1105
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setShowWrongInputToast(Z)V

    .line 1106
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v1, p3}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setIgnoreHoverEvt(Z)V

    .line 1107
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v1, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 1109
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->show()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1115
    :goto_0
    return-void

    .line 1110
    :catch_0
    move-exception v0

    .line 1111
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1112
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 1113
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    invoke-virtual {v0}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V

    goto :goto_0
.end method

.method private getEasyWindowMode()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1361
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "easy_mode_switch"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTablet(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method private onEasyModeConfigChanged(I)V
    .locals 2
    .param p1, "orien"    # I

    .prologue
    const v1, 0x7f080089

    .line 847
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 848
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 854
    :goto_0
    return-void

    .line 852
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setupActionBar(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 813
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mActionBar:Landroid/app/ActionBar;

    .line 814
    iput-object p1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mContext:Landroid/content/Context;

    .line 815
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    .line 816
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 817
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mActionBar:Landroid/app/ActionBar;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 818
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mActionBar:Landroid/app/ActionBar;

    new-instance v1, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;

    iget-object v2, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mContext:Landroid/content/Context;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/sbrowsertry/TbTabButtonContainer;-><init>(Lcom/sec/android/app/sbrowsertry/MainActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 819
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 821
    :cond_0
    const v0, 0x7f080024

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 822
    return-void
.end method

.method private setupEasyBar()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 825
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getOrientation()I

    move-result v0

    .line 826
    .local v0, "orien":I
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 829
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f08002a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 831
    const v2, 0x7f080084

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    const v3, 0x7f070096

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 832
    const v2, 0x7f08006c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070095

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v5, v3}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 834
    const v2, 0x7f08008a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 835
    const v2, 0x7f08008b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 836
    const v2, 0x7f08007e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 837
    const v2, 0x7f08007f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 839
    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    .line 844
    :cond_0
    :goto_0
    return-void

    .line 842
    :cond_1
    const v2, 0x7f080089

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected dismissHelpDialog()V
    .locals 2

    .prologue
    .line 1347
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v1, :cond_0

    .line 1349
    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 1350
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1358
    :cond_0
    :goto_0
    return-void

    .line 1352
    :catch_0
    move-exception v0

    .line 1354
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1355
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 1356
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    invoke-virtual {v0}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V

    goto :goto_0
.end method

.method protected handleStateChange()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const v10, 0x7f08006c

    const v9, 0x7f08006d

    const/16 v8, 0x8

    const/4 v7, 0x1

    .line 253
    iget v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    sparse-switch v5, :sswitch_data_0

    .line 485
    :cond_0
    :goto_0
    return-void

    .line 257
    :sswitch_0
    new-instance v3, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/app/sbrowsertry/ShowBookmarkActivity;

    invoke-direct {v3, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 258
    .local v3, "intent_bookmark":Landroid/content/Intent;
    const/4 v5, 0x2

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    .line 259
    const/4 v5, 0x2

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/app/sbrowsertry/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 279
    .end local v3    # "intent_bookmark":Landroid/content/Intent;
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->finish()V

    goto :goto_0

    .line 284
    :sswitch_2
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/app/sbrowsertry/AddBookmarkActivity;

    invoke-direct {v2, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 285
    .local v2, "intent_add_bookmark":Landroid/content/Intent;
    const/4 v5, 0x5

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    .line 286
    const/4 v5, 0x5

    invoke-virtual {p0, v2, v5}, Lcom/sec/android/app/sbrowsertry/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 294
    .end local v2    # "intent_add_bookmark":Landroid/content/Intent;
    :sswitch_3
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v5}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 296
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 298
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/app/sbrowsertry/TbAddBookmarkActivity;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 299
    .local v4, "intent_bookmark_tablet":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090024

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 301
    .local v0, "default_title":Ljava/lang/String;
    const-string v5, "title"

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 302
    const-string v5, "url"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "www."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".com"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 303
    const/16 v5, 0x48

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    .line 305
    const/16 v5, 0x47

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/sbrowsertry/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 309
    .end local v0    # "default_title":Ljava/lang/String;
    .end local v4    # "intent_bookmark_tablet":Landroid/content/Intent;
    :sswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->finish()V

    goto :goto_0

    .line 313
    :sswitch_5
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 314
    const v5, 0x7f030017

    sget-object v6, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 316
    const/16 v5, 0xc

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 318
    :cond_1
    const v5, 0x7f030018

    sget-object v6, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 320
    const v5, 0x7f080024

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 321
    const v5, 0x7f08006f

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 323
    sget-object v5, Lcom/sec/android/app/sbrowsertry/MainActivity;->CHINA:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 324
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 329
    :goto_1
    const v5, 0x7f080069

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    .line 331
    const v5, 0x7f080068

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 332
    invoke-virtual {p0, v10}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f060027

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setTextColor(I)V

    .line 336
    const/16 v5, 0xd

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 326
    :cond_2
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 341
    :sswitch_6
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 342
    const v5, 0x7f030018

    sget-object v6, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 344
    sget-object v5, Lcom/sec/android/app/sbrowsertry/MainActivity;->CHINA:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 345
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 350
    :goto_2
    const/16 v5, 0xd

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 347
    :cond_3
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 355
    :sswitch_7
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 356
    const v5, 0x7f030019

    sget-object v6, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 357
    invoke-virtual {p0, v10}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    const v6, 0x7f090025

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(I)V

    .line 358
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    iget v7, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->HELP_EXIT_NOTI_DELAY:I

    int-to-long v8, v7

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 359
    const/16 v5, 0xe

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 361
    :cond_4
    const v5, 0x7f080024

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 362
    const v5, 0x7f080024

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 363
    const v5, 0x7f08006f

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 364
    sget-object v5, Lcom/sec/android/app/sbrowsertry/MainActivity;->CHINA:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 365
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 369
    :goto_3
    const v5, 0x7f080068

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 372
    invoke-virtual {p0, v10}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 395
    .local v1, "et":Landroid/widget/EditText;
    const v5, 0x7f030019

    sget-object v6, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 397
    invoke-virtual {p0, v10}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    const-string v6, ""

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 398
    invoke-virtual {p0, v10}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f060027

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setTextColor(I)V

    .line 400
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    iget v7, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->HELP_EXIT_NOTI_DELAY:I

    int-to-long v8, v7

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 401
    const/16 v5, 0xe

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 367
    .end local v1    # "et":Landroid/widget/EditText;
    :cond_5
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 413
    :sswitch_8
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 414
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->finish()V

    goto/16 :goto_0

    .line 417
    :sswitch_9
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 419
    const v5, 0x7f030011

    sget-object v6, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 420
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    iget v7, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->HELP_EXIT_NOTI_DELAY:I

    int-to-long v8, v7

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 421
    const/16 v5, 0x16

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 425
    :sswitch_a
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 426
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->finish()V

    goto/16 :goto_0

    .line 429
    :sswitch_b
    const v5, 0x7f03000f

    sget-object v6, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 430
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    const-wide/16 v8, 0x1f4

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 431
    const/16 v5, 0x21

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 434
    :sswitch_c
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    iget v7, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->HELP_EXIT_NOTI_DELAY:I

    int-to-long v8, v7

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 435
    const/16 v5, 0x22

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 438
    :sswitch_d
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 439
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->finish()V

    goto/16 :goto_0

    .line 442
    :sswitch_e
    const v5, 0x7f03001b

    sget-object v6, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 443
    const/16 v5, 0x2a

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 446
    :sswitch_f
    const v5, 0x7f03001c

    sget-object v6, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 447
    const/16 v5, 0x2b

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 450
    :sswitch_10
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 451
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mTbMainToolbar:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    invoke-virtual {v5}, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;->dismissSearchEngineOptions()V

    .line 452
    const v5, 0x7f080094

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    const v6, 0x7f020002

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 458
    :cond_6
    const v5, 0x7f03001d

    sget-object v6, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 459
    iget-object v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    iget v7, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->HELP_EXIT_NOTI_DELAY:I

    int-to-long v8, v7

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 460
    const/16 v5, 0x2c

    iput v5, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 463
    :sswitch_11
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 464
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->finish()V

    goto/16 :goto_0

    .line 253
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x4 -> :sswitch_2
        0x6 -> :sswitch_1
        0xb -> :sswitch_5
        0xc -> :sswitch_6
        0xd -> :sswitch_7
        0xe -> :sswitch_8
        0x15 -> :sswitch_9
        0x16 -> :sswitch_a
        0x20 -> :sswitch_b
        0x21 -> :sswitch_c
        0x22 -> :sswitch_d
        0x29 -> :sswitch_e
        0x2a -> :sswitch_f
        0x2b -> :sswitch_10
        0x2c -> :sswitch_11
        0x47 -> :sswitch_3
        0x49 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const v5, 0x7f03000b

    const v4, 0x7f020005

    const/4 v3, 0x0

    const/4 v2, 0x1

    const v1, 0x7f08000d

    .line 166
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 167
    if-nez p2, :cond_0

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->finish()V

    .line 172
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 231
    :goto_0
    return-void

    .line 175
    :sswitch_0
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    .line 178
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 179
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 180
    const v0, 0x7f03000c

    sget-object v1, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto :goto_0

    .line 183
    :sswitch_1
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    .line 186
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 187
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 188
    const v0, 0x7f08006a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const v1, 0x7f020061

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 189
    sget-object v0, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v5, v0, v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    iget v2, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->HELP_EXIT_NOTI_DELAY:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 193
    :sswitch_2
    const/16 v0, 0x49

    iput v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    .line 194
    const v0, 0x7f08006a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setActivated(Z)V

    .line 195
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 196
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 197
    sget-object v0, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v5, v0, v2}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    iget v2, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->HELP_EXIT_NOTI_DELAY:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 172
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x5 -> :sswitch_1
        0x47 -> :sswitch_2
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 14
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v13, 0x7f03000b

    const v12, 0x7f020005

    const/4 v11, 0x1

    const/4 v10, 0x0

    const v9, 0x7f08000d

    .line 1121
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1123
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v8

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Display;->getOrientation()I

    move-result v2

    .line 1125
    .local v2, "orien":I
    iput v2, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentOrientation:I

    .line 1126
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 1128
    iget v8, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    sparse-switch v8, :sswitch_data_0

    .line 1340
    :cond_0
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1341
    iget-object v8, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mMainToolbar:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    invoke-virtual {v8, v2}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->onOrientationChanged(I)V

    .line 1342
    :cond_1
    return-void

    .line 1151
    :sswitch_0
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1152
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1153
    const v8, 0x7f030008

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto :goto_0

    .line 1208
    :sswitch_1
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1209
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1210
    sget-object v8, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v13, v8, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto :goto_0

    .line 1213
    :sswitch_2
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1214
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1215
    const v8, 0x7f03000c

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto :goto_0

    .line 1219
    :sswitch_3
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1220
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1221
    sget-object v8, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v13, v8, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto :goto_0

    .line 1225
    :sswitch_4
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1226
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1227
    const v8, 0x7f030008

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1230
    :sswitch_5
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1231
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1232
    const v8, 0x7f03000a

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1236
    :sswitch_6
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1237
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1238
    sget-object v8, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v13, v8, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1243
    :sswitch_7
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1244
    const v8, 0x7f08000c

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1245
    .local v0, "mainLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1246
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f030026

    invoke-virtual {v8, v9, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 1247
    .local v6, "view1":Landroid/view/View;
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1250
    .end local v0    # "mainLayout":Landroid/widget/LinearLayout;
    .end local v6    # "view1":Landroid/view/View;
    :cond_2
    const v8, 0x7f030016

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1253
    :sswitch_8
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1254
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1255
    const v8, 0x7f030017

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1259
    :sswitch_9
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 1260
    const v8, 0x7f08000c

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1261
    .local v1, "mainLayout1":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1262
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f030026

    invoke-virtual {v8, v9, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 1263
    .local v7, "view11":Landroid/view/View;
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1266
    .end local v1    # "mainLayout1":Landroid/widget/LinearLayout;
    .end local v7    # "view11":Landroid/view/View;
    :cond_3
    const v8, 0x7f030018

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1270
    :sswitch_a
    const v8, 0x7f030019

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1273
    :sswitch_b
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1274
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1275
    const v8, 0x7f03001a

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1278
    :sswitch_c
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1279
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1280
    const v8, 0x7f03001b

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1283
    :sswitch_d
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1284
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1285
    const v8, 0x7f03001c

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1288
    :sswitch_e
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1289
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1290
    const v8, 0x7f03001d

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1293
    :sswitch_f
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1295
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1296
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1297
    const v8, 0x7f030010

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1301
    :sswitch_10
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1303
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1304
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1305
    const v8, 0x7f030011

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1309
    :sswitch_11
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1310
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    const v9, 0x7f020063

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1311
    const v8, 0x7f03000e

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE_NO_MOVE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v10}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1316
    :sswitch_12
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1317
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    const v9, 0x7f020063

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1318
    const v8, 0x7f08000e

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1319
    const v8, 0x7f08000e

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    const v9, 0x7f020064

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1321
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1323
    .local v4, "res":Landroid/content/res/Resources;
    const v8, 0x7f08000e

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 1324
    .local v5, "view":Landroid/widget/ImageView;
    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1325
    .local v3, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v8, 0x7f070040

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1326
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1327
    const v8, 0x7f03000f

    sget-object v9, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE_NO_MOVE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v8, v9, v10}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_0

    .line 1128
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x6 -> :sswitch_3
        0xb -> :sswitch_7
        0xc -> :sswitch_8
        0xd -> :sswitch_9
        0xe -> :sswitch_a
        0x15 -> :sswitch_f
        0x16 -> :sswitch_10
        0x1f -> :sswitch_11
        0x20 -> :sswitch_12
        0x21 -> :sswitch_12
        0x22 -> :sswitch_12
        0x29 -> :sswitch_b
        0x2a -> :sswitch_c
        0x2b -> :sswitch_d
        0x2c -> :sswitch_e
        0x47 -> :sswitch_4
        0x48 -> :sswitch_5
        0x49 -> :sswitch_6
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v12, 0x7f08006c

    const v11, 0x7f08000d

    const/16 v10, 0x8

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 489
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 492
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "audio"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/AudioManager;

    iput-object v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 494
    const v6, 0x7f090089

    invoke-static {p0, v6, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mWrongInputToast:Landroid/widget/Toast;

    .line 496
    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    .line 497
    new-instance v6, Lcom/sec/android/app/sbrowsertry/MainActivity$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sbrowsertry/MainActivity$1;-><init>(Lcom/sec/android/app/sbrowsertry/MainActivity;)V

    iput-object v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    .line 631
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 632
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 633
    .local v0, "action":Ljava/lang/String;
    iget v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    if-nez v6, :cond_0

    .line 635
    const-string v6, "com.sec.android.app.sbrowsertry.GUIDE_ADD_BOOKMARK"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 637
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 638
    const/16 v6, 0x47

    iput v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    .line 656
    :cond_0
    :goto_0
    const v6, 0x7f030001

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->setContentView(I)V

    .line 657
    const-string v6, "com.sec.android.app.sbrowsertry.GUIDE_SEARCH_BAR"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 659
    invoke-virtual {p0, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 660
    const v6, 0x7f08000c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 661
    .local v4, "mainLayout":Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030026

    invoke-virtual {v6, v7, v4, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 662
    .local v5, "view1":Landroid/view/View;
    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 663
    invoke-virtual {p0, v12}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    const-string v7, ""

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 666
    .end local v4    # "mainLayout":Landroid/widget/LinearLayout;
    .end local v5    # "view1":Landroid/view/View;
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 667
    const v6, 0x7f08000b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    iput-object v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mTbMainToolbar:Lcom/sec/android/app/sbrowsertry/TbMainToolbar;

    .line 668
    invoke-direct {p0, p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->setupActionBar(Landroid/content/Context;)V

    .line 689
    :goto_1
    iget v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    if-ne v6, v8, :cond_2

    .line 690
    const v6, 0x7f030008

    sget-object v7, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v6, v7, v8}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 691
    const v6, 0x7f080024

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 692
    .local v2, "bottombar":Landroid/view/View;
    const v6, 0x7f08002a

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 693
    .local v1, "book_button":Landroid/widget/ImageButton;
    new-instance v6, Lcom/sec/android/app/sbrowsertry/MainActivity$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sbrowsertry/MainActivity$2;-><init>(Lcom/sec/android/app/sbrowsertry/MainActivity;)V

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 702
    .end local v1    # "book_button":Landroid/widget/ImageButton;
    .end local v2    # "bottombar":Landroid/view/View;
    :cond_2
    iget v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_3

    .line 703
    invoke-virtual {p0, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 704
    invoke-virtual {p0, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    const v7, 0x7f020005

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 705
    const v6, 0x7f03000c

    sget-object v7, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v6, v7, v8}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 708
    :cond_3
    iget v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v7, 0x47

    if-ne v6, v7, :cond_4

    .line 709
    const v6, 0x7f030008

    sget-object v7, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v6, v7, v8}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 755
    :cond_4
    iget v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v7, 0xb

    if-ne v6, v7, :cond_e

    .line 757
    const v6, 0x7f030016

    sget-object v7, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v6, v7, v8}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 759
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 810
    :cond_5
    :goto_2
    return-void

    .line 640
    :cond_6
    const/4 v6, 0x4

    iput v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 642
    :cond_7
    const-string v6, "com.sec.android.app.sbrowsertry.GUIDE_SEARCH_BAR"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 643
    const/16 v6, 0xb

    iput v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 644
    :cond_8
    const-string v6, "com.sec.android.app.sbrowsertry.GUIDE_NEW_TAB"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 645
    const/16 v6, 0x15

    iput v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 646
    :cond_9
    const-string v6, "com.sec.android.app.sbrowsertry.GUIDE_AIRVIEW_MAGNIFIER"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 647
    const/16 v6, 0x1f

    iput v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 648
    :cond_a
    const-string v6, "com.sec.android.app.sbrowsertry.GUIDE_SEARCH_ENGINE"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 649
    const/16 v6, 0x29

    iput v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    goto/16 :goto_0

    .line 670
    :cond_b
    const v6, 0x7f08000b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    iput-object v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mMainToolbar:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    goto/16 :goto_1

    .line 762
    :cond_c
    sget-object v6, Lcom/sec/android/app/sbrowsertry/MainActivity;->CHINA:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_d

    .line 763
    const v6, 0x7f08006d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 768
    :goto_3
    const v6, 0x7f080069

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 769
    const v6, 0x7f08006e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 770
    const v6, 0x7f08006a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 771
    invoke-virtual {p0, v12}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f060027

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setTextColor(I)V

    goto/16 :goto_2

    .line 765
    :cond_d
    const v6, 0x7f08006d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 774
    :cond_e
    iget v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v7, 0x15

    if-ne v6, v7, :cond_f

    .line 776
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 777
    const v6, 0x7f030010

    sget-object v7, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v6, v7, v8}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_2

    .line 780
    :cond_f
    iget v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v7, 0x1f

    if-ne v6, v7, :cond_10

    .line 782
    invoke-virtual {p0, v12}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    const-string v7, "www.samsung.com"

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 783
    invoke-virtual {p0, v11}, Lcom/sec/android/app/sbrowsertry/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    const v7, 0x7f020063

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 784
    const v6, 0x7f03000e

    sget-object v7, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE_NO_MOVE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v6, v7, v9}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_2

    .line 786
    :cond_10
    iget v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v7, 0x29

    if-ne v6, v7, :cond_5

    .line 788
    const v6, 0x7f03001a

    sget-object v7, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v6, v7, v8}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    goto/16 :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 923
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 925
    .local v0, "bool":Z
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0b0002

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 927
    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 859
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 860
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 861
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    .line 867
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 868
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v3, 0x20

    const/4 v0, 0x1

    .line 1071
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_2

    :cond_0
    iget v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v2, 0x1f

    if-lt v1, v2, :cond_2

    iget v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    if-gt v1, v3, :cond_2

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 1074
    iget-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mMagnifierPopupShowed:Z

    if-ne v1, v0, :cond_1

    .line 1086
    :goto_0
    return v0

    .line 1078
    :cond_1
    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mMagnifierPopupShowed:Z

    .line 1080
    iput v3, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    .line 1083
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpExitRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1086
    :cond_2
    invoke-super {p0, p1}, Landroid/app/Activity;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 122
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    .line 123
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    .line 125
    const/4 v0, 0x1

    .line 138
    :goto_0
    return v0

    .line 127
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->finish()V

    .line 132
    :cond_1
    const/16 v0, 0x42

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v1, 0x29

    if-ne v0, v1, :cond_3

    .line 134
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mMainToolbar:Lcom/sec/android/app/sbrowsertry/PhMainToolbar;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/PhMainToolbar;->doClick()V

    .line 138
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 111
    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 112
    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->closeOptionsMenu()V

    .line 114
    :cond_0
    const/4 v0, 0x1

    .line 116
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 910
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 915
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mWrongInputToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 918
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 912
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->handleStateChange()V

    goto :goto_0

    .line 910
    nop

    :pswitch_data_0
    .packed-switch 0x7f0800b0
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 874
    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->isOptionMenuUp:Z

    if-ne v0, v1, :cond_0

    .line 876
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->closeOptionsMenu()V

    .line 877
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->isOptionMenuUp:Z

    .line 879
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mOnPauseCalled:Z

    .line 880
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 881
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 10
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v9, 0x7f0800b7

    const v8, 0x7f0800b6

    const v7, 0x7f0800b4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 933
    iget v4, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    if-ne v4, v6, :cond_5

    invoke-static {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->isTablet(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 935
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->dismissHelpDialog()V

    .line 936
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getOrientation()I

    move-result v0

    .line 939
    .local v0, "orien":I
    const v4, 0x7f0800ae

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 940
    const v4, 0x7f0800ae

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 941
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 944
    const v4, 0x7f0800ae

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 946
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 947
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 949
    const v4, 0x7f0800af

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 950
    const v4, 0x7f0800af

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 951
    const v4, 0x7f0800b2

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 952
    const v4, 0x7f0800b2

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 953
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 954
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 955
    const v4, 0x7f0800b5

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 956
    const v4, 0x7f0800b5

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 957
    const v4, 0x7f0800b9

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 958
    const v4, 0x7f0800b9

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 959
    const v4, 0x7f0800ba

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 960
    const v4, 0x7f0800ba

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 961
    const v4, 0x7f0800ba

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 963
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 964
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 965
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 966
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 967
    const v4, 0x7f0800b0

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 968
    const v4, 0x7f0800b0

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 969
    const v4, 0x7f0800b1

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 971
    const v4, 0x7f0800b3

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 972
    const v4, 0x7f0800b3

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 973
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 974
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 976
    const v4, 0x7f0800b8

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 977
    const v4, 0x7f0800b9

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 978
    const v4, 0x7f0800ba

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 980
    if-eq v0, v6, :cond_0

    const/4 v4, 0x3

    if-ne v0, v4, :cond_1

    .line 982
    :cond_0
    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 983
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 984
    const v4, 0x7f0800b5

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 986
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 989
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v4}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v4

    if-nez v4, :cond_4

    .line 991
    :cond_2
    const v4, 0x7f030008

    sget-object v5, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-direct {p0, v4, v5, v6}, Lcom/sec/android/app/sbrowsertry/MainActivity;->createAndShowHelpDialog(ILcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;Z)V

    .line 992
    if-ne v0, v6, :cond_3

    .line 994
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 996
    .local v2, "res":Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v4, :cond_3

    .line 997
    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const v5, 0x7f080031

    invoke-virtual {v4, v5}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 998
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 999
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v4, 0x7f070034

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1000
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1002
    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const v5, 0x7f080030

    invoke-virtual {v4, v5}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1003
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1004
    .restart local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v4, 0x7f07002c

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1005
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1007
    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const/high16 v5, 0x7f080000

    invoke-virtual {v4, v5}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1008
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1009
    .restart local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v4, 0x7f070019

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1010
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1012
    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const v5, 0x7f08002f

    invoke-virtual {v4, v5}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1013
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1014
    .restart local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v4, 0x7f070028

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1015
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1019
    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v2    # "res":Landroid/content/res/Resources;
    .end local v3    # "view":Landroid/view/View;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1021
    .restart local v2    # "res":Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    if-eqz v4, :cond_4

    .line 1022
    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const v5, 0x7f080030

    invoke-virtual {v4, v5}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1023
    .restart local v3    # "view":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1024
    .restart local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v4, 0x7f07002e

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1025
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1027
    iget-object v4, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mHelpDialog:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    const/high16 v5, 0x7f080000

    invoke-virtual {v4, v5}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1028
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1029
    .restart local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    const v4, 0x7f07001c

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1030
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1034
    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v2    # "res":Landroid/content/res/Resources;
    .end local v3    # "view":Landroid/view/View;
    :cond_4
    iput-boolean v6, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->isOptionMenuUp:Z

    .line 1037
    .end local v0    # "orien":I
    :cond_5
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v4

    return v4
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 236
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v1, 0x47

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v1, 0x49

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v1, 0x16

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v1, 0xe

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v1, 0x21

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mCurrentStep:I

    const/16 v1, 0x22

    if-ne v0, v1, :cond_1

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sbrowsertry/MainActivity;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/app/sbrowsertry/MainActivity;->handleStateChange()V

    .line 246
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
