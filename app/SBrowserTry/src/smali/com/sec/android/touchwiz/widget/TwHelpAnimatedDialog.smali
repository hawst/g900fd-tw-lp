.class public Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;
.super Lcom/sec/android/touchwiz/widget/TwHelpDialog;
.source "TwHelpAnimatedDialog.java"


# static fields
.field private static final ANIMATIOT_START_OFFSET:I = 0x12c


# instance fields
.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field private mCurrentPointAnimation:I

.field private mHandPointerMarginEnd:I

.field private mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mPointAnimationView:Landroid/view/View;

.field private mPointAnimations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/animation/Animation;",
            ">;"
        }
    .end annotation
.end field

.field private mSummaryView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;-><init>(Landroid/content/Context;)V

    .line 17
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mCurrentPointAnimation:I

    .line 21
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mHandPointerMarginEnd:I

    .line 105
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;-><init>(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 63
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->init(Landroid/content/Context;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;-><init>(Landroid/content/Context;I)V

    .line 17
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mCurrentPointAnimation:I

    .line 21
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mHandPointerMarginEnd:I

    .line 105
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;-><init>(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->init(Landroid/content/Context;)V

    .line 68
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cancelable"    # Z
    .param p3, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;-><init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 17
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mCurrentPointAnimation:I

    .line 21
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mHandPointerMarginEnd:I

    .line 105
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;-><init>(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 59
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->init(Landroid/content/Context;)V

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mBubbleAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .prologue
    .line 15
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mCurrentPointAnimation:I

    return v0
.end method

.method static synthetic access$108(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .prologue
    .line 15
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mCurrentPointAnimation:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mCurrentPointAnimation:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimations:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationView:Landroid/view/View;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimations:Ljava/util/List;

    .line 73
    const v1, 0x7f040001

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 74
    .local v0, "animation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    const v1, 0x7f040002

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 79
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    const v1, 0x7f040003

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 83
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    const v1, 0x7f040004

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    const v1, 0x7f040005

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 91
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    const/high16 v1, 0x7f040000

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 95
    new-instance v1, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$1;

    invoke-direct {v1, p0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$1;-><init>(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 103
    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 3

    .prologue
    .line 47
    invoke-super {p0}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->onAttachedToWindow()V

    .line 48
    const/high16 v0, 0x7f080000

    invoke-virtual {p0, v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationView:Landroid/view/View;

    .line 50
    const v0, 0x7f08002d

    invoke-virtual {p0, v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mSummaryView:Landroid/view/View;

    .line 51
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 52
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationView:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimations:Ljava/util/List;

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mCurrentPointAnimation:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mSummaryView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mSummaryView:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 55
    :cond_1
    return-void
.end method

.method public setHandPointerMarginEnd()V
    .locals 4

    .prologue
    .line 28
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mHandPointerMarginEnd:I

    if-eqz v2, :cond_1

    .line 29
    const/high16 v2, 0x7f080000

    invoke-virtual {p0, v2}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 30
    .local v0, "iv":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 31
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 32
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mHandPointerMarginEnd:I

    iget v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 33
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 35
    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    const v2, 0x7f080034

    invoke-virtual {p0, v2}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "iv":Landroid/widget/ImageView;
    check-cast v0, Landroid/widget/ImageView;

    .line 36
    .restart local v0    # "iv":Landroid/widget/ImageView;
    if-eqz v0, :cond_1

    .line 37
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 38
    .restart local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mHandPointerMarginEnd:I

    iget v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 39
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 42
    .end local v0    # "iv":Landroid/widget/ImageView;
    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    return-void
.end method

.method public setHandPointerMarginEndValue(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mHandPointerMarginEnd:I

    .line 25
    return-void
.end method
