.class public interface abstract Lcom/sec/android/touchwiz/widget/TwHelpScrollView$IScrollListener;
.super Ljava/lang/Object;
.source "TwHelpScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/touchwiz/widget/TwHelpScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IScrollListener"
.end annotation


# virtual methods
.method public abstract getMaxScrollSize()I
.end method

.method public abstract isSmartScrollEnabled()Z
.end method

.method public abstract showScrollGuide()V
.end method

.method public abstract showScrollReadyGuide()V
.end method
