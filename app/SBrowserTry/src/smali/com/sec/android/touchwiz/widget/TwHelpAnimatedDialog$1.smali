.class Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$1;
.super Ljava/lang/Object;
.source "TwHelpAnimatedDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$1;->this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const-wide/16 v2, 0x12c

    .line 98
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$1;->this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->setHandPointerMarginEnd()V

    .line 99
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$1;->this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    # getter for: Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->access$000(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$1;->this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    # getter for: Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimations:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->access$200(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$1;->this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    # getter for: Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mCurrentPointAnimation:I
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->access$100(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 101
    return-void
.end method
