.class public Lcom/sec/android/touchwiz/widget/TwTouchPunchView;
.super Landroid/view/View;
.source "TwTouchPunchView.java"


# static fields
.field public static final CIRCLE_SHAPE:Ljava/lang/String; = "circle"

.field public static final PUNCH_EVENT_FLAG:I = 0x80

.field public static final RECTANGLE_SHAPE:Ljava/lang/String; = "rectangle"


# instance fields
.field private mBrush:Landroid/graphics/Paint;

.field private mGlobalRect:Landroid/graphics/Rect;

.field private mHitRect:Landroid/graphics/Rect;

.field private mInitialized:Z

.field private mRadius:F

.field private mShape:Ljava/lang/String;

.field private mShapeShow:Z

.field private positionRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x0

    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShape:Ljava/lang/String;

    .line 34
    iput-boolean v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShapeShow:Z

    .line 35
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mBrush:Landroid/graphics/Paint;

    .line 36
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    .line 37
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mRadius:F

    .line 38
    iput-boolean v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mInitialized:Z

    .line 40
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->positionRect:Landroid/graphics/Rect;

    .line 42
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mGlobalRect:Landroid/graphics/Rect;

    .line 58
    const-string v1, "TwTouchPunchView"

    const-string v2, "GUIDE: TwTouchPunchView() enter"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    sget-object v1, Lcom/sec/android/app/sbrowsertry/R$styleable;->TwTouchPunchView:[I

    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 61
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShape:Ljava/lang/String;

    .line 62
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShapeShow:Z

    .line 64
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 66
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mBrush:Landroid/graphics/Paint;

    .line 67
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mBrush:Landroid/graphics/Paint;

    const v2, 0xffff00

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 68
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mBrush:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 69
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mBrush:Landroid/graphics/Paint;

    const/16 v2, 0x8c

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 71
    iput-boolean v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mInitialized:Z

    .line 72
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 206
    iget-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mInitialized:Z

    if-eqz v2, :cond_0

    .line 207
    const-string v2, "TwTouchPunchView"

    const-string v3, "GUIDE: onDraw() "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mBrush:Landroid/graphics/Paint;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShapeShow:Z

    if-eqz v2, :cond_0

    .line 209
    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShape:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShape:Ljava/lang/String;

    const-string v3, "circle"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getTranslationX()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v3, v6

    add-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getTranslationY()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    div-float/2addr v4, v6

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mRadius:F

    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mBrush:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getTranslationX()F

    move-result v2

    float-to-int v0, v2

    .line 216
    .local v0, "xTranslation":I
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getTranslationY()F

    move-result v2

    float-to-int v1, v2

    .line 218
    .local v1, "yTranslation":I
    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->positionRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 223
    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->positionRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mBrush:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 18
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 120
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v4

    .line 122
    .local v4, "flags":I
    const-string v7, "TwTouchPunchView"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GUIDE: onGenericMotionEvent() - flags="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", action="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mInitialized:Z

    if-nez v7, :cond_0

    .line 126
    const/4 v7, 0x0

    .line 162
    :goto_0
    return v7

    .line 129
    :cond_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShape:Ljava/lang/String;

    if-eqz v7, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShape:Ljava/lang/String;

    const-string v8, "circle"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 130
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-double v8, v7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getTranslationX()F

    move-result v7

    float-to-double v10, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    sub-int/2addr v7, v12

    int-to-double v12, v7

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double/2addr v12, v14

    add-double/2addr v10, v12

    sub-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-double v10, v7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getTranslationY()F

    move-result v7

    float-to-double v12, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v14

    int-to-double v14, v7

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    div-double v14, v14, v16

    add-double/2addr v12, v14

    sub-double/2addr v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 139
    .local v2, "diff":D
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mRadius:F

    float-to-double v8, v7

    cmpg-double v7, v2, v8

    if-gez v7, :cond_1

    .line 140
    const-string v7, "TwTouchPunchView"

    const-string v8, "GUIDE: CIRCLE_SHAPE - set PUNCH_EVENT_FLAG"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    or-int/lit16 v7, v4, 0x80

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->setEdgeFlags(I)V

    .line 162
    .end local v2    # "diff":D
    :goto_1
    const/4 v7, 0x0

    goto :goto_0

    .line 144
    .restart local v2    # "diff":D
    :cond_1
    const-string v7, "TwTouchPunchView"

    const-string v8, "GUIDE: CIRCLE_SHAPE - NOT set PUNCH_EVENT_FLAG !!!!"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 146
    .end local v2    # "diff":D
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getTranslationX()F

    move-result v7

    float-to-int v5, v7

    .line 147
    .local v5, "xTranslation":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getTranslationY()F

    move-result v7

    float-to-int v6, v7

    .line 149
    .local v6, "yTranslation":I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->positionRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    add-int/2addr v8, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    sub-int/2addr v9, v10

    add-int/2addr v9, v6

    invoke-virtual {v7, v5, v6, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 154
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->positionRect:Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 155
    const-string v7, "TwTouchPunchView"

    const-string v8, "GUIDE: RECTANGLE - set PUNCH_EVENT_FLAG"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    or-int/lit16 v7, v4, 0x80

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->setEdgeFlags(I)V

    goto :goto_1

    .line 159
    :cond_3
    const-string v7, "TwTouchPunchView"

    const-string v8, "GUIDE: RECTANGLE - NOT set PUNCH_EVENT_FLAG !!!!"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v7, 0x1

    .line 233
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mInitialized:Z

    if-eqz v1, :cond_0

    if-eqz p1, :cond_1

    .line 234
    :cond_0
    const-string v1, "TwTouchPunchView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GUIDE: onLayout() : changed="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mInitialized="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mInitialized:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {v1, p2, p3, p4, p5}, Landroid/graphics/Rect;->set(IIII)V

    .line 238
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v4

    :goto_0
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mRadius:F

    .line 242
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 244
    .local v0, "l":[I
    invoke-virtual {p0, v0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getLocationOnScreen([I)V

    .line 245
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mGlobalRect:Landroid/graphics/Rect;

    aget v2, v0, v5

    aget v3, v0, v7

    aget v4, v0, v5

    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    aget v5, v0, v7

    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 247
    iput-boolean v7, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mInitialized:Z

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->invalidate()V

    .line 250
    .end local v0    # "l":[I
    :cond_1
    return-void

    .line 238
    :cond_2
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v4

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 18
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 79
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v4

    .line 81
    .local v4, "flags":I
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mInitialized:Z

    if-nez v7, :cond_0

    .line 82
    const/4 v7, 0x0

    .line 114
    :goto_0
    return v7

    .line 85
    :cond_0
    const-string v7, "TwTouchPunchView"

    const-string v8, "GUIDE: onTouchEvent() enter"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShape:Ljava/lang/String;

    if-eqz v7, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShape:Ljava/lang/String;

    const-string v8, "circle"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 88
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-double v8, v7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getTranslationX()F

    move-result v7

    float-to-double v10, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    sub-int/2addr v7, v12

    int-to-double v12, v7

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double/2addr v12, v14

    add-double/2addr v10, v12

    sub-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-double v10, v7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getTranslationY()F

    move-result v7

    float-to-double v12, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v14

    int-to-double v14, v7

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    div-double v14, v14, v16

    add-double/2addr v12, v14

    sub-double/2addr v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 97
    .local v2, "diff":D
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mRadius:F

    float-to-double v8, v7

    cmpg-double v7, v2, v8

    if-gez v7, :cond_1

    .line 98
    or-int/lit16 v7, v4, 0x80

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->setEdgeFlags(I)V

    .line 114
    .end local v2    # "diff":D
    :cond_1
    :goto_1
    const/4 v7, 0x0

    goto :goto_0

    .line 101
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getTranslationX()F

    move-result v7

    float-to-int v5, v7

    .line 102
    .local v5, "xTranslation":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->getTranslationY()F

    move-result v7

    float-to-int v6, v7

    .line 104
    .local v6, "yTranslation":I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->positionRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    add-int/2addr v8, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mHitRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    sub-int/2addr v9, v10

    add-int/2addr v9, v6

    invoke-virtual {v7, v5, v6, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 109
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->positionRect:Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 110
    or-int/lit16 v7, v4, 0x80

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->setEdgeFlags(I)V

    goto :goto_1
.end method

.method public processGlobalCoordinatesTouch(Landroid/view/MotionEvent;)V
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    .line 175
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v2

    .line 177
    .local v2, "flags":I
    iget-boolean v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mInitialized:Z

    if-nez v3, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShape:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mShape:Ljava/lang/String;

    const-string v4, "circle"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 182
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-double v4, v3

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mGlobalRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mGlobalRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v6

    int-to-double v6, v3

    div-double/2addr v6, v10

    sub-double/2addr v4, v6

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-double v6, v3

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mGlobalRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v8, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mGlobalRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v8

    int-to-double v8, v3

    div-double/2addr v8, v10

    sub-double/2addr v6, v8

    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 191
    .local v0, "diff":D
    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mRadius:F

    float-to-double v4, v3

    cmpg-double v3, v0, v4

    if-gez v3, :cond_0

    .line 192
    or-int/lit16 v3, v2, 0x80

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->setEdgeFlags(I)V

    goto :goto_0

    .line 195
    .end local v0    # "diff":D
    :cond_2
    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwTouchPunchView;->mGlobalRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 196
    or-int/lit16 v3, v2, 0x80

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->setEdgeFlags(I)V

    goto :goto_0
.end method
