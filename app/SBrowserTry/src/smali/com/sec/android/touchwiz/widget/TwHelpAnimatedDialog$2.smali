.class Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;
.super Ljava/lang/Object;
.source "TwHelpAnimatedDialog.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;->this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;->this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    # operator++ for: Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mCurrentPointAnimation:I
    invoke-static {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->access$108(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)I

    .line 117
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;->this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    # getter for: Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mCurrentPointAnimation:I
    invoke-static {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->access$100(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;->this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    # getter for: Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimations:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->access$200(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;->this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    # getter for: Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimationView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->access$300(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;->this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    # getter for: Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mPointAnimations:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->access$200(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog$2;->this$0:Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;

    # getter for: Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->mCurrentPointAnimation:I
    invoke-static {v2}, Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;->access$100(Lcom/sec/android/touchwiz/widget/TwHelpAnimatedDialog;)I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 120
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 113
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 109
    return-void
.end method
