.class public Lcom/sec/android/touchwiz/widget/TwHelpScrollView;
.super Landroid/widget/ScrollView;
.source "TwHelpScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;,
        Lcom/sec/android/touchwiz/widget/TwHelpScrollView$IScrollListener;
    }
.end annotation


# static fields
.field static final BLUE_LIGHT_HEIGHT_MULTIPLICATOR:F = 4.0f

.field private static final SMART_SCROLL_FACE:I = 0x1

.field private static final SMART_SCROLL_MOTION:I = 0x2

.field private static TAG:Ljava/lang/String;


# instance fields
.field private bcheckIsFirstScroll:Z

.field private mChoreographer:Landroid/view/Choreographer;

.field private mInVSync:Z

.field private mScrollMode:I

.field private mScrollTryListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

.field private mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

.field private mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

.field final mVSyncFrameCallback:Landroid/view/Choreographer$FrameCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-string v0, "TwHelpScrollView"

    sput-object v0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollMode:I

    .line 22
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    .line 23
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    .line 24
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollTryListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    .line 25
    iput-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->bcheckIsFirstScroll:Z

    .line 28
    iput-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mInVSync:Z

    .line 247
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView$1;-><init>(Lcom/sec/android/touchwiz/widget/TwHelpScrollView;)V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mVSyncFrameCallback:Landroid/view/Choreographer$FrameCallback;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollMode:I

    .line 22
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    .line 23
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    .line 24
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollTryListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    .line 25
    iput-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->bcheckIsFirstScroll:Z

    .line 28
    iput-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mInVSync:Z

    .line 247
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView$1;-><init>(Lcom/sec/android/touchwiz/widget/TwHelpScrollView;)V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mVSyncFrameCallback:Landroid/view/Choreographer$FrameCallback;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/touchwiz/widget/TwHelpScrollView;)Landroid/view/Choreographer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwHelpScrollView;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mChoreographer:Landroid/view/Choreographer;

    return-object v0
.end method

.method private initVSync()V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mChoreographer:Landroid/view/Choreographer;

    if-nez v0, :cond_0

    .line 198
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mChoreographer:Landroid/view/Choreographer;

    .line 200
    :cond_0
    return-void
.end method

.method private stopVSync()V
    .locals 2

    .prologue
    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mInVSync:Z

    .line 219
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mChoreographer:Landroid/view/Choreographer;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mChoreographer:Landroid/view/Choreographer;

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mVSyncFrameCallback:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mChoreographer:Landroid/view/Choreographer;

    .line 229
    :cond_0
    return-void
.end method

.method private updateVSync()V
    .locals 2

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mInVSync:Z

    if-eqz v0, :cond_0

    .line 215
    :goto_0
    return-void

    .line 212
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mInVSync:Z

    .line 213
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->initVSync()V

    .line 214
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mChoreographer:Landroid/view/Choreographer;

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mVSyncFrameCallback:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    goto :goto_0
.end method


# virtual methods
.method doAnimation(J)V
    .locals 1
    .param p1, "frameTimeNanos"    # J

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollTryListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;->isSmartScrollEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setStartSmartScroll()V

    .line 276
    :cond_0
    return-void
.end method

.method doVSync()V
    .locals 2

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->onStartScroll()V

    .line 257
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mChoreographer:Landroid/view/Choreographer;

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mVSyncFrameCallback:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 258
    return-void
.end method

.method public getMaxScrollContentSize()I
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollTryListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;->getMaxScrollSize()I

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 108
    sget-object v0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->TAG:Ljava/lang/String;

    const-string v1, "HelpScrollView - onPause() "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->stopSmartScroll()V

    .line 110
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 114
    sget-object v0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HelpScrollView - onResume() - mScrollMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollMode:I

    invoke-virtual {p0, v0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->setSmartScroll(I)V

    .line 116
    return-void
.end method

.method public onStartScroll()V
    .locals 2

    .prologue
    .line 261
    const-string v0, "SmartScrollHandler"

    const-string v1, "onStartScroll,onStartScroll:"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->setStartSmartScroll()V

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->setStartSmartScroll()V

    .line 270
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 82
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 103
    :cond_0
    :goto_0
    return v2

    .line 85
    :pswitch_0
    sget-object v0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->TAG:Ljava/lang/String;

    const-string v1, "HelpScrollView - onTouchEvent() - ACTION_DOWN "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->pauseSmartScroll()V

    goto :goto_0

    .line 93
    :pswitch_1
    sget-object v0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->TAG:Ljava/lang/String;

    const-string v1, "HelpScrollView - onTouchEvent() - ACTION_UP "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->setSmartMotionScrollAngle(I)V

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->delayResumeSmartScroll()V

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public scrollBy(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 44
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->scrollBy(II)V

    .line 45
    return-void
.end method

.method public setEnableVSync(Z)V
    .locals 0
    .param p1, "status"    # Z

    .prologue
    .line 189
    if-eqz p1, :cond_0

    .line 190
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->updateVSync()V

    .line 194
    :goto_0
    return-void

    .line 192
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->stopVSync()V

    goto :goto_0
.end method

.method public setInitVSync()V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mInVSync:Z

    .line 185
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->initVSync()V

    .line 186
    return-void
.end method

.method public setScrollLintener(Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    .prologue
    .line 73
    if-nez p1, :cond_0

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollTryListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollTryListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    goto :goto_0
.end method

.method public setSmartScroll(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 121
    iput p1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollMode:I

    .line 123
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->stopSmartMotionScroll()V

    .line 126
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    if-nez v0, :cond_1

    .line 130
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;-><init>(Lcom/sec/android/touchwiz/widget/TwHelpScrollView;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    .line 131
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->startSmartScroll()V

    .line 146
    :cond_1
    :goto_0
    return-void

    .line 133
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    .line 134
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    if-eqz v0, :cond_3

    .line 135
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->stopSmartScroll()V

    .line 136
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    .line 139
    :cond_3
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    if-nez v0, :cond_1

    .line 140
    new-instance v0, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;-><init>(Lcom/sec/android/touchwiz/widget/TwHelpScrollView;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    .line 141
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->startSmartMotionScroll()V

    goto :goto_0

    .line 145
    :cond_4
    sget-object v0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSmartScroll() - Scroll Mode is invalid -- type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setStartSmartScroll()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->setStartSmartScroll()V

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    if-eqz v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->setStartSmartScroll()V

    .line 237
    :cond_1
    return-void
.end method

.method public showEdgeEffect(I)V
    .locals 0
    .param p1, "y"    # I

    .prologue
    .line 49
    return-void
.end method

.method public smartScrollBy(I)V
    .locals 2
    .param p1, "offset"    # I

    .prologue
    const/4 v1, 0x0

    .line 165
    invoke-virtual {p0, v1, p1}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->scrollBy(II)V

    .line 167
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->bcheckIsFirstScroll:Z

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollTryListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;->showScrollGuide()V

    .line 170
    iput-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->bcheckIsFirstScroll:Z

    .line 172
    :cond_0
    return-void
.end method

.method public smartScrollReady()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mScrollTryListener:Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHelpScrollView$ScrollListener;->showScrollReadyGuide()V

    .line 176
    return-void
.end method

.method public stopSmartScroll()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartFaceListener;->stopSmartScroll()V

    .line 151
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartFaceListener:Lcom/sec/android/app/sbrowsertry/SmartFaceListener;

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    invoke-virtual {v0}, Lcom/sec/android/app/sbrowsertry/SmartMotionListener;->stopSmartMotionScroll()V

    .line 156
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpScrollView;->mSmartMotionListener:Lcom/sec/android/app/sbrowsertry/SmartMotionListener;

    .line 158
    :cond_1
    return-void
.end method
