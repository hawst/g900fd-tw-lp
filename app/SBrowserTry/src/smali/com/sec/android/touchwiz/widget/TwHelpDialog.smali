.class public Lcom/sec/android/touchwiz/widget/TwHelpDialog;
.super Landroid/app/Dialog;
.source "TwHelpDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/touchwiz/widget/TwHelpDialog$1;,
        Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;
    }
.end annotation


# instance fields
.field private fPunchEvent:Z

.field private mIgnoreHoverEvt:Z

.field private mShowWrongInputToast:Z

.field private mTouchTransparencyMode:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

.field private mWrongInputToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    const v0, 0x7f0a0010

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 27
    sget-object v0, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mTouchTransparencyMode:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    .line 30
    iput-boolean v3, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mShowWrongInputToast:Z

    .line 31
    iput-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->fPunchEvent:Z

    .line 32
    iput-boolean v3, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mIgnoreHoverEvt:Z

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 51
    sget-object v0, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mTouchTransparencyMode:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    .line 53
    const v0, 0x7f090089

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    .line 56
    iput-boolean v3, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mShowWrongInputToast:Z

    .line 57
    iput-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->fPunchEvent:Z

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;-><init>(Landroid/content/Context;)V

    .line 43
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cancelable"    # Z
    .param p3, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;-><init>(Landroid/content/Context;)V

    .line 37
    invoke-virtual {p0, p2}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->setCancelable(Z)V

    .line 38
    invoke-virtual {p0, p3}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 39
    return-void
.end method


# virtual methods
.method public getShowWrongInputToast()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mShowWrongInputToast:Z

    return v0
.end method

.method public getTouchTransparencyMode()Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mTouchTransparencyMode:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    return-object v0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x7

    const/16 v4, 0x9

    const/4 v3, 0x0

    .line 134
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 136
    .local v0, "action":I
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mIgnoreHoverEvt:Z

    if-eqz v1, :cond_1

    .line 177
    :cond_0
    :goto_0
    return v3

    .line 138
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v1

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_2

    .line 139
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->fPunchEvent:Z

    .line 144
    :cond_2
    sget-object v1, Lcom/sec/android/touchwiz/widget/TwHelpDialog$1;->$SwitchMap$com$sec$android$touchwiz$widget$TwHelpDialog$TouchMode:[I

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mTouchTransparencyMode:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v2}, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    .line 173
    :cond_3
    :goto_1
    if-eq v0, v6, :cond_4

    if-eq v0, v4, :cond_0

    .line 175
    :cond_4
    iput-boolean v3, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->fPunchEvent:Z

    goto :goto_0

    .line 146
    :pswitch_0
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->fPunchEvent:Z

    if-eqz v1, :cond_5

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 149
    :cond_5
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mShowWrongInputToast:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    if-eqz v1, :cond_3

    .line 150
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 156
    :pswitch_1
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->fPunchEvent:Z

    if-eqz v1, :cond_7

    if-eq v0, v4, :cond_6

    if-ne v0, v5, :cond_7

    .line 158
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 160
    :cond_7
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mShowWrongInputToast:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    if-eqz v1, :cond_3

    if-eq v0, v4, :cond_3

    if-eq v0, v5, :cond_3

    if-eq v0, v6, :cond_3

    .line 163
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 85
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 87
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v1

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_0

    .line 88
    iput-boolean v5, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->fPunchEvent:Z

    .line 93
    :cond_0
    sget-object v1, Lcom/sec/android/touchwiz/widget/TwHelpDialog$1;->$SwitchMap$com$sec$android$touchwiz$widget$TwHelpDialog$TouchMode:[I

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mTouchTransparencyMode:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v2}, Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 120
    :cond_1
    :goto_0
    if-eq v0, v5, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    if-eqz v0, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    if-eq v0, v3, :cond_3

    .line 126
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->fPunchEvent:Z

    .line 128
    :cond_3
    return v4

    .line 95
    :pswitch_0
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->fPunchEvent:Z

    if-eqz v1, :cond_4

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 98
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mShowWrongInputToast:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    if-eqz v1, :cond_1

    .line 99
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 105
    :pswitch_1
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->fPunchEvent:Z

    if-eqz v1, :cond_5

    if-eq v0, v3, :cond_5

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 108
    :cond_5
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mShowWrongInputToast:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    if-eqz v1, :cond_1

    if-eq v0, v3, :cond_1

    .line 110
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setIgnoreHoverEvt(Z)V
    .locals 0
    .param p1, "bIgnoreHoverEvt"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mIgnoreHoverEvt:Z

    .line 80
    return-void
.end method

.method public setShowWrongInputToast(Z)V
    .locals 0
    .param p1, "pShowWrongInputToast"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mShowWrongInputToast:Z

    .line 70
    return-void
.end method

.method public setTouchTransparencyMode(Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;)V
    .locals 0
    .param p1, "touchTransparencyMode"    # Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mTouchTransparencyMode:Lcom/sec/android/touchwiz/widget/TwHelpDialog$TouchMode;

    .line 62
    return-void
.end method

.method public showWrongInputToast()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 184
    :cond_0
    return-void
.end method
