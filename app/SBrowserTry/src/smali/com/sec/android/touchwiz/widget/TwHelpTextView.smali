.class public final Lcom/sec/android/touchwiz/widget/TwHelpTextView;
.super Landroid/widget/TextView;
.source "TwHelpTextView.java"


# static fields
.field private static final ICON_HTML_END:Ljava/lang/String; = "\"/>"

.field private static final ICON_HTML_START:Ljava/lang/String; = "<img src=\"@drawable/"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/touchwiz/widget/TwHelpDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mDrawablesIds:[I

.field private final mImgGetter:Landroid/text/Html$ImageGetter;

.field private mInsideImageGravity:I

.field private mInsideImageHeight:F

.field private mInsideImagePadding:Landroid/graphics/Rect;

.field private mInsideImageWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawables:Ljava/util/ArrayList;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawablesIds:[I

    .line 50
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageGravity:I

    .line 150
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpTextView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView$1;-><init>(Lcom/sec/android/touchwiz/widget/TwHelpTextView;)V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mImgGetter:Landroid/text/Html$ImageGetter;

    .line 66
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mContext:Landroid/content/Context;

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawables:Ljava/util/ArrayList;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawablesIds:[I

    .line 50
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageGravity:I

    .line 150
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwHelpTextView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView$1;-><init>(Lcom/sec/android/touchwiz/widget/TwHelpTextView;)V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mImgGetter:Landroid/text/Html$ImageGetter;

    .line 81
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mContext:Landroid/content/Context;

    .line 82
    invoke-direct {p0, p2}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->initSelfResources(Landroid/util/AttributeSet;)V

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/touchwiz/widget/TwHelpTextView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwHelpTextView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/touchwiz/widget/TwHelpTextView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwHelpTextView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawables:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/touchwiz/widget/TwHelpTextView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwHelpTextView;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->invalidateDrawables()V

    return-void
.end method

.method private applyImages(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 204
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 206
    .local v6, "sb":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 208
    .local v1, "index":I
    const-string v7, "%s"

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 210
    .local v3, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 211
    .local v2, "m":Ljava/util/regex/Matcher;
    iget-object v7, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 213
    .local v4, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    .line 216
    .local v5, "result":Z
    :goto_0
    if-eqz v5, :cond_1

    .line 217
    iget-object v7, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawablesIds:[I

    array-length v7, v7

    if-ge v1, v7, :cond_0

    .line 218
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<img src=\"@drawable/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawablesIds:[I

    aget v8, v8, v1

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\"/>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 223
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 224
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    goto :goto_0

    .line 229
    :cond_1
    invoke-virtual {v2, v6}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 230
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 232
    .local v0, "finalString":Ljava/lang/String;
    return-object v0
.end method

.method private initHtmlText(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v2, 0x0

    .line 179
    invoke-virtual {p1, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 181
    .local v1, "textResourceId":I
    if-eqz v1, :cond_0

    .line 183
    invoke-virtual {p0, v1}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->setText(I)V

    .line 186
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 189
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawablesIds:[I

    if-eqz v2, :cond_1

    .line 190
    invoke-direct {p0, v0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->applyImages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 194
    :cond_1
    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mImgGetter:Landroid/text/Html$ImageGetter;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    return-void
.end method

.method private initImgPadding(Landroid/content/res/TypedArray;)V
    .locals 3
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v2, 0x0

    .line 236
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    .line 239
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v0, v1

    .line 242
    .local v0, "padding":I
    if-eqz v0, :cond_0

    .line 243
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 246
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v0, v1

    .line 249
    if-eqz v0, :cond_1

    .line 250
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 253
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v0, v1

    .line 256
    if-eqz v0, :cond_2

    .line 257
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 260
    :cond_2
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v0, v1

    .line 263
    if-eqz v0, :cond_3

    .line 264
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 267
    :cond_3
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v0, v1

    .line 270
    if-eqz v0, :cond_4

    .line 271
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 274
    :cond_4
    return-void
.end method

.method private initSelfResources(Landroid/util/AttributeSet;)V
    .locals 11
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v10, 0x0

    const/high16 v9, -0x40800000    # -1.0f

    const/4 v8, 0x0

    .line 91
    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v1, v6, Landroid/util/DisplayMetrics;->density:F

    .line 93
    .local v1, "density":F
    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mContext:Landroid/content/Context;

    sget-object v7, Lcom/sec/android/app/sbrowsertry/R$styleable;->TwHelpTextView:[I

    invoke-virtual {v6, p1, v7, v8, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 97
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v6, 0x7

    invoke-virtual {v0, v6, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v6

    iput v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageWidth:F

    .line 98
    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageWidth:F

    cmpg-float v6, v6, v10

    if-gez v6, :cond_0

    .line 101
    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageWidth:F

    div-float/2addr v6, v1

    iput v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageWidth:F

    .line 105
    :cond_0
    const/4 v6, 0x6

    invoke-virtual {v0, v6, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v6

    iput v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageHeight:F

    .line 106
    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageHeight:F

    cmpg-float v6, v6, v10

    if-gez v6, :cond_1

    .line 109
    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageHeight:F

    div-float/2addr v6, v1

    iput v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageHeight:F

    .line 113
    :cond_1
    const/16 v6, 0x8

    const/16 v7, 0x11

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    iput v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageGravity:I

    .line 116
    const/16 v6, 0x9

    invoke-virtual {v0, v6, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 118
    .local v4, "iconsArrayId":I
    if-lez v4, :cond_5

    .line 119
    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 122
    .local v5, "iconsTypedArray":Landroid/content/res/TypedArray;
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Landroid/content/res/TypedArray;->length()I

    move-result v6

    if-lez v6, :cond_4

    .line 123
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->length()I

    move-result v6

    new-array v6, v6, [I

    iput-object v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawablesIds:[I

    .line 125
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawablesIds:[I

    array-length v6, v6

    if-ge v2, v6, :cond_2

    .line 126
    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawablesIds:[I

    invoke-virtual {v5, v2, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    aput v7, v6, v2

    .line 125
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 128
    :cond_2
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 143
    .end local v2    # "i":I
    .end local v5    # "iconsTypedArray":Landroid/content/res/TypedArray;
    :cond_3
    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->initImgPadding(Landroid/content/res/TypedArray;)V

    .line 145
    invoke-direct {p0, v0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->initHtmlText(Landroid/content/res/TypedArray;)V

    .line 147
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 148
    return-void

    .line 130
    .restart local v5    # "iconsTypedArray":Landroid/content/res/TypedArray;
    :cond_4
    if-eqz v5, :cond_3

    .line 132
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_1

    .line 135
    .end local v5    # "iconsTypedArray":Landroid/content/res/TypedArray;
    :cond_5
    const/16 v6, 0xa

    invoke-virtual {v0, v6, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 136
    .local v3, "iconId":I
    if-lez v3, :cond_3

    .line 137
    const/4 v6, 0x1

    new-array v6, v6, [I

    iput-object v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawablesIds:[I

    .line 138
    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawablesIds:[I

    aput v3, v6, v8

    goto :goto_1
.end method

.method private invalidateDrawables()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 277
    iget-object v7, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawables:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 281
    .local v0, "count":I
    :goto_0
    if-lez v0, :cond_3

    .line 282
    iget-object v7, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mDrawables:Ljava/util/ArrayList;

    add-int/lit8 v8, v0, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/touchwiz/widget/TwHelpDrawable;

    .line 284
    .local v1, "currentDrawable":Lcom/sec/android/touchwiz/widget/TwHelpDrawable;
    if-eqz v1, :cond_0

    .line 285
    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageGravity:I

    invoke-virtual {v1, v7}, Lcom/sec/android/touchwiz/widget/TwHelpDrawable;->setGravity(I)V

    .line 287
    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageWidth:F

    cmpl-float v7, v7, v10

    if-ltz v7, :cond_1

    .line 288
    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageWidth:F

    float-to-int v7, v7

    invoke-virtual {v1, v7}, Lcom/sec/android/touchwiz/widget/TwHelpDrawable;->setWidth(I)V

    .line 295
    :goto_1
    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageHeight:F

    cmpl-float v7, v7, v10

    if-ltz v7, :cond_2

    .line 296
    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageHeight:F

    float-to-int v7, v7

    neg-int v7, v7

    invoke-virtual {v1, v7}, Lcom/sec/android/touchwiz/widget/TwHelpDrawable;->setHeight(I)V

    .line 307
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->getLineHeight()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->getTextSize()F

    move-result v8

    sub-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    move-result v8

    int-to-float v8, v8

    sub-float v5, v7, v8

    .line 311
    .local v5, "textY":F
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 312
    .local v4, "res":Landroid/content/res/Resources;
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 313
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    iget v7, v3, Landroid/util/DisplayMetrics;->scaledDensity:F

    iget v8, v3, Landroid/util/DisplayMetrics;->density:F

    sub-float v2, v7, v8

    .line 323
    .local v2, "densityDelta":F
    const v7, 0x7f070074

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    int-to-float v6, v7

    .line 333
    .local v6, "textYimPrecision":F
    invoke-virtual {v1}, Lcom/sec/android/touchwiz/widget/TwHelpDrawable;->getIntrinsicHeight()I

    move-result v7

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    add-float/2addr v7, v5

    neg-float v7, v7

    add-float/2addr v7, v6

    invoke-virtual {v1, v7}, Lcom/sec/android/touchwiz/widget/TwHelpDrawable;->setBitmapY(F)V

    .line 335
    iget-object v7, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    invoke-virtual {v1, v7}, Lcom/sec/android/touchwiz/widget/TwHelpDrawable;->setPadding(Landroid/graphics/Rect;)V

    .line 338
    .end local v2    # "densityDelta":F
    .end local v3    # "metrics":Landroid/util/DisplayMetrics;
    .end local v4    # "res":Landroid/content/res/Resources;
    .end local v5    # "textY":F
    .end local v6    # "textYimPrecision":F
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 291
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/touchwiz/widget/TwHelpDrawable;->getIntrinsicWidth()I

    move-result v7

    int-to-float v7, v7

    iget v8, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageWidth:F

    mul-float/2addr v7, v8

    float-to-int v7, v7

    neg-int v7, v7

    invoke-virtual {v1, v7}, Lcom/sec/android/touchwiz/widget/TwHelpDrawable;->setWidth(I)V

    goto :goto_1

    .line 301
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/touchwiz/widget/TwHelpDrawable;->getIntrinsicHeight()I

    move-result v7

    int-to-float v7, v7

    iget v8, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageHeight:F

    mul-float/2addr v7, v8

    float-to-int v7, v7

    invoke-virtual {v1, v7}, Lcom/sec/android/touchwiz/widget/TwHelpDrawable;->setHeight(I)V

    goto :goto_2

    .line 340
    .end local v1    # "currentDrawable":Lcom/sec/android/touchwiz/widget/TwHelpDrawable;
    :cond_3
    return-void
.end method


# virtual methods
.method public getImgGravity()I
    .locals 1

    .prologue
    .line 391
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageGravity:I

    return v0
.end method

.method public getImgHeight()I
    .locals 1

    .prologue
    .line 382
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageHeight:F

    float-to-int v0, v0

    return v0
.end method

.method public getImgWidth()I
    .locals 1

    .prologue
    .line 373
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageWidth:F

    float-to-int v0, v0

    return v0
.end method

.method public setImgGravity(I)V
    .locals 0
    .param p1, "g"    # I

    .prologue
    .line 386
    iput p1, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageGravity:I

    .line 387
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->invalidateDrawables()V

    .line 388
    return-void
.end method

.method public setImgHeight(I)V
    .locals 1
    .param p1, "height"    # I

    .prologue
    .line 377
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageHeight:F

    .line 378
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->invalidateDrawables()V

    .line 379
    return-void
.end method

.method public setImgPadding(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "padding"    # Landroid/graphics/Rect;

    .prologue
    .line 343
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 344
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->invalidateDrawables()V

    .line 345
    return-void
.end method

.method public setImgPaddingBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 363
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->bottom:I

    .line 364
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->invalidateDrawables()V

    .line 365
    return-void
.end method

.method public setImgPaddingLeft(I)V
    .locals 1
    .param p1, "left"    # I

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->left:I

    .line 349
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->invalidateDrawables()V

    .line 350
    return-void
.end method

.method public setImgPaddingRight(I)V
    .locals 1
    .param p1, "right"    # I

    .prologue
    .line 353
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->right:I

    .line 354
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->invalidateDrawables()V

    .line 355
    return-void
.end method

.method public setImgPaddingTop(I)V
    .locals 1
    .param p1, "top"    # I

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->top:I

    .line 359
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->invalidateDrawables()V

    .line 360
    return-void
.end method

.method public setImgWidth(I)V
    .locals 1
    .param p1, "width"    # I

    .prologue
    .line 368
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->mInsideImageWidth:F

    .line 369
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwHelpTextView;->invalidateDrawables()V

    .line 370
    return-void
.end method
