.class public Lcom/android/browser/provider/AddBookmarkProxyActivity;
.super Landroid/app/Activity;
.source "AddBookmarkProxyActivity.java"


# instance fields
.field private final CHROME_INTENT:Ljava/lang/String;

.field private final TITLE:Ljava/lang/String;

.field private final URL:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 26
    const-string v0, "title"

    iput-object v0, p0, Lcom/android/browser/provider/AddBookmarkProxyActivity;->TITLE:Ljava/lang/String;

    .line 27
    const-string v0, "url"

    iput-object v0, p0, Lcom/android/browser/provider/AddBookmarkProxyActivity;->URL:Ljava/lang/String;

    .line 28
    const-string v0, "com.android.chrome.ADDBOOKMARK"

    iput-object v0, p0, Lcom/android/browser/provider/AddBookmarkProxyActivity;->CHROME_INTENT:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lcom/android/browser/provider/AddBookmarkProxyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 36
    .local v1, "incomingIntent":Landroid/content/Intent;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 37
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "title"

    const-string v3, "title"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    const-string v2, "url"

    const-string v3, "url"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 39
    const-string v2, "com.android.chrome.ADDBOOKMARK"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 40
    invoke-virtual {p0, v0}, Lcom/android/browser/provider/AddBookmarkProxyActivity;->startActivity(Landroid/content/Intent;)V

    .line 41
    return-void
.end method
