.class Lcom/samsung/android/app/FileShareClient/ThumbnailManager$ThumbnailDrawable;
.super Landroid/graphics/drawable/ColorDrawable;
.source "ThumbnailManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/ThumbnailManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ThumbnailDrawable"
.end annotation


# instance fields
.field private final bitmapGetterTaskReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;)V
    .locals 1
    .param p1, "bitmapGatterTask"    # Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;

    .prologue
    .line 136
    const/high16 v0, -0x1000000

    invoke-direct {p0, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 137
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$ThumbnailDrawable;->bitmapGetterTaskReference:Ljava/lang/ref/WeakReference;

    .line 138
    return-void
.end method


# virtual methods
.method public getBitmapGatterTask()Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$ThumbnailDrawable;->bitmapGetterTaskReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;

    return-object v0
.end method
