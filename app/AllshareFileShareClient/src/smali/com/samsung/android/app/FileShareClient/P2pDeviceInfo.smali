.class public Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;
.super Ljava/lang/Object;
.source "P2pDeviceInfo.java"


# static fields
.field public static final CONNECTED:I = 0x0

.field public static final CONNECTING:I = 0x2

.field public static final CONNECT_FAILED:I = 0x1

.field private static final TAGClass:Ljava/lang/String; = "P2pDeviceInfo"

.field private static final sDeviceTypeImages:[I


# instance fields
.field private mAtherosMacAddress:Ljava/lang/String;

.field private mConnectStatus:I

.field private mDeviceName:Ljava/lang/String;

.field private mInterfaceMacAddress:Ljava/lang/String;

.field private mIpAddress:Ljava/lang/String;

.field private mMacAddress:Ljava/lang/String;

.field private mType:Ljava/lang/String;

.field private mTypeIcon:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->sDeviceTypeImages:[I

    return-void

    :array_0
    .array-data 4
        0x7f02000b
        0x7f02000e
        0x7f020011
        0x7f02000a
        0x7f020013
        0x7f020010
        0x7f02000c
        0x7f02000f
        0x7f02000d
        0x7f020014
        0x7f020009
    .end array-data
.end method

.method constructor <init>(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .locals 5
    .param p1, "device"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    const/4 v4, 0x2

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iget-object v0, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mType:Ljava/lang/String;

    .line 51
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->setTypeIcon()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mTypeIcon:I

    .line 52
    iget-object v0, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mMacAddress:Ljava/lang/String;

    .line 53
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mMacAddress:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->convertInterfaceMacAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mInterfaceMacAddress:Ljava/lang/String;

    .line 54
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mMacAddress:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->convertAtherosMacAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mAtherosMacAddress:Ljava/lang/String;

    .line 55
    iget-object v0, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 57
    const-string v0, "P2pDeviceInfo"

    const-string v1, "P2pDeviceInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Name["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], Status["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], MAC["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mMacAddress:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], mType["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], mTypeIcon["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mTypeIcon:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], mInterfaceMacAddress["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mInterfaceMacAddress:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], mAtherosMacAddress["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mAtherosMacAddress:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iget v0, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    if-nez v0, :cond_0

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mConnectStatus:I

    .line 69
    :goto_0
    return-void

    .line 64
    :cond_0
    iget v0, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    if-eq v4, v0, :cond_1

    const/4 v0, 0x4

    iget v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    if-ne v0, v1, :cond_2

    .line 66
    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mConnectStatus:I

    goto :goto_0

    .line 68
    :cond_2
    iput v4, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mConnectStatus:I

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "macAddress"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p3, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mType:Ljava/lang/String;

    .line 73
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->setTypeIcon()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mTypeIcon:I

    .line 74
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mMacAddress:Ljava/lang/String;

    .line 75
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mMacAddress:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->convertInterfaceMacAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mInterfaceMacAddress:Ljava/lang/String;

    .line 76
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mMacAddress:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->convertAtherosMacAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mAtherosMacAddress:Ljava/lang/String;

    .line 77
    iput-object p2, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 78
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "macAddress"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;
    .param p4, "connectStatus"    # I
    .param p5, "ipAddress"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mType:Ljava/lang/String;

    .line 83
    iput-object p2, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mMacAddress:Ljava/lang/String;

    .line 84
    iput-object p3, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 85
    iput p4, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mConnectStatus:I

    .line 86
    iput-object p5, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mIpAddress:Ljava/lang/String;

    .line 87
    return-void
.end method

.method private convertAtherosMacAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "macAddress"    # Ljava/lang/String;

    .prologue
    .line 168
    new-instance v2, Ljava/util/Formatter;

    invoke-direct {v2}, Ljava/util/Formatter;-><init>()V

    .line 170
    .local v2, "partialMacAddr":Ljava/util/Formatter;
    const/4 v4, 0x0

    const/4 v5, 0x2

    :try_start_0
    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 171
    .local v3, "subString":Ljava/lang/String;
    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    .line 173
    .local v1, "enable":I
    or-int/lit8 v1, v1, 0x2

    .line 174
    const-string v4, "%02x"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 175
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 176
    invoke-virtual {v2}, Ljava/util/Formatter;->close()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    .end local v1    # "enable":I
    .end local v3    # "subString":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v4, "P2pDeviceInfo"

    const-string v5, "convertAtherosMacAddress"

    const-string v6, "NumberFormatException"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 179
    invoke-virtual {v2}, Ljava/util/Formatter;->close()V

    goto :goto_0
.end method

.method private convertInterfaceMacAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "macAddress"    # Ljava/lang/String;

    .prologue
    .line 149
    new-instance v2, Ljava/util/Formatter;

    invoke-direct {v2}, Ljava/util/Formatter;-><init>()V

    .line 151
    .local v2, "partialMacAddr":Ljava/util/Formatter;
    const/16 v4, 0xc

    const/16 v5, 0xe

    :try_start_0
    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 152
    .local v3, "subString":Ljava/lang/String;
    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    .line 154
    .local v1, "enable":I
    xor-int/lit16 v1, v1, 0x80

    .line 155
    const-string v4, "%02x"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 156
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    const/16 v6, 0xc

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xe

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 158
    invoke-virtual {v2}, Ljava/util/Formatter;->close()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    .end local v1    # "enable":I
    .end local v3    # "subString":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 160
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v4, "P2pDeviceInfo"

    const-string v5, "convertInterfaceMacAddress"

    const-string v6, "NumberFormatException"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 162
    invoke-virtual {v2}, Ljava/util/Formatter;->close()V

    goto :goto_0
.end method

.method private setTypeIcon()I
    .locals 6

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x1

    .line 131
    const/4 v1, 0x0

    .line 133
    .local v1, "tokens":[Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mType:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 134
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mType:Ljava/lang/String;

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 136
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mType:Ljava/lang/String;

    if-eqz v2, :cond_1

    array-length v2, v1

    if-ge v2, v4, :cond_2

    .line 137
    :cond_1
    const-string v2, "P2pDeviceInfo"

    const-string v3, "setTypeIcon"

    const-string v4, "Malformed primaryDeviceType"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    sget-object v2, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->sDeviceTypeImages:[I

    aget v2, v2, v5

    .line 144
    :goto_0
    return v2

    .line 140
    :cond_2
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 141
    .local v0, "deviceType":I
    if-lt v0, v4, :cond_3

    const/16 v2, 0xc

    if-ge v0, v2, :cond_3

    .line 142
    sget-object v2, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->sDeviceTypeImages:[I

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    goto :goto_0

    .line 144
    :cond_3
    sget-object v2, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->sDeviceTypeImages:[I

    aget v2, v2, v5

    goto :goto_0
.end method


# virtual methods
.method public getAtherosMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mAtherosMacAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getConnectStatus()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mConnectStatus:I

    return v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getInterfaceMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mInterfaceMacAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getIpAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mIpAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mMacAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public getTypeIcon()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mTypeIcon:I

    return v0
.end method

.method public setConnectStatus(I)V
    .locals 0
    .param p1, "connectStatus"    # I

    .prologue
    .line 127
    iput p1, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mConnectStatus:I

    .line 128
    return-void
.end method

.method public setIpAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "ipAddress"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->mIpAddress:Ljava/lang/String;

    .line 115
    return-void
.end method
