.class Lcom/samsung/android/app/FileShareClient/ReceiverFragment$4;
.super Ljava/lang/Object;
.source "ReceiverFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showCancelConnectionConfirmDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

.field final synthetic val$mReceiverList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$4;->this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    iput-object p2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$4;->val$mReceiverList:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 360
    const-string v2, "ReceiverFragment"

    const-string v3, "showCancelConnectionConfirmDialog"

    const-string v4, "cancel Connection clicked"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$4;->val$mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 364
    .local v1, "mReceiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->cancel()V

    goto :goto_0

    .line 366
    .end local v1    # "mReceiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$4;->this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    # getter for: Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->access$300(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)Lcom/samsung/android/app/FileShareClient/Outbound;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->cancel_connect()V

    .line 367
    return-void
.end method
