.class Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$5;
.super Ljava/lang/Object;
.source "DeviceSelectActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$5;->this$0:Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 319
    const-string v1, "DeviceSelectActivity"

    const-string v2, "onServiceConnected"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p2

    .line 321
    check-cast v0, Lcom/samsung/android/app/FileShareClient/ClientService$ClinetServiceBinder;

    .line 322
    .local v0, "binder":Lcom/samsung/android/app/FileShareClient/ClientService$ClinetServiceBinder;
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$5;->this$0:Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/ClientService$ClinetServiceBinder;->getService()Lcom/samsung/android/app/FileShareClient/ClientService;

    move-result-object v2

    # setter for: Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;
    invoke-static {v1, v2}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->access$402(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/app/FileShareClient/ClientService;

    .line 324
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$5;->this$0:Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;

    # getter for: Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mP2pDeviceInfoList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->access$500(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 325
    const-string v1, "DeviceSelectActivity"

    const-string v2, "onServiceConnected"

    const-string v3, "Late Service bind, Already done onActivityResult()"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$5;->this$0:Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;

    # invokes: Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->startSending()V
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->access$600(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)V

    .line 329
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 333
    const-string v0, "DeviceSelectActivity"

    const-string v1, "onServiceDisconnected"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$5;->this$0:Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;
    invoke-static {v0, v1}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->access$402(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/app/FileShareClient/ClientService;

    .line 335
    return-void
.end method
