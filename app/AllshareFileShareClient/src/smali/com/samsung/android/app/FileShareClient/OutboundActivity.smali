.class public Lcom/samsung/android/app/FileShareClient/OutboundActivity;
.super Landroid/app/Activity;
.source "OutboundActivity.java"

# interfaces
.implements Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;
.implements Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;


# static fields
.field private static final OUTBOUND_FRAGMENT:I = 0x1f5

.field private static final RECEIVER_FRAGMENT:I = 0x1f6

.field private static final SIDE_PADDING:I = 0x0

.field private static final SIDE_PADDING_PERCENT:F = 1.0f

.field private static final TAGClass:Ljava/lang/String; = "OutboudActivity"


# instance fields
.field blankLayout:Landroid/widget/LinearLayout;

.field center:Landroid/widget/RelativeLayout;

.field private deviceType:Ljava/lang/String;

.field left:Landroid/widget/RelativeLayout;

.field private mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mCurrentFragment:I

.field private mFragmentTransaction:Landroid/app/FragmentTransaction;

.field private mIntent:Landroid/content/Intent;

.field private mIsGrandeDisplay:Z

.field private mIsLandscape:Z

.field private mMenuCancelAllItem:Landroid/view/MenuItem;

.field private mMenuCancelConnectionItem:Landroid/view/MenuItem;

.field private mMenuCancelTransfer:Landroid/view/MenuItem;

.field private mMenuClearHistoryItem:Landroid/view/MenuItem;

.field private mMenuResendAllItem:Landroid/view/MenuItem;

.field private mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

.field private mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

.field right:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 38
    const/16 v0, 0x1f5

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mCurrentFragment:I

    .line 58
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIsLandscape:Z

    .line 59
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIsGrandeDisplay:Z

    .line 64
    const-string v0, "ro.build.characteristics"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->deviceType:Ljava/lang/String;

    .line 312
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    .line 314
    new-instance v0, Lcom/samsung/android/app/FileShareClient/OutboundActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity$1;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/FileShareClient/OutboundActivity;)Lcom/samsung/android/app/FileShareClient/ClientService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/OutboundActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/FileShareClient/OutboundActivity;Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/app/FileShareClient/ClientService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/OutboundActivity;
    .param p1, "x1"    # Lcom/samsung/android/app/FileShareClient/ClientService;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/FileShareClient/OutboundActivity;)Lcom/samsung/android/app/FileShareClient/OutboundFragment;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/OutboundActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    return-object v0
.end method

.method public static getDisplayWidth(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 502
    const-string v6, "window"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 503
    .local v5, "wm":Landroid/view/WindowManager;
    const/4 v4, 0x0

    .line 506
    .local v4, "maxWidth":I
    :try_start_0
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 507
    .local v0, "display":Landroid/view/Display;
    const-class v6, Landroid/view/Display;

    const-string v7, "getRawWidth"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 509
    .local v2, "getRawW":Ljava/lang/reflect/Method;
    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 517
    .end local v0    # "display":Landroid/view/Display;
    .end local v2    # "getRawW":Ljava/lang/reflect/Method;
    :goto_0
    return v4

    .line 510
    :catch_0
    move-exception v1

    .line 511
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 512
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 513
    .local v3, "matrix":Landroid/util/DisplayMetrics;
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 515
    iget v4, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_0
.end method

.method private isSingleMode(Landroid/content/Intent;)Z
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 263
    const-string v5, "OutboudActivity"

    const-string v6, "isSingleMode"

    const-string v7, ""

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string v5, "outboundId"

    invoke-virtual {p1, v5, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 265
    .local v2, "outboundId":I
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    if-eqz v5, :cond_0

    if-lez v2, :cond_0

    .line 266
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {v5, v2}, Lcom/samsung/android/app/FileShareClient/ClientService;->getOutbound(I)Lcom/samsung/android/app/FileShareClient/Outbound;

    move-result-object v1

    .line 267
    .local v1, "outbound":Lcom/samsung/android/app/FileShareClient/Outbound;
    if-eqz v1, :cond_0

    .line 268
    new-instance v0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;-><init>(Landroid/content/Context;Lcom/samsung/android/app/FileShareClient/Outbound;)V

    .line 269
    .local v0, "customAdapter":Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->getCount()I

    move-result v5

    if-ne v5, v3, :cond_0

    .line 270
    const-string v4, "OutboudActivity"

    const-string v5, "isSingleMode"

    const-string v6, "true"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    .end local v0    # "customAdapter":Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;
    .end local v1    # "outbound":Lcom/samsung/android/app/FileShareClient/Outbound;
    :goto_0
    return v3

    .line 275
    :cond_0
    const-string v3, "OutboudActivity"

    const-string v5, "isSingleMode"

    const-string v6, "false"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 276
    goto :goto_0
.end method

.method private setFragment(I)V
    .locals 2
    .param p1, "currentFragment"    # I

    .prologue
    .line 297
    iput p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mCurrentFragment:I

    .line 298
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mFragmentTransaction:Landroid/app/FragmentTransaction;

    .line 299
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mCurrentFragment:I

    const/16 v1, 0x1f5

    if-ne v0, v1, :cond_0

    .line 300
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mFragmentTransaction:Landroid/app/FragmentTransaction;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 301
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mFragmentTransaction:Landroid/app/FragmentTransaction;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 306
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mFragmentTransaction:Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 307
    return-void

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mFragmentTransaction:Landroid/app/FragmentTransaction;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 304
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mFragmentTransaction:Landroid/app/FragmentTransaction;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0
.end method

.method private setGrandeDisplayView(Z)V
    .locals 9
    .param p1, "land"    # Z

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 439
    const v3, 0x7f06001a

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    .line 440
    const v3, 0x7f06001b

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->left:Landroid/widget/RelativeLayout;

    .line 441
    const v3, 0x7f06001f

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->right:Landroid/widget/RelativeLayout;

    .line 442
    const v3, 0x7f06001c

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->center:Landroid/widget/RelativeLayout;

    .line 444
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->left:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 445
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->right:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 446
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->center:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 448
    if-eqz p1, :cond_0

    .line 449
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v6, v5, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 451
    .local v1, "leftParams":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v6, v5, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 453
    .local v2, "rightParams":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v3, 0x41000000    # 8.0f

    invoke-direct {v0, v6, v5, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 467
    .local v0, "centerParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->left:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020022

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 469
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->right:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020024

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 473
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->left:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 474
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->right:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 475
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->center:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 495
    .end local v0    # "centerParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v1    # "leftParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v2    # "rightParams":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->left:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 496
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->center:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 497
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->blankLayout:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->right:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 498
    return-void

    .line 477
    :cond_0
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 479
    .local v1, "leftParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0x9

    invoke-virtual {v1, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 480
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 482
    .local v2, "rightParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xb

    invoke-virtual {v2, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 483
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getDisplayWidth(Landroid/content/Context;)I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    invoke-direct {v0, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 487
    .local v0, "centerParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->left:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 489
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->right:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 490
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->left:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 491
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->right:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 492
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->center:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private setOptionMenuVisiblity()V
    .locals 2

    .prologue
    .line 289
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mCurrentFragment:I

    const/16 v1, 0x1f5

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getCompleted()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->notifyOutboundMenuChanged(Z)V

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mCurrentFragment:I

    const/16 v1, 0x1f6

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getEnableResend()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->notifyReceiverMenuChanged(I)V

    goto :goto_0
.end method

.method private startService()V
    .locals 4

    .prologue
    .line 357
    const-string v1, "OutboudActivity"

    const-string v2, "startService"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 359
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 360
    const-string v1, "com.samsung.android.app.FileShareClient.SERVICE_START"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 361
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 362
    return-void
.end method

.method private unbindClientService()V
    .locals 3

    .prologue
    .line 347
    const-string v0, "OutboudActivity"

    const-string v1, "unbindClientService"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 353
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    .line 354
    return-void
.end method


# virtual methods
.method public bindClientService()V
    .locals 4

    .prologue
    .line 337
    const-string v1, "OutboudActivity"

    const-string v2, "bindClientService"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    if-nez v1, :cond_0

    .line 339
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 340
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 341
    const-string v1, "com.samsung.android.app.FileShareClient.SERVICE_BIND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 342
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 344
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public notifyOutboundMenuChanged(Z)V
    .locals 3
    .param p1, "completed"    # Z

    .prologue
    const/4 v1, 0x0

    .line 386
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelAllItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuClearHistoryItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelConnectionItem:Landroid/view/MenuItem;

    if-nez v0, :cond_1

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 389
    :cond_1
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mCurrentFragment:I

    const/16 v2, 0x1f5

    if-ne v0, v2, :cond_0

    .line 390
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelAllItem:Landroid/view/MenuItem;

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 391
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuClearHistoryItem:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 392
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 393
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelConnectionItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 394
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelTransfer:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_2
    move v0, v1

    .line 390
    goto :goto_1
.end method

.method public notifyReceiverMenuChanged(I)V
    .locals 4
    .param p1, "enable"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 405
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelAllItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuClearHistoryItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelConnectionItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelTransfer:Landroid/view/MenuItem;

    if-nez v0, :cond_1

    .line 433
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mCurrentFragment:I

    const/16 v1, 0x1f6

    if-ne v0, v1, :cond_0

    .line 410
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelAllItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 411
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuClearHistoryItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 412
    if-ne p1, v3, :cond_2

    .line 413
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 414
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelConnectionItem:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 415
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelConnectionItem:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 416
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelTransfer:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 417
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 418
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 419
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 420
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelConnectionItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 421
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelTransfer:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 422
    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    .line 423
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 424
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 425
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelConnectionItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 426
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelTransfer:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 427
    :cond_4
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 429
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelConnectionItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 430
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelTransfer:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/16 v2, 0x1f5

    .line 131
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mCurrentFragment:I

    if-ne v0, v2, :cond_1

    .line 132
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 133
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->onBackPressed()V

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mCurrentFragment:I

    const/16 v1, 0x1f6

    if-ne v0, v1, :cond_0

    .line 135
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->onBackPressed()V

    .line 137
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIntent:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->isSingleMode(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    .line 140
    :cond_2
    const v0, 0x7f040008

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setTitle(I)V

    .line 141
    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setFragment(I)V

    .line 142
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setOptionMenuVisiblity()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x1

    .line 150
    const-string v1, "OutboudActivity"

    const-string v3, "onConfigurationChanged"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v2, :cond_3

    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIsLandscape:Z

    .line 154
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIsGrandeDisplay:Z

    if-eqz v1, :cond_0

    .line 155
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIsLandscape:Z

    invoke-direct {p0, v1}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setGrandeDisplayView(Z)V

    .line 158
    :cond_0
    :try_start_0
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->deviceType:Ljava/lang/String;

    const-string v3, "tablet"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v1, v2, :cond_2

    .line 160
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelAllItem:Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 161
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuClearHistoryItem:Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 162
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 163
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelConnectionItem:Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 164
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelTransfer:Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 176
    return-void

    :cond_3
    move v1, v2

    .line 152
    goto :goto_0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "OutboudActivity"

    const-string v2, "onConfigurationChanged"

    const-string v3, "NullPointerException"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x400

    .line 72
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIntent:Landroid/content/Intent;

    .line 75
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "action":Ljava/lang/String;
    const-string v2, "OutboudActivity"

    const-string v3, "onCreate"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->deviceType:Ljava/lang/String;

    const-string v3, "tablet"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 83
    :cond_0
    const v2, 0x7f030004

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setContentView(I)V

    .line 84
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 85
    .local v1, "titleBar":Landroid/app/ActionBar;
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 86
    const v2, 0x7f040008

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setTitle(I)V

    .line 88
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIsLandscape:Z

    .line 89
    const-string v2, "grande"

    const-string v3, "ro.build.scafe.size"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIsGrandeDisplay:Z

    .line 92
    iget-boolean v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIsGrandeDisplay:Z

    if-eqz v2, :cond_1

    .line 93
    iget-boolean v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIsLandscape:Z

    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setGrandeDisplayView(Z)V

    .line 95
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->startService()V

    .line 96
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->bindClientService()V

    .line 98
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const v3, 0x7f06001d

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    .line 100
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    invoke-virtual {v2, p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->setListener(Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;)V

    .line 101
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->setIntent(Landroid/content/Intent;)V

    .line 103
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const v3, 0x7f06001e

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    .line 106
    const/16 v2, 0x1f5

    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setFragment(I)V

    .line 107
    return-void

    .line 88
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v1, 0x5

    .line 181
    const v0, 0x7f040014

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelAllItem:Landroid/view/MenuItem;

    .line 182
    const v0, 0x7f040016

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuClearHistoryItem:Landroid/view/MenuItem;

    .line 183
    const v0, 0x7f040017

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    .line 184
    const v0, 0x7f040015

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelConnectionItem:Landroid/view/MenuItem;

    .line 185
    const/high16 v0, 0x7f040000

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelTransfer:Landroid/view/MenuItem;

    .line 193
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelAllItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 195
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuClearHistoryItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 197
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 199
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelConnectionItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 201
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelTransfer:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 203
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setOptionMenuVisiblity()V

    .line 204
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 123
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 124
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->unbindClientService()V

    .line 125
    const-string v0, "OutboudActivity"

    const-string v1, "onDestroy"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 227
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 229
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 230
    .local v1, "oldAct":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 231
    .local v0, "newAct":Ljava/lang/String;
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mIntent:Landroid/content/Intent;

    .line 233
    const-string v2, "OutboudActivity"

    const-string v3, "onNewIntent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "new : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "old : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v2, "onNewIntent"

    invoke-virtual {p1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 236
    const-string v2, "com.samsung.android.app.FileShareClient.SEND_RESULT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 237
    const-string v2, "bOutboundClear"

    invoke-virtual {p1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 239
    :cond_0
    const-string v2, "com.samsung.android.app.FileShareClient.SEND_RESULT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 241
    const-string v2, "OutboudActivity"

    const-string v3, "onNewIntent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mNotiCnt ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    sget v2, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    if-nez v2, :cond_1

    .line 243
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    if-eqz v2, :cond_1

    .line 244
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    invoke-virtual {v2, v6}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->setStopForegroundService(Z)V

    .line 248
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setIntent(Landroid/content/Intent;)V

    .line 250
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->isSingleMode(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 251
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    if-eqz v2, :cond_2

    .line 252
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    invoke-virtual {v2, p1}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->setNewIntent(Landroid/content/Intent;)V

    .line 253
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->onBackPressed()V

    .line 255
    :cond_2
    const/16 v2, 0x1f5

    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setFragment(I)V

    .line 256
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setOptionMenuVisiblity()V

    .line 260
    :goto_0
    return-void

    .line 258
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    invoke-virtual {v2, p1}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->setNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 210
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelAllItem:Landroid/view/MenuItem;

    if-ne p1, v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->onCancelAllMenuClicked()V

    .line 221
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuClearHistoryItem:Landroid/view/MenuItem;

    if-ne p1, v0, :cond_2

    .line 213
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->onClearHistoryMenuClicked()V

    goto :goto_0

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuResendAllItem:Landroid/view/MenuItem;

    if-ne p1, v0, :cond_3

    .line 215
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->onResendAllMenuClicked()V

    goto :goto_0

    .line 216
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelConnectionItem:Landroid/view/MenuItem;

    if-ne p1, v0, :cond_4

    .line 217
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->onCancelConnectMenuClicked()V

    goto :goto_0

    .line 218
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mMenuCancelTransfer:Landroid/view/MenuItem;

    if-ne p1, v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->onCancelTransferMenuClicked()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 111
    const-string v0, "OutboudActivity"

    const-string v1, "onPause"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 113
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 118
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 119
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 282
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 283
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->requestFinish()V

    .line 286
    :cond_0
    return-void
.end method

.method public startReceiverFragment(II)V
    .locals 3
    .param p1, "outboundId"    # I
    .param p2, "receiverId"    # I

    .prologue
    .line 370
    const-string v0, "OutboudActivity"

    const-string v1, "startReceiverFragment"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/FileShareClient/ClientService;->getOutbound(I)Lcom/samsung/android/app/FileShareClient/Outbound;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 373
    const v0, 0x7f040008

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setTitle(I)V

    .line 374
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->setIdInformation(II)V

    .line 375
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->setListener(Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;)V

    .line 376
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mReceiverFragment:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->setClientService(Lcom/samsung/android/app/FileShareClient/ClientService;)V

    .line 378
    const/16 v0, 0x1f6

    invoke-direct {p0, v0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setFragment(I)V

    .line 379
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->setOptionMenuVisiblity()V

    .line 381
    :cond_0
    return-void
.end method
