.class Lcom/samsung/android/app/FileShareClient/Outbound$3$1;
.super Ljava/lang/Object;
.source "Outbound.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/FileShareClient/Outbound$3;->onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/FileShareClient/Outbound$3;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/Outbound$3;)V
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$3$1;->this$1:Lcom/samsung/android/app/FileShareClient/Outbound$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 4
    .param p1, "reason"    # I

    .prologue
    .line 441
    const-string v0, "Outbound"

    const-string v1, "requestPeer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " connect fail "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    return-void
.end method

.method public onSuccess()V
    .locals 3

    .prologue
    .line 437
    const-string v0, "Outbound"

    const-string v1, "requestPeer"

    const-string v2, " connect success"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    return-void
.end method
