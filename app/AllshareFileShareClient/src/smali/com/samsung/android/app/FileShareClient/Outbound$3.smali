.class Lcom/samsung/android/app/FileShareClient/Outbound$3;
.super Ljava/lang/Object;
.source "Outbound.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/FileShareClient/Outbound;->requestPeer(Lcom/samsung/android/app/FileShareClient/Receiver;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

.field final synthetic val$r:Lcom/samsung/android/app/FileShareClient/Receiver;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/Outbound;Lcom/samsung/android/app/FileShareClient/Receiver;)V
    .locals 0

    .prologue
    .line 416
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$3;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    iput-object p2, p0, Lcom/samsung/android/app/FileShareClient/Outbound$3;->val$r:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 7
    .param p1, "arg0"    # Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .prologue
    .line 420
    const-string v3, "Outbound"

    const-string v4, "requestPeer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requestConnection = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound$3;->val$r:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const-string v3, "Outbound"

    const-string v4, "requestPeer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requestConnection = r.MacAddr = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound$3;->val$r:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 424
    const-string v3, "Outbound"

    const-string v4, "requestPeer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requestConnection = arg0 size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    :cond_0
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 428
    .local v1, "dev":Landroid/net/wifi/p2p/WifiP2pDevice;
    const-string v3, "Outbound"

    const-string v4, "requestPeer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requestConnection = dev.MacAddr = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-object v3, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$3;->val$r:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound$3;->val$r:Lcom/samsung/android/app/FileShareClient/Receiver;

    iget-object v4, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->isMyMacAddress(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 432
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound$3;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v4, 0x0

    # setter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqReconnect:Z
    invoke-static {v3, v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$302(Lcom/samsung/android/app/FileShareClient/Outbound;Z)Z

    .line 433
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound$3;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->getPreferredConfig(Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;
    invoke-static {v3, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$400(Lcom/samsung/android/app/FileShareClient/Outbound;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;

    move-result-object v0

    .line 434
    .local v0, "config":Landroid/net/wifi/p2p/WifiP2pConfig;
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound$3;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v3}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$600(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$3;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$500(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v4

    new-instance v5, Lcom/samsung/android/app/FileShareClient/Outbound$3$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$3$1;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound$3;)V

    invoke-virtual {v3, v4, v0, v5}, Landroid/net/wifi/p2p/WifiP2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pConfig;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto :goto_0

    .line 446
    .end local v0    # "config":Landroid/net/wifi/p2p/WifiP2pConfig;
    .end local v1    # "dev":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_2
    return-void
.end method
