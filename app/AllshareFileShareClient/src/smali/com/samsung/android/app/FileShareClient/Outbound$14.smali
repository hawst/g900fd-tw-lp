.class Lcom/samsung/android/app/FileShareClient/Outbound$14;
.super Landroid/os/Handler;
.source "Outbound.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/Outbound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/Outbound;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0

    .prologue
    .line 1291
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$14;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v6, 0x3ea

    .line 1296
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound$14;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1297
    const-string v2, "Outbound"

    const-string v3, "mTimerHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] mReceiverList is null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1326
    :cond_0
    :goto_0
    return-void

    .line 1302
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound$14;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$800(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/allshare/file/FileServiceProvider;

    move-result-object v2

    if-nez v2, :cond_2

    .line 1303
    const-string v2, "Outbound"

    const-string v3, "mTimerHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] mServiceProvider is null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1308
    :cond_2
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x3e9

    if-ne v2, v3, :cond_3

    .line 1309
    const-string v2, "Outbound"

    const-string v3, "mTimerHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Time Out ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1310
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1311
    .local v0, "id":I
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound$14;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 1312
    .local v1, "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 1313
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 1314
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setErrorCause(I)V

    .line 1315
    const/16 v2, 0x68

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileStatus(I)V

    .line 1316
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    goto/16 :goto_0

    .line 1319
    .end local v0    # "id":I
    .end local v1    # "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_3
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v6, :cond_0

    .line 1320
    const-string v2, "Outbound"

    const-string v3, "mTimerHandler"

    const-string v4, "DeviceFinder refresh."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1321
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound$14;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->refreshAndFindDev()V
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$900(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    .line 1322
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound$14;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->isConnectedDevice()Z
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1000(Lcom/samsung/android/app/FileShareClient/Outbound;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1323
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound$14;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->sendRefreshTimeout()V
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1100(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    goto/16 :goto_0
.end method
