.class public Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;
.super Landroid/app/Activity;
.source "DeviceSelectActivity.java"


# static fields
.field public static final DEVICE_PICKER_REQUEST:I = 0x0

.field private static final EXTRA_INFO_TYPE:Ljava/lang/String; = "info_type"

.field private static final HANDLE_DELAYED_FINISH:I = 0x3e8

.field private static final TAGClass:Ljava/lang/String; = "DeviceSelectActivity"


# instance fields
.field private INFO_TYPE_DPM_WIFI:I

.field private mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mHandler:Landroid/os/Handler;

.field private mP2pDeviceInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mThemeContext:Landroid/content/Context;

.field private mWifiStatus:Z

.field private showagain:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mWifiStatus:Z

    .line 44
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mP2pDeviceInfoList:Ljava/util/ArrayList;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mThemeContext:Landroid/content/Context;

    .line 53
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->INFO_TYPE_DPM_WIFI:I

    .line 314
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    .line 316
    new-instance v0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$5;-><init>(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mConnection:Landroid/content/ServiceConnection;

    .line 398
    new-instance v0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$6;-><init>(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->showagain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->setDoNotShowDialog_Pref()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->onStartActivity()V

    return-void
.end method

.method static synthetic access$402(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/app/FileShareClient/ClientService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;
    .param p1, "x1"    # Lcom/samsung/android/app/FileShareClient/ClientService;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mP2pDeviceInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->startSending()V

    return-void
.end method

.method private bindClientService()V
    .locals 4

    .prologue
    .line 339
    const-string v1, "DeviceSelectActivity"

    const-string v2, "bindClientService"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    if-nez v1, :cond_0

    .line 342
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 343
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mThemeContext:Landroid/content/Context;

    const-class v2, Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 344
    const-string v1, "com.samsung.android.app.FileShareClient.SERVICE_BIND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 346
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->startSending()V

    .line 348
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private getDoNotShowDialog_Pref()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 60
    const-string v2, "showdialog"

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 61
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 62
    .local v1, "show_dialog":Ljava/lang/Boolean;
    const-string v2, "showdialog"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 64
    const-string v2, "DeviceSelectActivity"

    const-string v3, "getDoNotShowDialog_Pref"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "show_dialog : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    return v2
.end method

.method private onAgainStartActivity()V
    .locals 0

    .prologue
    .line 222
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->startService()V

    .line 223
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->bindClientService()V

    .line 224
    return-void
.end method

.method private onStartActivity()V
    .locals 3

    .prologue
    .line 212
    const-string v0, "DeviceSelectActivity"

    const-string v1, "onStartActivity"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->startDevicePicker()V

    .line 215
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->startService()V

    .line 216
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->bindClientService()V

    .line 217
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mThemeContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Utils;->saveP2pStatus(Z)V

    .line 218
    return-void
.end method

.method private setDoNotShowDialog_Pref()V
    .locals 4

    .prologue
    .line 69
    const-string v2, "showdialog"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 70
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 71
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v2, "showdialog"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 72
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 73
    return-void
.end method

.method private setThemeContext()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const/high16 v1, 0x7f050000

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mThemeContext:Landroid/content/Context;

    .line 57
    return-void
.end method

.method private showWiFiTurnOnDialog()V
    .locals 6

    .prologue
    .line 76
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mThemeContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 77
    .local v0, "adbInflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03000c

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 78
    .local v2, "skipDialog":Landroid/view/View;
    const v3, 0x7f06003a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->showagain:Landroid/widget/CheckBox;

    .line 80
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->showagain:Landroid/widget/CheckBox;

    new-instance v4, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$1;-><init>(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 90
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mThemeContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 91
    .local v1, "mAlertDialog":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f04003b

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f040025

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f040007

    new-instance v5, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$4;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$4;-><init>(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x7f040000

    new-instance v5, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$3;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$3;-><init>(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$2;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity$2;-><init>(Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 127
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 128
    return-void
.end method

.method private startDevicePicker()V
    .locals 4

    .prologue
    .line 373
    const-string v1, "DeviceSelectActivity"

    const-string v2, "startDevicePicker"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_DIRECT_DEVICE_PICKER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 376
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 377
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 378
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 379
    return-void
.end method

.method private startSending()V
    .locals 3

    .prologue
    .line 382
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    if-nez v0, :cond_0

    .line 383
    const-string v0, "DeviceSelectActivity"

    const-string v1, "startSending"

    const-string v2, "not bind service"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :goto_0
    return-void

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mP2pDeviceInfoList:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 387
    const-string v0, "DeviceSelectActivity"

    const-string v1, "startSending"

    const-string v2, "not ready device list"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 391
    :cond_1
    const-string v0, "DeviceSelectActivity"

    const-string v1, "startSending"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mP2pDeviceInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/ClientService;->createOutbound(Landroid/content/Intent;Ljava/util/ArrayList;)Lcom/samsung/android/app/FileShareClient/Outbound;

    .line 394
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 395
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->finish()V

    goto :goto_0
.end method

.method private startService()V
    .locals 4

    .prologue
    .line 360
    const-string v1, "DeviceSelectActivity"

    const-string v2, "startService"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 363
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mThemeContext:Landroid/content/Context;

    const-class v2, Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 364
    const-string v1, "com.samsung.android.app.FileShareClient.SERVICE_BIND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 365
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 366
    return-void
.end method

.method private unbindClientService()V
    .locals 3

    .prologue
    .line 351
    const-string v0, "DeviceSelectActivity"

    const-string v1, "unbindClientService"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 356
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    .line 357
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 12
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 248
    const-string v8, "DeviceSelectActivity"

    const-string v9, "onActivityResult"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    if-nez p2, :cond_3

    .line 252
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "connectivity"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 255
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/16 v8, 0xd

    invoke-virtual {v0, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v5

    .line 258
    .local v5, "p2p_NetInfo":Landroid/net/NetworkInfo;
    const-string v8, "wifi"

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/WifiManager;

    .line 260
    .local v6, "wifiManager":Landroid/net/wifi/WifiManager;
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v7

    .line 263
    .local v7, "wlan_NetInfo":Landroid/net/NetworkInfo;
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mThemeContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 264
    const-string v8, "DeviceSelectActivity"

    const-string v9, "onActivityResult"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "p2p_NetInfo: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 267
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v5    # "p2p_NetInfo":Landroid/net/NetworkInfo;
    .end local v6    # "wifiManager":Landroid/net/wifi/WifiManager;
    .end local v7    # "wlan_NetInfo":Landroid/net/NetworkInfo;
    :goto_0
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/Utils;->resetP2pStatus()V

    .line 306
    :goto_1
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 307
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->startSending()V

    .line 308
    return-void

    .line 268
    .restart local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .restart local v5    # "p2p_NetInfo":Landroid/net/NetworkInfo;
    .restart local v6    # "wifiManager":Landroid/net/wifi/WifiManager;
    .restart local v7    # "wlan_NetInfo":Landroid/net/NetworkInfo;
    :cond_0
    if-eqz v7, :cond_1

    :try_start_1
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 269
    const-string v8, "DeviceSelectActivity"

    const-string v9, "onActivityResult"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "wlan_NetInfo: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 272
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->finish()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 282
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v5    # "p2p_NetInfo":Landroid/net/NetworkInfo;
    .end local v6    # "wifiManager":Landroid/net/wifi/WifiManager;
    .end local v7    # "wlan_NetInfo":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v2

    .line 283
    .local v2, "e":Ljava/lang/Exception;
    const-string v8, "DeviceSelectActivity"

    const-string v9, "onActivityResult"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "errmsg=["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 274
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .restart local v5    # "p2p_NetInfo":Landroid/net/NetworkInfo;
    .restart local v6    # "wifiManager":Landroid/net/wifi/WifiManager;
    .restart local v7    # "wlan_NetInfo":Landroid/net/NetworkInfo;
    :cond_1
    :try_start_2
    iget-boolean v8, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mWifiStatus:Z

    if-nez v8, :cond_2

    .line 275
    const-string v8, "DeviceSelectActivity"

    const-string v9, "onActivityResult"

    const-string v10, "Pre Condition Wi-Fi Disabled before FileShare. So set to Wi-Fi disabled."

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 279
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 280
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->finish()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 289
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v5    # "p2p_NetInfo":Landroid/net/NetworkInfo;
    .end local v6    # "wifiManager":Landroid/net/wifi/WifiManager;
    .end local v7    # "wlan_NetInfo":Landroid/net/NetworkInfo;
    :cond_3
    const-string v8, "com.android.settings.wifi.p2p.WifiP2pDeviceList"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .line 292
    .local v1, "deviceList":Landroid/net/wifi/p2p/WifiP2pDeviceList;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mP2pDeviceInfoList:Ljava/util/ArrayList;

    .line 294
    const-string v8, "DeviceSelectActivity"

    const-string v9, "onActivityResult"

    const-string v10, "+--------------------------------------------------------------------------+"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const-string v8, "DeviceSelectActivity"

    const-string v9, "onActivityResult"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "devicelist size = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Collection;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 300
    .local v4, "p2p":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mP2pDeviceInfoList:Ljava/util/ArrayList;

    new-instance v9, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-direct {v9, v4}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;-><init>(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 303
    .end local v4    # "p2p":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_4
    const-string v8, "DeviceSelectActivity"

    const-string v9, "onActivityResult"

    const-string v10, "+--------------------------------------------------------------------------+"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 133
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 134
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->setThemeContext()V

    .line 136
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 137
    .local v7, "action":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "CHECKER"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 138
    .local v6, "Checker":I
    const-string v0, "DeviceSelectActivity"

    const-string v2, "OnCreate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Checker = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/net/wifi/WifiManager;

    .line 141
    .local v10, "wifiManager":Landroid/net/wifi/WifiManager;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v2, "true"

    aput-object v2, v4, v0

    .line 142
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v0, "content://com.sec.knox.provider/RestrictionPolicy4"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 143
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const-string v3, "isWifiEnabled"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 145
    .local v8, "wifiCr":Landroid/database/Cursor;
    if-eqz v8, :cond_2

    .line 147
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 148
    const-string v0, "isWifiEnabled"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "false"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    const-string v0, "DeviceSelectActivity"

    const-string v2, "onCreate : MDM -> WiFi Not allowed"

    const-string v3, ""

    invoke-static {v0, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 151
    new-instance v11, Landroid/content/Intent;

    const-string v0, "android.net.wifi.SHOW_INFO_MESSAGE"

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 152
    .local v11, "wifiToastIntent":Landroid/content/Intent;
    const-string v0, "info_type"

    iget v2, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->INFO_TYPE_DPM_WIFI:I

    invoke-virtual {v11, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 153
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 154
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 209
    .end local v11    # "wifiToastIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 161
    :cond_2
    const-string v0, "content://com.sec.knox.provider/RestrictionPolicy4"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 162
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const-string v3, "isWifiDirectAllowed"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 164
    .local v9, "wifiDirectCr":Landroid/database/Cursor;
    if-eqz v9, :cond_4

    .line 166
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 167
    const-string v0, "isWifiDirectAllowed"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "false"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 168
    const-string v0, "DeviceSelectActivity"

    const-string v2, "onCreate : MDM -> WiFi Direct not allowed "

    const-string v3, ""

    invoke-static {v0, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 170
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->finish()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 174
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 158
    .end local v9    # "wifiDirectCr":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    .line 174
    .restart local v9    # "wifiDirectCr":Landroid/database/Cursor;
    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 177
    :cond_4
    if-nez v6, :cond_9

    .line 178
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "CHECKER"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 179
    if-nez v7, :cond_5

    .line 180
    const-string v0, "DeviceSelectActivity"

    const-string v2, "OnCreate"

    const-string v3, " action = null. Do not start DeviceSelectActivity"

    invoke-static {v0, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 182
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->finish()V

    goto :goto_0

    .line 174
    :catchall_1
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    .line 184
    :cond_5
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    :cond_6
    invoke-virtual {v10}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mWifiStatus:Z

    .line 188
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "mWifiStatus"

    iget-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mWifiStatus:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 189
    const-string v0, "DeviceSelectActivity"

    const-string v2, "onCreate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " WiFiStatus = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v5, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mWifiStatus:Z

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getDoNotShowDialog_Pref()Z

    move-result v0

    if-nez v0, :cond_8

    .line 192
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mWifiStatus:Z

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    .line 193
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->onStartActivity()V

    goto/16 :goto_0

    .line 195
    :cond_7
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->showWiFiTurnOnDialog()V

    goto/16 :goto_0

    .line 198
    :cond_8
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->onStartActivity()V

    goto/16 :goto_0

    .line 204
    :cond_9
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->onAgainStartActivity()V

    .line 205
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "mWifiStatus"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mWifiStatus:Z

    .line 207
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3e8

    const-wide/16 v12, 0x7d0

    invoke-virtual {v0, v2, v12, v13}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 228
    const-string v0, "DeviceSelectActivity"

    const-string v1, "onDestroy"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 230
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/DeviceSelectActivity;->unbindClientService()V

    .line 231
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 235
    const-string v0, "DeviceSelectActivity"

    const-string v1, "onPause"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 237
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 241
    const-string v0, "DeviceSelectActivity"

    const-string v1, "onResume"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 243
    return-void
.end method
