.class Lcom/samsung/android/app/FileShareClient/Outbound$12;
.super Landroid/os/Handler;
.source "Outbound.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/Outbound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/Outbound;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0

    .prologue
    .line 1198
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x0

    .line 1201
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 1262
    :cond_0
    :goto_0
    return-void

    .line 1203
    :pswitch_0
    const-string v4, "Outbound"

    const-string v5, "mFileMakerHandler"

    const-string v6, "setFileIntent: FileManager.GENERATE_FAIL"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mbShowWait:Z
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1800(Lcom/samsung/android/app/FileShareClient/Outbound;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1206
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # setter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mbShowWait:Z
    invoke-static {v4, v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1802(Lcom/samsung/android/app/FileShareClient/Outbound;Z)Z

    .line 1207
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 1208
    .local v2, "r":Lcom/samsung/android/app/FileShareClient/Receiver;
    const/16 v4, 0x3ef

    invoke-virtual {v2, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 1209
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    goto :goto_1

    .line 1212
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "r":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2700(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2700(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f04003a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1217
    :pswitch_1
    const-string v4, "Outbound"

    const-string v5, "mFileMakerHandler"

    const-string v6, "setFileIntent: FileManager.GENERATING"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1220
    :pswitch_2
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v4

    if-nez v4, :cond_2

    .line 1221
    const-string v4, "Outbound"

    const-string v5, "mFileMakerHandler"

    const-string v6, "setFileIntent: GENERATE_SUCCESS - null == mReceiverList"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1225
    :cond_2
    const-string v4, "Outbound"

    const-string v5, "mFileMakerHandler"

    const-string v6, "setFileIntent: GENERATE_SUCCESS"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mFileManager:Lcom/samsung/android/app/FileShareClient/FileManager;
    invoke-static {v5}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$200(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/FileManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareClient/FileManager;->getFileArryList()Ljava/util/ArrayList;

    move-result-object v5

    # setter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;
    invoke-static {v4, v5}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1702(Lcom/samsung/android/app/FileShareClient/Outbound;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1230
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1700(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v4

    if-nez v4, :cond_5

    .line 1231
    const-string v4, "Outbound"

    const-string v5, "mFileMakerHandler"

    const-string v6, "setFileIntent: GENERATE_SUCCESS - mFileArrayList == null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1238
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2900(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 1239
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2900(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;->readyFilelist()V

    .line 1242
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 1243
    .local v3, "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1700(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileList(Ljava/util/ArrayList;)V

    .line 1244
    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->startReceiveByCondition()V

    goto :goto_2

    .line 1234
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1700(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 1235
    .local v0, "file":Ljava/io/File;
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    # += operator for: Lcom/samsung/android/app/FileShareClient/Outbound;->mTotalSize:J
    invoke-static {v4, v6, v7}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2814(Lcom/samsung/android/app/FileShareClient/Outbound;J)J

    goto :goto_3

    .line 1247
    .end local v0    # "file":Ljava/io/File;
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mbShowWait:Z
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1800(Lcom/samsung/android/app/FileShareClient/Outbound;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1248
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # setter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mbShowWait:Z
    invoke-static {v4, v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1802(Lcom/samsung/android/app/FileShareClient/Outbound;Z)Z

    .line 1249
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v5, 0x1

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->addOngoing(ZZ)V
    invoke-static {v4, v5, v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$3000(Lcom/samsung/android/app/FileShareClient/Outbound;ZZ)V

    .line 1250
    sget v4, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    if-nez v4, :cond_7

    .line 1251
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mIfg:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2300(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;

    move-result-object v4

    invoke-interface {v4}, Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;->startOutboundForeground()V

    .line 1254
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound$12;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mIfg:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;
    invoke-static {v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2300(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;

    move-result-object v4

    invoke-interface {v4}, Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;->acquireWakeLock()V

    .line 1256
    sget v4, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    .line 1257
    const-string v4, "Outbound"

    const-string v5, "mFileMakerHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updateStatus : mNotiCnt ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1201
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
