.class Lcom/samsung/android/app/FileShareClient/Outbound$7;
.super Ljava/lang/Object;
.source "Outbound.java"

# interfaces
.implements Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/FileShareClient/Outbound;->requestServiceProvider()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/Outbound;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0

    .prologue
    .line 669
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$7;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreated(Lcom/samsung/android/allshare/ServiceProvider;Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V
    .locals 4
    .param p1, "sprovider"    # Lcom/samsung/android/allshare/ServiceProvider;
    .param p2, "state"    # Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .prologue
    .line 674
    const-string v0, "Outbound"

    const-string v1, "requestServiceProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createServiceProvider >"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound$7;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqSvcProvider:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$702(Lcom/samsung/android/app/FileShareClient/Outbound;Z)Z

    .line 677
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound$7;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    check-cast p1, Lcom/samsung/android/allshare/file/FileServiceProvider;

    .end local p1    # "sprovider":Lcom/samsung/android/allshare/ServiceProvider;
    # setter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;
    invoke-static {v0, p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$802(Lcom/samsung/android/app/FileShareClient/Outbound;Lcom/samsung/android/allshare/file/FileServiceProvider;)Lcom/samsung/android/allshare/file/FileServiceProvider;

    .line 679
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound$7;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->refreshAndFindDev()V
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$900(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    .line 680
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound$7;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->isConnectedDevice()Z
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1000(Lcom/samsung/android/app/FileShareClient/Outbound;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound$7;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->sendRefreshTimeout()V
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1100(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    .line 683
    :cond_0
    return-void
.end method

.method public onDeleted(Lcom/samsung/android/allshare/ServiceProvider;)V
    .locals 3
    .param p1, "sprovider"    # Lcom/samsung/android/allshare/ServiceProvider;

    .prologue
    .line 687
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound$7;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqSvcProvider:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$702(Lcom/samsung/android/app/FileShareClient/Outbound;Z)Z

    .line 688
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound$7;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;
    invoke-static {v0, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$802(Lcom/samsung/android/app/FileShareClient/Outbound;Lcom/samsung/android/allshare/file/FileServiceProvider;)Lcom/samsung/android/allshare/file/FileServiceProvider;

    .line 689
    const-string v0, "Outbound"

    const-string v1, "requestServiceProvider"

    const-string v2, "onDeleted"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    return-void
.end method
