.class public Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;
.super Landroid/widget/BaseAdapter;
.source "OutboundListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final TAGClass:Ljava/lang/String; = "OutboundListAdapter"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mContext:Landroid/content/Context;

.field private mInflator:Landroid/view/LayoutInflater;

.field private mItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/FileShareClient/Receiver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "outbound"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mAlertDialog:Landroid/app/AlertDialog;

    .line 39
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    .line 40
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mInflator:Landroid/view/LayoutInflater;

    .line 41
    invoke-virtual {p2}, Lcom/samsung/android/app/FileShareClient/Outbound;->getReceiverList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mItemList:Ljava/util/ArrayList;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;Lcom/samsung/android/app/FileShareClient/Receiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;
    .param p1, "x1"    # Lcom/samsung/android/app/FileShareClient/Receiver;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->showCancelConfirmDialog(Lcom/samsung/android/app/FileShareClient/Receiver;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private showCancelConfirmDialog(Lcom/samsung/android/app/FileShareClient/Receiver;)V
    .locals 3
    .param p1, "receiver"    # Lcom/samsung/android/app/FileShareClient/Receiver;

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->dismissDialog()V

    .line 208
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f040002

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f040024

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f040007

    new-instance v2, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$3;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$3;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;Lcom/samsung/android/app/FileShareClient/Receiver;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x7f040000

    new-instance v2, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$2;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mAlertDialog:Landroid/app/AlertDialog;

    .line 225
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 226
    return-void
.end method


# virtual methods
.method public addItem(Lcom/samsung/android/app/FileShareClient/Receiver;)V
    .locals 1
    .param p1, "r"    # Lcom/samsung/android/app/FileShareClient/Receiver;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->notifyDataSetChanged()V

    .line 81
    return-void
.end method

.method public dismissDialog()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 204
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 75
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 94
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 95
    .local v2, "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getCurrentCount()I

    move-result v0

    .line 96
    .local v0, "currentCount":I
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getTotalCount()I

    move-result v3

    .line 98
    .local v3, "totalCount":I
    if-nez p2, :cond_2

    .line 99
    new-instance v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;

    invoke-direct {v1}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;-><init>()V

    .line 100
    .local v1, "holder":Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mInflator:Landroid/view/LayoutInflater;

    const v5, 0x7f030005

    const/4 v6, 0x0

    invoke-virtual {v4, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 102
    const v4, 0x7f060020

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->completeLayout:Landroid/widget/LinearLayout;

    .line 103
    const v4, 0x7f060009

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressLayout:Landroid/widget/LinearLayout;

    .line 104
    const v4, 0x7f060013

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressSenderName:Landroid/widget/TextView;

    .line 106
    const v4, 0x7f060006

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->fileName:Landroid/widget/TextView;

    .line 107
    const v4, 0x7f060016

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->count:Landroid/widget/TextView;

    .line 108
    const v4, 0x7f060017

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->rate:Landroid/widget/TextView;

    .line 109
    const v4, 0x7f060014

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    iput-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progress:Landroid/widget/ProgressBar;

    .line 110
    const v4, 0x7f060019

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->cancelButton:Landroid/widget/ImageView;

    .line 111
    const v4, 0x7f06000b

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->deviceType:Landroid/widget/ImageView;

    .line 112
    const v4, 0x7f06000d

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->completeSenderName:Landroid/widget/TextView;

    .line 114
    const v4, 0x7f06000f

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->info:Landroid/widget/TextView;

    .line 115
    const v4, 0x7f06000e

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    iput-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressItem:Landroid/widget/ProgressBar;

    .line 116
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 121
    :goto_0
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v4

    const/16 v5, 0x3ec

    if-ne v4, v5, :cond_3

    .line 122
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->completeLayout:Landroid/widget/LinearLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 123
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressLayout:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 125
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressSenderName:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->fileName:Landroid/widget/TextView;

    if-eqz v4, :cond_0

    .line 127
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->fileName:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getTranferFileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :cond_0
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->count:Landroid/widget/TextView;

    const-string v5, "%d/%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    add-int/lit8 v8, v0, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->rate:Landroid/widget/TextView;

    const-string v5, "%d%%"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getProgress()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getProgress()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 133
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->cancelButton:Landroid/widget/ImageView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 134
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->cancelButton:Landroid/widget/ImageView;

    new-instance v5, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$1;

    invoke-direct {v5, p0, v2}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$1;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;Lcom/samsung/android/app/FileShareClient/Receiver;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    :cond_1
    :goto_1
    return-object p2

    .line 118
    .end local v1    # "holder":Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;

    .restart local v1    # "holder":Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;
    goto/16 :goto_0

    .line 143
    :cond_3
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->completeLayout:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 144
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressLayout:Landroid/widget/LinearLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 145
    if-eqz p2, :cond_1

    .line 146
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->deviceType:Landroid/widget/ImageView;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceTypeIcon()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 147
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->completeSenderName:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 189
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->info:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "error = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressItem:Landroid/widget/ProgressBar;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    .line 151
    :sswitch_0
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getErrorCause()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_5

    .line 152
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->info:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f040029

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    :cond_4
    :goto_2
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressItem:Landroid/widget/ProgressBar;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    .line 153
    :cond_5
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getErrorCause()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_6

    .line 154
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->info:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f040032

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 156
    :cond_6
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getErrorCause()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    .line 157
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->info:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f040029

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 161
    :sswitch_1
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->info:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f040035

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressItem:Landroid/widget/ProgressBar;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_1

    .line 165
    :sswitch_2
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->info:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const v7, 0x7f040035

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const v7, 0x7f040033

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    sub-int v10, v3, v0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressItem:Landroid/widget/ProgressBar;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_1

    .line 172
    :sswitch_3
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->info:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const v7, 0x7f040035

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const v7, 0x7f040034

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    sub-int v10, v3, v0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressItem:Landroid/widget/ProgressBar;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_1

    .line 180
    :sswitch_4
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->info:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f040028

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressItem:Landroid/widget/ProgressBar;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_1

    .line 185
    :sswitch_5
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->info:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f040027

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$ViewHolder;->progressItem:Landroid/widget/ProgressBar;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_1

    .line 149
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x3e9 -> :sswitch_4
        0x3ea -> :sswitch_5
        0x3eb -> :sswitch_5
        0x3ed -> :sswitch_2
        0x3ee -> :sswitch_1
        0x3ef -> :sswitch_3
    .end sparse-switch
.end method

.method public removeItem(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 84
    if-ltz p1, :cond_0

    .line 85
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 86
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->notifyDataSetChanged()V

    .line 88
    :cond_0
    return-void
.end method

.method public showCancelAllConfirmDialog(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 3
    .param p1, "outbound"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 294
    if-eqz p1, :cond_0

    .line 295
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->dismissDialog()V

    .line 296
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f040001

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f04001b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f040007

    new-instance v2, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$7;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$7;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;Lcom/samsung/android/app/FileShareClient/Outbound;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x7f040000

    new-instance v2, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$6;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$6;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mAlertDialog:Landroid/app/AlertDialog;

    .line 313
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 315
    :cond_0
    return-void
.end method

.method public showClearHistoryConfirmDialog(Lcom/samsung/android/app/FileShareClient/Outbound;Lcom/samsung/android/app/FileShareClient/ClientService;)V
    .locals 3
    .param p1, "outbound"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p2, "clientService"    # Lcom/samsung/android/app/FileShareClient/ClientService;

    .prologue
    .line 319
    if-eqz p1, :cond_0

    .line 320
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->dismissDialog()V

    .line 321
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f040016

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f040026

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f040007

    new-instance v2, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;Lcom/samsung/android/app/FileShareClient/Outbound;Lcom/samsung/android/app/FileShareClient/ClientService;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x7f040000

    new-instance v2, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$8;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$8;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mAlertDialog:Landroid/app/AlertDialog;

    .line 348
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 350
    :cond_0
    return-void
.end method

.method public showSendFileListDialog(Ljava/util/ArrayList;ZLjava/lang/String;)V
    .locals 9
    .param p2, "bCompleted"    # Z
    .param p3, "currentStateText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 231
    .local p1, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->dismissDialog()V

    .line 233
    iget-object v7, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 235
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const/high16 v7, 0x7f030000

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 239
    .local v4, "layout":Landroid/widget/LinearLayout;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v8, 0x6

    if-ge v7, v8, :cond_0

    .line 240
    const/high16 v7, 0x7f060000

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 241
    const v7, 0x7f060002

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 242
    const v7, 0x7f060001

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 249
    .local v5, "listLayout":Landroid/widget/LinearLayout;
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 250
    .local v1, "file":Ljava/io/File;
    const v7, 0x7f030009

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 251
    .local v6, "tv":Landroid/widget/TextView;
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    new-instance v7, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$4;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$4;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;)V

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 269
    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 244
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "listLayout":Landroid/widget/LinearLayout;
    .end local v6    # "tv":Landroid/widget/TextView;
    :cond_0
    const v7, 0x7f060002

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 245
    const/high16 v7, 0x7f060000

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 246
    const v7, 0x7f060003

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .restart local v5    # "listLayout":Landroid/widget/LinearLayout;
    goto :goto_0

    .line 272
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 273
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 275
    if-eqz p2, :cond_2

    .line 276
    invoke-virtual {v0, p3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 280
    :goto_2
    iget-object v7, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f040007

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$5;

    invoke-direct {v8, p0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$5;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 289
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mAlertDialog:Landroid/app/AlertDialog;

    .line 290
    iget-object v7, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    .line 291
    return-void

    .line 278
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f04000e

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_2
.end method
