.class Lcom/samsung/android/app/FileShareClient/Outbound$8;
.super Landroid/content/BroadcastReceiver;
.source "Outbound.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/Outbound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/Outbound;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0

    .prologue
    .line 908
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$8;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "it"    # Landroid/content/Intent;

    .prologue
    .line 911
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 912
    .local v0, "action":Ljava/lang/String;
    const-string v1, "Outbound"

    const-string v2, "mBroadcastReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceive: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 915
    const-string v1, "Outbound"

    const-string v2, "mBroadcastReceiver"

    const-string v3, "Local was changed"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$8;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->updateNotification()V
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1200(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    .line 918
    :cond_0
    return-void
.end method
