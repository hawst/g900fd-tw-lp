.class Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$7;
.super Ljava/lang/Object;
.source "OutboundListAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->showCancelAllConfirmDialog(Lcom/samsung/android/app/FileShareClient/Outbound;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

.field final synthetic val$outbound:Lcom/samsung/android/app/FileShareClient/Outbound;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$7;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    iput-object p2, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$7;->val$outbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 303
    const-string v0, "OutboundListAdapter"

    const-string v1, "showCancelAllConfirmDialog"

    const-string v2, "Cancel all clicked"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$7;->val$outbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/Outbound;->cancelAll()V

    .line 305
    return-void
.end method
