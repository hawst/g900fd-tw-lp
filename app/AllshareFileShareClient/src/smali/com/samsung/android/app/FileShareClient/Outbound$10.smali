.class Lcom/samsung/android/app/FileShareClient/Outbound$10;
.super Ljava/lang/Object;
.source "Outbound.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/FileShareClient/Outbound;->requestPeers(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/Outbound;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0

    .prologue
    .line 1138
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$10;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 8
    .param p1, "arg0"    # Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .prologue
    const/4 v7, 0x0

    .line 1143
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound$10;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mListenDevList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2600(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1144
    const-string v3, "Outbound"

    const-string v4, "requestPeers"

    const-string v5, "onPeersAvailable > there is no item to listen"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1178
    :cond_0
    :goto_0
    return-void

    .line 1149
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound$10;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mListenDevList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2600(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1150
    .local v1, "devAddr":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound$10;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mListenDevList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2600(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1151
    const-string v3, "Outbound"

    const-string v4, "requestPeers"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onPeersAvailable >"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 1154
    .local v0, "dev":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v3, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1155
    const-string v3, "Outbound"

    const-string v4, "requestPeers"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onPeersAvailable >"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    iget v3, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 1158
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound$10;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v4, 0x1

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->setConnectionStatus(ZLjava/lang/String;)V
    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1500(Lcom/samsung/android/app/FileShareClient/Outbound;ZLjava/lang/String;)V

    .line 1159
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound$10;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->refreshAndFindDev()V
    invoke-static {v3}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$900(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    .line 1160
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound$10;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->isConnectedDevice()Z
    invoke-static {v3}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1000(Lcom/samsung/android/app/FileShareClient/Outbound;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1161
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound$10;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->sendRefreshTimeout()V
    invoke-static {v3}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1100(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    goto/16 :goto_0

    .line 1167
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound$10;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->setConnectionStatus(ZLjava/lang/String;)V
    invoke-static {v3, v7, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1500(Lcom/samsung/android/app/FileShareClient/Outbound;ZLjava/lang/String;)V

    goto/16 :goto_0

    .line 1170
    :pswitch_2
    const-string v3, "Outbound"

    const-string v4, "requestPeers"

    const-string v5, "onPeersAvailable > ignore this status"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1156
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
