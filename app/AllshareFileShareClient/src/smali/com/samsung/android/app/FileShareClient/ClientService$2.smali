.class Lcom/samsung/android/app/FileShareClient/ClientService$2;
.super Ljava/lang/Object;
.source "ClientService.java"

# interfaces
.implements Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/ClientService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/ClientService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/ClientService;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/ClientService$2;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelled(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "mac"    # Ljava/lang/String;

    .prologue
    .line 391
    const-string v0, "ClientService"

    const-string v1, "onCancelled"

    invoke-virtual {p1}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$2;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$000(Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$2;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$000(Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    const-string v5, "STATUS_FAILED"

    move-object v2, p3

    move-object v3, p2

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 396
    :catch_0
    move-exception v6

    .line 397
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "ClientService"

    const-string v1, "onCancelled"

    const-string v2, "RemoteException:"

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 398
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onCompleted(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V
    .locals 7
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;
    .param p4, "err"    # Lcom/samsung/android/allshare/ERROR;
    .param p5, "mac"    # Ljava/lang/String;

    .prologue
    .line 357
    const-string v0, "ClientService"

    const-string v1, "onCompleted"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ReceiverName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " SessionID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$2;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$000(Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$2;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$000(Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "STATUS_COMPLETED"

    move-object v2, p5

    move-object v3, p2

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    :cond_0
    :goto_0
    return-void

    .line 364
    :catch_0
    move-exception v6

    .line 365
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "ClientService"

    const-string v1, "onCompleted"

    const-string v2, "RemoteException:"

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 366
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onFailed(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V
    .locals 7
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;
    .param p4, "err"    # Lcom/samsung/android/allshare/ERROR;
    .param p5, "mac"    # Ljava/lang/String;

    .prologue
    .line 341
    const-string v0, "ClientService"

    const-string v1, "onFailed"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ReceiverName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " SessionID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$2;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$000(Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$2;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$000(Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "STATUS_FAILED"

    move-object v2, p5

    move-object v3, p2

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 348
    :catch_0
    move-exception v6

    .line 349
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "ClientService"

    const-string v1, "onFailed"

    const-string v2, "RemoteException:"

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 350
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onProgressUpdated(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;JJLjava/io/File;Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V
    .locals 7
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "receivedSize"    # J
    .param p5, "totalSize"    # J
    .param p7, "file"    # Ljava/io/File;
    .param p8, "err"    # Lcom/samsung/android/allshare/ERROR;
    .param p9, "mac"    # Ljava/lang/String;

    .prologue
    .line 326
    const-string v0, "ClientService"

    const-string v1, "onProgressUpdated"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ReceiverName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " SessionID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$2;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$000(Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$2;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$000(Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "STATUS_PROGRESS"

    move-object/from16 v2, p9

    move-object v3, p2

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 332
    :catch_0
    move-exception v6

    .line 333
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "ClientService"

    const-string v1, "onProgressUpdated"

    const-string v2, "RemoteException:"

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 334
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onStart(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 9
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p4, "mac"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/file/FileReceiver;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 373
    .local p3, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    const-string v0, "ClientService"

    const-string v1, "onStart"

    invoke-virtual {p1}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/io/File;

    .line 376
    .local v7, "file":Ljava/io/File;
    const-string v0, "ClientService"

    const-string v1, "onStart:"

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$2;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$000(Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$2;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$000(Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "STATUS_STARTED"

    move-object v2, p4

    move-object v3, p2

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 382
    :catch_0
    move-exception v6

    .line 383
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "ClientService"

    const-string v1, "onStart"

    const-string v2, "RemoteException:"

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 384
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 387
    .end local v6    # "e":Landroid/os/RemoteException;
    .end local v7    # "file":Ljava/io/File;
    :cond_1
    return-void
.end method
