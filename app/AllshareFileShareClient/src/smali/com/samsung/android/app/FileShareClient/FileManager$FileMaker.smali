.class Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;
.super Ljava/lang/Object;
.source "FileManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/FileManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FileMaker"
.end annotation


# instance fields
.field private mCompleteCount:I

.field private mCopyFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mVCardUriList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/FileManager;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/FileShareClient/FileManager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 254
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mVCardUriList:Ljava/util/ArrayList;

    .line 258
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCopyFileList:Ljava/util/ArrayList;

    .line 260
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCompleteCount:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/FileShareClient/FileManager;Lcom/samsung/android/app/FileShareClient/FileManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/FileShareClient/FileManager;
    .param p2, "x1"    # Lcom/samsung/android/app/FileShareClient/FileManager$1;

    .prologue
    .line 254
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;-><init>(Lcom/samsung/android/app/FileShareClient/FileManager;)V

    return-void
.end method

.method private addCopyFileList(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 358
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCopyFileList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 359
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCopyFileList:Ljava/util/ArrayList;

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCopyFileList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    return-void
.end method

.method private addVCardUriList(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 366
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mVCardUriList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 367
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mVCardUriList:Ljava/util/ArrayList;

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mVCardUriList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    return-void
.end method

.method private getTotalCount()I
    .locals 2

    .prologue
    .line 394
    const/4 v0, 0x0

    .line 396
    .local v0, "nTotal":I
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mVCardUriList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 397
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mVCardUriList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 400
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCopyFileList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 401
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCopyFileList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 404
    :cond_1
    return v0
.end method

.method private isCompleted()Z
    .locals 3

    .prologue
    .line 408
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCompleteCount:I

    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->getTotalCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 409
    const-string v0, "FileManager"

    const-string v1, "FileMaker.isCompleted"

    const-string v2, "true"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const/4 v0, 0x1

    .line 413
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateSuccess(Ljava/io/File;)V
    .locals 3
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 374
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCompleteCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCompleteCount:I

    .line 376
    if-nez p1, :cond_0

    .line 391
    :goto_0
    return-void

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # getter for: Lcom/samsung/android/app/FileShareClient/FileManager;->mbDestroyed:Z
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$500(Lcom/samsung/android/app/FileShareClient/FileManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 381
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_1

    .line 382
    const-string v0, "FileManager"

    const-string v1, "FileMaker.updateSuccess"

    const-string v2, "delete fail "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :cond_1
    const-string v0, "FileManager"

    const-string v1, "FileMaker.updateSuccess"

    const-string v2, "updateSuccess > deleted"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 388
    :cond_2
    const-string v0, "FileManager"

    const-string v1, "FileMaker.updateSuccess"

    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # getter for: Lcom/samsung/android/app/FileShareClient/FileManager;->mFileList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$300(Lcom/samsung/android/app/FileShareClient/FileManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # getter for: Lcom/samsung/android/app/FileShareClient/FileManager;->mTempFileList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$700(Lcom/samsung/android/app/FileShareClient/FileManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public addUri(Landroid/net/Uri;Ljava/lang/String;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 263
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # invokes: Lcom/samsung/android/app/FileShareClient/FileManager;->uri2FileUri(Landroid/net/Uri;Ljava/lang/String;)I
    invoke-static {v2, p1, p2}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$100(Lcom/samsung/android/app/FileShareClient/FileManager;Landroid/net/Uri;Ljava/lang/String;)I

    move-result v1

    .line 264
    .local v1, "nResult":I
    const/4 v2, 0x1

    if-ne v2, v1, :cond_4

    .line 265
    const-string v2, "FileManager"

    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # getter for: Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$200(Lcom/samsung/android/app/FileShareClient/FileManager;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # getter for: Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$200(Lcom/samsung/android/app/FileShareClient/FileManager;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 268
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 269
    const-string v2, "FileManager"

    const-string v3, "FileMaker.addUri"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file does not exist["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const/4 v1, 0x0

    .line 287
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    return v1

    .line 272
    .restart local v0    # "file":Ljava/io/File;
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v2

    if-nez v2, :cond_2

    .line 273
    invoke-direct {p0, v0}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->addCopyFileList(Ljava/io/File;)V

    .line 274
    const/4 v1, 0x3

    .line 275
    const-string v2, "FileManager"

    const-string v3, "FileMaker.addUri"

    const-string v4, "no permission"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 276
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 277
    const-string v2, "FileManager"

    const-string v3, "FileMaker.addUri"

    const-string v4, "file size is 0"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const/4 v1, 0x0

    goto :goto_0

    .line 280
    :cond_3
    const-string v2, "FileManager"

    const-string v3, "FileMaker.addUri"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file size is"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # getter for: Lcom/samsung/android/app/FileShareClient/FileManager;->mFileList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$300(Lcom/samsung/android/app/FileShareClient/FileManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 283
    .end local v0    # "file":Ljava/io/File;
    :cond_4
    const/4 v2, 0x2

    if-ne v2, v1, :cond_0

    .line 284
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->addVCardUriList(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public needtoStartThread()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 291
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mVCardUriList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mVCardUriList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 292
    const-string v1, "FileManager"

    const-string v2, "FileMaker.needtoStartThread"

    const-string v3, "need to create"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :goto_0
    return v0

    .line 296
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCopyFileList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCopyFileList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 297
    const-string v1, "FileManager"

    const-string v2, "FileMaker.needtoStartThread"

    const-string v3, "need to copy"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 301
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 9

    .prologue
    .line 307
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mVCardUriList:Ljava/util/ArrayList;

    if-eqz v5, :cond_4

    .line 308
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mVCardUriList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 309
    .local v4, "uri":Landroid/net/Uri;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 310
    const-string v5, "FileManager"

    const-string v6, "FileMaker.run"

    const-string v7, "fail to make > interrupted"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_1
    return-void

    .line 314
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    invoke-virtual {v5, v4}, Lcom/samsung/android/app/FileShareClient/FileManager;->createVcard(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v2

    .line 315
    .local v2, "file":Ljava/io/File;
    if-nez v2, :cond_2

    .line 316
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # getter for: Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$400(Lcom/samsung/android/app/FileShareClient/FileManager;)Landroid/os/Handler;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 317
    const-string v5, "FileManager"

    const-string v6, "FileMaker.run"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fail to make > "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 352
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 353
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "FileManager"

    const-string v6, "FileMaker.run"

    const-string v7, "Exception"

    invoke-static {v5, v6, v7, v1}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 321
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "uri":Landroid/net/Uri;
    :cond_2
    :try_start_1
    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->updateSuccess(Ljava/io/File;)V

    goto :goto_0

    .line 324
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->isCompleted()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # getter for: Lcom/samsung/android/app/FileShareClient/FileManager;->mbDestroyed:Z
    invoke-static {v5}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$500(Lcom/samsung/android/app/FileShareClient/FileManager;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 325
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # getter for: Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$400(Lcom/samsung/android/app/FileShareClient/FileManager;)Landroid/os/Handler;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 330
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCopyFileList:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 331
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->mCopyFileList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 332
    .restart local v2    # "file":Ljava/io/File;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 333
    const-string v5, "FileManager"

    const-string v6, "FileMaker.run"

    const-string v7, "fail to copy > interrupted"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 337
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # invokes: Lcom/samsung/android/app/FileShareClient/FileManager;->copyFile(Ljava/io/File;)Ljava/io/File;
    invoke-static {v5, v2}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$600(Lcom/samsung/android/app/FileShareClient/FileManager;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 338
    .local v0, "copyFile":Ljava/io/File;
    if-nez v0, :cond_6

    .line 339
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # getter for: Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$400(Lcom/samsung/android/app/FileShareClient/FileManager;)Landroid/os/Handler;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 340
    const-string v5, "FileManager"

    const-string v6, "FileMaker.run"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fail to copy > "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 343
    :cond_6
    invoke-direct {p0, v0}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->updateSuccess(Ljava/io/File;)V

    goto :goto_2

    .line 346
    .end local v0    # "copyFile":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    :cond_7
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->isCompleted()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # getter for: Lcom/samsung/android/app/FileShareClient/FileManager;->mbDestroyed:Z
    invoke-static {v5}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$500(Lcom/samsung/android/app/FileShareClient/FileManager;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 347
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->this$0:Lcom/samsung/android/app/FileShareClient/FileManager;

    # getter for: Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/android/app/FileShareClient/FileManager;->access$400(Lcom/samsung/android/app/FileShareClient/FileManager;)Landroid/os/Handler;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method
