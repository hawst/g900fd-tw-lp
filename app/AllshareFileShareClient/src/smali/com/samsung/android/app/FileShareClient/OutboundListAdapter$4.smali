.class Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$4;
.super Ljava/lang/Object;
.source "OutboundListAdapter.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->showSendFileListDialog(Ljava/util/ArrayList;ZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$4;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 256
    const-string v0, "OutboundListAdapter"

    const-string v1, "onTouch"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_1

    .line 258
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 259
    const v0, 0x7f020028

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 266
    :cond_0
    :goto_0
    return v4

    .line 260
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eq v0, v4, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 262
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 263
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$4;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    # getter for: Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->access$100(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method
