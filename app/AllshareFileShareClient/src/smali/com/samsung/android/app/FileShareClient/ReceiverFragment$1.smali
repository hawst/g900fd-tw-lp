.class Lcom/samsung/android/app/FileShareClient/ReceiverFragment$1;
.super Ljava/lang/Object;
.source "ReceiverFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showFileList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$1;->this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 283
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$1;->this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    # getter for: Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->access$000(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)Lcom/samsung/android/app/FileShareClient/Receiver;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileStatus(I)I

    move-result v0

    .line 284
    .local v0, "status":I
    const/16 v1, 0x67

    if-ne v0, v1, :cond_1

    .line 285
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$1;->this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    # invokes: Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showFileDetailDialog(I)V
    invoke-static {v1, p3}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->access$100(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;I)V

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    const/16 v1, 0x69

    if-eq v0, v1, :cond_2

    const/16 v1, 0x68

    if-ne v0, v1, :cond_0

    .line 288
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$1;->this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    # invokes: Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->checkCurrentReceiverConnection(I)V
    invoke-static {v1, p3}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->access$200(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;I)V

    goto :goto_0
.end method
