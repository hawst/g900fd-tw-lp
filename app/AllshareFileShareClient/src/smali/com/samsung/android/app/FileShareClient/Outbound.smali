.class public Lcom/samsung/android/app/FileShareClient/Outbound;
.super Ljava/lang/Object;
.source "Outbound.java"

# interfaces
.implements Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
.implements Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;,
        Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;
    }
.end annotation


# static fields
.field public static final CLEAR_NOTIFICATION:Ljava/lang/String; = "com.samsung.android.app.FileShareClient.clearNotification"

.field private static final COMPLETE_SEND_FILE:I = 0x7d2

.field private static final DELAY_DISCONNECT_TIME:I = 0x9c40

.field private static final FAIL_SEND_FILE:I = 0x7d1

.field private static final FAIL_SEND_SOME_FILE:I = 0x7d0

.field private static final HANDLE_UPDATE_NOTIFICATION:I = 0xfa1

.field private static final NOTIFICATION_UPDATE_TIME:I = 0x1f4

.field private static final REFRESH:I = 0x3ea

.field private static final REMOVE_CONNECTION:I = 0x3e9

.field private static final RESEND_TIME_OUT:I = 0x3eb

.field public static final SEND_PROGRESS:Ljava/lang/String; = "com.samsung.android.app.FileShareClient.SEND_PROGRESS"

.field public static final SEND_RESULT:Ljava/lang/String; = "com.samsung.android.app.FileShareClient.SEND_RESULT"

.field private static final STAY_CONNECTION:I = 0x3e8

.field private static final TAGClass:Ljava/lang/String; = "Outbound"

.field private static final TIME_OUT:I = 0x3e9

.field private static final TIME_OUT_FIND_SERVICE:I = 0x7530

.field private static final TIME_OUT_REFRESH:I = 0x1388

.field private static mDelayDisconnectTime:Ljava/util/Timer;


# instance fields
.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mBuilder:Landroid/app/Notification$Builder;

.field private mCheckConditionHandler:Landroid/os/Handler;

.field private mCntCompleteReceiver:I

.field private mCompleteSend:Z

.field private mContext:Landroid/content/Context;

.field private mFileArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mFileMakerHandler:Landroid/os/Handler;

.field private mFileManager:Lcom/samsung/android/app/FileShareClient/FileManager;

.field private mId:I

.field private mIfg:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;

.field private mIntent:Landroid/content/Intent;

.field private mLastProgress:J

.field private mLastUpdateTime:J

.field private mListenDevList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

.field private mLocaleReceiver:Landroid/content/BroadcastReceiver;

.field private mNotificationUpdateHandler:Landroid/os/Handler;

.field private mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

.field private mReceiverList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/FileShareClient/Receiver;",
            ">;"
        }
    .end annotation
.end field

.field private mResendHandler:Landroid/os/Handler;

.field private mSconnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

.field private mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

.field private mStartTimer:Z

.field private mStatusHandler:Landroid/os/Handler;

.field private mThemeContext:Landroid/content/Context;

.field private mTimerHandler:Landroid/os/Handler;

.field private mTotalSize:J

.field private mbAddedOngoing:Z

.field private mbComplete:Z

.field private mbCompleteReceiverList:[Z

.field private mbIsWaiting:Z

.field private mbRegBroadcastReceiver:Z

.field private mbReqFinish:Z

.field private mbReqReconnect:Z

.field private mbReqSvcProvider:Z

.field private mbShowWait:Z

.field private mbTransferFinished:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1571
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/FileShareClient/Outbound;->mDelayDisconnectTime:Ljava/util/Timer;

    return-void
.end method

.method constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    .line 56
    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    .line 58
    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileManager:Lcom/samsung/android/app/FileShareClient/FileManager;

    .line 60
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    .line 66
    iput v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCntCompleteReceiver:I

    .line 68
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCompleteSend:Z

    .line 70
    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    .line 72
    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mSconnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    .line 74
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbRegBroadcastReceiver:Z

    .line 76
    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    .line 78
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqSvcProvider:Z

    .line 80
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbAddedOngoing:Z

    .line 82
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbTransferFinished:Z

    .line 84
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbIsWaiting:Z

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbShowWait:Z

    .line 90
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbComplete:Z

    .line 92
    iput-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mTotalSize:J

    .line 94
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqFinish:Z

    .line 96
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqReconnect:Z

    .line 98
    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mIntent:Landroid/content/Intent;

    .line 100
    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 102
    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 105
    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBuilder:Landroid/app/Notification$Builder;

    .line 128
    iput-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastUpdateTime:J

    .line 130
    iput-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastProgress:J

    .line 132
    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mIfg:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;

    .line 908
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$8;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLocaleReceiver:Landroid/content/BroadcastReceiver;

    .line 921
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$9;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListenDevList:Ljava/util/ArrayList;

    .line 1182
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$11;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mStatusHandler:Landroid/os/Handler;

    .line 1198
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$12;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileMakerHandler:Landroid/os/Handler;

    .line 1265
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$13;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mResendHandler:Landroid/os/Handler;

    .line 1291
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$14;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mTimerHandler:Landroid/os/Handler;

    .line 1572
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mStartTimer:Z

    .line 1601
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$16;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$16;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCheckConditionHandler:Landroid/os/Handler;

    .line 1689
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$18;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$18;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mNotificationUpdateHandler:Landroid/os/Handler;

    .line 167
    return-void
.end method

.method constructor <init>(Landroid/content/Context;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nId"    # I

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    .line 56
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileManager:Lcom/samsung/android/app/FileShareClient/FileManager;

    .line 60
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    .line 66
    iput v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCntCompleteReceiver:I

    .line 68
    iput-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCompleteSend:Z

    .line 70
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    .line 72
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mSconnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    .line 74
    iput-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbRegBroadcastReceiver:Z

    .line 76
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    .line 78
    iput-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqSvcProvider:Z

    .line 80
    iput-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbAddedOngoing:Z

    .line 82
    iput-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbTransferFinished:Z

    .line 84
    iput-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbIsWaiting:Z

    .line 88
    iput-boolean v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbShowWait:Z

    .line 90
    iput-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbComplete:Z

    .line 92
    iput-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mTotalSize:J

    .line 94
    iput-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqFinish:Z

    .line 96
    iput-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqReconnect:Z

    .line 98
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mIntent:Landroid/content/Intent;

    .line 100
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 102
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 105
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBuilder:Landroid/app/Notification$Builder;

    .line 128
    iput-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastUpdateTime:J

    .line 130
    iput-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastProgress:J

    .line 132
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mIfg:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;

    .line 908
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$8;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLocaleReceiver:Landroid/content/BroadcastReceiver;

    .line 921
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$9;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 1123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListenDevList:Ljava/util/ArrayList;

    .line 1182
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$11;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mStatusHandler:Landroid/os/Handler;

    .line 1198
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$12;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileMakerHandler:Landroid/os/Handler;

    .line 1265
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$13;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mResendHandler:Landroid/os/Handler;

    .line 1291
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$14;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mTimerHandler:Landroid/os/Handler;

    .line 1572
    iput-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mStartTimer:Z

    .line 1601
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$16;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$16;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCheckConditionHandler:Landroid/os/Handler;

    .line 1689
    new-instance v0, Lcom/samsung/android/app/FileShareClient/Outbound$18;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$18;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mNotificationUpdateHandler:Landroid/os/Handler;

    .line 170
    const-string v0, "Outbound"

    const-string v1, "Outbound"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    .line 172
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Utils;->setContext(Landroid/content/Context;)V

    .line 174
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->setThemeContext()V

    .line 175
    iput p2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    .line 177
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    const-string v0, "Outbound"

    const-string v1, "Outbound"

    const-string v2, "isP2pConnection! flag set to true!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->requestServiceProvider()V

    .line 184
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    .line 185
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->regBroadcastReceiver()V

    .line 186
    invoke-direct {p0, v6, v3}, Lcom/samsung/android/app/FileShareClient/Outbound;->addOngoing(ZZ)V

    .line 187
    return-void

    .line 181
    :cond_0
    const-string v0, "Outbound"

    const-string v1, "Outbound"

    const-string v2, "isP2pConnection! flag set to false!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private CheckClientCodition()V
    .locals 3

    .prologue
    .line 1613
    sget v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    if-nez v0, :cond_0

    .line 1614
    const-string v0, "Outbound"

    const-string v1, "CheckClientCodition"

    const-string v2, "Client is not Running. Remove connection."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1615
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->CheckServerCondition()V

    .line 1620
    :goto_0
    return-void

    .line 1617
    :cond_0
    const-string v0, "Outbound"

    const-string v1, "CheckClientCodition"

    const-string v2, "Client is Running. Do not remove connection."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1618
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCheckConditionHandler:Landroid/os/Handler;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private CheckQuickConnectCondition()V
    .locals 3

    .prologue
    .line 1633
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Utils;->getIsQuickConnectRunning(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1634
    const-string v0, "Outbound"

    const-string v1, "CheckQuickConnectCondition"

    const-string v2, "QC is not Running. Remove connection."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1635
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mStartTimer:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1636
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->removeConnection()V

    .line 1647
    :goto_0
    return-void

    .line 1638
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->startDelayTimer()V

    goto :goto_0

    .line 1641
    :cond_1
    const-string v0, "Outbound"

    const-string v1, "CheckQuickConnectCondition"

    const-string v2, "QC is Running. Do not remove connection."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1643
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCheckConditionHandler:Landroid/os/Handler;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1644
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->startDelayTimer()V

    goto :goto_0
.end method

.method private CheckServerCondition()V
    .locals 3

    .prologue
    .line 1623
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Utils;->getIsServerRunning(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1624
    const-string v0, "Outbound"

    const-string v1, "CheckServerCondition"

    const-string v2, "Server is not Running. Remove connection."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1625
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->CheckQuickConnectCondition()V

    .line 1630
    :goto_0
    return-void

    .line 1627
    :cond_0
    const-string v0, "Outbound"

    const-string v1, "CheckServerCondition"

    const-string v2, "Server is Running. Do not remove connection."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1628
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCheckConditionHandler:Landroid/os/Handler;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private CurrentStatusToast()V
    .locals 5

    .prologue
    .line 621
    const/4 v0, 0x0

    .line 623
    .local v0, "currentCount":I
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 624
    .local v2, "r":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getCurrentCount()I

    move-result v3

    add-int/2addr v0, v3

    .line 625
    goto :goto_0

    .line 627
    .end local v2    # "r":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_0
    if-nez v0, :cond_1

    .line 628
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mStatusHandler:Landroid/os/Handler;

    const/16 v4, 0x7d1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 635
    :goto_1
    return-void

    .line 629
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    mul-int/2addr v3, v4

    if-ne v0, v3, :cond_2

    .line 631
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mStatusHandler:Landroid/os/Handler;

    const/16 v4, 0x7d2

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 633
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mStatusHandler:Landroid/os/Handler;

    const/16 v4, 0x7d0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method

.method private ResendFindTimeout(I)V
    .locals 5
    .param p1, "nId"    # I

    .prologue
    .line 1389
    const-string v1, "Outbound"

    const-string v2, "ResendFindTimeout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ResendFindTimeout : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1391
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1392
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x3eb

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1393
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1394
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mResendHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7530

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1395
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileMakerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/FileShareClient/Outbound;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->isConnectedDevice()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->sendRefreshTimeout()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->updateNotification()V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/app/FileShareClient/Outbound;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCompleteSend:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/app/FileShareClient/Outbound;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/FileShareClient/Outbound;->setConnectionStatus(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->requestServiceProvider()V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/samsung/android/app/FileShareClient/Outbound;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/samsung/android/app/FileShareClient/Outbound;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbShowWait:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/samsung/android/app/FileShareClient/Outbound;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbShowWait:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/FileManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileManager:Lcom/samsung/android/app/FileShareClient/FileManager;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->unregBroadcastReceiver()V

    return-void
.end method

.method static synthetic access$2100(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mSconnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->destroyServiceProvider()V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mIfg:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/app/FileShareClient/Outbound;Lcom/samsung/android/app/FileShareClient/Receiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p1, "x1"    # Lcom/samsung/android/app/FileShareClient/Receiver;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->requestPeer(Lcom/samsung/android/app/FileShareClient/Receiver;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/samsung/android/app/FileShareClient/Outbound;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->changedPeer(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2600(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListenDevList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2814(Lcom/samsung/android/app/FileShareClient/Outbound;J)J
    .locals 3
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p1, "x1"    # J

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mTotalSize:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mTotalSize:J

    return-wide v0
.end method

.method static synthetic access$2900(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/FileShareClient/Outbound;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqReconnect:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/app/FileShareClient/Outbound;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/FileShareClient/Outbound;->addOngoing(ZZ)V

    return-void
.end method

.method static synthetic access$302(Lcom/samsung/android/app/FileShareClient/Outbound;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqReconnect:Z

    return p1
.end method

.method static synthetic access$3102(Lcom/samsung/android/app/FileShareClient/Outbound;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mStartTimer:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->CheckClientCodition()V

    return-void
.end method

.method static synthetic access$3300(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->startDelayTimer()V

    return-void
.end method

.method static synthetic access$3400(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->updateNotificationInfo()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/FileShareClient/Outbound;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p1, "x1"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->getPreferredConfig(Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/net/wifi/p2p/WifiP2pManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/app/FileShareClient/Outbound;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqSvcProvider:Z

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/allshare/file/FileServiceProvider;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    return-object v0
.end method

.method static synthetic access$802(Lcom/samsung/android/app/FileShareClient/Outbound;Lcom/samsung/android/allshare/file/FileServiceProvider;)Lcom/samsung/android/allshare/file/FileServiceProvider;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;
    .param p1, "x1"    # Lcom/samsung/android/allshare/file/FileServiceProvider;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    return-object p1
.end method

.method static synthetic access$900(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->refreshAndFindDev()V

    return-void
.end method

.method private addNotification(Z)V
    .locals 15
    .param p1, "bUpdate"    # Z

    .prologue
    .line 798
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->getCurrentStateString()Ljava/lang/String;

    move-result-object v4

    .line 799
    .local v4, "contentTitle":Ljava/lang/String;
    const-string v3, ""

    .line 800
    .local v3, "contentText":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    if-eqz v9, :cond_2

    .line 801
    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 802
    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_0

    .line 803
    iget-object v10, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v11, 0x7f04000a

    const/4 v9, 0x2

    new-array v12, v9, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v12, v13

    const/4 v9, 0x1

    iget-object v13, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v9

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 809
    :cond_0
    :goto_0
    if-nez p1, :cond_1

    .line 810
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->CurrentStatusToast()V

    .line 813
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-string v10, "notification"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/NotificationManager;

    .line 815
    .local v6, "notiMgr":Landroid/app/NotificationManager;
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-direct {v0, v9}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 817
    .local v0, "builder":Landroid/app/Notification$Builder;
    new-instance v8, Landroid/content/Intent;

    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-class v10, Lcom/samsung/android/app/FileShareClient/OutboundActivity;

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 818
    .local v8, "notificationIntent":Landroid/content/Intent;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "com.samsung.android.app.FileShareClient.SEND_RESULT"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 819
    const-string v9, "outboundId"

    iget v10, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 820
    const/high16 v9, 0x10800000

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 822
    const-string v9, "Outbound"

    const-string v10, "addNotification"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "id = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const/4 v10, 0x0

    const/high16 v11, 0x8000000

    invoke-static {v9, v10, v8, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 826
    .local v2, "contentIntent":Landroid/app/PendingIntent;
    new-instance v1, Landroid/content/Intent;

    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-class v10, Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-direct {v1, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 827
    .local v1, "clearNotiIntent":Landroid/content/Intent;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "com.samsung.android.app.FileShareClient.clearNotification"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 828
    const-string v9, "outboundId"

    iget v10, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 829
    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v9, v10, v1, v11}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 831
    .local v5, "deleteIntent":Landroid/app/PendingIntent;
    const v9, 0x1080089

    invoke-virtual {v0, v9}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 832
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 833
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 834
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 835
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 836
    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 837
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 838
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    .line 839
    .local v7, "notification":Landroid/app/Notification;
    const-string v9, "FileShareClient"

    iget v10, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    mul-int/lit8 v10, v10, 0x2

    invoke-virtual {v6, v9, v10}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 840
    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mNotificationUpdateHandler:Landroid/os/Handler;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 841
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBuilder:Landroid/app/Notification$Builder;

    .line 843
    const-string v9, "FileShareClient"

    iget v10, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    mul-int/lit8 v10, v10, 0x2

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v6, v9, v10, v7}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 844
    return-void

    .line 806
    .end local v0    # "builder":Landroid/app/Notification$Builder;
    .end local v1    # "clearNotiIntent":Landroid/content/Intent;
    .end local v2    # "contentIntent":Landroid/app/PendingIntent;
    .end local v5    # "deleteIntent":Landroid/app/PendingIntent;
    .end local v6    # "notiMgr":Landroid/app/NotificationManager;
    .end local v7    # "notification":Landroid/app/Notification;
    .end local v8    # "notificationIntent":Landroid/content/Intent;
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v10, 0x7f040004

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0
.end method

.method private addOngoing(ZZ)V
    .locals 11
    .param p1, "bWait"    # Z
    .param p2, "bUpdate"    # Z

    .prologue
    .line 707
    iput-boolean p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbIsWaiting:Z

    .line 708
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-string v6, "notification"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 710
    .local v3, "notiMgr":Landroid/app/NotificationManager;
    new-instance v4, Landroid/content/Intent;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-class v6, Lcom/samsung/android/app/FileShareClient/OutboundActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 711
    .local v4, "notificationIntent":Landroid/content/Intent;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "com.samsung.android.app.FileShareClient.SEND_PROGRESS"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 712
    const-string v5, "outboundId"

    iget v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 713
    const/high16 v5, 0x10800000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 715
    const-string v5, "Outbound"

    const-string v6, "addOngoing"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "addOngoing id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    const/high16 v7, 0x8000000

    invoke-static {v5, v6, v4, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 719
    .local v1, "contentIntent":Landroid/app/PendingIntent;
    if-eqz p1, :cond_5

    .line 720
    const-string v2, ""

    .line 721
    .local v2, "contentText":Ljava/lang/String;
    iget-boolean v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbShowWait:Z

    if-eqz v5, :cond_1

    .line 722
    if-nez p2, :cond_0

    .line 723
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v7, 0x7f040013

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 727
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v6, 0x7f040003

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 750
    :goto_0
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 751
    .local v0, "builder":Landroid/app/Notification$Builder;
    const v5, 0x1080089

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 752
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v6, 0x7f040012

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 754
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 755
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 756
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 757
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    .line 758
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 760
    const-string v5, "FileShareClient"

    iget v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    mul-int/lit8 v6, v6, 0x2

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v3, v5, v6, v7}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 795
    .end local v0    # "builder":Landroid/app/Notification$Builder;
    .end local v2    # "contentText":Ljava/lang/String;
    :goto_1
    return-void

    .line 729
    .restart local v2    # "contentText":Ljava/lang/String;
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    if-nez v5, :cond_2

    .line 730
    const-string v5, "Outbound"

    const-string v6, "addOngoing"

    const-string v7, "mFileArrayList == null"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 732
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_3

    .line 733
    const-string v5, "Outbound"

    const-string v6, "addOngoing"

    const-string v7, "mFileArrayList.size() == 0"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 735
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_4

    .line 736
    const-string v6, "Outbound"

    const-string v7, "addOngoing"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Name:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " Size:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v7, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v7, 0x7f04000a

    const/4 v5, 0x2

    new-array v8, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v9

    const/4 v5, 0x1

    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 742
    :cond_4
    const-string v6, "Outbound"

    const-string v7, "addOngoing"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Name:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v7, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 762
    .end local v2    # "contentText":Ljava/lang/String;
    :cond_5
    if-nez p2, :cond_6

    .line 763
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v7, 0x7f040036

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 767
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBuilder:Landroid/app/Notification$Builder;

    if-nez v5, :cond_8

    .line 768
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 769
    .restart local v0    # "builder":Landroid/app/Notification$Builder;
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x1060058

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    .line 772
    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_7

    .line 773
    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v7, 0x7f04000a

    const/4 v5, 0x2

    new-array v8, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v9

    const/4 v5, 0x1

    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 780
    :goto_2
    const-string v6, "%d%%"

    const/4 v5, 0x1

    new-array v7, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->getProgress()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 782
    const/16 v5, 0x64

    const/16 v6, 0x64

    const/4 v7, 0x1

    invoke-virtual {v0, v5, v6, v7}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 783
    const v5, 0x1080088

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 784
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 785
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    .line 786
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 787
    const-string v5, "FileShareClient"

    iget v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    mul-int/lit8 v6, v6, 0x2

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v3, v5, v6, v7}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 789
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBuilder:Landroid/app/Notification$Builder;

    goto/16 :goto_1

    .line 776
    :cond_7
    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v7, 0x7f040010

    const/4 v5, 0x1

    new-array v8, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_2

    .line 791
    .end local v0    # "builder":Landroid/app/Notification$Builder;
    :cond_8
    const-string v5, "Outbound"

    const-string v6, "addOngoing"

    const-string v7, "Do not update Notification"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private changedPeer(Ljava/lang/String;)V
    .locals 5
    .param p1, "devAddr"    # Ljava/lang/String;

    .prologue
    .line 1110
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 1111
    const-string v2, "Outbound"

    const-string v3, "changedPeer"

    const-string v4, "mReceiverList is null."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1121
    :goto_0
    return-void

    .line 1114
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 1115
    .local v1, "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v1, p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->isMyMacAddress(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1116
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->requestPeers(Ljava/lang/String;)V

    goto :goto_0

    .line 1120
    .end local v1    # "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_2
    const-string v2, "Outbound"

    const-string v3, "changedPeer"

    const-string v4, "there is no item to listen"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private destroyServiceProvider()V
    .locals 3

    .prologue
    .line 695
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 696
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    .line 698
    const-string v0, "Outbound"

    const-string v1, "destroyServiceProvider"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    return-void
.end method

.method private findReceiverWithUPnpDev(Lcom/samsung/android/allshare/Device;)Lcom/samsung/android/app/FileShareClient/Receiver;
    .locals 8
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    const/4 v3, 0x0

    .line 1085
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getIPAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/allshare/extension/DeviceChecker;->getMacAddrFromArpTable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1087
    .local v1, "macAddr":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 1088
    const-string v4, "Outbound"

    const-string v5, "findReceiverWithUPnpDev"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[NOT FOUND TABLE]["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getIPAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 1106
    :goto_0
    return-object v2

    .line 1092
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    if-nez v4, :cond_1

    .line 1093
    const-string v4, "Outbound"

    const-string v5, "findReceiverWithUPnpDev"

    const-string v6, "mReceiverList is null."

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 1094
    goto :goto_0

    .line 1096
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 1097
    .local v2, "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    const-string v4, "Outbound"

    const-string v5, "findReceiverWithUPnpDev"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "recv.getStatus() - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    const/16 v4, 0x3ea

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v5

    if-ne v4, v5, :cond_2

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->isMyMacAddress(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1099
    const-string v3, "Outbound"

    const-string v4, "findReceiverWithUPnpDev"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[FOUND]["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1104
    .end local v2    # "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_3
    const-string v4, "Outbound"

    const-string v5, "findReceiverWithUPnpDev"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[NOT HAVE] ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getIPAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 1106
    goto/16 :goto_0
.end method

.method private getPreferredConfig(Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;
    .locals 5
    .param p1, "device"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    const/4 v4, 0x0

    .line 451
    const-string v1, "Outbound"

    const-string v2, "getPreferredConfig"

    const-string v3, " "

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pConfig;

    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pConfig;-><init>()V

    .line 454
    .local v0, "config":Landroid/net/wifi/p2p/WifiP2pConfig;
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iput-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    .line 456
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->wpsPbcSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 457
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iput v4, v1, Landroid/net/wifi/WpsInfo;->setup:I

    .line 465
    :goto_0
    return-object v0

    .line 458
    :cond_0
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->wpsKeypadSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 459
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    const/4 v2, 0x2

    iput v2, v1, Landroid/net/wifi/WpsInfo;->setup:I

    goto :goto_0

    .line 460
    :cond_1
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->wpsDisplaySupported()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 461
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    const/4 v2, 0x1

    iput v2, v1, Landroid/net/wifi/WpsInfo;->setup:I

    goto :goto_0

    .line 463
    :cond_2
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iput v4, v1, Landroid/net/wifi/WpsInfo;->setup:I

    goto :goto_0
.end method

.method private getTransferProgress()I
    .locals 7

    .prologue
    .line 1479
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1480
    const/4 v2, 0x0

    .line 1481
    .local v2, "totalProgress":I
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 1482
    .local v1, "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getProgress()I

    move-result v3

    add-int/2addr v2, v3

    .line 1483
    goto :goto_0

    .line 1484
    .end local v1    # "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_0
    const-string v3, "Outbound"

    const-string v4, "getTransferProgress"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "progress: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    div-int v6, v2, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1486
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    div-int v3, v2, v3

    .line 1488
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "totalProgress":I
    :goto_1
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private isConnectedDevice()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1407
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 1408
    const-string v3, "Outbound"

    const-string v4, "isConnectedDevice"

    const-string v5, "mReceiverList is null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    :cond_0
    :goto_0
    return v2

    .line 1412
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 1413
    .local v1, "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    const/16 v3, 0x3ea

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 1414
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isP2pEnabled()Z
    .locals 6

    .prologue
    .line 397
    const-string v3, "Outbound"

    const-string v4, "isP2pEnabled"

    const-string v5, ""

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 402
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/16 v3, 0xd

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 405
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 409
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "netInfo":Landroid/net/NetworkInfo;
    :goto_0
    return v3

    .line 406
    :catch_0
    move-exception v1

    .line 407
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v3, "Outbound"

    const-string v4, "isP2pEnabled"

    const-string v5, "NullPointerException"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private refreshAndFindDev()V
    .locals 8

    .prologue
    .line 1331
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    if-nez v4, :cond_0

    .line 1332
    const-string v4, "Outbound"

    const-string v5, "refreshAndFindDev"

    const-string v6, "mServiceProvider is null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1378
    :goto_0
    return-void

    .line 1336
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/file/FileServiceProvider;->getDeviceFinder()Lcom/samsung/android/allshare/file/FileDeviceFinder;

    move-result-object v0

    .line 1337
    .local v0, "deviceFinder":Lcom/samsung/android/allshare/DeviceFinder;
    if-nez v0, :cond_1

    .line 1338
    const-string v4, "Outbound"

    const-string v5, "refreshAndFindDev"

    const-string v6, "deviceFinder is null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1342
    :cond_1
    const-string v4, "Outbound"

    const-string v5, "refreshAndFindDev"

    const-string v6, ""

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1343
    sget-object v4, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v4, p0}, Lcom/samsung/android/allshare/DeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 1344
    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceFinder;->refresh()V

    .line 1346
    sget-object v4, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/DeviceFinder;->getDevices(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1347
    .local v1, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    if-nez v1, :cond_2

    .line 1348
    const-string v4, "Outbound"

    const-string v5, "refreshAndFindDev"

    const-string v6, "Searched device size [0]"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1352
    :cond_2
    const-string v4, "Outbound"

    const-string v5, "refreshAndFindDev"

    const-string v6, "+---------------------------------------------------------------------+"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1354
    const-string v4, "Outbound"

    const-string v5, "refreshAndFindDev"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Searched device size ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1355
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_4

    .line 1368
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/Device;

    invoke-direct {p0, v4}, Lcom/samsung/android/app/FileShareClient/Outbound;->findReceiverWithUPnpDev(Lcom/samsung/android/allshare/Device;)Lcom/samsung/android/app/FileShareClient/Receiver;

    move-result-object v3

    .line 1369
    .local v3, "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    if-eqz v3, :cond_3

    .line 1370
    const/16 v4, 0x3eb

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 1371
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/file/FileReceiver;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setUPnPDevice(Lcom/samsung/android/allshare/file/FileReceiver;)V

    .line 1372
    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->startReceiveByCondition()V

    .line 1355
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1376
    .end local v3    # "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_4
    const-string v4, "Outbound"

    const-string v5, "refreshAndFindDev"

    const-string v6, "+---------------------------------------------------------------------+"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private regBroadcastReceiver()V
    .locals 6

    .prologue
    .line 865
    const-string v2, "Outbound"

    const-string v3, "regBroadcastReceiver"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    iget-boolean v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbRegBroadcastReceiver:Z

    if-eqz v2, :cond_0

    .line 893
    :goto_0
    return-void

    .line 873
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 875
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 879
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 880
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 881
    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 882
    const-string v2, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 883
    const-string v2, "file"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 884
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 886
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLocaleReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 889
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbRegBroadcastReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 890
    .end local v1    # "intentFilter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v0

    .line 891
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Outbound"

    const-string v3, "regBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private removeConnection()V
    .locals 4

    .prologue
    .line 1650
    const-string v0, "Outbound"

    const-string v1, "removeConnection"

    const-string v2, " "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1652
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1653
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    const-string v1, "wifip2p"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 1654
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v0, :cond_0

    .line 1655
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 1659
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/samsung/android/app/FileShareClient/Outbound$17;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$17;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 1675
    :goto_0
    return-void

    .line 1673
    :cond_1
    const-string v0, "Outbound"

    const-string v1, "removeConnection"

    const-string v2, "P2P already disconnected"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private requestPeer(Lcom/samsung/android/app/FileShareClient/Receiver;)V
    .locals 4
    .param p1, "nReceiver"    # Lcom/samsung/android/app/FileShareClient/Receiver;

    .prologue
    .line 413
    move-object v0, p1

    .line 414
    .local v0, "r":Lcom/samsung/android/app/FileShareClient/Receiver;
    const-string v1, "Outbound"

    const-string v2, "requestPeer"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v3, Lcom/samsung/android/app/FileShareClient/Outbound$3;

    invoke-direct {v3, p0, v0}, Lcom/samsung/android/app/FileShareClient/Outbound$3;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;Lcom/samsung/android/app/FileShareClient/Receiver;)V

    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    .line 448
    return-void
.end method

.method private requestPeers(Ljava/lang/String;)V
    .locals 4
    .param p1, "devAddr"    # Ljava/lang/String;

    .prologue
    .line 1127
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v0, :cond_0

    .line 1128
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-string v1, "wifip2p"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 1131
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v0, :cond_1

    .line 1132
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 1135
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListenDevList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1136
    const-string v0, "Outbound"

    const-string v1, "requestPeers"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1138
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/samsung/android/app/FileShareClient/Outbound$10;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$10;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    .line 1180
    return-void
.end method

.method private requestServiceProvider()V
    .locals 4

    .prologue
    .line 660
    const-string v0, "Outbound"

    const-string v1, "requestServiceProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqSvcProvider:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " + "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    if-eqz v0, :cond_1

    .line 692
    :cond_0
    :goto_0
    return-void

    .line 664
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqSvcProvider:Z

    if-nez v0, :cond_0

    .line 667
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqSvcProvider:Z

    .line 669
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/samsung/android/app/FileShareClient/Outbound$7;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$7;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    const-string v2, "com.samsung.android.allshare.file"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/ServiceConnector;->createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    goto :goto_0
.end method

.method private sendFindTimeout(I)V
    .locals 5
    .param p1, "nId"    # I

    .prologue
    .line 1398
    const-string v1, "Outbound"

    const-string v2, "sendFindTimeout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendFindTimeout : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1400
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1401
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x3e9

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1402
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1403
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7530

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1404
    return-void
.end method

.method private sendRefreshTimeout()V
    .locals 4

    .prologue
    .line 1381
    const-string v1, "Outbound"

    const-string v2, "sendRefreshTimeout"

    const-string v3, " "

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1383
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1384
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x3ea

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1385
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1386
    return-void
.end method

.method private setConnectionStatus(ZLjava/lang/String;)V
    .locals 5
    .param p1, "isConnected"    # Z
    .param p2, "macAddr"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x3ef

    .line 1057
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 1058
    const-string v2, "Outbound"

    const-string v3, "setConnectionStatus"

    const-string v4, "mReceiverList is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1082
    :cond_0
    :goto_0
    return-void

    .line 1061
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 1062
    .local v1, "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v1, p2}, Lcom/samsung/android/app/FileShareClient/Receiver;->isMyMacAddress(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1063
    if-eqz p1, :cond_3

    .line 1064
    const-string v2, "Outbound"

    const-string v3, "setConnectionStatus"

    const-string v4, "CONNECTED"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    const/16 v2, 0x3ea

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 1066
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->setConnectStatus(I)V

    .line 1067
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getId()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->sendFindTimeout(I)V

    .line 1068
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    goto :goto_0

    .line 1070
    :cond_3
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v2

    const/16 v3, 0x3ed

    if-eq v2, v3, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v2

    if-eq v2, v4, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 1073
    invoke-virtual {v1, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 1074
    const-string v2, "Outbound"

    const-string v3, "setConnectionStatus"

    const-string v4, "CONNECTING"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->setConnectStatus(I)V

    .line 1076
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    goto :goto_0
.end method

.method private setThemeContext()V
    .locals 3

    .prologue
    .line 190
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v2, 0x103012b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    .line 192
    return-void
.end method

.method private startDelayTimer()V
    .locals 4

    .prologue
    .line 1579
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/Outbound;->stopDelayTimer()V

    .line 1580
    const-string v0, "Outbound"

    const-string v1, "startDelayTimer"

    const-string v2, "40 seconds"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1582
    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Z)V

    sput-object v0, Lcom/samsung/android/app/FileShareClient/Outbound;->mDelayDisconnectTime:Ljava/util/Timer;

    .line 1583
    sget-object v0, Lcom/samsung/android/app/FileShareClient/Outbound;->mDelayDisconnectTime:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/app/FileShareClient/Outbound$15;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$15;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    const-wide/32 v2, 0x9c40

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1591
    return-void
.end method

.method public static stopDelayTimer()V
    .locals 3

    .prologue
    .line 1594
    const-string v0, "Outbound"

    const-string v1, "stopDelayTimer"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1595
    sget-object v0, Lcom/samsung/android/app/FileShareClient/Outbound;->mDelayDisconnectTime:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1596
    sget-object v0, Lcom/samsung/android/app/FileShareClient/Outbound;->mDelayDisconnectTime:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1597
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/FileShareClient/Outbound;->mDelayDisconnectTime:Ljava/util/Timer;

    .line 1599
    :cond_0
    return-void
.end method

.method private unregBroadcastReceiver()V
    .locals 5

    .prologue
    .line 897
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbRegBroadcastReceiver:Z

    if-nez v1, :cond_0

    .line 906
    :goto_0
    return-void

    .line 901
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 902
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbRegBroadcastReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 903
    :catch_0
    move-exception v0

    .line 904
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Outbound"

    const-string v2, "unregBroadcastReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateNotification()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 847
    const-string v1, "Outbound"

    const-string v2, "updateNotification"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbAddedOngoing:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbTransferFinished:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 850
    .local v0, "notiMgr":Landroid/app/NotificationManager;
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbAddedOngoing:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbAddedOngoing:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbTransferFinished:Z

    if-nez v1, :cond_2

    .line 851
    :cond_0
    const-string v1, "Outbound"

    const-string v2, "updateNotification"

    const-string v3, "mbAddedOngoing is true"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    const-string v1, "FileShareClient"

    iget v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 853
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbIsWaiting:Z

    invoke-direct {p0, v1, v5}, Lcom/samsung/android/app/FileShareClient/Outbound;->addOngoing(ZZ)V

    .line 859
    :cond_1
    :goto_0
    return-void

    .line 854
    :cond_2
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbTransferFinished:Z

    if-eqz v1, :cond_1

    .line 855
    const-string v1, "Outbound"

    const-string v2, "updateNotification"

    const-string v3, "mbTransferFinished is true"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    const-string v1, "FileShareClient"

    iget v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 857
    invoke-direct {p0, v5}, Lcom/samsung/android/app/FileShareClient/Outbound;->addNotification(Z)V

    goto :goto_0
.end method

.method private updateNotificationInfo()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1678
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBuilder:Landroid/app/Notification$Builder;

    if-eqz v1, :cond_0

    .line 1679
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBuilder:Landroid/app/Notification$Builder;

    const-string v2, "%d%%"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastProgress:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 1680
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v1, v6}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    .line 1681
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1683
    .local v0, "notiMgr":Landroid/app/NotificationManager;
    const-string v1, "FileShareClient"

    iget v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    mul-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mBuilder:Landroid/app/Notification$Builder;

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1685
    const-string v1, "Outbound"

    const-string v2, "updateStatus"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "update - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastProgress:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1687
    .end local v0    # "notiMgr":Landroid/app/NotificationManager;
    :cond_0
    return-void
.end method


# virtual methods
.method public Sconnect_Cancel(Lcom/samsung/android/app/FileShareClient/Receiver;)V
    .locals 4
    .param p1, "recv"    # Lcom/samsung/android/app/FileShareClient/Receiver;

    .prologue
    const/16 v2, 0x3e9

    .line 516
    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v0

    if-lt v0, v2, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v0

    const/16 v1, 0x3ec

    if-gt v0, v1, :cond_3

    .line 518
    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 519
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-string v1, "wifip2p"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 523
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v0, :cond_1

    .line 524
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 527
    :cond_1
    const-string v0, "Outbound"

    const-string v1, "Sconnect_Cancel"

    const-string v2, "receiver just one. : cancelConnet()"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/samsung/android/app/FileShareClient/Outbound$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$4;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->cancelConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 539
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->cancel()V

    .line 541
    :cond_3
    return-void
.end method

.method public cancelAll()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v7, 0x3ec

    const/16 v6, 0x3e9

    .line 544
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->unregBroadcastReceiver()V

    .line 546
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 547
    const-string v3, "Outbound"

    const-string v4, "cancelAll"

    const-string v5, "mReceiverList is null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    :cond_0
    :goto_0
    return-void

    .line 551
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    .line 552
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 553
    .local v2, "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v3

    if-lt v3, v6, :cond_0

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v3

    if-gt v3, v7, :cond_0

    .line 555
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v3

    if-ne v3, v6, :cond_4

    .line 556
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v3, :cond_2

    .line 557
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-string v4, "wifip2p"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 560
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v3, :cond_3

    .line 561
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v8}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 565
    :cond_3
    const-string v3, "Outbound"

    const-string v4, "cancelAll"

    const-string v5, "receiver just one. : cancelConnet()"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v5, Lcom/samsung/android/app/FileShareClient/Outbound$5;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$5;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    invoke-virtual {v3, v4, v5}, Landroid/net/wifi/p2p/WifiP2pManager;->cancelConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 576
    :cond_4
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->cancel()V

    goto :goto_0

    .line 579
    .end local v2    # "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_5
    const/4 v0, 0x0

    .line 580
    .local v0, "bCheckConnected":Z
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 581
    .restart local v2    # "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v3

    if-lt v3, v6, :cond_6

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v3

    if-gt v3, v7, :cond_6

    .line 583
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/app/FileShareClient/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 584
    const-string v3, "Outbound"

    const-string v4, "cancelAll"

    const-string v5, "p2p connected"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    const/4 v0, 0x1

    .line 587
    :cond_7
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->cancel()V

    goto :goto_1

    .line 591
    .end local v2    # "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_8
    if-nez v0, :cond_0

    .line 592
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v3, :cond_9

    .line 593
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-string v4, "wifip2p"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 596
    :cond_9
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v3, :cond_a

    .line 597
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v8}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 600
    :cond_a
    const-string v3, "Outbound"

    const-string v4, "cancelAll"

    const-string v5, "receiver are multi. There are no Connected : removeGroup()"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v5, Lcom/samsung/android/app/FileShareClient/Outbound$6;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$6;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    invoke-virtual {v3, v4, v5}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto/16 :goto_0
.end method

.method public cancel_connect()V
    .locals 4

    .prologue
    .line 345
    const-string v0, "Outbound"

    const-string v1, "cancel_connect"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-string v1, "wifip2p"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v0, :cond_1

    .line 351
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 354
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/samsung/android/app/FileShareClient/Outbound$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$2;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 366
    return-void
.end method

.method public destroyOutbound()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 473
    const-string v2, "Outbound"

    const-string v3, "destroyOutbound"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ID["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 476
    .local v1, "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v1, v6}, Lcom/samsung/android/app/FileShareClient/Receiver;->setListener(Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;)V

    .line 477
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 478
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setErrorCause(I)V

    goto :goto_0

    .line 481
    .end local v1    # "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    if-eqz v2, :cond_3

    .line 482
    const-string v2, "Outbound"

    const-string v3, "destroyOutbound"

    const-string v4, "call OutboundFinish()"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    invoke-interface {v2}, Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;->OutboundFinish()V

    .line 489
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileManager:Lcom/samsung/android/app/FileShareClient/FileManager;

    if-eqz v2, :cond_1

    .line 490
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileManager:Lcom/samsung/android/app/FileShareClient/FileManager;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/FileManager;->deleteTempFiles()V

    .line 493
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 494
    iput-object v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    .line 495
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    if-eqz v2, :cond_2

    .line 496
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->destroyServiceProvider()V

    .line 498
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->unregBroadcastReceiver()V

    .line 499
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLocaleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 500
    return-void

    .line 485
    :cond_3
    const-string v2, "Outbound"

    const-string v3, "destroyOutbound"

    const-string v4, "mListener is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public enableP2p()V
    .locals 4

    .prologue
    .line 369
    const-string v0, "Outbound"

    const-string v1, "enableP2p"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqReconnect:Z

    .line 372
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const-string v1, "wifip2p"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 380
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->isP2pEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 381
    const-string v0, "Outbound"

    const-string v1, "enableP2p"

    const-string v2, "isP2pEnabled"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :cond_2
    return-void
.end method

.method public getCurrentStateString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 638
    const-string v1, ""

    .line 639
    .local v1, "currentState":Ljava/lang/String;
    const/4 v0, 0x0

    .line 641
    .local v0, "currentCount":I
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 642
    .local v3, "r":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getCurrentCount()I

    move-result v4

    add-int/2addr v0, v4

    .line 643
    goto :goto_0

    .line 645
    .end local v3    # "r":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_0
    if-nez v0, :cond_1

    .line 646
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v5, 0x7f04002b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 652
    :goto_1
    return-object v1

    .line 647
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    mul-int/2addr v4, v5

    if-ne v0, v4, :cond_2

    .line 648
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v5, 0x7f04002e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 650
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    const v5, 0x7f04002d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public getFileArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    return v0
.end method

.method public getReceiverList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/FileShareClient/Receiver;",
            ">;"
        }
    .end annotation

    .prologue
    .line 469
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getReqFinish()Z
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqFinish:Z

    return v0
.end method

.method public getTotalSize()J
    .locals 2

    .prologue
    .line 210
    iget-wide v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mTotalSize:J

    return-wide v0
.end method

.method public isComplete()Z
    .locals 1

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbComplete:Z

    return v0
.end method

.method public isId(I)Z
    .locals 1
    .param p1, "nId"    # I

    .prologue
    .line 199
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mId:I

    if-ne v0, p1, :cond_0

    .line 200
    const/4 v0, 0x1

    .line 202
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDeviceAdded(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 5
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 1426
    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    if-eq v1, p1, :cond_0

    .line 1427
    const-string v1, "Outbound"

    const-string v2, "onDeviceAdded"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid devicetype >"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1455
    .end local p2    # "device":Lcom/samsung/android/allshare/Device;
    :goto_0
    return-void

    .line 1431
    .restart local p2    # "device":Lcom/samsung/android/allshare/Device;
    :cond_0
    const-string v1, "Outbound"

    const-string v2, "onDeviceAdded"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ERROR: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1433
    sget-object v1, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq p3, v1, :cond_1

    .line 1434
    const-string v1, "Outbound"

    const-string v2, "onDeviceAdded"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getDeviceDomain()Lcom/samsung/android/allshare/Device$DeviceDomain;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceDomain;->MY_DEVICE:Lcom/samsung/android/allshare/Device$DeviceDomain;

    if-ne v1, v2, :cond_2

    .line 1437
    const-string v1, "Outbound"

    const-string v2, "onDeviceAdded"

    const-string v3, "My Device. Ignore."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1441
    :cond_2
    const-string v1, "Outbound"

    const-string v2, "onDeviceAdded"

    const-string v3, "+---------------------------------------------------------------------+"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1443
    invoke-direct {p0, p2}, Lcom/samsung/android/app/FileShareClient/Outbound;->findReceiverWithUPnpDev(Lcom/samsung/android/allshare/Device;)Lcom/samsung/android/app/FileShareClient/Receiver;

    move-result-object v0

    .line 1444
    .local v0, "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    const-string v1, "Outbound"

    const-string v2, "onDeviceAdded"

    const-string v3, "+---------------------------------------------------------------------+"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1447
    if-nez v0, :cond_3

    .line 1448
    const-string v1, "Outbound"

    const-string v2, "onDeviceAdded"

    const-string v3, "device does not be found!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1450
    :cond_3
    const/16 v1, 0x3eb

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 1451
    check-cast p2, Lcom/samsung/android/allshare/file/FileReceiver;

    .end local p2    # "device":Lcom/samsung/android/allshare/Device;
    invoke-virtual {v0, p2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setUPnPDevice(Lcom/samsung/android/allshare/file/FileReceiver;)V

    .line 1452
    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/Receiver;->startReceiveByCondition()V

    goto/16 :goto_0
.end method

.method public onDeviceRemoved(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 6
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 1460
    const-string v2, "Outbound"

    const-string v3, "onDeviceRemoved"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ERROR: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1462
    sget-object v2, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq p3, v2, :cond_1

    .line 1463
    const-string v2, "Outbound"

    const-string v3, "onDeviceRemoved"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ERROR = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1475
    :cond_0
    :goto_0
    return-void

    .line 1465
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    if-nez v2, :cond_2

    .line 1466
    const-string v2, "Outbound"

    const-string v3, "onDeviceRemoved"

    const-string v4, "mReceiverList is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1469
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/Receiver;

    .local v1, "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    move-object v2, p2

    .line 1470
    check-cast v2, Lcom/samsung/android/allshare/file/FileReceiver;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->removedDevice(Lcom/samsung/android/allshare/file/FileReceiver;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public setFailedFileIntent(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "resendFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    const/4 v8, 0x0

    .line 240
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    if-nez v3, :cond_2

    .line 241
    const-string v3, "Outbound"

    const-string v4, "setFailedFileIntent"

    const-string v5, "GENERATE_SUCCESS - null == mReceiverList"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :goto_0
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    .line 248
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    if-nez v3, :cond_3

    .line 249
    const-string v3, "Outbound"

    const-string v4, "setFailedFileIntent"

    const-string v5, "GENERATE_SUCCESS - mFileArrayList == null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    if-eqz v3, :cond_1

    .line 257
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    invoke-interface {v3}, Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;->readyFilelist()V

    .line 260
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    if-eqz v3, :cond_4

    .line 261
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    if-eqz v3, :cond_4

    .line 262
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 263
    .local v2, "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileList(Ljava/util/ArrayList;)V

    .line 264
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->startReceiveByCondition()V

    goto :goto_1

    .line 243
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_2
    const-string v3, "Outbound"

    const-string v4, "setFailedFileIntent"

    const-string v5, "GENERATE_SUCCESS"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 251
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 252
    .local v0, "file":Ljava/io/File;
    iget-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mTotalSize:J

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mTotalSize:J

    goto :goto_2

    .line 269
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_4
    iget-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbShowWait:Z

    if-eqz v3, :cond_6

    .line 270
    iput-boolean v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbShowWait:Z

    .line 271
    const/4 v3, 0x1

    invoke-direct {p0, v3, v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->addOngoing(ZZ)V

    .line 272
    sget v3, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    if-nez v3, :cond_5

    .line 273
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mIfg:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;

    invoke-interface {v3}, Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;->startOutboundForeground()V

    .line 276
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mIfg:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;

    invoke-interface {v3}, Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;->acquireWakeLock()V

    .line 278
    sget v3, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    .line 279
    const-string v3, "Outbound"

    const-string v4, "setFailedFileIntent"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateStatus : mNotiCnt ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    :cond_6
    return-void
.end method

.method public setFileIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 226
    new-instance v1, Lcom/samsung/android/app/FileShareClient/FileManager;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/app/FileShareClient/FileManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileManager:Lcom/samsung/android/app/FileShareClient/FileManager;

    .line 227
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mIntent:Landroid/content/Intent;

    .line 228
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/FileShareClient/Outbound$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/FileShareClient/Outbound$1;-><init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 235
    .local v0, "filelistMaker":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 236
    return-void
.end method

.method public setForegroundInterface(Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;)V
    .locals 0
    .param p1, "iFg"    # Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;

    .prologue
    .line 617
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mIfg:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;

    .line 618
    return-void
.end method

.method public setListener(Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    .prologue
    .line 503
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    .line 504
    return-void
.end method

.method public setP2pDeviceInfoList(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285
    .local p1, "p2pDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Z

    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbCompleteReceiverList:[Z

    .line 287
    const/4 v0, 0x0

    .line 288
    .local v0, "deviceName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "device_name"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    if-nez v0, :cond_0

    .line 290
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 293
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 294
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    new-instance v4, Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    invoke-direct {v4, v1, v2, v0, v5}, Lcom/samsung/android/app/FileShareClient/Receiver;-><init>(ILcom/samsung/android/app/FileShareClient/P2pDeviceInfo;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v2, p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->setListener(Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;)V

    .line 296
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v2

    const/16 v3, 0x3ea

    if-ne v2, v3, :cond_1

    .line 297
    invoke-direct {p0, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->sendFindTimeout(I)V

    .line 299
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbCompleteReceiverList:[Z

    const/4 v3, 0x0

    aput-boolean v3, v2, v1

    .line 293
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 302
    :cond_2
    const-string v2, "Outbound"

    const-string v3, "setP2pDeviceInfoList"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->refreshAndFindDev()V

    .line 304
    return-void
.end method

.method public setP2pDeviceInfoListToResend(Lcom/samsung/android/app/FileShareClient/Receiver;)V
    .locals 7
    .param p1, "nResendReceiver"    # Lcom/samsung/android/app/FileShareClient/Receiver;

    .prologue
    const/4 v6, 0x0

    .line 307
    const-string v2, "Outbound"

    const-string v3, "setP2pDeviceInfoListToResend"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const/4 v0, 0x0

    .line 310
    .local v0, "deviceName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "device_name"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 311
    if-nez v0, :cond_0

    .line 312
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 315
    :cond_0
    const/4 v2, 0x1

    new-array v2, v2, [Z

    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbCompleteReceiverList:[Z

    .line 316
    new-instance v1, Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;

    invoke-direct {v1, v6, v2, v0, v3}, Lcom/samsung/android/app/FileShareClient/Receiver;-><init>(ILcom/samsung/android/app/FileShareClient/P2pDeviceInfo;Ljava/lang/String;Landroid/content/Context;)V

    .line 318
    .local v1, "resendReceiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    invoke-virtual {v1, p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->setListener(Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;)V

    .line 320
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getConnectStatus()I

    move-result v2

    if-nez v2, :cond_1

    .line 321
    invoke-direct {p0, v6}, Lcom/samsung/android/app/FileShareClient/Outbound;->sendFindTimeout(I)V

    .line 340
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbCompleteReceiverList:[Z

    aput-boolean v6, v2, v6

    .line 341
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->refreshAndFindDev()V

    .line 342
    return-void

    .line 322
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 323
    const-string v2, "Outbound"

    const-string v3, "setP2pDeviceInfoListToResend"

    const-string v4, "requestPeers!!!!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    invoke-direct {p0, v6}, Lcom/samsung/android/app/FileShareClient/Outbound;->sendFindTimeout(I)V

    .line 325
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->requestPeers(Ljava/lang/String;)V

    goto :goto_0

    .line 327
    :cond_2
    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 328
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->enableP2p()V

    .line 329
    const-string v2, "Outbound"

    const-string v3, "setP2pDeviceInfoListToResend"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DeviceInfo = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DeviceName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " status = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getConnectStatus()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getUPnPDevice()Lcom/samsung/android/allshare/file/FileReceiver;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setUPnPDevice(Lcom/samsung/android/allshare/file/FileReceiver;)V

    .line 334
    const-string v2, "Outbound"

    const-string v3, "setP2pDeviceInfoListToResend"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "newReceiver =  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getUPnPDevice()Lcom/samsung/android/allshare/file/FileReceiver;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    invoke-virtual {v1, p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->setListener(Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;)V

    .line 337
    invoke-direct {p0, v6}, Lcom/samsung/android/app/FileShareClient/Outbound;->ResendFindTimeout(I)V

    goto/16 :goto_0
.end method

.method public setReqFinish(Z)V
    .locals 0
    .param p1, "reqFinish"    # Z

    .prologue
    .line 218
    iput-boolean p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqFinish:Z

    .line 219
    return-void
.end method

.method public setSconnLitener(Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    .prologue
    .line 507
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 508
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 509
    .local v1, "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v1, p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->setSconnLitener(Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;)V

    goto :goto_0

    .line 512
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mSconnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    .line 513
    return-void
.end method

.method public updateStatus(III)V
    .locals 11
    .param p1, "nIndex"    # I
    .param p2, "nStatus"    # I
    .param p3, "nErrorCause"    # I

    .prologue
    const/16 v10, 0xfa1

    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1496
    const/16 v2, 0x3ec

    if-ne p2, v2, :cond_2

    .line 1497
    iget-boolean v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbAddedOngoing:Z

    if-nez v2, :cond_0

    .line 1498
    invoke-direct {p0, v7, v7}, Lcom/samsung/android/app/FileShareClient/Outbound;->addOngoing(ZZ)V

    .line 1499
    iput-boolean v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbAddedOngoing:Z

    .line 1502
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1503
    .local v0, "time":J
    iget-wide v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastUpdateTime:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0xc8

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    iget-wide v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastProgress:J

    cmp-long v2, v2, v8

    if-eqz v2, :cond_8

    iget-wide v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastProgress:J

    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->getTransferProgress()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_8

    .line 1505
    :cond_1
    const-string v2, "Outbound"

    const-string v3, "updateStatus"

    const-string v4, "skip nofication update"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1506
    iget-wide v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastUpdateTime:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_2

    .line 1507
    iput-wide v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastUpdateTime:J

    .line 1522
    .end local v0    # "time":J
    :cond_2
    :goto_0
    const/16 v2, 0x3ee

    if-eq p2, v2, :cond_3

    const/4 v2, -0x1

    if-eq p2, v2, :cond_3

    const/16 v2, 0x3ed

    if-eq p2, v2, :cond_3

    const/16 v2, 0x3ef

    if-ne p2, v2, :cond_6

    .line 1525
    :cond_3
    sget v2, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    if-nez v2, :cond_4

    .line 1526
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1527
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/Utils;->getSavedP2pStatus()Z

    move-result v2

    if-nez v2, :cond_a

    .line 1528
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->CheckServerCondition()V

    .line 1537
    :cond_4
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbCompleteReceiverList:[Z

    aget-boolean v2, v2, p1

    if-nez v2, :cond_5

    .line 1538
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbCompleteReceiverList:[Z

    aput-boolean v6, v2, p1

    .line 1539
    iget v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCntCompleteReceiver:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCntCompleteReceiver:I

    .line 1541
    :cond_5
    iput-wide v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastUpdateTime:J

    .line 1542
    iput-wide v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastProgress:J

    .line 1545
    :cond_6
    iget v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCntCompleteReceiver:I

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v2, v3, :cond_7

    .line 1547
    invoke-direct {p0, v7}, Lcom/samsung/android/app/FileShareClient/Outbound;->addNotification(Z)V

    .line 1548
    iput-boolean v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbComplete:Z

    .line 1549
    iput-boolean v7, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbAddedOngoing:Z

    .line 1550
    iput-boolean v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbTransferFinished:Z

    .line 1551
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->destroyServiceProvider()V

    .line 1552
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mIfg:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;

    invoke-interface {v2}, Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;->releaseWakeLock()V

    .line 1553
    iput-boolean v6, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mCompleteSend:Z

    .line 1554
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileManager:Lcom/samsung/android/app/FileShareClient/FileManager;

    if-eqz v2, :cond_7

    .line 1555
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mFileManager:Lcom/samsung/android/app/FileShareClient/FileManager;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/FileManager;->destroy()V

    .line 1558
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    if-nez v2, :cond_c

    .line 1559
    const-string v2, "Outbound"

    const-string v3, "updateStatus"

    const-string v4, "listener is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1569
    :goto_2
    return-void

    .line 1509
    .restart local v0    # "time":J
    :cond_8
    iput-wide v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastUpdateTime:J

    .line 1510
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->getTransferProgress()I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mLastProgress:J

    .line 1512
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mNotificationUpdateHandler:Landroid/os/Handler;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mNotificationUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v2, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1514
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mNotificationUpdateHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v10, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1517
    :cond_9
    const-string v2, "Outbound"

    const-string v3, "updateStatus"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DO NOT UPDATE(hasMessages)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mNotificationUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1530
    .end local v0    # "time":J
    :cond_a
    const-string v2, "Outbound"

    const-string v3, "updateStatus"

    const-string v4, "Utils.mWifiP2pStatus = true"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1533
    :cond_b
    const-string v2, "Outbound"

    const-string v3, "updateStatus"

    const-string v4, "p2p already disconnected"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1563
    :cond_c
    iget-boolean v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mbComplete:Z

    if-eqz v2, :cond_d

    .line 1564
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    invoke-interface {v2}, Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;->updateReciver()V

    .line 1565
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    invoke-interface {v2}, Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;->completed()V

    goto :goto_2

    .line 1567
    :cond_d
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound;->mListener:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;

    invoke-interface {v2}, Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;->updateReciver()V

    goto :goto_2
.end method
