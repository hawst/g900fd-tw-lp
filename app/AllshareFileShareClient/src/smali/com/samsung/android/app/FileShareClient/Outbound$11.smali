.class Lcom/samsung/android/app/FileShareClient/Outbound$11;
.super Landroid/os/Handler;
.source "Outbound.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/Outbound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/Outbound;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0

    .prologue
    .line 1182
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$11;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 1184
    const-string v0, " "

    .line 1185
    .local v0, "currentState":Ljava/lang/String;
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d1

    if-ne v1, v2, :cond_1

    .line 1186
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$11;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1300(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04002c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1187
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$11;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2700(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1195
    :cond_0
    :goto_0
    return-void

    .line 1188
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d0

    if-ne v1, v2, :cond_2

    .line 1189
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$11;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1300(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04002d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1190
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$11;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2700(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1191
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x7d2

    if-ne v1, v2, :cond_0

    .line 1192
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$11;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1300(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04002f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1193
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$11;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mThemeContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2700(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
