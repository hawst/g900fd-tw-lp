.class public Lcom/samsung/android/app/FileShareClient/ReceiverFragment;
.super Landroid/app/Fragment;
.source "ReceiverFragment.java"

# interfaces
.implements Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;
    }
.end annotation


# static fields
.field public static final DISABLE_ALL_MENUITEMS:I = 0x4

.field public static final DISABLE_RESEND_FAILED_FILES:I = 0x2

.field public static final ENABLE_CANCEL_CONNECTION:I = 0x1

.field public static final ENABLE_RESEND_FAILED_FILES:I = 0x3

.field private static final TAGClass:Ljava/lang/String; = "ReceiverFragment"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

.field private mCompleteLayout:Landroid/widget/LinearLayout;

.field private mCountTextView:Landroid/widget/TextView;

.field private mCurrentFileInfoTextView:Landroid/widget/TextView;

.field private mCurrentFileSizeTextView:Landroid/widget/TextView;

.field private mCurrentStateTextView:Landroid/widget/TextView;

.field private mCurrentTransferDeviceTextView:Landroid/widget/TextView;

.field private mCustomAdapter:Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;

.field private mFileList:Landroid/widget/ListView;

.field private mInfoTextView:Landroid/widget/TextView;

.field private mListener:Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;

.field private mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

.field private mOutboundId:I

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mProgressItem:Landroid/widget/ProgressBar;

.field private mProgressLayout:Landroid/widget/LinearLayout;

.field private mRateTextView:Landroid/widget/TextView;

.field private mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

.field private mReceiverId:I

.field private mResendFilelist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mResendIndex:I

.field final mUiHandler:Landroid/os/Handler;

.field final mUpdateResults:Ljava/lang/Runnable;

.field private mbEnableResend:I

.field private mbReqFileStatusUpdate:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 91
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 57
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    .line 59
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 61
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendFilelist:Ljava/util/ArrayList;

    .line 63
    iput v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mOutboundId:I

    .line 65
    iput v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiverId:I

    .line 67
    iput v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendIndex:I

    .line 69
    iput v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbEnableResend:I

    .line 79
    iput-boolean v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbReqFileStatusUpdate:Z

    .line 81
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    .line 89
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 680
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mUiHandler:Landroid/os/Handler;

    .line 682
    new-instance v0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$13;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mUpdateResults:Ljava/lang/Runnable;

    .line 92
    return-void
.end method

.method private SendAllFailedFiles(Ljava/lang/Boolean;)V
    .locals 7
    .param p1, "connection"    # Ljava/lang/Boolean;

    .prologue
    .line 530
    const-string v3, "ReceiverFragment"

    const-string v4, "SendAllFailedFiles"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "All failed files Resend clicked, Need to reconnect? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getCurrentCount()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendIndex:I

    .line 535
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 536
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "wifip2p"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/p2p/WifiP2pManager;

    .line 538
    .local v2, "wifiP2pManager":Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    .line 540
    .local v1, "p2pChannel":Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    new-instance v3, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$10;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$10;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)V

    invoke-virtual {v2, v1, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 549
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->setConnectStatus(I)V

    .line 552
    .end local v1    # "p2pChannel":Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .end local v2    # "wifiP2pManager":Landroid/net/wifi/p2p/WifiP2pManager;
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendIndex:I

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 553
    const-string v4, "ReceiverFragment"

    const-string v5, "SendAllFailedFiles"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SendAllFailedFiles : failed file name:  "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v5, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendFilelist:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 552
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 558
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendFilelist:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/ClientService;->createResendOutbound(Ljava/util/ArrayList;Lcom/samsung/android/app/FileShareClient/Receiver;)Lcom/samsung/android/app/FileShareClient/Outbound;

    .line 559
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->onBackPressed()V

    .line 560
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    .line 561
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)Lcom/samsung/android/app/FileShareClient/Receiver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment;
    .param p1, "x1"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showFileDetailDialog(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCustomAdapter:Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment;
    .param p1, "x1"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->checkCurrentReceiverConnection(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)Lcom/samsung/android/app/FileShareClient/Outbound;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    .prologue
    .line 34
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendIndex:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->checkCurrentConnection(ZI)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showResendConfirmDialog(ZI)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showResendAllConfirmDialog(ZI)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showStatus()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbReqFileStatusUpdate:Z

    return v0
.end method

.method static synthetic access$902(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbReqFileStatusUpdate:Z

    return p1
.end method

.method private checkCurrentConnection(ZI)V
    .locals 5
    .param p1, "nReqReconnect"    # Z
    .param p2, "index"    # I

    .prologue
    .line 483
    const-string v2, "ReceiverFragment"

    const-string v3, "checkCurrentConnection"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->checkMobileAP()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 486
    const-string v2, "ReceiverFragment"

    const-string v3, "checkCurrentConnection"

    const-string v4, "checkMobileAP is TRUE"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showCannotSendFile()V

    .line 499
    :goto_0
    return-void

    .line 489
    :cond_0
    move v1, p1

    .line 490
    .local v1, "reqReconnect":Z
    move v0, p2

    .line 491
    .local v0, "fileIndex":I
    const-string v2, "ReceiverFragment"

    const-string v3, "checkCurrentConnection"

    const-string v4, "checkMobileAP is FALSE"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    if-ltz v0, :cond_1

    .line 493
    invoke-direct {p0, v1, v0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->sendFailedFile(ZI)V

    goto :goto_0

    .line 495
    :cond_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->SendAllFailedFiles(Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method private checkCurrentReceiverConnection(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 591
    const-string v3, "ReceiverFragment"

    const-string v4, "checkCurrentReceiverConnection"

    const-string v5, ""

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    move v0, p1

    .line 594
    .local v0, "index":I
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "wifip2p"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/p2p/WifiP2pManager;

    .line 596
    .local v2, "wifiP2pManager":Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    .line 598
    .local v1, "p2pChannel":Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    new-instance v3, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$12;

    invoke-direct {v3, p0, v0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$12;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;I)V

    invoke-virtual {v2, v1, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->requestGroupInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;)V

    .line 627
    return-void
.end method

.method private sendFailedFile(ZI)V
    .locals 7
    .param p1, "connection"    # Z
    .param p2, "index"    # I

    .prologue
    .line 564
    const-string v3, "ReceiverFragment"

    const-string v4, "sendFailedFile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Resend clicked, Need to reconnect? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    iput p2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendIndex:I

    .line 567
    if-eqz p1, :cond_0

    .line 568
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "wifip2p"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/p2p/WifiP2pManager;

    .line 570
    .local v2, "wifiP2pManager":Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    .line 572
    .local v1, "p2pChannel":Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    new-instance v3, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$11;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$11;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)V

    invoke-virtual {v2, v1, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 581
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->setConnectStatus(I)V

    .line 583
    .end local v1    # "p2pChannel":Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .end local v2    # "wifiP2pManager":Landroid/net/wifi/p2p/WifiP2pManager;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 584
    .local v0, "file":Ljava/io/File;
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendFilelist:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 585
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendFilelist:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/ClientService;->createResendOutbound(Ljava/util/ArrayList;Lcom/samsung/android/app/FileShareClient/Receiver;)Lcom/samsung/android/app/FileShareClient/Outbound;

    .line 586
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->onBackPressed()V

    .line 587
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    .line 588
    return-void
.end method

.method private setListener()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->setInfoListener(Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;)V

    .line 163
    return-void
.end method

.method private showCancelConnectionConfirmDialog()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 341
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->getReceiverList()Ljava/util/ArrayList;

    move-result-object v0

    .line 342
    .local v0, "mReceiverList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/FileShareClient/Receiver;>;"
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 343
    const-string v2, "ReceiverFragment"

    const-string v3, "showCancelConnectionConfirmDialog_MultiReceiver"

    const-string v4, "mAlertDialg is null or mAlertDialog is showing"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    :goto_0
    return-void

    .line 346
    :cond_0
    const/4 v1, 0x0

    .line 347
    .local v1, "message":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 348
    const v2, 0x7f04001c

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 353
    :cond_1
    :goto_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f040015

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f040007

    new-instance v4, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$4;

    invoke-direct {v4, p0, v0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$4;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;Ljava/util/ArrayList;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x7f040000

    new-instance v4, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$3;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$3;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 375
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 349
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v3, :cond_1

    .line 350
    const v2, 0x7f04001d

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private showCannotSendFile()V
    .locals 4

    .prologue
    .line 459
    const/4 v0, 0x0

    .line 460
    .local v0, "message":Ljava/lang/String;
    const-string v1, "USC"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 461
    const v1, 0x7f040021

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 466
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 467
    const-string v1, "ReceiverFragment"

    const-string v2, "showCannotSendFile"

    const-string v3, "mAlertDialg is null or mAlertDialog is showing"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    :goto_1
    return-void

    .line 463
    :cond_0
    const v1, 0x7f040020

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 469
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f04003b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f040007

    new-instance v3, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$9;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$9;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 478
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_1
.end method

.method private showFileDetailDialog(I)V
    .locals 14
    .param p1, "index"    # I

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const-string v10, "layout_inflater"

    invoke-virtual {v9, v10}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 298
    .local v6, "inflater":Landroid/view/LayoutInflater;
    const v9, 0x7f03000a

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 300
    .local v7, "layout":Landroid/widget/LinearLayout;
    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v9}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 302
    .local v2, "file":Ljava/io/File;
    const v9, 0x7f040018

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 303
    .local v3, "filename":Ljava/lang/String;
    const v9, 0x7f04001a

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getDescription(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 305
    .local v5, "filetype":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v1, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 307
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/samsung/android/app/FileShareClient/FileManager;->getConvertSize(J)Ljava/lang/String;

    move-result-object v9

    const-string v10, "Zero"

    if-ne v9, v10, :cond_0

    .line 309
    const-string v9, "ReceiverFragment"

    const-string v10, "showFileDetailDialog"

    const-string v11, "file.length == Zero"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :goto_0
    const v9, 0x7f060033

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ListView;

    .line 318
    .local v8, "listView":Landroid/widget/ListView;
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v10, 0x7f030009

    invoke-direct {v0, v9, v10, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 320
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 321
    invoke-virtual {v8, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 323
    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 324
    const-string v9, "ReceiverFragment"

    const-string v10, "showFileDetailDialog"

    const-string v11, "mAlertDialg is null or mAlertDialog is showing"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    :goto_1
    return-void

    .line 311
    .end local v0    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    .end local v8    # "listView":Landroid/widget/ListView;
    :cond_0
    const v9, 0x7f040019

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/samsung/android/app/FileShareClient/FileManager;->getConvertSize(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 313
    .local v4, "filesize":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 327
    .end local v4    # "filesize":Ljava/lang/String;
    .restart local v0    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    .restart local v8    # "listView":Landroid/widget/ListView;
    :cond_1
    new-instance v9, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-direct {v9, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v10, 0x7f040005

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x7f040007

    new-instance v11, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$2;

    invoke-direct {v11, p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$2;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)V

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 336
    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    goto :goto_1
.end method

.method private showFileList()V
    .locals 3

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060024

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mFileList:Landroid/widget/ListView;

    .line 277
    new-instance v0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;-><init>(Landroid/content/Context;Lcom/samsung/android/app/FileShareClient/Receiver;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCustomAdapter:Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;

    .line 278
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mFileList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCustomAdapter:Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 279
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mFileList:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$1;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 293
    return-void
.end method

.method private showResendAllConfirmDialog(ZI)V
    .locals 6
    .param p1, "nReqReconnect"    # Z
    .param p2, "index"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 421
    move v1, p1

    .line 422
    .local v1, "reqReconnect":Z
    const/4 v0, 0x0

    .line 424
    .local v0, "message":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 425
    const v2, 0x7f040023

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 432
    :goto_0
    iput p2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendIndex:I

    .line 434
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 435
    const-string v2, "ReceiverFragment"

    const-string v3, "showResendAllConfirmDialog"

    const-string v4, "mAlertDialg is null or mAlertDialog is showing"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    :goto_1
    return-void

    .line 429
    :cond_0
    const v2, 0x7f040022

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 438
    :cond_1
    const-string v2, "ReceiverFragment"

    const-string v3, "showResendAllConfirmDialog"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f040017

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f040007

    new-instance v4, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$8;

    invoke-direct {v4, p0, v1}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$8;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;Z)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x7f040000

    new-instance v4, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$7;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$7;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 454
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_1
.end method

.method private showResendConfirmDialog(ZI)V
    .locals 8
    .param p1, "nReqReconnect"    # Z
    .param p2, "index"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 380
    move v2, p1

    .line 381
    .local v2, "reqReconnect":Z
    const/4 v1, 0x0

    .line 382
    .local v1, "message":Ljava/lang/String;
    const/4 v0, 0x0

    .line 384
    .local v0, "filename":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/app/FileShareClient/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 385
    const v4, 0x7f040018

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 387
    const v3, 0x7f040023

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 395
    :goto_0
    iput p2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendIndex:I

    .line 396
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 397
    const-string v3, "ReceiverFragment"

    const-string v4, "showResendConfirmDialog"

    const-string v5, "mAlertDialg is null or mAlertDialog is showing"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :goto_1
    return-void

    .line 391
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 392
    const v3, 0x7f04001f

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 400
    :cond_1
    const-string v3, "ReceiverFragment"

    const-string v4, "showResendConfirmDialog"

    const-string v5, ""

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f04000d

    new-instance v5, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$6;

    invoke-direct {v5, p0, v2}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$6;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;Z)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x7f040000

    new-instance v5, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$5;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$5;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    .line 416
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    goto :goto_1
.end method

.method private showStatus()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/16 v11, 0x8

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 166
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getCurrentCount()I

    move-result v0

    .line 167
    .local v0, "currentCount":I
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getTotalCount()I

    move-result v2

    .line 168
    .local v2, "totalCount":I
    const-string v1, ""

    .line 170
    .local v1, "currentState":Ljava/lang/String;
    invoke-virtual {p0, v8}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->setMenuVisibility(Z)V

    .line 171
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v3

    const/16 v4, 0x3ec

    if-ne v3, v4, :cond_2

    .line 172
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 173
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCompleteLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 175
    if-eq v0, v2, :cond_0

    .line 176
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCurrentTransferDeviceTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCurrentFileInfoTextView:Landroid/widget/TextView;

    const v5, 0x7f04000a

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v8

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getTotalCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCurrentFileSizeTextView:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f040011

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareClient/Outbound;->getTotalSize()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/samsung/android/app/FileShareClient/FileManager;->getConvertSize(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCountTextView:Landroid/widget/TextView;

    const-string v4, "%d/%d"

    new-array v5, v10, [Ljava/lang/Object;

    add-int/lit8 v6, v0, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mRateTextView:Landroid/widget/TextView;

    const-string v4, "%d%%"

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareClient/Receiver;->getProgress()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->getProgress()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 188
    iput v10, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbEnableResend:I

    .line 190
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mListener:Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;

    if-eqz v3, :cond_1

    .line 191
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mListener:Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;

    iget v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbEnableResend:I

    invoke-interface {v3, v4}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;->notifyReceiverMenuChanged(I)V

    .line 273
    :cond_1
    :goto_0
    return-void

    .line 196
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCompleteLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 197
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 201
    if-nez v0, :cond_3

    .line 202
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f04002b

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 214
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCurrentStateTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 265
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mInfoTextView:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "error = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressItem:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 267
    iput v10, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbEnableResend:I

    .line 269
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mListener:Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;

    if-eqz v3, :cond_1

    .line 270
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mListener:Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;

    iget v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbEnableResend:I

    invoke-interface {v3, v4}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;->notifyReceiverMenuChanged(I)V

    goto :goto_0

    .line 203
    :cond_3
    if-ne v0, v2, :cond_5

    .line 204
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v9, :cond_4

    .line 205
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f040031

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 208
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f040030

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 212
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f04002d

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 218
    :sswitch_0
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getErrorCause()I

    move-result v3

    if-ne v3, v10, :cond_7

    .line 219
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mInfoTextView:Landroid/widget/TextView;

    const v4, 0x7f040029

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    :cond_6
    :goto_3
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressItem:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 225
    iput v12, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbEnableResend:I

    goto :goto_2

    .line 220
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getErrorCause()I

    move-result v3

    if-ne v3, v12, :cond_8

    .line 221
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mInfoTextView:Landroid/widget/TextView;

    const v4, 0x7f040032

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 222
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getErrorCause()I

    move-result v3

    if-ne v3, v9, :cond_6

    .line 223
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mInfoTextView:Landroid/widget/TextView;

    const v4, 0x7f040029

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 228
    :sswitch_1
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getTotalCount()I

    move-result v3

    if-le v3, v9, :cond_9

    .line 229
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mInfoTextView:Landroid/widget/TextView;

    const v5, 0x7f04000a

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v8

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getTotalCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    :goto_4
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressItem:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 237
    const/4 v3, 0x4

    iput v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbEnableResend:I

    goto/16 :goto_2

    .line 234
    :cond_9
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mInfoTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 240
    :sswitch_2
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mInfoTextView:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f040035

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f040033

    new-array v6, v9, [Ljava/lang/Object;

    sub-int v7, v2, v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressItem:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 243
    iput v12, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbEnableResend:I

    goto/16 :goto_2

    .line 246
    :sswitch_3
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mInfoTextView:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f040035

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f040034

    new-array v6, v9, [Ljava/lang/Object;

    sub-int v7, v2, v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressItem:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 249
    iput v12, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbEnableResend:I

    goto/16 :goto_2

    .line 252
    :sswitch_4
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mInfoTextView:Landroid/widget/TextView;

    const v4, 0x7f040028

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressItem:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 254
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCurrentStateTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    iput v9, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbEnableResend:I

    goto/16 :goto_2

    .line 259
    :sswitch_5
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mInfoTextView:Landroid/widget/TextView;

    const v4, 0x7f040027

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressItem:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 261
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCurrentStateTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    iput v10, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbEnableResend:I

    goto/16 :goto_2

    .line 216
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x3e9 -> :sswitch_4
        0x3ea -> :sswitch_5
        0x3eb -> :sswitch_5
        0x3ed -> :sswitch_2
        0x3ee -> :sswitch_1
        0x3ef -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public checkMobileAP()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 502
    const-string v3, "ReceiverFragment"

    const-string v5, "checkMobileAP"

    const-string v6, ""

    invoke-static {v3, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    const/4 v1, 0x0

    .line 507
    .local v1, "m":Ljava/lang/reflect/Method;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v5, "wifi"

    invoke-virtual {v3, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 509
    .local v2, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "isWifiApEnabled"

    const/4 v3, 0x0

    check-cast v3, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    .line 511
    const/4 v3, 0x0

    :try_start_1
    check-cast v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-ne v4, v3, :cond_0

    .line 512
    const-string v3, "ReceiverFragment"

    const-string v5, "checkMobileAP -"

    const-string v6, "HOTSPOT Enabled"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2

    move v3, v4

    .line 525
    .end local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :goto_0
    return v3

    .line 515
    .restart local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_0
    move-exception v0

    .line 516
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 525
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_0
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 517
    .restart local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_1
    move-exception v0

    .line 518
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 522
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    .end local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_2
    move-exception v0

    .line 523
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "ReceiverFragment"

    const-string v4, "checkMobileAP : NoSuchMethodException :"

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 519
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_3
    move-exception v0

    .line 520
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1
.end method

.method public getEnableResend()I
    .locals 1

    .prologue
    .line 641
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbEnableResend:I

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 103
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060022

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressLayout:Landroid/widget/LinearLayout;

    .line 104
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f06002a

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCurrentTransferDeviceTextView:Landroid/widget/TextView;

    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f06002c

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCurrentFileInfoTextView:Landroid/widget/TextView;

    .line 108
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f06002d

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCurrentFileSizeTextView:Landroid/widget/TextView;

    .line 110
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060030

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCountTextView:Landroid/widget/TextView;

    .line 111
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060031

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mRateTextView:Landroid/widget/TextView;

    .line 112
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f06002e

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressBar:Landroid/widget/ProgressBar;

    .line 123
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060023

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCompleteLayout:Landroid/widget/LinearLayout;

    .line 124
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060026

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mCurrentStateTextView:Landroid/widget/TextView;

    .line 128
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060028

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mInfoTextView:Landroid/widget/TextView;

    .line 129
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060027

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mProgressItem:Landroid/widget/ProgressBar;

    .line 131
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 156
    const-string v0, "ReceiverFragment"

    const-string v1, "onBackPressed"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->setInfoListener(Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;)V

    .line 159
    :cond_0
    return-void
.end method

.method public onCancelConnectMenuClicked()V
    .locals 3

    .prologue
    .line 665
    const-string v0, "ReceiverFragment"

    const-string v1, "onCancelConnectMenuClicked"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showCancelConnectionConfirmDialog()V

    .line 667
    return-void
.end method

.method public onCancelTransferMenuClicked()V
    .locals 3

    .prologue
    .line 670
    const-string v0, "ReceiverFragment"

    const-string v1, "onCancelTransferMenuClicked"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/Receiver;->cancel()V

    .line 674
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 96
    const v0, 0x7f030006

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 148
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 149
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->setInfoListener(Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;)V

    .line 152
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 142
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 143
    return-void
.end method

.method public onResendAllMenuClicked()V
    .locals 1

    .prologue
    .line 677
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->checkCurrentReceiverConnection(I)V

    .line 678
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 137
    return-void
.end method

.method public setClientService(Lcom/samsung/android/app/FileShareClient/ClientService;)V
    .locals 3
    .param p1, "clientService"    # Lcom/samsung/android/app/FileShareClient/ClientService;

    .prologue
    .line 651
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    .line 652
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    iget v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mOutboundId:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/ClientService;->getOutbound(I)Lcom/samsung/android/app/FileShareClient/Outbound;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    .line 653
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    if-nez v0, :cond_0

    .line 654
    const-string v0, "ReceiverFragment"

    const-string v1, "showResendAllConfirmDialog"

    const-string v2, "setClientService: mOutbound is null."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    :goto_0
    return-void

    .line 657
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/Outbound;->getReceiverList()Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiverId:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/FileShareClient/Receiver;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 659
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->setListener()V

    .line 660
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showStatus()V

    .line 661
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showFileList()V

    goto :goto_0
.end method

.method public setIdInformation(II)V
    .locals 1
    .param p1, "outboundId"    # I
    .param p2, "receiverId"    # I

    .prologue
    .line 645
    iput p1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mOutboundId:I

    .line 646
    iput p2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiverId:I

    .line 647
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mResendFilelist:Ljava/util/ArrayList;

    .line 648
    return-void
.end method

.method public setListener(Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;)V
    .locals 0
    .param p1, "receiverFragmentListener"    # Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;

    .prologue
    .line 637
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mListener:Lcom/samsung/android/app/FileShareClient/ReceiverFragment$ReceiverFragmentListener;

    .line 638
    return-void
.end method

.method public updateStatus(Z)V
    .locals 2
    .param p1, "reqFileStatusUpdate"    # Z

    .prologue
    .line 704
    iput-boolean p1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mbReqFileStatusUpdate:Z

    .line 706
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$14;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$14;-><init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 713
    .local v0, "mUiThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 714
    return-void
.end method
