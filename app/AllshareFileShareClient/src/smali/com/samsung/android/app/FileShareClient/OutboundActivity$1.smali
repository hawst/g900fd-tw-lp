.class Lcom/samsung/android/app/FileShareClient/OutboundActivity$1;
.super Ljava/lang/Object;
.source "OutboundActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/OutboundActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/OutboundActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/OutboundActivity;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity$1;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 319
    const-string v1, "OutboudActivity"

    const-string v2, "onServiceConnected"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p2

    .line 320
    check-cast v0, Lcom/samsung/android/app/FileShareClient/ClientService$ClinetServiceBinder;

    .line 321
    .local v0, "binder":Lcom/samsung/android/app/FileShareClient/ClientService$ClinetServiceBinder;
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity$1;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundActivity;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/ClientService$ClinetServiceBinder;->getService()Lcom/samsung/android/app/FileShareClient/ClientService;

    move-result-object v2

    # setter for: Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;
    invoke-static {v1, v2}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->access$002(Lcom/samsung/android/app/FileShareClient/OutboundActivity;Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/app/FileShareClient/ClientService;

    .line 322
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity$1;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundActivity;

    # getter for: Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->access$100(Lcom/samsung/android/app/FileShareClient/OutboundActivity;)Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 323
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity$1;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundActivity;

    # getter for: Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mOutboundFragment:Lcom/samsung/android/app/FileShareClient/OutboundFragment;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->access$100(Lcom/samsung/android/app/FileShareClient/OutboundActivity;)Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity$1;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundActivity;

    # getter for: Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->access$000(Lcom/samsung/android/app/FileShareClient/OutboundActivity;)Lcom/samsung/android/app/FileShareClient/ClientService;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->setClientService(Lcom/samsung/android/app/FileShareClient/ClientService;)V

    .line 325
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 330
    const-string v0, "OutboudActivity"

    const-string v1, "onServiceDisconnected"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundActivity$1;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundActivity;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/FileShareClient/OutboundActivity;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;
    invoke-static {v0, v1}, Lcom/samsung/android/app/FileShareClient/OutboundActivity;->access$002(Lcom/samsung/android/app/FileShareClient/OutboundActivity;Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/app/FileShareClient/ClientService;

    .line 332
    return-void
.end method
