.class public Lcom/samsung/android/app/FileShareClient/DbAdapter;
.super Ljava/lang/Object;
.source "DbAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareClient/DbAdapter$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_CREATE_1:Ljava/lang/String; = "CREATE TABLE filelist_tbl (_id integer primary key autoincrement,outbound_id integer,file_path text not null);"

.field private static final DATABASE_CREATE_2:Ljava/lang/String; = "CREATE TABLE receiver_tbl (_id integer primary key autoincrement,outbound_id integer,device_name text not null,receiver_status integer,total_count integer,current_count integer);"

.field private static final DATABASE_NAME:Ljava/lang/String; = "outbound_transfer.db"

.field private static final DATABASE_TABLE_1:Ljava/lang/String; = "filelist_tbl"

.field private static final DATABASE_TABLE_2:Ljava/lang/String; = "receiver_tbl"

.field private static final DATABASE_VERSION:I = 0x1

.field public static final KEY_CURRENT_COUNT:Ljava/lang/String; = "current_count"

.field public static final KEY_DEVICE_NAME:Ljava/lang/String; = "device_name"

.field public static final KEY_FILE_PATH:Ljava/lang/String; = "file_path"

.field public static final KEY_OUTBOUND_ID:Ljava/lang/String; = "outbound_id"

.field public static final KEY_RECEIVER_STATUS:Ljava/lang/String; = "receiver_status"

.field public static final KEY_ROWID:Ljava/lang/String; = "_id"

.field public static final KEY_TOTAL_COUNT:Ljava/lang/String; = "total_count"

.field private static final TAGClass:Ljava/lang/String; = "DbAdapter"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mDbHelper:Lcom/samsung/android/app/FileShareClient/DbAdapter$DatabaseHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mContext:Landroid/content/Context;

    .line 81
    return-void
.end method


# virtual methods
.method public clearDbTable()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE IF EXISTS filelist_tbl"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE IF EXISTS receiver_tbl"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE filelist_tbl (_id integer primary key autoincrement,outbound_id integer,file_path text not null);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE receiver_tbl (_id integer primary key autoincrement,outbound_id integer,device_name text not null,receiver_status integer,total_count integer,current_count integer);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDbHelper:Lcom/samsung/android/app/FileShareClient/DbAdapter$DatabaseHelper;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/DbAdapter$DatabaseHelper;->close()V

    .line 91
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 92
    return-void
.end method

.method public deleteFilelistInfo(J)Z
    .locals 5
    .param p1, "rowId"    # J

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "filelist_tbl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deleteReceiverInfo(J)Z
    .locals 5
    .param p1, "rowId"    # J

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "receiver_tbl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public insertOutbound(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 7
    .param p1, "info"    # Lcom/samsung/android/app/FileShareClient/Outbound;

    .prologue
    const/4 v6, 0x0

    .line 95
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 98
    .local v3, "value":Landroid/content/ContentValues;
    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 99
    .local v0, "file":Ljava/io/File;
    const-string v4, "outbound_id"

    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->getId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 100
    const-string v4, "file_path"

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "filelist_tbl"

    invoke-virtual {v4, v5, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 102
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    goto :goto_0

    .line 106
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->getReceiverList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 107
    .local v2, "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    const-string v4, "outbound_id"

    invoke-virtual {p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->getId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 108
    const-string v4, "device_name"

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v4, "receiver_status"

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 110
    const-string v4, "total_count"

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getTotalCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 111
    const-string v4, "current_count"

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->getCurrentCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 112
    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "receiver_tbl"

    invoke-virtual {v4, v5, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 113
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    goto :goto_1

    .line 115
    .end local v2    # "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_1
    return-void
.end method

.method public open()Lcom/samsung/android/app/FileShareClient/DbAdapter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Lcom/samsung/android/app/FileShareClient/DbAdapter$DatabaseHelper;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/FileShareClient/DbAdapter$DatabaseHelper;-><init>(Lcom/samsung/android/app/FileShareClient/DbAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDbHelper:Lcom/samsung/android/app/FileShareClient/DbAdapter$DatabaseHelper;

    .line 85
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDbHelper:Lcom/samsung/android/app/FileShareClient/DbAdapter$DatabaseHelper;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/DbAdapter$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 86
    return-object p0
.end method

.method public queryFilelistInfo(I)Landroid/database/Cursor;
    .locals 9
    .param p1, "outboundId"    # I

    .prologue
    const/4 v2, 0x0

    .line 134
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "filelist_tbl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "outbound_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 136
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 137
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 139
    :cond_0
    return-object v8
.end method

.method public queryReceiverInfo(I)Landroid/database/Cursor;
    .locals 9
    .param p1, "outboundId"    # I

    .prologue
    const/4 v2, 0x0

    .line 143
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/DbAdapter;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "receiver_tbl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "outbound_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 145
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 146
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 148
    :cond_0
    return-object v8
.end method
