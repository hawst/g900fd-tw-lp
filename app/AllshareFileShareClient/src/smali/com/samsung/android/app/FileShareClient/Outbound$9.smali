.class Lcom/samsung/android/app/FileShareClient/Outbound$9;
.super Landroid/content/BroadcastReceiver;
.source "Outbound.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/Outbound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/Outbound;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0

    .prologue
    .line 921
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "it"    # Landroid/content/Intent;

    .prologue
    .line 926
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 927
    .local v1, "action":Ljava/lang/String;
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onReceive: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    const-string v8, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 931
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Outbound$9;->isInitialStickyBroadcast()Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1300(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 932
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    const-string v10, "onReceive: ignore this"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    .end local v1    # "action":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 934
    .restart local v1    # "action":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    .line 937
    .local v2, "connectedDevAddr":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1300(Lcom/samsung/android/app/FileShareClient/Outbound;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Utils;->isWifiP2pConnected(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 938
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    const-string v10, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE connected"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mCompleteSend:Z
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1400(Lcom/samsung/android/app/FileShareClient/Outbound;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 942
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    const-string v10, "Send contents to all mReceiverList"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v9, 0x1

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->setConnectionStatus(ZLjava/lang/String;)V
    invoke-static {v8, v9, v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1500(Lcom/samsung/android/app/FileShareClient/Outbound;ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1050
    .end local v1    # "action":Ljava/lang/String;
    .end local v2    # "connectedDevAddr":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 1051
    .local v3, "e":Ljava/lang/Exception;
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onReceive Exception"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 946
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "action":Ljava/lang/String;
    .restart local v2    # "connectedDevAddr":Ljava/lang/String;
    :cond_2
    :try_start_1
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v9, 0x1

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->setConnectionStatus(ZLjava/lang/String;)V
    invoke-static {v8, v9, v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1500(Lcom/samsung/android/app/FileShareClient/Outbound;ZLjava/lang/String;)V

    .line 947
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->requestServiceProvider()V
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1600(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    goto :goto_0

    .line 950
    :cond_3
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    const-string v10, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE disconnected"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    sget v8, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    if-lez v8, :cond_4

    .line 955
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Utils.mClientProperty : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v11, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    const/4 v8, 0x0

    sput v8, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    .line 958
    sget v8, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Utils;->setClientIsRunning(I)V

    .line 961
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqReconnect:Z
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$300(Lcom/samsung/android/app/FileShareClient/Outbound;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 976
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mFileArrayList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1700(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v8

    if-nez v8, :cond_5

    .line 977
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v9, 0x0

    # setter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mbShowWait:Z
    invoke-static {v8, v9}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1802(Lcom/samsung/android/app/FileShareClient/Outbound;Z)Z

    .line 979
    :cond_5
    if-nez v2, :cond_a

    .line 980
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v8

    if-nez v8, :cond_7

    .line 981
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    const-string v10, "onReceive : mReceiverList is null"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    :cond_6
    :goto_1
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->destroyServiceProvider()V
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2200(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    .line 1006
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mIfg:Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2300(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;

    move-result-object v8

    invoke-interface {v8}, Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;->releaseWakeLock()V

    goto/16 :goto_0

    .line 983
    :cond_7
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mCompleteSend:Z
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1400(Lcom/samsung/android/app/FileShareClient/Outbound;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 984
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    const-string v10, "Send contents to all mReceiverList."

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/Utils;->resetP2pStatus()V

    .line 987
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/Outbound;->stopDelayTimer()V

    .line 988
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->unregBroadcastReceiver()V
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2000(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    goto :goto_1

    .line 990
    :cond_8
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_9
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 991
    .local v5, "r":Lcom/samsung/android/app/FileShareClient/Receiver;
    const/16 v8, 0x3ef

    invoke-virtual {v5, v8}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 992
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    const-string v10, "set CONNECTING"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 993
    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->setConnectStatus(I)V

    .line 995
    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 996
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mSconnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2100(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    move-result-object v8

    if-eqz v8, :cond_9

    .line 997
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "send Cancelled: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mSconnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2100(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    move-result-object v8

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->getUPnPDevice()Lcom/samsung/android/allshare/file/FileReceiver;

    move-result-object v9

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->getSessionID()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v8, v9, v10, v11}, Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;->onCancelled(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1008
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "r":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_a
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v9, 0x0

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->setConnectionStatus(ZLjava/lang/String;)V
    invoke-static {v8, v9, v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1500(Lcom/samsung/android/app/FileShareClient/Outbound;ZLjava/lang/String;)V

    goto/16 :goto_0

    .line 1013
    .end local v2    # "connectedDevAddr":Ljava/lang/String;
    :cond_b
    const-string v8, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 1014
    const-string v8, "connectedDevAddress"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1016
    .restart local v2    # "connectedDevAddr":Ljava/lang/String;
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "android.net.wifi.p2p.PEERS_CHANGED "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mbReqReconnect:Z
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$300(Lcom/samsung/android/app/FileShareClient/Outbound;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 1019
    iget-object v9, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v8

    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/app/FileShareClient/Receiver;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->requestPeer(Lcom/samsung/android/app/FileShareClient/Receiver;)V
    invoke-static {v9, v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2400(Lcom/samsung/android/app/FileShareClient/Outbound;Lcom/samsung/android/app/FileShareClient/Receiver;)V

    .line 1021
    :cond_c
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_e

    .line 1022
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mCompleteSend:Z
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1400(Lcom/samsung/android/app/FileShareClient/Outbound;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 1023
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_0

    .line 1024
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    const-string v10, "Send contents to all mReceiverList."

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1026
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/Utils;->resetP2pStatus()V

    .line 1027
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/Outbound;->stopDelayTimer()V

    .line 1028
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->unregBroadcastReceiver()V
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2000(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    goto/16 :goto_0

    .line 1031
    :cond_d
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # invokes: Lcom/samsung/android/app/FileShareClient/Outbound;->changedPeer(Ljava/lang/String;)V
    invoke-static {v8, v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$2500(Lcom/samsung/android/app/FileShareClient/Outbound;Ljava/lang/String;)V

    .line 1032
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    const-string v10, "onReceive : call changedPeer"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1035
    :cond_e
    const-string v8, "Outbound"

    const-string v9, "mBroadcastReceiver"

    const-string v10, "onReceive: ignore this"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1038
    .end local v2    # "connectedDevAddr":Ljava/lang/String;
    :cond_f
    const-string v8, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_10

    const-string v8, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_10

    const-string v8, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_10

    const-string v8, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1043
    :cond_10
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_11
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 1044
    .local v6, "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/Outbound$9;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mFileManager:Lcom/samsung/android/app/FileShareClient/FileManager;
    invoke-static {v8}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$200(Lcom/samsung/android/app/FileShareClient/Outbound;)Lcom/samsung/android/app/FileShareClient/FileManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/app/FileShareClient/FileManager;->getSourceFilePath()Ljava/lang/String;

    move-result-object v0

    .line 1045
    .local v0, "SourceFilePath":Ljava/lang/String;
    const-string v7, "/storage/emulated/0"

    .line 1046
    .local v7, "substr":Ljava/lang/String;
    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_11

    .line 1047
    invoke-virtual {v6}, Lcom/samsung/android/app/FileShareClient/Receiver;->fail()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method
