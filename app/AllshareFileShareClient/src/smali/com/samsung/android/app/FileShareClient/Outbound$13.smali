.class Lcom/samsung/android/app/FileShareClient/Outbound$13;
.super Landroid/os/Handler;
.source "Outbound.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/Outbound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/Outbound;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0

    .prologue
    .line 1265
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$13;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1269
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x3eb

    if-ne v2, v3, :cond_0

    .line 1270
    const-string v2, "Outbound"

    const-string v3, "mResendHandler"

    const-string v4, "RESEND_TIME_OUT"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    const-string v2, "Outbound"

    const-string v3, "mResendHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Time Out ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1273
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1274
    .local v0, "id":I
    const-string v2, "Outbound"

    const-string v3, "mResendHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1276
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound$13;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1277
    const-string v2, "Outbound"

    const-string v3, "mResendHandler"

    const-string v4, "ReceiverList != null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1278
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Outbound$13;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    # getter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mReceiverList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$1900(Lcom/samsung/android/app/FileShareClient/Outbound;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 1280
    .local v1, "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v2

    const/16 v3, 0x3e9

    if-ne v2, v3, :cond_0

    .line 1281
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 1282
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setErrorCause(I)V

    .line 1283
    const/16 v2, 0x68

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileStatus(I)V

    .line 1284
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 1288
    .end local v0    # "id":I
    .end local v1    # "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_0
    return-void
.end method
