.class public Lcom/samsung/android/app/FileShareClient/Receiver;
.super Ljava/lang/Object;
.source "Receiver.java"

# interfaces
.implements Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;
.implements Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;,
        Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;
    }
.end annotation


# static fields
.field public static final ERROR_CONNECT_FAIL:I = 0x2

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_SVC_NOT_FOUND:I = 0x3

.field public static final ERROR_UNKNOWN:I = 0x1

.field public static final FILE_STATUS_CANCELLED:I = 0x69

.field public static final FILE_STATUS_FAILED:I = 0x68

.field public static final FILE_STATUS_SENDING:I = 0x66

.field public static final FILE_STATUS_SENT:I = 0x67

.field public static final FILE_STATUS_WAITING:I = 0x65

.field public static final STATUS_CANCELLED:I = 0x3ed

.field public static final STATUS_COMPLETED:I = 0x3ee

.field public static final STATUS_CONNECTED:I = 0x3ea

.field public static final STATUS_CONNECTING:I = 0x3e9

.field public static final STATUS_ERROR:I = -0x1

.field public static final STATUS_FAILED:I = 0x3ef

.field public static final STATUS_PROGRESS:I = 0x3ec

.field public static final STATUS_SEARCHED:I = 0x3eb

.field private static final TAGClass:Ljava/lang/String; = "Receiver"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentCount:I

.field private mCurrentSize:J

.field private mErrorCause:I

.field private mFileStatusList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mFilelist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mId:I

.field private mInfoListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;

.field private mListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;

.field private mMyName:Ljava/lang/String;

.field private mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

.field private mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

.field private mSessionID:Ljava/lang/String;

.field private mStatus:I

.field private mTotalCount:I

.field private mTotalSize:J

.field private mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

.field private mbReqFileStatusUpdate:Z


# direct methods
.method constructor <init>(ILcom/samsung/android/app/FileShareClient/P2pDeviceInfo;Ljava/lang/String;Landroid/content/Context;)V
    .locals 6
    .param p1, "nId"    # I
    .param p2, "devInfo"    # Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;
    .param p3, "deviceName"    # Ljava/lang/String;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mId:I

    .line 63
    iput v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    .line 65
    iput v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mErrorCause:I

    .line 67
    iput v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalCount:I

    .line 69
    iput v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentCount:I

    .line 71
    iput-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalSize:J

    .line 73
    iput-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentSize:J

    .line 75
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    .line 77
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mbReqFileStatusUpdate:Z

    .line 79
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    .line 81
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    .line 83
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFilelist:Ljava/util/ArrayList;

    .line 85
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFileStatusList:Ljava/util/ArrayList;

    .line 87
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;

    .line 89
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mInfoListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;

    .line 91
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    .line 93
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mMyName:Ljava/lang/String;

    .line 95
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mContext:Landroid/content/Context;

    .line 112
    iput p1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mId:I

    .line 113
    iput-object p2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    .line 114
    iput-object p3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mMyName:Ljava/lang/String;

    .line 115
    iput-object p4, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mContext:Landroid/content/Context;

    .line 117
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getConnectStatus()I

    move-result v0

    if-nez v0, :cond_0

    .line 118
    const/16 v0, 0x3ea

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    const/16 v0, 0x3e9

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 6

    .prologue
    const/16 v5, 0x3ed

    const/16 v4, 0x69

    .line 288
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    if-eqz v0, :cond_2

    .line 289
    const-string v0, "Receiver"

    const-string v1, "cancel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UPnP Device["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], mSession ID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 293
    const-string v0, "Receiver"

    const-string v1, "cancel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cancel : mSessionID is not null "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 295
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/file/FileReceiver;->cancel(Ljava/lang/String;)V

    .line 296
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileStatus(I)V

    .line 297
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    const-string v0, "Receiver"

    const-string v1, "cancel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSession ID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v0

    const/16 v1, 0x3ee

    if-eq v0, v1, :cond_0

    .line 301
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 302
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileStatus(I)V

    .line 303
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    goto :goto_0

    .line 307
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    if-eqz v0, :cond_0

    .line 308
    const-string v0, "Receiver"

    const-string v1, "cancel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "P2P Device["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 310
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileStatus(I)V

    .line 311
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    goto :goto_0
.end method

.method public fail()V
    .locals 6

    .prologue
    const/16 v5, 0x3ef

    const/16 v4, 0x68

    .line 316
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    if-eqz v0, :cond_2

    .line 317
    const-string v0, "Receiver"

    const-string v1, "fail"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UPnP Device["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], mSession ID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 321
    const-string v0, "Receiver"

    const-string v1, "fail"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cancel : mSessionID is not null "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 323
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/file/FileReceiver;->cancel(Ljava/lang/String;)V

    .line 324
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileStatus(I)V

    .line 325
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    const-string v0, "Receiver"

    const-string v1, "fail"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSession ID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v0

    const/16 v1, 0x3ee

    if-eq v0, v1, :cond_0

    .line 329
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 330
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileStatus(I)V

    .line 331
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    goto :goto_0

    .line 335
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    if-eqz v0, :cond_0

    .line 336
    const-string v0, "Receiver"

    const-string v1, "fail"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "P2P Device["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 339
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileStatus(I)V

    .line 340
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    goto :goto_0
.end method

.method public getCurrentCount()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentCount:I

    return v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    if-nez v0, :cond_0

    .line 189
    const-string v0, "Unknown"

    .line 191
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceTypeIcon()I
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getTypeIcon()I

    move-result v0

    return v0
.end method

.method public getErrorCause()I
    .locals 1

    .prologue
    .line 377
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mErrorCause:I

    return v0
.end method

.method public getFileArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 239
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFilelist:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFileStatus(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 243
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFileStatusList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 381
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mId:I

    return v0
.end method

.method public getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    return-object v0
.end method

.method public getProgress()I
    .locals 6

    .prologue
    .line 225
    const/4 v0, 0x0

    .line 226
    .local v0, "nPercent":I
    iget-wide v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalSize:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 227
    iget-wide v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentSize:J

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    iget-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalSize:J

    div-long/2addr v2, v4

    long-to-int v0, v2

    .line 230
    :cond_0
    const/16 v1, 0x64

    if-le v0, v1, :cond_1

    .line 231
    const/16 v0, 0x64

    .line 232
    const-string v1, "Receiver"

    const-string v2, "getProgress"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exceed ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentSize:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalSize:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_1
    return v0
.end method

.method public getSessionID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    return v0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalCount:I

    if-nez v0, :cond_0

    .line 216
    const/4 v0, 0x1

    .line 217
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalCount:I

    goto :goto_0
.end method

.method public getTranferFileName()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "tranferFile":Ljava/io/File;
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFilelist:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 197
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFilelist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getCurrentCount()I

    move-result v3

    if-le v2, v3, :cond_0

    .line 198
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFilelist:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getCurrentCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "tranferFile":Ljava/io/File;
    check-cast v0, Ljava/io/File;

    .line 199
    .restart local v0    # "tranferFile":Ljava/io/File;
    if-eqz v0, :cond_1

    .line 200
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 207
    :cond_0
    :goto_0
    return-object v1

    .line 202
    :cond_1
    const-string v2, "Receiver"

    const-string v3, "getTranferFileName"

    const-string v4, "tranferFile is null!!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getUPnPDevice()Lcom/samsung/android/allshare/file/FileReceiver;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    return-object v0
.end method

.method public isInfoListener()Z
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mInfoListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;

    if-eqz v0, :cond_0

    .line 386
    const/4 v0, 0x1

    .line 387
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMyMacAddress(Ljava/lang/String;)Z
    .locals 5
    .param p1, "macAddress"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 139
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    if-nez v1, :cond_1

    .line 151
    :cond_0
    :goto_0
    return v0

    .line 141
    :cond_1
    const-string v1, "Receiver"

    const-string v2, "isMyMacAddress"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], UPNP["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], WiFi["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v1, "Receiver"

    const-string v2, "isMyMacAddress"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Interface["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getInterfaceMacAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getAtherosMacAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getInterfaceMacAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getAtherosMacAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149
    :cond_2
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public onCancelResponseReceived(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Lcom/samsung/android/allshare/ERROR;)V
    .locals 5
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    const/16 v4, 0x3ed

    .line 506
    const-string v0, "Receiver"

    const-string v1, "onCancelResponseReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sessionID["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], getDeviceName["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ERROR: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq p3, v0, :cond_0

    .line 509
    const-string v0, "Receiver"

    const-string v1, "onCancelResponseReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    if-eq v0, v4, :cond_1

    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    const/16 v1, 0x3ef

    if-eq v0, v1, :cond_1

    .line 513
    const-string v0, "Receiver"

    const-string v1, "onCancelResponseReceived"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 515
    const/16 v0, 0x69

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileStatus(I)V

    .line 516
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 519
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    if-eqz v0, :cond_2

    .line 520
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;->onCancelled(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    :cond_2
    return-void
.end method

.method public onCompleted(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/samsung/android/allshare/ERROR;)V
    .locals 6
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;
    .param p4, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 582
    const-string v0, "Receiver"

    const-string v1, "onCompleted"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sessionID["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "File ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ERROR: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq p4, v0, :cond_0

    .line 587
    const-string v0, "Receiver"

    const-string v1, "onCompleted"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFileStatusList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentCount:I

    const/16 v2, 0x67

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 590
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentCount:I

    .line 591
    const-string v0, "Receiver"

    const-string v1, "onCompleted"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentCount:I

    iget v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalCount:I

    if-ne v0, v1, :cond_4

    .line 593
    const-string v0, "Receiver"

    const-string v1, "onCompleted"

    const-string v2, "All file completed"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    sget v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    if-lez v0, :cond_3

    .line 596
    sget v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    .line 597
    sget v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Utils;->setClientIsRunning(I)V

    .line 602
    :goto_0
    const/16 v0, 0x3ee

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 603
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    .line 607
    :cond_1
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mbReqFileStatusUpdate:Z

    .line 608
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 610
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    if-eqz v0, :cond_2

    .line 611
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;->onCompleted(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V

    .line 613
    :cond_2
    return-void

    .line 599
    :cond_3
    const-string v0, "Receiver"

    const-string v1, "onCoompleted"

    const-string v2, "Utils.mClientProperty = 0 "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 604
    :cond_4
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    const/16 v1, 0x3ec

    if-ne v0, v1, :cond_1

    .line 605
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFileStatusList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentCount:I

    const/16 v2, 0x66

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public onFailed(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/samsung/android/allshare/ERROR;)V
    .locals 6
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;
    .param p4, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    const/16 v4, 0x3ef

    .line 557
    const-string v0, "Receiver"

    const-string v1, "onFailed"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sessionID["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "File ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], Err ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p4}, Lcom/samsung/android/allshare/ERROR;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ERROR: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq p4, v0, :cond_0

    .line 560
    const-string v0, "Receiver"

    const-string v1, "onFailed"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 563
    const-string v0, "Receiver"

    const-string v1, "onFailed"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    const/16 v1, 0x3ed

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    if-eq v0, v4, :cond_1

    .line 566
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 567
    const/16 v0, 0x68

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileStatus(I)V

    .line 568
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 571
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    if-eqz v0, :cond_2

    .line 572
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;->onFailed(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V

    .line 575
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    .line 577
    :cond_3
    return-void
.end method

.method public onProgressUpdated(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;JJLjava/io/File;Lcom/samsung/android/allshare/ERROR;)V
    .locals 13
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "receivedSize"    # J
    .param p5, "totalSize"    # J
    .param p7, "file"    # Ljava/io/File;
    .param p8, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 534
    sget-object v2, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    move-object/from16 v0, p8

    if-eq v0, v2, :cond_1

    .line 535
    const-string v2, "Receiver"

    const-string v3, "onProgressUpdated"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SESSION = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ERROR = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p8

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    const/16 v2, 0x3ef

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 537
    const/16 v2, 0x68

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileStatus(I)V

    .line 538
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 552
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    const/16 v3, 0x3ed

    if-eq v2, v3, :cond_0

    .line 544
    const/16 v2, 0x3ec

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 545
    move-wide/from16 v0, p3

    iput-wide v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentSize:J

    .line 546
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 548
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    if-eqz v2, :cond_0

    .line 549
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v12

    move-object v4, p1

    move-object v5, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    invoke-interface/range {v3 .. v12}, Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;->onProgressUpdated(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;JJLjava/io/File;Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onReceiveResponseReceived(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/ERROR;)V
    .locals 6
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "err"    # Lcom/samsung/android/allshare/ERROR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/file/FileReceiver;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/ERROR;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "filelist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    const/4 v0, 0x5

    const/4 v2, 0x0

    .line 451
    const-string v1, "Receiver"

    const-string v3, "onReceiveResponseReceived"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sessionID["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], Sender name["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], getDeviceName["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ERROR: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v1

    const/16 v3, 0x3ed

    if-ne v1, v3, :cond_1

    .line 455
    const-string v1, "Receiver"

    const-string v3, "onReceiveResponseReceived"

    const-string v4, "This session is already cancelled by user"

    invoke-static {v1, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    iput-object p2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    .line 458
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/file/FileReceiver;->cancel(Ljava/lang/String;)V

    .line 459
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 461
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sending file "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getTranferFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " cancelled"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v1, v0

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 501
    :cond_0
    :goto_0
    return-void

    .line 468
    :cond_1
    sget-object v1, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq p5, v1, :cond_3

    .line 469
    const-string v1, "Receiver"

    const-string v3, "onReceiveResponseReceived"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ERROR = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    sget-object v1, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v1, p5}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 471
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mContext:Landroid/content/Context;

    const v4, 0x7f04003a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 474
    :cond_2
    const/16 v1, 0x3ef

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 475
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 477
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sending file "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getTranferFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " failed"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v1, v0

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 485
    :cond_3
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sending file "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getTranferFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " succeeded"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v1, v0

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 489
    const/16 v0, 0x3ec

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 490
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFileStatusList:Ljava/util/ArrayList;

    const/16 v1, 0x66

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 491
    iput-object p2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    .line 492
    iput v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentCount:I

    .line 493
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentSize:J

    .line 494
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 497
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFilelist:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;->onStart(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onThumbnailDispatched()V
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mInfoListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mInfoListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;->updateStatus(Z)V

    .line 401
    :cond_0
    return-void
.end method

.method public removedDevice(Lcom/samsung/android/allshare/file/FileReceiver;)Z
    .locals 4
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;

    .prologue
    .line 346
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/file/FileReceiver;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/file/FileReceiver;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    const/4 v0, 0x0

    .line 357
    :goto_0
    return v0

    .line 350
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v0

    const/16 v1, 0x3ec

    if-ne v0, v1, :cond_1

    .line 351
    const-string v0, "Receiver"

    const-string v1, "removedDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is removed in progressing."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const/16 v0, 0x3ef

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/Receiver;->setStatus(I)V

    .line 353
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSessionID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/file/FileReceiver;->cancel(Ljava/lang/String;)V

    .line 354
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateStatus()V

    .line 357
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setErrorCause(I)V
    .locals 0
    .param p1, "errorCause"    # I

    .prologue
    .line 373
    iput p1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mErrorCause:I

    .line 374
    return-void
.end method

.method public setFileList(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 125
    .local p1, "filelist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFilelist:Ljava/util/ArrayList;

    .line 126
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFileStatusList:Ljava/util/ArrayList;

    .line 127
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalCount:I

    .line 128
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalSize:J

    .line 129
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 130
    .local v0, "file":Ljava/io/File;
    iget-wide v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalSize:J

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalSize:J

    .line 131
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFileStatusList:Ljava/util/ArrayList;

    const/16 v3, 0x65

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 133
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    const-string v2, "Receiver"

    const-string v3, "setFileList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Total count = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Total Size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalSize:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    return-void
.end method

.method public setFileStatus(I)V
    .locals 3
    .param p1, "fileStatus"    # I

    .prologue
    .line 391
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mCurrentCount:I

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mTotalCount:I

    if-ge v0, v1, :cond_0

    .line 392
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFileStatusList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 391
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 394
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mbReqFileStatusUpdate:Z

    .line 395
    return-void
.end method

.method public setInfoListener(Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;

    .prologue
    .line 365
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mInfoListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;

    .line 366
    return-void
.end method

.method public setIpAddress(Ljava/lang/String;)V
    .locals 4
    .param p1, "ipAddress"    # Ljava/lang/String;

    .prologue
    .line 155
    const-string v0, "Receiver"

    const-string v1, "setIpAddress"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IP["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mP2pDevice:Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->setIpAddress(Ljava/lang/String;)V

    .line 157
    return-void
.end method

.method public setListener(Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;

    .prologue
    .line 361
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;

    .line 362
    return-void
.end method

.method public setSconnLitener(Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    .prologue
    .line 369
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    .line 370
    return-void
.end method

.method public setStatus(I)V
    .locals 0
    .param p1, "status"    # I

    .prologue
    .line 180
    iput p1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    .line 181
    return-void
.end method

.method public setUPnPDevice(Lcom/samsung/android/allshare/file/FileReceiver;)V
    .locals 4
    .param p1, "uPnPDevice"    # Lcom/samsung/android/allshare/file/FileReceiver;

    .prologue
    .line 161
    if-nez p1, :cond_0

    .line 162
    const-string v0, "Receiver"

    const-string v1, "setUPnPDevice"

    const-string v2, "UPnPDevice is null!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :goto_0
    return-void

    .line 166
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    .line 167
    const-string v0, "Receiver"

    const-string v1, "setUPnPDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Name ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/file/FileReceiver;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] ID["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/file/FileReceiver;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startReceive()V
    .locals 6

    .prologue
    .line 252
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    if-eqz v2, :cond_1

    .line 253
    const-string v2, "Receiver"

    const-string v3, "startReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ReceiverName["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFilelist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 255
    .local v0, "file":Ljava/io/File;
    const-string v2, "Receiver"

    const-string v3, "startReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "File["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 257
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mUPnPDevice:Lcom/samsung/android/allshare/file/FileReceiver;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFilelist:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mMyName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, p0, p0}, Lcom/samsung/android/allshare/file/FileReceiver;->receive(Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;)V

    .line 259
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/Outbound;->stopDelayTimer()V

    .line 260
    sget v2, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    .line 261
    sget v2, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/Utils;->setClientIsRunning(I)V

    .line 271
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    return-void

    .line 268
    :cond_1
    const-string v2, "Receiver"

    const-string v3, "startReceive"

    const-string v4, "mUPnPDeivce is null!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public startReceiveByCondition()V
    .locals 4

    .prologue
    .line 274
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mFilelist:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 275
    const-string v0, "Receiver"

    const-string v1, "startReceiveByCondition"

    const-string v2, "Filelist is not ready!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :goto_0
    return-void

    .line 279
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    const/16 v1, 0x3eb

    if-eq v0, v1, :cond_1

    .line 280
    const-string v0, "Receiver"

    const-string v1, "startReceiveByCondition"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Receiver can\'t be searched "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 284
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->startReceive()V

    goto :goto_0
.end method

.method updateInfoStatus()V
    .locals 2

    .prologue
    .line 437
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mInfoListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mInfoListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;

    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mbReqFileStatusUpdate:Z

    invoke-interface {v0, v1}, Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverInfoListener;->updateStatus(Z)V

    .line 440
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mbReqFileStatusUpdate:Z

    .line 441
    return-void
.end method

.method updateStatus()V
    .locals 4

    .prologue
    .line 407
    const-string v0, "Receiver"

    const-string v1, "updateStatus"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ReceiverName ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] mID["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] status ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;

    if-eqz v0, :cond_5

    .line 410
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    const/16 v1, 0x3ef

    if-ne v0, v1, :cond_1

    .line 411
    sget v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    if-lez v0, :cond_0

    .line 412
    sget v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    .line 413
    sget v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Utils;->setClientIsRunning(I)V

    .line 414
    const-string v0, "Receiver"

    const-string v1, "updateStatus"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Utils.mClientProperty : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :cond_0
    const-string v0, "Receiver"

    const-string v1, "UpdateStatus"

    const-string v2, "STATUS_FAILED"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    const/16 v0, 0x68

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/Receiver;->setFileStatus(I)V

    .line 421
    :cond_1
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    const/16 v1, 0x3ed

    if-ne v0, v1, :cond_4

    .line 422
    :cond_2
    sget v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    if-lez v0, :cond_3

    .line 423
    sget v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    .line 424
    sget v0, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/Utils;->setClientIsRunning(I)V

    .line 425
    const-string v0, "Receiver"

    const-string v1, "updateStatus"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Utils.mClientProperty : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/samsung/android/app/FileShareClient/Utils;->mClientProperty:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    :cond_3
    const-string v0, "Receiver"

    const-string v1, "UpdateStatus"

    const-string v2, "STATUS_ERROR or STATUS_CANCELLED"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mListener:Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;

    iget v1, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mId:I

    iget v2, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mStatus:I

    iget v3, p0, Lcom/samsung/android/app/FileShareClient/Receiver;->mErrorCause:I

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/Receiver$IReceiverListener;->updateStatus(III)V

    .line 433
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/Receiver;->updateInfoStatus()V

    .line 434
    return-void
.end method
