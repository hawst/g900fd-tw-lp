.class public Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ReceiverListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInflator:Landroid/view/LayoutInflater;

.field private mItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

.field private mThumbnailer:Lcom/samsung/android/app/FileShareClient/ThumbnailManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 33
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mInflator:Landroid/view/LayoutInflater;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mItemList:Ljava/util/ArrayList;

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/FileShareClient/Receiver;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "receiver"    # Lcom/samsung/android/app/FileShareClient/Receiver;

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mContext:Landroid/content/Context;

    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mInflator:Landroid/view/LayoutInflater;

    .line 42
    iput-object p2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mItemList:Ljava/util/ArrayList;

    .line 44
    new-instance v0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mThumbnailer:Lcom/samsung/android/app/FileShareClient/ThumbnailManager;

    .line 45
    return-void
.end method


# virtual methods
.method public addItem(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->notifyDataSetChanged()V

    .line 75
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 68
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 89
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 90
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 92
    .local v2, "path":Ljava/lang/String;
    if-nez p2, :cond_2

    .line 93
    new-instance v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;

    invoke-direct {v1}, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;-><init>()V

    .line 94
    .local v1, "holder":Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mInflator:Landroid/view/LayoutInflater;

    const v4, 0x7f03000b

    invoke-virtual {v3, v4, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 96
    const v3, 0x7f060034

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    .line 97
    const v3, 0x7f060036

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->videoThumbnailPlayView:Landroid/widget/ImageView;

    .line 99
    const v3, 0x7f060035

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->thumbnailBorder:Landroid/widget/ImageView;

    .line 100
    const v3, 0x7f060037

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileName:Landroid/widget/TextView;

    .line 102
    const v3, 0x7f060039

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileSize:Landroid/widget/TextView;

    .line 104
    const v3, 0x7f060038

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileStatus:Landroid/widget/TextView;

    .line 107
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 112
    :goto_0
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->videoThumbnailPlayView:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 113
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->thumbnailBorder:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3, p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileStatus(I)I

    move-result v3

    const/16 v4, 0x69

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3, p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileStatus(I)I

    move-result v3

    const/16 v4, 0x68

    if-ne v3, v4, :cond_3

    .line 117
    :cond_0
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f020002

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 129
    :cond_1
    :goto_1
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileName:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileSize:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 131
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;

    invoke-virtual {v3, p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->getFileStatus(I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 154
    :goto_2
    return-object p2

    .line 109
    .end local v1    # "holder":Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;

    .restart local v1    # "holder":Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;
    goto :goto_0

    .line 119
    :cond_3
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->isMediaFileType(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 120
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mThumbnailer:Lcom/samsung/android/app/FileShareClient/ThumbnailManager;

    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->getBitmap(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 121
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->thumbnailBorder:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 122
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->isVideoFileType(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 123
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->videoThumbnailPlayView:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 125
    :cond_4
    iget-object v4, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getIcon(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 133
    :pswitch_0
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileStatus:Landroid/widget/TextView;

    const v4, 0x7f040039

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 136
    :pswitch_1
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileStatus:Landroid/widget/TextView;

    const v4, 0x7f040036

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 139
    :pswitch_2
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/samsung/android/app/FileShareClient/FileManager;->getConvertSize(J)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Zero"

    if-ne v3, v4, :cond_5

    .line 140
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileSize:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 145
    :goto_3
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileStatus:Landroid/widget/TextView;

    const v4, 0x7f040038

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 142
    :cond_5
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileSize:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/samsung/android/app/FileShareClient/FileManager;->getConvertSize(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileSize:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 148
    :pswitch_3
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileStatus:Landroid/widget/TextView;

    const v4, 0x7f04002a

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 151
    :pswitch_4
    iget-object v3, v1, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter$ViewHolder;->fileStatus:Landroid/widget/TextView;

    const v4, 0x7f040037

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 131
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public removeItem(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 78
    if-ltz p1, :cond_0

    .line 79
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 81
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ReceiverListAdapter;->notifyDataSetChanged()V

    .line 83
    :cond_0
    return-void
.end method
