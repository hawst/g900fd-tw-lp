.class public final Lcom/samsung/android/app/FileShareClient/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final all_file_transfers_will_be_canceled:I = 0x7f04001b

.field public static final byteShort:I = 0x7f040047

.field public static final cancel:I = 0x7f040000

.field public static final cancel_all:I = 0x7f040014

.field public static final cancel_all_transfer:I = 0x7f040001

.field public static final cancel_connection:I = 0x7f040015

.field public static final cancel_connection_popup:I = 0x7f04001c

.field public static final cancel_connection_popup_multiple:I = 0x7f04001d

.field public static final cancel_transfer:I = 0x7f040002

.field public static final checking_files_to_send_ing:I = 0x7f040003

.field public static final clear_history:I = 0x7f040016

.field public static final connected:I = 0x7f040027

.field public static final connecting:I = 0x7f040028

.field public static final connection_failed:I = 0x7f040029

.field public static final do_not_show_again:I = 0x7f04001e

.field public static final failed:I = 0x7f04002a

.field public static final failed_to_create_vcf_file:I = 0x7f040043

.field public static final failed_to_generate_data:I = 0x7f040004

.field public static final failed_to_send_file:I = 0x7f04002b

.field public static final failed_to_send_file_toast:I = 0x7f04002c

.field public static final failed_to_send_some_files:I = 0x7f04002d

.field public static final failed_to_send_the_file:I = 0x7f04001f

.field public static final failur_reason_ps:I = 0x7f040046

.field public static final file_ps:I = 0x7f040018

.field public static final file_sending_completed:I = 0x7f04002e

.field public static final file_sending_completed_toast:I = 0x7f04002f

.field public static final file_sent_to_ps:I = 0x7f040030

.field public static final file_share_service_not_available:I = 0x7f040032

.field public static final file_size_pd:I = 0x7f040019

.field public static final file_transfer:I = 0x7f040005

.field public static final file_type_ps:I = 0x7f04001a

.field public static final files:I = 0x7f040006

.field public static final files_sent:I = 0x7f04003f

.field public static final files_sent_to_ps:I = 0x7f040031

.field public static final gigabyteShort:I = 0x7f04004a

.field public static final hotspot_already_turn_on_usc:I = 0x7f040021

.field public static final hotspot_alreay_turn_on:I = 0x7f040020

.field public static final kilobyteShort:I = 0x7f040048

.field public static final loading:I = 0x7f040040

.field public static final megabyteShort:I = 0x7f040049

.field public static final ok:I = 0x7f040007

.field public static final outbound_transfer:I = 0x7f040008

.field public static final pd_cancelled:I = 0x7f040033

.field public static final pd_failed:I = 0x7f040034

.field public static final pd_file_sent:I = 0x7f040041

.field public static final pd_more:I = 0x7f040009

.field public static final pd_sent:I = 0x7f040035

.field public static final ps_and_pd_1_more_file:I = 0x7f04000c

.field public static final ps_and_pd_more:I = 0x7f04000a

.field public static final ps_and_pd_more_files:I = 0x7f04000b

.field public static final resend_failed_files:I = 0x7f040017

.field public static final resend_failed_files_to_device:I = 0x7f040022

.field public static final resending_files:I = 0x7f040044

.field public static final retry:I = 0x7f04000d

.field public static final saved_file_name:I = 0x7f04003c

.field public static final sending:I = 0x7f040036

.field public static final sending_cancelled:I = 0x7f040037

.field public static final sending_failed_to_receiver:I = 0x7f040045

.field public static final sending_files:I = 0x7f04000e

.field public static final sending_files_to:I = 0x7f04000f

.field public static final sending_ps_ing:I = 0x7f040010

.field public static final sent:I = 0x7f040038

.field public static final stms_appgroup:I = 0x7f04003e

.field public static final stms_version:I = 0x7f04003d

.field public static final terabyteShort:I = 0x7f04004b

.field public static final to_resend_failed_files_to_ps_currently_connected_devices_will_be_disconnected:I = 0x7f040023

.field public static final total:I = 0x7f040011

.field public static final transfer_will_be_cancelled:I = 0x7f040024

.field public static final turn_on_wifi:I = 0x7f040025

.field public static final unable_to_send:I = 0x7f040042

.field public static final unable_to_share_toast:I = 0x7f04003a

.field public static final waiting_to_send_files_ing:I = 0x7f040012

.field public static final waiting_to_send_files_ing_toast:I = 0x7f040013

.field public static final wating_to_be_sent:I = 0x7f040039

.field public static final wifi_direct:I = 0x7f04003b

.field public static final your_outgoing_transfer_history_will_be_cleared:I = 0x7f040026


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
