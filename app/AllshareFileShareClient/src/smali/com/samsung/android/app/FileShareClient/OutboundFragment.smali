.class public Lcom/samsung/android/app/FileShareClient/OutboundFragment;
.super Landroid/app/Fragment;
.source "OutboundFragment.java"

# interfaces
.implements Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;
    }
.end annotation


# static fields
.field private static final TAGClass:Ljava/lang/String; = "OutboundFragments"


# instance fields
.field private mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

.field private mCurrentStateText:Ljava/lang/String;

.field private mCurrentStateTextView:Landroid/widget/TextView;

.field private mCustomAdapter:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

.field private mFilelistTextView:Landroid/widget/TextView;

.field private mIntent:Landroid/content/Intent;

.field private mListView:Landroid/widget/ListView;

.field private mListener:Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;

.field private mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

.field private mOutboundId:I

.field private mSendFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mSendLayout:Landroid/widget/LinearLayout;

.field private mTotalSizeTextView:Landroid/widget/TextView;

.field final mUiHandler:Landroid/os/Handler;

.field final mUpdateResults:Ljava/lang/Runnable;

.field private mbCompleted:Z

.field private mbGetOutbound:Z

.field private mbReadyFileList:Z

.field private mbStopForegroundService:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    .line 44
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendFileList:Ljava/util/ArrayList;

    .line 46
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mIntent:Landroid/content/Intent;

    .line 48
    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutboundId:I

    .line 52
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbCompleted:Z

    .line 54
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbGetOutbound:Z

    .line 56
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbStopForegroundService:Z

    .line 58
    iput-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbReadyFileList:Z

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCurrentStateText:Ljava/lang/String;

    .line 171
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mUiHandler:Landroid/os/Handler;

    .line 173
    new-instance v0, Lcom/samsung/android/app/FileShareClient/OutboundFragment$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment$3;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mUpdateResults:Ljava/lang/Runnable;

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbReadyFileList:Z

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    .prologue
    .line 24
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutboundId:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mListener:Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendFileList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbCompleted:Z

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCurrentStateText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCustomAdapter:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    return-object v0
.end method

.method private getOutbound()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 139
    const-string v0, "OutboundFragments"

    const-string v1, "getOutbound"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutboundId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->setListener(Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;)V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    iget v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutboundId:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/ClientService;->getOutbound(I)Lcom/samsung/android/app/FileShareClient/Outbound;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    .line 145
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    if-nez v0, :cond_1

    .line 146
    const-string v0, "OutboundFragments"

    const-string v1, "getOutbound"

    const-string v2, "getOutbound is null!!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->DeleteNotificationBar()V

    .line 148
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f040032

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 150
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 169
    :goto_0
    return-void

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/Outbound;->isComplete()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbCompleted:Z

    .line 154
    iput-boolean v4, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbReadyFileList:Z

    .line 155
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/Outbound;->getCurrentStateString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCurrentStateText:Ljava/lang/String;

    .line 156
    new-instance v0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;-><init>(Landroid/content/Context;Lcom/samsung/android/app/FileShareClient/Outbound;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCustomAdapter:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    .line 157
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCustomAdapter:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 159
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->readyFilelist()V

    .line 161
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 162
    const-string v0, "OutboundFragments"

    const-string v1, "getOutbound"

    const-string v2, "Single Mode"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbReadyFileList:Z

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mListener:Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;

    iget v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutboundId:I

    invoke-interface {v0, v1, v4}, Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;->startReceiverFragment(II)V

    .line 168
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->setListener(Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;)V

    goto :goto_0
.end method


# virtual methods
.method public Invalidate(Z)V
    .locals 3
    .param p1, "bRemoveOutbound"    # Z

    .prologue
    .line 379
    const-string v0, "OutboundFragments"

    const-string v1, "Invalidate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    if-eqz p1, :cond_0

    .line 381
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/ClientService;->destroyOutbound(I)V

    .line 385
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 387
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    if-eqz v0, :cond_1

    .line 389
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->setListener(Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;)V

    .line 392
    :cond_1
    return-void
.end method

.method public OutboundFinish()V
    .locals 3

    .prologue
    .line 397
    const-string v0, "OutboundFragments"

    const-string v1, "OutboundFinish"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 399
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->setListener(Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;)V

    .line 402
    return-void
.end method

.method public completed()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 318
    const-string v0, "OutboundFragments"

    const-string v1, "completed"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/Outbound;->getCurrentStateString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCurrentStateText:Ljava/lang/String;

    .line 320
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCurrentStateTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCurrentStateText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendFileList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mFilelistTextView:Landroid/widget/TextView;

    const v1, 0x7f040004

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mTotalSizeTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mListener:Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;

    invoke-interface {v0, v3}, Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;->notifyOutboundMenuChanged(Z)V

    .line 326
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCustomAdapter:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->dismissDialog()V

    .line 327
    iput-boolean v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbCompleted:Z

    .line 328
    return-void
.end method

.method public getCompleted()Z
    .locals 1

    .prologue
    .line 290
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbCompleted:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    const-string v0, "OutboundFragments"

    const-string v1, "onActivityCreated"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 78
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060009

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mListView:Landroid/widget/ListView;

    .line 79
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060005

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCurrentStateTextView:Landroid/widget/TextView;

    .line 80
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060007

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mTotalSizeTextView:Landroid/widget/TextView;

    .line 81
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060006

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mFilelistTextView:Landroid/widget/TextView;

    .line 82
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f060004

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendLayout:Landroid/widget/LinearLayout;

    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/app/FileShareClient/OutboundFragment$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment$1;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 95
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 96
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/samsung/android/app/FileShareClient/OutboundFragment$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment$2;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 131
    const-string v0, "OutboundFragments"

    const-string v1, "onBackPressed"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->setListener(Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;)V

    .line 135
    :cond_0
    return-void
.end method

.method public onCancelAllMenuClicked()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCustomAdapter:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->showCancelAllConfirmDialog(Lcom/samsung/android/app/FileShareClient/Outbound;)V

    .line 283
    return-void
.end method

.method public onClearHistoryMenuClicked()V
    .locals 3

    .prologue
    .line 286
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCustomAdapter:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->showClearHistoryConfirmDialog(Lcom/samsung/android/app/FileShareClient/Outbound;Lcom/samsung/android/app/FileShareClient/ClientService;)V

    .line 287
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    const v0, 0x7f030001

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 125
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 126
    const-string v0, "OutboundFragments"

    const-string v1, "onDestroy"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 119
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 120
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 113
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 114
    return-void
.end method

.method public readyFilelist()V
    .locals 10

    .prologue
    const v9, 0x7f04000e

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 333
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->getFileArrayList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendFileList:Ljava/util/ArrayList;

    .line 334
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendFileList:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 335
    const-string v1, "OutboundFragments"

    const-string v2, "readyFilelist"

    const-string v3, "mSendFileList == null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCurrentStateTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setText(I)V

    .line 337
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbCompleted:Z

    if-eqz v1, :cond_0

    .line 338
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mFilelistTextView:Landroid/widget/TextView;

    const v2, 0x7f040004

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 342
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mTotalSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 343
    iput-boolean v6, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbReadyFileList:Z

    .line 374
    :goto_1
    return-void

    .line 340
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mFilelistTextView:Landroid/widget/TextView;

    const v2, 0x7f040003

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 346
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mFilelistTextView:Landroid/widget/TextView;

    if-nez v1, :cond_2

    .line 347
    const-string v1, "OutboundFragments"

    const-string v2, "readyFilelist"

    const-string v3, "mFilelistTextView == null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 351
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendFileList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 352
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendFileList:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 353
    .local v0, "fileListText":Ljava/lang/String;
    iput-boolean v7, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbReadyFileList:Z

    .line 355
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendFileList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v7, :cond_4

    .line 356
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mFilelistTextView:Landroid/widget/TextView;

    const v2, 0x7f04000a

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v6

    iget-object v4, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendFileList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 363
    .end local v0    # "fileListText":Ljava/lang/String;
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mTotalSizeTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f040011

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Outbound;->getTotalSize()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/samsung/android/app/FileShareClient/FileManager;->getConvertSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbCompleted:Z

    if-eqz v1, :cond_5

    .line 366
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCurrentStateTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCurrentStateText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mTotalSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 368
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mListener:Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;

    invoke-interface {v1, v7}, Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;->notifyOutboundMenuChanged(Z)V

    .line 369
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCustomAdapter:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->dismissDialog()V

    goto/16 :goto_1

    .line 359
    .restart local v0    # "fileListText":Ljava/lang/String;
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mFilelistTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 371
    .end local v0    # "fileListText":Ljava/lang/String;
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mListener:Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;

    invoke-interface {v1, v6}, Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;->notifyOutboundMenuChanged(Z)V

    .line 372
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCurrentStateTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v9}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public requestFinish()V
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->setReqFinish(Z)V

    .line 279
    :cond_0
    return-void
.end method

.method public setClientService(Lcom/samsung/android/app/FileShareClient/ClientService;)V
    .locals 3
    .param p1, "clientService"    # Lcom/samsung/android/app/FileShareClient/ClientService;

    .prologue
    const/4 v2, 0x0

    .line 195
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    .line 196
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->setNewIntent(Landroid/content/Intent;)V

    .line 197
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbStopForegroundService:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 198
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->stopOutboundForeground()V

    .line 199
    iput-boolean v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbStopForegroundService:Z

    .line 202
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbGetOutbound:Z

    if-eqz v0, :cond_1

    .line 203
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getOutbound()V

    .line 204
    iput-boolean v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbGetOutbound:Z

    .line 206
    :cond_1
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 209
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mIntent:Landroid/content/Intent;

    .line 210
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 211
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.android.app.FileShareClient.SEND_RESULT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.samsung.android.app.FileShareClient.SEND_PROGRESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 212
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mIntent:Landroid/content/Intent;

    const-string v2, "outboundId"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutboundId:I

    .line 214
    iget v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutboundId:I

    if-gez v1, :cond_2

    .line 215
    const-string v1, "OutboundFragments"

    const-string v2, "setIntent"

    const-string v3, "onActivityCreated : Index is invalid"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 236
    :cond_1
    :goto_0
    return-void

    .line 220
    :cond_2
    const-string v1, "com.samsung.android.app.FileShareClient.SEND_RESULT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 221
    iput-boolean v5, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbCompleted:Z

    .line 225
    :cond_3
    const-string v1, "com.samsung.android.app.FileShareClient.SEND_RESULT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 227
    const-string v1, "OutboundFragments"

    const-string v2, "setIntent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mNotiCnt ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    sget v1, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    if-nez v1, :cond_1

    .line 229
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    if-eqz v1, :cond_4

    .line 230
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/ClientService;->stopOutboundForeground()V

    goto :goto_0

    .line 232
    :cond_4
    iput-boolean v5, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbStopForegroundService:Z

    goto :goto_0
.end method

.method public setListener(Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;)V
    .locals 0
    .param p1, "outboundFagmentListener"    # Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mListener:Lcom/samsung/android/app/FileShareClient/OutboundFragment$OutboundFagmentListener;

    .line 192
    return-void
.end method

.method public setNewIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 239
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mIntent:Landroid/content/Intent;

    .line 240
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 242
    .local v0, "action":Ljava/lang/String;
    const-string v2, "OutboundFragments"

    const-string v3, "setNewIntent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mIntent["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "outboundId"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 246
    .local v1, "outboundId":I
    const-string v2, "com.samsung.android.app.FileShareClient.SEND_RESULT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.samsung.android.app.FileShareClient.SEND_PROGRESS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 247
    :cond_0
    iget v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutboundId:I

    if-eq v1, v2, :cond_1

    .line 248
    iput v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutboundId:I

    .line 250
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    if-eqz v2, :cond_1

    .line 251
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/FileShareClient/Outbound;->setListener(Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundListener;)V

    .line 254
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    if-nez v2, :cond_3

    .line 255
    const-string v2, "OutboundFragments"

    const-string v3, "setNewIntent"

    const-string v4, "mClientService is null!!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 273
    :cond_2
    :goto_0
    return-void

    .line 259
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getOutbound()V

    .line 261
    iget-boolean v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbStopForegroundService:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 262
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mClientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/ClientService;->stopOutboundForeground()V

    .line 263
    iput-boolean v6, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbStopForegroundService:Z

    goto :goto_0

    .line 268
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->getReqFinish()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 269
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mOutbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v2, v6}, Lcom/samsung/android/app/FileShareClient/Outbound;->setReqFinish(Z)V

    .line 270
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public setStopForegroundService(Z)V
    .locals 0
    .param p1, "bStopForegroundService"    # Z

    .prologue
    .line 294
    iput-boolean p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbStopForegroundService:Z

    .line 295
    return-void
.end method

.method public updateReciver()V
    .locals 2

    .prologue
    .line 304
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/FileShareClient/OutboundFragment$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment$4;-><init>(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 312
    .local v0, "mUiThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 313
    return-void
.end method
