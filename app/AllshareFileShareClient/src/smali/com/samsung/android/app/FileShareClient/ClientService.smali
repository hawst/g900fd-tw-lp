.class public Lcom/samsung/android/app/FileShareClient/ClientService;
.super Landroid/app/Service;
.source "ClientService.java"

# interfaces
.implements Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareClient/ClientService$ClinetServiceBinder;
    }
.end annotation


# static fields
.field private static ID:I = 0x0

.field private static final STATUS_COMPLETED:Ljava/lang/String; = "STATUS_COMPLETED"

.field private static final STATUS_FAILED:Ljava/lang/String; = "STATUS_FAILED"

.field private static final STATUS_PROGRESS:Ljava/lang/String; = "STATUS_PROGRESS"

.field private static final STATUS_STARTED:Ljava/lang/String; = "STATUS_STARTED"

.field private static final TAGClass:Ljava/lang/String; = "ClientService"

.field private static mListOutbound:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/FileShareClient/Outbound;",
            ">;"
        }
    .end annotation
.end field

.field public static mNotiCnt:I


# instance fields
.field private mBinder:Landroid/os/Binder;

.field private mContext:Landroid/content/Context;

.field private final mFileShareBinder:Landroid/os/IBinder;

.field private mIdxOfOutbond:I

.field private mNotiManager:Landroid/app/NotificationManager;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

.field private final mSconnBinder:Lcom/samsung/android/sconnect/central/action/IFileShareService$Stub;

.field private mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mbForeground:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    .line 40
    const/16 v0, 0x3e8

    sput v0, Lcom/samsung/android/app/FileShareClient/ClientService;->ID:I

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 38
    iput v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mIdxOfOutbond:I

    .line 50
    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mbForeground:Z

    .line 52
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiManager:Landroid/app/NotificationManager;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mBinder:Landroid/os/Binder;

    .line 266
    new-instance v0, Lcom/samsung/android/app/FileShareClient/ClientService$ClinetServiceBinder;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/ClientService$ClinetServiceBinder;-><init>(Lcom/samsung/android/app/FileShareClient/ClientService;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mFileShareBinder:Landroid/os/IBinder;

    .line 268
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    .line 270
    new-instance v0, Lcom/samsung/android/app/FileShareClient/ClientService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/ClientService$1;-><init>(Lcom/samsung/android/app/FileShareClient/ClientService;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnBinder:Lcom/samsung/android/sconnect/central/action/IFileShareService$Stub;

    .line 321
    new-instance v0, Lcom/samsung/android/app/FileShareClient/ClientService$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/ClientService$2;-><init>(Lcom/samsung/android/app/FileShareClient/ClientService;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/FileShareClient/ClientService;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ClientService;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/FileShareClient/ClientService;Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ClientService;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/FileShareClient/ClientService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ClientService;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/ClientService;->notifyCurrentTransfer()V

    return-void
.end method

.method static synthetic access$200()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getOutboundId()I
    .locals 4

    .prologue
    .line 405
    sget v0, Lcom/samsung/android/app/FileShareClient/ClientService;->ID:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/samsung/android/app/FileShareClient/ClientService;->ID:I

    iput v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mIdxOfOutbond:I

    .line 407
    const-string v0, "ClientService"

    const-string v1, "getOutboundId"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mIdxOfOutbond:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    iget v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mIdxOfOutbond:I

    return v0
.end method

.method private getP2pDeviceInfo(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "it"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    const-string v5, "ClientService"

    const-string v6, "getP2pDeviceInfo"

    const-string v7, ""

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .local v4, "p2pDeviceInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;>;"
    const-string v5, "DevMac"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "mDeviceMac":Ljava/lang/String;
    const-string v5, "DevName"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 103
    .local v1, "mDeviceName":Ljava/lang/String;
    const-string v5, "DevType"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 104
    .local v2, "mDeviceType":Ljava/lang/String;
    const-string v5, "DevP2p"

    const/4 v6, 0x1

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 105
    .local v3, "mIsP2PConnected":Ljava/lang/Boolean;
    const-string v5, "ClientService"

    const-string v6, "getP2pDeviceInfo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "P2P Device MacAddress"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " P2P Device Name : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " P2P Device type : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " P2P connected : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    new-instance v5, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    invoke-direct {v5, v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-static {v5}, Lcom/samsung/android/app/FileShareClient/Utils;->saveP2pStatus(Z)V

    .line 111
    const-string v5, "ClientService"

    const-string v6, "getP2pDeviceInfo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    return-object v4
.end method

.method private manageProcessForeground(Z)V
    .locals 4
    .param p1, "start"    # Z

    .prologue
    .line 461
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    .line 463
    .local v1, "mAm":Landroid/app/IActivityManager;
    if-eqz v1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mBinder:Landroid/os/Binder;

    if-eqz v2, :cond_0

    .line 464
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mBinder:Landroid/os/Binder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-interface {v1, v2, v3, p1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 466
    :catch_0
    move-exception v0

    .line 467
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private notifyCurrentTransfer()V
    .locals 12

    .prologue
    .line 472
    const-string v0, "ClientService"

    const-string v1, "notifyCurrentTransfer"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    sget-object v0, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 475
    sget-object v0, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/app/FileShareClient/Outbound;

    .line 476
    .local v9, "outbound":Lcom/samsung/android/app/FileShareClient/Outbound;
    invoke-virtual {v9}, Lcom/samsung/android/app/FileShareClient/Outbound;->getReceiverList()Ljava/util/ArrayList;

    move-result-object v11

    .line 477
    .local v11, "recvList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/FileShareClient/Receiver;>;"
    if-eqz v11, :cond_2

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 478
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 479
    .local v10, "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-virtual {v10}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v0

    const/16 v1, 0x3ec

    if-ne v0, v1, :cond_1

    .line 481
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    invoke-virtual {v10}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10}, Lcom/samsung/android/app/FileShareClient/Receiver;->getP2pDeviceInfo()Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10}, Lcom/samsung/android/app/FileShareClient/Receiver;->getUPnPDevice()Lcom/samsung/android/allshare/file/FileReceiver;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/file/FileReceiver;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Lcom/samsung/android/app/FileShareClient/Receiver;->getTranferFileName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "STATUS_STARTED"

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 485
    :catch_0
    move-exception v6

    .line 486
    .local v6, "e":Landroid/os/RemoteException;
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 491
    .end local v6    # "e":Landroid/os/RemoteException;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "receiver":Lcom/samsung/android/app/FileShareClient/Receiver;
    :cond_2
    const-string v0, "ClientService"

    const-string v1, "notifyCurrentTransfer"

    const-string v2, "receiver is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 495
    .end local v9    # "outbound":Lcom/samsung/android/app/FileShareClient/Outbound;
    .end local v11    # "recvList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/FileShareClient/Receiver;>;"
    :cond_3
    const-string v0, "ClientService"

    const-string v1, "notifyCurrentTransfer"

    const-string v2, "outbound is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    :cond_4
    return-void
.end method


# virtual methods
.method public DeleteNotificationBar()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiManager:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 143
    return-void
.end method

.method public acquireWakeLock()V
    .locals 7

    .prologue
    .line 413
    const/4 v2, 0x0

    .line 415
    .local v2, "isCompCnt":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v3, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 416
    sget-object v3, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Outbound;->isComplete()Z

    move-result v3

    if-nez v3, :cond_1

    .line 417
    add-int/lit8 v2, v2, 0x1

    .line 419
    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 420
    const-string v3, "ClientService"

    const-string v4, "acquireWakeLock"

    const-string v5, "already Run!"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    :cond_0
    :goto_1
    return-void

    .line 415
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 427
    :cond_2
    :try_start_0
    const-string v3, "ClientService"

    const-string v4, "acquireWakeLock"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CompCnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v3, :cond_0

    .line 429
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 430
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 432
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 434
    :catch_0
    move-exception v0

    .line 435
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "ClientService"

    const-string v4, "acquireWakeLock"

    const-string v5, "Exception"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public createOutbound(Landroid/content/Intent;Ljava/util/ArrayList;)Lcom/samsung/android/app/FileShareClient/Outbound;
    .locals 6
    .param p1, "fileIntent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;",
            ">;)",
            "Lcom/samsung/android/app/FileShareClient/Outbound;"
        }
    .end annotation

    .prologue
    .line 160
    .local p2, "p2pDeviceInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;>;"
    const-string v2, "ClientService"

    const-string v3, "createOutbound"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/ClientService;->getOutboundId()I

    move-result v0

    .line 162
    .local v0, "id":I
    new-instance v1, Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/app/FileShareClient/Outbound;-><init>(Landroid/content/Context;I)V

    .line 163
    .local v1, "outbound":Lcom/samsung/android/app/FileShareClient/Outbound;
    invoke-virtual {v1, p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->setForegroundInterface(Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;)V

    .line 164
    invoke-virtual {v1, p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->setFileIntent(Landroid/content/Intent;)V

    .line 165
    invoke-virtual {v1, p2}, Lcom/samsung/android/app/FileShareClient/Outbound;->setP2pDeviceInfoList(Ljava/util/ArrayList;)V

    .line 166
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mSConnListener:Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->setSconnLitener(Lcom/samsung/android/app/FileShareClient/ISconnUpdateListener;)V

    .line 168
    sget-object v2, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    const-string v2, "ClientService"

    const-string v3, "createOutbound"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    return-object v1
.end method

.method public createResendOutbound(Ljava/util/ArrayList;Lcom/samsung/android/app/FileShareClient/Receiver;)Lcom/samsung/android/app/FileShareClient/Outbound;
    .locals 6
    .param p2, "r_Receiver"    # Lcom/samsung/android/app/FileShareClient/Receiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;",
            "Lcom/samsung/android/app/FileShareClient/Receiver;",
            ")",
            "Lcom/samsung/android/app/FileShareClient/Outbound;"
        }
    .end annotation

    .prologue
    .line 176
    .local p1, "resendFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/ClientService;->getOutboundId()I

    move-result v0

    .line 177
    .local v0, "id":I
    new-instance v1, Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/app/FileShareClient/Outbound;-><init>(Landroid/content/Context;I)V

    .line 178
    .local v1, "outbound":Lcom/samsung/android/app/FileShareClient/Outbound;
    invoke-virtual {v1, p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->setForegroundInterface(Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;)V

    .line 179
    invoke-virtual {v1, p2}, Lcom/samsung/android/app/FileShareClient/Outbound;->setP2pDeviceInfoListToResend(Lcom/samsung/android/app/FileShareClient/Receiver;)V

    .line 180
    invoke-virtual {v1, p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->setFailedFileIntent(Ljava/util/ArrayList;)V

    .line 181
    sget-object v2, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/Outbound;->stopDelayTimer()V

    .line 185
    const-string v2, "ClientService"

    const-string v3, "createResendOutbound"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    return-object v1
.end method

.method public destroyOutbound(I)V
    .locals 6
    .param p1, "nId"    # I

    .prologue
    .line 191
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 192
    sget-object v2, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/Outbound;

    .line 193
    .local v1, "outbound":Lcom/samsung/android/app/FileShareClient/Outbound;
    invoke-virtual {v1, p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->isId(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 194
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->destroyOutbound()V

    .line 195
    sget-object v2, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 196
    const-string v2, "ClientService"

    const-string v3, "destroyOutbound"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 199
    .end local v1    # "outbound":Lcom/samsung/android/app/FileShareClient/Outbound;
    :cond_1
    sget-object v2, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    sget v2, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    if-gtz v2, :cond_3

    .line 200
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ClientService;->stopOutboundForeground()V

    .line 202
    :cond_3
    return-void
.end method

.method public getNotiCnt()I
    .locals 1

    .prologue
    .line 217
    sget v0, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    return v0
.end method

.method public getOutbound(I)Lcom/samsung/android/app/FileShareClient/Outbound;
    .locals 6
    .param p1, "nId"    # I

    .prologue
    .line 205
    sget-object v2, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/Outbound;

    .line 206
    .local v1, "outbound":Lcom/samsung/android/app/FileShareClient/Outbound;
    invoke-virtual {v1, p1}, Lcom/samsung/android/app/FileShareClient/Outbound;->isId(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 207
    const-string v2, "ClientService"

    const-string v3, "getOutbound"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-virtual {v1, p0}, Lcom/samsung/android/app/FileShareClient/Outbound;->setForegroundInterface(Lcom/samsung/android/app/FileShareClient/Outbound$IOutboundForeground;)V

    .line 213
    .end local v1    # "outbound":Lcom/samsung/android/app/FileShareClient/Outbound;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 119
    const-string v0, "ClientService"

    const-string v1, "onBind"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mFileShareBinder:Landroid/os/IBinder;

    check-cast v0, Landroid/os/Binder;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mBinder:Landroid/os/Binder;

    .line 122
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCONNECT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnBinder:Lcom/samsung/android/sconnect/central/action/IFileShareService$Stub;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mBinder:Landroid/os/Binder;

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mBinder:Landroid/os/Binder;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 63
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiManager:Landroid/app/NotificationManager;

    .line 64
    const-string v0, "ClientService"

    const-string v1, "onCreate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 148
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 149
    const-string v0, "ClientService"

    const-string v1, "onDestroy"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 131
    const-string v0, "ClientService"

    const-string v1, "onRebind"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 133
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 70
    if-eqz p1, :cond_3

    .line 71
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "action":Ljava/lang/String;
    const-string v3, "ClientService"

    const-string v4, "onStartCommand "

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    if-nez v0, :cond_1

    .line 75
    const-string v3, "ClientService"

    const-string v4, "OnstartCommand"

    const-string v5, "action = null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ClientService;->stopSelf()V

    .line 93
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v3, 0x1

    return v3

    .line 78
    .restart local v0    # "action":Ljava/lang/String;
    :cond_1
    const-string v3, "android.intent.action.SCONNECT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 79
    const-string v3, "ClientService"

    const-string v4, "onStartCommand "

    const-string v5, "action : android.intent.action.SCONNECT"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/ClientService;->getP2pDeviceInfo(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v2

    .line 81
    .local v2, "p2pDeviceInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;>;"
    invoke-virtual {p0, p1, v2}, Lcom/samsung/android/app/FileShareClient/ClientService;->createOutbound(Landroid/content/Intent;Ljava/util/ArrayList;)Lcom/samsung/android/app/FileShareClient/Outbound;

    .line 84
    .end local v2    # "p2pDeviceInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/FileShareClient/P2pDeviceInfo;>;"
    :cond_2
    const-string v3, "com.samsung.android.app.FileShareClient.clearNotification"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 85
    sget v3, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    add-int/lit8 v3, v3, -0x1

    sput v3, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    .line 86
    const-string v3, "outboundId"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 87
    .local v1, "outboundId":I
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/FileShareClient/ClientService;->destroyOutbound(I)V

    goto :goto_0

    .line 91
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "outboundId":I
    :cond_3
    const-string v3, "ClientService"

    const-string v4, "onStartCommand"

    const-string v5, "intent is null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 137
    const-string v0, "ClientService"

    const-string v1, "onUnbind"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public releaseWakeLock()V
    .locals 6

    .prologue
    .line 442
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 443
    sget-object v2, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->isComplete()Z

    move-result v2

    if-nez v2, :cond_1

    .line 444
    const-string v3, "ClientService"

    const-string v4, "releaseWakeLock"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "There is ongoing Outbound.["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "]"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v2, Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->isComplete()Z

    move-result v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    :cond_0
    :goto_1
    return-void

    .line 442
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 451
    :cond_2
    :try_start_0
    const-string v2, "ClientService"

    const-string v3, "releaseWakeLock"

    const-string v4, "release Wake lock"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 453
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 455
    :catch_0
    move-exception v0

    .line 456
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "ClientService"

    const-string v3, "releaseWakeLock"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public setNotiCnt(I)V
    .locals 0
    .param p1, "notiCnt"    # I

    .prologue
    .line 221
    sput p1, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    .line 222
    return-void
.end method

.method public startOutboundForeground()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 229
    iget-boolean v1, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mbForeground:Z

    if-ne v1, v5, :cond_0

    .line 248
    :goto_0
    return-void

    .line 234
    :cond_0
    :try_start_0
    iput-object p0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mContext:Landroid/content/Context;

    .line 236
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mPowerManager:Landroid/os/PowerManager;

    if-nez v1, :cond_1

    .line 237
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mPowerManager:Landroid/os/PowerManager;

    .line 238
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mPowerManager:Landroid/os/PowerManager;

    const/4 v2, 0x1

    const-string v3, "ClientService Wake Lock"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    :cond_1
    :goto_1
    const-string v1, "ClientService"

    const-string v2, "startOutboundForeground"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mNotiCnt["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    invoke-direct {p0, v5}, Lcom/samsung/android/app/FileShareClient/ClientService;->manageProcessForeground(Z)V

    .line 247
    iput-boolean v5, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mbForeground:Z

    goto :goto_0

    .line 242
    :catch_0
    move-exception v0

    .line 243
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ClientService"

    const-string v2, "startOutboundForeground"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public stopOutboundForeground()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 252
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mbForeground:Z

    if-nez v0, :cond_0

    .line 260
    :goto_0
    return-void

    .line 256
    :cond_0
    const-string v0, "ClientService"

    const-string v1, "stopOutboundForeground()"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mNotiCnt["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    invoke-direct {p0, v4}, Lcom/samsung/android/app/FileShareClient/ClientService;->manageProcessForeground(Z)V

    .line 258
    invoke-virtual {p0}, Lcom/samsung/android/app/FileShareClient/ClientService;->releaseWakeLock()V

    .line 259
    iput-boolean v4, p0, Lcom/samsung/android/app/FileShareClient/ClientService;->mbForeground:Z

    goto :goto_0
.end method
