.class Lcom/samsung/android/app/FileShareClient/OutboundFragment$2;
.super Ljava/lang/Object;
.source "OutboundFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/FileShareClient/OutboundFragment;->onActivityCreated(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/OutboundFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment$2;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment$2;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbReadyFileList:Z
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->access$000(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment$2;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCustomAdapter:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->access$600(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment$2;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mSendFileList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->access$300(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment$2;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mbCompleted:Z
    invoke-static {v2}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->access$400(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)Z

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/OutboundFragment$2;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundFragment;

    # getter for: Lcom/samsung/android/app/FileShareClient/OutboundFragment;->mCurrentStateText:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/FileShareClient/OutboundFragment;->access$500(Lcom/samsung/android/app/FileShareClient/OutboundFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->showSendFileListDialog(Ljava/util/ArrayList;ZLjava/lang/String;)V

    .line 105
    :cond_0
    return-void
.end method
