.class Lcom/samsung/android/app/FileShareClient/Outbound$17;
.super Ljava/lang/Object;
.source "Outbound.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/FileShareClient/Outbound;->removeConnection()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/Outbound;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/Outbound;)V
    .locals 0

    .prologue
    .line 1659
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/Outbound$17;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 4
    .param p1, "reason"    # I

    .prologue
    .line 1669
    const-string v0, "Outbound"

    const-string v1, "removeConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " removeGroup fail "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1670
    return-void
.end method

.method public onSuccess()V
    .locals 3

    .prologue
    .line 1662
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/Outbound$17;->this$0:Lcom/samsung/android/app/FileShareClient/Outbound;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/FileShareClient/Outbound;->mStartTimer:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/FileShareClient/Outbound;->access$3102(Lcom/samsung/android/app/FileShareClient/Outbound;Z)Z

    .line 1663
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/Utils;->resetP2pStatus()V

    .line 1664
    const-string v0, "Outbound"

    const-string v1, "removeConnection"

    const-string v2, " removeGroup success"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1665
    return-void
.end method
