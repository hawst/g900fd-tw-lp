.class Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;
.super Ljava/lang/Object;
.source "OutboundListAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->showClearHistoryConfirmDialog(Lcom/samsung/android/app/FileShareClient/Outbound;Lcom/samsung/android/app/FileShareClient/ClientService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

.field final synthetic val$clientService:Lcom/samsung/android/app/FileShareClient/ClientService;

.field final synthetic val$outbound:Lcom/samsung/android/app/FileShareClient/Outbound;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;Lcom/samsung/android/app/FileShareClient/Outbound;Lcom/samsung/android/app/FileShareClient/ClientService;)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    iput-object p2, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;->val$outbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    iput-object p3, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;->val$clientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 328
    const-string v1, "OutboundListAdapter"

    const-string v2, "showClearHistoryConfirmDialog"

    const-string v3, "Clear History"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    # getter for: Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->access$100(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 332
    .local v0, "notiMgr":Landroid/app/NotificationManager;
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;->val$outbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    if-eqz v1, :cond_0

    .line 333
    const-string v1, "FileShareClient"

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;->val$outbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->getId()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 334
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;->val$clientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    if-eqz v1, :cond_0

    .line 335
    sget v1, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/samsung/android/app/FileShareClient/ClientService;->mNotiCnt:I

    .line 336
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;->val$clientService:Lcom/samsung/android/app/FileShareClient/ClientService;

    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;->val$outbound:Lcom/samsung/android/app/FileShareClient/Outbound;

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/FileShareClient/ClientService;->destroyOutbound(I)V

    .line 339
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter$9;->this$0:Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;

    # getter for: Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;->access$100(Lcom/samsung/android/app/FileShareClient/OutboundListAdapter;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 340
    return-void
.end method
