.class public Lcom/samsung/android/app/FileShareClient/ThumbnailManager;
.super Ljava/lang/Object;
.source "ThumbnailManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareClient/ThumbnailManager$ThumbnailDrawable;,
        Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;
    }
.end annotation


# static fields
.field private static final DELAY_BEFORE_PURGE:I = 0x2710

.field private static final GETTHUMBNAILTIME:I = 0xe4e1c0

.field private static final MICRO_HEIGH:I = 0x68

.field private static final MICRO_WIDTH:I = 0x68

.field private static final TAGClass:Ljava/lang/String; = "ThumbnailManager"

.field private static final THUMBNAIL_BUFFER_MAX_SIZE:I = 0xc8

.field private static final THUMBNAIL_BUFFER_MIN_SIZE:I = 0x64


# instance fields
.field private mCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mThumbnailBuffer:Lcom/samsung/android/app/FileShareClient/ThumbnailBuffer;

.field private final purgeHandler:Landroid/os/Handler;

.field private final purger:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mCache:Ljava/util/HashMap;

    .line 65
    new-instance v0, Lcom/samsung/android/app/FileShareClient/ThumbnailBuffer;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mCache:Ljava/util/HashMap;

    const/16 v2, 0xc8

    const/16 v3, 0x64

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/FileShareClient/ThumbnailBuffer;-><init>(Ljava/util/Map;II)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mThumbnailBuffer:Lcom/samsung/android/app/FileShareClient/ThumbnailBuffer;

    .line 69
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->purgeHandler:Landroid/os/Handler;

    .line 71
    new-instance v0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$1;-><init>(Lcom/samsung/android/app/FileShareClient/ThumbnailManager;)V

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->purger:Ljava/lang/Runnable;

    .line 78
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mContext:Landroid/content/Context;

    .line 79
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/FileShareClient/ThumbnailManager;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ThumbnailManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->getThumbnailBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/FileShareClient/ThumbnailManager;)Lcom/samsung/android/app/FileShareClient/ThumbnailBuffer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/ThumbnailManager;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mThumbnailBuffer:Lcom/samsung/android/app/FileShareClient/ThumbnailBuffer;

    return-object v0
.end method

.method static synthetic access$200(Landroid/widget/ImageView;)Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;
    .locals 1
    .param p0, "x0"    # Landroid/widget/ImageView;

    .prologue
    .line 41
    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->getBitmapGetterTask(Landroid/widget/ImageView;)Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;

    move-result-object v0

    return-object v0
.end method

.method private static cancelPotentialGet(Ljava/lang/String;Landroid/widget/ImageView;)Z
    .locals 4
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "imageView"    # Landroid/widget/ImageView;

    .prologue
    const/4 v2, 0x1

    .line 165
    invoke-static {p1}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->getBitmapGetterTask(Landroid/widget/ImageView;)Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;

    move-result-object v0

    .line 167
    .local v0, "bitmapGetterTask":Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;
    if-eqz v0, :cond_1

    .line 168
    # getter for: Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;->path:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;->access$300(Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;)Ljava/lang/String;

    move-result-object v1

    .line 169
    .local v1, "bitmapPath":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 170
    :cond_0
    invoke-virtual {v0, v2}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;->cancel(Z)Z

    .line 175
    .end local v1    # "bitmapPath":Ljava/lang/String;
    :cond_1
    :goto_0
    return v2

    .line 172
    .restart local v1    # "bitmapPath":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private forceGetThumbnail(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 146
    if-nez p1, :cond_1

    .line 147
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    invoke-static {p1, p2}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->cancelPotentialGet(Ljava/lang/String;Landroid/widget/ImageView;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 152
    new-instance v0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;-><init>(Lcom/samsung/android/app/FileShareClient/ThumbnailManager;Landroid/widget/ImageView;)V

    .line 153
    .local v0, "task":Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;
    new-instance v1, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$ThumbnailDrawable;

    invoke-direct {v1, v0}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$ThumbnailDrawable;-><init>(Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;)V

    .line 154
    .local v1, "thumbnailDrawable":Lcom/samsung/android/app/FileShareClient/ThumbnailManager$ThumbnailDrawable;
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 155
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private static getBitmapGetterTask(Landroid/widget/ImageView;)Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;
    .locals 3
    .param p0, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 179
    if-eqz p0, :cond_0

    .line 180
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 181
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    instance-of v2, v0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$ThumbnailDrawable;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 182
    check-cast v1, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$ThumbnailDrawable;

    .line 183
    .local v1, "thumbnailDrawable":Lcom/samsung/android/app/FileShareClient/ThumbnailManager$ThumbnailDrawable;
    invoke-virtual {v1}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager$ThumbnailDrawable;->getBitmapGatterTask()Lcom/samsung/android/app/FileShareClient/ThumbnailManager$BitmapGetterTask;

    move-result-object v2

    .line 186
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v1    # "thumbnailDrawable":Lcom/samsung/android/app/FileShareClient/ThumbnailManager$ThumbnailDrawable;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getExifOrientation(Ljava/lang/String;)I
    .locals 9
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v8, -0x1

    .line 272
    const/4 v0, 0x0

    .line 273
    .local v0, "degree":I
    const/4 v2, 0x0

    .line 275
    .local v2, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    move-object v2, v3

    .line 279
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_0
    if-eqz v2, :cond_0

    .line 280
    const-string v5, "Orientation"

    invoke-virtual {v2, v5, v8}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    .line 281
    .local v4, "orientation":I
    if-eq v4, v8, :cond_0

    .line 283
    packed-switch v4, :pswitch_data_0

    .line 298
    .end local v4    # "orientation":I
    :cond_0
    :goto_1
    :pswitch_0
    return v0

    .line 276
    :catch_0
    move-exception v1

    .line 277
    .local v1, "ex":Ljava/io/IOException;
    const-string v5, "ThumbnailManager"

    const-string v6, "getExifOrientation"

    const-string v7, "cannot read exif"

    invoke-static {v5, v6, v7, v1}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 285
    .end local v1    # "ex":Ljava/io/IOException;
    .restart local v4    # "orientation":I
    :pswitch_1
    const/16 v0, 0x5a

    .line 286
    goto :goto_1

    .line 288
    :pswitch_2
    const/16 v0, 0xb4

    .line 289
    goto :goto_1

    .line 291
    :pswitch_3
    const/16 v0, 0x10e

    .line 292
    goto :goto_1

    .line 283
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getImageBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 208
    const/4 v6, 0x0

    .line 209
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    .line 211
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 213
    if-eqz v7, :cond_2

    .line 214
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 216
    .local v10, "id":J
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-static {v0, v10, v11, v3, v4}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 218
    if-eqz v6, :cond_1

    .line 219
    const-string v0, "orientation"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 220
    .local v12, "index2":I
    invoke-interface {v7, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 222
    .local v8, "degree":I
    if-nez v8, :cond_0

    .line 223
    invoke-static {p1}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->getExifOrientation(Ljava/lang/String;)I

    move-result v8

    .line 225
    :cond_0
    if-eqz v8, :cond_1

    .line 226
    invoke-static {v6, v8}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 229
    .end local v8    # "degree":I
    .end local v10    # "id":J
    .end local v12    # "index2":I
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    :cond_2
    if-eqz v7, :cond_3

    .line 236
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 241
    :cond_3
    :goto_0
    if-nez v6, :cond_4

    .line 242
    const/4 v2, 0x0

    .line 243
    .local v2, "tempB":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 246
    .local v1, "m":Landroid/graphics/Matrix;
    const/16 v0, 0x100

    :try_start_2
    invoke-direct {p0, v0, p1}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->getSampleSizeBitmap(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 247
    const/16 v3, 0x40

    const/16 v4, 0x40

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->transform(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 248
    if-eqz v2, :cond_4

    .line 249
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 256
    .end local v1    # "m":Landroid/graphics/Matrix;
    .end local v2    # "tempB":Landroid/graphics/Bitmap;
    :cond_4
    :goto_1
    return-object v6

    .line 231
    :catch_0
    move-exception v9

    .line 232
    .local v9, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 235
    if-eqz v7, :cond_3

    .line 236
    :try_start_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 237
    :catch_1
    move-exception v0

    goto :goto_0

    .line 234
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    .line 235
    if-eqz v7, :cond_5

    .line 236
    :try_start_5
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 238
    :cond_5
    :goto_2
    throw v0

    .line 250
    .restart local v1    # "m":Landroid/graphics/Matrix;
    .restart local v2    # "tempB":Landroid/graphics/Bitmap;
    :catch_2
    move-exception v9

    .line 251
    .restart local v9    # "e":Ljava/lang/Exception;
    if-eqz v2, :cond_6

    .line 252
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 253
    :cond_6
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 237
    .end local v1    # "m":Landroid/graphics/Matrix;
    .end local v2    # "tempB":Landroid/graphics/Bitmap;
    .end local v9    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v0

    goto :goto_0

    :catch_4
    move-exception v3

    goto :goto_2
.end method

.method private getSampleSizeBitmap(ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "targetWidthHeight"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    .line 379
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 381
    .local v6, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v9, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 382
    invoke-static {p2, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 383
    .local v0, "bm":Landroid/graphics/Bitmap;
    const/4 v8, 0x0

    iput-boolean v8, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 385
    const/4 v8, -0x1

    if-eq p1, v8, :cond_3

    .line 387
    iget v7, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 388
    .local v7, "w":I
    iget v5, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 389
    .local v5, "h":I
    div-int v3, v7, p1

    .line 390
    .local v3, "candidateW":I
    div-int v2, v5, p1

    .line 391
    .local v2, "candidateH":I
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 392
    .local v1, "candidate":I
    if-nez v1, :cond_0

    .line 393
    const/4 v1, 0x1

    .line 395
    :cond_0
    if-le v1, v9, :cond_1

    .line 396
    if-le v7, p1, :cond_1

    div-int v8, v7, v1

    if-ge v8, p1, :cond_1

    .line 397
    add-int/lit8 v1, v1, -0x1

    .line 400
    :cond_1
    if-le v1, v9, :cond_2

    .line 401
    if-le v5, p1, :cond_2

    div-int v8, v5, v1

    if-ge v8, p1, :cond_2

    .line 402
    add-int/lit8 v1, v1, -0x1

    .line 405
    :cond_2
    iput v1, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 408
    .end local v1    # "candidate":I
    .end local v2    # "candidateH":I
    .end local v3    # "candidateW":I
    .end local v5    # "h":I
    .end local v7    # "w":I
    :cond_3
    invoke-static {p2, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 410
    const/4 v4, 0x0

    .line 411
    .local v4, "degree":I
    invoke-static {p2}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->getExifOrientation(Ljava/lang/String;)I

    move-result v4

    .line 413
    if-eqz v4, :cond_4

    .line 414
    invoke-static {v0, v4}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 416
    :cond_4
    return-object v0
.end method

.method private getThumbnailBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 190
    const/4 v0, 0x0

    .line 192
    .local v0, "b":Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->isImageFileType(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 193
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->getImageBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 198
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 199
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {p1, v3}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getIcon(Ljava/lang/String;Landroid/content/ContentResolver;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 201
    .local v1, "tempDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 204
    .end local v1    # "tempDrawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_1
    return-object v0

    .line 194
    :cond_2
    invoke-static {p1}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->isVideoFileType(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 195
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->getVideoBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private getVideoBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 260
    const/4 v0, 0x0

    .line 262
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 263
    .local v2, "file":Ljava/io/File;
    invoke-direct {p0, v2}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->getVideoThumbBitmap(Ljava/io/File;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 268
    .end local v2    # "file":Ljava/io/File;
    :goto_0
    return-object v0

    .line 265
    :catch_0
    move-exception v1

    .line 266
    .local v1, "ex":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method private getVideoThumbBitmap(Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "mFile"    # Ljava/io/File;

    .prologue
    const/high16 v8, 0x42d00000    # 104.0f

    .line 302
    new-instance v6, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v6}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 303
    .local v6, "retriever":Landroid/media/MediaMetadataRetriever;
    const/4 v2, 0x0

    .line 305
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 306
    const-wide/32 v4, 0xe4e1c0

    invoke-virtual {v6, v4, v5}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 307
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 308
    const/4 v7, 0x0

    .line 309
    .local v7, "scale":F
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 310
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v7, v8, v0

    .line 314
    :goto_0
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 315
    .local v1, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v1, v7, v7}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 316
    const/16 v3, 0x68

    const/16 v4, 0x68

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->transform(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 319
    if-eqz v6, :cond_0

    .line 320
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 323
    .end local v1    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "scale":F
    :cond_0
    :goto_1
    return-object v0

    .line 312
    .restart local v7    # "scale":F
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    int-to-float v0, v0

    div-float v7, v8, v0

    goto :goto_0

    .line 317
    .end local v7    # "scale":F
    :catch_0
    move-exception v0

    .line 319
    if-eqz v6, :cond_2

    .line 320
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V

    :cond_2
    move-object v0, v2

    .line 323
    goto :goto_1

    .line 319
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 320
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V

    :cond_3
    throw v0
.end method

.method private resetPurgeTimer()V
    .locals 4

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->purgeHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->purger:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 161
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->purgeHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->purger:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 162
    return-void
.end method

.method private static rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "b"    # Landroid/graphics/Bitmap;
    .param p1, "degrees"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 361
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 362
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 363
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 365
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 366
    .local v7, "b2":Landroid/graphics/Bitmap;
    invoke-virtual {p0, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    move-object p0, v7

    .line 375
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v7    # "b2":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p0

    .line 370
    .restart local v5    # "m":Landroid/graphics/Matrix;
    :catch_0
    move-exception v8

    .line 372
    .local v8, "ex":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v8}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method private transform(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "scaler"    # Landroid/graphics/Matrix;
    .param p2, "source"    # Landroid/graphics/Bitmap;
    .param p3, "targetWidth"    # I
    .param p4, "targetHeight"    # I
    .param p5, "scaleUp"    # Z

    .prologue
    .line 328
    if-nez p2, :cond_1

    .line 329
    const/4 v10, 0x0

    .line 357
    :cond_0
    :goto_0
    return-object v10

    .line 332
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 333
    move/from16 v0, p3

    int-to-float v2, v0

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v13, v2, v3

    .line 339
    .local v13, "scale":F
    :goto_1
    if-eqz p1, :cond_3

    .line 340
    invoke-virtual {p1, v13, v13}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 342
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    move-object/from16 v2, p2

    move-object v7, p1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 349
    .local v9, "b1":Landroid/graphics/Bitmap;
    :goto_2
    const/4 v2, 0x0

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int v3, v3, p3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 350
    .local v11, "dx1":I
    const/4 v2, 0x0

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int v3, v3, p4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 352
    .local v12, "dy1":I
    div-int/lit8 v2, v11, 0x2

    div-int/lit8 v3, v12, 0x2

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v9, v2, v3, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 354
    .local v10, "b2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p2

    invoke-virtual {v9, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 355
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 335
    .end local v9    # "b1":Landroid/graphics/Bitmap;
    .end local v10    # "b2":Landroid/graphics/Bitmap;
    .end local v11    # "dx1":I
    .end local v12    # "dy1":I
    .end local v13    # "scale":F
    :cond_2
    move/from16 v0, p4

    int-to-float v2, v0

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v13, v2, v3

    .restart local v13    # "scale":F
    goto :goto_1

    .line 345
    :cond_3
    move-object/from16 v9, p2

    .restart local v9    # "b1":Landroid/graphics/Bitmap;
    goto :goto_2
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mThumbnailBuffer:Lcom/samsung/android/app/FileShareClient/ThumbnailBuffer;

    invoke-virtual {v0}, Lcom/samsung/android/app/FileShareClient/ThumbnailBuffer;->clear()V

    .line 97
    return-void
.end method

.method public getBitmap(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->resetPurgeTimer()V

    .line 84
    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->mThumbnailBuffer:Lcom/samsung/android/app/FileShareClient/ThumbnailBuffer;

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/FileShareClient/ThumbnailBuffer;->get(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 86
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 87
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->forceGetThumbnail(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 93
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-static {p1, p2}, Lcom/samsung/android/app/FileShareClient/ThumbnailManager;->cancelPotentialGet(Ljava/lang/String;Landroid/widget/ImageView;)Z

    .line 90
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
