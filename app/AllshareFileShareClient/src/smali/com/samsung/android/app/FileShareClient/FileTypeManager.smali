.class public Lcom/samsung/android/app/FileShareClient/FileTypeManager;
.super Ljava/lang/Object;
.source "FileTypeManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;
    }
.end annotation


# static fields
.field private static final FILE_ICON_DEFAULT:I = 0x7f020001

.field private static final FILE_TYPE_3G2_AUDIO:I = 0x12

.field private static final FILE_TYPE_3GA:I = 0x9

.field private static final FILE_TYPE_3GPP:I = 0x21

.field private static final FILE_TYPE_3GPP2:I = 0x22

.field private static final FILE_TYPE_3GPP_AUDIO:I = 0x14

.field private static final FILE_TYPE_3GP_AUDIO:I = 0x11

.field private static final FILE_TYPE_AAC:I = 0x8

.field private static final FILE_TYPE_AK3G:I = 0x2e

.field private static final FILE_TYPE_AMR:I = 0x4

.field private static final FILE_TYPE_APK:I = 0x64

.field private static final FILE_TYPE_ASC:I = 0x4e

.field private static final FILE_TYPE_ASF:I = 0x25

.field private static final FILE_TYPE_ASF_AUDIO:I = 0x13

.field private static final FILE_TYPE_AVI:I = 0x26

.field private static final FILE_TYPE_AWB:I = 0x5

.field private static final FILE_TYPE_BMP:I = 0x40

.field private static final FILE_TYPE_CSV:I = 0x50

.field private static final FILE_TYPE_DCF:I = 0x57

.field private static final FILE_TYPE_DIVX:I = 0x27

.field private static final FILE_TYPE_DOC:I = 0x52

.field private static final FILE_TYPE_EBOOK:I = 0x59

.field private static final FILE_TYPE_EML:I = 0x8e

.field private static final FILE_TYPE_FLAC:I = 0xa

.field private static final FILE_TYPE_FLV:I = 0x28

.field private static final FILE_TYPE_GIF:I = 0x3e

.field private static final FILE_TYPE_GUL:I = 0x56

.field private static final FILE_TYPE_HTML:I = 0x7e

.field private static final FILE_TYPE_HWP:I = 0x8d

.field private static final FILE_TYPE_IMY:I = 0x18

.field private static final FILE_TYPE_ISMA:I = 0xd

.field private static final FILE_TYPE_ISMV:I = 0x33

.field private static final FILE_TYPE_JAD:I = 0x6e

.field private static final FILE_TYPE_JAR:I = 0x6f

.field private static final FILE_TYPE_JPEG:I = 0x3d

.field private static final FILE_TYPE_K3G:I = 0x2d

.field private static final FILE_TYPE_M3U:I = 0x47

.field private static final FILE_TYPE_M4A:I = 0x2

.field private static final FILE_TYPE_M4B:I = 0xb

.field private static final FILE_TYPE_M4V:I = 0x20

.field private static final FILE_TYPE_MEMO:I = 0x96

.field private static final FILE_TYPE_MID:I = 0x16

.field private static final FILE_TYPE_MKV:I = 0x29

.field private static final FILE_TYPE_MOV:I = 0x2a

.field private static final FILE_TYPE_MP3:I = 0x1

.field private static final FILE_TYPE_MP4:I = 0x1f

.field private static final FILE_TYPE_MP4_AUDIO:I = 0x10

.field private static final FILE_TYPE_MPG:I = 0x24

.field private static final FILE_TYPE_ODF:I = 0x58

.field private static final FILE_TYPE_OGG:I = 0x7

.field private static final FILE_TYPE_PDF:I = 0x51

.field private static final FILE_TYPE_PLS:I = 0x48

.field private static final FILE_TYPE_PNG:I = 0x3f

.field private static final FILE_TYPE_PPS:I = 0x4f

.field private static final FILE_TYPE_PPT:I = 0x54

.field private static final FILE_TYPE_PYA:I = 0xc

.field private static final FILE_TYPE_PYV:I = 0x2b

.field private static final FILE_TYPE_QCP:I = 0x15

.field private static final FILE_TYPE_RM:I = 0x30

.field private static final FILE_TYPE_RMVB:I = 0x31

.field private static final FILE_TYPE_SASF:I = 0x91

.field private static final FILE_TYPE_SCC:I = 0x95

.field private static final FILE_TYPE_SDP:I = 0x32

.field private static final FILE_TYPE_SKM:I = 0x2c

.field private static final FILE_TYPE_SMF:I = 0x17

.field private static final FILE_TYPE_SNB:I = 0x90

.field private static final FILE_TYPE_SPD:I = 0x94

.field private static final FILE_TYPE_SSF:I = 0x92

.field private static final FILE_TYPE_SVG:I = 0x5b

.field private static final FILE_TYPE_SWF:I = 0x5a

.field private static final FILE_TYPE_TRC:I = 0x93

.field private static final FILE_TYPE_TXT:I = 0x55

.field private static final FILE_TYPE_VCF:I = 0x79

.field private static final FILE_TYPE_VCS:I = 0x78

.field private static final FILE_TYPE_VNT:I = 0x7a

.field private static final FILE_TYPE_VTS:I = 0x7b

.field private static final FILE_TYPE_WAV:I = 0x3

.field private static final FILE_TYPE_WBMP:I = 0x41

.field private static final FILE_TYPE_WEBM:I = 0x2f

.field private static final FILE_TYPE_WGT:I = 0x65

.field private static final FILE_TYPE_WMA:I = 0x6

.field private static final FILE_TYPE_WMV:I = 0x23

.field private static final FILE_TYPE_WPL:I = 0x49

.field private static final FILE_TYPE_XHTML:I = 0x80

.field private static final FILE_TYPE_XLS:I = 0x53

.field private static final FILE_TYPE_XML:I = 0x7f

.field private static final FILE_TYPE_ZIP:I = 0x8f

.field private static final FIRST_IMAGE_FILE_TYPE:I = 0x3d

.field private static final FIRST_VIDEO_FILE_TYPE:I = 0x1f

.field private static final LAST_IMAGE_FILE_TYPE:I = 0x41

.field private static final LAST_VIDEO_FILE_TYPE:I = 0x33

.field private static final TAGClass:Ljava/lang/String; = "FileTypeManager"

.field private static sFileTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;",
            ">;"
        }
    .end annotation
.end field

.field private static sMimeType:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sMimeTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x16

    const v7, 0x7f020001

    const v6, 0x7f020008

    const/high16 v5, 0x7f020000

    const v4, 0x7f020004

    .line 218
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->sFileTypeMap:Ljava/util/HashMap;

    .line 220
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->sMimeTypeMap:Ljava/util/HashMap;

    .line 222
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->sMimeType:Ljava/util/HashMap;

    .line 231
    const-string v0, "EML"

    const/16 v1, 0x8e

    const-string v2, "message/rfc822"

    const-string v3, "EML"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 232
    const-string v0, "MP3"

    const/4 v1, 0x1

    const-string v2, "audio/mpeg"

    const-string v3, "Mpeg"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 233
    const-string v0, "M4A"

    const/4 v1, 0x2

    const-string v2, "audio/mp4"

    const-string v3, "M4A"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 234
    const-string v0, "WAV"

    const/4 v1, 0x3

    const-string v2, "audio/x-wav"

    const-string v3, "WAVE"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 235
    const-string v0, "AMR"

    const/4 v1, 0x4

    const-string v2, "audio/amr"

    const-string v3, "AMR"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 236
    const-string v0, "AWB"

    const/4 v1, 0x5

    const-string v2, "audio/amr-wb"

    const-string v3, "AWB"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 237
    const-string v0, "WMA"

    const/4 v1, 0x6

    const-string v2, "audio/x-ms-wma"

    const-string v3, "WMA"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 238
    const-string v0, "OGG"

    const/4 v1, 0x7

    const-string v2, "audio/ogg"

    const-string v3, "OGG"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 239
    const-string v0, "OGA"

    const/4 v1, 0x7

    const-string v2, "audio/ogg"

    const-string v3, "OGA"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 240
    const-string v0, "AAC"

    const/16 v1, 0x8

    const-string v2, "audio/aac"

    const-string v3, "AAC"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 241
    const-string v0, "3GA"

    const/16 v1, 0x9

    const-string v2, "audio/3gpp"

    const-string v3, "3GA"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 242
    const-string v0, "FLAC"

    const/16 v1, 0xa

    const-string v2, "audio/flac"

    const-string v3, "FLAC"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 243
    const-string v0, "MPGA"

    const/4 v1, 0x1

    const-string v2, "audio/mpeg"

    const-string v3, "MPGA"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 244
    const-string v0, "MP4_A"

    const/16 v1, 0x10

    const-string v2, "audio/mp4"

    const-string v3, "MP4 Audio"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 246
    const-string v0, "3GP_A"

    const/16 v1, 0x11

    const-string v2, "audio/3gpp"

    const-string v3, "3GP Audio"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 248
    const-string v0, "3G2_A"

    const/16 v1, 0x12

    const-string v2, "audio/3gpp2"

    const-string v3, "3G2 Audio"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 250
    const-string v0, "ASF_A"

    const/16 v1, 0x13

    const-string v2, "audio/x-ms-asf"

    const-string v3, "ASF Audio"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 252
    const-string v0, "3GPP_A"

    const/16 v1, 0x14

    const-string v2, "audio/3gpp"

    const-string v3, "3GPP"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 254
    const-string v0, "MID"

    const-string v1, "audio/midi"

    const-string v2, "MIDI"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 255
    const-string v0, "XMF"

    const-string v1, "audio/midi"

    const-string v2, "XMF"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 256
    const-string v0, "MXMF"

    const-string v1, "audio/midi"

    const-string v2, "MXMF"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 257
    const-string v0, "RTTTL"

    const-string v1, "audio/midi"

    const-string v2, "RTTTL"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 258
    const-string v0, "SMF"

    const/16 v1, 0x17

    const-string v2, "audio/sp-midi"

    const-string v3, "SMF"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 259
    const-string v0, "IMY"

    const/16 v1, 0x18

    const-string v2, "audio/imelody"

    const-string v3, "IMY"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 260
    const-string v0, "MIDI"

    const-string v1, "audio/midi"

    const-string v2, "MIDI"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 261
    const-string v0, "RTX"

    const-string v1, "audio/midi"

    const-string v2, "MIDI"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 262
    const-string v0, "OTA"

    const-string v1, "audio/midi"

    const-string v2, "MIDI"

    invoke-static {v0, v8, v1, v2, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 263
    const-string v0, "PYA"

    const/16 v1, 0xc

    const-string v2, "audio/vnd.ms-playready.media.pya"

    const-string v3, "PYA"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 265
    const-string v0, "M4B"

    const/16 v1, 0xb

    const-string v2, "audio/mp4"

    const-string v3, "M4B"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 266
    const-string v0, "ISMA"

    const/16 v1, 0xd

    const-string v2, "audio/isma"

    const-string v3, "ISMA"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 267
    const-string v0, "QCP"

    const/16 v1, 0x15

    const-string v2, "audio/qcelp"

    const-string v3, "QCP"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 268
    const-string v0, "M3U"

    const/16 v1, 0x47

    const-string v2, "audio/x-mpegurl"

    const-string v3, "M3U"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 270
    const-string v0, "PLS"

    const/16 v1, 0x48

    const-string v2, "audio/x-scpls"

    const-string v3, "PLS"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 272
    const-string v0, "MPEG"

    const/16 v1, 0x24

    const-string v2, "video/mpeg"

    const-string v3, "MPEG"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 273
    const-string v0, "MPG"

    const/16 v1, 0x24

    const-string v2, "video/mpeg"

    const-string v3, "MPEG"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 274
    const-string v0, "MP4"

    const/16 v1, 0x1f

    const-string v2, "video/mp4"

    const-string v3, "MP4"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 275
    const-string v0, "M4V"

    const/16 v1, 0x20

    const-string v2, "video/mp4"

    const-string v3, "M4V"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 276
    const-string v0, "3GP"

    const/16 v1, 0x21

    const-string v2, "video/3gpp"

    const-string v3, "3GP"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 277
    const-string v0, "3GPP"

    const/16 v1, 0x21

    const-string v2, "video/3gpp"

    const-string v3, "3GPP"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 278
    const-string v0, "3G2"

    const/16 v1, 0x22

    const-string v2, "video/3gpp2"

    const-string v3, "3G2"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 279
    const-string v0, "3GPP2"

    const/16 v1, 0x22

    const-string v2, "video/3gpp2"

    const-string v3, "3GPP2"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 281
    const-string v0, "WMV"

    const/16 v1, 0x23

    const-string v2, "video/x-ms-wmv"

    const-string v3, "WMV"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 282
    const-string v0, "ASF"

    const/16 v1, 0x25

    const-string v2, "video/x-ms-asf"

    const-string v3, "ASF"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 283
    const-string v0, "AVI"

    const/16 v1, 0x26

    const-string v2, "video/avi"

    const-string v3, "AVI"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 284
    const-string v0, "DIVX"

    const/16 v1, 0x27

    const-string v2, "video/divx"

    const-string v3, "DIVX"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 285
    const-string v0, "FLV"

    const/16 v1, 0x28

    const-string v2, "video/flv"

    const-string v3, "FLV"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 286
    const-string v0, "MKV"

    const/16 v1, 0x29

    const-string v2, "video/mkv"

    const-string v3, "MKV"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 287
    const-string v0, "SDP"

    const/16 v1, 0x32

    const-string v2, "application/sdp"

    const-string v3, "SDP"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 289
    const-string v0, "RM"

    const/16 v1, 0x30

    const-string v2, "video/mp4"

    const-string v3, "RM"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 290
    const-string v0, "RMVB"

    const/16 v1, 0x31

    const-string v2, "video/mp4"

    const-string v3, "RMVB"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 291
    const-string v0, "MOV"

    const/16 v1, 0x2a

    const-string v2, "video/quicktime"

    const-string v3, "MOV"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 293
    const-string v0, "PYV"

    const/16 v1, 0x2b

    const-string v2, "video/vnd.ms-playready.media.pyv"

    const-string v3, "PYV"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 295
    const-string v0, "ISMV"

    const/16 v1, 0x33

    const-string v2, "video/ismv"

    const-string v3, "ISMV"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 296
    const-string v0, "SKM"

    const/16 v1, 0x2c

    const-string v2, "video/skm"

    const-string v3, "SKM"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 297
    const-string v0, "K3G"

    const/16 v1, 0x2d

    const-string v2, "video/k3g"

    const-string v3, "K3G"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 298
    const-string v0, "AK3G"

    const/16 v1, 0x2e

    const-string v2, "video/ak3g"

    const-string v3, "AK3G"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 299
    const-string v0, "WEBM"

    const/16 v1, 0x2f

    const-string v2, "video/webm"

    const-string v3, "WEBM"

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 301
    const-string v0, "JPG"

    const/16 v1, 0x3d

    const-string v2, "image/jpeg"

    const-string v3, "JPEG"

    const v4, 0x7f020003

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 302
    const-string v0, "JPEG"

    const/16 v1, 0x3d

    const-string v2, "image/jpeg"

    const-string v3, "JPEG"

    const v4, 0x7f020003

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 303
    const-string v0, "MY5"

    const/16 v1, 0x3d

    const-string v2, "image/vnd.tmo.my5"

    const-string v3, "JPEG"

    const v4, 0x7f020003

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 305
    const-string v0, "GIF"

    const/16 v1, 0x3e

    const-string v2, "image/gif"

    const-string v3, "GIF"

    const v4, 0x7f020003

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 306
    const-string v0, "PNG"

    const/16 v1, 0x3f

    const-string v2, "image/png"

    const-string v3, "PNG"

    const v4, 0x7f020003

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 307
    const-string v0, "BMP"

    const/16 v1, 0x40

    const-string v2, "image/x-ms-bmp"

    const-string v3, "Microsoft BMP"

    const v4, 0x7f020003

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 309
    const-string v0, "WBMP"

    const/16 v1, 0x41

    const-string v2, "image/vnd.wap.wbmp"

    const-string v3, "Wireless BMP"

    const v4, 0x7f020003

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 312
    const-string v0, "WPL"

    const/16 v1, 0x49

    const-string v2, "application/vnd.ms-wpl"

    const-string v3, "WPL"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 314
    const-string v0, "PDF"

    const/16 v1, 0x51

    const-string v2, "application/pdf"

    const-string v3, "Acrobat PDF"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 316
    const-string v0, "RTF"

    const/16 v1, 0x52

    const-string v2, "application/msword"

    const-string v3, "Microsoft Office WORD"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 318
    const-string v0, "DOC"

    const/16 v1, 0x52

    const-string v2, "application/msword"

    const-string v3, "Microsoft Office WORD"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 320
    const-string v0, "DOCX"

    const/16 v1, 0x52

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    const-string v3, "Microsoft Office WORD"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 323
    const-string v0, "DOT"

    const/16 v1, 0x52

    const-string v2, "application/msword"

    const-string v3, "Microsoft Office WORD"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 325
    const-string v0, "DOTX"

    const/16 v1, 0x52

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    const-string v3, "Microsoft Office WORD"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 328
    const-string v0, "CSV"

    const/16 v1, 0x50

    const-string v2, "text/comma-separated-values"

    const-string v3, "Microsoft Office Excel"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 330
    const-string v0, "XLS"

    const/16 v1, 0x53

    const-string v2, "application/vnd.ms-excel"

    const-string v3, "Microsoft Office Excel"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 332
    const-string v0, "XLSX"

    const/16 v1, 0x53

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    const-string v3, "Microsoft Office Excel"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 335
    const-string v0, "XLT"

    const/16 v1, 0x53

    const-string v2, "application/vnd.ms-excel"

    const-string v3, "Microsoft Office Excel"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 337
    const-string v0, "XLTX"

    const/16 v1, 0x53

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    const-string v3, "Microsoft Office Excel"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 340
    const-string v0, "PPS"

    const/16 v1, 0x4f

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "Microsoft Office PowerPoint"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 342
    const-string v0, "PPT"

    const/16 v1, 0x54

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "Microsoft Office PowerPoint"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 344
    const-string v0, "PPTX"

    const/16 v1, 0x54

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    const-string v3, "Microsoft Office PowerPoint"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 347
    const-string v0, "POT"

    const/16 v1, 0x54

    const-string v2, "application/vnd.ms-powerpoint"

    const-string v3, "Microsoft Office PowerPoint"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 349
    const-string v0, "POTX"

    const/16 v1, 0x54

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.template"

    const-string v3, "Microsoft Office PowerPoint"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 352
    const-string v0, "ASC"

    const/16 v1, 0x4e

    const-string v2, "text/plain"

    const-string v3, "Text Document"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 354
    const-string v0, "TXT"

    const/16 v1, 0x55

    const-string v2, "text/plain"

    const-string v3, "Text Document"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 356
    const-string v0, "GUL"

    const/16 v1, 0x56

    const-string v2, "application/jungumword"

    const-string v3, "Jungum Word"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 358
    const-string v0, "EPUB"

    const/16 v1, 0x59

    const-string v2, "application/epub+zip"

    const-string v3, "eBookReader"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 360
    const-string v0, "ACSM"

    const/16 v1, 0x59

    const-string v2, "application/vnd.adobe.adept+xml"

    const-string v3, "eBookReader"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 362
    const-string v0, "SWF"

    const/16 v1, 0x5a

    const-string v2, "application/x-shockwave-flash"

    const-string v3, "SWF"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 364
    const-string v0, "SVG"

    const/16 v1, 0x5b

    const-string v2, "image/svg+xml"

    const-string v3, "SVG"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 365
    const-string v0, "DCF"

    const/16 v1, 0x57

    const-string v2, "application/vnd.oma.drm.content"

    const-string v3, "DRM Content"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 367
    const-string v0, "ODF"

    const/16 v1, 0x58

    const-string v2, "application/vnd.oma.drm.content"

    const-string v3, "DRM Content"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 369
    const-string v0, "APK"

    const/16 v1, 0x64

    const-string v2, "application/apk"

    const-string v3, "Android package install file"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 371
    const-string v0, "JAD"

    const/16 v1, 0x6e

    const-string v2, "text/vnd.sun.j2me.app-descriptor"

    const-string v3, "JAD"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 373
    const-string v0, "JAR"

    const/16 v1, 0x6f

    const-string v2, "application/java-archive "

    const-string v3, "JAR"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 375
    const-string v0, "VCS"

    const/16 v1, 0x78

    const-string v2, "text/x-vCalendar"

    const-string v3, "VCS"

    const v4, 0x7f020005

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 377
    const-string v0, "ICS"

    const/16 v1, 0x78

    const-string v2, "text/x-vCalendar"

    const-string v3, "ICS"

    const v4, 0x7f020005

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 379
    const-string v0, "VTS"

    const/16 v1, 0x7b

    const-string v2, "text/x-vtodo"

    const-string v3, "VTS"

    const v4, 0x7f020005

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 381
    const-string v0, "VCF"

    const/16 v1, 0x79

    const-string v2, "text/x-vcard"

    const-string v3, "VCF"

    const v4, 0x7f020006

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 382
    const-string v0, "VNT"

    const/16 v1, 0x7a

    const-string v2, "text/x-vnote"

    const-string v3, "VNT"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 383
    const-string v0, "HTML"

    const/16 v1, 0x7e

    const-string v2, "text/html"

    const-string v3, "HTML"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 384
    const-string v0, "HTM"

    const/16 v1, 0x7e

    const-string v2, "text/html"

    const-string v3, "HTML"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 385
    const-string v0, "XHTML"

    const/16 v1, 0x80

    const-string v2, "text/html"

    const-string v3, "XHTML"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 386
    const-string v0, "XML"

    const/16 v1, 0x7f

    const-string v2, "application/xhtml+xml"

    const-string v3, "XML"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 388
    const-string v0, "WGT"

    const/16 v1, 0x65

    const-string v2, "application/vnd.samsung.widget"

    const-string v3, "WGT"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 390
    const-string v0, "HWP"

    const/16 v1, 0x8d

    const-string v2, "application/x-hwp"

    const-string v3, "HWP"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 392
    const-string v0, "ZIP"

    const/16 v1, 0x8f

    const-string v2, "application/zip"

    const-string v3, "ZIP"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 393
    const-string v0, "SNB"

    const/16 v1, 0x90

    const-string v2, "application/snb"

    const-string v3, "SNB"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 394
    const-string v0, "SASF"

    const/16 v1, 0x91

    const-string v2, "application/x-sasf"

    const-string v3, "SASF"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 396
    const-string v0, "SSF"

    const/16 v1, 0x92

    const-string v2, "application/ssf"

    const-string v3, "SSF"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 401
    const-string v0, "TRC"

    const/16 v1, 0x93

    const-string v2, "application/x-toruca"

    const-string v3, "TRC"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 404
    const-string v0, "TRCS"

    const/16 v1, 0x93

    const-string v2, "application/x-storuca"

    const-string v3, "TRC"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 407
    const-string v0, "SPD"

    const/16 v1, 0x94

    const-string v2, "application/spd"

    const-string v3, "SPD"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 409
    const-string v0, "SCC"

    const/16 v1, 0x95

    const-string v2, "application/vnd.samsung.scc.storyalbum"

    const-string v3, "SCC"

    invoke-static {v0, v1, v2, v3, v7}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 412
    const-string v0, "MEMO"

    const/16 v1, 0x96

    const-string v2, "application/vnd.samsung.android.memo"

    const-string v3, "MEMO"

    invoke-static {v0, v1, v2, v3, v5}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 414
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    return-void
.end method

.method static addFileType(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p0, "extension"    # Ljava/lang/String;
    .param p1, "fileType"    # I
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "desc"    # Ljava/lang/String;
    .param p4, "icon"    # I

    .prologue
    .line 225
    sget-object v0, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->sFileTypeMap:Ljava/util/HashMap;

    new-instance v1, Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->sMimeTypeMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v0, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->sMimeType:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    return-void
.end method

.method public static getDescription(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 474
    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;

    move-result-object v0

    .line 475
    .local v0, "mediaType":Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;
    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;->description:Ljava/lang/String;

    goto :goto_0
.end method

.method private static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 507
    if-nez p0, :cond_1

    .line 512
    :cond_0
    :goto_0
    return-object v1

    .line 509
    :cond_1
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 510
    .local v0, "lastDot":I
    if-ltz v0, :cond_0

    .line 512
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 441
    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 442
    .local v0, "ext":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 443
    const/4 v1, 0x0

    .line 445
    :goto_0
    return-object v1

    .line 444
    :cond_0
    sget-object v2, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->sFileTypeMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;

    .line 445
    .local v1, "mediaType":Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;
    goto :goto_0
.end method

.method public static getFileTypeInt(Ljava/lang/String;)I
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 449
    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;

    move-result-object v0

    .line 450
    .local v0, "mediaType":Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;->fileType:I

    goto :goto_0
.end method

.method public static getIcon(Ljava/lang/String;)I
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 459
    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;

    move-result-object v0

    .line 460
    .local v0, "mediaType":Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;
    if-nez v0, :cond_0

    const v1, 0x7f020001

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;->icon:I

    goto :goto_0
.end method

.method public static getIcon(Ljava/lang/String;Landroid/content/ContentResolver;)I
    .locals 2
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 464
    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getIcon(Ljava/lang/String;)I

    move-result v0

    .line 465
    .local v0, "icon":I
    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 466
    invoke-static {p0, p1}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->isAudioInMediaStore(Ljava/lang/String;Landroid/content/ContentResolver;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 467
    const v0, 0x7f020004

    .line 470
    :cond_0
    return v0
.end method

.method public static getMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 454
    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;

    move-result-object v0

    .line 455
    .local v0, "mediaType":Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;->mimeType:Ljava/lang/String;

    goto :goto_0
.end method

.method public static isAudioInMediaStore(Ljava/lang/String;Landroid/content/ContentResolver;)Z
    .locals 9
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 490
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "_data= ? COLLATE LOCALIZED"

    new-array v4, v8, [Ljava/lang/String;

    aput-object p0, v4, v7

    move-object v0, p1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 494
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 495
    const-string v0, "FileTypeManager"

    const-string v1, "isAudioInMediaStore"

    const-string v2, "MediaFile : isAudioInMediaStore : c is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    .line 503
    :goto_0
    return v0

    .line 498
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 499
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v8

    .line 500
    goto :goto_0

    .line 502
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v7

    .line 503
    goto :goto_0
.end method

.method public static isImageFileType(Ljava/lang/String;)Z
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 425
    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;

    move-result-object v1

    .line 426
    .local v1, "mediaType":Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;
    if-nez v1, :cond_1

    .line 429
    :cond_0
    :goto_0
    return v2

    .line 428
    :cond_1
    iget v0, v1, Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;->fileType:I

    .line 429
    .local v0, "fileType":I
    const/16 v3, 0x3d

    if-lt v0, v3, :cond_0

    const/16 v3, 0x41

    if-gt v0, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isMediaFileType(Ljava/lang/String;)Z
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 433
    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;

    move-result-object v1

    .line 434
    .local v1, "mediaType":Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;
    if-nez v1, :cond_1

    .line 437
    :cond_0
    :goto_0
    return v2

    .line 436
    :cond_1
    iget v0, v1, Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;->fileType:I

    .line 437
    .local v0, "fileType":I
    const/16 v3, 0x1f

    if-lt v0, v3, :cond_0

    const/16 v3, 0x41

    if-gt v0, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isVideoFileType(Ljava/lang/String;)Z
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 417
    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getFileType(Ljava/lang/String;)Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;

    move-result-object v1

    .line 418
    .local v1, "mediaType":Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;
    if-nez v1, :cond_1

    .line 421
    :cond_0
    :goto_0
    return v2

    .line 420
    :cond_1
    iget v0, v1, Lcom/samsung/android/app/FileShareClient/FileTypeManager$MediaFileType;->fileType:I

    .line 421
    .local v0, "fileType":I
    const/16 v3, 0x1f

    if-lt v0, v3, :cond_0

    const/16 v3, 0x33

    if-gt v0, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static needToCheckMimeType(Ljava/lang/String;)Z
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 479
    invoke-static {p0}, Lcom/samsung/android/app/FileShareClient/FileTypeManager;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 480
    .local v0, "ext":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 486
    :cond_0
    :goto_0
    return v1

    .line 482
    :cond_1
    const-string v2, "MP4"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "3GP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "3G2"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "ASF"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "3GPP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 484
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method
