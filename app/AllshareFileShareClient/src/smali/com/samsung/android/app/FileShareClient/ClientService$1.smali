.class Lcom/samsung/android/app/FileShareClient/ClientService$1;
.super Lcom/samsung/android/sconnect/central/action/IFileShareService$Stub;
.source "ClientService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/FileShareClient/ClientService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/ClientService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/ClientService;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/ClientService$1;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/IFileShareService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelTransfer(Ljava/lang/String;)Z
    .locals 9
    .param p1, "mac"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 290
    const-string v5, "ClientService"

    const-string v6, "cancelTransfer"

    const-string v7, ""

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$200()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_4

    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$200()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 293
    # getter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mListOutbound:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$200()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/FileShareClient/Outbound;

    .line 294
    .local v2, "outbound":Lcom/samsung/android/app/FileShareClient/Outbound;
    const-string v5, "ClientService"

    const-string v6, "cancelTransfer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "outbound.getId() - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/Outbound;->getReceiverList()Ljava/util/ArrayList;

    move-result-object v4

    .line 297
    .local v4, "recvList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/FileShareClient/Receiver;>;"
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 298
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/FileShareClient/Receiver;

    .line 299
    .local v3, "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    const-string v5, "ClientService"

    const-string v6, "cancelTransfer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "recvList.get(i).getDeviceName() - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getDeviceName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const-string v5, "ClientService"

    const-string v6, "cancelTransfer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "recvList.get(i).getDeviceName() - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3, p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->isMyMacAddress(Ljava/lang/String;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    invoke-virtual {v3, p1}, Lcom/samsung/android/app/FileShareClient/Receiver;->isMyMacAddress(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 305
    invoke-virtual {v3}, Lcom/samsung/android/app/FileShareClient/Receiver;->getStatus()I

    move-result v5

    const/16 v6, 0x3ee

    if-eq v5, v6, :cond_1

    .line 306
    const-string v5, "ClientService"

    const-string v6, "cancelTransfer"

    invoke-static {v5, v6, p1}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    invoke-virtual {v2, v3}, Lcom/samsung/android/app/FileShareClient/Outbound;->Sconnect_Cancel(Lcom/samsung/android/app/FileShareClient/Receiver;)V

    goto :goto_0

    .line 310
    :cond_2
    const-string v5, "ClientService"

    const-string v6, "cancelTransfer"

    const-string v7, "Invalid state"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 315
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "outbound":Lcom/samsung/android/app/FileShareClient/Outbound;
    .end local v3    # "recv":Lcom/samsung/android/app/FileShareClient/Receiver;
    .end local v4    # "recvList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/FileShareClient/Receiver;>;"
    :cond_3
    const/4 v5, 0x1

    .line 317
    :goto_1
    return v5

    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public registerCallback(Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;)Z
    .locals 3
    .param p1, "callback"    # Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 282
    const-string v0, "ClientService"

    const-string v1, "registerCallback"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$1;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    # setter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    invoke-static {v0, p1}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$002(Lcom/samsung/android/app/FileShareClient/ClientService;Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    .line 284
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$1;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    # invokes: Lcom/samsung/android/app/FileShareClient/ClientService;->notifyCurrentTransfer()V
    invoke-static {v0}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$100(Lcom/samsung/android/app/FileShareClient/ClientService;)V

    .line 285
    const/4 v0, 0x1

    return v0
.end method

.method public unregisterCallback(Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;)Z
    .locals 3
    .param p1, "callback"    # Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 275
    const-string v0, "ClientService"

    const-string v1, "unregisterCallback"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/ClientService$1;->this$0:Lcom/samsung/android/app/FileShareClient/ClientService;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/FileShareClient/ClientService;->mSconnCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;
    invoke-static {v0, v1}, Lcom/samsung/android/app/FileShareClient/ClientService;->access$002(Lcom/samsung/android/app/FileShareClient/ClientService;Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;)Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    .line 277
    const/4 v0, 0x1

    return v0
.end method
