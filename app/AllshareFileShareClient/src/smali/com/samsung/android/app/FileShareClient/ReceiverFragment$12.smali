.class Lcom/samsung/android/app/FileShareClient/ReceiverFragment$12;
.super Ljava/lang/Object;
.source "ReceiverFragment.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->checkCurrentReceiverConnection(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

.field final synthetic val$index:I


# direct methods
.method constructor <init>(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;I)V
    .locals 0

    .prologue
    .line 598
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$12;->this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    iput p2, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$12;->val$index:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupInfoAvailable(Landroid/net/wifi/p2p/WifiP2pGroup;)V
    .locals 7
    .param p1, "group"    # Landroid/net/wifi/p2p/WifiP2pGroup;

    .prologue
    .line 603
    const/4 v2, 0x1

    .line 604
    .local v2, "reqReconnect":Z
    if-eqz p1, :cond_2

    .line 605
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 606
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 607
    .local v0, "device":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$12;->this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    # getter for: Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-static {v3}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->access$000(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)Lcom/samsung/android/app/FileShareClient/Receiver;

    move-result-object v3

    iget-object v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->isMyMacAddress(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 608
    const/4 v2, 0x0

    .line 609
    const-string v3, "ReceiverFragment"

    const-string v4, "checkCurrentReceiverConnection"

    const-string v5, "group.isGroupOwner"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 614
    .end local v0    # "device":Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$12;->this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    # getter for: Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->mReceiver:Lcom/samsung/android/app/FileShareClient/Receiver;
    invoke-static {v3}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->access$000(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;)Lcom/samsung/android/app/FileShareClient/Receiver;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v4

    iget-object v4, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/FileShareClient/Receiver;->isMyMacAddress(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v2, 0x1

    .line 615
    :goto_1
    const-string v3, "ReceiverFragment"

    const-string v4, "checkCurrentReceiverConnection"

    const-string v5, "group.isNotGroupOwner"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    :cond_2
    const-string v3, "ReceiverFragment"

    const-string v4, "checkCurrentReceiverConnection"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reqReconnect : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    iget v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$12;->val$index:I

    if-ltz v3, :cond_4

    .line 621
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$12;->this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    iget v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$12;->val$index:I

    # invokes: Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showResendConfirmDialog(ZI)V
    invoke-static {v3, v2, v4}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->access$600(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;ZI)V

    .line 625
    :goto_2
    return-void

    .line 614
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 623
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$12;->this$0:Lcom/samsung/android/app/FileShareClient/ReceiverFragment;

    iget v4, p0, Lcom/samsung/android/app/FileShareClient/ReceiverFragment$12;->val$index:I

    # invokes: Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->showResendAllConfirmDialog(ZI)V
    invoke-static {v3, v2, v4}, Lcom/samsung/android/app/FileShareClient/ReceiverFragment;->access$700(Lcom/samsung/android/app/FileShareClient/ReceiverFragment;ZI)V

    goto :goto_2
.end method
