.class Lcom/samsung/android/app/FileShareClient/FileManager;
.super Ljava/lang/Object;
.source "FileManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/FileShareClient/FileManager$1;,
        Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;
    }
.end annotation


# static fields
.field public static final GENERATE_FAIL:I = 0x0

.field public static final GENERATE_SUCCESS:I = 0x1

.field public static final GENERATING:I = 0x2

.field private static final TAGClass:Ljava/lang/String; = "FileManager"

.field private static final URI_ERROR:I = 0x0

.field private static final URI_GENERATED:I = 0x1

.field private static final URI_NEED_TO_COPY_FILE:I = 0x3

.field private static final URI_NEED_TO_MAKE_VCARD:I = 0x2

.field private static final URI_VCARD_HEADER:Ljava/lang/String; = "content://com.android.contacts/contacts/as_multi_vcard/"

.field private static final URI_VCARD_WRITE_MAX:I = 0x3e8

.field private static filesizeContext:Landroid/content/Context;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mFileMaker:Ljava/lang/Thread;

.field private mResultHandler:Landroid/os/Handler;

.field private mRetFilePath:Ljava/lang/String;

.field private mTempFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mbDestroyed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/FileShareClient/FileManager;->filesizeContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    .line 50
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileList:Ljava/util/ArrayList;

    .line 56
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mTempFileList:Ljava/util/ArrayList;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileMaker:Ljava/lang/Thread;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mbDestroyed:Z

    .line 63
    iput-object p1, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    .line 64
    sput-object p1, Lcom/samsung/android/app/FileShareClient/FileManager;->filesizeContext:Landroid/content/Context;

    .line 65
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/FileShareClient/FileManager;Landroid/net/Uri;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/FileManager;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/FileShareClient/FileManager;->uri2FileUri(Landroid/net/Uri;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/FileShareClient/FileManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/FileManager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/FileShareClient/FileManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/FileManager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/FileShareClient/FileManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/FileManager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/FileShareClient/FileManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/FileManager;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mbDestroyed:Z

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/FileShareClient/FileManager;Ljava/io/File;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/FileManager;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/FileManager;->copyFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/FileShareClient/FileManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/FileShareClient/FileManager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mTempFileList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private copyFile(Ljava/io/File;)Ljava/io/File;
    .locals 16
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 535
    const/4 v6, 0x0

    .line 536
    .local v6, "inFile":Ljava/io/FileInputStream;
    const/4 v9, 0x0

    .line 538
    .local v9, "outFile":Ljava/io/FileOutputStream;
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/app/FileShareClient/FileManager;->getDestCopyFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 539
    .local v2, "destPath":Ljava/lang/String;
    const-string v11, "FileManager"

    const-string v12, "copyFile"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "dest "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 544
    .local v8, "newFile":Ljava/io/File;
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 545
    .end local v6    # "inFile":Ljava/io/FileInputStream;
    .local v7, "inFile":Ljava/io/FileInputStream;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v11, v12, v13}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v9

    .line 547
    const/16 v11, 0x200

    new-array v1, v11, [B

    .line 548
    .local v1, "buffer":[B
    const/4 v10, 0x0

    .line 549
    .local v10, "readCount":I
    :goto_0
    const/4 v11, -0x1

    invoke-virtual {v7, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v10

    if-eq v11, v10, :cond_4

    .line 550
    const/4 v11, 0x0

    invoke-virtual {v9, v1, v11, v10}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 553
    .end local v1    # "buffer":[B
    .end local v10    # "readCount":I
    :catch_0
    move-exception v3

    move-object v6, v7

    .line 554
    .end local v7    # "inFile":Ljava/io/FileInputStream;
    .local v3, "e":Ljava/lang/Exception;
    .restart local v6    # "inFile":Ljava/io/FileInputStream;
    :goto_1
    :try_start_2
    const-string v11, "FileManager"

    const-string v12, "copyFile"

    const-string v13, "Exception"

    invoke-static {v11, v12, v13, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 555
    const/4 v8, 0x0

    .line 558
    if-eqz v6, :cond_0

    .line 559
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 566
    :cond_0
    :goto_2
    if-eqz v9, :cond_1

    .line 567
    :try_start_4
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 574
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_3
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_3

    .line 575
    :cond_2
    const/4 v8, 0x0

    .line 576
    const-string v11, "FileManager"

    const-string v12, "copyFile"

    const-string v13, "fail to copy"

    invoke-static {v11, v12, v13}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    :cond_3
    return-object v8

    .line 558
    .end local v6    # "inFile":Ljava/io/FileInputStream;
    .restart local v1    # "buffer":[B
    .restart local v7    # "inFile":Ljava/io/FileInputStream;
    .restart local v10    # "readCount":I
    :cond_4
    if-eqz v7, :cond_5

    .line 559
    :try_start_5
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 566
    :cond_5
    :goto_4
    if-eqz v9, :cond_6

    .line 567
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    :cond_6
    move-object v6, v7

    .line 571
    .end local v7    # "inFile":Ljava/io/FileInputStream;
    .restart local v6    # "inFile":Ljava/io/FileInputStream;
    goto :goto_3

    .line 561
    .end local v6    # "inFile":Ljava/io/FileInputStream;
    .restart local v7    # "inFile":Ljava/io/FileInputStream;
    :catch_1
    move-exception v4

    .line 562
    .local v4, "eIn":Ljava/io/IOException;
    const-string v11, "FileManager"

    const-string v12, "copyFile"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "IOException: in "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 569
    .end local v4    # "eIn":Ljava/io/IOException;
    :catch_2
    move-exception v5

    .line 570
    .local v5, "eOut":Ljava/lang/Exception;
    const-string v11, "FileManager"

    const-string v12, "copyFile"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Exception: out "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v7

    .line 572
    .end local v7    # "inFile":Ljava/io/FileInputStream;
    .restart local v6    # "inFile":Ljava/io/FileInputStream;
    goto :goto_3

    .line 561
    .end local v1    # "buffer":[B
    .end local v5    # "eOut":Ljava/lang/Exception;
    .end local v10    # "readCount":I
    .restart local v3    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v4

    .line 562
    .restart local v4    # "eIn":Ljava/io/IOException;
    const-string v11, "FileManager"

    const-string v12, "copyFile"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "IOException: in "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 569
    .end local v4    # "eIn":Ljava/io/IOException;
    :catch_4
    move-exception v5

    .line 570
    .restart local v5    # "eOut":Ljava/lang/Exception;
    const-string v11, "FileManager"

    const-string v12, "copyFile"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Exception: out "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 557
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v5    # "eOut":Ljava/lang/Exception;
    :catchall_0
    move-exception v11

    .line 558
    :goto_5
    if-eqz v6, :cond_7

    .line 559
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 566
    :cond_7
    :goto_6
    if-eqz v9, :cond_8

    .line 567
    :try_start_8
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    .line 571
    :cond_8
    :goto_7
    throw v11

    .line 561
    :catch_5
    move-exception v4

    .line 562
    .restart local v4    # "eIn":Ljava/io/IOException;
    const-string v12, "FileManager"

    const-string v13, "copyFile"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "IOException: in "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 569
    .end local v4    # "eIn":Ljava/io/IOException;
    :catch_6
    move-exception v5

    .line 570
    .restart local v5    # "eOut":Ljava/lang/Exception;
    const-string v12, "FileManager"

    const-string v13, "copyFile"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception: out "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 557
    .end local v5    # "eOut":Ljava/lang/Exception;
    .end local v6    # "inFile":Ljava/io/FileInputStream;
    .restart local v7    # "inFile":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v11

    move-object v6, v7

    .end local v7    # "inFile":Ljava/io/FileInputStream;
    .restart local v6    # "inFile":Ljava/io/FileInputStream;
    goto :goto_5

    .line 553
    :catch_7
    move-exception v3

    goto/16 :goto_1
.end method

.method public static getConvertSize(J)Ljava/lang/String;
    .locals 4
    .param p0, "size"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 808
    const-string v0, ""

    .line 810
    .local v0, "fileSize":Ljava/lang/String;
    cmp-long v1, p0, v2

    if-gez v1, :cond_0

    .line 811
    const-string v0, "Wrong size"

    .line 818
    :goto_0
    const-string v1, "FileManager"

    const-string v2, "getConvertSize"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    return-object v0

    .line 812
    :cond_0
    cmp-long v1, p0, v2

    if-nez v1, :cond_1

    .line 813
    const-string v0, "Zero"

    goto :goto_0

    .line 815
    :cond_1
    sget-object v1, Lcom/samsung/android/app/FileShareClient/FileManager;->filesizeContext:Landroid/content/Context;

    invoke-static {v1, p0, p1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getDestCopyFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 516
    const-string v5, "/"

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 518
    .local v4, "nPos":I
    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 519
    .local v2, "fileName":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 522
    .local v0, "destPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 523
    .local v1, "file":Ljava/io/File;
    const/4 v3, 0x0

    .line 524
    .local v3, "index":I
    :goto_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 525
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 526
    add-int/lit8 v3, v3, 0x1

    .line 527
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v1    # "file":Ljava/io/File;
    goto :goto_0

    .line 531
    :cond_0
    return-object v0
.end method

.method private getDestHtmlFile()Ljava/io/File;
    .locals 10

    .prologue
    .line 706
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v7, "yyyyMMdd_HHmmss"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-direct {v3, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 707
    .local v3, "formatter":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 708
    .local v0, "currentTime":Ljava/util/Date;
    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 710
    .local v1, "dTime":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    const v9, 0x7f04003c

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 712
    .local v6, "name":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".html"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 713
    .local v4, "htmlName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    iget-object v7, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-direct {v2, v7, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 715
    .local v2, "file":Ljava/io/File;
    const/4 v5, 0x0

    .line 716
    .local v5, "index":I
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 717
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".html"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 718
    new-instance v2, Ljava/io/File;

    .end local v2    # "file":Ljava/io/File;
    iget-object v7, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-direct {v2, v7, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 719
    .restart local v2    # "file":Ljava/io/File;
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 721
    :cond_0
    return-object v2
.end method

.method private getDestVcardPath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const-wide/16 v10, 0x3e8

    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 586
    const-string v8, ""

    .line 587
    .local v8, "retPath":Ljava/lang/String;
    const-string v7, ""

    .line 588
    .local v7, "filename":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_display_name"

    aput-object v0, v2, v9

    .line 592
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 593
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 594
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 595
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 597
    const-string v0, "FileManager"

    const-string v1, "getDestVcardPath"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "filename:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    const-string v0, ".vcf"

    invoke-virtual {v7, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 600
    const-string v0, "FileManager"

    const-string v1, "getDestVcardPath"

    const-string v3, "endsWith .vcf"

    invoke-static {v0, v1, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    const-string v0, ".vcf"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    rem-long/2addr v4, v10

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ".vcf"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 602
    const-string v0, "[\\\\/:?<>|]"

    const-string v1, "_"

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 609
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 610
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 613
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 614
    const-string v0, "FileManager"

    const-string v1, "getDestVcardPath"

    invoke-static {v0, v1, v8}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    return-object v8

    .line 604
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[\\\\/:?<>|]"

    const-string v3, "_"

    invoke-virtual {v7, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    rem-long/2addr v4, v10

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method private getFilePathFromUri(Landroid/net/Uri;)I
    .locals 12
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 447
    const/4 v11, 0x0

    .line 449
    .local v11, "nRetResult":I
    if-nez p1, :cond_0

    .line 450
    const-string v0, "FileManager"

    const-string v1, "getFilePathFromUri"

    const-string v3, "In is null "

    invoke-static {v0, v1, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v11

    .line 508
    :goto_0
    return v0

    .line 454
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    .line 455
    .local v7, "contentType":Ljava/lang/String;
    const-string v0, "FileManager"

    const-string v1, "getFilePathFromUri"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "contentType > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    if-eqz v7, :cond_1

    const-string v0, "text/x-vcard"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 457
    const/4 v0, 0x2

    goto :goto_0

    .line 460
    :cond_1
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 463
    .local v2, "proj":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 467
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 473
    :goto_1
    if-nez v8, :cond_2

    move v0, v11

    .line 474
    goto :goto_0

    .line 469
    :catch_0
    move-exception v9

    .line 471
    .local v9, "e":Ljava/lang/Exception;
    const-string v0, "FileManager"

    const-string v1, "getFilePathFromUri"

    const-string v3, "Exception"

    invoke-static {v0, v1, v3, v9}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 477
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gez v0, :cond_3

    .line 478
    const-string v0, "FileManager"

    const-string v1, "getFilePathFromUri"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wrong query > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move v0, v11

    .line 482
    goto :goto_0

    .line 485
    :cond_3
    const/4 v6, -0x1

    .line 487
    .local v6, "col":I
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 489
    const-string v0, "_data"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 491
    invoke-interface {v8, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;

    .line 492
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 493
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 504
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 507
    :goto_3
    const-string v0, "FileManager"

    const-string v1, "getFilePathFromUri"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ret > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v11

    .line 508
    goto/16 :goto_0

    .line 495
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 496
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    .line 497
    .local v10, "index":I
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;

    add-int/lit8 v1, v10, 0x3

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 499
    .end local v10    # "index":I
    :cond_5
    const/4 v11, 0x1

    goto :goto_2

    .line 501
    :catch_1
    move-exception v9

    .line 502
    .restart local v9    # "e":Ljava/lang/Exception;
    :try_start_3
    const-string v0, "FileManager"

    const-string v1, "getFilePathFromUri"

    const-string v3, "Exception"

    invoke-static {v0, v1, v3, v9}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 504
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private uri2FileUri(Landroid/net/Uri;Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mime"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 419
    const-string v2, "FileManager"

    const-string v3, "uri2FileUri"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] mime : ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    if-nez p1, :cond_0

    .line 422
    const-string v2, "FileManager"

    const-string v3, "uri2FileUri"

    const-string v4, "uri is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :goto_0
    return v1

    .line 426
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 427
    .local v0, "scheme":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 428
    const-string v2, "FileManager"

    const-string v3, "uri2FileUri"

    const-string v4, "scheme is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 432
    :cond_1
    const-string v2, "content"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 433
    invoke-direct {p0, p1}, Lcom/samsung/android/app/FileShareClient/FileManager;->getFilePathFromUri(Landroid/net/Uri;)I

    move-result v1

    goto :goto_0

    .line 434
    :cond_2
    const-string v2, "file"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 436
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;

    .line 442
    const/4 v1, 0x1

    goto :goto_0

    .line 438
    :cond_3
    const-string v2, "FileManager"

    const-string v3, "uri2FileUri"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path can\'t handle >"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method createHtml(Ljava/lang/CharSequence;)Ljava/io/File;
    .locals 17
    .param p1, "charSeq"    # Ljava/lang/CharSequence;

    .prologue
    .line 726
    if-nez p1, :cond_1

    .line 727
    const-string v12, "FileManager"

    const-string v13, "createHtml"

    const-string v14, "charSeq is null"

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    const/4 v4, 0x0

    .line 781
    :cond_0
    :goto_0
    return-object v4

    .line 732
    :cond_1
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v12}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 733
    .local v10, "strBody":Ljava/lang/StringBuffer;
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/FileShareClient/FileManager;->getMultiLiskList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 735
    .local v6, "linkList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v6, :cond_2

    .line 736
    const-string v12, "FileManager"

    const-string v13, "createHtml"

    const-string v14, "linkList is null"

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    const/4 v4, 0x0

    goto :goto_0

    .line 740
    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 741
    .local v7, "nSize":I
    const-string v12, "FileManager"

    const-string v13, "createHtml"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "link > "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v7, :cond_4

    .line 744
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 745
    .local v9, "start_position":I
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int v3, v9, v12

    .line 746
    .local v3, "end_position":I
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    const-string v13, "www"

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 747
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "<a href=\"http://"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\">"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "</a>"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v9, v3, v12}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 743
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 750
    :cond_3
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "<a href=\""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\">"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "</a>"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v9, v3, v12}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 756
    .end local v3    # "end_position":I
    .end local v9    # "start_position":I
    :cond_4
    const-string v11, "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"/></head><body>"

    .line 757
    .local v11, "strHtml":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    const-string v14, "\n"

    const-string v15, "<br>"

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 758
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "</body></html>"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 761
    const/4 v8, 0x0

    .line 762
    .local v8, "outStream":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .line 766
    .local v4, "htmlFile":Ljava/io/File;
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/FileShareClient/FileManager;->getDestHtmlFile()Ljava/io/File;

    move-result-object v4

    .line 767
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v12, v13, v14}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v8

    .line 768
    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 774
    if-eqz v8, :cond_0

    .line 775
    :try_start_1
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 776
    :catch_0
    move-exception v2

    .line 777
    .local v2, "eOst":Ljava/lang/Exception;
    const-string v12, "FileManager"

    const-string v13, "createHtml"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception:outStream"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 769
    .end local v2    # "eOst":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 770
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v12, "FileManager"

    const-string v13, "createHtml"

    const-string v14, "Exception"

    invoke-static {v12, v13, v14, v1}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 771
    const/4 v4, 0x0

    .line 774
    if-eqz v8, :cond_0

    .line 775
    :try_start_3
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 776
    :catch_2
    move-exception v2

    .line 777
    .restart local v2    # "eOst":Ljava/lang/Exception;
    const-string v12, "FileManager"

    const-string v13, "createHtml"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception:outStream"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 773
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "eOst":Ljava/lang/Exception;
    :catchall_0
    move-exception v12

    .line 774
    if-eqz v8, :cond_5

    .line 775
    :try_start_4
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 778
    :cond_5
    :goto_3
    throw v12

    .line 776
    :catch_3
    move-exception v2

    .line 777
    .restart local v2    # "eOst":Ljava/lang/Exception;
    const-string v13, "FileManager"

    const-string v14, "createHtml"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Exception:outStream"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method createVcard(Landroid/net/Uri;)Ljava/io/File;
    .locals 23
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 621
    const/4 v8, 0x0

    .line 622
    .local v8, "inStream":Ljava/io/InputStream;
    const/4 v11, 0x0

    .line 623
    .local v11, "outStream":Ljava/io/FileOutputStream;
    const/16 v16, 0x0

    .line 624
    .local v16, "vCard":Ljava/io/File;
    const/4 v13, 0x1

    .line 627
    .local v13, "reqWrite":Z
    const/4 v15, 0x0

    .line 628
    .local v15, "uriString":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    .line 629
    .local v10, "nextToken":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/app/FileShareClient/FileManager;->getDestVcardPath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v14

    .line 630
    .local v14, "savePath":Ljava/lang/String;
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    invoke-direct {v0, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 632
    .end local v16    # "vCard":Ljava/io/File;
    .local v17, "vCard":Ljava/io/File;
    :cond_0
    if-eqz v13, :cond_d

    .line 633
    move-object v15, v10

    .line 634
    const/4 v9, 0x0

    .line 635
    .local v9, "index":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    const/16 v18, 0x3e8

    move/from16 v0, v18

    if-ge v7, v0, :cond_1

    .line 636
    :try_start_1
    const-string v18, "%3A"

    add-int/lit8 v19, v9, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v9

    .line 637
    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v9, v0, :cond_5

    .line 641
    :cond_1
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v9, v0, :cond_6

    .line 642
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "content://com.android.contacts/contacts/as_multi_vcard/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    add-int/lit8 v19, v9, 0x3

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 643
    const/16 v18, 0x0

    add-int/lit8 v19, v9, 0x3

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 644
    const/4 v13, 0x1

    .line 648
    :goto_1
    if-eqz v8, :cond_2

    .line 649
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    .line 652
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v8

    .line 653
    if-nez v8, :cond_7

    .line 654
    const-string v18, "FileManager"

    const-string v19, "createVcard"

    const-string v20, "fail to create [inStream is null]"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 655
    const/16 v16, 0x0

    .line 677
    if-eqz v8, :cond_3

    .line 678
    :try_start_2
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 685
    :cond_3
    :goto_2
    if-eqz v11, :cond_4

    .line 686
    :try_start_3
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_4
    :goto_3
    move-object/from16 v18, v16

    move-object/from16 v16, v17

    .line 698
    .end local v7    # "i":I
    .end local v9    # "index":I
    .end local v10    # "nextToken":Ljava/lang/String;
    .end local v14    # "savePath":Ljava/lang/String;
    .end local v17    # "vCard":Ljava/io/File;
    .restart local v16    # "vCard":Ljava/io/File;
    :goto_4
    return-object v18

    .line 635
    .end local v16    # "vCard":Ljava/io/File;
    .restart local v7    # "i":I
    .restart local v9    # "index":I
    .restart local v10    # "nextToken":Ljava/lang/String;
    .restart local v14    # "savePath":Ljava/lang/String;
    .restart local v17    # "vCard":Ljava/io/File;
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 646
    :cond_6
    const/4 v13, 0x0

    goto :goto_1

    .line 680
    :catch_0
    move-exception v5

    .line 681
    .local v5, "eIst":Ljava/io/IOException;
    const-string v18, "FileManager"

    const-string v19, "createVcard"

    const-string v20, "IOException"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 688
    .end local v5    # "eIst":Ljava/io/IOException;
    :catch_1
    move-exception v6

    .line 689
    .local v6, "eOst":Ljava/lang/Exception;
    const-string v18, "FileManager"

    const-string v19, "createVcard"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception: OutStream"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 658
    .end local v6    # "eOst":Ljava/lang/Exception;
    :cond_7
    if-eqz v11, :cond_8

    .line 659
    :try_start_4
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V

    .line 661
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/FileShareClient/FileManager;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    const v20, 0x8001

    invoke-virtual/range {v18 .. v20}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v11

    .line 664
    const/16 v18, 0x200

    move/from16 v0, v18

    new-array v3, v0, [B

    .line 665
    .local v3, "buffer":[B
    const/4 v12, 0x0

    .line 666
    .local v12, "readCount":I
    :cond_9
    :goto_5
    const/16 v18, -0x1

    invoke-virtual {v8, v3}, Ljava/io/InputStream;->read([B)I

    move-result v12

    move/from16 v0, v18

    if-eq v0, v12, :cond_0

    .line 667
    if-lez v12, :cond_9

    .line 668
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v11, v3, v0, v12}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_5

    .line 672
    .end local v3    # "buffer":[B
    .end local v12    # "readCount":I
    :catch_2
    move-exception v4

    move-object/from16 v16, v17

    .line 673
    .end local v7    # "i":I
    .end local v9    # "index":I
    .end local v10    # "nextToken":Ljava/lang/String;
    .end local v14    # "savePath":Ljava/lang/String;
    .end local v17    # "vCard":Ljava/io/File;
    .local v4, "e":Ljava/lang/Exception;
    .restart local v16    # "vCard":Ljava/io/File;
    :goto_6
    :try_start_5
    const-string v18, "FileManager"

    const-string v19, "createVcard"

    const-string v20, "Exception "

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 674
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 677
    if-eqz v8, :cond_a

    .line 678
    :try_start_6
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 685
    :cond_a
    :goto_7
    if-eqz v11, :cond_b

    .line 686
    :try_start_7
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    .line 693
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_b
    :goto_8
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_c

    .line 694
    const/16 v16, 0x0

    .line 695
    const-string v18, "FileManager"

    const-string v19, "createVcard"

    const-string v20, "fail to create"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    move-object/from16 v18, v16

    .line 698
    goto/16 :goto_4

    .line 677
    .end local v16    # "vCard":Ljava/io/File;
    .restart local v10    # "nextToken":Ljava/lang/String;
    .restart local v14    # "savePath":Ljava/lang/String;
    .restart local v17    # "vCard":Ljava/io/File;
    :cond_d
    if-eqz v8, :cond_e

    .line 678
    :try_start_8
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 685
    :cond_e
    :goto_9
    if-eqz v11, :cond_f

    .line 686
    :try_start_9
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    :cond_f
    move-object/from16 v16, v17

    .line 690
    .end local v17    # "vCard":Ljava/io/File;
    .restart local v16    # "vCard":Ljava/io/File;
    goto :goto_8

    .line 680
    .end local v16    # "vCard":Ljava/io/File;
    .restart local v17    # "vCard":Ljava/io/File;
    :catch_3
    move-exception v5

    .line 681
    .restart local v5    # "eIst":Ljava/io/IOException;
    const-string v18, "FileManager"

    const-string v19, "createVcard"

    const-string v20, "IOException"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_9

    .line 688
    .end local v5    # "eIst":Ljava/io/IOException;
    :catch_4
    move-exception v6

    .line 689
    .restart local v6    # "eOst":Ljava/lang/Exception;
    const-string v18, "FileManager"

    const-string v19, "createVcard"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception: OutStream"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v16, v17

    .line 691
    .end local v17    # "vCard":Ljava/io/File;
    .restart local v16    # "vCard":Ljava/io/File;
    goto :goto_8

    .line 680
    .end local v6    # "eOst":Ljava/lang/Exception;
    .end local v10    # "nextToken":Ljava/lang/String;
    .end local v14    # "savePath":Ljava/lang/String;
    .restart local v4    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v5

    .line 681
    .restart local v5    # "eIst":Ljava/io/IOException;
    const-string v18, "FileManager"

    const-string v19, "createVcard"

    const-string v20, "IOException"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_7

    .line 688
    .end local v5    # "eIst":Ljava/io/IOException;
    :catch_6
    move-exception v6

    .line 689
    .restart local v6    # "eOst":Ljava/lang/Exception;
    const-string v18, "FileManager"

    const-string v19, "createVcard"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception: OutStream"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 676
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v6    # "eOst":Ljava/lang/Exception;
    :catchall_0
    move-exception v18

    .line 677
    :goto_a
    if-eqz v8, :cond_10

    .line 678
    :try_start_a
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 685
    :cond_10
    :goto_b
    if-eqz v11, :cond_11

    .line 686
    :try_start_b
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_8

    .line 690
    :cond_11
    :goto_c
    throw v18

    .line 680
    :catch_7
    move-exception v5

    .line 681
    .restart local v5    # "eIst":Ljava/io/IOException;
    const-string v19, "FileManager"

    const-string v20, "createVcard"

    const-string v21, "IOException"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2, v5}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_b

    .line 688
    .end local v5    # "eIst":Ljava/io/IOException;
    :catch_8
    move-exception v6

    .line 689
    .restart local v6    # "eOst":Ljava/lang/Exception;
    const-string v19, "FileManager"

    const-string v20, "createVcard"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Exception: OutStream"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 676
    .end local v6    # "eOst":Ljava/lang/Exception;
    .end local v16    # "vCard":Ljava/io/File;
    .restart local v7    # "i":I
    .restart local v9    # "index":I
    .restart local v10    # "nextToken":Ljava/lang/String;
    .restart local v14    # "savePath":Ljava/lang/String;
    .restart local v17    # "vCard":Ljava/io/File;
    :catchall_1
    move-exception v18

    move-object/from16 v16, v17

    .end local v17    # "vCard":Ljava/io/File;
    .restart local v16    # "vCard":Ljava/io/File;
    goto :goto_a

    .line 672
    .end local v7    # "i":I
    .end local v9    # "index":I
    .end local v10    # "nextToken":Ljava/lang/String;
    .end local v14    # "savePath":Ljava/lang/String;
    :catch_9
    move-exception v4

    goto/16 :goto_6
.end method

.method public deleteTempFiles()V
    .locals 6

    .prologue
    .line 242
    const-string v2, "FileManager"

    const-string v3, "deleteTempFiles"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mTempFileList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mTempFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 244
    iget-object v2, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mTempFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 246
    .local v1, "tmp":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 249
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "tmp":Ljava/io/File;
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileMaker:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileMaker:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileMaker:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 227
    const-string v0, "FileManager"

    const-string v1, "destroy"

    const-string v2, "call interruped"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mbDestroyed:Z

    .line 239
    return-void
.end method

.method public generateFileList(Landroid/content/Intent;Landroid/os/Handler;)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "resultHandler"    # Landroid/os/Handler;

    .prologue
    .line 72
    iput-object p2, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    .line 73
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileList:Ljava/util/ArrayList;

    .line 74
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mTempFileList:Ljava/util/ArrayList;

    .line 76
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "action":Ljava/lang/String;
    const-string v8, "FileManager"

    const-string v9, "generateFileList"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "action : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v8, "FileManager"

    const-string v9, "generateFileList"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "type : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v8, "android.intent.action.SCONNECT"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 81
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 83
    .local v5, "stream":Landroid/net/Uri;
    if-eqz v5, :cond_3

    .line 84
    new-instance v2, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;

    const/4 v8, 0x0

    invoke-direct {v2, p0, v8}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;-><init>(Lcom/samsung/android/app/FileShareClient/FileManager;Lcom/samsung/android/app/FileShareClient/FileManager$1;)V

    .line 85
    .local v2, "fileMaker":Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v5, v8}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->addUri(Landroid/net/Uri;Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_1

    .line 86
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 218
    .end local v2    # "fileMaker":Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;
    .end local v5    # "stream":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 90
    .restart local v2    # "fileMaker":Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;
    .restart local v5    # "stream":Landroid/net/Uri;
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->needtoStartThread()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 91
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 92
    new-instance v8, Ljava/lang/Thread;

    invoke-direct {v8, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileMaker:Ljava/lang/Thread;

    .line 93
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileMaker:Ljava/lang/Thread;

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 95
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 101
    .end local v2    # "fileMaker":Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;
    :cond_3
    const-string v8, "android.intent.extra.TEXT"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 102
    .local v1, "extra_text":Ljava/lang/CharSequence;
    if-eqz v1, :cond_0

    .line 107
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/FileShareClient/FileManager;->createHtml(Ljava/lang/CharSequence;)Ljava/io/File;

    move-result-object v3

    .line 108
    .local v3, "html":Ljava/io/File;
    if-eqz v3, :cond_4

    .line 109
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mTempFileList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 113
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 118
    .end local v1    # "extra_text":Ljava/lang/CharSequence;
    .end local v3    # "html":Ljava/io/File;
    .end local v5    # "stream":Landroid/net/Uri;
    :cond_5
    const-string v8, "android.intent.action.SCONNECT_MULTIPLE"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 119
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 120
    .local v7, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 122
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 123
    :cond_6
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 124
    const-string v8, "FileManager"

    const-string v9, "generateFileList"

    const-string v10, "invalid extra"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_7
    new-instance v2, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;

    const/4 v8, 0x0

    invoke-direct {v2, p0, v8}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;-><init>(Lcom/samsung/android/app/FileShareClient/FileManager;Lcom/samsung/android/app/FileShareClient/FileManager$1;)V

    .line 130
    .restart local v2    # "fileMaker":Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    .line 131
    .local v6, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v6, v8}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->addUri(Landroid/net/Uri;Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_8

    .line 132
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 137
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_9
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->needtoStartThread()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 138
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 139
    new-instance v8, Ljava/lang/Thread;

    invoke-direct {v8, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileMaker:Ljava/lang/Thread;

    .line 140
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileMaker:Ljava/lang/Thread;

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 142
    :cond_a
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 145
    .end local v2    # "fileMaker":Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v7    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_b
    const-string v8, "android.intent.action.SEND"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 146
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 148
    .restart local v5    # "stream":Landroid/net/Uri;
    if-eqz v5, :cond_e

    .line 149
    new-instance v2, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;

    const/4 v8, 0x0

    invoke-direct {v2, p0, v8}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;-><init>(Lcom/samsung/android/app/FileShareClient/FileManager;Lcom/samsung/android/app/FileShareClient/FileManager$1;)V

    .line 150
    .restart local v2    # "fileMaker":Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v5, v8}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->addUri(Landroid/net/Uri;Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_c

    .line 151
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 155
    :cond_c
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->needtoStartThread()Z

    move-result v8

    if-eqz v8, :cond_d

    .line 156
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 157
    new-instance v8, Ljava/lang/Thread;

    invoke-direct {v8, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileMaker:Ljava/lang/Thread;

    .line 158
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileMaker:Ljava/lang/Thread;

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 160
    :cond_d
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 166
    .end local v2    # "fileMaker":Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;
    :cond_e
    const-string v8, "android.intent.extra.TEXT"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 167
    .restart local v1    # "extra_text":Ljava/lang/CharSequence;
    if-eqz v1, :cond_0

    .line 172
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/FileShareClient/FileManager;->createHtml(Ljava/lang/CharSequence;)Ljava/io/File;

    move-result-object v3

    .line 173
    .restart local v3    # "html":Ljava/io/File;
    if-eqz v3, :cond_f

    .line 174
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mTempFileList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 178
    :cond_f
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 183
    .end local v1    # "extra_text":Ljava/lang/CharSequence;
    .end local v3    # "html":Ljava/io/File;
    .end local v5    # "stream":Landroid/net/Uri;
    :cond_10
    const-string v8, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 184
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 185
    .restart local v7    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 187
    if-eqz v7, :cond_11

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_12

    .line 188
    :cond_11
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 189
    const-string v8, "FileManager"

    const-string v9, "generateFileList"

    const-string v10, "invalid extra"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 193
    :cond_12
    new-instance v2, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;

    const/4 v8, 0x0

    invoke-direct {v2, p0, v8}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;-><init>(Lcom/samsung/android/app/FileShareClient/FileManager;Lcom/samsung/android/app/FileShareClient/FileManager$1;)V

    .line 195
    .restart local v2    # "fileMaker":Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_13
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_14

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    .line 196
    .restart local v6    # "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v6, v8}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->addUri(Landroid/net/Uri;Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_13

    .line 197
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 202
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_14
    invoke-virtual {v2}, Lcom/samsung/android/app/FileShareClient/FileManager$FileMaker;->needtoStartThread()Z

    move-result v8

    if-eqz v8, :cond_15

    .line 203
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 204
    new-instance v8, Ljava/lang/Thread;

    invoke-direct {v8, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileMaker:Ljava/lang/Thread;

    .line 205
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileMaker:Ljava/lang/Thread;

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 207
    :cond_15
    iget-object v8, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mResultHandler:Landroid/os/Handler;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method

.method public getFileArryList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mFileList:Ljava/util/ArrayList;

    return-object v0
.end method

.method getMultiLiskList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "srcStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 786
    if-nez p1, :cond_1

    .line 787
    const-string v5, "FileManager"

    const-string v6, "getMultiLiskList"

    const-string v7, "srcStr is null"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/FileShareClient/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    const/4 v4, 0x0

    .line 804
    :cond_0
    return-object v4

    .line 791
    :cond_1
    const-string v0, "\\(?\\b(http://|https://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]"

    .line 792
    .local v0, "URL_REGEX":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 793
    .local v4, "urlList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 794
    .local v2, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 796
    .local v1, "matcher":Ljava/util/regex/Matcher;
    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 797
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    .line 798
    .local v3, "url":Ljava/lang/String;
    const-string v5, "("

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, ")"

    invoke-virtual {v3, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 799
    const/4 v5, 0x1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 801
    :cond_2
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getSourceFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/app/FileShareClient/FileManager;->mRetFilePath:Ljava/lang/String;

    return-object v0
.end method
