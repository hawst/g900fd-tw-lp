.class public Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;
.super Ljava/lang/Object;
.source "UsageEvent.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/usage/UsageEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UsageSuccessListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# static fields
.field private static _instance:Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;->_instance:Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;

    if-nez v0, :cond_0

    .line 186
    new-instance v0, Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;

    invoke-direct {v0}, Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;-><init>()V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;->_instance:Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;

    .line 188
    :cond_0
    sget-object v0, Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;->_instance:Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 180
    check-cast p1, Lorg/json/JSONObject;

    invoke-virtual {p0, p1}, Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 195
    :try_start_0
    const-string v0, "result"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 196
    if-nez v0, :cond_0

    .line 197
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "Usage report failed %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 200
    :catch_0
    move-exception v0

    .line 201
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "Usage report failed %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
