.class public final enum Lcom/flipboard/usage/UsageEvent$EventCategory;
.super Ljava/lang/Enum;
.source "UsageEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/usage/UsageEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EventCategory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flipboard/usage/UsageEvent$EventCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/flipboard/usage/UsageEvent$EventCategory;

.field public static final enum general:Lcom/flipboard/usage/UsageEvent$EventCategory;

.field public static final enum item:Lcom/flipboard/usage/UsageEvent$EventCategory;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/flipboard/usage/UsageEvent$EventCategory;

    const-string v1, "general"

    invoke-direct {v0, v1, v2}, Lcom/flipboard/usage/UsageEvent$EventCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$EventCategory;->general:Lcom/flipboard/usage/UsageEvent$EventCategory;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$EventCategory;

    const-string v1, "item"

    invoke-direct {v0, v1, v3}, Lcom/flipboard/usage/UsageEvent$EventCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$EventCategory;->item:Lcom/flipboard/usage/UsageEvent$EventCategory;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/flipboard/usage/UsageEvent$EventCategory;

    sget-object v1, Lcom/flipboard/usage/UsageEvent$EventCategory;->general:Lcom/flipboard/usage/UsageEvent$EventCategory;

    aput-object v1, v0, v2

    sget-object v1, Lcom/flipboard/usage/UsageEvent$EventCategory;->item:Lcom/flipboard/usage/UsageEvent$EventCategory;

    aput-object v1, v0, v3

    sput-object v0, Lcom/flipboard/usage/UsageEvent$EventCategory;->$VALUES:[Lcom/flipboard/usage/UsageEvent$EventCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flipboard/usage/UsageEvent$EventCategory;
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/flipboard/usage/UsageEvent$EventCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flipboard/usage/UsageEvent$EventCategory;

    return-object v0
.end method

.method public static values()[Lcom/flipboard/usage/UsageEvent$EventCategory;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/flipboard/usage/UsageEvent$EventCategory;->$VALUES:[Lcom/flipboard/usage/UsageEvent$EventCategory;

    invoke-virtual {v0}, [Lcom/flipboard/usage/UsageEvent$EventCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/flipboard/usage/UsageEvent$EventCategory;

    return-object v0
.end method
