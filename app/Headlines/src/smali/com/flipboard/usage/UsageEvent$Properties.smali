.class public Lcom/flipboard/usage/UsageEvent$Properties;
.super Ljava/lang/Object;
.source "UsageEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/usage/UsageEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Properties"
.end annotation


# instance fields
.field public app_version:Ljava/lang/String;

.field public appmode:Ljava/lang/String;

.field public device_model:Ljava/lang/String;

.field public device_version:Ljava/lang/String;

.field public lang:Ljava/lang/String;

.field public locale:Ljava/lang/String;

.field public os:Ljava/lang/String;

.field public os_version:Ljava/lang/String;

.field public time:J

.field public time_offset:I

.field public unique_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-static {}, Lcom/flipboard/data/Request;->getInstance()Lcom/flipboard/data/Request;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flipboard/data/Request;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flipboard/usage/UsageEvent$Properties;->unique_id:Ljava/lang/String;

    .line 72
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/flipboard/usage/UsageEvent$Properties;->time:J

    .line 73
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 74
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/2addr v0, v1

    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    div-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/flipboard/usage/UsageEvent$Properties;->time_offset:I

    .line 75
    iput-object p2, p0, Lcom/flipboard/usage/UsageEvent$Properties;->app_version:Ljava/lang/String;

    .line 76
    const-string v0, "android"

    iput-object v0, p0, Lcom/flipboard/usage/UsageEvent$Properties;->os:Ljava/lang/String;

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flipboard/usage/UsageEvent$Properties;->os_version:Ljava/lang/String;

    .line 78
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, p0, Lcom/flipboard/usage/UsageEvent$Properties;->device_model:Ljava/lang/String;

    .line 79
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/flipboard/usage/UsageEvent$Properties;->device_version:Ljava/lang/String;

    .line 80
    invoke-static {p1}, Lcom/flipboard/util/FDLUtil;->appMode(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flipboard/usage/UsageEvent$Properties;->appmode:Ljava/lang/String;

    .line 81
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/flipboard/usage/UsageEvent$Properties;->locale:Ljava/lang/String;

    .line 83
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flipboard/usage/UsageEvent$Properties;->lang:Ljava/lang/String;

    .line 84
    return-void
.end method


# virtual methods
.method public toJson()Lorg/json/JSONObject;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 87
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 88
    const-string v1, "unique_id"

    iget-object v2, p0, Lcom/flipboard/usage/UsageEvent$Properties;->unique_id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 89
    const-string v1, "time"

    iget-wide v2, p0, Lcom/flipboard/usage/UsageEvent$Properties;->time:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 90
    const-string v1, "time_offset"

    iget v2, p0, Lcom/flipboard/usage/UsageEvent$Properties;->time_offset:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 91
    const-string v1, "app_version"

    iget-object v2, p0, Lcom/flipboard/usage/UsageEvent$Properties;->app_version:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 92
    const-string v1, "locale"

    iget-object v2, p0, Lcom/flipboard/usage/UsageEvent$Properties;->locale:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 93
    const-string v1, "appmode"

    iget-object v2, p0, Lcom/flipboard/usage/UsageEvent$Properties;->appmode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 94
    const-string v1, "device_model"

    iget-object v2, p0, Lcom/flipboard/usage/UsageEvent$Properties;->device_model:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 95
    const-string v1, "device_version"

    iget-object v2, p0, Lcom/flipboard/usage/UsageEvent$Properties;->device_version:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 96
    const-string v1, "network"

    const-string v2, "unknown"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 97
    const-string v1, "os"

    iget-object v2, p0, Lcom/flipboard/usage/UsageEvent$Properties;->os:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 98
    const-string v1, "os_version"

    iget-object v2, p0, Lcom/flipboard/usage/UsageEvent$Properties;->os_version:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 99
    return-object v0
.end method
