.class public final enum Lcom/flipboard/usage/UsageEvent$CommonEventData;
.super Ljava/lang/Enum;
.source "UsageEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/usage/UsageEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CommonEventData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flipboard/usage/UsageEvent$CommonEventData;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/flipboard/usage/UsageEvent$CommonEventData;

.field public static final enum app_id:Lcom/flipboard/usage/UsageEvent$CommonEventData;

.field public static final enum error:Lcom/flipboard/usage/UsageEvent$CommonEventData;

.field public static final enum flip_count:Lcom/flipboard/usage/UsageEvent$CommonEventData;

.field public static final enum item_partner_id:Lcom/flipboard/usage/UsageEvent$CommonEventData;

.field public static final enum load_time:Lcom/flipboard/usage/UsageEvent$CommonEventData;

.field public static final enum nav_from:Lcom/flipboard/usage/UsageEvent$CommonEventData;

.field public static final enum page_num:Lcom/flipboard/usage/UsageEvent$CommonEventData;

.field public static final enum partner_id:Lcom/flipboard/usage/UsageEvent$CommonEventData;

.field public static final enum time_spent:Lcom/flipboard/usage/UsageEvent$CommonEventData;

.field public static final enum url:Lcom/flipboard/usage/UsageEvent$CommonEventData;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 41
    new-instance v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;

    const-string v1, "app_id"

    invoke-direct {v0, v1, v3}, Lcom/flipboard/usage/UsageEvent$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;->app_id:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;

    const-string v1, "nav_from"

    invoke-direct {v0, v1, v4}, Lcom/flipboard/usage/UsageEvent$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;->nav_from:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;

    const-string v1, "time_spent"

    invoke-direct {v0, v1, v5}, Lcom/flipboard/usage/UsageEvent$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;->time_spent:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;

    const-string v1, "partner_id"

    invoke-direct {v0, v1, v6}, Lcom/flipboard/usage/UsageEvent$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;->partner_id:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;

    const-string v1, "item_partner_id"

    invoke-direct {v0, v1, v7}, Lcom/flipboard/usage/UsageEvent$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;->item_partner_id:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;

    const-string v1, "url"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/flipboard/usage/UsageEvent$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;->url:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;

    const-string v1, "page_num"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/flipboard/usage/UsageEvent$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;->page_num:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;

    const-string v1, "flip_count"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/flipboard/usage/UsageEvent$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;->flip_count:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;

    const-string v1, "error"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/flipboard/usage/UsageEvent$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;->error:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;

    const-string v1, "load_time"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/flipboard/usage/UsageEvent$CommonEventData;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;->load_time:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/flipboard/usage/UsageEvent$CommonEventData;

    sget-object v1, Lcom/flipboard/usage/UsageEvent$CommonEventData;->app_id:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    aput-object v1, v0, v3

    sget-object v1, Lcom/flipboard/usage/UsageEvent$CommonEventData;->nav_from:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    aput-object v1, v0, v4

    sget-object v1, Lcom/flipboard/usage/UsageEvent$CommonEventData;->time_spent:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    aput-object v1, v0, v5

    sget-object v1, Lcom/flipboard/usage/UsageEvent$CommonEventData;->partner_id:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    aput-object v1, v0, v6

    sget-object v1, Lcom/flipboard/usage/UsageEvent$CommonEventData;->item_partner_id:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/flipboard/usage/UsageEvent$CommonEventData;->url:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/flipboard/usage/UsageEvent$CommonEventData;->page_num:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/flipboard/usage/UsageEvent$CommonEventData;->flip_count:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/flipboard/usage/UsageEvent$CommonEventData;->error:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/flipboard/usage/UsageEvent$CommonEventData;->load_time:Lcom/flipboard/usage/UsageEvent$CommonEventData;

    aput-object v2, v0, v1

    sput-object v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;->$VALUES:[Lcom/flipboard/usage/UsageEvent$CommonEventData;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flipboard/usage/UsageEvent$CommonEventData;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;

    return-object v0
.end method

.method public static values()[Lcom/flipboard/usage/UsageEvent$CommonEventData;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/flipboard/usage/UsageEvent$CommonEventData;->$VALUES:[Lcom/flipboard/usage/UsageEvent$CommonEventData;

    invoke-virtual {v0}, [Lcom/flipboard/usage/UsageEvent$CommonEventData;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/flipboard/usage/UsageEvent$CommonEventData;

    return-object v0
.end method
