.class public final enum Lcom/flipboard/usage/UsageEvent$EventAction;
.super Ljava/lang/Enum;
.source "UsageEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/usage/UsageEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EventAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flipboard/usage/UsageEvent$EventAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/flipboard/usage/UsageEvent$EventAction;

.field public static final enum enter:Lcom/flipboard/usage/UsageEvent$EventAction;

.field public static final enum error:Lcom/flipboard/usage/UsageEvent$EventAction;

.field public static final enum item_load:Lcom/flipboard/usage/UsageEvent$EventAction;

.field public static final enum viewed:Lcom/flipboard/usage/UsageEvent$EventAction;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lcom/flipboard/usage/UsageEvent$EventAction;

    const-string v1, "enter"

    invoke-direct {v0, v1, v2}, Lcom/flipboard/usage/UsageEvent$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$EventAction;->enter:Lcom/flipboard/usage/UsageEvent$EventAction;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$EventAction;

    const-string v1, "item_load"

    invoke-direct {v0, v1, v3}, Lcom/flipboard/usage/UsageEvent$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$EventAction;->item_load:Lcom/flipboard/usage/UsageEvent$EventAction;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$EventAction;

    const-string v1, "viewed"

    invoke-direct {v0, v1, v4}, Lcom/flipboard/usage/UsageEvent$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$EventAction;->viewed:Lcom/flipboard/usage/UsageEvent$EventAction;

    new-instance v0, Lcom/flipboard/usage/UsageEvent$EventAction;

    const-string v1, "error"

    invoke-direct {v0, v1, v5}, Lcom/flipboard/usage/UsageEvent$EventAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$EventAction;->error:Lcom/flipboard/usage/UsageEvent$EventAction;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/flipboard/usage/UsageEvent$EventAction;

    sget-object v1, Lcom/flipboard/usage/UsageEvent$EventAction;->enter:Lcom/flipboard/usage/UsageEvent$EventAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/flipboard/usage/UsageEvent$EventAction;->item_load:Lcom/flipboard/usage/UsageEvent$EventAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/flipboard/usage/UsageEvent$EventAction;->viewed:Lcom/flipboard/usage/UsageEvent$EventAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/flipboard/usage/UsageEvent$EventAction;->error:Lcom/flipboard/usage/UsageEvent$EventAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/flipboard/usage/UsageEvent$EventAction;->$VALUES:[Lcom/flipboard/usage/UsageEvent$EventAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flipboard/usage/UsageEvent$EventAction;
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/flipboard/usage/UsageEvent$EventAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flipboard/usage/UsageEvent$EventAction;

    return-object v0
.end method

.method public static values()[Lcom/flipboard/usage/UsageEvent$EventAction;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/flipboard/usage/UsageEvent$EventAction;->$VALUES:[Lcom/flipboard/usage/UsageEvent$EventAction;

    invoke-virtual {v0}, [Lcom/flipboard/usage/UsageEvent$EventAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/flipboard/usage/UsageEvent$EventAction;

    return-object v0
.end method
