.class public Lcom/flipboard/usage/UsageEvent$UsageErrorListener;
.super Ljava/lang/Object;
.source "UsageEvent.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/usage/UsageEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UsageErrorListener"
.end annotation


# static fields
.field private static _instance:Lcom/flipboard/usage/UsageEvent$UsageErrorListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/flipboard/usage/UsageEvent$UsageErrorListener;
    .locals 1

    .prologue
    .line 212
    sget-object v0, Lcom/flipboard/usage/UsageEvent$UsageErrorListener;->_instance:Lcom/flipboard/usage/UsageEvent$UsageErrorListener;

    if-nez v0, :cond_0

    .line 213
    new-instance v0, Lcom/flipboard/usage/UsageEvent$UsageErrorListener;

    invoke-direct {v0}, Lcom/flipboard/usage/UsageEvent$UsageErrorListener;-><init>()V

    sput-object v0, Lcom/flipboard/usage/UsageEvent$UsageErrorListener;->_instance:Lcom/flipboard/usage/UsageEvent$UsageErrorListener;

    .line 215
    :cond_0
    sget-object v0, Lcom/flipboard/usage/UsageEvent$UsageErrorListener;->_instance:Lcom/flipboard/usage/UsageEvent$UsageErrorListener;

    return-object v0
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 5

    .prologue
    .line 220
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "Usage report failed %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 221
    return-void
.end method
