.class public Lcom/flipboard/usage/UsageEvent;
.super Ljava/lang/Object;
.source "UsageEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipboard/usage/UsageEvent$UsageErrorListener;,
        Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;,
        Lcom/flipboard/usage/UsageEvent$Properties;,
        Lcom/flipboard/usage/UsageEvent$CommonEventData;,
        Lcom/flipboard/usage/UsageEvent$EventCategory;,
        Lcom/flipboard/usage/UsageEvent$EventAction;
    }
.end annotation


# static fields
.field private static final USAGE_CHINA_URL:Ljava/lang/String; = "https://ue.flipboard.cn/usage"

.field private static final USAGE_URL:Ljava/lang/String; = "https://ue.flipboard.com/usage"

.field private static final USAGE_URL_DEV:Ljava/lang/String; = "https://ue-test.flipboard.com/usage"

.field static log:Lcom/flipboard/util/Log;


# instance fields
.field public final event_action:Lcom/flipboard/usage/UsageEvent$EventAction;

.field public final event_category:Lcom/flipboard/usage/UsageEvent$EventCategory;

.field public event_data:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public transient next:Lcom/flipboard/usage/UsageEvent;

.field public final prod_type:Ljava/lang/String;

.field public properties:Lcom/flipboard/usage/UsageEvent$Properties;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "usage"

    invoke-static {v0}, Lcom/flipboard/util/Log;->getLog(Ljava/lang/String;)Lcom/flipboard/util/Log;

    move-result-object v0

    sput-object v0, Lcom/flipboard/usage/UsageEvent;->log:Lcom/flipboard/util/Log;

    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Lcom/flipboard/usage/UsageEvent$EventAction;Lcom/flipboard/usage/UsageEvent$EventCategory;)V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-string v0, "embedded"

    iput-object v0, p0, Lcom/flipboard/usage/UsageEvent;->prod_type:Ljava/lang/String;

    .line 104
    iput-object p3, p0, Lcom/flipboard/usage/UsageEvent;->event_action:Lcom/flipboard/usage/UsageEvent$EventAction;

    .line 105
    iput-object p4, p0, Lcom/flipboard/usage/UsageEvent;->event_category:Lcom/flipboard/usage/UsageEvent$EventCategory;

    .line 106
    new-instance v0, Lcom/flipboard/usage/UsageEvent$Properties;

    invoke-direct {v0, p1, p2}, Lcom/flipboard/usage/UsageEvent$Properties;-><init>(ZLjava/lang/String;)V

    iput-object v0, p0, Lcom/flipboard/usage/UsageEvent;->properties:Lcom/flipboard/usage/UsageEvent$Properties;

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/flipboard/usage/UsageEvent;->event_data:Ljava/util/Map;

    .line 108
    return-void
.end method


# virtual methods
.method public getUsuageURL(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    if-eqz p1, :cond_0

    const-string v0, "https://ue.flipboard.cn/usage"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "https://ue.flipboard.com/usage"

    goto :goto_0
.end method

.method public set(Lcom/flipboard/usage/UsageEvent$CommonEventData;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p1}, Lcom/flipboard/usage/UsageEvent$CommonEventData;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/flipboard/usage/UsageEvent;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 127
    return-void
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 112
    if-nez p1, :cond_0

    .line 114
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_0
    if-nez p2, :cond_1

    .line 117
    iget-object v0, p0, Lcom/flipboard/usage/UsageEvent;->event_data:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    :goto_0
    return-void

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/flipboard/usage/UsageEvent;->event_data:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public submit(Z)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 150
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    .line 151
    if-nez v0, :cond_0

    .line 152
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "Currently not support submit usage before Volley Request is initialized. Usage report abort!"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    :goto_0
    return-void

    .line 157
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/flipboard/usage/UsageEvent;->toJson()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 162
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 163
    const-string v1, "data"

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v3, "POST usage data %s"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v1, v3, v5}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    new-instance v0, Lcom/flipboard/usage/UsageEvent$1;

    invoke-virtual {p0, p1}, Lcom/flipboard/usage/UsageEvent;->getUsuageURL(Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;->getInstance()Lcom/flipboard/usage/UsageEvent$UsageSuccessListener;

    move-result-object v5

    invoke-static {}, Lcom/flipboard/usage/UsageEvent$UsageErrorListener;->getInstance()Lcom/flipboard/usage/UsageEvent$UsageErrorListener;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/flipboard/usage/UsageEvent$1;-><init>(Lcom/flipboard/usage/UsageEvent;ILjava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 173
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    goto :goto_0

    .line 158
    :catch_0
    move-exception v0

    .line 159
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v3, "Cannot covert usageEvent object to json string: %s"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-virtual {v1, v3, v2}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 131
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 132
    const-string v0, "event_action"

    iget-object v2, p0, Lcom/flipboard/usage/UsageEvent;->event_action:Lcom/flipboard/usage/UsageEvent$EventAction;

    invoke-virtual {v2}, Lcom/flipboard/usage/UsageEvent$EventAction;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 133
    const-string v0, "event_category"

    iget-object v2, p0, Lcom/flipboard/usage/UsageEvent;->event_category:Lcom/flipboard/usage/UsageEvent$EventCategory;

    invoke-virtual {v2}, Lcom/flipboard/usage/UsageEvent$EventCategory;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 134
    const-string v0, "prod_type"

    const-string v2, "embedded"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 136
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 137
    iget-object v0, p0, Lcom/flipboard/usage/UsageEvent;->event_data:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 138
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 140
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 142
    :cond_0
    const-string v0, "event_data"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 143
    const-string v0, "properties"

    iget-object v2, p0, Lcom/flipboard/usage/UsageEvent;->properties:Lcom/flipboard/usage/UsageEvent$Properties;

    invoke-virtual {v2}, Lcom/flipboard/usage/UsageEvent$Properties;->toJson()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 144
    return-object v1
.end method
