.class Lcom/flipboard/ecg/ContentGuide$1;
.super Lcom/flipboard/data/Request$FLSuccessListener;
.source "ContentGuide.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flipboard/ecg/ContentGuide;->getGuide()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipboard/ecg/ContentGuide;


# direct methods
.method constructor <init>(Lcom/flipboard/ecg/ContentGuide;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/flipboard/ecg/ContentGuide$1;->this$0:Lcom/flipboard/ecg/ContentGuide;

    invoke-direct {p0}, Lcom/flipboard/data/Request$FLSuccessListener;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 242
    check-cast p1, Lorg/json/JSONObject;

    invoke-virtual {p0, p1}, Lcom/flipboard/ecg/ContentGuide$1;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 245
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide$1;->this$0:Lcom/flipboard/ecg/ContentGuide;

    # getter for: Lcom/flipboard/ecg/ContentGuide;->category:Ljava/lang/String;
    invoke-static {v0}, Lcom/flipboard/ecg/ContentGuide;->access$000(Lcom/flipboard/ecg/ContentGuide;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "\'%s\'"

    new-array v1, v10, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/flipboard/ecg/ContentGuide$1;->this$0:Lcom/flipboard/ecg/ContentGuide;

    # getter for: Lcom/flipboard/ecg/ContentGuide;->category:Ljava/lang/String;
    invoke-static {v3}, Lcom/flipboard/ecg/ContentGuide;->access$000(Lcom/flipboard/ecg/ContentGuide;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v9

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 246
    :goto_0
    iget-object v1, p0, Lcom/flipboard/ecg/ContentGuide$1;->this$0:Lcom/flipboard/ecg/ContentGuide;

    # getter for: Lcom/flipboard/ecg/ContentGuide;->sections:Ljava/util/List;
    invoke-static {v1}, Lcom/flipboard/ecg/ContentGuide;->access$100(Lcom/flipboard/ecg/ContentGuide;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/flipboard/ecg/ContentGuide$1;->this$0:Lcom/flipboard/ecg/ContentGuide;

    # getter for: Lcom/flipboard/ecg/ContentGuide;->sections:Ljava/util/List;
    invoke-static {v1}, Lcom/flipboard/ecg/ContentGuide;->access$100(Lcom/flipboard/ecg/ContentGuide;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    new-instance v1, Lorg/json/JSONArray;

    iget-object v3, p0, Lcom/flipboard/ecg/ContentGuide$1;->this$0:Lcom/flipboard/ecg/ContentGuide;

    # getter for: Lcom/flipboard/ecg/ContentGuide;->sections:Ljava/util/List;
    invoke-static {v3}, Lcom/flipboard/ecg/ContentGuide;->access$100(Lcom/flipboard/ecg/ContentGuide;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, v3}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 247
    :goto_1
    if-eqz v1, :cond_2

    const-string v3, "\'%s\'"

    new-array v4, v10, [Ljava/lang/Object;

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "\'"

    const-string v6, "\\\\\'"

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v9

    invoke-static {v3, v4}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 249
    :goto_2
    iget-object v3, p0, Lcom/flipboard/ecg/ContentGuide$1;->this$0:Lcom/flipboard/ecg/ContentGuide;

    const-string v4, "javascript:window.ECGsetGuideData(\'%s\', %s, %s);"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "\'"

    const-string v8, "\\\\\'"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    aput-object v0, v5, v10

    const/4 v0, 0x2

    aput-object v1, v5, v0

    invoke-static {v4, v5}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/flipboard/ecg/ContentGuide;->loadUrl(Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide$1;->this$0:Lcom/flipboard/ecg/ContentGuide;

    # getter for: Lcom/flipboard/ecg/ContentGuide;->observers:Lcom/flipboard/util/Observable$Proxy;
    invoke-static {v0}, Lcom/flipboard/ecg/ContentGuide;->access$200(Lcom/flipboard/ecg/ContentGuide;)Lcom/flipboard/util/Observable$Proxy;

    move-result-object v0

    sget-object v1, Lcom/flipboard/ecg/ContentGuide$Message;->READY:Lcom/flipboard/ecg/ContentGuide$Message;

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Observable$Proxy;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 251
    return-void

    .line 245
    :cond_0
    const-string v0, "undefined"

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 246
    goto :goto_1

    .line 247
    :cond_2
    const-string v1, "undefined"

    goto :goto_2
.end method
