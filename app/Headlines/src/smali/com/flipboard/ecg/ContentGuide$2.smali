.class Lcom/flipboard/ecg/ContentGuide$2;
.super Lcom/flipboard/data/Request$FLErrorListener;
.source "ContentGuide.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flipboard/ecg/ContentGuide;->getGuide()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipboard/ecg/ContentGuide;


# direct methods
.method constructor <init>(Lcom/flipboard/ecg/ContentGuide;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/flipboard/ecg/ContentGuide$2;->this$0:Lcom/flipboard/ecg/ContentGuide;

    invoke-direct {p0}, Lcom/flipboard/data/Request$FLErrorListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 5

    .prologue
    .line 256
    iget-object v1, p0, Lcom/flipboard/ecg/ContentGuide$2;->this$0:Lcom/flipboard/ecg/ContentGuide;

    const-string v2, "Error: (%s), %s"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    iget v0, v0, Lcom/android/volley/NetworkResponse;->statusCode:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/flipboard/ecg/ContentGuide;->error(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/flipboard/ecg/ContentGuide;->access$300(Lcom/flipboard/ecg/ContentGuide;Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide$2;->this$0:Lcom/flipboard/ecg/ContentGuide;

    const-string v1, "javascript:window.ECGsetGuideData(\'{}\');"

    invoke-virtual {v0, v1}, Lcom/flipboard/ecg/ContentGuide;->loadUrl(Ljava/lang/String;)V

    .line 259
    return-void

    .line 256
    :cond_0
    const-string v0, "?"

    goto :goto_0
.end method
