.class public Lcom/flipboard/ecg/ContentGuide;
.super Lcom/flipboard/gui/FDLWebView;
.source "ContentGuide.java"

# interfaces
.implements Lcom/flipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipboard/ecg/ContentGuide$3;,
        Lcom/flipboard/ecg/ContentGuide$ECGInterface;,
        Lcom/flipboard/ecg/ContentGuide$Message;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/flipboard/gui/FDLWebView;",
        "Lcom/flipboard/util/Observer",
        "<",
        "Lcom/flipboard/data/Request;",
        "Lcom/flipboard/data/Request$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final ECG_HTML_LOCATION:Ljava/lang/String; = "http://cdn.flipboard.com.s3.amazonaws.com/samsung/ecg/ecg.html"


# instance fields
.field private category:Ljava/lang/String;

.field private clientId:Ljava/lang/String;

.field private jsInterface:Lcom/flipboard/ecg/ContentGuide$ECGInterface;

.field private locale:Ljava/lang/String;

.field private observers:Lcom/flipboard/util/Observable$Proxy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipboard/util/Observable$Proxy",
            "<",
            "Lcom/flipboard/ecg/ContentGuide;",
            "Lcom/flipboard/ecg/ContentGuide$Message;",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field private request:Lcom/flipboard/data/Request;

.field private sections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0, p1}, Lcom/flipboard/gui/FDLWebView;-><init>(Landroid/content/Context;)V

    .line 35
    new-instance v0, Lcom/flipboard/ecg/ContentGuide$ECGInterface;

    invoke-direct {v0, p0}, Lcom/flipboard/ecg/ContentGuide$ECGInterface;-><init>(Lcom/flipboard/ecg/ContentGuide;)V

    iput-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->jsInterface:Lcom/flipboard/ecg/ContentGuide$ECGInterface;

    .line 36
    new-instance v0, Lcom/flipboard/util/Observable$Proxy;

    invoke-direct {v0, p0}, Lcom/flipboard/util/Observable$Proxy;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->observers:Lcom/flipboard/util/Observable$Proxy;

    .line 62
    invoke-direct {p0, p2, v1, v1, v1}, Lcom/flipboard/ecg/ContentGuide;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0, p1}, Lcom/flipboard/gui/FDLWebView;-><init>(Landroid/content/Context;)V

    .line 35
    new-instance v0, Lcom/flipboard/ecg/ContentGuide$ECGInterface;

    invoke-direct {v0, p0}, Lcom/flipboard/ecg/ContentGuide$ECGInterface;-><init>(Lcom/flipboard/ecg/ContentGuide;)V

    iput-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->jsInterface:Lcom/flipboard/ecg/ContentGuide$ECGInterface;

    .line 36
    new-instance v0, Lcom/flipboard/util/Observable$Proxy;

    invoke-direct {v0, p0}, Lcom/flipboard/util/Observable$Proxy;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->observers:Lcom/flipboard/util/Observable$Proxy;

    .line 79
    invoke-direct {p0, p2, p3, v1, v1}, Lcom/flipboard/ecg/ContentGuide;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/flipboard/gui/FDLWebView;-><init>(Landroid/content/Context;)V

    .line 35
    new-instance v0, Lcom/flipboard/ecg/ContentGuide$ECGInterface;

    invoke-direct {v0, p0}, Lcom/flipboard/ecg/ContentGuide$ECGInterface;-><init>(Lcom/flipboard/ecg/ContentGuide;)V

    iput-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->jsInterface:Lcom/flipboard/ecg/ContentGuide$ECGInterface;

    .line 36
    new-instance v0, Lcom/flipboard/util/Observable$Proxy;

    invoke-direct {v0, p0}, Lcom/flipboard/util/Observable$Proxy;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->observers:Lcom/flipboard/util/Observable$Proxy;

    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, p4, v0}, Lcom/flipboard/ecg/ContentGuide;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/flipboard/gui/FDLWebView;-><init>(Landroid/content/Context;)V

    .line 35
    new-instance v0, Lcom/flipboard/ecg/ContentGuide$ECGInterface;

    invoke-direct {v0, p0}, Lcom/flipboard/ecg/ContentGuide$ECGInterface;-><init>(Lcom/flipboard/ecg/ContentGuide;)V

    iput-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->jsInterface:Lcom/flipboard/ecg/ContentGuide$ECGInterface;

    .line 36
    new-instance v0, Lcom/flipboard/util/Observable$Proxy;

    invoke-direct {v0, p0}, Lcom/flipboard/util/Observable$Proxy;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->observers:Lcom/flipboard/util/Observable$Proxy;

    .line 119
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/flipboard/ecg/ContentGuide;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/flipboard/ecg/ContentGuide;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->category:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/flipboard/ecg/ContentGuide;)Ljava/util/List;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->sections:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/flipboard/ecg/ContentGuide;)Lcom/flipboard/util/Observable$Proxy;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->observers:Lcom/flipboard/util/Observable$Proxy;

    return-object v0
.end method

.method static synthetic access$300(Lcom/flipboard/ecg/ContentGuide;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/flipboard/ecg/ContentGuide;->error(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/flipboard/ecg/ContentGuide;Lcom/flipboard/ecg/ContentGuide$Message;Lorg/json/JSONObject;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/flipboard/ecg/ContentGuide;->notifyObservers(Lcom/flipboard/ecg/ContentGuide$Message;Lorg/json/JSONObject;)V

    return-void
.end method

.method private error(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 219
    const-string v0, "javascript:window.ECGsetError(\'%s\');"

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/flipboard/ecg/ContentGuide;->loadUrl(Ljava/lang/String;)V

    .line 220
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "%s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 223
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 224
    const-string v1, "success"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 225
    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 226
    iget-object v1, p0, Lcom/flipboard/ecg/ContentGuide;->observers:Lcom/flipboard/util/Observable$Proxy;

    sget-object v2, Lcom/flipboard/ecg/ContentGuide$Message;->ERROR:Lcom/flipboard/ecg/ContentGuide$Message;

    invoke-virtual {v1, v2, v0}, Lcom/flipboard/util/Observable$Proxy;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :goto_0
    return-void

    .line 227
    :catch_0
    move-exception v0

    .line 228
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    invoke-virtual {v1, v0}, Lcom/flipboard/util/Log;->error(Ljava/lang/Throwable;)V

    .line 229
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->observers:Lcom/flipboard/util/Observable$Proxy;

    sget-object v1, Lcom/flipboard/ecg/ContentGuide$Message;->ERROR:Lcom/flipboard/ecg/ContentGuide$Message;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Observable$Proxy;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private getGuide()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 241
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->request:Lcom/flipboard/data/Request;

    new-instance v1, Lcom/flipboard/ecg/ContentGuide$1;

    invoke-direct {v1, p0}, Lcom/flipboard/ecg/ContentGuide$1;-><init>(Lcom/flipboard/ecg/ContentGuide;)V

    new-instance v2, Lcom/flipboard/ecg/ContentGuide$2;

    invoke-direct {v2, p0}, Lcom/flipboard/ecg/ContentGuide$2;-><init>(Lcom/flipboard/ecg/ContentGuide;)V

    iget-object v3, p0, Lcom/flipboard/ecg/ContentGuide;->locale:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/flipboard/data/Request;->getGuide(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;Ljava/lang/String;)V

    .line 261
    return-void
.end method

.method private init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 173
    iput-object p2, p0, Lcom/flipboard/ecg/ContentGuide;->locale:Ljava/lang/String;

    .line 174
    iput-object p3, p0, Lcom/flipboard/ecg/ContentGuide;->category:Ljava/lang/String;

    .line 175
    iput-object p1, p0, Lcom/flipboard/ecg/ContentGuide;->clientId:Ljava/lang/String;

    .line 176
    iput-object p4, p0, Lcom/flipboard/ecg/ContentGuide;->sections:Ljava/util/List;

    .line 178
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->jsInterface:Lcom/flipboard/ecg/ContentGuide$ECGInterface;

    const-string v1, "ecgInterface"

    invoke-virtual {p0, v0, v1}, Lcom/flipboard/ecg/ContentGuide;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    const-string v0, "http://cdn.flipboard.com.s3.amazonaws.com/samsung/ecg/ecg.html"

    invoke-virtual {p0, v0}, Lcom/flipboard/ecg/ContentGuide;->loadUrl(Ljava/lang/String;)V

    .line 180
    return-void
.end method

.method private notifyObservers(Lcom/flipboard/ecg/ContentGuide$Message;Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->observers:Lcom/flipboard/util/Observable$Proxy;

    invoke-virtual {v0, p1, p2}, Lcom/flipboard/util/Observable$Proxy;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 283
    return-void
.end method


# virtual methods
.method public addObserver(Lcom/flipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipboard/util/Observer",
            "<",
            "Lcom/flipboard/ecg/ContentGuide;",
            "Lcom/flipboard/ecg/ContentGuide$Message;",
            "Lorg/json/JSONObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->observers:Lcom/flipboard/util/Observable$Proxy;

    invoke-virtual {v0, p1}, Lcom/flipboard/util/Observable$Proxy;->addObserver(Lcom/flipboard/util/Observer;)V

    .line 272
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 290
    invoke-super {p0}, Lcom/flipboard/gui/FDLWebView;->destroy()V

    .line 291
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->request:Lcom/flipboard/data/Request;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->request:Lcom/flipboard/data/Request;

    invoke-virtual {v0, p0}, Lcom/flipboard/data/Request;->removeObserver(Lcom/flipboard/util/Observer;)V

    .line 294
    :cond_0
    return-void
.end method

.method public init()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->request:Lcom/flipboard/data/Request;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->request:Lcom/flipboard/data/Request;

    invoke-virtual {v0, p0}, Lcom/flipboard/data/Request;->removeObserver(Lcom/flipboard/util/Observer;)V

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/flipboard/ecg/ContentGuide;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/flipboard/ecg/ContentGuide;->clientId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/flipboard/data/Request;->init(Landroid/content/Context;Ljava/lang/String;)Lcom/flipboard/data/Request;

    move-result-object v0

    iput-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->request:Lcom/flipboard/data/Request;

    .line 189
    iget-object v0, p0, Lcom/flipboard/ecg/ContentGuide;->request:Lcom/flipboard/data/Request;

    invoke-virtual {v0, p0}, Lcom/flipboard/data/Request;->addObserver(Lcom/flipboard/util/Observer;)V

    .line 190
    return-void
.end method

.method public notify(Lcom/flipboard/data/Request;Lcom/flipboard/data/Request$Message;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 197
    sget-object v0, Lcom/flipboard/ecg/ContentGuide$3;->$SwitchMap$com$flipboard$data$Request$Message:[I

    invoke-virtual {p2}, Lcom/flipboard/data/Request$Message;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 208
    const-string v0, "Something went wrong: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/flipboard/ecg/ContentGuide;->error(Ljava/lang/String;)V

    .line 210
    :goto_0
    return-void

    .line 200
    :pswitch_0
    :try_start_0
    invoke-direct {p0}, Lcom/flipboard/ecg/ContentGuide;->getGuide()V
    :try_end_0
    .catch Lcom/flipboard/data/FDLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    invoke-virtual {v1, v0}, Lcom/flipboard/util/Log;->error(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 197
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic notify(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lcom/flipboard/data/Request;

    check-cast p2, Lcom/flipboard/data/Request$Message;

    invoke-virtual {p0, p1, p2, p3}, Lcom/flipboard/ecg/ContentGuide;->notify(Lcom/flipboard/data/Request;Lcom/flipboard/data/Request$Message;Ljava/lang/Object;)V

    return-void
.end method

.method public refreshGuide()V
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/flipboard/ecg/ContentGuide;->refreshGuide(Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method public refreshGuide(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/flipboard/ecg/ContentGuide;->refreshGuide(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public refreshGuide(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 154
    iput-object p1, p0, Lcom/flipboard/ecg/ContentGuide;->locale:Ljava/lang/String;

    .line 155
    iput-object p2, p0, Lcom/flipboard/ecg/ContentGuide;->category:Ljava/lang/String;

    .line 157
    :try_start_0
    invoke-direct {p0}, Lcom/flipboard/ecg/ContentGuide;->getGuide()V
    :try_end_0
    .catch Lcom/flipboard/data/FDLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v0

    .line 159
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    invoke-virtual {v1, v0}, Lcom/flipboard/util/Log;->error(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
