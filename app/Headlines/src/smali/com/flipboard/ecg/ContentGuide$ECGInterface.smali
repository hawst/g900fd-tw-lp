.class Lcom/flipboard/ecg/ContentGuide$ECGInterface;
.super Ljava/lang/Object;
.source "ContentGuide.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/ecg/ContentGuide;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ECGInterface"
.end annotation


# instance fields
.field contentGuide:Lcom/flipboard/ecg/ContentGuide;


# direct methods
.method public constructor <init>(Lcom/flipboard/ecg/ContentGuide;)V
    .locals 0

    .prologue
    .line 304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 305
    iput-object p1, p0, Lcom/flipboard/ecg/ContentGuide$ECGInterface;->contentGuide:Lcom/flipboard/ecg/ContentGuide;

    .line 306
    return-void
.end method


# virtual methods
.method public rowDeselected(Ljava/lang/String;)V
    .locals 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 317
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 318
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "row deselected: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 319
    iget-object v1, p0, Lcom/flipboard/ecg/ContentGuide$ECGInterface;->contentGuide:Lcom/flipboard/ecg/ContentGuide;

    sget-object v2, Lcom/flipboard/ecg/ContentGuide$Message;->ITEM_DESELECTED:Lcom/flipboard/ecg/ContentGuide$Message;

    # invokes: Lcom/flipboard/ecg/ContentGuide;->notifyObservers(Lcom/flipboard/ecg/ContentGuide$Message;Lorg/json/JSONObject;)V
    invoke-static {v1, v2, v0}, Lcom/flipboard/ecg/ContentGuide;->access$400(Lcom/flipboard/ecg/ContentGuide;Lcom/flipboard/ecg/ContentGuide$Message;Lorg/json/JSONObject;)V

    .line 320
    return-void
.end method

.method public rowSelected(Ljava/lang/String;)V
    .locals 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 310
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 311
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "row selected: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 312
    iget-object v1, p0, Lcom/flipboard/ecg/ContentGuide$ECGInterface;->contentGuide:Lcom/flipboard/ecg/ContentGuide;

    sget-object v2, Lcom/flipboard/ecg/ContentGuide$Message;->ITEM_SELECTED:Lcom/flipboard/ecg/ContentGuide$Message;

    # invokes: Lcom/flipboard/ecg/ContentGuide;->notifyObservers(Lcom/flipboard/ecg/ContentGuide$Message;Lorg/json/JSONObject;)V
    invoke-static {v1, v2, v0}, Lcom/flipboard/ecg/ContentGuide;->access$400(Lcom/flipboard/ecg/ContentGuide;Lcom/flipboard/ecg/ContentGuide$Message;Lorg/json/JSONObject;)V

    .line 313
    return-void
.end method
