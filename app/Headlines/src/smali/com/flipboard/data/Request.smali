.class public Lcom/flipboard/data/Request;
.super Lcom/flipboard/util/Observable;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipboard/data/Request$11;,
        Lcom/flipboard/data/Request$FLErrorListener;,
        Lcom/flipboard/data/Request$FLSuccessListener;,
        Lcom/flipboard/data/Request$Message;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/flipboard/util/Observable",
        "<",
        "Lcom/flipboard/data/Request;",
        "Lcom/flipboard/data/Request$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final ADS_CLICK:Ljava/lang/String; = "click"

.field private static final ADS_IMPRESSION:Ljava/lang/String; = "impression"

.field private static final ADS_METRIC:Ljava/lang/String; = "metric"

.field private static final ADS_QUERY:Ljava/lang/String; = "query"

.field private static final AUTHORIZE:Ljava/lang/String; = "oauth/authorize"

.field protected static final AUTH_HOST:Ljava/lang/String; = "authorize.redirect"

.field protected static final AUTH_PARAM_CODE:Ljava/lang/String; = "code"

.field protected static final AUTH_PARAM_STATE:Ljava/lang/String; = "state"

.field protected static final AUTH_REDIRECT_URI:Ljava/lang/String;

.field private static final BASE_URL:Ljava/lang/String; = "https://api.flipboard.com"

.field private static final BASE_URL_ADS:Ljava/lang/String; = "https://ad.flipboard.com"

.field private static final BASE_URL_ADS_CHINA:Ljava/lang/String; = "https://ad.flipboard.cn"

.field private static final BASE_URL_CHINA:Ljava/lang/String; = "https://api.flipboard.cn"

.field private static final CATEGORIES:Ljava/lang/String; = "categories"

.field private static final CONTENT_EXTRACT:Ljava/lang/String; = "content/extract"

.field protected static final FDL_SCHEME:Ljava/lang/String; = "fdl"

.field private static final FLIPBOARD_SERVICE_ID:Ljava/lang/String; = "flipboard"

.field private static final GUIDE:Ljava/lang/String; = "guide"

.field private static final ITEMS:Ljava/lang/String; = "items"

.field private static final LOCALES:Ljava/lang/String; = "locales"

.field private static final PREF_ACCESS_TOKEN:Ljava/lang/String; = "com.flipboard.fdl.ACCESS_TOKEN"

.field private static final PREF_REFRESH_TOKEN:Ljava/lang/String; = "com.flipboard.fdl.PREF_REFRESH_TOKEN"

.field private static final SERVICES:Ljava/lang/String; = "services"

.field private static final SERVICE_LOGIN:Ljava/lang/String; = "services/login"

.field protected static final SERVICE_LOGIN_HOST:Ljava/lang/String; = "service.login.redirect"

.field protected static final SERVICE_LOGIN_PARAM_SUCCESS:Ljava/lang/String; = "success"

.field protected static final SERVICE_LOGIN_REDIRECT_URI:Ljava/lang/String;

.field private static final SERVICE_LOGOUT:Ljava/lang/String; = "services/logout"

.field public static final TOKEN:Ljava/lang/String; = "oauth/token"

.field private static final TOKEN_INFO:Ljava/lang/String; = "oauth/tokeninfo"

.field private static instance:Lcom/flipboard/data/Request;

.field private static requestingToken:Z


# instance fields
.field private accessToken:Ljava/lang/String;

.field private china:Z

.field private final clientId:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private deviceId:Ljava/lang/String;

.field private pendingFLJSONRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/flipboard/data/FLJSONRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final randomizer:Ljava/security/SecureRandom;

.field private refreshToken:Ljava/lang/String;

.field private serviceLoginClass:Ljava/lang/Class;

.field private sharedPrefs:Landroid/content/SharedPreferences;

.field private syncedWithFlipboard:Z

.field private tokenAuthClass:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 83
    const-string v0, "%s://%s"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "fdl"

    aput-object v2, v1, v3

    const-string v2, "authorize.redirect"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/flipboard/data/Request;->AUTH_REDIRECT_URI:Ljava/lang/String;

    .line 87
    const-string v0, "%s://%s"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "fdl"

    aput-object v2, v1, v3

    const-string v2, "service.login.redirect"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/flipboard/data/Request;->SERVICE_LOGIN_REDIRECT_URI:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Class;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 130
    invoke-direct {p0}, Lcom/flipboard/util/Observable;-><init>()V

    .line 97
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/flipboard/data/Request;->randomizer:Ljava/security/SecureRandom;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/flipboard/data/Request;->pendingFLJSONRequests:Ljava/util/List;

    .line 131
    iput-object p1, p0, Lcom/flipboard/data/Request;->clientId:Ljava/lang/String;

    .line 132
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    .line 133
    if-eqz p3, :cond_2

    :goto_0
    iput-object p3, p0, Lcom/flipboard/data/Request;->tokenAuthClass:Ljava/lang/Class;

    .line 134
    if-eqz p4, :cond_3

    :goto_1
    iput-object p4, p0, Lcom/flipboard/data/Request;->serviceLoginClass:Ljava/lang/Class;

    .line 135
    iput-boolean p6, p0, Lcom/flipboard/data/Request;->china:Z

    .line 136
    const-string v0, "com.flipboard.fdl.sharedPreferences"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/flipboard/data/Request;->sharedPrefs:Landroid/content/SharedPreferences;

    .line 137
    iget-object v0, p0, Lcom/flipboard/data/Request;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v1, "deviceId"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flipboard/data/Request;->deviceId:Ljava/lang/String;

    .line 138
    iget-object v0, p0, Lcom/flipboard/data/Request;->deviceId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 139
    invoke-static {p2}, Lcom/flipboard/util/FDLUtil;->generateDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flipboard/data/Request;->deviceId:Ljava/lang/String;

    .line 140
    iget-object v0, p0, Lcom/flipboard/data/Request;->sharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "deviceId"

    iget-object v2, p0, Lcom/flipboard/data/Request;->deviceId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 142
    :cond_0
    invoke-static {p2}, Lcom/flipboard/data/FDLVolley;->init(Landroid/content/Context;)V

    .line 144
    if-eqz p7, :cond_4

    if-eqz p8, :cond_4

    .line 146
    iput-object p7, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    .line 147
    iput-object p8, p0, Lcom/flipboard/data/Request;->refreshToken:Ljava/lang/String;

    .line 148
    invoke-direct {p0, p7, p8, v3}, Lcom/flipboard/data/Request;->saveTokens(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :goto_2
    iput-boolean p5, p0, Lcom/flipboard/data/Request;->syncedWithFlipboard:Z

    .line 156
    sput-object p0, Lcom/flipboard/data/Request;->instance:Lcom/flipboard/data/Request;

    .line 158
    iget-boolean v0, p0, Lcom/flipboard/data/Request;->syncedWithFlipboard:Z

    if-eqz v0, :cond_1

    .line 159
    invoke-virtual {p0}, Lcom/flipboard/data/Request;->updateTokensFromBroadcast()V

    .line 161
    :cond_1
    return-void

    .line 133
    :cond_2
    const-class p3, Lcom/flipboard/gui/FDLWebViewActivity;

    goto :goto_0

    .line 134
    :cond_3
    const-class p4, Lcom/flipboard/gui/FDLWebViewActivity;

    goto :goto_1

    .line 150
    :cond_4
    iget-object v0, p0, Lcom/flipboard/data/Request;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v1, "com.flipboard.fdl.ACCESS_TOKEN"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    .line 151
    iget-object v0, p0, Lcom/flipboard/data/Request;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v1, "com.flipboard.fdl.PREF_REFRESH_TOKEN"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flipboard/data/Request;->refreshToken:Ljava/lang/String;

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/flipboard/data/Request;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/flipboard/data/Request;->syncedWithFlipboard:Z

    return v0
.end method

.method static synthetic access$100(Lcom/flipboard/data/Request;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/flipboard/data/Request;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/flipboard/data/Request;->refreshToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/flipboard/data/Request;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/flipboard/data/Request;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/flipboard/data/Request;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/flipboard/data/Request;->getToken(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400()Z
    .locals 1

    .prologue
    .line 42
    sget-boolean v0, Lcom/flipboard/data/Request;->requestingToken:Z

    return v0
.end method

.method static synthetic access$402(Z)Z
    .locals 0

    .prologue
    .line 42
    sput-boolean p0, Lcom/flipboard/data/Request;->requestingToken:Z

    return p0
.end method

.method static synthetic access$500(Lcom/flipboard/data/Request;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/flipboard/data/Request;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/flipboard/data/Request;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/flipboard/data/Request;->saveTokens(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/flipboard/data/Request;)Ljava/util/List;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/flipboard/data/Request;->pendingFLJSONRequests:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$800(Lcom/flipboard/data/Request;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/flipboard/data/Request;->replaceAccessToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/flipboard/data/Request;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/flipboard/data/Request;->sharedPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private authorize(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 5

    .prologue
    .line 861
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 862
    iget-object v1, p0, Lcom/flipboard/data/Request;->randomizer:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 863
    const/16 v1, 0xb

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 866
    invoke-static {}, Lcom/flipboard/data/AuthorizationClient;->getInstance()Lcom/flipboard/data/AuthorizationClient;

    move-result-object v1

    .line 867
    new-instance v2, Lcom/flipboard/data/Request$8;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/flipboard/data/Request$8;-><init>(Lcom/flipboard/data/Request;Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V

    invoke-virtual {v1, v2}, Lcom/flipboard/data/AuthorizationClient;->addObserver(Lcom/flipboard/util/Observer;)V

    .line 899
    const-string v1, "oauth/authorize"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "response_type"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "code"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "client_id"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/flipboard/data/Request;->clientId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "redirect_uri"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    sget-object v4, Lcom/flipboard/data/Request;->AUTH_REDIRECT_URI:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "state"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    aput-object v0, v2, v3

    invoke-direct {p0, v1, v2}, Lcom/flipboard/data/Request;->getAPIAuthURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 902
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/flipboard/data/Request;->tokenAuthClass:Ljava/lang/Class;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 903
    const-string v2, "com.flipboard.fdl.URL"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 904
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 905
    iget-object v0, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 906
    return-void
.end method

.method private varargs constructAPIURL(Ljava/lang/StringBuilder;Z[Ljava/lang/Object;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/16 v9, 0x3d

    .line 1242
    if-eqz p2, :cond_1

    const-string v0, "&"

    :goto_0
    move-object v3, v0

    move v0, v1

    .line 1244
    :goto_1
    array-length v2, p3

    if-ge v0, v2, :cond_5

    .line 1245
    add-int/lit8 v4, v0, 0x1

    aget-object v5, p3, v0

    .line 1246
    add-int/lit8 v2, v4, 0x1

    aget-object v0, p3, v4

    .line 1247
    if-nez v0, :cond_2

    .line 1261
    :cond_0
    :goto_2
    if-nez p2, :cond_6

    .line 1262
    const/4 p2, 0x1

    .line 1263
    const-string v0, "&"

    :goto_3
    move-object v3, v0

    move v0, v2

    .line 1265
    goto :goto_1

    .line 1242
    :cond_1
    const-string v0, "?"

    goto :goto_0

    .line 1249
    :cond_2
    instance-of v4, v0, [Ljava/lang/Object;

    if-eqz v4, :cond_3

    .line 1250
    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    array-length v6, v0

    move v4, v1

    :goto_4
    if-ge v4, v6, :cond_0

    aget-object v7, v0, v4

    .line 1251
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/flipboard/util/FDLUtil;->escapeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1250
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1253
    :cond_3
    instance-of v4, v0, Ljava/util/List;

    if-eqz v4, :cond_4

    .line 1254
    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 1255
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/flipboard/util/FDLUtil;->escapeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 1258
    :cond_4
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/flipboard/util/FDLUtil;->escapeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1266
    :cond_5
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_6
    move-object v0, v3

    goto :goto_3
.end method

.method private getAPIAuthURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1162
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, p1, v0}, Lcom/flipboard/data/Request;->getAPIAuthURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private varargs getAPIAuthURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1172
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x80

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1173
    iget-boolean v0, p0, Lcom/flipboard/data/Request;->china:Z

    if-eqz v0, :cond_0

    const-string v0, "https://api.flipboard.cn"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1174
    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1175
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1176
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, p2}, Lcom/flipboard/data/Request;->constructAPIURL(Ljava/lang/StringBuilder;Z[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1173
    :cond_0
    const-string v0, "https://api.flipboard.com"

    goto :goto_0
.end method

.method private getAPIURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 1167
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, p1, v0}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private varargs getAPIURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 1181
    iget-object v0, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1182
    new-instance v0, Lcom/flipboard/data/FDLException;

    const-string v1, "no accessToken has been received"

    invoke-direct {v0, v1}, Lcom/flipboard/data/FDLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1185
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x80

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1186
    iget-boolean v0, p0, Lcom/flipboard/data/Request;->china:Z

    if-eqz v0, :cond_1

    const-string v0, "https://api.flipboard.cn"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1187
    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1188
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189
    const-string v0, "?access_token=%s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1190
    invoke-direct {p0, v1, v5, p2}, Lcom/flipboard/data/Request;->constructAPIURL(Ljava/lang/StringBuilder;Z[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1186
    :cond_1
    const-string v0, "https://api.flipboard.com"

    goto :goto_0
.end method

.method private varargs getAPIURLWithoutAccessToken(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 1195
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x80

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1196
    iget-boolean v0, p0, Lcom/flipboard/data/Request;->china:Z

    if-eqz v0, :cond_0

    const-string v0, "https://api.flipboard.cn"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1197
    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1198
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1199
    const-string v0, "?"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1201
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0, p2}, Lcom/flipboard/data/Request;->constructAPIURL(Ljava/lang/StringBuilder;Z[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1196
    :cond_0
    const-string v0, "https://api.flipboard.com"

    goto :goto_0
.end method

.method public static getInstance()Lcom/flipboard/data/Request;
    .locals 1

    .prologue
    .line 297
    sget-object v0, Lcom/flipboard/data/Request;->instance:Lcom/flipboard/data/Request;

    return-object v0
.end method

.method private getToken(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 818
    new-instance v0, Lcom/flipboard/data/Request$6;

    invoke-direct {v0, p0}, Lcom/flipboard/data/Request$6;-><init>(Lcom/flipboard/data/Request;)V

    new-instance v1, Lcom/flipboard/data/Request$7;

    invoke-direct {v1, p0}, Lcom/flipboard/data/Request$7;-><init>(Lcom/flipboard/data/Request;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/flipboard/data/Request;->token(Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V

    .line 849
    return-void
.end method

.method public static init(Landroid/content/Context;Ljava/lang/String;)Lcom/flipboard/data/Request;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 261
    const/4 v0, 0x0

    invoke-static {p0, p1, v1, v1, v0}, Lcom/flipboard/data/Request;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Z)Lcom/flipboard/data/Request;

    move-result-object v0

    return-object v0
.end method

.method public static init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Lcom/flipboard/data/Request;
    .locals 2

    .prologue
    .line 248
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/flipboard/data/Request;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Z)Lcom/flipboard/data/Request;

    move-result-object v0

    return-object v0
.end method

.method public static init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)Lcom/flipboard/data/Request;
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/flipboard/data/Request;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Z)Lcom/flipboard/data/Request;

    move-result-object v0

    return-object v0
.end method

.method public static init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Z)Lcom/flipboard/data/Request;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 177
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    move-object v7, v6

    invoke-static/range {v0 .. v7}, Lcom/flipboard/data/Request;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;ZZLjava/lang/String;Ljava/lang/String;)Lcom/flipboard/data/Request;

    move-result-object v0

    return-object v0
.end method

.method public static init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;ZZ)Lcom/flipboard/data/Request;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 194
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v7, v6

    invoke-static/range {v0 .. v7}, Lcom/flipboard/data/Request;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;ZZLjava/lang/String;Ljava/lang/String;)Lcom/flipboard/data/Request;

    move-result-object v0

    return-object v0
.end method

.method public static init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;ZZLjava/lang/String;Ljava/lang/String;)Lcom/flipboard/data/Request;
    .locals 9

    .prologue
    .line 213
    sget-object v0, Lcom/flipboard/data/Request;->instance:Lcom/flipboard/data/Request;

    if-nez v0, :cond_0

    .line 214
    new-instance v0, Lcom/flipboard/data/Request;

    move-object v1, p1

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/flipboard/data/Request;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Class;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 217
    :cond_0
    sget-boolean v0, Lcom/flipboard/data/Request;->requestingToken:Z

    if-nez v0, :cond_1

    .line 218
    const/4 v0, 0x1

    sput-boolean v0, Lcom/flipboard/data/Request;->requestingToken:Z

    .line 219
    sget-object v0, Lcom/flipboard/data/Request;->instance:Lcom/flipboard/data/Request;

    invoke-direct {v0}, Lcom/flipboard/data/Request;->requestToken()V

    .line 221
    :cond_1
    sget-object v0, Lcom/flipboard/data/Request;->instance:Lcom/flipboard/data/Request;

    return-object v0
.end method

.method public static init(Landroid/content/Context;Ljava/lang/String;Z)Lcom/flipboard/data/Request;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 287
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v5, p2

    move-object v6, v2

    move-object v7, v2

    invoke-static/range {v0 .. v7}, Lcom/flipboard/data/Request;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;ZZLjava/lang/String;Ljava/lang/String;)Lcom/flipboard/data/Request;

    move-result-object v0

    return-object v0
.end method

.method public static init(Landroid/content/Context;Ljava/lang/String;ZZ)Lcom/flipboard/data/Request;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 274
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v4, p2

    move v5, p3

    move-object v6, v2

    move-object v7, v2

    invoke-static/range {v0 .. v7}, Lcom/flipboard/data/Request;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;ZZLjava/lang/String;Ljava/lang/String;)Lcom/flipboard/data/Request;

    move-result-object v0

    return-object v0
.end method

.method private replaceAccessToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1271
    const-string v2, "access_token="

    .line 1272
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 1273
    const-string v0, "&"

    .line 1274
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_1

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    move v1, v0

    .line 1275
    :goto_0
    const/4 v0, 0x0

    .line 1276
    if-ltz v3, :cond_0

    if-le v1, v3, :cond_0

    .line 1277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v3

    invoke-virtual {p1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1281
    :cond_0
    return-object v0

    .line 1274
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    move v1, v0

    goto :goto_0
.end method

.method private requestToken()V
    .locals 4

    .prologue
    .line 761
    invoke-virtual {p0}, Lcom/flipboard/data/Request;->hasValidToken()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 762
    iget-boolean v0, p0, Lcom/flipboard/data/Request;->syncedWithFlipboard:Z

    if-eqz v0, :cond_0

    .line 763
    iget-object v0, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/flipboard/util/FDLUtil;->fireTokenBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/flipboard/data/Request;->requestingToken:Z

    .line 810
    :goto_0
    return-void

    .line 769
    :cond_1
    new-instance v0, Lcom/flipboard/data/Request$3;

    invoke-direct {v0, p0}, Lcom/flipboard/data/Request$3;-><init>(Lcom/flipboard/data/Request;)V

    new-instance v1, Lcom/flipboard/data/Request$4;

    invoke-direct {v1, p0}, Lcom/flipboard/data/Request$4;-><init>(Lcom/flipboard/data/Request;)V

    invoke-direct {p0, v0, v1}, Lcom/flipboard/data/Request;->authorize(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V

    .line 799
    new-instance v0, Lcom/flipboard/data/Request$5;

    invoke-direct {v0, p0}, Lcom/flipboard/data/Request$5;-><init>(Lcom/flipboard/data/Request;)V

    .line 809
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    const-wide/16 v2, 0x4e20

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private saveTokens(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x6

    .line 1146
    if-eqz p3, :cond_0

    const-string v0, "flipboard"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/flipboard/data/Request;->refreshToken:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1147
    :cond_1
    iput-object p1, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    .line 1148
    iput-object p2, p0, Lcom/flipboard/data/Request;->refreshToken:Ljava/lang/String;

    .line 1149
    iget-object v0, p0, Lcom/flipboard/data/Request;->sharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.flipboard.fdl.ACCESS_TOKEN"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.flipboard.fdl.PREF_REFRESH_TOKEN"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1151
    sget-object v0, Lcom/flipboard/data/Request$Message;->ACCOUNT_SYNC:Lcom/flipboard/data/Request$Message;

    invoke-virtual {p0, v0, p3}, Lcom/flipboard/data/Request;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1152
    iget-boolean v0, p0, Lcom/flipboard/data/Request;->syncedWithFlipboard:Z

    if-eqz v0, :cond_2

    .line 1153
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "FDL: saved service changed and token, then firebroadcast [service: %s] [accessToken: %s] [refreshToken: %s]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    const/4 v3, 0x1

    invoke-static {v5, p1}, Lcom/flipboard/util/Log;->maskString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v5, p2}, Lcom/flipboard/util/Log;->maskString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1155
    iget-object v0, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/flipboard/util/FDLUtil;->fireTokenBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    :cond_2
    return-void
.end method

.method private token(Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 6

    .prologue
    .line 917
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 918
    const-string v0, "grant_type"

    const-string v1, "authorization_code"

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 919
    const-string v0, "client_id"

    iget-object v1, p0, Lcom/flipboard/data/Request;->clientId:Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 920
    const-string v0, "code"

    invoke-interface {v3, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 921
    const-string v0, "redirect_uri"

    sget-object v1, Lcom/flipboard/data/Request;->AUTH_REDIRECT_URI:Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 923
    new-instance v0, Lcom/flipboard/data/FLJSONRequest;

    const/4 v1, 0x1

    const-string v2, "oauth/token"

    invoke-direct {p0, v2}, Lcom/flipboard/data/Request;->getAPIAuthURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 924
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 925
    return-void
.end method

.method private tokenInfo(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 936
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    new-instance v1, Lcom/flipboard/data/FLJSONRequest;

    const/4 v2, 0x0

    const-string v3, "oauth/tokeninfo"

    invoke-direct {p0, v3}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v0, v1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 937
    return-void
.end method


# virtual methods
.method public adClick(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;ZLjava/lang/String;J)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1100
    const-string v0, "click"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "click_value"

    aput-object v2, v1, v4

    aput-object p4, v1, v5

    const/4 v2, 0x2

    const-string v3, "click_timestamp"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, p3, v1}, Lcom/flipboard/data/Request;->getFlintUrl(Ljava/lang/String;Z[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1101
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "Fdl request ad/click %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1103
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    new-instance v2, Lcom/flipboard/data/FLJSONRequest;

    invoke-direct {v2, v4, v0, p1, p2}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v1, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 1104
    return-void
.end method

.method public adImpression(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;ZLjava/lang/String;Ljava/lang/String;J)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1063
    const-string v0, "impression"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "impression_value"

    aput-object v2, v1, v4

    aput-object p4, v1, v5

    const/4 v2, 0x2

    const-string v3, "impression_event"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p5, v1, v2

    const/4 v2, 0x4

    const-string v3, "impression_timestamp"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, p3, v1}, Lcom/flipboard/data/Request;->getFlintUrl(Ljava/lang/String;Z[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1064
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "Fdl request ad/impression %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1066
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    new-instance v2, Lcom/flipboard/data/FLJSONRequest;

    invoke-direct {v2, v4, v0, p1, p2}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v1, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 1067
    return-void
.end method

.method public adMetric(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;ZLjava/lang/String;JJ)V
    .locals 5

    .prologue
    .line 1079
    const-wide/16 v0, 0x0

    cmp-long v0, p7, v0

    if-lez v0, :cond_0

    .line 1080
    const-string v0, "metric"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "metric_value"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p4, v1, v2

    const/4 v2, 0x2

    const-string v3, "metric_duration"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "metric_timestamp"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, p3, v1}, Lcom/flipboard/data/Request;->getFlintUrl(Ljava/lang/String;Z[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1084
    :goto_0
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "Fdl request ad/metric %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1085
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    new-instance v2, Lcom/flipboard/data/FLJSONRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0, p1, p2}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v1, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 1086
    return-void

    .line 1082
    :cond_0
    const-string v0, "metric"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "metric_value"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p4, v1, v2

    const/4 v2, 0x2

    const-string v3, "metric_timestamp"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, p3, v1}, Lcom/flipboard/data/Request;->getFlintUrl(Ljava/lang/String;Z[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public adQuery(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;ZZILjava/lang/String;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1020
    if-lez p10, :cond_2

    invoke-static/range {p10 .. p10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 1021
    :goto_0
    if-nez p6, :cond_0

    if-eqz p7, :cond_3

    .line 1022
    :cond_0
    const-string v1, "query"

    const/16 v2, 0x12

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "feed_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p11, v2, v3

    const/4 v3, 0x2

    const-string v4, "pages_since_last_ad"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    const/4 v0, 0x4

    const-string v3, "impression_value"

    aput-object v3, v2, v0

    const/4 v0, 0x5

    aput-object p6, v2, v0

    const/4 v0, 0x6

    const-string v3, "impression_event"

    aput-object v3, v2, v0

    const/4 v0, 0x7

    aput-object p7, v2, v0

    const/16 v0, 0x8

    const-string v3, "impression_timestamp"

    aput-object v3, v2, v0

    const/16 v0, 0x9

    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xa

    const-string v3, "content_category"

    aput-object v3, v2, v0

    const/16 v0, 0xb

    aput-object p12, v2, v0

    const/16 v0, 0xc

    const-string v3, "partner_id"

    aput-object v3, v2, v0

    const/16 v0, 0xd

    aput-object p13, v2, v0

    const/16 v0, 0xe

    const-string v3, "page_index"

    aput-object v3, v2, v0

    const/16 v0, 0xf

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0x10

    const-string v3, "screensize"

    aput-object v3, v2, v0

    const/16 v0, 0x11

    iget-object v3, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/flipboard/util/FDLUtil;->getScreenInches(Landroid/content/Context;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p0, v1, p3, v2}, Lcom/flipboard/data/Request;->getFlintUrl(Ljava/lang/String;Z[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1044
    :goto_1
    if-eqz p4, :cond_1

    .line 1045
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&refresh=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1048
    :cond_1
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "Fdl request ad/query %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1050
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    new-instance v2, Lcom/flipboard/data/FLJSONRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0, p1, p2}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v1, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 1051
    return-void

    .line 1020
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1031
    :cond_3
    const-string v1, "query"

    const/16 v2, 0xc

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "feed_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p11, v2, v3

    const/4 v3, 0x2

    const-string v4, "pages_since_last_ad"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    const/4 v0, 0x4

    const-string v3, "content_category"

    aput-object v3, v2, v0

    const/4 v0, 0x5

    aput-object p12, v2, v0

    const/4 v0, 0x6

    const-string v3, "partner_id"

    aput-object v3, v2, v0

    const/4 v0, 0x7

    aput-object p13, v2, v0

    const/16 v0, 0x8

    const-string v3, "page_index"

    aput-object v3, v2, v0

    const/16 v0, 0x9

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xa

    const-string v3, "screensize"

    aput-object v3, v2, v0

    const/16 v0, 0xb

    iget-object v3, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/flipboard/util/FDLUtil;->getScreenInches(Landroid/content/Context;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p0, v1, p3, v2}, Lcom/flipboard/data/Request;->getFlintUrl(Ljava/lang/String;Z[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public declared-synchronized addObserver(Lcom/flipboard/util/Observer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipboard/util/Observer",
            "<",
            "Lcom/flipboard/data/Request;",
            "Lcom/flipboard/data/Request$Message;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1136
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/flipboard/util/Observable;->addObserver(Lcom/flipboard/util/Observer;)V

    .line 1137
    iget-object v0, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1138
    sget-object v0, Lcom/flipboard/data/Request$Message;->READY:Lcom/flipboard/data/Request$Message;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/flipboard/data/Request;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1140
    :cond_0
    monitor-exit p0

    return-void

    .line 1136
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public contentExtract(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;Ljava/lang/String;Ljava/lang/String;JZZLjava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 657
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "eap-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 658
    iget-object v1, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 660
    const-wide/16 v2, 0x0

    cmp-long v1, p5, v2

    if-lez v1, :cond_1

    .line 661
    const-string v1, "content/extract"

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "client"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string v3, "deviceType"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    invoke-static {p8}, Lcom/flipboard/util/FDLUtil;->appMode(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string v3, "screensize"

    aput-object v3, v2, v0

    const/4 v0, 0x5

    iget-object v3, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/flipboard/util/FDLUtil;->getScreenInches(Landroid/content/Context;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x6

    const-string v3, "url"

    aput-object v3, v2, v0

    const/4 v0, 0x7

    aput-object p4, v2, v0

    const/16 v0, 0x8

    const-string v3, "articleDate"

    aput-object v3, v2, v0

    const/16 v0, 0x9

    invoke-static {p5, p6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xa

    const-string v3, "cacheOnly"

    aput-object v3, v2, v0

    const/16 v0, 0xb

    invoke-static {p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xc

    const-string v3, "version"

    aput-object v3, v2, v0

    const/16 v0, 0xd

    aput-object p9, v2, v0

    invoke-direct {p0, v1, v2}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 668
    :goto_0
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "Fdl request content/extract %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 670
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    new-instance v2, Lcom/flipboard/data/FLJSONRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0, p1, p2}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v1, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 684
    :cond_0
    :goto_1
    return-void

    .line 665
    :cond_1
    const-string v1, "content/extract"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "client"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string v3, "deviceType"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    invoke-static {p8}, Lcom/flipboard/util/FDLUtil;->appMode(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string v3, "screensize"

    aput-object v3, v2, v0

    const/4 v0, 0x5

    iget-object v3, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/flipboard/util/FDLUtil;->getScreenInches(Landroid/content/Context;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x6

    const-string v3, "version"

    aput-object v3, v2, v0

    const/4 v0, 0x7

    aput-object p9, v2, v0

    const/16 v0, 0x8

    const-string v3, "url"

    aput-object v3, v2, v0

    const/16 v0, 0x9

    aput-object p4, v2, v0

    invoke-direct {p0, v1, v2}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 671
    :cond_2
    sget-boolean v1, Lcom/flipboard/data/Request;->requestingToken:Z

    if-eqz v1, :cond_0

    .line 673
    const-wide/16 v2, 0x0

    cmp-long v1, p5, v2

    if-lez v1, :cond_3

    .line 674
    const-string v1, "content/extract"

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "client"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string v3, "deviceType"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    invoke-static {p8}, Lcom/flipboard/util/FDLUtil;->appMode(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string v3, "screensize"

    aput-object v3, v2, v0

    const/4 v0, 0x5

    iget-object v3, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/flipboard/util/FDLUtil;->getScreenInches(Landroid/content/Context;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x6

    const-string v3, "url"

    aput-object v3, v2, v0

    const/4 v0, 0x7

    aput-object p4, v2, v0

    const/16 v0, 0x8

    const-string v3, "articleDate"

    aput-object v3, v2, v0

    const/16 v0, 0x9

    invoke-static {p5, p6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xa

    const-string v3, "cacheOnly"

    aput-object v3, v2, v0

    const/16 v0, 0xb

    invoke-static {p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xc

    const-string v3, "version"

    aput-object v3, v2, v0

    const/16 v0, 0xd

    aput-object p9, v2, v0

    invoke-direct {p0, v1, v2}, Lcom/flipboard/data/Request;->getAPIURLWithoutAccessToken(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 680
    :goto_2
    new-instance v1, Lcom/flipboard/data/FLJSONRequest;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0, p1, p2}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 681
    iget-object v0, p0, Lcom/flipboard/data/Request;->pendingFLJSONRequests:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 682
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "Fdl request not send now because token is not ready. Queued to send later when token is ready. content/extract %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 678
    :cond_3
    const-string v1, "content/extract"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "client"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string v3, "deviceType"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    invoke-static {p8}, Lcom/flipboard/util/FDLUtil;->appMode(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string v3, "screensize"

    aput-object v3, v2, v0

    const/4 v0, 0x5

    iget-object v3, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/flipboard/util/FDLUtil;->getScreenInches(Landroid/content/Context;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x6

    const-string v3, "version"

    aput-object v3, v2, v0

    const/4 v0, 0x7

    aput-object p9, v2, v0

    const/16 v0, 0x8

    const-string v3, "url"

    aput-object v3, v2, v0

    const/16 v0, 0x9

    aput-object p4, v2, v0

    invoke-direct {p0, v1, v2}, Lcom/flipboard/data/Request;->getAPIURLWithoutAccessToken(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public contentExtract(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 637
    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move/from16 v9, p5

    move-object/from16 v10, p6

    invoke-virtual/range {v1 .. v10}, Lcom/flipboard/data/Request;->contentExtract(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;Ljava/lang/String;Ljava/lang/String;JZZLjava/lang/String;)V

    .line 638
    return-void
.end method

.method public doHttpGetRetryRequest(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1108
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    new-instance v1, Lcom/flipboard/data/FLJSONRequest;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p3, p1, p2}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v0, v1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 1109
    return-void
.end method

.method public getCategories(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 357
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/flipboard/data/Request;->getCategories(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;Ljava/lang/String;)V

    .line 358
    return-void
.end method

.method public getCategories(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 375
    if-nez p3, :cond_0

    .line 376
    const-string v0, "categories"

    invoke-direct {p0, v0}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 381
    :goto_0
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    new-instance v2, Lcom/flipboard/data/FLJSONRequest;

    invoke-direct {v2, v3, v0, p1, p2}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v1, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 382
    return-void

    .line 378
    :cond_0
    const-string v0, "categories"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "locale"

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1289
    iget-object v0, p0, Lcom/flipboard/data/Request;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1285
    iget-object v0, p0, Lcom/flipboard/data/Request;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public varargs getFlintUrl(Ljava/lang/String;Z[Ljava/lang/Object;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1207
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x80

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1209
    iget-boolean v0, p0, Lcom/flipboard/data/Request;->china:Z

    if-eqz v0, :cond_0

    const-string v0, "https://ad.flipboard.cn"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1210
    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1211
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1213
    const-string v0, "?device=%s&user_id=%s&user_id_type=%s&model=%s&ver=%s"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Lcom/flipboard/util/FDLUtil;->device(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/flipboard/util/FDLUtil;->escapeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    aput-object v3, v2, v6

    const/4 v3, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/flipboard/util/FDLUtil;->getClientPartner()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_access_token"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {}, Lcom/flipboard/util/FDLUtil;->model()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/flipboard/util/FDLUtil;->escapeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "0.67"

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1237
    invoke-direct {p0, v1, v6, p3}, Lcom/flipboard/data/Request;->constructAPIURL(Ljava/lang/StringBuilder;Z[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1209
    :cond_0
    const-string v0, "https://ad.flipboard.com"

    goto :goto_0
.end method

.method public getGuide(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 393
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/flipboard/data/Request;->getGuide(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;Ljava/lang/String;)V

    .line 394
    return-void
.end method

.method public getGuide(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 410
    if-nez p3, :cond_0

    .line 411
    const-string v0, "guide"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "deviceType"

    aput-object v2, v1, v4

    const-string v2, "android"

    aput-object v2, v1, v3

    invoke-direct {p0, v0, v1}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 416
    :goto_0
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    new-instance v2, Lcom/flipboard/data/FLJSONRequest;

    invoke-direct {v2, v4, v0, p1, p2}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v1, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 417
    return-void

    .line 413
    :cond_0
    const-string v0, "guide"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "locale"

    aput-object v2, v1, v4

    aput-object p3, v1, v3

    const-string v2, "deviceType"

    aput-object v2, v1, v5

    const/4 v2, 0x3

    const-string v3, "android"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;ILcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Lcom/flipboard/data/Request$FLSuccessListener;",
            "Lcom/flipboard/data/Request$FLErrorListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 467
    const/4 v4, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move v6, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-virtual/range {v0 .. v9}, Lcom/flipboard/data/Request;->getItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;ZLjava/lang/String;ILjava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V

    .line 468
    return-void
.end method

.method public getItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/flipboard/data/Request$FLSuccessListener;",
            "Lcom/flipboard/data/Request$FLErrorListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 447
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/flipboard/data/Request;->getItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;ILcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V

    .line 448
    return-void
.end method

.method public getItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/flipboard/data/Request$FLSuccessListener;",
            "Lcom/flipboard/data/Request$FLErrorListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 488
    const/4 v4, 0x0

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-virtual/range {v0 .. v9}, Lcom/flipboard/data/Request;->getItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;ZLjava/lang/String;ILjava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V

    .line 489
    return-void
.end method

.method public getItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;ZLjava/lang/String;ILjava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/flipboard/data/Request$FLSuccessListener;",
            "Lcom/flipboard/data/Request$FLErrorListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 514
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 517
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 520
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 521
    const-string v1, ""

    .line 522
    const/4 v0, 0x0

    move v7, v0

    move-object v0, v1

    move v1, v7

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 523
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 524
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_0

    .line 525
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 522
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 529
    :cond_1
    const-string v1, "categories"

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    :cond_2
    if-eqz p2, :cond_5

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 534
    const-string v1, ""

    .line 535
    const/4 v0, 0x0

    move v7, v0

    move-object v0, v1

    move v1, v7

    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 536
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 537
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_3

    .line 538
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 535
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 542
    :cond_4
    const-string v1, "services"

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    :cond_5
    if-eqz p3, :cond_8

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 547
    const-string v1, ""

    .line 548
    const/4 v0, 0x0

    move v7, v0

    move-object v0, v1

    move v1, v7

    :goto_2
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_7

    .line 549
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 550
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_6

    .line 551
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 548
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 555
    :cond_7
    const-string v1, "sections"

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    :cond_8
    const/4 v0, -0x1

    if-le p6, v0, :cond_9

    .line 560
    const-string v0, "count"

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    :cond_9
    if-eqz p7, :cond_a

    .line 565
    const-string v0, "pageKey"

    invoke-interface {v2, v0, p7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 569
    :cond_a
    if-eqz p4, :cond_b

    .line 570
    const-string v0, "topStories"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 574
    :cond_b
    if-eqz p5, :cond_c

    .line 575
    const-string v0, "locale"

    invoke-interface {v2, v0, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579
    :cond_c
    const-string v0, "deviceType"

    const-string v1, "android"

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    .line 582
    const/4 v0, 0x0

    .line 583
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 584
    add-int/lit8 v5, v1, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v1

    .line 585
    add-int/lit8 v1, v5, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v4, v5

    goto :goto_3

    .line 588
    :cond_d
    new-instance v0, Lcom/flipboard/data/FLJSONRequest;

    const/4 v1, 0x1

    const-string v2, "items"

    invoke-direct {p0, v2, v4}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v4, p8

    move-object/from16 v5, p9

    invoke-direct/range {v0 .. v5}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 589
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 590
    return-void
.end method

.method public getLocales(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 428
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    new-instance v1, Lcom/flipboard/data/FLJSONRequest;

    const/4 v2, 0x0

    const-string v3, "locales"

    invoke-direct {p0, v3}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v0, v1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 429
    return-void
.end method

.method public getServices(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 622
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    new-instance v1, Lcom/flipboard/data/FLJSONRequest;

    const-string v2, "services"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "deviceType"

    aput-object v4, v3, v6

    const/4 v4, 0x1

    const-string v5, "android"

    aput-object v5, v3, v4

    invoke-direct {p0, v2, v3}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v6, v2, p1, p2}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v0, v1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 623
    return-void
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getTopStoryItemsInCategory(Ljava/lang/String;Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 606
    if-eqz p1, :cond_0

    .line 607
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 608
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 609
    const/4 v6, -0x1

    move-object v0, p0

    move-object v3, v2

    move-object v5, p2

    move-object v7, v2

    move-object v8, p3

    move-object v9, p4

    invoke-virtual/range {v0 .. v9}, Lcom/flipboard/data/Request;->getItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;ZLjava/lang/String;ILjava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V

    .line 611
    :cond_0
    return-void
.end method

.method public hasValidToken()Z
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flipboard/data/Request;->refreshToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isChina()Z
    .locals 1

    .prologue
    .line 342
    iget-boolean v0, p0, Lcom/flipboard/data/Request;->china:Z

    return v0
.end method

.method protected refreshToken(Lcom/flipboard/data/FLJSONRequest;)V
    .locals 6

    .prologue
    .line 946
    new-instance v4, Lcom/flipboard/data/Request$9;

    invoke-direct {v4, p0, p1}, Lcom/flipboard/data/Request$9;-><init>(Lcom/flipboard/data/Request;Lcom/flipboard/data/FLJSONRequest;)V

    .line 972
    new-instance v5, Lcom/flipboard/data/Request$10;

    invoke-direct {v5, p0, p1}, Lcom/flipboard/data/Request$10;-><init>(Lcom/flipboard/data/Request;Lcom/flipboard/data/FLJSONRequest;)V

    .line 995
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 996
    const-string v0, "grant_type"

    const-string v1, "refresh_token"

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 997
    const-string v0, "refresh_token"

    iget-object v1, p0, Lcom/flipboard/data/Request;->refreshToken:Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 999
    new-instance v0, Lcom/flipboard/data/FLJSONRequest;

    const/4 v1, 0x1

    const-string v2, "oauth/token"

    invoke-direct {p0, v2}, Lcom/flipboard/data/Request;->getAPIAuthURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1000
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 1001
    return-void
.end method

.method public serviceLogin(Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    .line 702
    invoke-static {}, Lcom/flipboard/data/AuthorizationClient;->getInstance()Lcom/flipboard/data/AuthorizationClient;

    move-result-object v0

    .line 703
    new-instance v1, Lcom/flipboard/data/Request$1;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/flipboard/data/Request$1;-><init>(Lcom/flipboard/data/Request;Lcom/flipboard/data/Request$FLSuccessListener;Ljava/lang/String;Lcom/flipboard/data/Request$FLErrorListener;)V

    invoke-virtual {v0, v1}, Lcom/flipboard/data/AuthorizationClient;->addObserver(Lcom/flipboard/util/Observer;)V

    .line 726
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/flipboard/data/Request;->serviceLoginClass:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 727
    const-string v1, "com.flipboard.fdl.URL"

    const-string v2, "services/login"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "service"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    const/4 v4, 0x2

    const-string v5, "redirect_uri"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget-object v5, Lcom/flipboard/data/Request;->SERVICE_LOGIN_REDIRECT_URI:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-direct {p0, v2, v3}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 728
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 729
    iget-object v1, p0, Lcom/flipboard/data/Request;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 730
    return-void
.end method

.method public serviceLogout(Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/flipboard/data/FDLException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 745
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    new-instance v1, Lcom/flipboard/data/FLJSONRequest;

    const-string v2, "services/logout"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "service"

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-direct {p0, v2, v3}, Lcom/flipboard/data/Request;->getAPIURL(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/flipboard/data/Request$2;

    invoke-direct {v3, p0, p1, p2}, Lcom/flipboard/data/Request$2;-><init>(Lcom/flipboard/data/Request;Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;)V

    invoke-direct {v1, v5, v2, v3, p3}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {v0, v1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 754
    return-void
.end method

.method public updateTokensFromBroadcast()V
    .locals 4

    .prologue
    .line 310
    iget-boolean v0, p0, Lcom/flipboard/data/Request;->syncedWithFlipboard:Z

    if-eqz v0, :cond_1

    .line 311
    sget-object v0, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_ACCESS_TOKEN:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_LOGOUT:Z

    if-eqz v0, :cond_1

    .line 312
    :cond_0
    sget-object v0, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_SERVICE:Ljava/lang/String;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_SERVICE:Ljava/lang/String;

    .line 313
    :goto_0
    invoke-static {}, Lcom/flipboard/util/FlipboardBroadcastReceiver;->clearService()V

    .line 315
    sget-object v1, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_ACCESS_TOKEN:Ljava/lang/String;

    sget-object v2, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_REFRESH_TOKEN:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v0}, Lcom/flipboard/data/Request;->saveTokens(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    sget-boolean v1, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_LOGOUT:Z

    if-eqz v1, :cond_1

    .line 318
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "FDL: Received logout broadcast from the app, notify observers of fdl Request"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 319
    sget-object v1, Lcom/flipboard/data/Request$Message;->ACCOUNT_LOGOUT:Lcom/flipboard/data/Request$Message;

    invoke-virtual {p0, v1, v0}, Lcom/flipboard/data/Request;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 323
    :cond_1
    return-void

    .line 312
    :cond_2
    const-string v0, "flipboard"

    goto :goto_0
.end method
