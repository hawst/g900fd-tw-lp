.class public final enum Lcom/flipboard/data/Request$Message;
.super Ljava/lang/Enum;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/data/Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Message"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flipboard/data/Request$Message;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/flipboard/data/Request$Message;

.field public static final enum ACCOUNT_LOGOUT:Lcom/flipboard/data/Request$Message;

.field public static final enum ACCOUNT_SYNC:Lcom/flipboard/data/Request$Message;

.field public static final enum ERROR:Lcom/flipboard/data/Request$Message;

.field public static final enum READY:Lcom/flipboard/data/Request$Message;

.field public static final enum REFRESHING:Lcom/flipboard/data/Request$Message;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    new-instance v0, Lcom/flipboard/data/Request$Message;

    const-string v1, "READY"

    invoke-direct {v0, v1, v2}, Lcom/flipboard/data/Request$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/data/Request$Message;->READY:Lcom/flipboard/data/Request$Message;

    .line 47
    new-instance v0, Lcom/flipboard/data/Request$Message;

    const-string v1, "REFRESHING"

    invoke-direct {v0, v1, v3}, Lcom/flipboard/data/Request$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/data/Request$Message;->REFRESHING:Lcom/flipboard/data/Request$Message;

    .line 48
    new-instance v0, Lcom/flipboard/data/Request$Message;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, Lcom/flipboard/data/Request$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/data/Request$Message;->ERROR:Lcom/flipboard/data/Request$Message;

    .line 49
    new-instance v0, Lcom/flipboard/data/Request$Message;

    const-string v1, "ACCOUNT_SYNC"

    invoke-direct {v0, v1, v5}, Lcom/flipboard/data/Request$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/data/Request$Message;->ACCOUNT_SYNC:Lcom/flipboard/data/Request$Message;

    .line 50
    new-instance v0, Lcom/flipboard/data/Request$Message;

    const-string v1, "ACCOUNT_LOGOUT"

    invoke-direct {v0, v1, v6}, Lcom/flipboard/data/Request$Message;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/data/Request$Message;->ACCOUNT_LOGOUT:Lcom/flipboard/data/Request$Message;

    .line 45
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/flipboard/data/Request$Message;

    sget-object v1, Lcom/flipboard/data/Request$Message;->READY:Lcom/flipboard/data/Request$Message;

    aput-object v1, v0, v2

    sget-object v1, Lcom/flipboard/data/Request$Message;->REFRESHING:Lcom/flipboard/data/Request$Message;

    aput-object v1, v0, v3

    sget-object v1, Lcom/flipboard/data/Request$Message;->ERROR:Lcom/flipboard/data/Request$Message;

    aput-object v1, v0, v4

    sget-object v1, Lcom/flipboard/data/Request$Message;->ACCOUNT_SYNC:Lcom/flipboard/data/Request$Message;

    aput-object v1, v0, v5

    sget-object v1, Lcom/flipboard/data/Request$Message;->ACCOUNT_LOGOUT:Lcom/flipboard/data/Request$Message;

    aput-object v1, v0, v6

    sput-object v0, Lcom/flipboard/data/Request$Message;->$VALUES:[Lcom/flipboard/data/Request$Message;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flipboard/data/Request$Message;
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/flipboard/data/Request$Message;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flipboard/data/Request$Message;

    return-object v0
.end method

.method public static values()[Lcom/flipboard/data/Request$Message;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/flipboard/data/Request$Message;->$VALUES:[Lcom/flipboard/data/Request$Message;

    invoke-virtual {v0}, [Lcom/flipboard/data/Request$Message;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/flipboard/data/Request$Message;

    return-object v0
.end method
