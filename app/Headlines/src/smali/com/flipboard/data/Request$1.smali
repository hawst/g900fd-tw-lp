.class Lcom/flipboard/data/Request$1;
.super Ljava/lang/Object;
.source "Request.java"

# interfaces
.implements Lcom/flipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flipboard/data/Request;->serviceLogin(Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/flipboard/util/Observer",
        "<",
        "Lcom/flipboard/data/AuthorizationClient;",
        "Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;",
        "Lcom/flipboard/data/AuthorizationClient$Response;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipboard/data/Request;

.field final synthetic val$errorListener:Lcom/flipboard/data/Request$FLErrorListener;

.field final synthetic val$serviceId:Ljava/lang/String;

.field final synthetic val$successListener:Lcom/flipboard/data/Request$FLSuccessListener;


# direct methods
.method constructor <init>(Lcom/flipboard/data/Request;Lcom/flipboard/data/Request$FLSuccessListener;Ljava/lang/String;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 0

    .prologue
    .line 704
    iput-object p1, p0, Lcom/flipboard/data/Request$1;->this$0:Lcom/flipboard/data/Request;

    iput-object p2, p0, Lcom/flipboard/data/Request$1;->val$successListener:Lcom/flipboard/data/Request$FLSuccessListener;

    iput-object p3, p0, Lcom/flipboard/data/Request$1;->val$serviceId:Ljava/lang/String;

    iput-object p4, p0, Lcom/flipboard/data/Request$1;->val$errorListener:Lcom/flipboard/data/Request$FLErrorListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public notify(Lcom/flipboard/data/AuthorizationClient;Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;Lcom/flipboard/data/AuthorizationClient$Response;)V
    .locals 3

    .prologue
    .line 708
    sget-object v0, Lcom/flipboard/data/Request$11;->$SwitchMap$com$flipboard$data$AuthorizationClient$AuthorizeMessage:[I

    invoke-virtual {p2}, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 721
    :cond_0
    :goto_0
    invoke-virtual {p1, p0}, Lcom/flipboard/data/AuthorizationClient;->removeObserver(Lcom/flipboard/util/Observer;)V

    .line 722
    return-void

    .line 710
    :pswitch_0
    iget-object v0, p0, Lcom/flipboard/data/Request$1;->val$successListener:Lcom/flipboard/data/Request$FLSuccessListener;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flipboard/data/Request$FLSuccessListener;->onResponse(Ljava/lang/Object;)V

    .line 711
    iget-object v0, p0, Lcom/flipboard/data/Request$1;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->syncedWithFlipboard:Z
    invoke-static {v0}, Lcom/flipboard/data/Request;->access$000(Lcom/flipboard/data/Request;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 712
    iget-object v0, p0, Lcom/flipboard/data/Request$1;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/flipboard/data/Request;->access$100(Lcom/flipboard/data/Request;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/flipboard/data/Request$1;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;
    invoke-static {v1}, Lcom/flipboard/data/Request;->access$200(Lcom/flipboard/data/Request;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/flipboard/data/Request$1;->val$serviceId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/flipboard/util/FDLUtil;->fireTokenBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 716
    :pswitch_1
    iget-object v0, p0, Lcom/flipboard/data/Request$1;->val$errorListener:Lcom/flipboard/data/Request$FLErrorListener;

    new-instance v1, Lcom/android/volley/VolleyError;

    iget-object v2, p3, Lcom/flipboard/data/AuthorizationClient$Response;->message:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/flipboard/data/Request$FLErrorListener;->onErrorResponse(Lcom/android/volley/VolleyError;)V

    goto :goto_0

    .line 708
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic notify(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 704
    check-cast p1, Lcom/flipboard/data/AuthorizationClient;

    check-cast p2, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;

    check-cast p3, Lcom/flipboard/data/AuthorizationClient$Response;

    invoke-virtual {p0, p1, p2, p3}, Lcom/flipboard/data/Request$1;->notify(Lcom/flipboard/data/AuthorizationClient;Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;Lcom/flipboard/data/AuthorizationClient$Response;)V

    return-void
.end method
