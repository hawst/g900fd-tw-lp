.class public Lcom/flipboard/data/FLJSONRequest;
.super Lcom/android/volley/Request;
.source "FLJSONRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/volley/Request",
        "<",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# static fields
.field private static final MAX_RETRIES:I = 0x0

.field private static final MAX_TOKEN_REFRESH_RETRIES:I = 0x2

.field private static final SOCKET_TIMEOUT:I = 0x4e20


# instance fields
.field private final errorListener:Lcom/android/volley/Response$ErrorListener;

.field private final mListener:Lcom/android/volley/Response$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field paramsInBody:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private tokenRefreshRetries:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lorg/json/JSONObject;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 41
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lorg/json/JSONObject;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 45
    invoke-direct {p0, p1, p2, p5}, Lcom/android/volley/Request;-><init>(ILjava/lang/String;Lcom/android/volley/Response$ErrorListener;)V

    .line 36
    iput v3, p0, Lcom/flipboard/data/FLJSONRequest;->tokenRefreshRetries:I

    .line 46
    iput-object p4, p0, Lcom/flipboard/data/FLJSONRequest;->mListener:Lcom/android/volley/Response$Listener;

    .line 47
    iput-object p5, p0, Lcom/flipboard/data/FLJSONRequest;->errorListener:Lcom/android/volley/Response$ErrorListener;

    .line 48
    iput-object p3, p0, Lcom/flipboard/data/FLJSONRequest;->paramsInBody:Ljava/util/Map;

    .line 49
    new-instance v0, Lcom/android/volley/DefaultRetryPolicy;

    const/16 v1, 0x4e20

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v3, v2}, Lcom/android/volley/DefaultRetryPolicy;-><init>(IIF)V

    invoke-virtual {p0, v0}, Lcom/flipboard/data/FLJSONRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)V

    .line 50
    return-void
.end method


# virtual methods
.method public deliverError(Lcom/android/volley/VolleyError;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 79
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    iget v0, v0, Lcom/android/volley/NetworkResponse;->statusCode:I

    const/16 v1, 0x193

    if-ne v0, v1, :cond_2

    .line 80
    invoke-virtual {p0}, Lcom/flipboard/data/FLJSONRequest;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/flipboard/data/FLJSONRequest;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "oauth/token"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-super {p0, p1}, Lcom/android/volley/Request;->deliverError(Lcom/android/volley/VolleyError;)V

    .line 94
    :goto_0
    return-void

    .line 83
    :cond_0
    iget v0, p0, Lcom/flipboard/data/FLJSONRequest;->tokenRefreshRetries:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 84
    invoke-static {}, Lcom/flipboard/data/Request;->getInstance()Lcom/flipboard/data/Request;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/flipboard/data/Request;->refreshToken(Lcom/flipboard/data/FLJSONRequest;)V

    .line 85
    iget v0, p0, Lcom/flipboard/data/FLJSONRequest;->tokenRefreshRetries:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/flipboard/data/FLJSONRequest;->tokenRefreshRetries:I

    goto :goto_0

    .line 87
    :cond_1
    iput v2, p0, Lcom/flipboard/data/FLJSONRequest;->tokenRefreshRetries:I

    .line 88
    invoke-super {p0, p1}, Lcom/android/volley/Request;->deliverError(Lcom/android/volley/VolleyError;)V

    goto :goto_0

    .line 91
    :cond_2
    iput v2, p0, Lcom/flipboard/data/FLJSONRequest;->tokenRefreshRetries:I

    .line 92
    invoke-super {p0, p1}, Lcom/android/volley/Request;->deliverError(Lcom/android/volley/VolleyError;)V

    goto :goto_0
.end method

.method protected bridge synthetic deliverResponse(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lorg/json/JSONObject;

    invoke-virtual {p0, p1}, Lcom/flipboard/data/FLJSONRequest;->deliverResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method protected deliverResponse(Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/flipboard/data/FLJSONRequest;->mListener:Lcom/android/volley/Response$Listener;

    invoke-interface {v0, p1}, Lcom/android/volley/Response$Listener;->onResponse(Ljava/lang/Object;)V

    .line 99
    return-void
.end method

.method public getErrorListener()Lcom/android/volley/Response$ErrorListener;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/flipboard/data/FLJSONRequest;->errorListener:Lcom/android/volley/Response$ErrorListener;

    return-object v0
.end method

.method public getParams()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/flipboard/data/FLJSONRequest;->paramsInBody:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/flipboard/data/FLJSONRequest;->paramsInBody:Ljava/util/Map;

    .line 106
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/android/volley/Request;->getParams()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public getPerformedTokenRefreshRetries()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/flipboard/data/FLJSONRequest;->tokenRefreshRetries:I

    return v0
.end method

.method public getSuccessListener()Lcom/android/volley/Response$Listener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lcom/flipboard/data/FLJSONRequest;->mListener:Lcom/android/volley/Response$Listener;

    return-object v0
.end method

.method protected parseNetworkError(Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;
    .locals 1

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/android/volley/Request;->parseNetworkError(Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;

    move-result-object v0

    return-object v0
.end method

.method public parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/flipboard/data/FLJSONRequest;->tokenRefreshRetries:I

    .line 56
    iget-object v0, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    invoke-static {v0}, Lcom/android/volley/toolbox/HttpHeaderParser;->parseCharset(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 57
    iget v1, p1, Lcom/android/volley/NetworkResponse;->statusCode:I

    const/16 v2, 0xca

    if-ne v1, v2, :cond_0

    .line 61
    new-instance v0, Lcom/android/volley/VolleyError;

    invoke-direct {v0, p1}, Lcom/android/volley/VolleyError;-><init>(Lcom/android/volley/NetworkResponse;)V

    invoke-static {v0}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v0

    .line 68
    :goto_0
    return-object v0

    .line 63
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    new-instance v2, Ljava/lang/String;

    iget-object v3, p1, Lcom/android/volley/NetworkResponse;->data:[B

    const-string v4, "ISO-8859-1"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v0, "UTF-8"

    :cond_1
    invoke-direct {v2, v3, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/android/volley/toolbox/HttpHeaderParser;->parseCacheHeaders(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Cache$Entry;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/volley/Response;->success(Ljava/lang/Object;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    new-instance v1, Lcom/android/volley/ParseError;

    invoke-direct {v1, v0}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v0

    goto :goto_0

    .line 67
    :catch_1
    move-exception v0

    .line 68
    new-instance v1, Lcom/android/volley/ParseError;

    invoke-direct {v1, v0}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v0

    goto :goto_0
.end method

.method public setParamsInBody(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 111
    iput-object p1, p0, Lcom/flipboard/data/FLJSONRequest;->paramsInBody:Ljava/util/Map;

    .line 112
    return-void
.end method

.method public setPerformedTokenRefreshRetries(I)V
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lcom/flipboard/data/FLJSONRequest;->tokenRefreshRetries:I

    .line 116
    return-void
.end method
