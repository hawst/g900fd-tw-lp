.class Lcom/flipboard/data/Request$8;
.super Ljava/lang/Object;
.source "Request.java"

# interfaces
.implements Lcom/flipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flipboard/data/Request;->authorize(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/flipboard/util/Observer",
        "<",
        "Lcom/flipboard/data/AuthorizationClient;",
        "Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;",
        "Lcom/flipboard/data/AuthorizationClient$Response;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipboard/data/Request;

.field final synthetic val$errorListener:Lcom/flipboard/data/Request$FLErrorListener;

.field final synthetic val$state:Ljava/lang/String;

.field final synthetic val$successListener:Lcom/flipboard/data/Request$FLSuccessListener;


# direct methods
.method constructor <init>(Lcom/flipboard/data/Request;Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    .locals 0

    .prologue
    .line 868
    iput-object p1, p0, Lcom/flipboard/data/Request$8;->this$0:Lcom/flipboard/data/Request;

    iput-object p2, p0, Lcom/flipboard/data/Request$8;->val$state:Ljava/lang/String;

    iput-object p3, p0, Lcom/flipboard/data/Request$8;->val$successListener:Lcom/flipboard/data/Request$FLSuccessListener;

    iput-object p4, p0, Lcom/flipboard/data/Request$8;->val$errorListener:Lcom/flipboard/data/Request$FLErrorListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public notify(Lcom/flipboard/data/AuthorizationClient;Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;Lcom/flipboard/data/AuthorizationClient$Response;)V
    .locals 3

    .prologue
    .line 872
    sget-object v0, Lcom/flipboard/data/Request$11;->$SwitchMap$com$flipboard$data$AuthorizationClient$AuthorizeMessage:[I

    invoke-virtual {p2}, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 895
    :goto_0
    invoke-virtual {p1, p0}, Lcom/flipboard/data/AuthorizationClient;->removeObserver(Lcom/flipboard/util/Observer;)V

    .line 896
    return-void

    .line 875
    :pswitch_0
    iget-object v0, p0, Lcom/flipboard/data/Request$8;->val$state:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flipboard/data/Request$8;->val$state:Ljava/lang/String;

    iget-object v1, p3, Lcom/flipboard/data/AuthorizationClient$Response;->state:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 877
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 878
    const-string v1, "code"

    iget-object v2, p3, Lcom/flipboard/data/AuthorizationClient$Response;->code:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 879
    iget-object v1, p0, Lcom/flipboard/data/Request$8;->val$successListener:Lcom/flipboard/data/Request$FLSuccessListener;

    invoke-virtual {v1, v0}, Lcom/flipboard/data/Request$FLSuccessListener;->onResponse(Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 881
    :catch_0
    move-exception v0

    .line 883
    const-string v0, "invalid response"

    iput-object v0, p3, Lcom/flipboard/data/AuthorizationClient$Response;->message:Ljava/lang/String;

    .line 890
    :goto_1
    :pswitch_1
    iget-object v0, p0, Lcom/flipboard/data/Request$8;->val$errorListener:Lcom/flipboard/data/Request$FLErrorListener;

    new-instance v1, Lcom/android/volley/VolleyError;

    iget-object v2, p3, Lcom/flipboard/data/AuthorizationClient$Response;->message:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/flipboard/data/Request$FLErrorListener;->onErrorResponse(Lcom/android/volley/VolleyError;)V

    goto :goto_0

    .line 886
    :cond_0
    const-string v0, "invalid state, state does not match"

    iput-object v0, p3, Lcom/flipboard/data/AuthorizationClient$Response;->message:Ljava/lang/String;

    goto :goto_1

    .line 872
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic notify(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 868
    check-cast p1, Lcom/flipboard/data/AuthorizationClient;

    check-cast p2, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;

    check-cast p3, Lcom/flipboard/data/AuthorizationClient$Response;

    invoke-virtual {p0, p1, p2, p3}, Lcom/flipboard/data/Request$8;->notify(Lcom/flipboard/data/AuthorizationClient;Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;Lcom/flipboard/data/AuthorizationClient$Response;)V

    return-void
.end method
