.class Lcom/flipboard/data/Request$6;
.super Lcom/flipboard/data/Request$FLSuccessListener;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flipboard/data/Request;->getToken(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipboard/data/Request;


# direct methods
.method constructor <init>(Lcom/flipboard/data/Request;)V
    .locals 0

    .prologue
    .line 818
    iput-object p1, p0, Lcom/flipboard/data/Request$6;->this$0:Lcom/flipboard/data/Request;

    invoke-direct {p0}, Lcom/flipboard/data/Request$FLSuccessListener;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 818
    check-cast p1, Lorg/json/JSONObject;

    invoke-virtual {p0, p1}, Lcom/flipboard/data/Request$6;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 821
    # setter for: Lcom/flipboard/data/Request;->requestingToken:Z
    invoke-static {v9}, Lcom/flipboard/data/Request;->access$402(Z)Z

    .line 823
    :try_start_0
    iget-object v0, p0, Lcom/flipboard/data/Request$6;->this$0:Lcom/flipboard/data/Request;

    const-string v1, "access_token"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "refresh_token"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    # invokes: Lcom/flipboard/data/Request;->saveTokens(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v3}, Lcom/flipboard/data/Request;->access$600(Lcom/flipboard/data/Request;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    iget-object v0, p0, Lcom/flipboard/data/Request$6;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->pendingFLJSONRequests:Ljava/util/List;
    invoke-static {v0}, Lcom/flipboard/data/Request;->access$700(Lcom/flipboard/data/Request;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipboard/data/FLJSONRequest;

    .line 827
    new-instance v2, Lcom/flipboard/data/FLJSONRequest;

    invoke-virtual {v0}, Lcom/flipboard/data/FLJSONRequest;->getMethod()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/flipboard/data/FLJSONRequest;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&access_token=%s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/flipboard/data/Request$6;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;
    invoke-static {v8}, Lcom/flipboard/data/Request;->access$200(Lcom/flipboard/data/Request;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/flipboard/data/FLJSONRequest;->getSuccessListener()Lcom/android/volley/Response$Listener;

    move-result-object v5

    invoke-virtual {v0}, Lcom/flipboard/data/FLJSONRequest;->getErrorListener()Lcom/android/volley/Response$ErrorListener;

    move-result-object v0

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 833
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 834
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v3, "Re-send pending request when token ready: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v2}, Lcom/flipboard/data/FLJSONRequest;->getUrl()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 838
    :catch_0
    move-exception v0

    .line 839
    iget-object v1, p0, Lcom/flipboard/data/Request$6;->this$0:Lcom/flipboard/data/Request;

    sget-object v2, Lcom/flipboard/data/Request$Message;->ERROR:Lcom/flipboard/data/Request$Message;

    const-string v3, "Invalid response from server: %s"

    new-array v4, v10, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v9

    invoke-static {v3, v4}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/flipboard/data/Request;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 841
    :goto_1
    return-void

    .line 836
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/flipboard/data/Request$6;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->pendingFLJSONRequests:Ljava/util/List;
    invoke-static {v0}, Lcom/flipboard/data/Request;->access$700(Lcom/flipboard/data/Request;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 837
    iget-object v0, p0, Lcom/flipboard/data/Request$6;->this$0:Lcom/flipboard/data/Request;

    sget-object v1, Lcom/flipboard/data/Request$Message;->READY:Lcom/flipboard/data/Request$Message;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/data/Request;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
