.class Lcom/flipboard/data/Request$9;
.super Lcom/flipboard/data/Request$FLSuccessListener;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flipboard/data/Request;->refreshToken(Lcom/flipboard/data/FLJSONRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipboard/data/Request;

.field final synthetic val$request:Lcom/flipboard/data/FLJSONRequest;


# direct methods
.method constructor <init>(Lcom/flipboard/data/Request;Lcom/flipboard/data/FLJSONRequest;)V
    .locals 0

    .prologue
    .line 947
    iput-object p1, p0, Lcom/flipboard/data/Request$9;->this$0:Lcom/flipboard/data/Request;

    iput-object p2, p0, Lcom/flipboard/data/Request$9;->val$request:Lcom/flipboard/data/FLJSONRequest;

    invoke-direct {p0}, Lcom/flipboard/data/Request$FLSuccessListener;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 947
    check-cast p1, Lorg/json/JSONObject;

    invoke-virtual {p0, p1}, Lcom/flipboard/data/Request$9;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 6

    .prologue
    .line 951
    :try_start_0
    const-string v0, "access_token"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 952
    iget-object v0, p0, Lcom/flipboard/data/Request$9;->this$0:Lcom/flipboard/data/Request;

    const-string v1, "refresh_token"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    # invokes: Lcom/flipboard/data/Request;->saveTokens(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v2, v1, v3}, Lcom/flipboard/data/Request;->access$600(Lcom/flipboard/data/Request;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    iget-object v0, p0, Lcom/flipboard/data/Request$9;->val$request:Lcom/flipboard/data/FLJSONRequest;

    invoke-virtual {v0}, Lcom/flipboard/data/FLJSONRequest;->getUrl()Ljava/lang/String;

    move-result-object v3

    .line 960
    new-instance v0, Lcom/flipboard/data/FLJSONRequest;

    iget-object v1, p0, Lcom/flipboard/data/Request$9;->val$request:Lcom/flipboard/data/FLJSONRequest;

    invoke-virtual {v1}, Lcom/flipboard/data/FLJSONRequest;->getMethod()I

    move-result v1

    iget-object v4, p0, Lcom/flipboard/data/Request$9;->this$0:Lcom/flipboard/data/Request;

    # invokes: Lcom/flipboard/data/Request;->replaceAccessToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v4, v3, v2}, Lcom/flipboard/data/Request;->access$800(Lcom/flipboard/data/Request;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/flipboard/data/Request$9;->val$request:Lcom/flipboard/data/FLJSONRequest;

    invoke-virtual {v3}, Lcom/flipboard/data/FLJSONRequest;->getParams()Ljava/util/Map;

    move-result-object v3

    iget-object v4, p0, Lcom/flipboard/data/Request$9;->val$request:Lcom/flipboard/data/FLJSONRequest;

    invoke-virtual {v4}, Lcom/flipboard/data/FLJSONRequest;->getSuccessListener()Lcom/android/volley/Response$Listener;

    move-result-object v4

    iget-object v5, p0, Lcom/flipboard/data/Request$9;->val$request:Lcom/flipboard/data/FLJSONRequest;

    invoke-virtual {v5}, Lcom/flipboard/data/FLJSONRequest;->getErrorListener()Lcom/android/volley/Response$ErrorListener;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/flipboard/data/FLJSONRequest;-><init>(ILjava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 962
    iget-object v1, p0, Lcom/flipboard/data/Request$9;->val$request:Lcom/flipboard/data/FLJSONRequest;

    invoke-virtual {v1}, Lcom/flipboard/data/FLJSONRequest;->getPerformedTokenRefreshRetries()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/flipboard/data/FLJSONRequest;->setPerformedTokenRefreshRetries(I)V

    .line 963
    invoke-static {}, Lcom/flipboard/data/FDLVolley;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/AuthFailureError; {:try_start_0 .. :try_end_0} :catch_1

    .line 969
    :goto_0
    return-void

    .line 964
    :catch_0
    move-exception v0

    .line 965
    iget-object v1, p0, Lcom/flipboard/data/Request$9;->val$request:Lcom/flipboard/data/FLJSONRequest;

    new-instance v2, Lcom/android/volley/VolleyError;

    invoke-direct {v2, v0}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v1, v2}, Lcom/flipboard/data/FLJSONRequest;->deliverError(Lcom/android/volley/VolleyError;)V

    goto :goto_0

    .line 966
    :catch_1
    move-exception v0

    .line 967
    iget-object v1, p0, Lcom/flipboard/data/Request$9;->val$request:Lcom/flipboard/data/FLJSONRequest;

    invoke-virtual {v1, v0}, Lcom/flipboard/data/FLJSONRequest;->deliverError(Lcom/android/volley/VolleyError;)V

    goto :goto_0
.end method
