.class Lcom/flipboard/data/Request$3;
.super Lcom/flipboard/data/Request$FLSuccessListener;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flipboard/data/Request;->requestToken()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipboard/data/Request;


# direct methods
.method constructor <init>(Lcom/flipboard/data/Request;)V
    .locals 0

    .prologue
    .line 770
    iput-object p1, p0, Lcom/flipboard/data/Request$3;->this$0:Lcom/flipboard/data/Request;

    invoke-direct {p0}, Lcom/flipboard/data/Request$FLSuccessListener;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 770
    check-cast p1, Lorg/json/JSONObject;

    invoke-virtual {p0, p1}, Lcom/flipboard/data/Request$3;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 774
    :try_start_0
    const-string v0, "code"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 775
    iget-object v0, p0, Lcom/flipboard/data/Request$3;->this$0:Lcom/flipboard/data/Request;

    const-string v1, "code"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/flipboard/data/Request;->getToken(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/flipboard/data/Request;->access$300(Lcom/flipboard/data/Request;Ljava/lang/String;)V

    .line 788
    :goto_0
    return-void

    .line 776
    :cond_0
    const-string v0, "error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 777
    const/4 v0, 0x0

    # setter for: Lcom/flipboard/data/Request;->requestingToken:Z
    invoke-static {v0}, Lcom/flipboard/data/Request;->access$402(Z)Z

    .line 778
    iget-object v0, p0, Lcom/flipboard/data/Request$3;->this$0:Lcom/flipboard/data/Request;

    sget-object v1, Lcom/flipboard/data/Request$Message;->ERROR:Lcom/flipboard/data/Request$Message;

    const-string v2, "Error: \'%s\' while authorizing clientId \'%s\'"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "error"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/flipboard/data/Request$3;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->clientId:Ljava/lang/String;
    invoke-static {v5}, Lcom/flipboard/data/Request;->access$500(Lcom/flipboard/data/Request;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/data/Request;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 783
    :catch_0
    move-exception v0

    .line 784
    # setter for: Lcom/flipboard/data/Request;->requestingToken:Z
    invoke-static {v6}, Lcom/flipboard/data/Request;->access$402(Z)Z

    .line 785
    iget-object v1, p0, Lcom/flipboard/data/Request$3;->this$0:Lcom/flipboard/data/Request;

    sget-object v2, Lcom/flipboard/data/Request$Message;->ERROR:Lcom/flipboard/data/Request$Message;

    const-string v3, "Invalid response from server while requesting authorization on clientId \'%s\': %s"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/flipboard/data/Request$3;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->clientId:Ljava/lang/String;
    invoke-static {v5}, Lcom/flipboard/data/Request;->access$500(Lcom/flipboard/data/Request;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/flipboard/data/Request;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 780
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    # setter for: Lcom/flipboard/data/Request;->requestingToken:Z
    invoke-static {v0}, Lcom/flipboard/data/Request;->access$402(Z)Z

    .line 781
    iget-object v0, p0, Lcom/flipboard/data/Request$3;->this$0:Lcom/flipboard/data/Request;

    sget-object v1, Lcom/flipboard/data/Request$Message;->ERROR:Lcom/flipboard/data/Request$Message;

    const-string v2, "Error: \'unknown\' while authorizing clientId \'%s\'"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/flipboard/data/Request$3;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->clientId:Ljava/lang/String;
    invoke-static {v5}, Lcom/flipboard/data/Request;->access$500(Lcom/flipboard/data/Request;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/data/Request;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
