.class public Lcom/flipboard/data/FDLVolley;
.super Ljava/lang/Object;
.source "FDLVolley.java"


# static fields
.field private static mRequestQueue:Lcom/android/volley/RequestQueue;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public static getRequestQueue()Lcom/android/volley/RequestQueue;
    .locals 2

    .prologue
    .line 30
    sget-object v0, Lcom/flipboard/data/FDLVolley;->mRequestQueue:Lcom/android/volley/RequestQueue;

    if-eqz v0, :cond_0

    .line 31
    sget-object v0, Lcom/flipboard/data/FDLVolley;->mRequestQueue:Lcom/android/volley/RequestQueue;

    return-object v0

    .line 33
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RequestQueue not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/flipboard/data/FDLVolley;->mRequestQueue:Lcom/android/volley/RequestQueue;

    if-nez v0, :cond_0

    .line 24
    invoke-static {p0}, Lcom/android/volley/toolbox/Volley;->newRequestQueue(Landroid/content/Context;)Lcom/android/volley/RequestQueue;

    move-result-object v0

    sput-object v0, Lcom/flipboard/data/FDLVolley;->mRequestQueue:Lcom/android/volley/RequestQueue;

    .line 26
    :cond_0
    return-void
.end method
