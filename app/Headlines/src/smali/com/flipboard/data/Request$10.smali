.class Lcom/flipboard/data/Request$10;
.super Lcom/flipboard/data/Request$FLErrorListener;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flipboard/data/Request;->refreshToken(Lcom/flipboard/data/FLJSONRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipboard/data/Request;

.field final synthetic val$request:Lcom/flipboard/data/FLJSONRequest;


# direct methods
.method constructor <init>(Lcom/flipboard/data/Request;Lcom/flipboard/data/FLJSONRequest;)V
    .locals 0

    .prologue
    .line 973
    iput-object p1, p0, Lcom/flipboard/data/Request$10;->this$0:Lcom/flipboard/data/Request;

    iput-object p2, p0, Lcom/flipboard/data/Request$10;->val$request:Lcom/flipboard/data/FLJSONRequest;

    invoke-direct {p0}, Lcom/flipboard/data/Request$FLErrorListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 976
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    iget v0, v0, Lcom/android/volley/NetworkResponse;->statusCode:I

    const/16 v1, 0x193

    if-ne v0, v1, :cond_0

    .line 978
    iget-object v0, p0, Lcom/flipboard/data/Request$10;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->sharedPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/flipboard/data/Request;->access$900(Lcom/flipboard/data/Request;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.flipboard.fdl.ACCESS_TOKEN"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.flipboard.fdl.PREF_REFRESH_TOKEN"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 979
    iget-object v0, p0, Lcom/flipboard/data/Request$10;->this$0:Lcom/flipboard/data/Request;

    # setter for: Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/flipboard/data/Request;->access$202(Lcom/flipboard/data/Request;Ljava/lang/String;)Ljava/lang/String;

    .line 980
    iget-object v0, p0, Lcom/flipboard/data/Request$10;->this$0:Lcom/flipboard/data/Request;

    # setter for: Lcom/flipboard/data/Request;->refreshToken:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/flipboard/data/Request;->access$1002(Lcom/flipboard/data/Request;Ljava/lang/String;)Ljava/lang/String;

    .line 991
    :cond_0
    iget-object v0, p0, Lcom/flipboard/data/Request$10;->val$request:Lcom/flipboard/data/FLJSONRequest;

    invoke-virtual {v0, p1}, Lcom/flipboard/data/FLJSONRequest;->deliverError(Lcom/android/volley/VolleyError;)V

    .line 992
    return-void
.end method
