.class Lcom/flipboard/data/Request$2;
.super Lcom/flipboard/data/Request$FLSuccessListener;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flipboard/data/Request;->serviceLogout(Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipboard/data/Request;

.field final synthetic val$serviceId:Ljava/lang/String;

.field final synthetic val$successListener:Lcom/flipboard/data/Request$FLSuccessListener;


# direct methods
.method constructor <init>(Lcom/flipboard/data/Request;Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;)V
    .locals 0

    .prologue
    .line 745
    iput-object p1, p0, Lcom/flipboard/data/Request$2;->this$0:Lcom/flipboard/data/Request;

    iput-object p2, p0, Lcom/flipboard/data/Request$2;->val$serviceId:Ljava/lang/String;

    iput-object p3, p0, Lcom/flipboard/data/Request$2;->val$successListener:Lcom/flipboard/data/Request$FLSuccessListener;

    invoke-direct {p0}, Lcom/flipboard/data/Request$FLSuccessListener;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 745
    check-cast p1, Lorg/json/JSONObject;

    invoke-virtual {p0, p1}, Lcom/flipboard/data/Request$2;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    .line 748
    iget-object v0, p0, Lcom/flipboard/data/Request$2;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->syncedWithFlipboard:Z
    invoke-static {v0}, Lcom/flipboard/data/Request;->access$000(Lcom/flipboard/data/Request;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 749
    iget-object v0, p0, Lcom/flipboard/data/Request$2;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/flipboard/data/Request;->access$100(Lcom/flipboard/data/Request;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/flipboard/data/Request$2;->this$0:Lcom/flipboard/data/Request;

    # getter for: Lcom/flipboard/data/Request;->accessToken:Ljava/lang/String;
    invoke-static {v1}, Lcom/flipboard/data/Request;->access$200(Lcom/flipboard/data/Request;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/flipboard/data/Request$2;->val$serviceId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/flipboard/util/FDLUtil;->fireTokenBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    :cond_0
    iget-object v0, p0, Lcom/flipboard/data/Request$2;->val$successListener:Lcom/flipboard/data/Request$FLSuccessListener;

    invoke-virtual {v0, p1}, Lcom/flipboard/data/Request$FLSuccessListener;->onResponse(Ljava/lang/Object;)V

    .line 752
    return-void
.end method
