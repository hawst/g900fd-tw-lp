.class public Lcom/flipboard/data/AuthorizationClient$Response;
.super Ljava/lang/Object;
.source "AuthorizationClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/data/AuthorizationClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Response"
.end annotation


# instance fields
.field public code:Ljava/lang/String;

.field public message:Ljava/lang/String;

.field public state:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lcom/flipboard/data/AuthorizationClient$Response;->message:Ljava/lang/String;

    .line 125
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-object p1, p0, Lcom/flipboard/data/AuthorizationClient$Response;->code:Ljava/lang/String;

    .line 130
    iput-object p2, p0, Lcom/flipboard/data/AuthorizationClient$Response;->state:Ljava/lang/String;

    .line 131
    iput-object p3, p0, Lcom/flipboard/data/AuthorizationClient$Response;->message:Ljava/lang/String;

    .line 132
    return-void
.end method
