.class Lcom/flipboard/data/Request$5;
.super Ljava/lang/Object;
.source "Request.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flipboard/data/Request;->requestToken()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipboard/data/Request;


# direct methods
.method constructor <init>(Lcom/flipboard/data/Request;)V
    .locals 0

    .prologue
    .line 799
    iput-object p1, p0, Lcom/flipboard/data/Request$5;->this$0:Lcom/flipboard/data/Request;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 803
    # getter for: Lcom/flipboard/data/Request;->requestingToken:Z
    invoke-static {}, Lcom/flipboard/data/Request;->access$400()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 804
    iget-object v0, p0, Lcom/flipboard/data/Request$5;->this$0:Lcom/flipboard/data/Request;

    sget-object v1, Lcom/flipboard/data/Request$Message;->ERROR:Lcom/flipboard/data/Request$Message;

    const-string v2, "Request token authorize timeout, please try again"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/data/Request;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 805
    # setter for: Lcom/flipboard/data/Request;->requestingToken:Z
    invoke-static {v4}, Lcom/flipboard/data/Request;->access$402(Z)Z

    .line 807
    :cond_0
    return-void
.end method
