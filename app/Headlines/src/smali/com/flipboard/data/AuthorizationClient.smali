.class public Lcom/flipboard/data/AuthorizationClient;
.super Landroid/webkit/WebViewClient;
.source "AuthorizationClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipboard/data/AuthorizationClient$Response;,
        Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;
    }
.end annotation


# static fields
.field private static instance:Lcom/flipboard/data/AuthorizationClient;


# instance fields
.field private observers:Lcom/flipboard/util/Observable$Proxy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipboard/util/Observable$Proxy",
            "<",
            "Lcom/flipboard/data/AuthorizationClient;",
            "Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;",
            "Lcom/flipboard/data/AuthorizationClient$Response;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 43
    new-instance v0, Lcom/flipboard/util/Observable$Proxy;

    invoke-direct {v0, p0}, Lcom/flipboard/util/Observable$Proxy;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/flipboard/data/AuthorizationClient;->observers:Lcom/flipboard/util/Observable$Proxy;

    .line 28
    return-void
.end method

.method public static getInstance()Lcom/flipboard/data/AuthorizationClient;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/flipboard/data/AuthorizationClient;->instance:Lcom/flipboard/data/AuthorizationClient;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/flipboard/data/AuthorizationClient;

    invoke-direct {v0}, Lcom/flipboard/data/AuthorizationClient;-><init>()V

    sput-object v0, Lcom/flipboard/data/AuthorizationClient;->instance:Lcom/flipboard/data/AuthorizationClient;

    .line 36
    :cond_0
    sget-object v0, Lcom/flipboard/data/AuthorizationClient;->instance:Lcom/flipboard/data/AuthorizationClient;

    return-object v0
.end method


# virtual methods
.method public addObserver(Lcom/flipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipboard/util/Observer",
            "<",
            "Lcom/flipboard/data/AuthorizationClient;",
            "Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;",
            "Lcom/flipboard/data/AuthorizationClient$Response;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/flipboard/data/AuthorizationClient;->observers:Lcom/flipboard/util/Observable$Proxy;

    invoke-virtual {v0, p1}, Lcom/flipboard/util/Observable$Proxy;->addObserver(Lcom/flipboard/util/Observer;)V

    .line 48
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 97
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 98
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "page finished %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 91
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 92
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "page started %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 109
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "Error code: %d - %s - %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    aput-object p3, v2, v7

    aput-object p4, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, Lcom/flipboard/data/AuthorizationClient;->observers:Lcom/flipboard/util/Observable$Proxy;

    sget-object v1, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;->ERROR:Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;

    new-instance v2, Lcom/flipboard/data/AuthorizationClient$Response;

    const-string v3, "web error (%s): %s"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object p3, v4, v7

    invoke-static {v3, v4}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/flipboard/data/AuthorizationClient$Response;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Observable$Proxy;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 111
    return-void
.end method

.method public onScaleChanged(Landroid/webkit/WebView;FF)V
    .locals 5

    .prologue
    .line 103
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onScaleChanged(Landroid/webkit/WebView;FF)V

    .line 104
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "scale changed from %s to %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    return-void
.end method

.method public removeObserver(Lcom/flipboard/util/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipboard/util/Observer",
            "<",
            "Lcom/flipboard/data/AuthorizationClient;",
            "Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;",
            "Lcom/flipboard/data/AuthorizationClient$Response;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/flipboard/data/AuthorizationClient;->observers:Lcom/flipboard/util/Observable$Proxy;

    invoke-virtual {v0, p1}, Lcom/flipboard/util/Observable$Proxy;->removeObserver(Lcom/flipboard/util/Observer;)V

    .line 53
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 57
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 59
    sget-object v3, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v4, "should override url %s"

    new-array v5, v0, [Ljava/lang/Object;

    aput-object p2, v5, v1

    invoke-virtual {v3, v4, v5}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    const-string v3, "fdl"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 64
    const-string v3, "authorize.redirect"

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 65
    const-string v1, "code"

    invoke-virtual {v2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 66
    const-string v3, "state"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 67
    new-instance v3, Lcom/flipboard/data/AuthorizationClient$Response;

    invoke-direct {v3, v1, v2, v6}, Lcom/flipboard/data/AuthorizationClient$Response;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v1, p0, Lcom/flipboard/data/AuthorizationClient;->observers:Lcom/flipboard/util/Observable$Proxy;

    sget-object v2, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;->SUCCESS:Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/util/Observable$Proxy;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 86
    :goto_0
    return v0

    .line 73
    :cond_0
    const-string v3, "service.login.redirect"

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 74
    const-string v1, "success"

    invoke-virtual {v2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75
    if-eqz v1, :cond_1

    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    iget-object v1, p0, Lcom/flipboard/data/AuthorizationClient;->observers:Lcom/flipboard/util/Observable$Proxy;

    sget-object v2, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;->SUCCESS:Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;

    invoke-virtual {v1, v2, v6}, Lcom/flipboard/util/Observable$Proxy;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 78
    :cond_1
    iget-object v1, p0, Lcom/flipboard/data/AuthorizationClient;->observers:Lcom/flipboard/util/Observable$Proxy;

    sget-object v2, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;->ERROR:Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;

    new-instance v3, Lcom/flipboard/data/AuthorizationClient$Response;

    const-string v4, "service login failed, please try again"

    invoke-direct {v3, v4}, Lcom/flipboard/data/AuthorizationClient$Response;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/util/Observable$Proxy;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 86
    goto :goto_0
.end method
