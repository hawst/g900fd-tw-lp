.class Lcom/flipboard/data/Request$7;
.super Lcom/flipboard/data/Request$FLErrorListener;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/flipboard/data/Request;->getToken(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipboard/data/Request;


# direct methods
.method constructor <init>(Lcom/flipboard/data/Request;)V
    .locals 0

    .prologue
    .line 842
    iput-object p1, p0, Lcom/flipboard/data/Request$7;->this$0:Lcom/flipboard/data/Request;

    invoke-direct {p0}, Lcom/flipboard/data/Request$FLErrorListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 845
    # setter for: Lcom/flipboard/data/Request;->requestingToken:Z
    invoke-static {v5}, Lcom/flipboard/data/Request;->access$402(Z)Z

    .line 846
    iget-object v0, p0, Lcom/flipboard/data/Request$7;->this$0:Lcom/flipboard/data/Request;

    sget-object v1, Lcom/flipboard/data/Request$Message;->ERROR:Lcom/flipboard/data/Request$Message;

    const-string v2, "Could not retreive token from server: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/data/Request;->notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 847
    return-void
.end method
