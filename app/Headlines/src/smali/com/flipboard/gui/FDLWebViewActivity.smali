.class public Lcom/flipboard/gui/FDLWebViewActivity;
.super Landroid/app/Activity;
.source "FDLWebViewActivity.java"

# interfaces
.implements Lcom/flipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipboard/gui/FDLWebViewActivity$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Activity;",
        "Lcom/flipboard/util/Observer",
        "<",
        "Lcom/flipboard/data/AuthorizationClient;",
        "Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;",
        "Lcom/flipboard/data/AuthorizationClient$Response;",
        ">;"
    }
.end annotation


# instance fields
.field private url:Ljava/lang/String;

.field private webview:Lcom/flipboard/gui/FDLWebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 49
    return-void
.end method


# virtual methods
.method public notify(Lcom/flipboard/data/AuthorizationClient;Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;Lcom/flipboard/data/AuthorizationClient$Response;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    sget-object v0, Lcom/flipboard/gui/FDLWebViewActivity$1;->$SwitchMap$com$flipboard$data$AuthorizationClient$AuthorizeMessage:[I

    invoke-virtual {p2}, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 49
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "unsupported message"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    :goto_0
    invoke-virtual {p1, p0}, Lcom/flipboard/data/AuthorizationClient;->removeObserver(Lcom/flipboard/util/Observer;)V

    .line 54
    invoke-virtual {p0}, Lcom/flipboard/gui/FDLWebViewActivity;->finish()V

    .line 55
    return-void

    .line 46
    :pswitch_0
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "success message"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic notify(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/flipboard/data/AuthorizationClient;

    check-cast p2, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;

    check-cast p3, Lcom/flipboard/data/AuthorizationClient$Response;

    invoke-virtual {p0, p1, p2, p3}, Lcom/flipboard/gui/FDLWebViewActivity;->notify(Lcom/flipboard/data/AuthorizationClient;Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;Lcom/flipboard/data/AuthorizationClient$Response;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/flipboard/gui/FDLWebViewActivity;->requestWindowFeature(I)Z

    .line 28
    invoke-virtual {p0}, Lcom/flipboard/gui/FDLWebViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 31
    invoke-virtual {p0}, Lcom/flipboard/gui/FDLWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 32
    const-string v1, "com.flipboard.fdl.URL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flipboard/gui/FDLWebViewActivity;->url:Ljava/lang/String;

    .line 34
    new-instance v0, Lcom/flipboard/gui/FDLWebView;

    invoke-direct {v0, p0}, Lcom/flipboard/gui/FDLWebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/flipboard/gui/FDLWebViewActivity;->webview:Lcom/flipboard/gui/FDLWebView;

    .line 35
    invoke-static {}, Lcom/flipboard/data/AuthorizationClient;->getInstance()Lcom/flipboard/data/AuthorizationClient;

    move-result-object v0

    .line 36
    invoke-virtual {v0, p0}, Lcom/flipboard/data/AuthorizationClient;->addObserver(Lcom/flipboard/util/Observer;)V

    .line 37
    iget-object v1, p0, Lcom/flipboard/gui/FDLWebViewActivity;->webview:Lcom/flipboard/gui/FDLWebView;

    invoke-virtual {v1, v0}, Lcom/flipboard/gui/FDLWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 38
    iget-object v0, p0, Lcom/flipboard/gui/FDLWebViewActivity;->webview:Lcom/flipboard/gui/FDLWebView;

    invoke-virtual {p0, v0}, Lcom/flipboard/gui/FDLWebViewActivity;->setContentView(Landroid/view/View;)V

    .line 39
    iget-object v0, p0, Lcom/flipboard/gui/FDLWebViewActivity;->webview:Lcom/flipboard/gui/FDLWebView;

    iget-object v1, p0, Lcom/flipboard/gui/FDLWebViewActivity;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/flipboard/gui/FDLWebView;->loadUrl(Ljava/lang/String;)V

    .line 40
    return-void
.end method
