.class public Lcom/flipboard/gui/FDLWebView;
.super Landroid/webkit/WebView;
.source "FDLWebView.java"


# static fields
.field public static final URL:Ljava/lang/String; = "com.flipboard.fdl.URL"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 18
    invoke-direct {p0}, Lcom/flipboard/gui/FDLWebView;->init()V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-direct {p0}, Lcom/flipboard/gui/FDLWebView;->init()V

    .line 25
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 29
    invoke-virtual {p0}, Lcom/flipboard/gui/FDLWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 30
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 31
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 32
    return-void
.end method
