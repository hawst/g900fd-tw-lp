.class public interface abstract Lcom/flipboard/util/TimeConstants;
.super Ljava/lang/Object;
.source "TimeConstants.java"


# static fields
.field public static final DAY:J = 0x5265c00L

.field public static final HOUR:J = 0x36ee80L

.field public static final MIN:J = 0xea60L

.field public static final MONTH:J = 0x9fa52400L

.field public static final MSEC:J = 0x1L

.field public static final NEVER:J = 0x0L

.field public static final SEC:J = 0x3e8L

.field public static final WEEK:J = 0x240c8400L

.field public static final YEAR:J = 0x757b12c00L
