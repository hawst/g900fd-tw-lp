.class public final enum Lcom/flipboard/util/Log$Level;
.super Ljava/lang/Enum;
.source "Log.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/util/Log;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Level"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/flipboard/util/Log$Level;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/flipboard/util/Log$Level;

.field public static final enum CUSTOM:Lcom/flipboard/util/Log$Level;

.field public static final enum DEBUG:Lcom/flipboard/util/Log$Level;

.field public static final enum ERROR:Lcom/flipboard/util/Log$Level;

.field public static final enum INFO:Lcom/flipboard/util/Log$Level;

.field public static final enum WARN:Lcom/flipboard/util/Log$Level;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    new-instance v0, Lcom/flipboard/util/Log$Level;

    const-string v1, "CUSTOM"

    invoke-direct {v0, v1, v2}, Lcom/flipboard/util/Log$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/util/Log$Level;->CUSTOM:Lcom/flipboard/util/Log$Level;

    new-instance v0, Lcom/flipboard/util/Log$Level;

    const-string v1, "DEBUG"

    invoke-direct {v0, v1, v3}, Lcom/flipboard/util/Log$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/util/Log$Level;->DEBUG:Lcom/flipboard/util/Log$Level;

    new-instance v0, Lcom/flipboard/util/Log$Level;

    const-string v1, "INFO"

    invoke-direct {v0, v1, v4}, Lcom/flipboard/util/Log$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/util/Log$Level;->INFO:Lcom/flipboard/util/Log$Level;

    new-instance v0, Lcom/flipboard/util/Log$Level;

    const-string v1, "WARN"

    invoke-direct {v0, v1, v5}, Lcom/flipboard/util/Log$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/util/Log$Level;->WARN:Lcom/flipboard/util/Log$Level;

    new-instance v0, Lcom/flipboard/util/Log$Level;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v6}, Lcom/flipboard/util/Log$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/flipboard/util/Log$Level;->ERROR:Lcom/flipboard/util/Log$Level;

    .line 61
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/flipboard/util/Log$Level;

    sget-object v1, Lcom/flipboard/util/Log$Level;->CUSTOM:Lcom/flipboard/util/Log$Level;

    aput-object v1, v0, v2

    sget-object v1, Lcom/flipboard/util/Log$Level;->DEBUG:Lcom/flipboard/util/Log$Level;

    aput-object v1, v0, v3

    sget-object v1, Lcom/flipboard/util/Log$Level;->INFO:Lcom/flipboard/util/Log$Level;

    aput-object v1, v0, v4

    sget-object v1, Lcom/flipboard/util/Log$Level;->WARN:Lcom/flipboard/util/Log$Level;

    aput-object v1, v0, v5

    sget-object v1, Lcom/flipboard/util/Log$Level;->ERROR:Lcom/flipboard/util/Log$Level;

    aput-object v1, v0, v6

    sput-object v0, Lcom/flipboard/util/Log$Level;->$VALUES:[Lcom/flipboard/util/Log$Level;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/flipboard/util/Log$Level;
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/flipboard/util/Log$Level;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/flipboard/util/Log$Level;

    return-object v0
.end method

.method public static values()[Lcom/flipboard/util/Log$Level;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/flipboard/util/Log$Level;->$VALUES:[Lcom/flipboard/util/Log$Level;

    invoke-virtual {v0}, [Lcom/flipboard/util/Log$Level;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/flipboard/util/Log$Level;

    return-object v0
.end method
