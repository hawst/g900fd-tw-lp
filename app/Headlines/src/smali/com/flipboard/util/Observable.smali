.class public Lcom/flipboard/util/Observable;
.super Ljava/lang/Object;
.source "Observable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipboard/util/Observable$Proxy;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        "M:",
        "Ljava/lang/Object;",
        "A:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final NULL_OBS:[Ljava/lang/Object;


# instance fields
.field protected observers:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lcom/flipboard/util/Observable;->NULL_OBS:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    sget-object v0, Lcom/flipboard/util/Observable;->NULL_OBS:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    .line 17
    return-void
.end method


# virtual methods
.method public declared-synchronized addObserver(Lcom/flipboard/util/Observer;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipboard/util/Observer",
            "<TE;TM;TA;>;)V"
        }
    .end annotation

    .prologue
    .line 51
    monitor-enter p0

    if-nez p1, :cond_0

    .line 52
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null observer"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 54
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iput-object v2, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    iget-object v0, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aput-object p1, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 57
    monitor-exit p0

    return-void
.end method

.method public final countObservers()I
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    array-length v0, v0

    return v0
.end method

.method public getSource()Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 31
    return-object p0
.end method

.method public final hasObservers()Z
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyObserver(Lcom/flipboard/util/Observer;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipboard/util/Observer",
            "<TE;TM;TA;>;TM;TA;)V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/flipboard/util/Observable;->getSource()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0, p2, p3}, Lcom/flipboard/util/Observer;->notify(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 96
    return-void
.end method

.method public notifyObservers(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;TA;)V"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v2, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    .line 88
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 89
    check-cast v0, Lcom/flipboard/util/Observer;

    invoke-virtual {p0, v0, p1, p2}, Lcom/flipboard/util/Observable;->notifyObserver(Lcom/flipboard/util/Observer;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 88
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 91
    :cond_0
    return-void
.end method

.method public declared-synchronized removeObserver(Lcom/flipboard/util/Observer;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipboard/util/Observer",
            "<TE;TM;TA;>;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 61
    monitor-enter p0

    if-nez p1, :cond_0

    .line 62
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null observer"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 65
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    array-length v0, v0

    :cond_1
    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_4

    .line 66
    iget-object v2, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-ne v2, p1, :cond_1

    .line 67
    iget-object v2, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    array-length v2, v2

    if-ne v2, v7, :cond_2

    .line 68
    sget-object v2, Lcom/flipboard/util/Observable;->NULL_OBS:[Ljava/lang/Object;

    iput-object v2, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    .line 77
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 70
    :cond_2
    iget-object v2, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 71
    iget-object v3, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v2, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 72
    add-int/lit8 v3, v0, 0x1

    iget-object v4, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    array-length v4, v4

    if-ge v3, v4, :cond_3

    .line 73
    iget-object v3, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    add-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    array-length v5, v5

    add-int/lit8 v6, v0, 0x1

    sub-int/2addr v5, v6

    invoke-static {v3, v4, v2, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    :cond_3
    iput-object v2, p0, Lcom/flipboard/util/Observable;->observers:[Ljava/lang/Object;

    goto :goto_1

    .line 80
    :cond_4
    if-nez v1, :cond_5

    .line 81
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "cannot remove observer: %s from %s, %t"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->warning(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83
    :cond_5
    monitor-exit p0

    return-void
.end method
