.class public Lcom/flipboard/util/IntentUtil;
.super Ljava/lang/Object;
.source "IntentUtil.java"


# static fields
.field public static final API_PARTNER_SAMSUNG:Ljava/lang/String; = "samsung"

.field public static final API_VERSION:I = 0x2

.field public static final FLIPBOARD_INTENT_API_PARTNER_KEY:Ljava/lang/String; = "apiPartner"

.field public static final FLIPBOARD_INTENT_API_VERSION_KEY:Ljava/lang/String; = "apiVersion"

.field public static final NATIVE_APP_TYPE_USE_FLIPBOARD:I = 0x0

.field public static final NATIVE_APP_TYPE_USE_NATIVE_ALWAYS:I = 0x2

.field public static final NATIVE_APP_TYPE_USE_NATIVE_FIRST:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static activityExists(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getDetailIntent(Landroid/content/Context;Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/String;)Landroid/content/Intent;
    .locals 11

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 45
    if-nez p1, :cond_0

    move-object v0, v2

    .line 111
    :goto_0
    return-object v0

    .line 53
    :cond_0
    :try_start_0
    sget-object v4, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v5, "FDL: prepare to open itme [item: %s]  [service: %s]"

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v7, 0x1

    if-nez p2, :cond_2

    const-string v3, "null"

    :goto_1
    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    const-string v3, "flipboardURL"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "flipboardURL"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v7, v3

    .line 55
    :goto_2
    const-string v3, "linkURL"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "linkURL"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 56
    :goto_3
    const-string v3, "nativeAppLink"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "nativeAppLink"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 57
    :goto_4
    const-string v5, "nativeAppType"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "nativeAppType"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    move v6, v5

    .line 62
    :goto_5
    if-nez v6, :cond_7

    move-object v0, v2

    move v5, v1

    .line 71
    :goto_6
    if-eqz v5, :cond_b

    .line 72
    if-eqz v0, :cond_a

    if-eqz v3, :cond_a

    move-object v0, v3

    .line 73
    :goto_7
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v3, v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v2, v3

    .line 84
    :goto_8
    :try_start_1
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v3, "FDL: create detail intent [nativeAppType: %s]"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v0, v3, v5}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v3, "FDL: create detail intent flipboardURL is %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v5}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v3, "FDL: create detail intent linkURL is %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    invoke-virtual {v0, v3, v5}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    const/high16 v0, 0x10000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 89
    const/high16 v0, 0x80000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 90
    const-string v0, "apiPartner"

    const-string v3, "samsung"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    const-string v0, "apiVersion"

    const/4 v3, 0x2

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 92
    const-string v0, "fdlVersion"

    const-string v3, "0.67"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 94
    const-string v0, "section_title"

    invoke-virtual {v2, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    :cond_1
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v3, "FDL: successfully created intent to open item"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 99
    if-eqz v3, :cond_e

    .line 100
    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 101
    invoke-virtual {v3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 102
    sget-object v6, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v7, "[%s: %s]"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    const/4 v0, 0x1

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v0

    invoke-virtual {v6, v7, v8}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_9

    .line 107
    :catch_0
    move-exception v0

    move-object v10, v0

    move-object v0, v2

    move-object v2, v10

    .line 108
    :goto_a
    sget-object v3, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FDL: Exception when creating intent to open item %s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v1}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 53
    :cond_2
    :try_start_2
    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    :cond_3
    move-object v7, v2

    .line 54
    goto/16 :goto_2

    :cond_4
    move-object v4, v2

    .line 55
    goto/16 :goto_3

    :cond_5
    move-object v3, v2

    .line 56
    goto/16 :goto_4

    :cond_6
    move v6, v1

    .line 57
    goto/16 :goto_5

    .line 68
    :cond_7
    invoke-static {p2, p0}, Lcom/flipboard/util/IntentUtil;->getInstalledAppPackage(Lorg/json/JSONObject;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 69
    if-eq v6, v8, :cond_8

    if-eqz v3, :cond_9

    if-eqz v5, :cond_9

    :cond_8
    :goto_b
    move-object v10, v5

    move v5, v0

    move-object v0, v10

    goto/16 :goto_6

    :cond_9
    move v0, v1

    goto :goto_b

    :cond_a
    move-object v0, v4

    .line 72
    goto/16 :goto_7

    .line 75
    :cond_b
    if-eqz v0, :cond_c

    const-string v3, "flipboard"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 76
    :cond_c
    new-instance v3, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 77
    const/4 v0, 0x1

    :try_start_3
    invoke-static {p0, v0}, Lcom/flipboard/util/FDLUtil;->getInstalledFlipboardPackage(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    move-object v2, v3

    goto/16 :goto_8

    .line 79
    :cond_d
    :try_start_4
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v3, v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1

    .line 80
    :try_start_5
    invoke-virtual {v3, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_3

    move-object v2, v3

    goto/16 :goto_8

    :cond_e
    move-object v0, v2

    .line 109
    goto/16 :goto_0

    .line 107
    :catch_1
    move-exception v0

    move-object v10, v0

    move-object v0, v2

    move-object v2, v10

    goto :goto_a

    :catch_2
    move-exception v0

    move-object v2, v0

    move-object v0, v3

    goto/16 :goto_a

    :catch_3
    move-exception v0

    move-object v2, v0

    move-object v0, v3

    goto/16 :goto_a
.end method

.method private static getInstalledAppPackage(Lorg/json/JSONObject;Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 117
    if-eqz p0, :cond_1

    :try_start_0
    const-string v0, "nativeAppPackages"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    const-string v0, "nativeAppPackages"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 119
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 120
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 121
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-static {v0, p1}, Lcom/flipboard/util/IntentUtil;->isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 132
    :goto_1
    return-object v0

    .line 120
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 128
    :catch_0
    move-exception v0

    .line 129
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    invoke-virtual {v1, v0}, Lcom/flipboard/util/Log;->error(Ljava/lang/Throwable;)V

    .line 132
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 137
    if-eqz p0, :cond_1

    .line 138
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    .line 139
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 140
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const/4 v0, 0x1

    .line 145
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
