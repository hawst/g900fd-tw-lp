.class final Lcom/flipboard/util/Log$1;
.super Ljava/lang/Object;
.source "Log.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/util/Log;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 33
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    sget-object v1, Lcom/flipboard/util/Log$Level;->ERROR:Lcom/flipboard/util/Log$Level;

    const-string v2, "%-E"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    # invokes: Lcom/flipboard/util/Log;->output(Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V
    invoke-static {v0, v1, v2, v3}, Lcom/flipboard/util/Log;->access$000(Lcom/flipboard/util/Log;Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    sget-object v0, Lcom/flipboard/util/Log;->defaultHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 41
    return-void

    .line 37
    :catch_0
    move-exception v0

    goto :goto_0
.end method
