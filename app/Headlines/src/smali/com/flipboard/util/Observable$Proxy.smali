.class public Lcom/flipboard/util/Observable$Proxy;
.super Lcom/flipboard/util/Observable;
.source "Observable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/util/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Proxy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        "M:",
        "Ljava/lang/Object;",
        "A:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/flipboard/util/Observable",
        "<TE;TM;TA;>;"
    }
.end annotation


# instance fields
.field final source:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/flipboard/util/Observable;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/flipboard/util/Observable$Proxy;->source:Ljava/lang/Object;

    .line 41
    return-void
.end method


# virtual methods
.method public getSource()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/flipboard/util/Observable$Proxy;->source:Ljava/lang/Object;

    return-object v0
.end method
