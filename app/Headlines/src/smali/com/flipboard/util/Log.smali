.class public final Lcom/flipboard/util/Log;
.super Ljava/lang/Object;
.source "Log.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipboard/util/Log$2;,
        Lcom/flipboard/util/Log$Level;
    }
.end annotation


# static fields
.field public static final START_WITH_LOGS:Ljava/lang/String; = "start_with_logs"

.field private static final TAG:Ljava/lang/String; = "flipboard_fdl"

.field static final defaultHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

.field static final logs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/flipboard/util/Log;",
            ">;"
        }
    .end annotation
.end field

.field public static final main:Lcom/flipboard/util/Log;


# instance fields
.field enabled:Z

.field final level:Lcom/flipboard/util/Log$Level;

.field final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    new-instance v0, Lcom/flipboard/util/Log;

    const-string v1, "main"

    sget-object v2, Lcom/flipboard/util/Log$Level;->DEBUG:Lcom/flipboard/util/Log$Level;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/flipboard/util/Log;-><init>(Ljava/lang/String;Lcom/flipboard/util/Log$Level;Z)V

    sput-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/flipboard/util/Log;->logs:Ljava/util/Map;

    .line 27
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    sput-object v0, Lcom/flipboard/util/Log;->defaultHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 29
    new-instance v0, Lcom/flipboard/util/Log$1;

    invoke-direct {v0}, Lcom/flipboard/util/Log$1;-><init>()V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 43
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/flipboard/util/Log$Level;Z)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/flipboard/util/Log;->name:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lcom/flipboard/util/Log;->level:Lcom/flipboard/util/Log$Level;

    .line 73
    iput-boolean p3, p0, Lcom/flipboard/util/Log;->enabled:Z

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/flipboard/util/Log;Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/flipboard/util/Log;->output(Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static declared-synchronized getLog(Ljava/lang/String;)Lcom/flipboard/util/Log;
    .locals 5

    .prologue
    .line 47
    const-class v1, Lcom/flipboard/util/Log;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/flipboard/util/Log;->logs:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipboard/util/Log;

    .line 48
    if-nez v0, :cond_0

    .line 49
    sget-object v2, Lcom/flipboard/util/Log;->logs:Ljava/util/Map;

    new-instance v0, Lcom/flipboard/util/Log;

    sget-object v3, Lcom/flipboard/util/Log$Level;->CUSTOM:Lcom/flipboard/util/Log$Level;

    const/4 v4, 0x0

    invoke-direct {v0, p0, v3, v4}, Lcom/flipboard/util/Log;-><init>(Ljava/lang/String;Lcom/flipboard/util/Log$Level;Z)V

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    :cond_0
    monitor-exit v1

    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getLogNames()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    const-class v1, Lcom/flipboard/util/Log;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/flipboard/util/Log;->logs:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 57
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    monitor-exit v1

    return-object v0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static maskString(ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 177
    if-nez p1, :cond_0

    .line 178
    const-string v0, "n/a"

    .line 184
    :goto_0
    return-object v0

    .line 181
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, p0, :cond_2

    .line 182
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, p0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 184
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, p0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "****"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, p1

    goto :goto_1
.end method

.method private varargs declared-synchronized output(Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/flipboard/util/Log$Level;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/flipboard/util/Log;->level:Lcom/flipboard/util/Log$Level;

    invoke-virtual {v1}, Lcom/flipboard/util/Log$Level;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/flipboard/util/Log;->enabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 102
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 95
    :cond_1
    :try_start_1
    invoke-static {p2, p3}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/flipboard/util/Log;->name:Ljava/lang/String;

    const-string v2, "main"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 97
    const-string v1, "[%s:%d] %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/flipboard/util/Log$Level;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 101
    :goto_1
    invoke-virtual {p0, p1, v0}, Lcom/flipboard/util/Log;->output0(Lcom/flipboard/util/Log$Level;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 99
    :cond_2
    :try_start_2
    const-string v1, "%s: [%s:%d] %s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/flipboard/util/Log;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p1}, Lcom/flipboard/util/Log$Level;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public varargs debug(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/flipboard/util/Log$Level;->DEBUG:Lcom/flipboard/util/Log$Level;

    invoke-direct {p0, v0, p1, p2}, Lcom/flipboard/util/Log;->output(Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    return-void
.end method

.method public debug(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 150
    sget-object v0, Lcom/flipboard/util/Log$Level;->DEBUG:Lcom/flipboard/util/Log$Level;

    const-string v1, "%-E"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/flipboard/util/Log;->output(Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    return-void
.end method

.method public varargs error(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/flipboard/util/Log$Level;->ERROR:Lcom/flipboard/util/Log$Level;

    invoke-direct {p0, v0, p1, p2}, Lcom/flipboard/util/Log;->output(Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    return-void
.end method

.method public error(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 165
    sget-object v0, Lcom/flipboard/util/Log$Level;->ERROR:Lcom/flipboard/util/Log$Level;

    const-string v1, "%-E"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/flipboard/util/Log;->output(Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 167
    return-void
.end method

.method public varargs info(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/flipboard/util/Log$Level;->INFO:Lcom/flipboard/util/Log$Level;

    invoke-direct {p0, v0, p1, p2}, Lcom/flipboard/util/Log;->output(Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 136
    return-void
.end method

.method public info(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 155
    sget-object v0, Lcom/flipboard/util/Log$Level;->INFO:Lcom/flipboard/util/Log$Level;

    const-string v1, "%-e"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/flipboard/util/Log;->output(Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 156
    return-void
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/flipboard/util/Log;->enabled:Z

    return v0
.end method

.method protected output0(Lcom/flipboard/util/Log$Level;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 106
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    .line 107
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 108
    sub-int v2, v1, v0

    const/16 v3, 0x800

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 110
    add-int v3, v0, v2

    invoke-virtual {p2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 111
    sget-object v4, Lcom/flipboard/util/Log$2;->$SwitchMap$com$flipboard$util$Log$Level:[I

    invoke-virtual {p1}, Lcom/flipboard/util/Log$Level;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 107
    :goto_1
    add-int/2addr v0, v2

    goto :goto_0

    .line 113
    :pswitch_0
    const-string v4, "flipboard_fdl"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 116
    :pswitch_1
    const-string v4, "flipboard_fdl"

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 119
    :pswitch_2
    const-string v4, "flipboard_fdl"

    invoke-static {v4, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 122
    :pswitch_3
    const-string v4, "flipboard_fdl"

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 126
    :cond_0
    return-void

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setEnabled(Z)Lcom/flipboard/util/Log;
    .locals 5

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/flipboard/util/Log;->enabled:Z

    if-eq v0, p1, :cond_0

    .line 84
    iput-boolean p1, p0, Lcom/flipboard/util/Log;->enabled:Z

    .line 85
    sget-object v1, Lcom/flipboard/util/Log$Level;->DEBUG:Lcom/flipboard/util/Log$Level;

    const-string v2, "%s: logging %s"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/flipboard/util/Log;->name:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v4, 0x1

    if-eqz p1, :cond_1

    const-string v0, "on"

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/flipboard/util/Log;->output0(Lcom/flipboard/util/Log$Level;Ljava/lang/String;)V

    .line 87
    :cond_0
    return-object p0

    .line 85
    :cond_1
    const-string v0, "off"

    goto :goto_0
.end method

.method public varargs warning(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/flipboard/util/Log$Level;->WARN:Lcom/flipboard/util/Log$Level;

    invoke-direct {p0, v0, p1, p2}, Lcom/flipboard/util/Log;->output(Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    return-void
.end method

.method public warning(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 160
    sget-object v0, Lcom/flipboard/util/Log$Level;->WARN:Lcom/flipboard/util/Log$Level;

    const-string v1, "%-e"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/flipboard/util/Log;->output(Lcom/flipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 161
    return-void
.end method
