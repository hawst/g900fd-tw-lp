.class public Lcom/flipboard/util/FDLUtil;
.super Ljava/lang/Object;
.source "FDLUtil.java"


# static fields
.field private static final ACCOUNT_SYNC_ACTION:Ljava/lang/String; = "flipboard.app.broadcast.SYNC_USER_CHANGE"

.field private static final BROADCAST_PERMISSION:Ljava/lang/String; = "sstream.app.broadcast.SYNC_USER"

.field public static final CLIENT_PARTNER_DEFAULT_CNN:Ljava/lang/String; = "cnn"

.field public static final FDL_VERSION:Ljava/lang/String; = "0.67"

.field public static final FLIPBOARD_CHINA_DEV_PACKAGE:Ljava/lang/String; = "flipboard.cn.debug"

.field public static final FLIPBOARD_CHINA_PACKAGE:Ljava/lang/String; = "flipboard.cn"

.field public static final FLIPBOARD_DEV_PACKAGE:Ljava/lang/String; = "flipboard.internal.debug"

.field public static final FLIPBOARD_INTERNL_BETA:Ljava/lang/String; = "flipboard.internal"

.field public static final FLIPBOARD_PACKAGE:Ljava/lang/String; = "flipboard.app"

.field public static final FLIPBOARD_SERVICE:Ljava/lang/String; = "flipboard"

.field private static final KEY_BROADCAST_OAUTH_TOKEN:Ljava/lang/String; = "oauth_token"

.field public static final KEY_FDL_VERSION:Ljava/lang/String; = "fdlVersion"

.field private static final KEY_SYNC_SERVICE:Ljava/lang/String; = "logged_in_to_service"

.field public static final SHARED_PREFS_NAME:Ljava/lang/String; = "com.flipboard.fdl.sharedPreferences"

.field private static clientPartner:Ljava/lang/String;

.field private static device:Ljava/lang/String;

.field private static installedFlipboardPackage:Ljava/lang/String;

.field public static final isAdDebuggingMode:Z

.field private static model:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static SHA1str(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 240
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 241
    const-string v1, "SHA1"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 242
    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, Ljava/security/MessageDigest;->update([BII)V

    .line 243
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    .line 244
    const-string v1, "%08x%08x%08x%08x%08x"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/flipboard/util/FDLUtil;->toInteger([BI)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x4

    invoke-static {v0, v4}, Lcom/flipboard/util/FDLUtil;->toInteger([BI)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const/16 v4, 0x8

    invoke-static {v0, v4}, Lcom/flipboard/util/FDLUtil;->toInteger([BI)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/16 v4, 0xc

    invoke-static {v0, v4}, Lcom/flipboard/util/FDLUtil;->toInteger([BI)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const/16 v4, 0x10

    invoke-static {v0, v4}, Lcom/flipboard/util/FDLUtil;->toInteger([BI)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 245
    :catch_0
    move-exception v0

    .line 246
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SHA1: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static appMode(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    if-eqz p0, :cond_0

    const-string v0, "apad"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "aphone"

    goto :goto_0
.end method

.method public static device(Z)Ljava/lang/String;
    .locals 4

    .prologue
    .line 164
    sget-object v0, Lcom/flipboard/util/FDLUtil;->device:Ljava/lang/String;

    .line 165
    if-nez v0, :cond_0

    .line 166
    if-eqz p0, :cond_1

    const-string v0, "apad"

    .line 167
    :goto_0
    const-string v1, "%s-%s-%s-%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x3

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 168
    sput-object v0, Lcom/flipboard/util/FDLUtil;->device:Ljava/lang/String;

    .line 170
    :cond_0
    return-object v0

    .line 166
    :cond_1
    const-string v0, "aphone"

    goto :goto_0
.end method

.method public static escapeURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 191
    :try_start_0
    const-string v0, "utf-8"

    invoke-static {p0, v0}, Lcom/flipboard/util/FDLUtil;->escapeURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 194
    :goto_0
    return-object p0

    .line 192
    :catch_0
    move-exception v0

    .line 193
    sget-object v1, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    invoke-virtual {v1, v0}, Lcom/flipboard/util/Log;->warning(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static escapeURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 200
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static fireTokenBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 58
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "fire token broadcast %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p1, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    if-nez p1, :cond_0

    .line 61
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "No token present, not firing account sync intent"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-static {}, Lcom/flipboard/data/Request;->getInstance()Lcom/flipboard/data/Request;

    move-result-object v0

    if-nez v0, :cond_1

    .line 66
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "Try to fire token broadcast before Request is initialized"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 70
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v0, "flipboard.app.broadcast.SYNC_USER_CHANGE"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 71
    invoke-static {p0}, Lcom/flipboard/util/FDLUtil;->getFlipboardPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 72
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x6

    if-le v0, v3, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 74
    :goto_1
    const-string v3, "oauth_token"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    const-string v3, "logged_in_to_service"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    const-string v3, "fdlVersion"

    const-string v4, "0.67"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 79
    sget-object v3, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v4, "FDL: broadcast intent [to: %s] [fdlVersion: %s] [authToken: %s] [service: %s]"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    const-string v2, "0.67"

    aput-object v2, v5, v7

    const/4 v2, 0x2

    aput-object v0, v5, v2

    const/4 v0, 0x3

    aput-object p2, v5, v0

    invoke-virtual {v3, v4, v5}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    invoke-static {}, Lcom/flipboard/data/Request;->getInstance()Lcom/flipboard/data/Request;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flipboard/data/Request;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "sstream.app.broadcast.SYNC_USER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 73
    goto :goto_1
.end method

.method public static generateDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    invoke-static {p0}, Lcom/flipboard/util/FDLUtil;->getDeviceSerialNumber(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/flipboard/util/FDLUtil;->SHA1str(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getClientPartner()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lcom/flipboard/util/FDLUtil;->clientPartner:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "cnn"

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/flipboard/util/FDLUtil;->clientPartner:Ljava/lang/String;

    goto :goto_0
.end method

.method private static getDeviceSerialNumber(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 224
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 225
    if-eqz v0, :cond_0

    const-string v1, "9774d56d682e549c"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 227
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 229
    :cond_1
    return-object v0
.end method

.method public static getFlipboardPackage(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/flipboard/util/FDLUtil;->getInstalledFlipboardPackage(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 94
    if-nez v0, :cond_0

    .line 96
    const-string v0, "flipboard.app"

    .line 98
    :cond_0
    return-object v0
.end method

.method public static getInstalledFlipboardPackage(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 115
    sget-object v1, Lcom/flipboard/util/FDLUtil;->installedFlipboardPackage:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 121
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 123
    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v1

    .line 125
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    move v5, v0

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 126
    const-string v8, "flipboard.cn.debug"

    iget-object v9, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    move v0, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v6

    :goto_1
    move v5, v4

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    .line 135
    goto :goto_0

    .line 128
    :cond_0
    const-string v8, "flipboard.cn"

    iget-object v9, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move v0, v1

    move v4, v5

    move v1, v2

    move v2, v3

    move v3, v6

    .line 129
    goto :goto_1

    .line 130
    :cond_1
    const-string v8, "flipboard.internal.debug"

    iget-object v9, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v0, v1

    move v3, v4

    move v1, v2

    move v4, v5

    move v2, v6

    .line 131
    goto :goto_1

    .line 132
    :cond_2
    const-string v8, "flipboard.internal"

    iget-object v9, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v0, v1

    move v2, v3

    move v1, v6

    move v3, v4

    move v4, v5

    .line 133
    goto :goto_1

    .line 134
    :cond_3
    const-string v8, "flipboard.app"

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v6

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v5

    .line 135
    goto :goto_1

    .line 138
    :cond_4
    if-eqz p1, :cond_6

    if-eqz v5, :cond_6

    .line 140
    const-string v0, "flipboard.cn.debug"

    sput-object v0, Lcom/flipboard/util/FDLUtil;->installedFlipboardPackage:Ljava/lang/String;

    .line 151
    :cond_5
    :goto_2
    sget-object v0, Lcom/flipboard/util/FDLUtil;->installedFlipboardPackage:Ljava/lang/String;

    return-object v0

    .line 141
    :cond_6
    if-eqz p1, :cond_7

    if-eqz v4, :cond_7

    .line 142
    const-string v0, "flipboard.cn"

    sput-object v0, Lcom/flipboard/util/FDLUtil;->installedFlipboardPackage:Ljava/lang/String;

    goto :goto_2

    .line 143
    :cond_7
    if-eqz v3, :cond_8

    .line 144
    const-string v0, "flipboard.internal.debug"

    sput-object v0, Lcom/flipboard/util/FDLUtil;->installedFlipboardPackage:Ljava/lang/String;

    goto :goto_2

    .line 145
    :cond_8
    if-eqz v2, :cond_9

    .line 146
    const-string v0, "flipboard.internal"

    sput-object v0, Lcom/flipboard/util/FDLUtil;->installedFlipboardPackage:Ljava/lang/String;

    goto :goto_2

    .line 147
    :cond_9
    if-eqz v1, :cond_5

    .line 148
    const-string v0, "flipboard.app"

    sput-object v0, Lcom/flipboard/util/FDLUtil;->installedFlipboardPackage:Ljava/lang/String;

    goto :goto_2

    :cond_a
    move v0, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v5

    goto :goto_1
.end method

.method public static getScreenInches(Landroid/content/Context;)F
    .locals 3

    .prologue
    .line 156
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 157
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    iget v2, v0, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v1, v2

    .line 158
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    iget v0, v0, Landroid/util/DisplayMetrics;->ydpi:F

    div-float v0, v2, v0

    .line 159
    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static model()Ljava/lang/String;
    .locals 4

    .prologue
    .line 180
    sget-object v0, Lcom/flipboard/util/FDLUtil;->model:Ljava/lang/String;

    .line 181
    if-nez v0, :cond_0

    .line 182
    const-string v0, "%s-%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 183
    sput-object v0, Lcom/flipboard/util/FDLUtil;->model:Ljava/lang/String;

    .line 185
    :cond_0
    return-object v0
.end method

.method public static setClientPartner(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 205
    sput-object p0, Lcom/flipboard/util/FDLUtil;->clientPartner:Ljava/lang/String;

    .line 206
    return-void
.end method

.method private static toInteger([BI)I
    .locals 2

    .prologue
    .line 234
    add-int/lit8 v0, p1, 0x0

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x0

    add-int/2addr v0, v1

    return v0
.end method
