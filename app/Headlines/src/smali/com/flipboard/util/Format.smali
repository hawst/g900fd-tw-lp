.class public Lcom/flipboard/util/Format;
.super Ljava/lang/Object;
.source "Format.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipboard/util/Format$1;,
        Lcom/flipboard/util/Format$CharBuf;,
        Lcom/flipboard/util/Format$Exception;,
        Lcom/flipboard/util/Format$Selector;
    }
.end annotation


# static fields
.field private static final HEX_LOWER:[C

.field private static final HEX_UPPER:[C

.field private static final QUESTIONMARKS:Ljava/lang/String; = "???"

.field private static final SUPPRESS_EXCEPTIONS:Z = true


# instance fields
.field argWidth:Z

.field args:[Ljava/lang/Object;

.field buf:Lcom/flipboard/util/Format$CharBuf;

.field comma:Z

.field fmt:Ljava/lang/String;

.field left:Z

.field precision:I

.field width:I

.field zeroPad:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "0123456789ABCDEF"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/flipboard/util/Format;->HEX_UPPER:[C

    .line 18
    const-string v0, "0123456789abcdef"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/flipboard/util/Format;->HEX_LOWER:[C

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/flipboard/util/Format;->fmt:Ljava/lang/String;

    .line 109
    iput-object p2, p0, Lcom/flipboard/util/Format;->args:[Ljava/lang/Object;

    .line 110
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;[Ljava/lang/Object;Lcom/flipboard/util/Format$1;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lcom/flipboard/util/Format;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$100(Lcom/flipboard/util/Format;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lcom/flipboard/util/Format;->output(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200(Lcom/flipboard/util/Format;Ljava/lang/RuntimeException;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/flipboard/util/Format;->throwIfAllowed(Ljava/lang/RuntimeException;)V

    return-void
.end method

.method static synthetic access$300()[C
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/flipboard/util/Format;->HEX_UPPER:[C

    return-object v0
.end method

.method static synthetic access$400()[C
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/flipboard/util/Format;->HEX_LOWER:[C

    return-object v0
.end method

.method public static varargs create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/flipboard/util/Format;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/flipboard/util/Format;

    invoke-direct {v0, p0, p1}, Lcom/flipboard/util/Format;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public static varargs format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/flipboard/util/Format;

    invoke-direct {v0, p0, p1}, Lcom/flipboard/util/Format;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/flipboard/util/Format;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private formatIfNecessary()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/flipboard/util/Format;->args:[Ljava/lang/Object;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/flipboard/util/Format;->buf:Lcom/flipboard/util/Format$CharBuf;

    if-nez v0, :cond_0

    .line 284
    new-instance v0, Lcom/flipboard/util/Format$CharBuf;

    iget-object v1, p0, Lcom/flipboard/util/Format;->fmt:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    invoke-direct {v0, p0, v1}, Lcom/flipboard/util/Format$CharBuf;-><init>(Lcom/flipboard/util/Format;I)V

    iput-object v0, p0, Lcom/flipboard/util/Format;->buf:Lcom/flipboard/util/Format$CharBuf;

    .line 285
    iget-object v0, p0, Lcom/flipboard/util/Format;->fmt:Ljava/lang/String;

    iget-object v1, p0, Lcom/flipboard/util/Format;->args:[Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/flipboard/util/Format;->output(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    :cond_0
    return-void
.end method

.method public static join(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 65
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    const/4 v0, 0x0

    .line 67
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 68
    add-int/lit8 v1, v0, 0x1

    if-lez v0, :cond_0

    .line 69
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    :cond_0
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_0

    .line 73
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static join(Ljava/lang/String;Ljava/lang/Iterable;Lcom/flipboard/util/Format$Selector;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Iterable",
            "<TT;>;",
            "Lcom/flipboard/util/Format$Selector",
            "<TT;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 83
    const/4 v0, 0x0

    .line 84
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 85
    invoke-interface {p2, v1}, Lcom/flipboard/util/Format$Selector;->get(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 86
    add-int/lit8 v1, v0, 0x1

    if-lez v0, :cond_0

    .line 87
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    :cond_0
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 90
    goto :goto_0

    .line 91
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static join(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p0, p1, v0, v1}, Lcom/flipboard/util/Format;->join(Ljava/lang/String;[Ljava/lang/Object;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static join(Ljava/lang/String;[Ljava/lang/Object;II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 50
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_1

    .line 52
    if-lez v0, :cond_0

    .line 53
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    :cond_0
    add-int v2, p2, v0

    aget-object v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 10

    .prologue
    const-wide v8, 0x7fffffffffffffffL

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 646
    const-string v0, "maxInt = %d, %x, %X"

    new-array v1, v7, [Ljava/lang/Object;

    const v2, 0x7fffffff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const v2, 0x7fffffff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const v2, 0x7fffffff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->test(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 647
    const-string v0, "0 = %d, %x, %X"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->test(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 648
    const-string v0, "123456789 = %d, %x, %X"

    new-array v1, v7, [Ljava/lang/Object;

    const v2, 0x75bcd15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const v2, 0x75bcd15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const v2, 0x75bcd15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->test(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 649
    const-string v0, "-1 = %d, %x, %X"

    new-array v1, v7, [Ljava/lang/Object;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->test(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 650
    const-string v0, "0x123456789abcdef = %d, %x, %X"

    new-array v1, v7, [Ljava/lang/Object;

    const-wide v2, 0x123456789abcdefL

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    const-wide v2, 0x123456789abcdefL

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    const-wide v2, 0x123456789abcdefL

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->test(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 651
    const-string v0, "maxLong = %d, %x, %X"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->test(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 652
    const-string v0, "maxLong = %1$d, %1$x, %X"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->test(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 653
    const-string v0, "maxLong = %010d, %1$x, %1X"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->test(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 655
    const-string v0, "%1$-10s| |%10s| |%10s| |%1$10s"

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "one"

    aput-object v2, v1, v4

    const-string v2, "two"

    aput-object v2, v1, v5

    const-string v2, "three"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/flipboard/util/Format;->test(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 659
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "%1$-10s| |%10s| |%10s| |%1$10s outofrange %10$s"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "one"

    aput-object v3, v2, v4

    const-string v3, "two"

    aput-object v3, v2, v5

    const-string v3, "three"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 660
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "%d"

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "hello"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 661
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "This is a % test"

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "hello"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 664
    const/4 v0, 0x0

    .line 666
    :try_start_0
    const-string v1, "%-E"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 670
    :goto_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "This is a stack trace: %-E"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 671
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "This is a stack trace: %-e"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 672
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "This is a stack trace: %E"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 673
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "This is a stack trace: %e"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 674
    return-void

    .line 667
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private output(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 114
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    .line 115
    const/4 v4, 0x0

    .line 116
    const/4 v6, 0x0

    :goto_0
    if-ge v6, v7, :cond_1

    .line 117
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 118
    const/16 v1, 0x25

    if-eq v0, v1, :cond_0

    .line 119
    iget-object v1, p0, Lcom/flipboard/util/Format;->buf:Lcom/flipboard/util/Format$CharBuf;

    invoke-virtual {v1, v0}, Lcom/flipboard/util/Format$CharBuf;->putChar(C)V

    move v2, v6

    move v3, v4

    .line 116
    :goto_1
    add-int/lit8 v6, v2, 0x1

    move v4, v3

    goto :goto_0

    .line 127
    :cond_0
    const/4 v1, 0x0

    .line 128
    const/4 v0, 0x0

    iput v0, p0, Lcom/flipboard/util/Format;->width:I

    .line 129
    add-int/lit8 v2, v6, 0x1

    invoke-virtual {p0, v2, v7}, Lcom/flipboard/util/Format;->checkEOF(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 258
    :cond_1
    return-void

    .line 132
    :cond_2
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 133
    const/16 v3, 0x31

    if-lt v0, v3, :cond_3

    const/16 v3, 0x39

    if-gt v0, v3, :cond_3

    .line 134
    add-int/lit8 v3, v0, -0x30

    move v11, v3

    move v3, v2

    move v2, v0

    move v0, v11

    .line 136
    :goto_2
    add-int/lit8 v3, v3, 0x1

    if-ge v3, v7, :cond_10

    .line 137
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 138
    packed-switch v2, :pswitch_data_0

    .line 153
    :pswitch_0
    iput v0, p0, Lcom/flipboard/util/Format;->width:I

    move v0, v2

    move v2, v3

    .line 160
    :cond_3
    :goto_3
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/flipboard/util/Format;->argWidth:Z

    iput-boolean v3, p0, Lcom/flipboard/util/Format;->zeroPad:Z

    iput-boolean v3, p0, Lcom/flipboard/util/Format;->comma:Z

    iput-boolean v3, p0, Lcom/flipboard/util/Format;->left:Z

    .line 161
    iget v3, p0, Lcom/flipboard/util/Format;->width:I

    if-nez v3, :cond_f

    .line 164
    :goto_4
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 165
    packed-switch v3, :pswitch_data_1

    .line 180
    :cond_4
    :pswitch_1
    invoke-virtual {p0, v2, v7}, Lcom/flipboard/util/Format;->checkEOF(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 185
    const/4 v0, 0x0

    iput v0, p0, Lcom/flipboard/util/Format;->width:I

    .line 186
    iget-boolean v0, p0, Lcom/flipboard/util/Format;->argWidth:Z

    if-eqz v0, :cond_e

    .line 187
    array-length v0, p2

    if-lt v4, v0, :cond_6

    .line 188
    new-instance v0, Lcom/flipboard/util/Format$Exception;

    const-string v5, "missing argument for %s at index %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    add-int/lit8 v10, v2, 0x1

    invoke-virtual {p1, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-direct {v0, v5, v8}, Lcom/flipboard/util/Format$Exception;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/flipboard/util/Format;->throwIfAllowed(Ljava/lang/RuntimeException;)V

    move v0, v3

    move v3, v4

    .line 212
    :goto_5
    invoke-virtual {p0, v2, v7}, Lcom/flipboard/util/Format;->checkEOF(II)Z

    move-result v4

    if-nez v4, :cond_1

    .line 217
    const/4 v4, -0x1

    iput v4, p0, Lcom/flipboard/util/Format;->precision:I

    .line 218
    const/16 v4, 0x2e

    if-ne v0, v4, :cond_5

    .line 219
    const/4 v4, 0x0

    iput v4, p0, Lcom/flipboard/util/Format;->precision:I

    .line 221
    :goto_6
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v7, :cond_5

    .line 222
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 223
    packed-switch v0, :pswitch_data_2

    .line 234
    :cond_5
    invoke-virtual {p0, v2, v7}, Lcom/flipboard/util/Format;->checkEOF(II)Z

    move-result v4

    if-nez v4, :cond_1

    .line 239
    const/16 v4, 0x25

    if-ne v0, v4, :cond_8

    .line 240
    iget-object v0, p0, Lcom/flipboard/util/Format;->buf:Lcom/flipboard/util/Format$CharBuf;

    const/16 v1, 0x25

    invoke-virtual {v0, v1}, Lcom/flipboard/util/Format$CharBuf;->putChar(C)V

    goto/16 :goto_1

    .line 141
    :pswitch_2
    mul-int/lit8 v0, v0, 0xa

    add-int/lit8 v5, v2, -0x30

    add-int/2addr v0, v5

    .line 142
    goto/16 :goto_2

    .line 148
    :pswitch_3
    add-int/lit8 v1, v3, 0x1

    move v11, v0

    move v0, v2

    move v2, v1

    move v1, v11

    .line 149
    goto :goto_3

    .line 166
    :pswitch_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flipboard/util/Format;->left:Z

    .line 173
    :goto_7
    add-int/lit8 v2, v2, 0x1

    if-ge v2, v7, :cond_4

    .line 176
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    goto :goto_4

    .line 167
    :pswitch_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flipboard/util/Format;->comma:Z

    goto :goto_7

    .line 168
    :pswitch_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flipboard/util/Format;->zeroPad:Z

    goto :goto_7

    .line 169
    :pswitch_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flipboard/util/Format;->argWidth:Z

    goto :goto_7

    .line 191
    :cond_6
    add-int/lit8 v5, v4, 0x1

    aget-object v0, p2, v4

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    iput v0, p0, Lcom/flipboard/util/Format;->width:I

    move v0, v3

    move v3, v5

    goto :goto_5

    .line 207
    :cond_7
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 196
    :goto_8
    packed-switch v0, :pswitch_data_3

    move v3, v4

    .line 202
    goto :goto_5

    .line 199
    :pswitch_8
    iget v3, p0, Lcom/flipboard/util/Format;->width:I

    mul-int/lit8 v3, v3, 0xa

    add-int/lit8 v5, v0, -0x30

    add-int/2addr v3, v5

    iput v3, p0, Lcom/flipboard/util/Format;->width:I

    .line 204
    add-int/lit8 v2, v2, 0x1

    if-lt v2, v7, :cond_7

    move v3, v4

    .line 205
    goto :goto_5

    .line 226
    :pswitch_9
    iget v4, p0, Lcom/flipboard/util/Format;->precision:I

    mul-int/lit8 v4, v4, 0xa

    add-int/lit8 v5, v0, -0x30

    add-int/2addr v4, v5

    iput v4, p0, Lcom/flipboard/util/Format;->precision:I

    goto :goto_6

    .line 244
    :cond_8
    const/16 v4, 0x74

    if-eq v0, v4, :cond_9

    const/16 v4, 0x54

    if-ne v0, v4, :cond_b

    .line 246
    :cond_9
    iget-object v1, p0, Lcom/flipboard/util/Format;->buf:Lcom/flipboard/util/Format$CharBuf;

    const/16 v4, 0x54

    if-ne v0, v4, :cond_a

    const/4 v0, 0x1

    :goto_9
    iget v4, p0, Lcom/flipboard/util/Format;->width:I

    invoke-virtual {v1, v0, v4}, Lcom/flipboard/util/Format$CharBuf;->putDumpStack(ZI)V

    goto/16 :goto_1

    :cond_a
    const/4 v0, 0x0

    goto :goto_9

    .line 249
    :cond_b
    if-lez v1, :cond_c

    add-int/lit8 v1, v1, -0x1

    move v11, v1

    move v1, v3

    move v3, v11

    .line 250
    :goto_a
    array-length v4, p2

    if-lt v3, v4, :cond_d

    .line 251
    new-instance v0, Lcom/flipboard/util/Format$Exception;

    const-string v3, "missing argument for %s at index %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    add-int/lit8 v8, v2, 0x1

    invoke-virtual {p1, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v0, v3, v4}, Lcom/flipboard/util/Format$Exception;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/flipboard/util/Format;->throwIfAllowed(Ljava/lang/RuntimeException;)V

    .line 252
    iget-object v0, p0, Lcom/flipboard/util/Format;->buf:Lcom/flipboard/util/Format$CharBuf;

    const/16 v3, 0x73

    const-string v4, "???"

    invoke-virtual {v0, v3, v4}, Lcom/flipboard/util/Format$CharBuf;->putFormat(CLjava/lang/Object;)V

    move v3, v1

    goto/16 :goto_1

    .line 249
    :cond_c
    add-int/lit8 v1, v3, 0x1

    goto :goto_a

    .line 254
    :cond_d
    iget-object v4, p0, Lcom/flipboard/util/Format;->buf:Lcom/flipboard/util/Format$CharBuf;

    aget-object v3, p2, v3

    invoke-virtual {v4, v0, v3}, Lcom/flipboard/util/Format$CharBuf;->putFormat(CLjava/lang/Object;)V

    move v3, v1

    goto/16 :goto_1

    :cond_e
    move v0, v3

    goto :goto_8

    :cond_f
    move v3, v4

    goto/16 :goto_5

    :cond_10
    move v0, v2

    move v2, v3

    goto/16 :goto_3

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x24
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 165
    :pswitch_data_1
    .packed-switch 0x2a
        :pswitch_7
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_6
    .end packed-switch

    .line 223
    :pswitch_data_2
    .packed-switch 0x30
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch

    .line 196
    :pswitch_data_3
    .packed-switch 0x30
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method private static varargs test(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 633
    invoke-static {p0, p1}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 634
    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 635
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 636
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "mismatch:\n   \'%s\' !=\n   \'%s\'\n"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Ljava/io/PrintStream;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    .line 641
    :goto_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->println()V

    .line 642
    return-void

    .line 638
    :cond_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "pass: %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Ljava/io/PrintStream;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    goto :goto_0
.end method

.method private throwIfAllowed(Ljava/lang/RuntimeException;)V
    .locals 3

    .prologue
    .line 272
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ERROR: suppressing format exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 278
    return-void
.end method


# virtual methods
.method checkEOF(II)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 262
    if-lt p1, p2, :cond_0

    .line 263
    new-instance v2, Lcom/flipboard/util/Format$Exception;

    const-string v3, "malformed format: %s at index %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/flipboard/util/Format;->fmt:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-direct {v2, v3, v4}, Lcom/flipboard/util/Format$Exception;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v2}, Lcom/flipboard/util/Format;->throwIfAllowed(Ljava/lang/RuntimeException;)V

    .line 266
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public print(Ljava/io/PrintStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/flipboard/util/Format;->formatIfNecessary()V

    .line 299
    iget-object v0, p0, Lcom/flipboard/util/Format;->buf:Lcom/flipboard/util/Format$CharBuf;

    if-nez v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/flipboard/util/Format;->fmt:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->append(Ljava/lang/CharSequence;)Ljava/io/PrintStream;

    .line 304
    :goto_0
    return-void

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/flipboard/util/Format;->buf:Lcom/flipboard/util/Format$CharBuf;

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->append(Ljava/lang/CharSequence;)Ljava/io/PrintStream;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 292
    invoke-direct {p0}, Lcom/flipboard/util/Format;->formatIfNecessary()V

    .line 293
    iget-object v0, p0, Lcom/flipboard/util/Format;->buf:Lcom/flipboard/util/Format$CharBuf;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/flipboard/util/Format;->fmt:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/flipboard/util/Format;->buf:Lcom/flipboard/util/Format$CharBuf;

    invoke-virtual {v0}, Lcom/flipboard/util/Format$CharBuf;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
