.class final Lcom/flipboard/util/Format$CharBuf;
.super Ljava/lang/Object;
.source "Format.java"

# interfaces
.implements Ljava/lang/CharSequence;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipboard/util/Format;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "CharBuf"
.end annotation


# instance fields
.field buf:[C

.field off:I

.field final synthetic this$0:Lcom/flipboard/util/Format;


# direct methods
.method constructor <init>(Lcom/flipboard/util/Format;I)V
    .locals 1

    .prologue
    .line 320
    iput-object p1, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 321
    new-array v0, p2, [C

    iput-object v0, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    .line 322
    return-void
.end method

.method private grow(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 622
    iget-object v0, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    mul-int/lit8 v1, p1, 0x2

    new-array v1, v1, [C

    iput-object v1, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    iget v2, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 623
    return-void
.end method

.method private reverse(I)V
    .locals 5

    .prologue
    .line 503
    iget v0, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    .line 504
    :goto_0
    sub-int v1, v0, p1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    .line 505
    iget-object v1, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    aget-char v2, v1, p1

    .line 506
    iget-object v3, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    add-int/lit8 v1, p1, 0x1

    iget-object v4, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    add-int/lit8 v0, v0, -0x1

    aget-char v4, v4, v0

    aput-char v4, v3, p1

    .line 507
    iget-object v3, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    aput-char v2, v3, v0

    move p1, v1

    .line 508
    goto :goto_0

    .line 509
    :cond_0
    return-void
.end method


# virtual methods
.method public charAt(I)C
    .locals 3

    .prologue
    .line 328
    iget v0, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    if-ge p1, v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    aget-char v0, v0, p1

    return v0

    .line 331
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >= limit="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 341
    iget v0, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    return v0
.end method

.method putChar(C)V
    .locals 3

    .prologue
    .line 346
    iget v0, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    iget-object v1, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 347
    iget v0, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/flipboard/util/Format$CharBuf;->grow(I)V

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    iget v1, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    aput-char p1, v0, v1

    .line 350
    return-void
.end method

.method putDumpStack(ZI)V
    .locals 3

    .prologue
    .line 548
    new-instance v0, Lcom/flipboard/util/Format$Exception;

    const-string v1, "dump stack"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {v0, v1, v2}, Lcom/flipboard/util/Format$Exception;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0, v0, p1, p2}, Lcom/flipboard/util/Format$CharBuf;->putException(Ljava/lang/Throwable;ZI)Z

    .line 549
    return-void
.end method

.method putException(Ljava/lang/Throwable;ZI)Z
    .locals 1

    .prologue
    .line 553
    if-nez p1, :cond_0

    .line 554
    const/4 v0, 0x0

    .line 560
    :goto_0
    return v0

    .line 556
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/flipboard/util/Format$CharBuf;->putException(Ljava/lang/Throwable;ZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    if-eqz p2, :cond_2

    const-string v0, "\n"

    :goto_1
    invoke-virtual {p0, v0}, Lcom/flipboard/util/Format$CharBuf;->putString(Ljava/lang/String;)V

    .line 559
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/flipboard/util/Format$CharBuf;->putExceptionTrace(Ljava/lang/Throwable;ZI)V

    .line 560
    const/4 v0, 0x1

    goto :goto_0

    .line 557
    :cond_2
    const-string v0, ", "

    goto :goto_1
.end method

.method putExceptionTrace(Ljava/lang/Throwable;ZI)V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 565
    iget-object v0, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget-boolean v7, v0, Lcom/flipboard/util/Format;->left:Z

    .line 568
    invoke-virtual {p1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v8

    .line 569
    if-gez p3, :cond_1

    .line 570
    neg-int p3, p3

    .line 576
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/flipboard/util/Format$CharBuf;->putString(Ljava/lang/String;)V

    move v0, v1

    move v2, v3

    move v4, v3

    move v5, v1

    .line 581
    :goto_1
    array-length v6, v8

    if-ge v0, v6, :cond_9

    if-lez p3, :cond_9

    .line 582
    aget-object v9, v8, v0

    .line 583
    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v6

    .line 585
    if-eqz v4, :cond_3

    .line 586
    const-string v10, "flipboard.util.Format"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 581
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 571
    :cond_1
    if-nez p3, :cond_0

    .line 572
    const/16 p3, 0x14

    goto :goto_0

    :cond_2
    move v4, v1

    .line 594
    :cond_3
    if-nez v7, :cond_4

    const-string v2, "flipboard."

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 595
    :cond_4
    if-nez p2, :cond_6

    move-object v2, v6

    .line 600
    const-string v5, "%s:%s:%d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v1

    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v3

    const/4 v2, 0x2

    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v2

    invoke-static {v5, v6}, Lcom/flipboard/util/Format;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 605
    :goto_3
    add-int/lit8 p3, p3, -0x1

    move-object v6, v2

    move v5, v1

    .line 612
    :goto_4
    if-eqz p2, :cond_8

    const-string v2, "\n    "

    :goto_5
    invoke-virtual {p0, v2}, Lcom/flipboard/util/Format$CharBuf;->putString(Ljava/lang/String;)V

    .line 613
    invoke-virtual {p0, v6}, Lcom/flipboard/util/Format$CharBuf;->putString(Ljava/lang/String;)V

    :cond_5
    move v2, v1

    goto :goto_2

    .line 602
    :cond_6
    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 606
    :cond_7
    if-nez v5, :cond_5

    .line 607
    const-string v2, "..."

    move-object v6, v2

    move v5, v3

    .line 608
    goto :goto_4

    .line 612
    :cond_8
    const-string v2, ", "

    goto :goto_5

    .line 615
    :cond_9
    if-eqz v2, :cond_a

    .line 616
    const-string v0, "<no stack trace>"

    invoke-virtual {p0, v0}, Lcom/flipboard/util/Format$CharBuf;->putString(Ljava/lang/String;)V

    .line 618
    :cond_a
    return-void
.end method

.method putFloat(F)V
    .locals 4

    .prologue
    .line 516
    invoke-static {p1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    .line 517
    iget-object v1, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget v1, v1, Lcom/flipboard/util/Format;->precision:I

    if-ltz v1, :cond_0

    .line 518
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 519
    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 520
    sub-int/2addr v1, v2

    iget-object v3, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget v3, v3, Lcom/flipboard/util/Format;->precision:I

    if-le v1, v3, :cond_0

    .line 521
    const/4 v1, 0x0

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget v3, v3, Lcom/flipboard/util/Format;->precision:I

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 524
    :cond_0
    invoke-virtual {p0, v0}, Lcom/flipboard/util/Format$CharBuf;->putString(Ljava/lang/String;)V

    .line 525
    return-void
.end method

.method putFormat(CLjava/lang/Object;)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 353
    iget v5, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    .line 355
    sparse-switch p1, :sswitch_data_0

    .line 418
    :try_start_0
    iget-object v2, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": not supported. SORRY!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    # invokes: Lcom/flipboard/util/Format;->throwIfAllowed(Ljava/lang/RuntimeException;)V
    invoke-static {v2, v3}, Lcom/flipboard/util/Format;->access$200(Lcom/flipboard/util/Format;Ljava/lang/RuntimeException;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 428
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget v2, v2, Lcom/flipboard/util/Format;->width:I

    if-lez v2, :cond_f

    .line 429
    iget v2, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    sub-int v3, v2, v5

    .line 430
    iget-object v2, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget v2, v2, Lcom/flipboard/util/Format;->width:I

    sub-int v4, v2, v3

    .line 431
    if-lez v4, :cond_f

    .line 432
    iget-object v2, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget-boolean v2, v2, Lcom/flipboard/util/Format;->zeroPad:Z

    if-eqz v2, :cond_d

    const/16 v2, 0x30

    .line 433
    :goto_1
    iget-object v6, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget-boolean v6, v6, Lcom/flipboard/util/Format;->left:Z

    if-nez v6, :cond_e

    .line 435
    iget v6, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    add-int/2addr v6, v4

    iget-object v7, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    array-length v7, v7

    if-lt v6, v7, :cond_1

    .line 436
    iget v6, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    add-int/2addr v6, v4

    invoke-direct {p0, v6}, Lcom/flipboard/util/Format$CharBuf;->grow(I)V

    .line 438
    :cond_1
    iget-object v6, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    iget-object v7, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    add-int v8, v5, v4

    invoke-static {v6, v5, v7, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 439
    iget v3, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    add-int/2addr v3, v4

    iput v3, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    move v3, v4

    move v4, v5

    .line 440
    :goto_2
    add-int/lit8 v3, v3, -0x1

    if-ltz v3, :cond_f

    .line 441
    iget-object v6, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    add-int/lit8 v5, v4, 0x1

    aput-char v2, v6, v4

    move v4, v5

    goto :goto_2

    .line 357
    :sswitch_0
    :try_start_1
    instance-of v2, p2, Ljava/lang/Character;

    if-eqz v2, :cond_2

    .line 358
    move-object v0, p2

    check-cast v0, Ljava/lang/Character;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    invoke-virtual {p0, v2}, Lcom/flipboard/util/Format$CharBuf;->putChar(C)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 420
    :catch_0
    move-exception v2

    .line 421
    :goto_3
    iget-object v2, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    new-instance v3, Lcom/flipboard/util/Format$Exception;

    const-string v7, "%s not compatible with %%%s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    aput-object v6, v8, v4

    invoke-direct {v3, v7, v8}, Lcom/flipboard/util/Format$Exception;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    # invokes: Lcom/flipboard/util/Format;->throwIfAllowed(Ljava/lang/RuntimeException;)V
    invoke-static {v2, v3}, Lcom/flipboard/util/Format;->access$200(Lcom/flipboard/util/Format;Ljava/lang/RuntimeException;)V

    .line 422
    const-string v2, "???"

    invoke-virtual {p0, v2}, Lcom/flipboard/util/Format$CharBuf;->putString(Ljava/lang/String;)V

    goto :goto_0

    .line 360
    :cond_2
    :try_start_2
    move-object v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    const v3, 0xffff

    and-int/2addr v2, v3

    .line 361
    int-to-char v2, v2

    invoke-virtual {p0, v2}, Lcom/flipboard/util/Format$CharBuf;->putChar(C)V

    goto/16 :goto_0

    .line 368
    :sswitch_1
    instance-of v2, p2, Ljava/lang/Long;

    if-nez v2, :cond_3

    instance-of v2, p2, Ljava/lang/Double;

    if-eqz v2, :cond_4

    .line 369
    :cond_3
    move-object v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    .line 377
    :goto_4
    invoke-virtual {p0, v2, v3}, Lcom/flipboard/util/Format$CharBuf;->putInteger(J)V

    goto/16 :goto_0

    .line 370
    :cond_4
    instance-of v2, p2, Ljava/lang/Integer;

    if-nez v2, :cond_5

    instance-of v2, p2, Ljava/lang/Float;

    if-eqz v2, :cond_6

    .line 371
    :cond_5
    move-object v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    int-to-long v2, v2

    goto :goto_4

    .line 372
    :cond_6
    instance-of v2, p2, Ljava/lang/Short;

    if-eqz v2, :cond_7

    .line 373
    move-object v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->shortValue()S

    move-result v2

    int-to-long v2, v2

    goto :goto_4

    .line 375
    :cond_7
    move-object v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->byteValue()B

    move-result v2

    int-to-long v2, v2

    goto :goto_4

    .line 383
    :sswitch_2
    move-object v0, p2

    check-cast v0, Ljava/lang/Throwable;

    move-object v2, v0

    const/16 v3, 0x45

    if-ne p1, v3, :cond_8

    move v3, v4

    :goto_5
    iget-object v7, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget v7, v7, Lcom/flipboard/util/Format;->width:I

    invoke-virtual {p0, v2, v3, v7}, Lcom/flipboard/util/Format$CharBuf;->putException(Ljava/lang/Throwable;ZI)Z

    move-result v2

    if-nez v2, :cond_0

    .line 384
    const-string v2, "null"

    invoke-virtual {p0, v2}, Lcom/flipboard/util/Format$CharBuf;->putString(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    move v3, v6

    .line 383
    goto :goto_5

    .line 389
    :sswitch_3
    move-object v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/flipboard/util/Format$CharBuf;->putFloat(F)V

    goto/16 :goto_0

    .line 393
    :sswitch_4
    if-nez p2, :cond_11

    .line 394
    const-string v3, "null"
    :try_end_2
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_0

    .line 396
    :goto_6
    :try_start_3
    instance-of v2, v3, Lcom/flipboard/util/Format;

    if-eqz v2, :cond_9

    .line 397
    move-object v0, v3

    check-cast v0, Lcom/flipboard/util/Format;

    move-object v2, v0

    .line 398
    iget-object v7, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget-object v8, v2, Lcom/flipboard/util/Format;->fmt:Ljava/lang/String;

    iget-object v2, v2, Lcom/flipboard/util/Format;->args:[Ljava/lang/Object;

    # invokes: Lcom/flipboard/util/Format;->output(Ljava/lang/String;[Ljava/lang/Object;)V
    invoke-static {v7, v8, v2}, Lcom/flipboard/util/Format;->access$100(Lcom/flipboard/util/Format;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 420
    :catch_1
    move-exception v2

    move-object p2, v3

    goto/16 :goto_3

    .line 400
    :cond_9
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/flipboard/util/Format$CharBuf;->putString(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 405
    :sswitch_5
    :try_start_4
    move-object v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v2, v0

    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    .line 406
    instance-of v7, p2, Ljava/lang/Integer;

    if-eqz v7, :cond_a

    .line 407
    const-wide v8, 0xffffffffL

    and-long/2addr v2, v8

    move-wide v8, v2

    .line 413
    :goto_7
    const/16 v2, 0x58

    if-ne p1, v2, :cond_c

    move v2, v4

    :goto_8
    invoke-virtual {p0, v8, v9, v2}, Lcom/flipboard/util/Format$CharBuf;->putHexInteger(JZ)V

    goto/16 :goto_0

    .line 408
    :cond_a
    instance-of v7, p2, Ljava/lang/Short;

    if-eqz v7, :cond_b

    .line 409
    const-wide/32 v8, 0xffff

    and-long/2addr v2, v8

    move-wide v8, v2

    goto :goto_7

    .line 410
    :cond_b
    instance-of v7, p2, Ljava/lang/Byte;
    :try_end_4
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_4} :catch_0

    if-eqz v7, :cond_10

    .line 411
    const-wide/16 v8, 0xff

    and-long/2addr v2, v8

    move-wide v8, v2

    goto :goto_7

    :cond_c
    move v2, v6

    .line 413
    goto :goto_8

    .line 432
    :cond_d
    const/16 v2, 0x20

    goto/16 :goto_1

    .line 444
    :cond_e
    :goto_9
    iget-object v4, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget v4, v4, Lcom/flipboard/util/Format;->width:I

    if-ge v3, v4, :cond_f

    .line 445
    invoke-virtual {p0, v2}, Lcom/flipboard/util/Format$CharBuf;->putChar(C)V

    .line 446
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    .line 451
    :cond_f
    return-void

    :cond_10
    move-wide v8, v2

    goto :goto_7

    :cond_11
    move-object v3, p2

    goto :goto_6

    .line 355
    nop

    :sswitch_data_0
    .sparse-switch
        0x45 -> :sswitch_2
        0x58 -> :sswitch_5
        0x63 -> :sswitch_0
        0x64 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_3
        0x73 -> :sswitch_4
        0x75 -> :sswitch_1
        0x78 -> :sswitch_5
    .end sparse-switch
.end method

.method putHexInteger(JZ)V
    .locals 5

    .prologue
    .line 484
    if-eqz p3, :cond_1

    # getter for: Lcom/flipboard/util/Format;->HEX_UPPER:[C
    invoke-static {}, Lcom/flipboard/util/Format;->access$300()[C

    move-result-object v0

    .line 487
    :goto_0
    iget v1, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    .line 490
    :cond_0
    const-wide/16 v2, 0xf

    and-long/2addr v2, p1

    long-to-int v2, v2

    .line 491
    aget-char v2, v0, v2

    invoke-virtual {p0, v2}, Lcom/flipboard/util/Format$CharBuf;->putChar(C)V

    .line 492
    const/4 v2, 0x4

    ushr-long/2addr p1, v2

    .line 493
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    .line 495
    invoke-direct {p0, v1}, Lcom/flipboard/util/Format$CharBuf;->reverse(I)V

    .line 496
    return-void

    .line 484
    :cond_1
    # getter for: Lcom/flipboard/util/Format;->HEX_LOWER:[C
    invoke-static {}, Lcom/flipboard/util/Format;->access$400()[C

    move-result-object v0

    goto :goto_0
.end method

.method putInteger(J)V
    .locals 13

    .prologue
    const-wide/16 v10, 0xa

    const-wide/16 v8, 0x0

    const/4 v1, 0x0

    .line 458
    cmp-long v0, p1, v8

    if-gez v0, :cond_0

    .line 459
    const/16 v0, 0x2d

    invoke-virtual {p0, v0}, Lcom/flipboard/util/Format$CharBuf;->putChar(C)V

    .line 460
    neg-long p1, p1

    .line 464
    :cond_0
    iget v2, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    move v0, v1

    .line 467
    :cond_1
    iget-object v3, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget-boolean v3, v3, Lcom/flipboard/util/Format;->comma:Z

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    .line 468
    const/16 v0, 0x2c

    invoke-virtual {p0, v0}, Lcom/flipboard/util/Format$CharBuf;->putChar(C)V

    move v0, v1

    .line 471
    :cond_2
    const-wide/16 v4, 0x30

    rem-long v6, p1, v10

    add-long/2addr v4, v6

    long-to-int v3, v4

    int-to-char v3, v3

    invoke-virtual {p0, v3}, Lcom/flipboard/util/Format$CharBuf;->putChar(C)V

    .line 472
    div-long/2addr p1, v10

    .line 473
    add-int/lit8 v0, v0, 0x1

    .line 474
    cmp-long v3, p1, v8

    if-nez v3, :cond_1

    .line 476
    invoke-direct {p0, v2}, Lcom/flipboard/util/Format$CharBuf;->reverse(I)V

    .line 477
    return-void
.end method

.method putString(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 532
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 533
    iget-object v0, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget-boolean v0, v0, Lcom/flipboard/util/Format;->zeroPad:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x30

    .line 534
    :goto_0
    iget-object v1, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget-boolean v1, v1, Lcom/flipboard/util/Format;->left:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget v1, v1, Lcom/flipboard/util/Format;->width:I

    if-lez v1, :cond_1

    .line 535
    iget-object v1, p0, Lcom/flipboard/util/Format$CharBuf;->this$0:Lcom/flipboard/util/Format;

    iget v1, v1, Lcom/flipboard/util/Format;->width:I

    sub-int/2addr v1, v2

    :goto_1
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_1

    .line 536
    invoke-virtual {p0, v0}, Lcom/flipboard/util/Format$CharBuf;->putChar(C)V

    goto :goto_1

    .line 533
    :cond_0
    const/16 v0, 0x20

    goto :goto_0

    .line 539
    :cond_1
    iget v0, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    add-int/2addr v0, v2

    iget-object v1, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 540
    iget v0, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    add-int/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/flipboard/util/Format$CharBuf;->grow(I)V

    .line 542
    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    iget v3, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    invoke-virtual {p1, v0, v2, v1, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 543
    iget v0, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    .line 544
    return-void
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 336
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 627
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/flipboard/util/Format$CharBuf;->buf:[C

    const/4 v2, 0x0

    iget v3, p0, Lcom/flipboard/util/Format$CharBuf;->off:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
