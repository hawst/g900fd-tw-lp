.class public Lcom/flipboard/util/FlipboardBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "FlipboardBroadcastReceiver.java"


# static fields
.field public static API_ACCESS_TOKEN:Ljava/lang/String; = null

.field public static API_LOGOUT:Z = false

.field public static API_REFRESH_TOKEN:Ljava/lang/String; = null

.field public static API_SERVICE:Ljava/lang/String; = null

.field private static final FLIPBOARD_SERVICE:Ljava/lang/String; = "flipboard"

.field private static final KEY_BROADCAST_API_ACCESS_TOKEN:Ljava/lang/String; = "sstream.api.access.token"

.field private static final KEY_BROADCAST_API_ACTION_ID:Ljava/lang/String; = "sstream.api.action.id"

.field private static final KEY_BROADCAST_API_REFRESH_TOKEN:Ljava/lang/String; = "sstream.api.refresh.token"

.field private static final KEY_BROADCAST_SERVICE_NAME:Ljava/lang/String; = "service_name"

.field private static final VALUE_BROADCAST_API_ACTION_LOGIN:I = 0x1

.field private static final VALUE_BROADCAST_API_ACTION_LOGOUT:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static clearService()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    sput-object v0, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_SERVICE:Ljava/lang/String;

    .line 67
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x6

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 33
    const-string v2, "sstream.api.access.token"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 34
    const-string v3, "sstream.api.refresh.token"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 36
    if-eqz v2, :cond_0

    if-nez v3, :cond_2

    .line 38
    :cond_0
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "FDL: Receive broadcast and drop it. Either apiToken or refresh token is null"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    :cond_1
    :goto_0
    return-void

    .line 42
    :cond_2
    const-string v4, "service_name"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_SERVICE:Ljava/lang/String;

    .line 43
    const-string v4, "sstream.api.action.id"

    invoke-virtual {p2, v4, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    if-nez v4, :cond_3

    :goto_1
    sput-boolean v0, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_LOGOUT:Z

    .line 45
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FDL: Receive broadcast [API_SERVICE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_SERVICE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "],  [API_LOGOUT: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_LOGOUT:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v4, v5}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FDL: Receive broadcast api token is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v6, v2}, Lcom/flipboard/util/Log;->maskString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v4, v5}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FDL: Receive broadcast api refreshToken is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v6, v3}, Lcom/flipboard/util/Log;->maskString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v4, v5}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    sget-boolean v0, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_LOGOUT:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_SERVICE:Ljava/lang/String;

    const-string v4, "flipboard"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 49
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "FDL: Received logout broadcast from the app, clear token"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    sput-object v7, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_ACCESS_TOKEN:Ljava/lang/String;

    .line 51
    sput-object v7, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_REFRESH_TOKEN:Ljava/lang/String;

    .line 58
    :goto_2
    invoke-static {}, Lcom/flipboard/data/Request;->getInstance()Lcom/flipboard/data/Request;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 59
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v2, "FDL: Update the Request instance with api tokens from the broadcast"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    invoke-static {}, Lcom/flipboard/data/Request;->getInstance()Lcom/flipboard/data/Request;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flipboard/data/Request;->updateTokensFromBroadcast()V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 43
    goto/16 :goto_1

    .line 53
    :cond_4
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v4, "FDL: Received api access token, update tokens in memory"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v4, v5}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    sput-object v2, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_ACCESS_TOKEN:Ljava/lang/String;

    .line 55
    sput-object v3, Lcom/flipboard/util/FlipboardBroadcastReceiver;->API_REFRESH_TOKEN:Ljava/lang/String;

    goto :goto_2
.end method
