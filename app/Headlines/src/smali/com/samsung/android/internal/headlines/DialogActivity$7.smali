.class Lcom/samsung/android/internal/headlines/DialogActivity$7;
.super Ljava/lang/Object;
.source "DialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/DialogActivity;->dialogOpenAppInfo(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

.field final synthetic val$pkgName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/DialogActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/DialogActivity$7;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/DialogActivity$7;->val$pkgName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 258
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 259
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "package"

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/DialogActivity$7;->val$pkgName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 260
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/DialogActivity$7;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    invoke-virtual {v1, v0}, Lcom/samsung/android/internal/headlines/DialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 262
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 263
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/DialogActivity$7;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    invoke-virtual {v1}, Lcom/samsung/android/internal/headlines/DialogActivity;->finish()V

    .line 264
    return-void
.end method
