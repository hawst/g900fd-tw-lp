.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$8;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->serviceLogOut(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

.field final synthetic val$observer:Ljava/util/Observer;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/Observer;)V
    .locals 0

    .prologue
    .line 419
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$8;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$8;->val$observer:Ljava/util/Observer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 423
    if-eqz p2, :cond_0

    instance-of v0, p2, Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 430
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p2

    .line 425
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 426
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$8;->val$observer:Ljava/util/Observer;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->instance:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$200()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    goto :goto_0

    .line 429
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$8;->val$observer:Ljava/util/Observer;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->instance:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$200()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    goto :goto_0
.end method
