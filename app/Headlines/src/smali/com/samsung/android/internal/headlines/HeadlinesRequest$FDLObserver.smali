.class Lcom/samsung/android/internal/headlines/HeadlinesRequest$FDLObserver;
.super Ljava/lang/Object;
.source "HeadlinesRequest.java"

# interfaces
.implements Lcom/flipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/internal/headlines/HeadlinesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FDLObserver"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/flipboard/util/Observer",
        "<",
        "Lcom/flipboard/data/Request;",
        "Lcom/flipboard/data/Request$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field mOb:Ljava/util/Observer;

.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/util/Observer;)V
    .locals 0
    .param p2, "observer"    # Ljava/util/Observer;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$FDLObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$FDLObserver;->mOb:Ljava/util/Observer;

    .line 73
    return-void
.end method


# virtual methods
.method public notify(Lcom/flipboard/data/Request;Lcom/flipboard/data/Request$Message;Ljava/lang/Object;)V
    .locals 3
    .param p1, "observable"    # Lcom/flipboard/data/Request;
    .param p2, "msg"    # Lcom/flipboard/data/Request$Message;
    .param p3, "arg"    # Ljava/lang/Object;

    .prologue
    .line 77
    sget-object v0, Lcom/flipboard/data/Request$Message;->READY:Lcom/flipboard/data/Request$Message;

    if-ne p2, v0, :cond_0

    .line 78
    const-string v0, "HeadlinesRequest"

    const-string v1, "FDL request observer Message.READY"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$FDLObserver;->mOb:Ljava/util/Observer;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$000()Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$FDLObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    sget-object v1, Lcom/flipboard/data/Request$Message;->READY:Lcom/flipboard/data/Request$Message;

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mMsg:Lcom/flipboard/data/Request$Message;
    invoke-static {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$102(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Lcom/flipboard/data/Request$Message;)Lcom/flipboard/data/Request$Message;

    .line 98
    :goto_0
    return-void

    .line 82
    :cond_0
    sget-object v0, Lcom/flipboard/data/Request$Message;->ERROR:Lcom/flipboard/data/Request$Message;

    if-ne p2, v0, :cond_1

    .line 83
    const-string v0, "HeadlinesRequest"

    const-string v1, "FDL request observer Message.ERROR"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$FDLObserver;->mOb:Ljava/util/Observer;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$000()Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 85
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$FDLObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    sget-object v1, Lcom/flipboard/data/Request$Message;->ERROR:Lcom/flipboard/data/Request$Message;

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mMsg:Lcom/flipboard/data/Request$Message;
    invoke-static {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$102(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Lcom/flipboard/data/Request$Message;)Lcom/flipboard/data/Request$Message;

    goto :goto_0

    .line 87
    :cond_1
    sget-object v0, Lcom/flipboard/data/Request$Message;->ACCOUNT_SYNC:Lcom/flipboard/data/Request$Message;

    if-ne p2, v0, :cond_2

    .line 88
    const-string v0, "HeadlinesRequest"

    const-string v1, "FDL request observer Message.ACCOUNT_SYNC"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 90
    :cond_2
    sget-object v0, Lcom/flipboard/data/Request$Message;->ACCOUNT_LOGOUT:Lcom/flipboard/data/Request$Message;

    if-ne p2, v0, :cond_3

    .line 91
    const-string v0, "HeadlinesRequest"

    const-string v1, "FDL request observer Message.ACCOUNT_LOGOUT"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 93
    :cond_3
    sget-object v0, Lcom/flipboard/data/Request$Message;->REFRESHING:Lcom/flipboard/data/Request$Message;

    if-ne p2, v0, :cond_4

    .line 94
    const-string v0, "HeadlinesRequest"

    const-string v1, "FDL request observer Message.REFRESHING"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 97
    :cond_4
    const-string v0, "HeadlinesRequest"

    const-string v1, "FDL request observer Message. UNKNOWN type"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bridge synthetic notify(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # Ljava/lang/Object;

    .prologue
    .line 68
    check-cast p1, Lcom/flipboard/data/Request;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/flipboard/data/Request$Message;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$FDLObserver;->notify(Lcom/flipboard/data/Request;Lcom/flipboard/data/Request$Message;Ljava/lang/Object;)V

    return-void
.end method
