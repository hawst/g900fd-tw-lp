.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->update(Ljava/util/Observable;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 288
    if-eqz p2, :cond_0

    instance-of v0, p2, Ljava/util/List;

    if-nez v0, :cond_3

    .line 289
    :cond_0
    const-string v0, "Headlines"

    const-string v1, "queryCategory. return null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    const-string v0, "CN"

    invoke-static {v0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 291
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$500(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getChinaDefaultCategory()Ljava/util/List;

    move-result-object p2

    .line 292
    .local p2, "arg1":Ljava/util/List;
    if-nez p2, :cond_1

    .line 293
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->val$observer:Ljava/util/Observer;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 320
    .end local p2    # "arg1":Ljava/util/List;
    :cond_1
    :goto_0
    return-void

    .line 297
    .local p2, "arg1":Ljava/lang/Object;
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->val$observer:Ljava/util/Observer;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move-object v0, p2

    .line 301
    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 302
    const-string v0, "Headlines"

    const-string v1, "queryCategory. return size is 0."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    const-string v0, "CN"

    invoke-static {v0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v3, :cond_4

    .line 304
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$500(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getChinaDefaultCategory()Ljava/util/List;

    move-result-object p2

    .line 305
    .local p2, "arg1":Ljava/util/List;
    if-nez p2, :cond_1

    .line 306
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->val$observer:Ljava/util/Observer;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    goto :goto_0

    .line 310
    .local p2, "arg1":Ljava/lang/Object;
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->val$observer:Ljava/util/Observer;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    goto :goto_0

    .line 314
    :cond_5
    const-string v0, "Headlines"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queryCategory. return : ."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    iget-object v2, v2, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mNewsList:Ljava/util/List;
    invoke-static {v2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$600(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    check-cast p2, Ljava/util/List;

    .end local p2    # "arg1":Ljava/lang/Object;
    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mNewsList:Ljava/util/List;
    invoke-static {v0, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$602(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/List;)Ljava/util/List;

    .line 316
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mHeadlinesCardManager:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$400(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    iget-object v1, v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mNewsList:Ljava/util/List;
    invoke-static {v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$600(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->setNewsList(Ljava/util/List;)V

    .line 317
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->val$observer:Ljava/util/Observer;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
