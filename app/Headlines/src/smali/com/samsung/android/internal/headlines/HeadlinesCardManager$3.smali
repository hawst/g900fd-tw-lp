.class Lcom/samsung/android/internal/headlines/HeadlinesCardManager$3;
.super Ljava/lang/Object;
.source "HeadlinesCardManager.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestServiceLogout(Ljava/lang/String;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

.field final synthetic val$cardType:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 708
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$3;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$3;->val$cardType:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 712
    if-eqz p2, :cond_0

    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 713
    const-string v0, "HeadlinesCardManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestServiceLogout : social id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$3;->val$cardType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is log out!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$3;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$500(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$3;->val$cardType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->removeCategory(Ljava/lang/String;)V

    .line 716
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$3;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$3;->val$cardType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->postSocialWelcomeCard(Ljava/lang/String;)V

    .line 720
    :cond_0
    return-void
.end method
