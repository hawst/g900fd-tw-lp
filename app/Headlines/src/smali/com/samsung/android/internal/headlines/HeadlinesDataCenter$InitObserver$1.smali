.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver$1;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->update(Ljava/util/Observable;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;)V
    .locals 0

    .prologue
    .line 637
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 640
    if-eqz p2, :cond_2

    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 641
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "arg1":Ljava/lang/Object;
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 642
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HeadlinesDataCenter was succeeded. "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->DONE:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    invoke-static {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$902(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    .line 644
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$100(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$802(Ljava/lang/String;)Ljava/lang/String;

    .line 645
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mHeadlinesCardManager:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$400(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->registerDefaultCardName()V

    .line 646
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Pending intent count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mPendingList:Ljava/util/List;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$1000()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    :goto_0
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mPendingList:Ljava/util/List;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$1000()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 649
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Pending intent is activated. : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mPendingList:Ljava/util/List;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$1000()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesService;->instance:Lcom/samsung/android/internal/headlines/HeadlinesService;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mPendingList:Ljava/util/List;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$1000()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    const/16 v2, 0x12d

    invoke-virtual {v1, v0, v2, v3}, Lcom/samsung/android/internal/headlines/HeadlinesService;->onStartCommand(Landroid/content/Intent;II)I

    .line 651
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mPendingList:Ljava/util/List;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$1000()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 653
    :cond_0
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mPendingList:Ljava/util/List;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$1000()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 662
    :goto_1
    return-void

    .line 655
    :cond_1
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HeadlinesDataCenter was failed. "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    invoke-static {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$902(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    goto :goto_1

    .line 659
    .restart local p2    # "arg1":Ljava/lang/Object;
    :cond_2
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HeadlinesDataCenter was failed. "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 660
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    invoke-static {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$902(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    goto :goto_1
.end method
