.class Lcom/samsung/android/internal/headlines/HeadlinesRequest$11;
.super Lcom/flipboard/data/Request$FLSuccessListener;
.source "HeadlinesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getService(Ljava/util/Observer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

.field final synthetic val$observer:Ljava/util/Observer;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/util/Observer;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$11;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$11;->val$observer:Ljava/util/Observer;

    invoke-direct {p0}, Lcom/flipboard/data/Request$FLSuccessListener;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 364
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$11;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 8
    .param p1, "response"    # Lorg/json/JSONObject;

    .prologue
    .line 369
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$11;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v5, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mServiceList:Ljava/util/List;

    .line 370
    const-string v5, "services"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->services:Lorg/json/JSONArray;
    invoke-static {v5}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$402(Lorg/json/JSONArray;)Lorg/json/JSONArray;

    .line 371
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->services:Lorg/json/JSONArray;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$400()Lorg/json/JSONArray;

    move-result-object v5

    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 372
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 373
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->services:Lorg/json/JSONArray;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$400()Lorg/json/JSONArray;

    move-result-object v5

    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 374
    .local v4, "obj":Lorg/json/JSONObject;
    new-instance v3, Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    invoke-direct {v3}, Lcom/samsung/android/internal/headlines/HeadlinesCategory;-><init>()V

    .line 375
    .local v3, "item":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    const-string v5, "id"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    .line 376
    const-string v5, "name"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->name:Ljava/lang/String;

    .line 377
    const-string v5, "active"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, v3, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->active:Z

    .line 378
    iget-object v5, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$11;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    iget-object v5, v5, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mServiceList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 379
    const-string v5, "HeadlinesRequest"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Service was added. id : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " name : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 381
    .end local v3    # "item":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .end local v4    # "obj":Lorg/json/JSONObject;
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$11;->val$observer:Ljava/util/Observer;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$000()Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$11;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    iget-object v7, v7, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mServiceList:Ljava/util/List;

    invoke-interface {v5, v6, v7}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 387
    .end local v0    # "count":I
    .end local v2    # "i":I
    :goto_1
    return-void

    .line 382
    :catch_0
    move-exception v1

    .line 383
    .local v1, "e":Lorg/json/JSONException;
    const-string v5, "HeadlinesRequest"

    const-string v6, "There is no Services tag in the result of mRequest.getServices"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
