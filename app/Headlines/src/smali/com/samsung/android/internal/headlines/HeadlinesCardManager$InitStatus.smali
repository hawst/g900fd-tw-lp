.class final enum Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;
.super Ljava/lang/Enum;
.source "HeadlinesCardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/internal/headlines/HeadlinesCardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "InitStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

.field public static final enum DOING:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

.field public static final enum DONE:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

.field public static final enum INIT:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    const-string v1, "DOING"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;->DOING:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;->DONE:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    .line 54
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;->DOING:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;->DONE:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;->$VALUES:[Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 54
    const-class v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;->$VALUES:[Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    invoke-virtual {v0}, [Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    return-object v0
.end method
