.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12$1;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;->update(Ljava/util/Observable;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;)V
    .locals 0

    .prologue
    .line 579
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    .line 582
    if-eqz p2, :cond_0

    instance-of v0, p2, Ljava/lang/Boolean;

    if-nez v0, :cond_2

    .line 583
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;->val$observer:Ljava/util/Observer;

    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 591
    .end local p2    # "arg1":Ljava/lang/Object;
    :cond_1
    :goto_0
    return-void

    .line 587
    .restart local p2    # "arg1":Ljava/lang/Object;
    :cond_2
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "arg1":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v3, :cond_1

    .line 588
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "logged out :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;

    iget-object v2, v2, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;->val$socialId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;->val$observer:Ljava/util/Observer;

    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    goto :goto_0
.end method
