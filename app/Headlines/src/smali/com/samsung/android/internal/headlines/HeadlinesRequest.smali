.class public Lcom/samsung/android/internal/headlines/HeadlinesRequest;
.super Ljava/util/Observable;
.source "HeadlinesRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/internal/headlines/HeadlinesRequest$FDLObserver;
    }
.end annotation


# static fields
.field private static final CONTENT_COUNT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "HeadlinesRequest"

.field private static final clientId:Ljava/lang/String; = "825929369"

.field private static instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

.field private static mContext:Landroid/content/Context;

.field private static mObserver:Lcom/flipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipboard/util/Observer",
            "<",
            "Lcom/flipboard/data/Request;",
            "Lcom/flipboard/data/Request$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static mRequest:Lcom/flipboard/data/Request;

.field private static mToast:Landroid/widget/Toast;

.field private static services:Lorg/json/JSONArray;


# instance fields
.field mCategoryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesCategory;",
            ">;"
        }
    .end annotation
.end field

.field mDefaultChinaCategoryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesCategory;",
            ">;"
        }
    .end annotation
.end field

.field private mMsg:Lcom/flipboard/data/Request$Message;

.field mServiceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesCategory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    .line 38
    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    .line 39
    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->services:Lorg/json/JSONArray;

    .line 41
    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mCategoryList:Ljava/util/List;

    .line 34
    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mServiceList:Ljava/util/List;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mDefaultChinaCategoryList:Ljava/util/List;

    .line 40
    sget-object v0, Lcom/flipboard/data/Request$Message;->ERROR:Lcom/flipboard/data/Request$Message;

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mMsg:Lcom/flipboard/data/Request$Message;

    .line 68
    return-void
.end method

.method static synthetic access$000()Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Lcom/flipboard/data/Request$Message;)Lcom/flipboard/data/Request$Message;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    .param p1, "x1"    # Lcom/flipboard/data/Request$Message;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mMsg:Lcom/flipboard/data/Request$Message;

    return-object p1
.end method

.method static synthetic access$200()Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$202(Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Landroid/widget/Toast;

    .prologue
    .line 29
    sput-object p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;

    return-object p0
.end method

.method static synthetic access$300()Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400()Lorg/json/JSONArray;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->services:Lorg/json/JSONArray;

    return-object v0
.end method

.method static synthetic access$402(Lorg/json/JSONArray;)Lorg/json/JSONArray;
    .locals 0
    .param p0, "x0"    # Lorg/json/JSONArray;

    .prologue
    .line 29
    sput-object p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->services:Lorg/json/JSONArray;

    return-object p0
.end method

.method static init(Landroid/content/Context;)Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    sput-object p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;

    .line 46
    const-string v0, "CN"

    invoke-static {v0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;

    const-string v1, "825929369"

    const-class v2, Lcom/samsung/android/internal/headlines/AuthActivity;

    const-class v3, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;

    invoke-static {v0, v1, v2, v3}, Lcom/flipboard/data/Request;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)Lcom/flipboard/data/Request;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    .line 51
    :goto_0
    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    invoke-direct {v0}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;-><init>()V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    .line 52
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    return-object v0

    .line 49
    :cond_0
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;

    const-string v1, "825929369"

    const-class v2, Lcom/samsung/android/internal/headlines/AuthActivity;

    const-class v3, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/flipboard/data/Request;->init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Z)Lcom/flipboard/data/Request;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    goto :goto_0
.end method


# virtual methods
.method public addObserver(Ljava/util/Observer;)V
    .locals 2
    .param p1, "observer"    # Ljava/util/Observer;

    .prologue
    .line 105
    const-string v0, "HeadlinesRequest"

    const-string v1, "FDL request add observer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    invoke-virtual {v0}, Lcom/flipboard/data/Request;->hasObservers()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mObserver:Lcom/flipboard/util/Observer;

    invoke-virtual {v0, v1}, Lcom/flipboard/data/Request;->removeObserver(Lcom/flipboard/util/Observer;)V

    .line 108
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mObserver:Lcom/flipboard/util/Observer;

    .line 110
    :cond_0
    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$FDLObserver;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$FDLObserver;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/util/Observer;)V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mObserver:Lcom/flipboard/util/Observer;

    .line 111
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mObserver:Lcom/flipboard/util/Observer;

    invoke-virtual {v0, v1}, Lcom/flipboard/data/Request;->addObserver(Lcom/flipboard/util/Observer;)V

    .line 112
    invoke-super {p0, p1}, Ljava/util/Observable;->addObserver(Ljava/util/Observer;)V

    .line 113
    return-void
.end method

.method checkSupportedCategory(Lcom/samsung/android/internal/headlines/HeadlinesCategory;)Z
    .locals 3
    .param p1, "category"    # Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    .prologue
    const/4 v0, 0x1

    .line 448
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "news"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 485
    :cond_0
    :goto_0
    return v0

    .line 450
    :cond_1
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "tech"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 452
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "new"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 454
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "sports"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 456
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "business"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 458
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "photos"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 460
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "style"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 462
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "arts"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 464
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "living"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 466
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "music"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 468
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "travel"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 470
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "by"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 472
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "books"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 474
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "englishonly"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 476
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "food"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 478
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "netherlands"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 480
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "firstlaunch"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 482
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    const-string v2, "local"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 485
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public clearCategory()V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mCategoryList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mCategoryList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 410
    :cond_0
    return-void
.end method

.method public declared-synchronized deleteObserver(Ljava/util/Observer;)V
    .locals 2
    .param p1, "observer"    # Ljava/util/Observer;

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    const-string v0, "HeadlinesRequest"

    const-string v1, "FDL request deleteObserver observer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mObserver:Lcom/flipboard/util/Observer;

    if-eqz v0, :cond_0

    .line 119
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mObserver:Lcom/flipboard/util/Observer;

    invoke-virtual {v0, v1}, Lcom/flipboard/data/Request;->removeObserver(Lcom/flipboard/util/Observer;)V

    .line 120
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mObserver:Lcom/flipboard/util/Observer;

    .line 121
    invoke-super {p0, p1}, Ljava/util/Observable;->deleteObserver(Ljava/util/Observer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    monitor-exit p0

    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized deleteObservers()V
    .locals 2

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    const-string v0, "HeadlinesRequest"

    const-string v1, "FDL request deleteObservers observer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mObserver:Lcom/flipboard/util/Observer;

    if-eqz v0, :cond_0

    .line 128
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mObserver:Lcom/flipboard/util/Observer;

    invoke-virtual {v0, v1}, Lcom/flipboard/data/Request;->removeObserver(Lcom/flipboard/util/Observer;)V

    .line 129
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mObserver:Lcom/flipboard/util/Observer;

    .line 130
    invoke-super {p0}, Ljava/util/Observable;->deleteObservers()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    monitor-exit p0

    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getCategory()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mCategoryList:Ljava/util/List;

    return-object v0
.end method

.method public getChinaDefaultCategory()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 575
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mDefaultChinaCategoryList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 576
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->initChinaDefaultCategoryList()V

    .line 577
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mDefaultChinaCategoryList:Ljava/util/List;

    return-object v0
.end method

.method public getItems(Ljava/lang/String;Ljava/lang/String;ILjava/util/Observer;)V
    .locals 9
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "count"    # I
    .param p4, "observer"    # Ljava/util/Observer;

    .prologue
    .line 231
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    .line 232
    .local v4, "locale":Ljava/lang/String;
    const-string v0, "CN"

    invoke-static {v0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 233
    const-string v4, "zh_CN"

    .line 234
    :cond_0
    const-string v0, "HeadlinesRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HeadlinesRequest getItems. [id] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " [name] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " [locale] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 237
    .local v1, "categories":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    :try_start_0
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-instance v6, Lcom/samsung/android/internal/headlines/HeadlinesRequest$3;

    invoke-direct {v6, p0, p4}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$3;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/util/Observer;)V

    new-instance v7, Lcom/samsung/android/internal/headlines/HeadlinesRequest$4;

    invoke-direct {v7, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$4;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/lang/String;)V

    move v5, p3

    invoke-virtual/range {v0 .. v7}, Lcom/flipboard/data/Request;->getItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;ILcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    :try_end_0
    .catch Lcom/flipboard/data/FDLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    :goto_0
    return-void

    .line 252
    :catch_0
    move-exception v8

    .line 253
    .local v8, "e":Lcom/flipboard/data/FDLException;
    const-string v0, "HeadlinesRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HeadlinesRequest getItems occured exception : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Lcom/flipboard/data/FDLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", cause :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Lcom/flipboard/data/FDLException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", category : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-virtual {v8}, Lcom/flipboard/data/FDLException;->printStackTrace()V

    goto :goto_0
.end method

.method public getLocales(Ljava/lang/String;Ljava/util/Observer;)V
    .locals 3
    .param p1, "locale"    # Ljava/lang/String;
    .param p2, "observer"    # Ljava/util/Observer;

    .prologue
    .line 222
    if-eqz p2, :cond_1

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 223
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "_locale":Ljava/lang/String;
    const-string v1, "CN"

    invoke-static {v1}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 225
    const-string v0, "zh_CN"

    .line 226
    :cond_0
    invoke-interface {p2, p0, v0}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 228
    .end local v0    # "_locale":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public getService(Ljava/util/Observer;)V
    .locals 4
    .param p1, "observer"    # Ljava/util/Observer;

    .prologue
    .line 362
    const-string v1, "HeadlinesRequest"

    const-string v2, "HeadlinesRequest getService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    :try_start_0
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    new-instance v2, Lcom/samsung/android/internal/headlines/HeadlinesRequest$11;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$11;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/util/Observer;)V

    new-instance v3, Lcom/samsung/android/internal/headlines/HeadlinesRequest$12;

    invoke-direct {v3, p0}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$12;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;)V

    invoke-virtual {v1, v2, v3}, Lcom/flipboard/data/Request;->getServices(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    :try_end_0
    .catch Lcom/flipboard/data/FDLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 405
    :goto_0
    return-void

    .line 398
    :catch_0
    move-exception v0

    .line 399
    .local v0, "e":Lcom/flipboard/data/FDLException;
    const-string v1, "HeadlinesRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mRequest.getServices occured "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/flipboard/data/FDLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/flipboard/data/FDLException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    invoke-virtual {v0}, Lcom/flipboard/data/FDLException;->printStackTrace()V

    goto :goto_0
.end method

.method public getServiceForItem(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 6
    .param p1, "item"    # Lorg/json/JSONObject;

    .prologue
    .line 414
    :try_start_0
    const-string v3, "service"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 415
    .local v2, "serviceId":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getServiceObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 416
    .local v1, "service":Lorg/json/JSONObject;
    const-string v3, "HeadlinesRequest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[getServiceForItem] serviceId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " service = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 420
    .end local v1    # "service":Lorg/json/JSONObject;
    .end local v2    # "serviceId":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 418
    :catch_0
    move-exception v0

    .line 419
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 420
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getServiceItems(Ljava/lang/String;Ljava/lang/String;Ljava/util/Observer;)V
    .locals 9
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "observer"    # Ljava/util/Observer;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 260
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    if-eqz v0, :cond_0

    .line 262
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    invoke-interface {p3, v0, v1}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    .line 267
    .local v4, "locale":Ljava/lang/String;
    const-string v0, "CN"

    invoke-static {v0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 268
    const-string v4, "zh_CN"

    .line 269
    :cond_2
    const-string v0, "HeadlinesRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HeadlinesRequest getServiceItems. [id] "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " [name] "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " [locale] "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 271
    .local v2, "categories":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 273
    :try_start_0
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x1

    new-instance v6, Lcom/samsung/android/internal/headlines/HeadlinesRequest$5;

    invoke-direct {v6, p0, p1, p3}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$5;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/lang/String;Ljava/util/Observer;)V

    new-instance v7, Lcom/samsung/android/internal/headlines/HeadlinesRequest$6;

    invoke-direct {v7, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$6;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/lang/String;)V

    invoke-virtual/range {v0 .. v7}, Lcom/flipboard/data/Request;->getItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;ILcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    :try_end_0
    .catch Lcom/flipboard/data/FDLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 287
    :catch_0
    move-exception v8

    .line 288
    .local v8, "e":Lcom/flipboard/data/FDLException;
    const-string v0, "HeadlinesRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HeadlinesRequest getItems occured exception : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Lcom/flipboard/data/FDLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", cause :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Lcom/flipboard/data/FDLException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", category : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    invoke-virtual {v8}, Lcom/flipboard/data/FDLException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public getServiceObject(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 8
    .param p1, "serviceId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 425
    sget-object v5, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->services:Lorg/json/JSONArray;

    if-nez v5, :cond_1

    move-object v0, v4

    .line 444
    :cond_0
    :goto_0
    return-object v0

    .line 429
    :cond_1
    :try_start_0
    const-string v5, "HeadlinesRequest"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "services.length() = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->services:Lorg/json/JSONArray;

    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    sget-object v5, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->services:Lorg/json/JSONArray;

    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 432
    sget-object v5, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->services:Lorg/json/JSONArray;

    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 433
    .local v0, "aService":Lorg/json/JSONObject;
    const-string v5, "id"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 434
    .local v3, "id":Ljava/lang/String;
    const-string v5, "HeadlinesRequest"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "id = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const-string v5, "id"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-nez v5, :cond_0

    .line 431
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 440
    .end local v0    # "aService":Lorg/json/JSONObject;
    .end local v2    # "i":I
    .end local v3    # "id":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 441
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    move-object v0, v4

    .line 444
    goto :goto_0
.end method

.method public initChinaDefaultCategoryList()V
    .locals 21

    .prologue
    .line 489
    const-string v18, "CN"

    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 572
    :cond_0
    :goto_0
    return-void

    .line 492
    :cond_1
    const-string v18, "HeadlinesRequest"

    const-string v19, "initChinaDefaultCategoryList"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    const/4 v4, 0x0

    .line 494
    .local v4, "categoryJson":Lorg/json/JSONObject;
    const/4 v11, 0x0

    .line 495
    .local v11, "in":Ljava/io/InputStream;
    const/16 v16, 0x0

    .line 498
    .local v16, "reader":Ljava/io/BufferedReader;
    :try_start_0
    sget-object v18, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v18

    const-string v19, "defaultCategory_CHN.json"

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v11

    .line 499
    new-instance v17, Ljava/io/BufferedReader;

    new-instance v18, Ljava/io/InputStreamReader;

    move-object/from16 v0, v18

    invoke-direct {v0, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct/range {v17 .. v18}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_f
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 500
    .end local v16    # "reader":Ljava/io/BufferedReader;
    .local v17, "reader":Ljava/io/BufferedReader;
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 504
    .local v2, "builder":Ljava/lang/StringBuilder;
    :goto_1
    invoke-virtual/range {v17 .. v17}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v14

    .local v14, "line":Ljava/lang/String;
    if-eqz v14, :cond_4

    .line 505
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_e
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 509
    .end local v2    # "builder":Ljava/lang/StringBuilder;
    .end local v14    # "line":Ljava/lang/String;
    :catch_0
    move-exception v8

    move-object/from16 v16, v17

    .line 510
    .end local v17    # "reader":Ljava/io/BufferedReader;
    .local v8, "e":Ljava/io/IOException;
    .restart local v16    # "reader":Ljava/io/BufferedReader;
    :goto_2
    :try_start_2
    const-string v18, "HeadlinesRequest"

    const-string v19, "initDefaultCategoryList failed"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 517
    if-eqz v16, :cond_2

    .line 518
    :try_start_3
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 523
    :cond_2
    :goto_3
    if-eqz v11, :cond_3

    .line 524
    :try_start_4
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 530
    .end local v8    # "e":Ljava/io/IOException;
    :cond_3
    :goto_4
    if-eqz v4, :cond_0

    .line 533
    :try_start_5
    const-string v18, "categories"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 534
    .local v3, "categories":Lorg/json/JSONArray;
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    .line 535
    .local v7, "count":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_b

    .line 536
    .local v6, "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_5
    if-ge v10, v7, :cond_b

    .line 539
    :try_start_6
    invoke-virtual {v3, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_a

    move-result-object v15

    .line 544
    .local v15, "obj":Lorg/json/JSONObject;
    :try_start_7
    new-instance v12, Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    invoke-direct {v12}, Lcom/samsung/android/internal/headlines/HeadlinesCategory;-><init>()V
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_b

    .line 546
    .local v12, "item":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    :try_start_8
    const-string v18, "id"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v12, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_c

    .line 552
    :try_start_9
    const-string v18, "name"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v12, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->name:Ljava/lang/String;
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_d

    .line 557
    const/16 v18, 0x1

    :try_start_a
    move/from16 v0, v18

    iput-boolean v0, v12, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->active:Z

    .line 559
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->checkSupportedCategory(Lcom/samsung/android/internal/headlines/HeadlinesCategory;)Z

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    .line 560
    invoke-interface {v6, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 561
    const-string v18, "HeadlinesRequest"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Category was added. id : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget-object v0, v12, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " name : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget-object v0, v12, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_b

    .line 536
    .end local v12    # "item":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .end local v15    # "obj":Lorg/json/JSONObject;
    :goto_6
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 507
    .end local v3    # "categories":Lorg/json/JSONArray;
    .end local v6    # "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    .end local v7    # "count":I
    .end local v10    # "i":I
    .end local v16    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "builder":Ljava/lang/StringBuilder;
    .restart local v14    # "line":Ljava/lang/String;
    .restart local v17    # "reader":Ljava/io/BufferedReader;
    :cond_4
    :try_start_b
    new-instance v5, Lorg/json/JSONObject;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_e
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 517
    .end local v4    # "categoryJson":Lorg/json/JSONObject;
    .local v5, "categoryJson":Lorg/json/JSONObject;
    if-eqz v17, :cond_5

    .line 518
    :try_start_c
    invoke-virtual/range {v17 .. v17}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1

    .line 523
    :cond_5
    :goto_7
    if-eqz v11, :cond_6

    .line 524
    :try_start_d
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_2

    :cond_6
    move-object/from16 v16, v17

    .end local v17    # "reader":Ljava/io/BufferedReader;
    .restart local v16    # "reader":Ljava/io/BufferedReader;
    move-object v4, v5

    .line 527
    .end local v5    # "categoryJson":Lorg/json/JSONObject;
    .restart local v4    # "categoryJson":Lorg/json/JSONObject;
    goto/16 :goto_4

    .line 519
    .end local v4    # "categoryJson":Lorg/json/JSONObject;
    .end local v16    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "categoryJson":Lorg/json/JSONObject;
    .restart local v17    # "reader":Ljava/io/BufferedReader;
    :catch_1
    move-exception v8

    .line 520
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 525
    .end local v8    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v8

    .line 526
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    move-object/from16 v16, v17

    .end local v17    # "reader":Ljava/io/BufferedReader;
    .restart local v16    # "reader":Ljava/io/BufferedReader;
    move-object v4, v5

    .line 528
    .end local v5    # "categoryJson":Lorg/json/JSONObject;
    .restart local v4    # "categoryJson":Lorg/json/JSONObject;
    goto/16 :goto_4

    .line 519
    .end local v2    # "builder":Ljava/lang/StringBuilder;
    .end local v14    # "line":Ljava/lang/String;
    :catch_3
    move-exception v8

    .line 520
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 525
    :catch_4
    move-exception v8

    .line 526
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 512
    .end local v8    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v13

    .line 513
    .local v13, "jsonException":Lorg/json/JSONException;
    :goto_8
    :try_start_e
    const-string v18, "HeadlinesRequest"

    const-string v19, "initDefaultCategoryList failed"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    invoke-virtual {v13}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 517
    if-eqz v16, :cond_7

    .line 518
    :try_start_f
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedReader;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_7

    .line 523
    :cond_7
    :goto_9
    if-eqz v11, :cond_3

    .line 524
    :try_start_10
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6

    goto/16 :goto_4

    .line 525
    :catch_6
    move-exception v8

    .line 526
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 519
    .end local v8    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v8

    .line 520
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 516
    .end local v8    # "e":Ljava/io/IOException;
    .end local v13    # "jsonException":Lorg/json/JSONException;
    :catchall_0
    move-exception v18

    .line 517
    :goto_a
    if-eqz v16, :cond_8

    .line 518
    :try_start_11
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedReader;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_8

    .line 523
    :cond_8
    :goto_b
    if-eqz v11, :cond_9

    .line 524
    :try_start_12
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_9

    .line 527
    :cond_9
    :goto_c
    throw v18

    .line 519
    :catch_8
    move-exception v8

    .line 520
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 525
    .end local v8    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v8

    .line 526
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 540
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v3    # "categories":Lorg/json/JSONArray;
    .restart local v6    # "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    .restart local v7    # "count":I
    .restart local v10    # "i":I
    :catch_a
    move-exception v8

    .line 541
    .local v8, "e":Lorg/json/JSONException;
    :try_start_13
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_13
    .catch Lorg/json/JSONException; {:try_start_13 .. :try_end_13} :catch_b

    goto :goto_6

    .line 567
    .end local v3    # "categories":Lorg/json/JSONArray;
    .end local v6    # "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    .end local v7    # "count":I
    .end local v8    # "e":Lorg/json/JSONException;
    .end local v10    # "i":I
    :catch_b
    move-exception v9

    .line 568
    .local v9, "e1":Lorg/json/JSONException;
    const-string v18, "HeadlinesRequest"

    const-string v19, "Category json data is wrong. There is no categories field."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    invoke-virtual {v9}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0

    .line 547
    .end local v9    # "e1":Lorg/json/JSONException;
    .restart local v3    # "categories":Lorg/json/JSONArray;
    .restart local v6    # "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    .restart local v7    # "count":I
    .restart local v10    # "i":I
    .restart local v12    # "item":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .restart local v15    # "obj":Lorg/json/JSONObject;
    :catch_c
    move-exception v8

    .line 548
    .restart local v8    # "e":Lorg/json/JSONException;
    :try_start_14
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_6

    .line 553
    .end local v8    # "e":Lorg/json/JSONException;
    :catch_d
    move-exception v8

    .line 554
    .restart local v8    # "e":Lorg/json/JSONException;
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_6

    .line 563
    .end local v8    # "e":Lorg/json/JSONException;
    :cond_a
    const-string v18, "HeadlinesRequest"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Category was not supported. id : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget-object v0, v12, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " name : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget-object v0, v12, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 566
    .end local v12    # "item":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .end local v15    # "obj":Lorg/json/JSONObject;
    :cond_b
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mDefaultChinaCategoryList:Ljava/util/List;
    :try_end_14
    .catch Lorg/json/JSONException; {:try_start_14 .. :try_end_14} :catch_b

    goto/16 :goto_0

    .line 516
    .end local v3    # "categories":Lorg/json/JSONArray;
    .end local v6    # "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    .end local v7    # "count":I
    .end local v10    # "i":I
    .end local v16    # "reader":Ljava/io/BufferedReader;
    .restart local v17    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v18

    move-object/from16 v16, v17

    .end local v17    # "reader":Ljava/io/BufferedReader;
    .restart local v16    # "reader":Ljava/io/BufferedReader;
    goto :goto_a

    .line 512
    .end local v16    # "reader":Ljava/io/BufferedReader;
    .restart local v17    # "reader":Ljava/io/BufferedReader;
    :catch_e
    move-exception v13

    move-object/from16 v16, v17

    .end local v17    # "reader":Ljava/io/BufferedReader;
    .restart local v16    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_8

    .line 509
    :catch_f
    move-exception v8

    goto/16 :goto_2
.end method

.method public isInitialized()Z
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mMsg:Lcom/flipboard/data/Request$Message;

    sget-object v1, Lcom/flipboard/data/Request$Message;->ERROR:Lcom/flipboard/data/Request$Message;

    if-ne v0, v1, :cond_0

    .line 57
    const-string v0, "HeadlinesRequest"

    const-string v1, "HeadlinesRequest::isInitialized(), Currently HeadlinesRequest is not ready."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const/4 v0, 0x0

    .line 60
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public notifyObservers(Ljava/lang/Object;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 65
    invoke-super {p0, p1}, Ljava/util/Observable;->notifyObservers(Ljava/lang/Object;)V

    .line 66
    return-void
.end method

.method public queryCategory(Ljava/lang/String;Ljava/util/Observer;)V
    .locals 5
    .param p1, "localeString"    # Ljava/lang/String;
    .param p2, "observer"    # Ljava/util/Observer;

    .prologue
    .line 134
    sget-object v2, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "locale":Ljava/lang/String;
    const-string v2, "CN"

    invoke-static {v2}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 136
    const-string v1, "zh_CN"

    .line 137
    :cond_0
    const-string v2, "HeadlinesRequest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HeadlinesRequest getCategory. localeString = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mCategoryList:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mCategoryList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 139
    const-string v2, "HeadlinesRequest"

    const-string v3, "HeadlinesRequest getCategory. category list is exist."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    sget-object v2, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mCategoryList:Ljava/util/List;

    invoke-interface {p2, v2, v3}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 215
    :goto_0
    return-void

    .line 143
    :cond_1
    const-string v2, "HeadlinesRequest"

    const-string v3, "HeadlinesRequest getCategory"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :try_start_0
    sget-object v2, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    new-instance v3, Lcom/samsung/android/internal/headlines/HeadlinesRequest$1;

    invoke-direct {v3, p0, p2}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$1;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/util/Observer;)V

    new-instance v4, Lcom/samsung/android/internal/headlines/HeadlinesRequest$2;

    invoke-direct {v4, p0, p2}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$2;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/util/Observer;)V

    invoke-virtual {v2, v3, v4, v1}, Lcom/flipboard/data/Request;->getCategories(Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/flipboard/data/FDLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 207
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Lcom/flipboard/data/FDLException;
    const-string v2, "HeadlinesRequest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mRequest.getCategories occured exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/flipboard/data/FDLException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-virtual {v0}, Lcom/flipboard/data/FDLException;->printStackTrace()V

    .line 210
    sget-object v2, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    const/4 v3, 0x0

    invoke-interface {p2, v2, v3}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public serviceLogOut(Ljava/lang/String;Ljava/util/Observer;)V
    .locals 4
    .param p1, "socialId"    # Ljava/lang/String;
    .param p2, "observer"    # Ljava/util/Observer;

    .prologue
    .line 336
    :try_start_0
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    new-instance v2, Lcom/samsung/android/internal/headlines/HeadlinesRequest$9;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$9;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/lang/String;Ljava/util/Observer;)V

    new-instance v3, Lcom/samsung/android/internal/headlines/HeadlinesRequest$10;

    invoke-direct {v3, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$10;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/lang/String;)V

    invoke-virtual {v1, p1, v2, v3}, Lcom/flipboard/data/Request;->serviceLogout(Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    :try_end_0
    .catch Lcom/flipboard/data/FDLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :goto_0
    return-void

    .line 353
    :catch_0
    move-exception v0

    .line 354
    .local v0, "e":Lcom/flipboard/data/FDLException;
    const-string v1, "HeadlinesRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mRequest.serviceLogout occurred exception. service : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/flipboard/data/FDLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/flipboard/data/FDLException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    invoke-virtual {v0}, Lcom/flipboard/data/FDLException;->printStackTrace()V

    goto :goto_0
.end method

.method public serviceLogin(Ljava/lang/String;Ljava/util/Observer;)V
    .locals 4
    .param p1, "socialId"    # Ljava/lang/String;
    .param p2, "observer"    # Ljava/util/Observer;

    .prologue
    .line 297
    :try_start_0
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mRequest:Lcom/flipboard/data/Request;

    new-instance v2, Lcom/samsung/android/internal/headlines/HeadlinesRequest$7;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$7;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/lang/String;Ljava/util/Observer;)V

    new-instance v3, Lcom/samsung/android/internal/headlines/HeadlinesRequest$8;

    invoke-direct {v3, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$8;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/lang/String;)V

    invoke-virtual {v1, p1, v2, v3}, Lcom/flipboard/data/Request;->serviceLogin(Ljava/lang/String;Lcom/flipboard/data/Request$FLSuccessListener;Lcom/flipboard/data/Request$FLErrorListener;)V
    :try_end_0
    .catch Lcom/flipboard/data/FDLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    :goto_0
    return-void

    .line 319
    :catch_0
    move-exception v0

    .line 320
    .local v0, "e":Lcom/flipboard/data/FDLException;
    const-string v1, "HeadlinesRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "it is failed to log on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " caused by "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/flipboard/data/FDLException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;

    if-eqz v1, :cond_0

    .line 322
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    .line 323
    const/4 v1, 0x0

    sput-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;

    .line 325
    :cond_0
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090009

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x3e8

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;

    .line 326
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 328
    invoke-virtual {v0}, Lcom/flipboard/data/FDLException;->printStackTrace()V

    goto :goto_0
.end method
