.class public Lcom/samsung/android/internal/headlines/HeadlinesItem;
.super Ljava/lang/Object;
.source "HeadlinesItem.java"


# instance fields
.field public authorImageURL:Ljava/lang/String;

.field public authorName:Ljava/lang/String;

.field public category:Ljava/lang/String;

.field public dataCreated:J

.field public imageHeight:I

.field public imageURL:Ljava/lang/String;

.field public imageWidth:I

.field public jsonObject:Ljava/lang/String;

.field public linkURL:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public service:Ljava/lang/String;

.field public text:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public type:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorName:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorImageURL:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->text:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->title:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->linkURL:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->service:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageURL:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->name:Ljava/lang/String;

    .line 20
    iput v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageWidth:I

    .line 21
    iput v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageHeight:I

    .line 22
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->dataCreated:J

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->type:Ljava/lang/String;

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->jsonObject:Ljava/lang/String;

    .line 26
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "_category"    # Ljava/lang/String;
    .param p3, "_name"    # Ljava/lang/String;
    .param p4, "obj"    # Lorg/json/JSONObject;

    .prologue
    const/4 v3, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    .line 12
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorName:Ljava/lang/String;

    .line 13
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorImageURL:Ljava/lang/String;

    .line 14
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->text:Ljava/lang/String;

    .line 15
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->title:Ljava/lang/String;

    .line 16
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->linkURL:Ljava/lang/String;

    .line 17
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->service:Ljava/lang/String;

    .line 18
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageURL:Ljava/lang/String;

    .line 19
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->name:Ljava/lang/String;

    .line 20
    iput v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageWidth:I

    .line 21
    iput v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageHeight:I

    .line 22
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->dataCreated:J

    .line 23
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->type:Ljava/lang/String;

    .line 24
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->jsonObject:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->name:Ljava/lang/String;

    .line 31
    const/4 v1, 0x0

    .line 33
    .local v1, "object":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "author"

    invoke-virtual {p4, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 34
    const-string v2, "name"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorName:Ljava/lang/String;

    .line 35
    const-string v2, "imageURL"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorImageURL:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_8

    .line 40
    :goto_0
    :try_start_1
    const-string v2, "text"

    invoke-virtual {p4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->text:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_7

    .line 45
    :goto_1
    :try_start_2
    const-string v2, "title"

    invoke-virtual {p4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->title:Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_6

    .line 50
    :goto_2
    :try_start_3
    const-string v2, "flipboardURL"

    invoke-virtual {p4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->linkURL:Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 61
    :goto_3
    :try_start_4
    const-string v2, "service"

    invoke-virtual {p4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->service:Ljava/lang/String;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_5

    .line 66
    :goto_4
    :try_start_5
    const-string v2, "image"

    invoke-virtual {p4, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 67
    const-string v2, "url"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageURL:Ljava/lang/String;

    .line 68
    const-string v2, "width"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageWidth:I

    .line 69
    const-string v2, "height"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageHeight:I
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_4

    .line 75
    :goto_5
    :try_start_6
    const-string v2, "dateCreated"

    invoke-virtual {p4, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->dataCreated:J
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_3

    .line 80
    :goto_6
    :try_start_7
    const-string v2, "type"

    invoke-virtual {p4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->type:Ljava/lang/String;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_2

    .line 84
    :goto_7
    invoke-virtual {p4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->jsonObject:Ljava/lang/String;

    .line 85
    const-string v2, "Headlines"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "jsonObject = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->jsonObject:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Lorg/json/JSONException;
    :try_start_8
    const-string v2, "linkURL"

    invoke-virtual {p4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->linkURL:Ljava/lang/String;
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_3

    .line 55
    :catch_1
    move-exception v2

    goto :goto_3

    .line 81
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v2

    goto :goto_7

    .line 76
    :catch_3
    move-exception v2

    goto :goto_6

    .line 70
    :catch_4
    move-exception v2

    goto :goto_5

    .line 62
    :catch_5
    move-exception v2

    goto :goto_4

    .line 46
    :catch_6
    move-exception v2

    goto :goto_2

    .line 41
    :catch_7
    move-exception v2

    goto :goto_1

    .line 36
    :catch_8
    move-exception v2

    goto/16 :goto_0
.end method
