.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$4;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->queryService(Ljava/util/Observer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

.field final synthetic val$observer:Ljava/util/Observer;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/Observer;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$4;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$4;->val$observer:Ljava/util/Observer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 254
    if-eqz p2, :cond_0

    instance-of v0, p2, Ljava/util/List;

    if-nez v0, :cond_1

    .line 256
    :cond_0
    const-string v0, "Headlines"

    const-string v1, "queryService. return null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$4;->val$observer:Ljava/util/Observer;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 265
    .end local p2    # "arg1":Ljava/lang/Object;
    :goto_0
    return-void

    .line 260
    .restart local p2    # "arg1":Ljava/lang/Object;
    :cond_1
    const-string v0, "Headlines"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queryService. return count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$4;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mSocialList:Ljava/util/List;
    invoke-static {v2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$300(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$4;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    check-cast p2, Ljava/util/List;

    .end local p2    # "arg1":Ljava/lang/Object;
    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mSocialList:Ljava/util/List;
    invoke-static {v0, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$302(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/List;)Ljava/util/List;

    .line 262
    const-string v0, "Headlines"

    const-string v1, "queryService."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$4;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mHeadlinesCardManager:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$400(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$4;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mSocialList:Ljava/util/List;
    invoke-static {v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$300(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->setSocialList(Ljava/util/List;)V

    .line 264
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$4;->val$observer:Ljava/util/Observer;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    goto :goto_0
.end method
