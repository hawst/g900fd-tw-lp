.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestServiceLogout(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$observer:Ljava/util/Observer;

.field final synthetic val$socialId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V
    .locals 0

    .prologue
    .line 567
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;->val$socialId:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;->val$intent:Landroid/content/Intent;

    iput-object p4, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;->val$observer:Ljava/util/Observer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 7
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 570
    if-nez p2, :cond_1

    .line 596
    :cond_0
    return-void

    .line 573
    :cond_1
    instance-of v3, p2, Ljava/util/List;

    if-eqz v3, :cond_0

    move-object v2, p2

    .line 575
    check-cast v2, Ljava/util/List;

    .line 576
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 577
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    .line 578
    .local v0, "category":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    iget-object v3, v0, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;->val$socialId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, v0, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->active:Z

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 579
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;->val$socialId:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;->val$intent:Landroid/content/Intent;

    new-instance v6, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12$1;

    invoke-direct {v6, p0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12$1;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;)V

    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->serviceLogOut(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V

    .line 576
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
