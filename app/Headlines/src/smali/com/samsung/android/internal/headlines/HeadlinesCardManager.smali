.class public Lcom/samsung/android/internal/headlines/HeadlinesCardManager;
.super Ljava/lang/Object;
.source "HeadlinesCardManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;,
        Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;,
        Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;
    }
.end annotation


# static fields
.field private static AUTHOR_IMAGE_HEIGHT_SIZE:I = 0x0

.field private static AUTHOR_IMAGE_WIDTH_SIZE:I = 0x0

.field private static MAIN_IMAGE_HEIGHT_SIZE:I = 0x0

.field private static MAIN_IMAGE_WIDTH_SIZE:I = 0x0

.field private static MAX_AUTHOR_IMAGE_HEIGHT_SIZE:I = 0x0

.field private static MAX_AUTHOR_IMAGE_WIDTH_SIZE:I = 0x0

.field private static MAX_MAIN_IMAGE_HEIGHT_SIZE:I = 0x0

.field private static MAX_MAIN_IMAGE_WIDTH_SIZE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "HeadlinesCardManager"

.field private static final TIME_OUT_IMAGE_DOWNLOAD:I = 0x4e20

.field private static volatile instance:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;


# instance fields
.field private final MAX_AUTHOR_SAMPLE_SIZE:I

.field private final MAX_MAIN_SAMPLE_SIZE:I

.field private mCardManager:Lcom/samsung/android/magazine/cardprovider/CardManager;

.field private mCardTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mChangedLocaleTime:J

.field private mConfigurationManager:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

.field private mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

.field private mInit:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

.field private mNewsWelcomeList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSocialList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSocialWelcomeList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mToast:Landroid/widget/Toast;

.field private retryCount:I

.field private welcomeInit:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ShowToast"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v6, 0x4

    iput v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAX_MAIN_SAMPLE_SIZE:I

    .line 64
    const/16 v6, 0x20

    iput v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAX_AUTHOR_SAMPLE_SIZE:I

    .line 74
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v6}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    .line 75
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    invoke-static {v6}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    .line 76
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    invoke-static {v6}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialWelcomeList:Ljava/util/Set;

    .line 77
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v6}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialList:Ljava/util/List;

    .line 78
    new-instance v6, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    invoke-direct {v6}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;-><init>()V

    iput-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    .line 82
    iput-object v9, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardprovider/CardManager;

    .line 83
    iput-object v9, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    .line 85
    iput v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->retryCount:I

    .line 86
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mChangedLocaleTime:J

    .line 87
    iput-object v9, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mToast:Landroid/widget/Toast;

    .line 88
    sget-object v6, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    iput-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    .line 215
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    .line 216
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/android/magazine/cardprovider/CardManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/magazine/cardprovider/CardManager;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardprovider/CardManager;

    .line 217
    const-string v6, "HeadlinesCardManager"

    const-string v7, "HeadlinesCardManager : constructor"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080082

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sput v6, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAIN_IMAGE_WIDTH_SIZE:I

    .line 220
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080083

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sput v6, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAIN_IMAGE_HEIGHT_SIZE:I

    .line 221
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080084

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sput v6, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->AUTHOR_IMAGE_WIDTH_SIZE:I

    .line 222
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080085

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sput v6, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->AUTHOR_IMAGE_HEIGHT_SIZE:I

    .line 223
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080086

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sput v6, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAX_MAIN_IMAGE_WIDTH_SIZE:I

    .line 224
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080087

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sput v6, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAX_MAIN_IMAGE_HEIGHT_SIZE:I

    .line 225
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080088

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sput v6, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAX_AUTHOR_IMAGE_WIDTH_SIZE:I

    .line 226
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080089

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sput v6, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAX_AUTHOR_IMAGE_HEIGHT_SIZE:I

    .line 228
    sput-object p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->instance:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    .line 229
    invoke-static {}, Landroid/app/ActivityManager;->staticGetMemoryClass()I

    move-result v2

    .line 230
    .local v2, "memoryInMB":I
    const-string v6, "HeadlinesCardManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HeadlinesCardManager : ctor = image_cache size is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    div-int/lit8 v8, v2, 0x3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "MB"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    invoke-static {p1}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    .line 233
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    if-nez v6, :cond_1

    .line 234
    const-string v6, "HeadlinesCardManager"

    const-string v7, "HeadlinesCardManager : ctor = ConfigurationManager is null !!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    invoke-virtual {v6, v9}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->registerCardProvider(Landroid/content/Intent;)Z

    .line 239
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->Create(Landroid/content/Context;)V

    .line 240
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    .line 241
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v7, "mymagazineService"

    invoke-virtual {v6, v7, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 242
    .local v4, "pref":Landroid/content/SharedPreferences;
    const-string v6, "REGISTER_DEFAULT_CARD_NAME"

    const-string v7, "NO"

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->welcomeInit:Ljava/lang/String;

    .line 243
    const-string v6, "NEWS_WELCOME_LIST"

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v3

    .line 244
    .local v3, "newsList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 245
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 246
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 247
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 248
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 252
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    const-string v6, "SOCIAL_WELCOME_LIST"

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    .line 253
    .local v5, "socialList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v5, :cond_3

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 254
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 255
    .restart local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 256
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialWelcomeList:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 257
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 260
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_3
    const-string v6, "HeadlinesCardManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HeadlinesCardManager : constructor welcome :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->welcomeInit:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v7, "null"

    invoke-static {v6, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mToast:Landroid/widget/Toast;

    .line 264
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->welcomeInit:Ljava/lang/String;

    const-string v7, "NO"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 265
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->registerDefaultCardName()V

    .line 266
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 267
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v6, "REGISTER_DEFAULT_CARD_NAME"

    const-string v7, "YES"

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 268
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)Lcom/samsung/android/magazine/cardprovider/CardManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardprovider/CardManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesCardManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->isSocial(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getChangedLocaleTime()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$400(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/magazine/cardprovider/card/Card;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesCardManager;
    .param p1, "x1"    # Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->updateCard(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/magazine/cardprovider/card/Card;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    return-object v0
.end method

.method private addCardName(Ljava/lang/String;)V
    .locals 6
    .param p1, "cardName"    # Ljava/lang/String;

    .prologue
    .line 1525
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1526
    .local v4, "multi":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "default"

    invoke-interface {v4, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1527
    const/4 v2, 0x0

    .line 1528
    .local v2, "section":Ljava/lang/String;
    const-string v0, "social "

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "social"

    .line 1530
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->isCardNameAdded(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1531
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->addCardName(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/util/Map;Z)Z

    .line 1532
    const-string v0, "HeadlinesCardManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addCardName : cardName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " section = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1536
    :goto_1
    return-void

    .line 1528
    :cond_0
    const-string v2, "news"

    goto :goto_0

    .line 1534
    :cond_1
    const-string v0, "HeadlinesCardManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addCardName : This Card("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") is already added."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private calculateSimpleSize(ZIIII)I
    .locals 5
    .param p1, "main"    # Z
    .param p2, "imgWidth"    # I
    .param p3, "imgHeight"    # I
    .param p4, "reqWidth"    # I
    .param p5, "reqHeight"    # I

    .prologue
    .line 892
    const-string v2, "HeadlinesCardManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "calculateSimpleSize : imput image(w*h) = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "*"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " request Size(w*h) = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "*"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    const/4 v1, 0x1

    .line 894
    .local v1, "inSampleSize":I
    const/4 v0, 0x4

    .line 896
    .local v0, "MaxSampleSize":I
    if-eqz p1, :cond_1

    .line 897
    const/4 v0, 0x4

    .line 902
    :goto_0
    if-gt p2, p4, :cond_0

    if-le p3, p5, :cond_3

    .line 903
    :cond_0
    if-lt p2, p3, :cond_2

    .line 904
    :goto_1
    div-int v2, p2, v1

    if-le v2, p4, :cond_3

    if-ge v1, v0, :cond_3

    .line 905
    mul-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 899
    :cond_1
    const/16 v0, 0x20

    goto :goto_0

    .line 908
    :cond_2
    :goto_2
    div-int v2, p3, v1

    if-le v2, p5, :cond_3

    if-ge v1, v0, :cond_3

    .line 909
    mul-int/lit8 v1, v1, 0x2

    goto :goto_2

    .line 913
    :cond_3
    const-string v2, "HeadlinesCardManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "calculateSimpleSize : return simple size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    return v1
.end method

.method static create(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 199
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->instance:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    if-nez v0, :cond_0

    .line 200
    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->instance:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    .line 202
    :cond_0
    return-void
.end method

.method private decodeBitmapFromStream(ILjava/io/BufferedInputStream;Z)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "maxStreamSize"    # I
    .param p2, "bis"    # Ljava/io/BufferedInputStream;
    .param p3, "main"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 918
    const-string v0, "HeadlinesCardManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "decodeBitmapFromStream : maxStreamSize "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 922
    .local v7, "options":Landroid/graphics/BitmapFactory$Options;
    if-eqz p3, :cond_0

    .line 923
    sget v4, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAIN_IMAGE_WIDTH_SIZE:I

    .line 924
    .local v4, "ImageWidthPX":I
    sget v5, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAIN_IMAGE_HEIGHT_SIZE:I

    .line 930
    .local v5, "ImageHeightPX":I
    :goto_0
    invoke-virtual {p2, p1}, Ljava/io/BufferedInputStream;->mark(I)V

    .line 932
    iput-boolean v9, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 933
    invoke-static {p2, v10, v7}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 934
    iget v2, v7, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 935
    .local v2, "imageWidth":I
    iget v3, v7, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .local v3, "imageHeight":I
    move-object v0, p0

    move v1, p3

    .line 936
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->calculateSimpleSize(ZIIII)I

    move-result v6

    .line 938
    .local v6, "inSampleSize":I
    const/4 v0, 0x0

    iput-boolean v0, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 939
    invoke-virtual {p2}, Ljava/io/BufferedInputStream;->reset()V

    .line 941
    iput v6, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 942
    iput-boolean v9, v7, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 944
    const-string v0, "HeadlinesCardManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "decodeBitmapFromStream : orignal Image =  Width = "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " Height = "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " isMain = "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " sample Size = "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 945
    invoke-static {p2, v10, v7}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 926
    .end local v2    # "imageWidth":I
    .end local v3    # "imageHeight":I
    .end local v4    # "ImageWidthPX":I
    .end local v5    # "ImageHeightPX":I
    .end local v6    # "inSampleSize":I
    :cond_0
    sget v4, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->AUTHOR_IMAGE_WIDTH_SIZE:I

    .line 927
    .restart local v4    # "ImageWidthPX":I
    sget v5, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->AUTHOR_IMAGE_HEIGHT_SIZE:I

    .restart local v5    # "ImageHeightPX":I
    goto :goto_0
.end method

.method private downloadImage(Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "main"    # Z

    .prologue
    const/4 v8, 0x0

    .line 773
    const/4 v7, 0x0

    .line 774
    .local v7, "is":Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 775
    .local v0, "bis":Ljava/io/BufferedInputStream;
    const/4 v2, 0x0

    .line 777
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 778
    .local v6, "imageurl":Ljava/net/URL;
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v3

    check-cast v3, Ljava/net/HttpURLConnection;

    .line 779
    .local v3, "conn":Ljava/net/HttpURLConnection;
    const/16 v9, 0x4e20

    invoke-virtual {v3, v9}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 780
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->connect()V

    .line 781
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    .line 782
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 785
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .local v1, "bis":Ljava/io/BufferedInputStream;
    :try_start_1
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v9

    invoke-direct {p0, v9, v1, p2}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->decodeBitmapFromStream(ILjava/io/BufferedInputStream;Z)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 790
    if-eqz v7, :cond_0

    .line 791
    :try_start_2
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 792
    :cond_0
    const/4 v7, 0x0

    .line 793
    if-eqz v1, :cond_1

    .line 794
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 795
    :cond_1
    const/4 v0, 0x0

    .line 798
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    :goto_0
    if-eqz v2, :cond_2

    :try_start_3
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result v9

    if-nez v9, :cond_9

    .line 830
    .end local v3    # "conn":Ljava/net/HttpURLConnection;
    .end local v6    # "imageurl":Ljava/net/URL;
    :cond_2
    :goto_1
    return-object v8

    .line 786
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "conn":Ljava/net/HttpURLConnection;
    .restart local v6    # "imageurl":Ljava/net/URL;
    :catch_0
    move-exception v4

    .line 787
    .local v4, "e":Ljava/lang/OutOfMemoryError;
    :try_start_4
    invoke-virtual {v4}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 788
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v9

    invoke-direct {p0, v9, v1, p2}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->decodeBitmapFromStream(ILjava/io/BufferedInputStream;Z)Landroid/graphics/Bitmap;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    .line 790
    if-eqz v7, :cond_3

    .line 791
    :try_start_5
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 792
    :cond_3
    const/4 v7, 0x0

    .line 793
    if-eqz v1, :cond_4

    .line 794
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 795
    :cond_4
    const/4 v0, 0x0

    .line 796
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_0

    .line 790
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v4    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    :catchall_0
    move-exception v9

    if-eqz v7, :cond_5

    .line 791
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 792
    :cond_5
    const/4 v7, 0x0

    .line 793
    if-eqz v1, :cond_6

    .line 794
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 795
    :cond_6
    const/4 v0, 0x0

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    :try_start_6
    throw v9
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 807
    .end local v3    # "conn":Ljava/net/HttpURLConnection;
    .end local v6    # "imageurl":Ljava/net/URL;
    :catch_1
    move-exception v4

    .line 808
    .local v4, "e":Ljava/lang/Exception;
    :goto_2
    const-string v9, "HeadlinesCardManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "downloadImage : Exception url : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    if-eqz v7, :cond_7

    .line 813
    :try_start_7
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 814
    const/4 v7, 0x0

    .line 819
    :cond_7
    :goto_3
    if-eqz v0, :cond_8

    .line 821
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 822
    const/4 v0, 0x0

    .line 827
    :cond_8
    :goto_4
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 802
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v3    # "conn":Ljava/net/HttpURLConnection;
    .restart local v6    # "imageurl":Ljava/net/URL;
    :cond_9
    :try_start_9
    const-string v9, "HeadlinesCardManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "downloadImage : Before image scale =  Width = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Height = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    invoke-direct {p0, v2, p2}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->scaledBitmap(Landroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 805
    const-string v9, "HeadlinesCardManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "downloadImage : finish download Image url = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Width = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Height = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " isMain = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    move-object v8, v2

    .line 830
    goto/16 :goto_1

    .line 815
    .end local v3    # "conn":Ljava/net/HttpURLConnection;
    .end local v6    # "imageurl":Ljava/net/URL;
    .restart local v4    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v5

    .line 817
    .local v5, "e1":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 823
    .end local v5    # "e1":Ljava/io/IOException;
    :catch_3
    move-exception v5

    .line 825
    .restart local v5    # "e1":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 807
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v5    # "e1":Ljava/io/IOException;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "conn":Ljava/net/HttpURLConnection;
    .restart local v6    # "imageurl":Ljava/net/URL;
    :catch_4
    move-exception v4

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    goto/16 :goto_2
.end method

.method private getCardType(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 531
    const-string v0, "news news"

    .line 533
    .local v0, "cardType":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "news "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 534
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "news "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 593
    :cond_0
    :goto_0
    return-object v0

    .line 535
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "social "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 536
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "social "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 537
    :cond_2
    const-string v1, "news"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 538
    const-string v0, "news news"

    goto :goto_0

    .line 539
    :cond_3
    const-string v1, "tech"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 540
    const-string v0, "news tech"

    goto :goto_0

    .line 541
    :cond_4
    const-string v1, "new"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 542
    const-string v0, "news new"

    goto :goto_0

    .line 543
    :cond_5
    const-string v1, "sports"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 544
    const-string v0, "news sports"

    goto :goto_0

    .line 545
    :cond_6
    const-string v1, "business"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 546
    const-string v0, "news business"

    goto :goto_0

    .line 547
    :cond_7
    const-string v1, "photos"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 548
    const-string v0, "news photos"

    goto :goto_0

    .line 549
    :cond_8
    const-string v1, "style"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 550
    const-string v0, "news style"

    goto :goto_0

    .line 551
    :cond_9
    const-string v1, "arts"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 552
    const-string v0, "news arts"

    goto/16 :goto_0

    .line 553
    :cond_a
    const-string v1, "living"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 554
    const-string v0, "news living"

    goto/16 :goto_0

    .line 555
    :cond_b
    const-string v1, "music"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 556
    const-string v0, "news music"

    goto/16 :goto_0

    .line 557
    :cond_c
    const-string v1, "travel"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 558
    const-string v0, "news travel"

    goto/16 :goto_0

    .line 559
    :cond_d
    const-string v1, "by"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 560
    const-string v0, "news by"

    goto/16 :goto_0

    .line 561
    :cond_e
    const-string v1, "books"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 562
    const-string v0, "news books"

    goto/16 :goto_0

    .line 563
    :cond_f
    const-string v1, "englishonly"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 564
    const-string v0, "news englishonly"

    goto/16 :goto_0

    .line 565
    :cond_10
    const-string v1, "food"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 566
    const-string v0, "news food"

    goto/16 :goto_0

    .line 567
    :cond_11
    const-string v1, "netherlands"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 568
    const-string v0, "news netherlands"

    goto/16 :goto_0

    .line 569
    :cond_12
    const-string v1, "firstlaunch"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 570
    const-string v0, "news firstlaunch"

    goto/16 :goto_0

    .line 571
    :cond_13
    const-string v1, "local"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 572
    const-string v0, "news local"

    goto/16 :goto_0

    .line 573
    :cond_14
    const-string v1, "twitter"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 574
    const-string v0, "social twitter"

    goto/16 :goto_0

    .line 575
    :cond_15
    const-string v1, "googleplus"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 576
    const-string v0, "social googleplus"

    goto/16 :goto_0

    .line 577
    :cond_16
    const-string v1, "linkedin"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 578
    const-string v0, "social linkedin"

    goto/16 :goto_0

    .line 579
    :cond_17
    const-string v1, "flickr"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 580
    const-string v0, "social flickr"

    goto/16 :goto_0

    .line 581
    :cond_18
    const-string v1, "tumblr"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 582
    const-string v0, "social tumblr"

    goto/16 :goto_0

    .line 583
    :cond_19
    const-string v1, "fivehundredpx"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 584
    const-string v0, "social fivehundredpx"

    goto/16 :goto_0

    .line 585
    :cond_1a
    const-string v1, "weibo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 586
    const-string v0, "social weibo"

    goto/16 :goto_0

    .line 587
    :cond_1b
    const-string v1, "renren"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 588
    const-string v0, "social renren"

    goto/16 :goto_0

    .line 589
    :cond_1c
    const-string v1, "youtube"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 590
    const-string v0, "social youtube"

    goto/16 :goto_0
.end method

.method private getCategoryColor(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 481
    const-string v0, "#3149c1"

    .line 483
    .local v0, "categoryColor":Ljava/lang/String;
    const-string v1, "twitter"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 484
    const-string v0, "#00aced"

    .line 502
    :cond_0
    :goto_0
    return-object v0

    .line 485
    :cond_1
    const-string v1, "googleplus"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 486
    const-string v0, "#db4a37"

    goto :goto_0

    .line 487
    :cond_2
    const-string v1, "linkedin"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 488
    const-string v0, "#2267a7"

    goto :goto_0

    .line 489
    :cond_3
    const-string v1, "flickr"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 490
    const-string v0, "#ff0078"

    goto :goto_0

    .line 491
    :cond_4
    const-string v1, "tumblr"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 492
    const-string v0, "#2f4d6a"

    goto :goto_0

    .line 493
    :cond_5
    const-string v1, "fivehundredpx"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 494
    const-string v0, "#373737"

    goto :goto_0

    .line 495
    :cond_6
    const-string v1, "weibo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 496
    const-string v0, "#e89214"

    goto :goto_0

    .line 497
    :cond_7
    const-string v1, "renren"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 498
    const-string v0, "#015eac"

    goto :goto_0

    .line 499
    :cond_8
    const-string v1, "youtube"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 500
    const-string v0, "#d33633"

    goto :goto_0
.end method

.method private getChangedLocaleTime()J
    .locals 2

    .prologue
    .line 1547
    iget-wide v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mChangedLocaleTime:J

    return-wide v0
.end method

.method public static getCurrentDate(Landroid/content/Context;Ljava/lang/Long;)Ljava/lang/String;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeStamp"    # Ljava/lang/Long;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 1315
    const-string v13, "HeadlinesCardManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getCurrentDate : getCurrentDate(): timeStamp = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1316
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    mul-long v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    .line 1318
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 1319
    .local v8, "now":J
    new-instance v11, Landroid/text/format/Time;

    invoke-direct {v11}, Landroid/text/format/Time;-><init>()V

    .line 1320
    .local v11, "startTime":Landroid/text/format/Time;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-virtual {v11, v14, v15}, Landroid/text/format/Time;->set(J)V

    .line 1321
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 1322
    .local v4, "currentTime":Landroid/text/format/Time;
    invoke-virtual {v4, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 1324
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    iget-wide v0, v11, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v10

    .line 1325
    .local v10, "startDay":I
    iget-wide v14, v4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v8, v9, v14, v15}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v3

    .line 1327
    .local v3, "currentDay":I
    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    .line 1328
    .local v2, "b24HourFormat":Z
    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v7

    .line 1330
    .local v7, "orders":[C
    const-string v5, ""

    .line 1331
    .local v5, "dateFormat":Ljava/lang/String;
    const-string v12, ""

    .line 1333
    .local v12, "timeFormat":Ljava/lang/String;
    const/4 v13, 0x0

    aget-char v13, v7, v13

    const/16 v14, 0x4d

    if-ne v13, v14, :cond_1

    const/4 v13, 0x1

    aget-char v13, v7, v13

    const/16 v14, 0x64

    if-ne v13, v14, :cond_1

    const/4 v13, 0x2

    aget-char v13, v7, v13

    const/16 v14, 0x79

    if-ne v13, v14, :cond_1

    .line 1334
    const-string v5, "MM/dd EEEE "

    .line 1340
    :cond_0
    :goto_0
    if-eqz v2, :cond_3

    .line 1341
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "HH:mm"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1347
    :goto_1
    if-eq v10, v3, :cond_4

    .line 1348
    new-instance v6, Ljava/text/SimpleDateFormat;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v6, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1352
    .local v6, "formatter":Ljava/text/SimpleDateFormat;
    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    return-object v13

    .line 1335
    .end local v6    # "formatter":Ljava/text/SimpleDateFormat;
    :cond_1
    const/4 v13, 0x0

    aget-char v13, v7, v13

    const/16 v14, 0x64

    if-ne v13, v14, :cond_2

    const/4 v13, 0x1

    aget-char v13, v7, v13

    const/16 v14, 0x4d

    if-ne v13, v14, :cond_2

    const/4 v13, 0x2

    aget-char v13, v7, v13

    const/16 v14, 0x79

    if-ne v13, v14, :cond_2

    .line 1336
    const-string v5, "dd/MM EEEE "

    goto :goto_0

    .line 1337
    :cond_2
    const/4 v13, 0x0

    aget-char v13, v7, v13

    const/16 v14, 0x79

    if-ne v13, v14, :cond_0

    const/4 v13, 0x1

    aget-char v13, v7, v13

    const/16 v14, 0x4d

    if-ne v13, v14, :cond_0

    const/4 v13, 0x2

    aget-char v13, v7, v13

    const/16 v14, 0x64

    if-ne v13, v14, :cond_0

    .line 1338
    const-string v5, "MM/dd EEEE "

    goto :goto_0

    .line 1343
    :cond_3
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "h:mm a"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto :goto_1

    .line 1350
    :cond_4
    new-instance v6, Ljava/text/SimpleDateFormat;

    invoke-direct {v6, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v6    # "formatter":Ljava/text/SimpleDateFormat;
    goto :goto_2
.end method

.method static getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->instance:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    return-object v0
.end method

.method private getSocialBackgroundColor(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 506
    const-string v0, "#3149c1"

    .line 508
    .local v0, "categoryColor":Ljava/lang/String;
    const-string v1, "twitter"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 509
    const-string v0, "#00aced"

    .line 527
    :cond_0
    :goto_0
    return-object v0

    .line 510
    :cond_1
    const-string v1, "googleplus"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 511
    const-string v0, "#db4a37"

    goto :goto_0

    .line 512
    :cond_2
    const-string v1, "linkedin"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 513
    const-string v0, "#2267a7"

    goto :goto_0

    .line 514
    :cond_3
    const-string v1, "flickr"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 515
    const-string v0, "#c3c3c3"

    goto :goto_0

    .line 516
    :cond_4
    const-string v1, "tumblr"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 517
    const-string v0, "#2f4d6a"

    goto :goto_0

    .line 518
    :cond_5
    const-string v1, "fivehundredpx"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 519
    const-string v0, "#373737"

    goto :goto_0

    .line 520
    :cond_6
    const-string v1, "weibo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 521
    const-string v0, "#e89214"

    goto :goto_0

    .line 522
    :cond_7
    const-string v1, "renren"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 523
    const-string v0, "#015eac"

    goto :goto_0

    .line 524
    :cond_8
    const-string v1, "youtube"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 525
    const-string v0, "#d33633"

    goto :goto_0
.end method

.method private getTempleteType(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Ljava/lang/String;
    .locals 3
    .param p1, "item"    # Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .prologue
    .line 469
    const-string v0, "template_1"

    .line 471
    .local v0, "templateType":Ljava/lang/String;
    iget-object v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageURL:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 472
    const-string v0, "template_5"

    .line 477
    :cond_0
    :goto_0
    return-object v0

    .line 473
    :cond_1
    iget v1, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageWidth:I

    iget v2, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageHeight:I

    if-ge v1, v2, :cond_0

    .line 474
    const-string v0, "template_3"

    goto :goto_0
.end method

.method private isSocial(Ljava/lang/String;)Z
    .locals 2
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 1042
    const-string v1, "twitter"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1060
    :cond_0
    :goto_0
    return v0

    .line 1044
    :cond_1
    const-string v1, "googleplus"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1046
    const-string v1, "linkedin"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1048
    const-string v1, "flickr"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1050
    const-string v1, "tumblr"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1052
    const-string v1, "fivehundredpx"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1054
    const-string v1, "weibo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1056
    const-string v1, "renren"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1058
    const-string v1, "youtube"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1060
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private registerCardNames(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 283
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    if-nez v3, :cond_1

    .line 310
    :cond_0
    return-void

    .line 286
    :cond_1
    const-string v3, "HeadlinesCardManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "registerCardNames : list size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->getRegisteredCardNames()Ljava/util/List;

    move-result-object v2

    .line 289
    .local v2, "prevList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    .line 290
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 291
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 292
    .local v0, "cardName":Ljava/lang/String;
    const-string v3, "social "

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 290
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 295
    :cond_3
    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 296
    const-string v3, "HeadlinesCardManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "registerCardNames : Remove CardName. CardName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    const-string v3, "social "

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 298
    invoke-direct {p0, v0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->removeCardName(Ljava/lang/String;)V

    goto :goto_1

    .line 301
    .end local v0    # "cardName":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x0

    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 302
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 304
    .restart local v0    # "cardName":Ljava/lang/String;
    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 305
    const-string v3, "HeadlinesCardManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "registerCardNames : Add CardName. CardName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    invoke-direct {p0, v0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->addCardName(Ljava/lang/String;)V

    .line 301
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private removeCardName(Ljava/lang/String;)V
    .locals 4
    .param p1, "cardName"    # Ljava/lang/String;

    .prologue
    .line 1539
    const/4 v0, 0x0

    .line 1540
    .local v0, "section":Ljava/lang/String;
    const-string v1, "social "

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "social"

    .line 1542
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    invoke-virtual {v1, p1}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->removeCardName(Ljava/lang/String;)V

    .line 1543
    const-string v1, "HeadlinesCardManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeCardName : cardName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " section = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1544
    return-void

    .line 1540
    :cond_0
    const-string v0, "news"

    goto :goto_0
.end method

.method private scaledBitmap(Landroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "main"    # Z

    .prologue
    .line 834
    const-string v8, "HeadlinesCardManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "scaledBitmap : input bitmap (w*h) = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "*"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " main = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 840
    if-eqz p2, :cond_0

    .line 841
    sget v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAIN_IMAGE_WIDTH_SIZE:I

    .line 842
    .local v1, "ImageWidthPX":I
    sget v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAIN_IMAGE_HEIGHT_SIZE:I

    .line 843
    .local v0, "ImageHeightPX":I
    sget v4, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAX_MAIN_IMAGE_WIDTH_SIZE:I

    .line 844
    .local v4, "maxImageWidthPX":I
    sget v3, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAX_MAIN_IMAGE_HEIGHT_SIZE:I

    .line 853
    .local v3, "maxImageHeightPX":I
    :goto_0
    const/4 v7, 0x0

    .line 854
    .local v7, "temp":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    if-le v8, v9, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    if-le v8, v4, :cond_2

    .line 855
    const-string v8, "HeadlinesCardManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "scaledBitmap : emergency resize mode : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "x"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 856
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    int-to-float v9, v1

    mul-float/2addr v8, v9

    float-to-int v5, v8

    .line 857
    .local v5, "newHeight":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    if-eq v5, v8, :cond_1

    .line 859
    const/4 v8, 0x0

    :try_start_0
    invoke-static {p1, v1, v5, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 864
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 887
    .end local v5    # "newHeight":I
    :goto_1
    const-string v8, "HeadlinesCardManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "scaledBitmap : return bitmap (w*h) = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "*"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " main = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    return-object v7

    .line 847
    .end local v0    # "ImageHeightPX":I
    .end local v1    # "ImageWidthPX":I
    .end local v3    # "maxImageHeightPX":I
    .end local v4    # "maxImageWidthPX":I
    .end local v7    # "temp":Landroid/graphics/Bitmap;
    :cond_0
    sget v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->AUTHOR_IMAGE_WIDTH_SIZE:I

    .line 848
    .restart local v1    # "ImageWidthPX":I
    sget v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->AUTHOR_IMAGE_HEIGHT_SIZE:I

    .line 849
    .restart local v0    # "ImageHeightPX":I
    sget v4, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAX_AUTHOR_IMAGE_WIDTH_SIZE:I

    .line 850
    .restart local v4    # "maxImageWidthPX":I
    sget v3, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->MAX_AUTHOR_IMAGE_HEIGHT_SIZE:I

    .restart local v3    # "maxImageHeightPX":I
    goto/16 :goto_0

    .line 860
    .restart local v5    # "newHeight":I
    .restart local v7    # "temp":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v2

    .line 861
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 862
    const/4 v8, 0x0

    invoke-static {p1, v1, v5, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 864
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .end local v2    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v8

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    throw v8

    .line 867
    :cond_1
    move-object v7, p1

    goto :goto_1

    .line 869
    .end local v5    # "newHeight":I
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    if-le v8, v9, :cond_4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    if-le v8, v3, :cond_4

    .line 870
    const-string v8, "HeadlinesCardManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "scaledBitmap : emergency resize mode : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "x"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    int-to-float v9, v0

    mul-float/2addr v8, v9

    float-to-int v6, v8

    .line 872
    .local v6, "newWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    if-eq v6, v8, :cond_3

    .line 874
    const/4 v8, 0x0

    :try_start_2
    invoke-static {p1, v6, v0, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v7

    .line 879
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_1

    .line 875
    :catch_1
    move-exception v2

    .line 876
    .restart local v2    # "e":Ljava/lang/OutOfMemoryError;
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 877
    const/4 v8, 0x0

    invoke-static {p1, v6, v0, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v7

    .line 879
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_1

    .end local v2    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_1
    move-exception v8

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    throw v8

    .line 882
    :cond_3
    move-object v7, p1

    goto/16 :goto_1

    .line 885
    .end local v6    # "newWidth":I
    :cond_4
    move-object v7, p1

    goto/16 :goto_1
.end method

.method private updateCard(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/magazine/cardprovider/card/Card;
    .locals 7
    .param p1, "item"    # Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .prologue
    .line 725
    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    invoke-virtual {v4, p1}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->refresh(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/internal/headlines/HeadlinesItem;

    move-result-object v2

    .line 726
    .local v2, "current":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    if-nez v2, :cond_0

    .line 727
    const-string v4, "HeadlinesCardManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateCard : this item is not latest!!. Card Type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 728
    const/4 v1, 0x0

    .line 769
    :goto_0
    return-object v1

    .line 731
    :cond_0
    const-string v4, "HeadlinesCardManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateCard : Card Type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    const/4 v3, 0x0

    .line 733
    .local v3, "mainImage":Landroid/graphics/Bitmap;
    const/4 v0, 0x0

    .line 735
    .local v0, "authorImage":Landroid/graphics/Bitmap;
    iget-object v4, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageURL:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 736
    iget-object v4, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageURL:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->downloadImage(Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 739
    :cond_1
    iget-object v4, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorImageURL:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 740
    iget-object v4, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorImageURL:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->downloadImage(Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 743
    :cond_2
    if-nez v3, :cond_3

    .line 744
    const-string v4, ""

    iput-object v4, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->imageURL:Ljava/lang/String;

    .line 746
    :cond_3
    if-nez v0, :cond_4

    .line 747
    const-string v4, ""

    iput-object v4, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorImageURL:Ljava/lang/String;

    .line 750
    :cond_4
    invoke-virtual {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->buildCard(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/magazine/cardprovider/card/Card;

    move-result-object v1

    .line 752
    .local v1, "card":Lcom/samsung/android/magazine/cardprovider/card/Card;
    if-eqz v3, :cond_5

    .line 753
    const-string v4, "HeadlinesCardManager"

    const-string v5, "updateCard : mainImage was updated."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 754
    new-instance v4, Lcom/samsung/android/magazine/cardprovider/card/CardImage;

    const-string v5, "image1"

    invoke-direct {v4, v5, v3}, Lcom/samsung/android/magazine/cardprovider/card/CardImage;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v4}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 759
    :goto_1
    if-eqz v0, :cond_7

    .line 760
    const-string v4, "HeadlinesCardManager"

    const-string v5, "updateCard : authorImage was updated."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getTemplate()Ljava/lang/String;

    move-result-object v4

    const-string v5, "template_5"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 763
    new-instance v4, Lcom/samsung/android/magazine/cardprovider/card/CardImage;

    const-string v5, "image1"

    invoke-direct {v4, v5, v0}, Lcom/samsung/android/magazine/cardprovider/card/CardImage;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v4}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    goto :goto_0

    .line 756
    :cond_5
    const-string v4, "HeadlinesCardManager"

    const-string v5, "updateCard : It is failed to get mainImage"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 765
    :cond_6
    new-instance v4, Lcom/samsung/android/magazine/cardprovider/card/CardImage;

    const-string v5, "image2"

    invoke-direct {v4, v5, v0}, Lcom/samsung/android/magazine/cardprovider/card/CardImage;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v4}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    goto/16 :goto_0

    .line 767
    :cond_7
    const-string v4, "HeadlinesCardManager"

    const-string v5, "updateCard : It is failed to get authorImage"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method


# virtual methods
.method public ClearSnapshot()V
    .locals 1

    .prologue
    .line 1521
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    invoke-virtual {v0}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->clear()V

    .line 1522
    return-void
.end method

.method public buildCard(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/magazine/cardprovider/card/Card;
    .locals 13
    .param p1, "item"    # Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .prologue
    const/16 v12, 0x20

    .line 949
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getTempleteType(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Ljava/lang/String;

    move-result-object v7

    .line 951
    .local v7, "templateType":Ljava/lang/String;
    iget-object v9, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-direct {p0, v9}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getCardType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 952
    .local v4, "cardType":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    iget-wide v10, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->dataCreated:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getCurrentDate(Landroid/content/Context;Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v5

    .line 953
    .local v5, "date":Ljava/lang/String;
    const-string v1, ""

    .line 955
    .local v1, "bgColor":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/magazine/cardprovider/card/Card;

    iget-object v9, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-direct {v2, v4, v9, v7}, Lcom/samsung/android/magazine/cardprovider/card/Card;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    .local v2, "card":Lcom/samsung/android/magazine/cardprovider/card/Card;
    new-instance v9, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v10, "text1"

    const-string v11, ""

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 961
    new-instance v9, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v10, "text2"

    const-string v11, ""

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 962
    new-instance v9, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v10, "text3"

    const-string v11, ""

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 963
    new-instance v9, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v10, "text4"

    const-string v11, ""

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 964
    new-instance v9, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v10, "text5"

    const-string v11, ""

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 967
    iget-object v9, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->name:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_1

    .line 968
    new-instance v8, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v9, "text1"

    iget-object v10, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->name:Ljava/lang/String;

    invoke-direct {v8, v9, v10}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    .local v8, "text1":Lcom/samsung/android/magazine/cardprovider/card/CardText;
    iget-object v9, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-direct {p0, v9}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getCategoryColor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 971
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_0

    .line 972
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 973
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v9, "backgroundColor"

    invoke-virtual {v0, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 974
    invoke-virtual {v8, v0}, Lcom/samsung/android/magazine/cardprovider/card/CardText;->setAttributes(Ljava/util/Map;)V

    .line 976
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {v2, v8}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 979
    .end local v8    # "text1":Lcom/samsung/android/magazine/cardprovider/card/CardText;
    :cond_1
    iget-object v9, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->title:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_4

    .line 980
    new-instance v9, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v10, "text2"

    iget-object v11, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->title:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 985
    :goto_0
    const-string v9, "template_1"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 986
    iget-object v9, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorName:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 987
    new-instance v9, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v10, "text3"

    iget-object v11, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorName:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 988
    :cond_2
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 989
    new-instance v9, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v10, "text4"

    invoke-direct {v9, v10, v5}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 999
    :cond_3
    :goto_1
    const-string v9, "HeadlinesCardManager"

    const-string v10, "buildCard : =============================================================="

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1000
    const-string v9, "HeadlinesCardManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "buildCard : ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] Building Card"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    const-string v9, "HeadlinesCardManager"

    const-string v10, "buildCard : =============================================================="

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    const-string v9, "HeadlinesCardManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "buildCard : text1 : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1003
    const-string v9, "HeadlinesCardManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "buildCard : text2 : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->title:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1004
    const-string v9, "HeadlinesCardManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "buildCard : text3 : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->text:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1005
    const-string v9, "HeadlinesCardManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "buildCard : text4 : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1006
    const-string v9, "HeadlinesCardManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "buildCard : text5 : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    const-string v9, "HeadlinesCardManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "buildCard : backgroundColor : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1008
    const-string v9, "HeadlinesCardManager"

    const-string v10, "buildCard : =============================================================="

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1010
    iget-object v9, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-direct {p0, v9}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->isSocial(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1011
    const-string v9, "HeadlinesCardManager"

    const-string v10, "buildCard : [Social Card]"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1012
    new-instance v6, Landroid/content/Intent;

    iget-object v9, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-class v10, Lcom/samsung/android/internal/headlines/HeadlinesBroadcastReceiver;

    invoke-direct {v6, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1013
    .local v6, "linkIntent":Landroid/content/Intent;
    const-string v9, "HeadlinesCardManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "buildCard : jsonObject : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->jsonObject:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    const-string v9, "jsonObject"

    iget-object v10, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->jsonObject:Ljava/lang/String;

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1015
    const-string v9, "category"

    iget-object v10, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1016
    const-string v9, "link"

    iget-object v10, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->linkURL:Ljava/lang/String;

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1017
    invoke-virtual {v6, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1018
    const-string v9, "com.samsung.android.internal.headlines.LAUNCH_FLIPBOARD_APP_FOR_SOCIAL"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1019
    new-instance v3, Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    const-string v9, "broadcast"

    invoke-direct {v3, v9}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;-><init>(Ljava/lang/String;)V

    .line 1020
    .local v3, "cardAction":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    const-string v9, "?"

    invoke-virtual {v3, v9}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->setLabel(Ljava/lang/String;)V

    .line 1021
    invoke-virtual {v3, v6}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->setData(Landroid/content/Intent;)V

    .line 1022
    const-string v9, "action1"

    invoke-virtual {v2, v9, v3}, Lcom/samsung/android/magazine/cardprovider/card/Card;->addCardAction(Ljava/lang/String;Lcom/samsung/android/magazine/cardprovider/card/CardAction;)V

    .line 1023
    const-string v9, "HeadlinesCardManager"

    const-string v10, "buildCard : action1 : com.samsung.android.internal.headlines.LAUNCH_FLIPBOARD_APP_FOR_SOCIAL"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    :goto_2
    return-object v2

    .line 982
    .end local v3    # "cardAction":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    .end local v6    # "linkIntent":Landroid/content/Intent;
    :cond_4
    new-instance v9, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v10, "text2"

    const-string v11, ""

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    goto/16 :goto_0

    .line 991
    :cond_5
    iget-object v9, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->text:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_6

    .line 992
    new-instance v9, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v10, "text3"

    iget-object v11, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->text:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 993
    :cond_6
    iget-object v9, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorName:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_7

    .line 994
    new-instance v9, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v10, "text4"

    iget-object v11, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->authorName:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 995
    :cond_7
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 996
    new-instance v9, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v10, "text5"

    invoke-direct {v9, v10, v5}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    goto/16 :goto_1

    .line 1025
    :cond_8
    const-string v9, "HeadlinesCardManager"

    const-string v10, "buildCard : [News Card]"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    new-instance v6, Landroid/content/Intent;

    iget-object v9, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-class v10, Lcom/samsung/android/internal/headlines/HeadlinesBroadcastReceiver;

    invoke-direct {v6, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1027
    .restart local v6    # "linkIntent":Landroid/content/Intent;
    const-string v9, "jsonObject"

    iget-object v10, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->jsonObject:Ljava/lang/String;

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1028
    const-string v9, "category"

    iget-object v10, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1029
    const-string v9, "link"

    iget-object v10, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->linkURL:Ljava/lang/String;

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1030
    invoke-virtual {v6, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1031
    const-string v9, "com.samsung.android.internal.headlines.LAUNCH_FLIPBOARD_APP_FOR_NEWS"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1032
    new-instance v3, Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    const-string v9, "broadcast"

    invoke-direct {v3, v9}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;-><init>(Ljava/lang/String;)V

    .line 1033
    .restart local v3    # "cardAction":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    const-string v9, "?"

    invoke-virtual {v3, v9}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->setLabel(Ljava/lang/String;)V

    .line 1034
    invoke-virtual {v3, v6}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->setData(Landroid/content/Intent;)V

    .line 1035
    const-string v9, "action1"

    invoke-virtual {v2, v9, v3}, Lcom/samsung/android/magazine/cardprovider/card/Card;->addCardAction(Ljava/lang/String;Lcom/samsung/android/magazine/cardprovider/card/CardAction;)V

    .line 1036
    const-string v9, "HeadlinesCardManager"

    const-string v10, "buildCard : action1 : com.samsung.android.internal.headlines.LAUNCH_FLIPBOARD_APP_FOR_NEWS"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public getInitStatus()Z
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;->DONE:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$InitStatus;

    if-ne v0, v1, :cond_0

    .line 193
    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNewsWelcomeList()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1356
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    return-object v0
.end method

.method public getObserver(Ljava/lang/String;)Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;
    .locals 1
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 205
    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;)V

    return-object v0
.end method

.method public getSocialDisplayName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "social"    # Ljava/lang/String;

    .prologue
    .line 1064
    const-string v0, "googleplus"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1065
    const-string v0, "Google+"

    .line 1083
    :goto_0
    return-object v0

    .line 1066
    :cond_0
    const-string v0, "youtube"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1067
    const-string v0, "YouTube"

    goto :goto_0

    .line 1068
    :cond_1
    const-string v0, "twitter"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1069
    const-string v0, "Twitter"

    goto :goto_0

    .line 1070
    :cond_2
    const-string v0, "linkedin"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1071
    const-string v0, "LinkedIn"

    goto :goto_0

    .line 1072
    :cond_3
    const-string v0, "flickr"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1073
    const-string v0, "Flickr"

    goto :goto_0

    .line 1074
    :cond_4
    const-string v0, "tumblr"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1075
    const-string v0, "Tumblr"

    goto :goto_0

    .line 1076
    :cond_5
    const-string v0, "fivehundredpx"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1077
    const-string v0, "500px"

    goto :goto_0

    .line 1078
    :cond_6
    const-string v0, "weibo"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1079
    const-string v0, "Sina Weibo"

    goto :goto_0

    .line 1080
    :cond_7
    const-string v0, "renren"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1081
    const-string v0, "RenRen"

    goto :goto_0

    .line 1083
    :cond_8
    const-string v0, ""

    goto :goto_0
.end method

.method public getSocialWelcomeList()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1360
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialWelcomeList:Ljava/util/Set;

    return-object v0
.end method

.method public isNetworkavailable()Z
    .locals 1

    .prologue
    .line 1301
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    invoke-virtual {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->isConnected()Z

    move-result v0

    return v0
.end method

.method public postNewsWelcomeCard(Ljava/lang/String;)V
    .locals 14
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 1236
    const-string v10, "HeadlinesCardManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "postNewsWelcomeCard : Card Type = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1237
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getCardType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1238
    .local v3, "cardType":Ljava/lang/String;
    const-string v9, "template_welcome_1"

    .line 1239
    .local v9, "templateType":Ljava/lang/String;
    const-string v1, "#fff3f3f0"

    .line 1241
    .local v1, "bgColor":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/magazine/cardprovider/card/Card;

    invoke-direct {v2, v3, p1, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1242
    .local v2, "card":Lcom/samsung/android/magazine/cardprovider/card/Card;
    new-instance v10, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v11, "text1"

    invoke-direct {v10, v11, p1}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 1243
    new-instance v10, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v11, "text2"

    const-string v12, "Content will be available later."

    invoke-direct {v10, v11, v12}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 1245
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1246
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v10, "backgroundColor"

    invoke-virtual {v0, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1247
    invoke-virtual {v2, v0}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setAttributes(Ljava/util/Map;)V

    .line 1249
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1250
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : Building News Welcome Card"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1251
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1252
    const-string v10, "HeadlinesCardManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "postNewsWelcomeCard : text1 : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : text2 : Content will be available later."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1254
    const-string v10, "HeadlinesCardManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "postNewsWelcomeCard : backgroundColor : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1255
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    new-instance v6, Lcom/samsung/android/internal/headlines/HeadlinesItem;

    invoke-direct {v6}, Lcom/samsung/android/internal/headlines/HeadlinesItem;-><init>()V

    .line 1258
    .local v6, "item":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    iput-object p1, v6, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    .line 1259
    const-wide/16 v10, 0x0

    iput-wide v10, v6, Lcom/samsung/android/internal/headlines/HeadlinesItem;->dataCreated:J

    .line 1260
    const/4 v4, 0x0

    .line 1261
    .local v4, "current":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    iget-object v11, v6, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    iget-object v11, v6, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/internal/headlines/HeadlinesItem;

    iget-wide v10, v10, Lcom/samsung/android/internal/headlines/HeadlinesItem;->dataCreated:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-lez v10, :cond_1

    .line 1263
    :cond_0
    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    iget-object v11, v6, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1264
    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    invoke-virtual {v10, v6}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->refresh(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/internal/headlines/HeadlinesItem;

    move-result-object v4

    .line 1266
    :cond_1
    if-eqz v4, :cond_4

    .line 1267
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1269
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : Posting card is succedded."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1270
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1272
    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    invoke-interface {v10, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 1273
    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardprovider/CardManager;

    invoke-virtual {v10, v2}, Lcom/samsung/android/magazine/cardprovider/CardManager;->postCard(Lcom/samsung/android/magazine/cardprovider/card/Card;)Z

    move-result v8

    .line 1274
    .local v8, "result":Z
    if-nez v8, :cond_3

    .line 1275
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1277
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : Posting card is failed. this card is welcome card"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1279
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    .end local v8    # "result":Z
    :cond_2
    :goto_0
    return-void

    .line 1282
    .restart local v8    # "result":Z
    :cond_3
    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    invoke-interface {v10, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1283
    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v11, "mymagazineService"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 1285
    .local v7, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 1286
    .local v5, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v10, "NEWS_WELCOME_LIST"

    iget-object v11, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    invoke-interface {v5, v10, v11}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 1287
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 1291
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v7    # "pref":Landroid/content/SharedPreferences;
    .end local v8    # "result":Z
    :cond_4
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1293
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : Posting card is failed. this card is welcome card"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1295
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNewsWelcomeCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public postNoContentCard(Ljava/lang/String;)V
    .locals 14
    .param p1, "category"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 1154
    const-string v10, "HeadlinesCardManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "postNoContentCard : Card Type = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1156
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getCardType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1157
    .local v4, "cardType":Ljava/lang/String;
    const-string v9, "template_no_content"

    .line 1158
    .local v9, "templateType":Ljava/lang/String;
    const-string v1, "#fff3f3f0"

    .line 1160
    .local v1, "bgColor":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->isSocial(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1161
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getSocialBackgroundColor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1162
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : social bg color is set."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1165
    :cond_0
    new-instance v2, Lcom/samsung/android/magazine/cardprovider/card/Card;

    invoke-direct {v2, v4, p1, v9}, Lcom/samsung/android/magazine/cardprovider/card/Card;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1167
    .local v2, "card":Lcom/samsung/android/magazine/cardprovider/card/Card;
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->isSocial(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1168
    new-instance v6, Landroid/content/Intent;

    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-class v11, Lcom/samsung/android/internal/headlines/HeadlinesBroadcastReceiver;

    invoke-direct {v6, v10, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1169
    .local v6, "intent":Landroid/content/Intent;
    const-string v10, "category"

    invoke-virtual {v6, v10, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1170
    const/16 v10, 0x20

    invoke-virtual {v6, v10}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1171
    const-string v10, "com.samsung.android.internal.headlines.LOGIN_SOCIAL"

    invoke-virtual {v6, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1172
    new-instance v3, Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    const-string v10, "broadcast"

    invoke-direct {v3, v10}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;-><init>(Ljava/lang/String;)V

    .line 1173
    .local v3, "cardAction":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    const-string v10, "?"

    invoke-virtual {v3, v10}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->setLabel(Ljava/lang/String;)V

    .line 1174
    invoke-virtual {v3, v6}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->setData(Landroid/content/Intent;)V

    .line 1175
    const-string v10, "action1"

    invoke-virtual {v2, v10, v3}, Lcom/samsung/android/magazine/cardprovider/card/Card;->addCardAction(Ljava/lang/String;Lcom/samsung/android/magazine/cardprovider/card/CardAction;)V

    .line 1187
    :goto_0
    new-instance v10, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v11, "text1"

    invoke-direct {v10, v11, p1}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 1188
    new-instance v10, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v11, "text2"

    const-string v12, "Contents will be available later."

    invoke-direct {v10, v11, v12}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 1190
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1191
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v10, "backgroundColor"

    invoke-virtual {v0, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1192
    invoke-virtual {v2, v0}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setAttributes(Ljava/util/Map;)V

    .line 1194
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1195
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : Building No Content Card"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1196
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    const-string v10, "HeadlinesCardManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "postNoContentCard : text1 : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1198
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : text2 : Contents will be available later."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1199
    const-string v10, "HeadlinesCardManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "postNoContentCard : backgroundColor : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1200
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1202
    new-instance v7, Lcom/samsung/android/internal/headlines/HeadlinesItem;

    invoke-direct {v7}, Lcom/samsung/android/internal/headlines/HeadlinesItem;-><init>()V

    .line 1203
    .local v7, "item":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    iput-object p1, v7, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    .line 1204
    const-wide/16 v10, 0x0

    iput-wide v10, v7, Lcom/samsung/android/internal/headlines/HeadlinesItem;->dataCreated:J

    .line 1205
    const/4 v5, 0x0

    .line 1206
    .local v5, "current":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    iget-object v11, v7, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    iget-object v11, v7, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/internal/headlines/HeadlinesItem;

    iget-wide v10, v10, Lcom/samsung/android/internal/headlines/HeadlinesItem;->dataCreated:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-ltz v10, :cond_2

    .line 1208
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    iget-object v11, v7, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1209
    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    invoke-virtual {v10, v7}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->refresh(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/internal/headlines/HeadlinesItem;

    move-result-object v5

    .line 1211
    :cond_2
    if-eqz v5, :cond_5

    .line 1212
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1214
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : Posting card is succedded."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1215
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1217
    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardprovider/CardManager;

    invoke-virtual {v10, v2}, Lcom/samsung/android/magazine/cardprovider/CardManager;->updateCard(Lcom/samsung/android/magazine/cardprovider/card/Card;)Z

    move-result v8

    .line 1219
    .local v8, "result":Z
    if-nez v8, :cond_3

    .line 1220
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1222
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : Posting card is failed. this card is welcome card"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1223
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    .end local v8    # "result":Z
    :cond_3
    :goto_1
    return-void

    .line 1177
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "cardAction":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    .end local v5    # "current":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "item":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    :cond_4
    new-instance v6, Landroid/content/Intent;

    iget-object v10, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-class v11, Lcom/samsung/android/internal/headlines/HeadlinesBroadcastReceiver;

    invoke-direct {v6, v10, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1178
    .restart local v6    # "intent":Landroid/content/Intent;
    const-string v10, "category"

    invoke-virtual {v6, v10, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1179
    new-instance v3, Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    const-string v10, "broadcast"

    invoke-direct {v3, v10}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;-><init>(Ljava/lang/String;)V

    .line 1180
    .restart local v3    # "cardAction":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    const-string v10, "?"

    invoke-virtual {v3, v10}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->setLabel(Ljava/lang/String;)V

    .line 1181
    invoke-virtual {v3, v6}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->setData(Landroid/content/Intent;)V

    .line 1182
    const/16 v10, 0x20

    invoke-virtual {v6, v10}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1183
    const-string v10, "com.samsung.android.internal.headlines.UPDATE_FEED"

    invoke-virtual {v6, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1184
    const-string v10, "action1"

    invoke-virtual {v2, v10, v3}, Lcom/samsung/android/magazine/cardprovider/card/Card;->addCardAction(Ljava/lang/String;Lcom/samsung/android/magazine/cardprovider/card/CardAction;)V

    goto/16 :goto_0

    .line 1227
    .restart local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v5    # "current":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    .restart local v7    # "item":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    :cond_5
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1229
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : Posting card is failed. this card is welcome card"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1230
    const-string v10, "HeadlinesCardManager"

    const-string v11, "postNoContentCard : =============================================================="

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public postSocialWelcomeCard(Ljava/lang/String;)V
    .locals 20
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 1087
    const-string v16, "HeadlinesCardManager"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "postSocialWelcomeCard : Post Social Welcome Card. Card Type = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1089
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getCardType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1090
    .local v7, "cardType":Ljava/lang/String;
    const-string v14, "template_welcome_2"

    .line 1091
    .local v14, "templateType":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getSocialBackgroundColor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1093
    .local v3, "bgColor":Ljava/lang/String;
    new-instance v4, Lcom/samsung/android/magazine/cardprovider/card/Card;

    move-object/from16 v0, p1

    invoke-direct {v4, v7, v0, v14}, Lcom/samsung/android/magazine/cardprovider/card/Card;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1094
    .local v4, "card":Lcom/samsung/android/magazine/cardprovider/card/Card;
    new-instance v10, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    const-class v17, Lcom/samsung/android/internal/headlines/HeadlinesBroadcastReceiver;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1095
    .local v10, "intent":Landroid/content/Intent;
    const-string v16, "category"

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1096
    const/16 v16, 0x20

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1097
    const-string v16, "com.samsung.android.internal.headlines.LOGIN_SOCIAL"

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1098
    new-instance v5, Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    const-string v16, "broadcast"

    move-object/from16 v0, v16

    invoke-direct {v5, v0}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;-><init>(Ljava/lang/String;)V

    .line 1099
    .local v5, "cardAction":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    const-string v16, "?"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->setLabel(Ljava/lang/String;)V

    .line 1100
    invoke-virtual {v5, v10}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->setData(Landroid/content/Intent;)V

    .line 1101
    const-string v16, "action1"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0, v5}, Lcom/samsung/android/magazine/cardprovider/card/Card;->addCardAction(Ljava/lang/String;Lcom/samsung/android/magazine/cardprovider/card/CardAction;)V

    .line 1103
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getSocialDisplayName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1105
    .local v13, "socialDisplay":Ljava/lang/String;
    new-instance v16, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v17, "text1"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v13}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 1106
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    const v17, 0x7f09000e

    invoke-virtual/range {v16 .. v17}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1107
    .local v15, "text":Ljava/lang/String;
    new-instance v6, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    const-string v16, "text2"

    move-object/from16 v0, v16

    invoke-direct {v6, v0, v15}, Lcom/samsung/android/magazine/cardprovider/card/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1108
    .local v6, "cardText2":Lcom/samsung/android/magazine/cardprovider/card/CardText;
    invoke-virtual {v6, v5}, Lcom/samsung/android/magazine/cardprovider/card/CardText;->setAction(Lcom/samsung/android/magazine/cardprovider/card/CardAction;)V

    .line 1109
    invoke-virtual {v4, v6}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V

    .line 1111
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1112
    .local v2, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v16, "backgroundColor"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113
    invoke-virtual {v4, v2}, Lcom/samsung/android/magazine/cardprovider/card/Card;->setAttributes(Ljava/util/Map;)V

    .line 1115
    const-string v16, "HeadlinesCardManager"

    const-string v17, "postSocialWelcomeCard : =============================================================="

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    const-string v16, "HeadlinesCardManager"

    const-string v17, "postSocialWelcomeCard : Building Social Welcome Card"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1117
    const-string v16, "HeadlinesCardManager"

    const-string v17, "postSocialWelcomeCard : =============================================================="

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1118
    const-string v16, "HeadlinesCardManager"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "postSocialWelcomeCard : text1 : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1119
    const-string v16, "HeadlinesCardManager"

    const-string v17, "postSocialWelcomeCard : text2 : Tap to get started"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1120
    const-string v16, "HeadlinesCardManager"

    const-string v17, "postSocialWelcomeCard : text3 : Sign in to your favorite social network to get started."

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1121
    const-string v16, "HeadlinesCardManager"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "postSocialWelcomeCard : backgroundColor : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1122
    const-string v16, "HeadlinesCardManager"

    const-string v17, "postSocialWelcomeCard : =============================================================="

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1124
    new-instance v11, Lcom/samsung/android/internal/headlines/HeadlinesItem;

    invoke-direct {v11}, Lcom/samsung/android/internal/headlines/HeadlinesItem;-><init>()V

    .line 1125
    .local v11, "item":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    move-object/from16 v0, p1

    iput-object v0, v11, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    .line 1126
    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    iput-wide v0, v11, Lcom/samsung/android/internal/headlines/HeadlinesItem;->dataCreated:J

    .line 1127
    const/4 v8, 0x0

    .line 1128
    .local v8, "current":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    move-object/from16 v16, v0

    iget-object v0, v11, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    if-eqz v16, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    move-object/from16 v16, v0

    iget-object v0, v11, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/internal/headlines/HeadlinesItem;

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->dataCreated:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x0

    cmp-long v16, v16, v18

    if-lez v16, :cond_1

    .line 1129
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    move-object/from16 v16, v0

    iget-object v0, v11, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1130
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->refresh(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/internal/headlines/HeadlinesItem;

    move-result-object v8

    .line 1132
    :cond_1
    if-eqz v8, :cond_3

    .line 1133
    const-string v16, "HeadlinesCardManager"

    const-string v17, "postSocialWelcomeCard : =============================================================="

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1134
    const-string v16, "HeadlinesCardManager"

    const-string v17, "postSocialWelcomeCard : Posting card is succedded."

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1135
    const-string v16, "HeadlinesCardManager"

    const-string v17, "postSocialWelcomeCard : =============================================================="

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialWelcomeList:Ljava/util/Set;

    move-object/from16 v16, v0

    iget-object v0, v11, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-interface/range {v16 .. v17}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_2

    .line 1137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardprovider/CardManager;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/samsung/android/magazine/cardprovider/CardManager;->postCard(Lcom/samsung/android/magazine/cardprovider/card/Card;)Z

    .line 1138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialWelcomeList:Ljava/util/Set;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    const-string v17, "mymagazineService"

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v12

    .line 1140
    .local v12, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v12}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    .line 1141
    .local v9, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v16, "SOCIAL_WELCOME_LIST"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialWelcomeList:Ljava/util/Set;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v9, v0, v1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 1142
    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1150
    .end local v9    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v12    # "pref":Landroid/content/SharedPreferences;
    :cond_2
    :goto_0
    return-void

    .line 1145
    :cond_3
    const-string v16, "HeadlinesCardManager"

    const-string v17, "postSocialWelcomeCard : =============================================================="

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1146
    const-string v16, "HeadlinesCardManager"

    const-string v17, "postSocialWelcomeCard : Posting card is failed. this card is welcome card"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1147
    const-string v16, "HeadlinesCardManager"

    const-string v17, "postSocialWelcomeCard : =============================================================="

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public registerDefaultCardName()V
    .locals 7

    .prologue
    .line 313
    const-string v5, "HeadlinesCardManager"

    const-string v6, "registerDefaultCardName : "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    iget-object v5, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->getRegisteredCardNames()Ljava/util/List;

    move-result-object v4

    .line 317
    .local v4, "prevList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 318
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 320
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v5, "news arts"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 321
    const-string v5, "news business"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    const-string v5, "news new"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 323
    const-string v5, "news news"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    const-string v5, "news photos"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    const-string v5, "news sports"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    const-string v5, "news style"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    const-string v5, "news tech"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    const-string v5, "social fivehundredpx"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    const-string v5, "social flickr"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 331
    const-string v5, "social googleplus"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 332
    const-string v5, "social linkedin"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    const-string v5, "social renren"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    const-string v5, "social weibo"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 335
    const-string v5, "social tumblr"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    const-string v5, "social twitter"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    const-string v5, "social youtube"

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 339
    move-object v0, v3

    .line 340
    .local v0, "_list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 341
    .local v1, "cardName":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->addCardName(Ljava/lang/String;)V

    goto :goto_0

    .line 345
    .end local v0    # "_list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "cardName":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    const-string v5, "CN"

    invoke-static {v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 346
    const-string v5, "news englishonly"

    invoke-direct {p0, v5}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->addCardName(Ljava/lang/String;)V

    .line 347
    const-string v5, "news living"

    invoke-direct {p0, v5}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->addCardName(Ljava/lang/String;)V

    .line 348
    const-string v5, "news music"

    invoke-direct {p0, v5}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->addCardName(Ljava/lang/String;)V

    .line 350
    :cond_2
    return-void
.end method

.method public requestMigrationCard(Landroid/content/Intent;)V
    .locals 17
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1429
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getFlags()I

    move-result v14

    const/16 v15, 0x12d

    if-ne v14, v15, :cond_1

    .line 1518
    :cond_0
    :goto_0
    return-void

    .line 1431
    :cond_1
    const-string v14, "HeadlinesCardManager"

    const-string v15, "requestMigrationCard() "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1432
    new-instance v5, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    new-instance v14, Lcom/samsung/android/magazine/cardchannel/CardChannel;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v16, "headlines"

    invoke-direct/range {v14 .. v16}, Lcom/samsung/android/magazine/cardchannel/CardChannel;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {v5, v14}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;-><init>(Lcom/samsung/android/magazine/cardchannel/CardChannel;)V

    .line 1434
    .local v5, "configurationManager":Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v15, "mymagazineService"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 1435
    .local v9, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 1436
    .local v6, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v14, "NEWS_WELCOME_LIST"

    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v6, v14, v15}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 1437
    const-string v14, "SOCIAL_WELCOME_LIST"

    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v6, v14, v15}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 1438
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1439
    invoke-virtual {v5}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->registerCardChannel()Z

    .line 1440
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v15, "CardOrder"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 1441
    const-string v14, "CardCount"

    const/4 v15, 0x0

    invoke-interface {v9, v14, v15}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v11

    .line 1443
    .local v11, "savedCardCount":I
    invoke-virtual {v5}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->getSections()Ljava/util/List;

    move-result-object v13

    .line 1444
    .local v13, "sectionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1445
    .local v2, "cardList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v14

    if-ge v8, v14, :cond_4

    .line 1446
    invoke-interface {v13, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 1447
    .local v12, "section":Ljava/lang/String;
    const-string v14, "ASC"

    invoke-virtual {v5, v12, v14}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->getRegisteredCardInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 1448
    .local v3, "cardNameList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/CardInfo;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v14

    if-ge v7, v14, :cond_3

    .line 1449
    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/magazine/cardchannel/CardInfo;

    invoke-virtual {v14}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1450
    .local v1, "_category":Ljava/lang/String;
    const/4 v14, 0x0

    invoke-virtual {v5, v14, v1, v12}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->isSubscribed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1451
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1452
    const-string v14, "HeadlinesCardManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "requestMigrationCard "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1448
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1445
    .end local v1    # "_category":Ljava/lang/String;
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 1456
    .end local v3    # "cardNameList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/CardInfo;>;"
    .end local v7    # "i":I
    .end local v12    # "section":Ljava/lang/String;
    :cond_4
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_3
    if-ge v7, v11, :cond_8

    .line 1457
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-interface {v9, v14, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1458
    .restart local v1    # "_category":Ljava/lang/String;
    if-nez v1, :cond_5

    .line 1456
    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 1461
    :cond_5
    const-string v14, "HeadlinesCardManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "requestMigrationCard savedCardCount : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1463
    const-string v14, "news "

    invoke-virtual {v1, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1464
    const/4 v14, 0x5

    invoke-virtual {v1, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1465
    .local v4, "category":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->postNewsWelcomeCard(Ljava/lang/String;)V

    .line 1466
    if-eqz p1, :cond_6

    .line 1467
    new-instance v10, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-class v15, Lcom/samsung/android/internal/headlines/HeadlinesService;

    invoke-direct {v10, v14, v15}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1468
    .local v10, "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.magazine.intent.action.CARD_REFRESH"

    invoke-virtual {v10, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1469
    const-string v14, "com.samsung.android.magazine.intent.extra.CARD_ID"

    invoke-virtual {v10, v14, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1470
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v10}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestUpdate(Ljava/lang/String;Landroid/content/Intent;)V

    .line 1483
    .end local v4    # "category":Ljava/lang/String;
    .end local v10    # "sIntent":Landroid/content/Intent;
    :cond_6
    :goto_5
    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1473
    :cond_7
    const-string v14, "social "

    invoke-virtual {v1, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1474
    const/4 v14, 0x7

    invoke-virtual {v1, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1475
    .restart local v4    # "category":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->postSocialWelcomeCard(Ljava/lang/String;)V

    .line 1476
    if-eqz p1, :cond_6

    .line 1477
    new-instance v10, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-class v15, Lcom/samsung/android/internal/headlines/HeadlinesService;

    invoke-direct {v10, v14, v15}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1478
    .restart local v10    # "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.magazine.intent.action.CARD_REFRESH"

    invoke-virtual {v10, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1479
    const-string v14, "com.samsung.android.magazine.intent.extra.CARD_ID"

    invoke-virtual {v10, v14, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1480
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v10}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestUpdate(Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_5

    .line 1485
    .end local v1    # "_category":Ljava/lang/String;
    .end local v4    # "category":Ljava/lang/String;
    .end local v10    # "sIntent":Landroid/content/Intent;
    :cond_8
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v14

    if-lez v14, :cond_b

    .line 1486
    const-string v14, "HeadlinesCardManager"

    const-string v15, "requestMigrationCard remain size > 0"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1487
    const/4 v7, 0x0

    :goto_6
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v14

    if-ge v7, v14, :cond_b

    .line 1488
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1490
    .restart local v1    # "_category":Ljava/lang/String;
    const-string v14, "news "

    invoke-virtual {v1, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 1491
    const/4 v14, 0x5

    invoke-virtual {v1, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1492
    .restart local v4    # "category":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->postNewsWelcomeCard(Ljava/lang/String;)V

    .line 1493
    if-eqz p1, :cond_9

    .line 1494
    new-instance v10, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-class v15, Lcom/samsung/android/internal/headlines/HeadlinesService;

    invoke-direct {v10, v14, v15}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1495
    .restart local v10    # "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.magazine.intent.action.CARD_REFRESH"

    invoke-virtual {v10, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1496
    const-string v14, "com.samsung.android.magazine.intent.extra.CARD_ID"

    invoke-virtual {v10, v14, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1497
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v10}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestUpdate(Ljava/lang/String;Landroid/content/Intent;)V

    .line 1510
    .end local v4    # "category":Ljava/lang/String;
    .end local v10    # "sIntent":Landroid/content/Intent;
    :cond_9
    :goto_7
    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1487
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 1500
    :cond_a
    const-string v14, "social "

    invoke-virtual {v1, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1501
    const/4 v14, 0x7

    invoke-virtual {v1, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1502
    .restart local v4    # "category":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->postSocialWelcomeCard(Ljava/lang/String;)V

    .line 1503
    if-eqz p1, :cond_9

    .line 1504
    new-instance v10, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-class v15, Lcom/samsung/android/internal/headlines/HeadlinesService;

    invoke-direct {v10, v14, v15}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1505
    .restart local v10    # "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.magazine.intent.action.CARD_REFRESH"

    invoke-virtual {v10, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1506
    const-string v14, "com.samsung.android.magazine.intent.extra.CARD_ID"

    invoke-virtual {v10, v14, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1507
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v10}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestUpdate(Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_7

    .line 1513
    .end local v1    # "_category":Ljava/lang/String;
    .end local v4    # "category":Ljava/lang/String;
    .end local v10    # "sIntent":Landroid/content/Intent;
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v15, "CardOrder"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 1514
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v15, "mymagazine"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 1515
    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 1516
    const-string v14, "ENABLE_NEW_STORY_NOTIFICATION"

    const/4 v15, 0x0

    invoke-interface {v6, v14, v15}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1517
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method

.method public requestServiceLogin(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 3
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 664
    const-string v0, "HeadlinesCardManager"

    const-string v1, "requestServiceLogin : To Check social login or not first."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    const-string v0, "HeadlinesCardManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestServiceLogin : category = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-virtual {v0, v1, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->getService(Ljava/util/Observer;Landroid/content/Intent;)V

    .line 703
    return-void
.end method

.method public requestServiceLogout(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 3
    .param p1, "cardType"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 706
    const-string v0, "HeadlinesCardManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestServiceLogout : social id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$3;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$3;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestServiceLogout(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V

    .line 722
    return-void
.end method

.method public requestSocialAction(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 2
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1373
    const-string v0, "HeadlinesCardManager"

    const-string v1, "requestSocialAction : To Check social login or not."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1375
    if-nez p2, :cond_0

    .line 1426
    :goto_0
    return-void

    .line 1378
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-virtual {v0, v1, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->getService(Ljava/util/Observer;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public requestUpdate(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 2
    .param p1, "cardType"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 603
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->isSocial(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestSocialItem(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V

    .line 608
    :goto_0
    return-void

    .line 606
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestUpdate(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V

    goto :goto_0
.end method

.method public requestUpdateNewsWelcomeCard(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1305
    const-string v0, "HeadlinesCardManager"

    const-string v1, "requestUpdateNewsWelcomeCard : request update welcome card to content card."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1306
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    invoke-virtual {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->isChangedLocale()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1307
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    invoke-virtual {v0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestUpdateSystemLanguage(Landroid/content/Intent;)V

    .line 1311
    :goto_0
    return-void

    .line 1309
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestUpdateNewsWelcomeCard(Landroid/content/Intent;Ljava/util/Set;)V

    goto :goto_0
.end method

.method public requestUpdateSystemLanguage(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1364
    const-string v0, "HeadlinesCardManager"

    const-string v1, "requestUpdateSystemLanguage : HeadlinesCardManager"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1365
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mChangedLocaleTime:J

    .line 1366
    const-string v0, "CN"

    invoke-static {v0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1367
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1369
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    invoke-virtual {v0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestUpdateSystemLanguage(Landroid/content/Intent;)V

    .line 1370
    return-void
.end method

.method public retryInit(I)V
    .locals 10
    .param p1, "second"    # I

    .prologue
    const/4 v8, 0x0

    .line 273
    iget v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->retryCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->retryCount:I

    .line 274
    const-string v3, "HeadlinesCardManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "retryInit : HeadlinesCardManager retryInit "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->retryCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v6, "alarm"

    invoke-virtual {v3, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 276
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-class v6, Lcom/samsung/android/internal/headlines/HeadlinesBroadcastReceiver;

    invoke-direct {v1, v3, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 277
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-static {v3, v8, v1, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 278
    .local v2, "sender":Landroid/app/PendingIntent;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    mul-int/lit16 v3, p1, 0x3e8

    int-to-long v8, v3

    add-long v4, v6, v8

    .line 279
    .local v4, "triggerTime":J
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 280
    return-void
.end method

.method public setNewsList(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesCategory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 438
    .local p1, "newsList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 441
    :cond_1
    const-string v3, "HeadlinesCardManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setNewsList : newsList size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 444
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 445
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    .line 446
    .local v0, "category":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "news "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 447
    .local v2, "id":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 444
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 449
    .end local v0    # "category":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .end local v2    # "id":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    invoke-direct {p0, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->registerCardNames(Ljava/util/List;)V

    goto :goto_0
.end method

.method public setSocialList(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesCategory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 453
    .local p1, "socialList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    if-nez p1, :cond_0

    .line 466
    :goto_0
    return-void

    .line 456
    :cond_0
    const-string v3, "HeadlinesCardManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setNewsList : setSocialList socialList size = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 459
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 460
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    .line 461
    .local v0, "category":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "social "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 462
    .local v2, "id":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 463
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialList:Ljava/util/List;

    iget-object v4, v0, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 465
    .end local v0    # "category":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .end local v2    # "id":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    invoke-direct {p0, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->registerCardNames(Ljava/util/List;)V

    goto :goto_0
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 597
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 598
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 599
    const-string v0, "HeadlinesCardManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showToast : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    return-void
.end method

.method public updateCardSubscriptionState(Ljava/lang/String;ZLandroid/content/Intent;)V
    .locals 9
    .param p1, "cardType"    # Ljava/lang/String;
    .param p2, "isEnabled"    # Z
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x5

    const/4 v6, 0x0

    .line 611
    const-string v3, "HeadlinesCardManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateCardSubscriptionState : Card Subscription State is changed. Card Type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v4, "mymagazineService"

    invoke-virtual {v3, v4, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 614
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v3, "USER_DATA_ALLOWED"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 615
    const-string v3, "HeadlinesCardManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateCardSubscriptionState : Card Subscription State is changed. Card Type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 617
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "USER_DATA_ALLOWED"

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 618
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 621
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    const-string v0, ""

    .line 622
    .local v0, "category":Ljava/lang/String;
    if-eqz p2, :cond_4

    .line 623
    const-string v3, "news "

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 624
    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 626
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 627
    invoke-virtual {p0, v0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->postNewsWelcomeCard(Ljava/lang/String;)V

    .line 628
    :cond_1
    invoke-virtual {p0, v0, p3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestUpdate(Ljava/lang/String;Landroid/content/Intent;)V

    .line 661
    :cond_2
    :goto_0
    return-void

    .line 629
    :cond_3
    const-string v3, "social "

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 630
    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 631
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialWelcomeList:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 632
    invoke-virtual {p0, v0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->postSocialWelcomeCard(Ljava/lang/String;)V

    goto :goto_0

    .line 635
    :cond_4
    const-string v3, "news "

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 636
    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 638
    :cond_5
    const-string v3, "social "

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 639
    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 641
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardprovider/CardManager;

    invoke-virtual {v3, v0}, Lcom/samsung/android/magazine/cardprovider/CardManager;->removeCard(Ljava/lang/String;)Z

    .line 642
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    invoke-virtual {v3, v0}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 643
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    invoke-virtual {v3, v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->removeCategory(Ljava/lang/String;)V

    .line 645
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v3

    invoke-direct {v3, v0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->isSocial(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 646
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialWelcomeList:Ljava/util/Set;

    if-eqz v3, :cond_2

    .line 647
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialWelcomeList:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 648
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 649
    .restart local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "SOCIAL_WELCOME_LIST"

    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mSocialWelcomeList:Ljava/util/Set;

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 650
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 653
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    if-eqz v3, :cond_2

    .line 654
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 655
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 656
    .restart local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "NEWS_WELCOME_LIST"

    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 657
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public updateCategory()V
    .locals 3

    .prologue
    .line 175
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 176
    const-string v0, "HeadlinesCardManager"

    const-string v1, "updateCategory : HeadlinesCardManager:updateCategory() category size == 0"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    if-eqz v0, :cond_0

    .line 178
    const-string v0, "HeadlinesCardManager"

    const-string v1, "updateCategory : HeadlinesCardManager:updateCategory() try to query category list"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$1;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->queryCategory(Ljava/util/Observer;Z)V

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    const-string v0, "HeadlinesCardManager"

    const-string v1, "updateCategory : HeadlinesCardManager:updateCategory() category size > 0"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateNewsCardNamesList(Ljava/util/List;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesCategory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 353
    .local p1, "newsList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    if-nez v13, :cond_1

    .line 435
    :cond_0
    return-void

    .line 355
    :cond_1
    const-string v13, "HeadlinesCardManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "updateNewsCardNamesList : newsList size = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    invoke-virtual {v13}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->getRegisteredCardNames()Ljava/util/List;

    move-result-object v12

    .line 358
    .local v12, "prevList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v12, :cond_3

    .line 359
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v13

    if-ge v5, v13, :cond_0

    .line 360
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    .line 361
    .local v2, "category":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    if-eqz v2, :cond_2

    .line 362
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "news "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v2, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 363
    .local v6, "id":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    invoke-interface {v13, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 364
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->addCardName(Ljava/lang/String;)V

    .line 365
    const-string v13, "HeadlinesCardManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "updateNewsCardNamesList : registerCardName card name = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    .end local v6    # "id":Ljava/lang/String;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 371
    .end local v2    # "category":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .end local v5    # "i":I
    :cond_3
    const-string v13, "HeadlinesCardManager"

    const-string v14, "updateNewsCardNamesList : ================ prev category ============"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_1
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v13

    if-ge v5, v13, :cond_4

    .line 373
    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 374
    .local v10, "prev":Ljava/lang/String;
    const-string v13, "HeadlinesCardManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "updateNewsCardNamesList : prev = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 376
    .end local v10    # "prev":Ljava/lang/String;
    :cond_4
    const-string v13, "HeadlinesCardManager"

    const-string v14, "updateNewsCardNamesList : ==========================================="

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    const-string v13, "HeadlinesCardManager"

    const-string v14, "updateNewsCardNamesList : ================ new category ============"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    const/4 v5, 0x0

    :goto_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v13

    if-ge v5, v13, :cond_5

    .line 380
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    .line 381
    .local v8, "newCategory":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    const-string v13, "HeadlinesCardManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "updateNewsCardNamesList : newCategory = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v8, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 383
    .end local v8    # "newCategory":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    :cond_5
    const-string v13, "HeadlinesCardManager"

    const-string v14, "updateNewsCardNamesList : ==========================================="

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    const/4 v5, 0x0

    :goto_3
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v13

    if-ge v5, v13, :cond_7

    .line 386
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    .line 387
    .restart local v2    # "category":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    if-eqz v2, :cond_6

    .line 388
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "news "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v2, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 390
    .restart local v6    # "id":Ljava/lang/String;
    invoke-interface {v12, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_6

    .line 391
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    invoke-interface {v13, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 392
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->addCardName(Ljava/lang/String;)V

    .line 385
    .end local v6    # "id":Ljava/lang/String;
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 397
    .end local v2    # "category":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    :cond_7
    const/4 v5, 0x0

    :goto_4
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v13

    if-ge v5, v13, :cond_0

    .line 398
    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 399
    .local v1, "cardName":Ljava/lang/String;
    const/4 v13, 0x5

    invoke-virtual {v1, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 401
    .local v11, "prevCardId":Ljava/lang/String;
    const-string v13, "social "

    invoke-virtual {v1, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 397
    :cond_8
    :goto_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 404
    :cond_9
    const/4 v3, 0x1

    .line 405
    .local v3, "deleted":Z
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_6
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v13

    if-ge v7, v13, :cond_b

    .line 406
    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    .line 408
    .restart local v2    # "category":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    iget-object v13, v2, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 409
    const-string v13, "HeadlinesCardManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "updateNewsCardNamesList : prevCardId.equals("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v2, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    const/4 v3, 0x0

    .line 405
    :cond_a
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 414
    .end local v2    # "category":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    :cond_b
    const/4 v13, 0x1

    if-ne v3, v13, :cond_d

    .line 415
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardTypes:Ljava/util/List;

    invoke-interface {v13, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 416
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardprovider/CardManager;

    invoke-virtual {v13, v11}, Lcom/samsung/android/magazine/cardprovider/CardManager;->removeCard(Ljava/lang/String;)Z

    .line 417
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCurrentMap:Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;

    invoke-virtual {v13, v11}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->removeCardName(Ljava/lang/String;)V

    .line 420
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    if-eqz v13, :cond_c

    .line 421
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    invoke-interface {v13, v11}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 422
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v14, "mymagazineService"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 423
    .local v9, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 424
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v13, "NEWS_WELCOME_LIST"

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mNewsWelcomeList:Ljava/util/Set;

    invoke-interface {v4, v13, v14}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 425
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 427
    .end local v4    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v9    # "pref":Landroid/content/SharedPreferences;
    :cond_c
    const-string v13, "HeadlinesCardManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "updateNewsCardNamesList : unregisterCardName card name = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 429
    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardprovider/CardManager;

    invoke-virtual {v13, v11}, Lcom/samsung/android/magazine/cardprovider/CardManager;->containsCard(Ljava/lang/String;)Z

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_8

    .line 430
    const-string v13, "HeadlinesCardManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "updateNewsCardNamesList : This Category Card["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] is posted."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    new-instance v14, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v11}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;)V

    invoke-virtual {v13, v11, v14}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestUpdate(Ljava/lang/String;Ljava/util/Observer;)V

    goto/16 :goto_5
.end method
