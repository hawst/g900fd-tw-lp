.class public Lcom/samsung/android/internal/headlines/HeadlinesService;
.super Landroid/app/Service;
.source "HeadlinesService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;
    }
.end annotation


# static fields
.field static LoggingFeatureTable:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static volatile instance:Lcom/samsung/android/internal/headlines/HeadlinesService;

.field static status:Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;


# instance fields
.field private latestRequestTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->instance:Lcom/samsung/android/internal/headlines/HeadlinesService;

    .line 24
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->status:Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    .line 27
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "news"

    const-string v2, "NEWS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "tech"

    const-string v2, "TECH"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "new"

    const-string v2, "NEWW"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "sports"

    const-string v2, "SPOR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "business"

    const-string v2, "BUSI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "photos"

    const-string v2, "PHTO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "style"

    const-string v2, "STYL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "arts"

    const-string v2, "ARTS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "living"

    const-string v2, "LIVI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "music"

    const-string v2, "MUSC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "travel"

    const-string v2, "TRAV"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "by"

    const-string v2, "PICK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "books"

    const-string v2, "BOOK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "englishonly"

    const-string v2, "ENGL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "food"

    const-string v2, "FOOD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "netherlands"

    const-string v2, "NETH"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "firstlaunch"

    const-string v2, "UKUK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "twitter"

    const-string v2, "TWIT"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "googleplus"

    const-string v2, "GOGL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "linkedin"

    const-string v2, "LINK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "flickr"

    const-string v2, "FLIC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "tumblr"

    const-string v2, "TMBL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "fivehundredpx"

    const-string v2, "500X"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "weibo"

    const-string v2, "WEBO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "renren"

    const-string v2, "RENR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    const-string v1, "youtube"

    const-string v2, "YTUB"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 22
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesService;->latestRequestTime:J

    .line 23
    return-void
.end method

.method public static Log(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 176
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    sget-object v3, Lcom/samsung/android/internal/headlines/HeadlinesService;->status:Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;

    sget-object v4, Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;

    if-ne v3, v4, :cond_2

    .line 180
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 181
    sget-object v3, Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;->ON:Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;

    sput-object v3, Lcom/samsung/android/internal/headlines/HeadlinesService;->status:Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;

    .line 186
    :cond_2
    :goto_1
    sget-object v3, Lcom/samsung/android/internal/headlines/HeadlinesService;->status:Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;

    sget-object v4, Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;->ON:Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;

    if-ne v3, v4, :cond_0

    .line 187
    invoke-static {p1}, Lcom/samsung/android/internal/headlines/HeadlinesService;->getLogFeature(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 188
    .local v2, "feature":Ljava/lang/String;
    const-string v3, "Headlines"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Logging tap : category : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    if-eqz v2, :cond_0

    .line 192
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 193
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v3, "app_id"

    const-string v4, "com.samsung.android.app.headlines"

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v3, "feature"

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 198
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string v3, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    const-string v3, "data"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 201
    const-string v3, "com.samsung.android.providers.context"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 203
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 183
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v2    # "feature":Ljava/lang/String;
    :cond_3
    sget-object v3, Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;->OFF:Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;

    sput-object v3, Lcom/samsung/android/internal/headlines/HeadlinesService;->status:Lcom/samsung/android/internal/headlines/HeadlinesService$LogginStatus;

    goto :goto_1
.end method

.method private static getLogFeature(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "category"    # Ljava/lang/String;

    .prologue
    .line 208
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesService;->LoggingFeatureTable:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 212
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public breath()V
    .locals 6

    .prologue
    .line 76
    const-string v0, "Headlines"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Breath "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesService;->latestRequestTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " latestRequest time : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesService;->latestRequestTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " current time : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 172
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 59
    const-string v0, "Headlines"

    const-string v1, "Breath.onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    sput-object p0, Lcom/samsung/android/internal/headlines/HeadlinesService;->instance:Lcom/samsung/android/internal/headlines/HeadlinesService;

    .line 61
    invoke-static {p0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->create(Landroid/content/Context;)V

    .line 63
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesService;->latestRequestTime:J

    .line 64
    return-void
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 162
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 163
    const-string v3, "Headlines"

    const-string v4, "Breath.onDestroy : "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/samsung/android/internal/headlines/HeadlinesBroadcastReceiver;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 165
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Lcom/samsung/android/internal/headlines/HeadlinesService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 166
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-static {p0, v5, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 167
    .local v2, "sender":Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 168
    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 218
    const-string v0, "HeadlinesService"

    const-string v1, "HeadlinesService.onLowMemory()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/high16 v9, 0x7f090000

    const/4 v8, 0x1

    const/4 v3, 0x0

    const/16 v7, 0x12d

    .line 90
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 157
    .end local p1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return v8

    .line 93
    .restart local p1    # "intent":Landroid/content/Intent;
    :cond_1
    const-string v4, "Headlines"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Breath.onStartCommand : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " flag : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.samsung.android.internal.headlines.ALIVE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 96
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/HeadlinesService;->breath()V

    goto :goto_0

    .line 99
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesService;->latestRequestTime:J

    .line 101
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->updateCategory()V

    .line 102
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.samsung.android.internal.headlines.UPDATE_WELCOME_CARD"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 103
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    if-ne p2, v7, :cond_3

    move-object p1, v3

    .end local p1    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-virtual {v4, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestUpdateNewsWelcomeCard(Landroid/content/Intent;)V

    goto :goto_0

    .line 104
    .restart local p1    # "intent":Landroid/content/Intent;
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.samsung.android.internal.headlines.UPDATE_HEADLINES_CATEGORY_LIST"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 105
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v5

    if-ne p2, v7, :cond_5

    move-object v4, v3

    :goto_1
    invoke-virtual {v5, v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestUpdateSystemLanguage(Landroid/content/Intent;)V

    .line 106
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    if-ne p2, v7, :cond_6

    :goto_2
    invoke-virtual {v4, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestUpdateNewsWelcomeCard(Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    move-object v4, p1

    .line 105
    goto :goto_1

    :cond_6
    move-object v3, p1

    .line 106
    goto :goto_2

    .line 107
    :cond_7
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 108
    const-string v4, "Headlines"

    const-string v5, "HeadlinesConstants.ACTION_LOCALE_CHANGED"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    if-ne p2, v7, :cond_8

    :goto_3
    invoke-virtual {v4, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestUpdateSystemLanguage(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_8
    move-object v3, p1

    goto :goto_3

    .line 110
    :cond_9
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.samsung.android.internal.headlines.UPDATE_FEED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 111
    const-string v4, "category"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "category":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    if-ne p2, v7, :cond_a

    :goto_4
    invoke-virtual {v4, v0, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestUpdate(Ljava/lang/String;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_a
    move-object v3, p1

    goto :goto_4

    .line 113
    .end local v0    # "category":Ljava/lang/String;
    :cond_b
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.samsung.android.magazine.intent.action.CARD_NAME_SUBSCRIPTION_STATE_CHANGED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 114
    const-string v4, "com.samsung.android.magazine.intent.extra.CARD_NAME"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    .restart local v0    # "category":Ljava/lang/String;
    const-string v4, "com.samsung.android.magazine.intent.extra.CARD_NAME_SUBSCRIBED"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 116
    .local v1, "enable":Z
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    if-ne p2, v7, :cond_c

    :goto_5
    invoke-virtual {v4, v0, v1, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->updateCardSubscriptionState(Ljava/lang/String;ZLandroid/content/Intent;)V

    goto/16 :goto_0

    :cond_c
    move-object v3, p1

    goto :goto_5

    .line 117
    .end local v0    # "category":Ljava/lang/String;
    .end local v1    # "enable":Z
    :cond_d
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.samsung.android.magazine.intent.action.INITIALIZE_CARD_PROVIDER"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 118
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    if-ne p2, v7, :cond_e

    :goto_6
    invoke-virtual {v4, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestUpdateNewsWelcomeCard(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_e
    move-object v3, p1

    goto :goto_6

    .line 119
    :cond_f
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.samsung.android.magazine.intent.action.CARD_REFRESH"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 120
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->isNetworkavailable()Z

    move-result v4

    if-nez v4, :cond_10

    .line 121
    invoke-virtual {p0, v9}, Lcom/samsung/android/internal/headlines/HeadlinesService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 122
    .local v2, "text":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->showToast(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 125
    .end local v2    # "text":Ljava/lang/String;
    :cond_10
    const-string v4, "com.samsung.android.magazine.intent.extra.CARD_ID"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    .restart local v0    # "category":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    if-ne p2, v7, :cond_11

    :goto_7
    invoke-virtual {v4, v0, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestUpdate(Ljava/lang/String;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_11
    move-object v3, p1

    goto :goto_7

    .line 127
    .end local v0    # "category":Ljava/lang/String;
    :cond_12
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.samsung.android.internal.headlines.LOGIN_SOCIAL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 128
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->isNetworkavailable()Z

    move-result v4

    if-nez v4, :cond_13

    .line 129
    invoke-virtual {p0, v9}, Lcom/samsung/android/internal/headlines/HeadlinesService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 130
    .restart local v2    # "text":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->showToast(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 133
    .end local v2    # "text":Ljava/lang/String;
    :cond_13
    const-string v4, "category"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    .restart local v0    # "category":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    if-ne p2, v7, :cond_14

    :goto_8
    invoke-virtual {v4, v0, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestServiceLogin(Ljava/lang/String;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_14
    move-object v3, p1

    goto :goto_8

    .line 135
    .end local v0    # "category":Ljava/lang/String;
    :cond_15
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.samsung.android.internal.headlines.LOGOUT_SOCIAL_INTERNAL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 136
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->isNetworkavailable()Z

    move-result v4

    if-nez v4, :cond_16

    .line 137
    invoke-virtual {p0, v9}, Lcom/samsung/android/internal/headlines/HeadlinesService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 138
    .restart local v2    # "text":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->showToast(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 141
    .end local v2    # "text":Ljava/lang/String;
    :cond_16
    const-string v4, "category"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 142
    .restart local v0    # "category":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    if-ne p2, v7, :cond_17

    :goto_9
    invoke-virtual {v4, v0, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestServiceLogout(Ljava/lang/String;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_17
    move-object v3, p1

    goto :goto_9

    .line 143
    .end local v0    # "category":Ljava/lang/String;
    :cond_18
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.samsung.android.internal.headlines.LAUNCH_FLIPBOARD_APP_FOR_SOCIAL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 144
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->isNetworkavailable()Z

    move-result v4

    if-nez v4, :cond_19

    .line 145
    invoke-virtual {p0, v9}, Lcom/samsung/android/internal/headlines/HeadlinesService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 146
    .restart local v2    # "text":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->showToast(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 149
    .end local v2    # "text":Ljava/lang/String;
    :cond_19
    const-string v4, "category"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 150
    .restart local v0    # "category":Ljava/lang/String;
    const-string v4, "CN"

    invoke-static {v4}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v4

    if-ne v4, v8, :cond_1a

    .line 151
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v3

    invoke-virtual {v3, v0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestSocialAction(Ljava/lang/String;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 153
    :cond_1a
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    if-ne p2, v7, :cond_1b

    :goto_a
    invoke-virtual {v4, v0, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestSocialAction(Ljava/lang/String;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_1b
    move-object v3, p1

    goto :goto_a

    .line 154
    .end local v0    # "category":Ljava/lang/String;
    :cond_1c
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.samsung.android.magazine.intent.action.DB_MIGRATION"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 155
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestMigrationCard(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 223
    const-string v0, "HeadlinesService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeadlinesService.onTrimMemory(). level : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    return-void
.end method

.method public setAlarm(I)V
    .locals 0
    .param p1, "ms"    # I

    .prologue
    .line 73
    return-void
.end method
