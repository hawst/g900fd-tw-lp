.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$2;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestUpdate(Ljava/lang/String;Ljava/util/Observer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

.field final synthetic val$category:Ljava/lang/String;

.field final synthetic val$observer:Ljava/util/Observer;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/lang/String;Ljava/util/Observer;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$2;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$2;->val$category:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$2;->val$observer:Ljava/util/Observer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 7
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 190
    if-eqz p2, :cond_0

    instance-of v4, p2, Lorg/json/JSONObject;

    if-eqz v4, :cond_0

    .line 192
    :try_start_0
    check-cast p2, Lorg/json/JSONObject;

    .end local p2    # "arg1":Ljava/lang/Object;
    const-string v4, "items"

    invoke-virtual {p2, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 193
    .local v2, "items":Lorg/json/JSONArray;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 206
    .end local v2    # "items":Lorg/json/JSONArray;
    :cond_0
    :goto_0
    return-void

    .line 195
    .restart local v2    # "items":Lorg/json/JSONArray;
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 196
    .local v3, "json":Lorg/json/JSONObject;
    if-eqz v3, :cond_0

    .line 197
    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesItem;

    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$2;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$100(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$2;->val$category:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$2;->val$category:Ljava/lang/String;

    invoke-direct {v1, v4, v5, v6, v3}, Lcom/samsung/android/internal/headlines/HeadlinesItem;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 198
    .local v1, "item":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    const-string v4, "Headlines"

    const-string v5, "[requestUpdate] HeadlinesItem is created successful."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$2;->val$observer:Ljava/util/Observer;

    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 201
    .end local v1    # "item":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    .end local v2    # "items":Lorg/json/JSONArray;
    .end local v3    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
