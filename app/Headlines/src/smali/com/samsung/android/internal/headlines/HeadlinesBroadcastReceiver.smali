.class public Lcom/samsung/android/internal/headlines/HeadlinesBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HeadlinesBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 21
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    if-nez v14, :cond_8

    .line 23
    const-string v14, "Headlines"

    const-string v15, "HeadlinesBroadcastReceiver onReceive "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    :goto_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.samsung.android.internal.headlines.UPDATE_WELCOME_CARD"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 38
    new-instance v12, Landroid/content/Intent;

    const-class v14, Lcom/samsung/android/internal/headlines/HeadlinesService;

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 39
    .local v12, "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.internal.headlines.UPDATE_WELCOME_CARD"

    invoke-virtual {v12, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 40
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 43
    .end local v12    # "sIntent":Landroid/content/Intent;
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_1

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.samsung.android.internal.headlines.UPDATE_FEED"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 45
    new-instance v12, Landroid/content/Intent;

    const-class v14, Lcom/samsung/android/internal/headlines/HeadlinesService;

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 46
    .restart local v12    # "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.internal.headlines.UPDATE_FEED"

    invoke-virtual {v12, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    const-string v14, "category"

    const-string v15, "category"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 48
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 51
    .end local v12    # "sIntent":Landroid/content/Intent;
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_2

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.samsung.android.internal.headlines.LOGIN_SOCIAL"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 53
    new-instance v12, Landroid/content/Intent;

    const-class v14, Lcom/samsung/android/internal/headlines/HeadlinesService;

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 54
    .restart local v12    # "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.internal.headlines.LOGIN_SOCIAL"

    invoke-virtual {v12, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    const-string v14, "category"

    const-string v15, "category"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 59
    .end local v12    # "sIntent":Landroid/content/Intent;
    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_3

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.samsung.android.internal.headlines.LOGOUT_SOCIAL_INTERNAL"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 61
    const-string v14, "msg"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_3

    const-string v14, "msg"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "YES"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 63
    new-instance v12, Landroid/content/Intent;

    const-class v14, Lcom/samsung/android/internal/headlines/HeadlinesService;

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    .restart local v12    # "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.internal.headlines.LOGOUT_SOCIAL_INTERNAL"

    invoke-virtual {v12, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    const-string v14, "category"

    const-string v15, "category"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    const-string v14, "msg"

    const-string v15, "YES"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 71
    .end local v12    # "sIntent":Landroid/content/Intent;
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_4

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.samsung.android.internal.headlines.LOGOUT_SOCIAL"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 73
    new-instance v12, Landroid/content/Intent;

    const-string v14, "android.intent.action.MAIN"

    invoke-direct {v12, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 74
    .restart local v12    # "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.app.headlines"

    const-string v15, "com.samsung.android.internal.headlines.DialogActivity"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    const-string v14, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE"

    const-string v15, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_SOCIAL_LOGOUT"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    const-string v14, "category"

    const-string v15, "category"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    const/high16 v14, 0x10000000

    invoke-virtual {v12, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 78
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 81
    .end local v12    # "sIntent":Landroid/content/Intent;
    :cond_4
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_5

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.samsung.android.internal.headlines.LAUNCH_FLIPBOARD_APP_FOR_NEWS"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 83
    const-string v14, "link"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 85
    .local v9, "link":Ljava/lang/String;
    :try_start_0
    const-string v14, "CN"

    invoke-static {v14}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_9

    const-string v5, "flipboard.cn"

    .line 87
    .local v5, "flipboardPackage":Ljava/lang/String;
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v14, v5, v15}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 88
    .local v6, "info":Landroid/content/pm/PackageInfo;
    iget-object v14, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v14, v14, Landroid/content/pm/ApplicationInfo;->enabled:Z

    const/4 v15, 0x1

    if-ne v14, v15, :cond_a

    .line 89
    const-string v14, "HeadlinesBroadcastReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onReceive() : ACTION_LAUNCH_FLIPBOARD_APP_FOR_NEWS. link = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    new-instance v10, Landroid/content/Intent;

    const-string v14, "android.intent.action.VIEW"

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-direct {v10, v14, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 91
    .local v10, "linkIntent":Landroid/content/Intent;
    const-string v14, "category"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "category":Ljava/lang/String;
    const-string v14, "displayName"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 93
    .local v2, "displayName":Ljava/lang/String;
    const-string v14, "jsonObject"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 94
    .local v13, "stringJsonObject":Ljava/lang/String;
    if-eqz v13, :cond_5

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v14

    if-nez v14, :cond_5

    .line 95
    const/4 v7, 0x0

    .line 97
    .local v7, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, v13}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .local v8, "jsonObject":Lorg/json/JSONObject;
    move-object v7, v8

    .line 102
    .end local v8    # "jsonObject":Lorg/json/JSONObject;
    .restart local v7    # "jsonObject":Lorg/json/JSONObject;
    :goto_2
    const/4 v14, 0x0

    :try_start_2
    move-object/from16 v0, p1

    invoke-static {v0, v7, v14, v2}, Lcom/flipboard/util/IntentUtil;->getDetailIntent(Landroid/content/Context;Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    .line 103
    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesService;->Log(Landroid/content/Context;Ljava/lang/String;)V

    .line 104
    const/high16 v14, 0x10000000

    invoke-virtual {v10, v14}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 105
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    .line 129
    .end local v1    # "category":Ljava/lang/String;
    .end local v2    # "displayName":Ljava/lang/String;
    .end local v5    # "flipboardPackage":Ljava/lang/String;
    .end local v6    # "info":Landroid/content/pm/PackageInfo;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .end local v9    # "link":Ljava/lang/String;
    .end local v10    # "linkIntent":Landroid/content/Intent;
    .end local v13    # "stringJsonObject":Ljava/lang/String;
    :cond_5
    :goto_3
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_6

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.samsung.android.internal.headlines.LAUNCH_FLIPBOARD_APP_FOR_SOCIAL"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 131
    const-string v14, "link"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 133
    .restart local v9    # "link":Ljava/lang/String;
    :try_start_3
    const-string v14, "CN"

    invoke-static {v14}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_b

    const-string v5, "flipboard.cn"

    .line 134
    .restart local v5    # "flipboardPackage":Ljava/lang/String;
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v14, v5, v15}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 135
    .restart local v6    # "info":Landroid/content/pm/PackageInfo;
    iget-object v14, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v14, v14, Landroid/content/pm/ApplicationInfo;->enabled:Z

    const/4 v15, 0x1

    if-ne v14, v15, :cond_c

    .line 136
    new-instance v12, Landroid/content/Intent;

    const-class v14, Lcom/samsung/android/internal/headlines/HeadlinesService;

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 137
    .restart local v12    # "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.internal.headlines.LAUNCH_FLIPBOARD_APP_FOR_SOCIAL"

    invoke-virtual {v12, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    const-string v14, "link"

    invoke-virtual {v12, v14, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    const-string v14, "category"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 140
    .restart local v1    # "category":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesService;->Log(Landroid/content/Context;Ljava/lang/String;)V

    .line 141
    const-string v14, "category"

    invoke-virtual {v12, v14, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    const-string v14, "displayName"

    const-string v15, "displayName"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    const-string v14, "jsonObject"

    const-string v15, "jsonObject"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_2

    .line 169
    .end local v1    # "category":Ljava/lang/String;
    .end local v5    # "flipboardPackage":Ljava/lang/String;
    .end local v6    # "info":Landroid/content/pm/PackageInfo;
    .end local v9    # "link":Ljava/lang/String;
    .end local v12    # "sIntent":Landroid/content/Intent;
    :cond_6
    :goto_5
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_7

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.samsung.android.internal.headlines.ALLOW_DATA_USE"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 170
    const-string v14, "mymagazineService"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 172
    .local v11, "pref":Landroid/content/SharedPreferences;
    const-string v14, "DATA_ALLOWED"

    const/4 v15, 0x0

    invoke-interface {v11, v14, v15}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-nez v14, :cond_7

    .line 173
    invoke-interface {v11}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 174
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v14, "DATA_ALLOWED"

    const/4 v15, 0x1

    invoke-interface {v4, v14, v15}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 175
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 178
    .end local v4    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v11    # "pref":Landroid/content/SharedPreferences;
    :cond_7
    return-void

    .line 27
    :cond_8
    const-string v14, "Headlines"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "HeadlinesBroadcastReceiver onReceive "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 85
    .restart local v9    # "link":Ljava/lang/String;
    :cond_9
    :try_start_4
    const-string v5, "flipboard.app"

    goto/16 :goto_1

    .line 98
    .restart local v1    # "category":Ljava/lang/String;
    .restart local v2    # "displayName":Ljava/lang/String;
    .restart local v5    # "flipboardPackage":Ljava/lang/String;
    .restart local v6    # "info":Landroid/content/pm/PackageInfo;
    .restart local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v10    # "linkIntent":Landroid/content/Intent;
    .restart local v13    # "stringJsonObject":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 100
    .local v3, "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_2

    .line 119
    .end local v1    # "category":Ljava/lang/String;
    .end local v2    # "displayName":Ljava/lang/String;
    .end local v3    # "e":Lorg/json/JSONException;
    .end local v5    # "flipboardPackage":Ljava/lang/String;
    .end local v6    # "info":Landroid/content/pm/PackageInfo;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .end local v10    # "linkIntent":Landroid/content/Intent;
    .end local v13    # "stringJsonObject":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 120
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v14, "Headlines"

    const-string v15, "Flipboard app is not installed."

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    new-instance v12, Landroid/content/Intent;

    const-string v14, "android.intent.action.MAIN"

    invoke-direct {v12, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 123
    .restart local v12    # "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.app.headlines"

    const-string v15, "com.samsung.android.internal.headlines.DialogActivity"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    const-string v14, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE"

    const-string v15, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_OPEN_MARKET"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 125
    const/high16 v14, 0x10000000

    invoke-virtual {v12, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 126
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 109
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v12    # "sIntent":Landroid/content/Intent;
    .restart local v5    # "flipboardPackage":Ljava/lang/String;
    .restart local v6    # "info":Landroid/content/pm/PackageInfo;
    :cond_a
    :try_start_5
    new-instance v12, Landroid/content/Intent;

    const-string v14, "android.intent.action.MAIN"

    invoke-direct {v12, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 110
    .restart local v12    # "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.app.headlines"

    const-string v15, "com.samsung.android.internal.headlines.DialogActivity"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const-string v14, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE"

    const-string v15, "com.samsung.android.magazine.intent.extra.EXTRA_DIALOG_TYPE_OPEN_APPINFO"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    const-string v14, "appname"

    const-string v15, "Flipboard"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const-string v14, "packagename"

    invoke-virtual {v12, v14, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const/high16 v14, 0x10000000

    invoke-virtual {v12, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 115
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_3

    .line 133
    .end local v5    # "flipboardPackage":Ljava/lang/String;
    .end local v6    # "info":Landroid/content/pm/PackageInfo;
    .end local v12    # "sIntent":Landroid/content/Intent;
    :cond_b
    :try_start_6
    const-string v5, "flipboard.app"

    goto/16 :goto_4

    .line 147
    .restart local v5    # "flipboardPackage":Ljava/lang/String;
    .restart local v6    # "info":Landroid/content/pm/PackageInfo;
    :cond_c
    new-instance v12, Landroid/content/Intent;

    const-string v14, "android.intent.action.MAIN"

    invoke-direct {v12, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 148
    .restart local v12    # "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.app.headlines"

    const-string v15, "com.samsung.android.internal.headlines.DialogActivity"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    const-string v14, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE"

    const-string v15, "com.samsung.android.magazine.intent.extra.EXTRA_DIALOG_TYPE_OPEN_APPINFO"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    const-string v14, "appname"

    const-string v15, "Flipboard"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    const-string v14, "packagename"

    invoke-virtual {v12, v14, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    const/high16 v14, 0x10000000

    invoke-virtual {v12, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 153
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_5

    .line 157
    .end local v5    # "flipboardPackage":Ljava/lang/String;
    .end local v6    # "info":Landroid/content/pm/PackageInfo;
    .end local v12    # "sIntent":Landroid/content/Intent;
    :catch_2
    move-exception v3

    .line 158
    .restart local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v14, "Headlines"

    const-string v15, "Flipboard app is not installed."

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    new-instance v12, Landroid/content/Intent;

    const-string v14, "android.intent.action.MAIN"

    invoke-direct {v12, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 161
    .restart local v12    # "sIntent":Landroid/content/Intent;
    const-string v14, "com.samsung.android.app.headlines"

    const-string v15, "com.samsung.android.internal.headlines.DialogActivity"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    const-string v14, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE"

    const-string v15, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_OPEN_MARKET"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 163
    const/high16 v14, 0x10000000

    invoke-virtual {v12, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 164
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_5
.end method
