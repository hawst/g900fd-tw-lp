.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11$1;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;->update(Ljava/util/Observable;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 511
    if-eqz p2, :cond_0

    instance-of v0, p2, Ljava/util/List;

    if-nez v0, :cond_1

    .line 523
    .end local p2    # "arg1":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 513
    .restart local p2    # "arg1":Ljava/lang/Object;
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    check-cast p2, Ljava/util/List;

    .end local p2    # "arg1":Ljava/lang/Object;
    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mNewsList:Ljava/util/List;
    invoke-static {v0, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$602(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/List;)Ljava/util/List;

    .line 515
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mNewsList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$600(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 516
    const-string v0, "Headlines"

    const-string v1, "[requestUpdateSystemLanguage] updated no category list."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 520
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mHeadlinesCardManager:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$400(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;

    iget-object v1, v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mNewsList:Ljava/util/List;
    invoke-static {v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$600(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->updateNewsCardNamesList(Ljava/util/List;)V

    .line 522
    const-string v0, "Headlines"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[requestUpdateSystemLanguage] system locale is changed. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$800()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;

    iget-object v2, v2, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;->val$current:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
