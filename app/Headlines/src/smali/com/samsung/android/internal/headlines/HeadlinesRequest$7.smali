.class Lcom/samsung/android/internal/headlines/HeadlinesRequest$7;
.super Lcom/flipboard/data/Request$FLSuccessListener;
.source "HeadlinesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesRequest;->serviceLogin(Ljava/lang/String;Ljava/util/Observer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

.field final synthetic val$observer:Ljava/util/Observer;

.field final synthetic val$socialId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/lang/String;Ljava/util/Observer;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$7;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$7;->val$socialId:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$7;->val$observer:Ljava/util/Observer;

    invoke-direct {p0}, Lcom/flipboard/data/Request$FLSuccessListener;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 297
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$7;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "response"    # Lorg/json/JSONObject;

    .prologue
    .line 300
    const-string v0, "HeadlinesRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "it is succeeded to log on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$7;->val$socialId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$7;->val$observer:Ljava/util/Observer;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$000()Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 302
    return-void
.end method
