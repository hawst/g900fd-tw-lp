.class public Lcom/samsung/android/internal/headlines/ServiceLoginActivity;
.super Landroid/app/Activity;
.source "ServiceLoginActivity.java"

# interfaces
.implements Lcom/flipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/internal/headlines/ServiceLoginActivity$2;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Activity;",
        "Lcom/flipboard/util/Observer",
        "<",
        "Lcom/flipboard/data/AuthorizationClient;",
        "Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;",
        "Lcom/flipboard/data/AuthorizationClient$Response;",
        ">;"
    }
.end annotation


# instance fields
.field private mClient:Lcom/flipboard/data/AuthorizationClient;

.field private mImageView:Landroid/widget/ImageView;

.field private mLoadingView:Landroid/widget/RelativeLayout;

.field private mTextView:Landroid/widget/TextView;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mWebView:Landroid/webkit/WebView;

    .line 25
    iput-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mImageView:Landroid/widget/ImageView;

    .line 26
    iput-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mTextView:Landroid/widget/TextView;

    .line 27
    iput-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mLoadingView:Landroid/widget/RelativeLayout;

    .line 28
    iput-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mClient:Lcom/flipboard/data/AuthorizationClient;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/internal/headlines/ServiceLoginActivity;)Landroid/webkit/WebView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/ServiceLoginActivity;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/internal/headlines/ServiceLoginActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/ServiceLoginActivity;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mLoadingView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/internal/headlines/ServiceLoginActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/ServiceLoginActivity;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/internal/headlines/ServiceLoginActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/ServiceLoginActivity;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public notify(Lcom/flipboard/data/AuthorizationClient;Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;Lcom/flipboard/data/AuthorizationClient$Response;)V
    .locals 4
    .param p1, "observable"    # Lcom/flipboard/data/AuthorizationClient;
    .param p2, "msg"    # Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;
    .param p3, "response"    # Lcom/flipboard/data/AuthorizationClient$Response;

    .prologue
    const/4 v3, 0x0

    .line 93
    sget-object v0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity$2;->$SwitchMap$com$flipboard$data$AuthorizationClient$AuthorizeMessage:[I

    invoke-virtual {p2}, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 101
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "Social login window : unsupported message"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    :goto_0
    invoke-virtual {p1, p0}, Lcom/flipboard/data/AuthorizationClient;->removeObserver(Lcom/flipboard/util/Observer;)V

    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->finish()V

    .line 107
    return-void

    .line 95
    :pswitch_0
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "Social login window : success message"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 98
    :pswitch_1
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "Social login window : error message: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic notify(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # Ljava/lang/Object;

    .prologue
    .line 22
    check-cast p1, Lcom/flipboard/data/AuthorizationClient;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;

    .end local p2    # "x1":Ljava/lang/Object;
    check-cast p3, Lcom/flipboard/data/AuthorizationClient$Response;

    .end local p3    # "x2":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->notify(Lcom/flipboard/data/AuthorizationClient;Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;Lcom/flipboard/data/AuthorizationClient$Response;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mClient:Lcom/flipboard/data/AuthorizationClient;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mClient:Lcom/flipboard/data/AuthorizationClient;

    invoke-virtual {v0, p0}, Lcom/flipboard/data/AuthorizationClient;->removeObserver(Lcom/flipboard/util/Observer;)V

    .line 117
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "Social login window : back key pressed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 120
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x1

    .line 31
    const-string v1, "Headlines"

    const-string v2, "ServiceLoginActivity onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    .line 36
    .local v7, "extras":Landroid/os/Bundle;
    const-string v1, "com.flipboard.fdl.URL"

    invoke-virtual {v7, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 38
    .local v8, "url":Ljava/lang/String;
    const v1, 0x7f03000f

    invoke-virtual {p0, v1}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->setContentView(I)V

    .line 40
    const v1, 0x7f0c002b

    invoke-virtual {p0, v1}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    iput-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mWebView:Landroid/webkit/WebView;

    .line 43
    invoke-static {}, Lcom/flipboard/data/AuthorizationClient;->getInstance()Lcom/flipboard/data/AuthorizationClient;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mClient:Lcom/flipboard/data/AuthorizationClient;

    .line 45
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mClient:Lcom/flipboard/data/AuthorizationClient;

    if-nez v1, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->finish()V

    .line 87
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mClient:Lcom/flipboard/data/AuthorizationClient;

    invoke-virtual {v1, p0}, Lcom/flipboard/data/AuthorizationClient;->addObserver(Lcom/flipboard/util/Observer;)V

    .line 54
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mWebView:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mClient:Lcom/flipboard/data/AuthorizationClient;

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 55
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 56
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 57
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/samsung/android/internal/headlines/ServiceLoginActivity$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity$1;-><init>(Lcom/samsung/android/internal/headlines/ServiceLoginActivity;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 75
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v8}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 76
    const v1, 0x7f0c002e

    invoke-virtual {p0, v1}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mTextView:Landroid/widget/TextView;

    .line 77
    const v1, 0x7f0c002d

    invoke-virtual {p0, v1}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mImageView:Landroid/widget/ImageView;

    .line 78
    const v1, 0x7f0c002c

    invoke-virtual {p0, v1}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mLoadingView:Landroid/widget/RelativeLayout;

    .line 79
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43b40000    # 360.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 82
    .local v0, "rotateAnimation":Landroid/view/animation/RotateAnimation;
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 83
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 84
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 85
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method
