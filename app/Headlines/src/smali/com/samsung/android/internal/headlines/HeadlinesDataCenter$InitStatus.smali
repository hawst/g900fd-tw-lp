.class final enum Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
.super Ljava/lang/Enum;
.source "HeadlinesDataCenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "InitStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

.field public static final enum ADD_OBSERVER:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

.field public static final enum CANCEL:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

.field public static final enum DOING:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

.field public static final enum DONE:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

.field public static final enum INIT:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    const-string v1, "DOING"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->DOING:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->DONE:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    const-string v1, "ADD_OBSERVER"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->ADD_OBSERVER:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    const-string v1, "CANCEL"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->CANCEL:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    .line 26
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->DOING:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->DONE:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->ADD_OBSERVER:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->CANCEL:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->$VALUES:[Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->$VALUES:[Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    invoke-virtual {v0}, [Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    return-object v0
.end method
