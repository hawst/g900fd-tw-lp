.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$9;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->serviceLogin(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

.field final synthetic val$observer:Ljava/util/Observer;

.field final synthetic val$socialId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/lang/String;Ljava/util/Observer;)V
    .locals 0

    .prologue
    .line 439
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$9;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$9;->val$socialId:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$9;->val$observer:Ljava/util/Observer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 443
    if-eqz p2, :cond_0

    instance-of v0, p2, Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 451
    :cond_0
    :goto_0
    return-void

    .line 445
    :cond_1
    const-string v0, "Headlines"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "response of requestServiceLogin socialId Type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$9;->val$socialId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 446
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 447
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$9;->val$observer:Ljava/util/Observer;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->instance:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$200()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    goto :goto_0

    .line 450
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$9;->val$observer:Ljava/util/Observer;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->instance:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$200()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    goto :goto_0
.end method
