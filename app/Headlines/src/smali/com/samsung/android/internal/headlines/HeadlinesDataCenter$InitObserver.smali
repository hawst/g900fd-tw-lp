.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;


# direct methods
.method private constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)V
    .locals 0

    .prologue
    .line 618
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    .param p2, "x1"    # Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$1;

    .prologue
    .line 618
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 621
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "response from mRequest.addObserver : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$900(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->CANCEL:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    if-ne v0, v1, :cond_0

    .line 623
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HeadlinesDataCenter was already initialized. Here is init observer."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    :goto_0
    return-void

    .line 626
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$900(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->DONE:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    if-ne v0, v1, :cond_1

    .line 627
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HeadlinesDataCenter was already initialized. Here is init observer."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 630
    :cond_1
    if-nez p2, :cond_2

    .line 631
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HeadlinesDataCenter was failed to initialize . Here is init observer. cause : unknown error."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    invoke-static {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$902(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    goto :goto_0

    .line 635
    :cond_2
    if-eqz p2, :cond_4

    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 636
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 637
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver$1;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->queryCategory(Ljava/util/Observer;Z)V

    goto :goto_0

    .line 665
    :cond_3
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HeadlinesDataCenter was failed. "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    invoke-static {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$902(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    goto :goto_0

    .line 669
    :cond_4
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HeadlinesDataCenter was failed to initialize cause : FDL error"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    invoke-static {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$902(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    goto :goto_0
.end method
