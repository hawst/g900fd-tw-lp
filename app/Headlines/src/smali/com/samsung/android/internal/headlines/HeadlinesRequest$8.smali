.class Lcom/samsung/android/internal/headlines/HeadlinesRequest$8;
.super Lcom/flipboard/data/Request$FLErrorListener;
.source "HeadlinesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesRequest;->serviceLogin(Ljava/lang/String;Ljava/util/Observer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

.field final synthetic val$socialId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$8;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$8;->val$socialId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/flipboard/data/Request$FLErrorListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 3
    .param p1, "arg0"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 307
    const-string v0, "HeadlinesRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "it is failed to log on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$8;->val$socialId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$200()Landroid/widget/Toast;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 309
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$200()Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 310
    const/4 v0, 0x0

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$202(Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 312
    :cond_0
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$300()Landroid/content/Context;

    move-result-object v0

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$300()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$202(Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 313
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$200()Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 315
    return-void
.end method
