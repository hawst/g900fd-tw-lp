.class public Lcom/samsung/android/internal/headlines/DialogActivity;
.super Landroid/app/Activity;
.source "DialogActivity.java"


# static fields
.field private static final DIALOG_SHOW_TIME:I = 0x3e8

.field static instance:Lcom/samsung/android/internal/headlines/DialogActivity;

.field private static mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    sput-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    .line 27
    sput-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->instance:Lcom/samsung/android/internal/headlines/DialogActivity;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/internal/headlines/DialogActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/DialogActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/DialogActivity;->dialogAppIsNotInstalled(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/internal/headlines/DialogActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/DialogActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/internal/headlines/DialogActivity;->dialogOpenAppInfo(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private dialogAppIsNotInstalled(Ljava/lang/String;)V
    .locals 4
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 113
    sget-object v2, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    if-eqz v2, :cond_0

    .line 114
    sget-object v2, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->cancel()V

    .line 115
    const/4 v2, 0x0

    sput-object v2, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    .line 117
    :cond_0
    const v2, 0x7f0a0005

    invoke-virtual {p0, v2}, Lcom/samsung/android/internal/headlines/DialogActivity;->setTheme(I)V

    .line 119
    const v2, 0x7f090063

    invoke-virtual {p0, v2}, Lcom/samsung/android/internal/headlines/DialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "isNotInstalled":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 121
    .local v1, "text":Ljava/lang/String;
    const/16 v2, 0x3e8

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    sput-object v2, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    .line 122
    sget-object v2, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 123
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->finish()V

    .line 124
    return-void
.end method

.method private dialogNetworkNotAvailable()V
    .locals 2

    .prologue
    .line 80
    sget-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 81
    sget-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 82
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    .line 84
    :cond_0
    const v0, 0x7f0a0005

    invoke-virtual {p0, v0}, Lcom/samsung/android/internal/headlines/DialogActivity;->setTheme(I)V

    .line 85
    const-string v0, "Network not available. Try later."

    const/16 v1, 0x3e8

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    .line 86
    sget-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 87
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->finish()V

    .line 88
    return-void
.end method

.method private dialogNoMoreContent()V
    .locals 2

    .prologue
    .line 102
    sget-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 103
    sget-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 104
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    .line 106
    :cond_0
    const v0, 0x7f0a0005

    invoke-virtual {p0, v0}, Lcom/samsung/android/internal/headlines/DialogActivity;->setTheme(I)V

    .line 107
    const-string v0, "No more content"

    const/16 v1, 0x3e8

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    .line 108
    sget-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 109
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->finish()V

    .line 110
    return-void
.end method

.method private dialogOpenAppInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 245
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 247
    .local v0, "OpenAppInfoDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 249
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 250
    .local v2, "unable_to_open_this_application":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 251
    .local v1, "text":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 254
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/internal/headlines/DialogActivity$7;

    invoke-direct {v4, p0, p2}, Lcom/samsung/android/internal/headlines/DialogActivity$7;-><init>(Lcom/samsung/android/internal/headlines/DialogActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 269
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x104000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/internal/headlines/DialogActivity$8;

    invoke-direct {v4, p0}, Lcom/samsung/android/internal/headlines/DialogActivity$8;-><init>(Lcom/samsung/android/internal/headlines/DialogActivity;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 279
    new-instance v3, Lcom/samsung/android/internal/headlines/DialogActivity$9;

    invoke-direct {v3, p0}, Lcom/samsung/android/internal/headlines/DialogActivity$9;-><init>(Lcom/samsung/android/internal/headlines/DialogActivity;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 287
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 288
    return-void
.end method

.method private dialogOpenMarket()V
    .locals 6

    .prologue
    .line 172
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 174
    .local v0, "OpenMarketDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 176
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090005

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 177
    .local v3, "to_use_this_function":Ljava/lang/String;
    const-string v1, "Flipboard"

    .line 178
    .local v1, "flipboard":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 180
    .local v2, "text":Ljava/lang/String;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 181
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x1040000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/samsung/android/internal/headlines/DialogActivity$4;

    invoke-direct {v5, p0}, Lcom/samsung/android/internal/headlines/DialogActivity$4;-><init>(Lcom/samsung/android/internal/headlines/DialogActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 190
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090003

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/samsung/android/internal/headlines/DialogActivity$5;

    invoke-direct {v5, p0}, Lcom/samsung/android/internal/headlines/DialogActivity$5;-><init>(Lcom/samsung/android/internal/headlines/DialogActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 233
    new-instance v4, Lcom/samsung/android/internal/headlines/DialogActivity$6;

    invoke-direct {v4, p0}, Lcom/samsung/android/internal/headlines/DialogActivity$6;-><init>(Lcom/samsung/android/internal/headlines/DialogActivity;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 241
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 242
    return-void
.end method

.method private dialogServerError()V
    .locals 2

    .prologue
    .line 91
    sget-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 92
    sget-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 93
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    .line 95
    :cond_0
    const v0, 0x7f0a0005

    invoke-virtual {p0, v0}, Lcom/samsung/android/internal/headlines/DialogActivity;->setTheme(I)V

    .line 96
    const-string v0, "Server error occurred. Try later."

    const/16 v1, 0x3e8

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    .line 97
    sget-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 98
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->finish()V

    .line 99
    return-void
.end method

.method private dialogSocialLogout(Ljava/lang/String;)V
    .locals 3
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 128
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 130
    .local v0, "socialLogoutDialog":Landroid/app/AlertDialog$Builder;
    const-string v1, "Sign out"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 131
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sign out of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 133
    const-string v1, "Sign out"

    new-instance v2, Lcom/samsung/android/internal/headlines/DialogActivity$1;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/internal/headlines/DialogActivity$1;-><init>(Lcom/samsung/android/internal/headlines/DialogActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 148
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/internal/headlines/DialogActivity$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/internal/headlines/DialogActivity$2;-><init>(Lcom/samsung/android/internal/headlines/DialogActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 158
    new-instance v1, Lcom/samsung/android/internal/headlines/DialogActivity$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/internal/headlines/DialogActivity$3;-><init>(Lcom/samsung/android/internal/headlines/DialogActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 166
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 167
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/internal/headlines/DialogActivity;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->instance:Lcom/samsung/android/internal/headlines/DialogActivity;

    return-object v0
.end method

.method private showLoadingPopup()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    .line 67
    const v1, 0x7f03000c

    invoke-virtual {p0, v1}, Lcom/samsung/android/internal/headlines/DialogActivity;->setContentView(I)V

    .line 68
    const v1, 0x7f0c0015

    invoke-virtual {p0, v1}, Lcom/samsung/android/internal/headlines/DialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 70
    .local v7, "progress":Landroid/widget/ImageView;
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43b40000    # 360.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 73
    .local v0, "rotateAnimation1":Landroid/view/animation/RotateAnimation;
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 74
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 75
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 76
    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 77
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    sput-object p0, Lcom/samsung/android/internal/headlines/DialogActivity;->instance:Lcom/samsung/android/internal/headlines/DialogActivity;

    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-virtual {v4, v5, v6}, Landroid/view/Window;->setLayout(II)V

    .line 38
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 40
    .local v2, "dialogType":Ljava/lang/String;
    const-string v4, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_NETWORK_NOT_AVAILABLE"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 41
    invoke-direct {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->dialogNetworkNotAvailable()V

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    const-string v4, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_SOCIAL_LOGOUT"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 44
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "category"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 45
    .local v1, "category":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/samsung/android/internal/headlines/DialogActivity;->dialogSocialLogout(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    .end local v1    # "category":Ljava/lang/String;
    :cond_2
    const-string v4, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_SERVER_ERROR"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 48
    invoke-direct {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->dialogServerError()V

    goto :goto_0

    .line 50
    :cond_3
    const-string v4, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_NO_MORE_CONTENT"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 51
    invoke-direct {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->dialogNoMoreContent()V

    goto :goto_0

    .line 53
    :cond_4
    const-string v4, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_OPEN_MARKET"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 54
    invoke-direct {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->dialogOpenMarket()V

    goto :goto_0

    .line 56
    :cond_5
    const-string v4, "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_SHOW_LOADING_POPUP"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 57
    invoke-direct {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->showLoadingPopup()V

    goto :goto_0

    .line 59
    :cond_6
    const-string v4, "com.samsung.android.magazine.intent.extra.EXTRA_DIALOG_TYPE_OPEN_APPINFO"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "appname"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "appName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/DialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "packagename"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 62
    .local v3, "pkgName":Ljava/lang/String;
    invoke-direct {p0, v0, v3}, Lcom/samsung/android/internal/headlines/DialogActivity;->dialogOpenAppInfo(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 293
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 294
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/internal/headlines/DialogActivity;->instance:Lcom/samsung/android/internal/headlines/DialogActivity;

    .line 295
    return-void
.end method
