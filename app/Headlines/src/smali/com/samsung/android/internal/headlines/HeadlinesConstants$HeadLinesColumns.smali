.class public Lcom/samsung/android/internal/headlines/HeadlinesConstants$HeadLinesColumns;
.super Ljava/lang/Object;
.source "HeadlinesConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/internal/headlines/HeadlinesConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HeadLinesColumns"
.end annotation


# static fields
.field public static final AUTHOR:Ljava/lang/String; = "author"

.field public static final AUTHOR_NAME:Ljava/lang/String; = "name"

.field public static final AUTHOR_URL:Ljava/lang/String; = "imageURL"

.field public static final BODY:Ljava/lang/String; = "body"

.field public static final CATEGORY:Ljava/lang/String; = "category"

.field public static final FLIPBOARD_LINK_URL:Ljava/lang/String; = "flipboardURL"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final IMAGE:Ljava/lang/String; = "image"

.field public static final IMAGE_HEIGHT:Ljava/lang/String; = "height"

.field public static final IMAGE_PATH:Ljava/lang/String; = "path"

.field public static final IMAGE_URL:Ljava/lang/String; = "url"

.field public static final IMAGE_WIDTH:Ljava/lang/String; = "width"

.field public static final LINK_URL:Ljava/lang/String; = "linkURL"

.field public static final SERVICE:Ljava/lang/String; = "service"

.field public static final TEXT:Ljava/lang/String; = "text"

.field public static final TIME_STAMP:Ljava/lang/String; = "dateCreated"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TYPE:Ljava/lang/String; = "type"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
