.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestUpdateSystemLanguage(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

.field final synthetic val$current:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 503
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;->val$current:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 506
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$500(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->clearCategory()V

    .line 507
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$500(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v0

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$800()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11$1;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->queryCategory(Ljava/lang/String;Ljava/util/Observer;)V

    .line 525
    return-void
.end method
