.class Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;
.super Landroid/os/AsyncTask;
.source "HeadlinesCardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/internal/headlines/HeadlinesCardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "updateCardTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/samsung/android/internal/headlines/HeadlinesItem;",
        "Ljava/lang/Void;",
        "Lcom/samsung/android/magazine/cardprovider/card/Card;",
        ">;"
    }
.end annotation


# instance fields
.field item:Lcom/samsung/android/internal/headlines/HeadlinesItem;

.field mTriggerTime:J

.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;J)V
    .locals 2
    .param p2, "triggeredTime"    # J

    .prologue
    .line 140
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->item:Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .line 92
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->mTriggerTime:J

    .line 141
    iput-wide p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->mTriggerTime:J

    .line 142
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/magazine/cardprovider/card/Card;
    .locals 8
    .param p1, "params"    # [Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .prologue
    const/4 v0, 0x0

    .line 126
    iget-wide v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->mTriggerTime:J

    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v1

    # invokes: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getChangedLocaleTime()J
    invoke-static {v1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$300(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 127
    const-string v1, "HeadlinesCardManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doInBackground : updateCardTask, old content! trigger time : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->mTriggerTime:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " locale changed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v3

    # invokes: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getChangedLocaleTime()J
    invoke-static {v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$300(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_0
    :goto_0
    return-object v0

    .line 130
    :cond_1
    const/4 v1, 0x0

    aget-object v1, p1, v1

    iput-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->item:Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .line 131
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->item:Lcom/samsung/android/internal/headlines/HeadlinesItem;

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->item:Lcom/samsung/android/internal/headlines/HeadlinesItem;

    iget-object v1, v1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->title:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 134
    const-string v1, "HeadlinesCardManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doInBackground : updateCardTask, new content! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->item:Lcom/samsung/android/internal/headlines/HeadlinesItem;

    iget-object v3, v3, Lcom/samsung/android/internal/headlines/HeadlinesItem;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :cond_2
    const-string v1, "HeadlinesCardManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doInBackground() updateCardTask, new content! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->mTriggerTime:J

    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v3

    # invokes: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getChangedLocaleTime()J
    invoke-static {v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$300(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->item:Lcom/samsung/android/internal/headlines/HeadlinesItem;

    # invokes: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->updateCard(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/magazine/cardprovider/card/Card;
    invoke-static {v1, v2}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$400(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/magazine/cardprovider/card/Card;

    move-result-object v0

    .line 137
    .local v0, "card":Lcom/samsung/android/magazine/cardprovider/card/Card;
    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 90
    check-cast p1, [Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->doInBackground([Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/magazine/cardprovider/card/Card;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/samsung/android/magazine/cardprovider/card/Card;)V
    .locals 7
    .param p1, "card"    # Lcom/samsung/android/magazine/cardprovider/card/Card;

    .prologue
    const/4 v6, 0x0

    .line 96
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 97
    if-nez p1, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardprovider/CardManager;
    invoke-static {v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$000(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)Lcom/samsung/android/magazine/cardprovider/CardManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/samsung/android/magazine/cardprovider/CardManager;->updateCard(Lcom/samsung/android/magazine/cardprovider/card/Card;)Z

    .line 101
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->item:Lcom/samsung/android/internal/headlines/HeadlinesItem;

    iget-object v5, v5, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    # invokes: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->isSocial(Ljava/lang/String;)Z
    invoke-static {v4, v5}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$100(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 102
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getSocialWelcomeList()Ljava/util/Set;

    move-result-object v2

    .line 103
    .local v2, "socialList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    .line 104
    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->item:Lcom/samsung/android/internal/headlines/HeadlinesItem;

    iget-object v4, v4, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 105
    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$200(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "mymagazineService"

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 106
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 107
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "SOCIAL_WELCOME_LIST"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 108
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 111
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    .end local v2    # "socialList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getNewsWelcomeList()Ljava/util/Set;

    move-result-object v3

    .line 112
    .local v3, "welcomeList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v3, :cond_0

    .line 113
    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->item:Lcom/samsung/android/internal/headlines/HeadlinesItem;

    iget-object v4, v4, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 114
    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$200(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "mymagazineService"

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 115
    .restart local v1    # "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 116
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "NEWS_WELCOME_LIST"

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 117
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 90
    check-cast p1, Lcom/samsung/android/magazine/cardprovider/card/Card;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;->onPostExecute(Lcom/samsung/android/magazine/cardprovider/card/Card;)V

    return-void
.end method
