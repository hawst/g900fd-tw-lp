.class Lcom/samsung/android/internal/headlines/DialogActivity$5;
.super Ljava/lang/Object;
.source "DialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/DialogActivity;->dialogOpenMarket()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/DialogActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/DialogActivity;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/DialogActivity$5;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const v9, 0x7f090004

    const/4 v8, 0x1

    .line 193
    const/4 v5, 0x0

    .line 197
    .local v5, "vendingEnabled":Z
    const-string v3, "com.sec.android.app.samsungapps"

    .line 199
    .local v3, "packageName":Ljava/lang/String;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/DialogActivity$5;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    invoke-virtual {v6}, Lcom/samsung/android/internal/headlines/DialogActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v3, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 200
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v6, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v5, v6, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    if-ne v5, v8, :cond_2

    .line 212
    const-string v6, "CN"

    invoke-static {v6}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v6

    if-ne v6, v8, :cond_1

    const-string v4, "samsungapps://ProductDetail/flipboard.cn"

    .line 213
    .local v4, "path":Ljava/lang/String;
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 214
    .local v1, "intentForMarket":Landroid/content/Intent;
    const/high16 v6, 0x10000000

    invoke-virtual {v1, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 215
    const-string v6, "NOACCOUNT"

    const-string v7, "Y"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/DialogActivity$5;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    invoke-virtual {v6, v1}, Lcom/samsung/android/internal/headlines/DialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 217
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 218
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/DialogActivity$5;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    invoke-virtual {v6}, Lcom/samsung/android/internal/headlines/DialogActivity;->finish()V

    .line 228
    .end local v1    # "intentForMarket":Landroid/content/Intent;
    .end local v4    # "path":Ljava/lang/String;
    :goto_1
    return-void

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 203
    const-string v6, "CN"

    invoke-static {v6}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 204
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/DialogActivity$5;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    iget-object v7, p0, Lcom/samsung/android/internal/headlines/DialogActivity$5;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    invoke-virtual {v7}, Lcom/samsung/android/internal/headlines/DialogActivity;->getApplication()Landroid/app/Application;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/samsung/android/internal/headlines/DialogActivity;->dialogAppIsNotInstalled(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/android/internal/headlines/DialogActivity;->access$000(Lcom/samsung/android/internal/headlines/DialogActivity;Ljava/lang/String;)V

    .line 205
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 206
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/DialogActivity$5;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    invoke-virtual {v6}, Lcom/samsung/android/internal/headlines/DialogActivity;->finish()V

    goto :goto_1

    .line 212
    .end local v0    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    const-string v4, "samsungapps://ProductDetail/flipboard.app"

    goto :goto_0

    .line 223
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/internal/headlines/DialogActivity$5;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    iget-object v7, p0, Lcom/samsung/android/internal/headlines/DialogActivity$5;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    invoke-virtual {v7}, Lcom/samsung/android/internal/headlines/DialogActivity;->getApplication()Landroid/app/Application;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.sec.android.app.samsungapps"

    # invokes: Lcom/samsung/android/internal/headlines/DialogActivity;->dialogOpenAppInfo(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v7, v8}, Lcom/samsung/android/internal/headlines/DialogActivity;->access$100(Lcom/samsung/android/internal/headlines/DialogActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
