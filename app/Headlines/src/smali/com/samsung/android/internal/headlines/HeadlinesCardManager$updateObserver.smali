.class public Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;
.super Ljava/lang/Object;
.source "HeadlinesCardManager.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/internal/headlines/HeadlinesCardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "updateObserver"
.end annotation


# instance fields
.field category:Ljava/lang/String;

.field mTriggerTime:J

.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;)V
    .locals 2
    .param p2, "_category"    # Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;->category:Ljava/lang/String;

    .line 147
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;->mTriggerTime:J

    .line 150
    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;->category:Ljava/lang/String;

    .line 151
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;->mTriggerTime:J

    .line 152
    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 6
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 157
    if-nez p2, :cond_1

    .line 158
    const-string v2, "HeadlinesCardManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "update : updateObserver, data is null category : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;->category:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;->category:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->postNoContentCard(Ljava/lang/String;)V

    .line 171
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p2

    .line 163
    check-cast v0, Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .line 165
    .local v0, "item":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    if-eqz v0, :cond_0

    .line 167
    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    iget-wide v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;->mTriggerTime:J

    invoke-direct {v1, v2, v4, v5}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateCardTask;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;J)V

    .line 168
    .local v1, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Lcom/samsung/android/internal/headlines/HeadlinesItem;Ljava/lang/Void;Lcom/samsung/android/magazine/cardprovider/card/Card;>;"
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/samsung/android/internal/headlines/HeadlinesItem;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
