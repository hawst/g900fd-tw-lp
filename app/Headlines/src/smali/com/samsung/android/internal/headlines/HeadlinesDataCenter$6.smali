.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$6;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestSocialItem(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

.field final synthetic val$observer:Ljava/util/Observer;

.field final synthetic val$socialId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/lang/String;Ljava/util/Observer;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$6;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$6;->val$socialId:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$6;->val$observer:Ljava/util/Observer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 7
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    .line 335
    if-eqz p2, :cond_2

    instance-of v3, p2, Ljava/util/List;

    if-eqz v3, :cond_2

    move-object v2, p2

    .line 337
    check-cast v2, Ljava/util/List;

    .line 338
    .local v2, "socialList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 339
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    .line 340
    .local v1, "social":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    if-nez v1, :cond_1

    .line 338
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 343
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$6;->val$socialId:Ljava/lang/String;

    iget-object v4, v1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v5, :cond_0

    .line 344
    iget-boolean v3, v1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->active:Z

    if-ne v3, v5, :cond_3

    .line 345
    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$700()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestSocialItem(): social id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") is login."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$6;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    invoke-static {v3}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$500(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v3

    iget-object v4, v1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    iget-object v5, v1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->name:Ljava/lang/String;

    new-instance v6, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$6$1;

    invoke-direct {v6, p0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$6$1;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$6;)V

    invoke-virtual {v3, v4, v5, v6}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getServiceItems(Ljava/lang/String;Ljava/lang/String;Ljava/util/Observer;)V

    .line 378
    .end local v0    # "i":I
    .end local v1    # "social":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .end local v2    # "socialList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    :cond_2
    :goto_1
    return-void

    .line 371
    .restart local v0    # "i":I
    .restart local v1    # "social":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .restart local v2    # "socialList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    :cond_3
    const-string v3, "Headlines"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestSocialItem(): social name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not login."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
