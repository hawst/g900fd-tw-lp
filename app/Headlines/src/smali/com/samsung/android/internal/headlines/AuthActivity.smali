.class public Lcom/samsung/android/internal/headlines/AuthActivity;
.super Landroid/app/Activity;
.source "AuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/internal/headlines/AuthActivity$1;,
        Lcom/samsung/android/internal/headlines/AuthActivity$AuthObserver;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 16
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/AuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 43
    .local v1, "extras":Landroid/os/Bundle;
    const-string v4, "com.flipboard.fdl.URL"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "url":Ljava/lang/String;
    new-instance v3, Landroid/webkit/WebView;

    invoke-direct {v3, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 49
    .local v3, "webview":Landroid/webkit/WebView;
    invoke-static {}, Lcom/flipboard/data/AuthorizationClient;->getInstance()Lcom/flipboard/data/AuthorizationClient;

    move-result-object v0

    .line 52
    .local v0, "client":Lcom/flipboard/data/AuthorizationClient;
    new-instance v4, Lcom/samsung/android/internal/headlines/AuthActivity$AuthObserver;

    invoke-direct {v4}, Lcom/samsung/android/internal/headlines/AuthActivity$AuthObserver;-><init>()V

    invoke-virtual {v0, v4}, Lcom/flipboard/data/AuthorizationClient;->addObserver(Lcom/flipboard/util/Observer;)V

    .line 55
    invoke-virtual {v3, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 56
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 57
    invoke-virtual {p0, v3}, Lcom/samsung/android/internal/headlines/AuthActivity;->setContentView(Landroid/view/View;)V

    .line 60
    invoke-virtual {v3, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/AuthActivity;->finish()V

    .line 63
    return-void
.end method
