.class Lcom/samsung/android/internal/headlines/DialogActivity$1;
.super Ljava/lang/Object;
.source "DialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/DialogActivity;->dialogSocialLogout(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

.field final synthetic val$category:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/DialogActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/DialogActivity$1;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/DialogActivity$1;->val$category:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 137
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/samsung/android/internal/headlines/DialogActivity;->instance:Lcom/samsung/android/internal/headlines/DialogActivity;

    const-class v2, Lcom/samsung/android/internal/headlines/HeadlinesBroadcastReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 138
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.android.internal.headlines.LOGOUT_SOCIAL_INTERNAL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    const-string v1, "category"

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/DialogActivity$1;->val$category:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    const-string v1, "msg"

    const-string v2, "YES"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/DialogActivity$1;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    invoke-virtual {v1, v0}, Lcom/samsung/android/internal/headlines/DialogActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 142
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 143
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/DialogActivity$1;->this$0:Lcom/samsung/android/internal/headlines/DialogActivity;

    invoke-virtual {v1}, Lcom/samsung/android/internal/headlines/DialogActivity;->finish()V

    .line 144
    return-void
.end method
