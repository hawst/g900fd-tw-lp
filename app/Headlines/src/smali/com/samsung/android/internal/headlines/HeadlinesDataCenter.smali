.class public Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
.super Ljava/util/Observable;
.source "HeadlinesDataCenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;,
        Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field private static instance:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

.field private static mLocale:Ljava/lang/String;

.field private static final mPendingList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHeadlinesCardManager:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

.field private mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

.field private mNewsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesCategory;",
            ">;"
        }
    .end annotation
.end field

.field private mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

.field private mSocialList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesCategory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    const-string v0, "HeadlinesDataCenter"

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    .line 32
    sput-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->instance:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    .line 38
    sput-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mPendingList:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    .line 33
    iput-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mNewsList:Ljava/util/List;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mSocialList:Ljava/util/List;

    .line 36
    iput-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mHeadlinesCardManager:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    .line 39
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    .line 61
    const-string v0, "Headlines"

    const-string v1, "HeadlinesDataCenter ctor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;

    .line 63
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;

    .line 65
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mHeadlinesCardManager:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    .line 67
    const-string v0, "Headlines"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeadlinesDataCenter. mLocale = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const-string v0, "CN"

    invoke-static {v0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 69
    const-string v0, "zh_CN"

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;

    .line 70
    :cond_0
    return-void
.end method

.method public static Create(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->instance:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    invoke-direct {v0, p0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->instance:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    .line 47
    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000()Ljava/util/List;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mPendingList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->instance:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mSocialList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mSocialList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesCardManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mHeadlinesCardManager:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mNewsList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mNewsList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 24
    sput-object p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$900(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    return-object v0
.end method

.method static synthetic access$902(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    .param p1, "x1"    # Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    return-object p1
.end method

.method private addPendingIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 603
    if-nez p1, :cond_1

    .line 610
    :cond_0
    :goto_0
    return-void

    .line 605
    :cond_1
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeadlinesDataCenter.addPendingIntent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 607
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeadlinesDataCenter.addPendingIntent extra : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    :cond_2
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mPendingList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 609
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mPendingList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getInstance()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->instance:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    return-object v0
.end method

.method private isDataAllowed()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 559
    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;

    const-string v3, "mymagazineService"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 560
    .local v0, "mPrefs":Landroid/content/SharedPreferences;
    const-string v2, "DATA_ALLOWED"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 561
    .local v1, "value":Z
    return v1
.end method

.method private isEnabledUserData()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 613
    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;

    const-string v3, "mymagazineService"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 614
    .local v0, "mPrefs":Landroid/content/SharedPreferences;
    const-string v2, "USER_DATA_ALLOWED"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 615
    .local v1, "value":Z
    return v1
.end method


# virtual methods
.method public IsInit()Z
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->DONE:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCategoryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mNewsList:Ljava/util/List;

    return-object v0
.end method

.method public getLocale(Ljava/util/Observer;)V
    .locals 3
    .param p1, "observer"    # Ljava/util/Observer;

    .prologue
    .line 461
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->DONE:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    if-ne v0, v1, :cond_0

    .line 462
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$10;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$10;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/Observer;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getLocales(Ljava/lang/String;Ljava/util/Observer;)V

    .line 471
    :cond_0
    return-void
.end method

.method public getService(Ljava/util/Observer;Landroid/content/Intent;)V
    .locals 2
    .param p1, "observer"    # Ljava/util/Observer;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 222
    invoke-virtual {p0, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->init(Landroid/content/Intent;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 223
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$3;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$3;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/Observer;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getService(Ljava/util/Observer;)V

    .line 239
    :cond_0
    return-void
.end method

.method public getServiceForItem(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 1
    .param p1, "item"    # Lorg/json/JSONObject;

    .prologue
    .line 550
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    invoke-virtual {v0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getServiceForItem(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public getServiceObject(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 1
    .param p1, "serviceId"    # Ljava/lang/String;

    .prologue
    .line 555
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    invoke-virtual {v0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getServiceObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public getSocialList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mSocialList:Ljava/util/List;

    return-object v0
.end method

.method public init(Landroid/content/Intent;)Z
    .locals 5
    .param p1, "actionIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    sget-object v4, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->DONE:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    if-ne v3, v4, :cond_0

    .line 87
    sget-object v2, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    const-string v3, "HeadlinesDataCenter was already initialized."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :goto_0
    return v1

    .line 91
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    sget-object v4, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->DOING:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    if-ne v3, v4, :cond_2

    .line 92
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    const-string v3, "HeadlinesDataCenter is initializing."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    if-eqz p1, :cond_1

    .line 95
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->addPendingIntent(Landroid/content/Intent;)V

    :cond_1
    move v1, v2

    .line 97
    goto :goto_0

    .line 100
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->isConnected()Z

    move-result v3

    if-nez v3, :cond_4

    .line 101
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    const-string v3, "HeadlinesDataCenter was failed to initialize cause : network error."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    if-eqz p1, :cond_3

    .line 104
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->addPendingIntent(Landroid/content/Intent;)V

    .line 107
    :cond_3
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->INIT:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    iput-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    move v1, v2

    .line 108
    goto :goto_0

    .line 111
    :cond_4
    const-string v3, "CN"

    invoke-static {v3}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v1, :cond_5

    invoke-direct {p0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->isDataAllowed()Z

    move-result v1

    if-nez v1, :cond_5

    .line 112
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    const-string v3, "HeadlinesDataCenter was failed to initialize cause : network use denied."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 113
    goto :goto_0

    .line 116
    :cond_5
    invoke-direct {p0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->isEnabledUserData()Z

    move-result v1

    if-nez v1, :cond_7

    .line 117
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    const-string v3, "init : HeadlinesDataCenter was failed to initialize cause : user didn\'t allow to user data"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    if-eqz p1, :cond_6

    .line 119
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init : (Intent = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") is denied."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move v1, v2

    .line 121
    goto :goto_0

    .line 125
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->init(Landroid/content/Context;)Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    .line 126
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->DOING:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    iput-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    .line 127
    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitObserver;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$1;)V

    .line 128
    .local v0, "observer":Ljava/util/Observer;
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    invoke-virtual {v1, v0}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->addObserver(Ljava/util/Observer;)V

    .line 130
    if-eqz p1, :cond_8

    .line 131
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->addPendingIntent(Landroid/content/Intent;)V

    :cond_8
    move v1, v2

    .line 133
    goto/16 :goto_0
.end method

.method public isChangedLocale()Z
    .locals 2

    .prologue
    .line 474
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    const/4 v0, 0x0

    .line 478
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesConnection;->isConnected(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public notifyObservers(Ljava/lang/Object;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 142
    invoke-super {p0, p1}, Ljava/util/Observable;->notifyObservers(Ljava/lang/Object;)V

    .line 143
    return-void
.end method

.method public queryCategory(Ljava/util/Observer;Z)V
    .locals 3
    .param p1, "observer"    # Ljava/util/Observer;
    .param p2, "check"    # Z

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    if-nez v0, :cond_1

    .line 271
    const-string v0, "Headlines"

    const-string v1, "queryCategory.  mRequest is not initialized."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, p0, v0}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 275
    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mInit:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;->DONE:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$InitStatus;

    if-ne v0, v1, :cond_0

    .line 276
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;

    new-instance v2, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/Observer;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getLocales(Ljava/lang/String;Ljava/util/Observer;)V

    goto :goto_0
.end method

.method public queryService(Ljava/util/Observer;)V
    .locals 2
    .param p1, "observer"    # Ljava/util/Observer;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    if-nez v0, :cond_0

    .line 244
    const-string v0, "Headlines"

    const-string v1, "queryService.  mRequest is not initialized."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, p0, v0}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 267
    :goto_0
    return-void

    .line 248
    :cond_0
    const-string v0, "Headlines"

    const-string v1, "queryService."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$4;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$4;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/Observer;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getService(Ljava/util/Observer;)V

    goto :goto_0
.end method

.method public removeCategory(Ljava/lang/String;)V
    .locals 3
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 457
    const-string v0, "Headlines"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeCategory "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    return-void
.end method

.method public requestServiceLogout(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V
    .locals 3
    .param p1, "socialId"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "observer"    # Ljava/util/Observer;

    .prologue
    .line 565
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestServiceLogout() : social id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    invoke-virtual {p0, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->init(Landroid/content/Intent;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 567
    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$12;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->getService(Ljava/util/Observer;Landroid/content/Intent;)V

    .line 600
    :cond_0
    return-void
.end method

.method public requestSocialItem(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V
    .locals 3
    .param p1, "socialId"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "observer"    # Ljava/util/Observer;

    .prologue
    .line 328
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestSocialItem(): social name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    invoke-virtual {p0, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->init(Landroid/content/Intent;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 331
    new-instance v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$6;

    invoke-direct {v0, p0, p1, p3}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$6;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/lang/String;Ljava/util/Observer;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->getService(Ljava/util/Observer;Landroid/content/Intent;)V

    .line 381
    :cond_0
    return-void
.end method

.method public requestSocialItem(Ljava/lang/String;Ljava/util/Observer;)V
    .locals 3
    .param p1, "socialId"    # Ljava/lang/String;
    .param p2, "observer"    # Ljava/util/Observer;

    .prologue
    .line 384
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    if-nez v0, :cond_0

    .line 385
    const-string v0, "Headlines"

    const-string v1, "requestSocialItem.  mRequest is not initialized."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2, p0, v0}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 413
    :goto_0
    return-void

    .line 389
    :cond_0
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeadlinesDataCenter requestSocialItem "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$7;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$7;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/lang/String;Ljava/util/Observer;)V

    invoke-virtual {v0, p1, p1, v1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getServiceItems(Ljava/lang/String;Ljava/lang/String;Ljava/util/Observer;)V

    goto :goto_0
.end method

.method public requestUpdate(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V
    .locals 4
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "observer"    # Ljava/util/Observer;

    .prologue
    const/4 v3, 0x1

    .line 146
    invoke-virtual {p0, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->init(Landroid/content/Intent;)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 147
    sget-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[HeadlinesDataCenter] requestUpdate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    if-nez v0, :cond_1

    .line 149
    const-string v0, "Headlines"

    const-string v1, "requestUpdate.  mRequest is not initialized."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p3, p0, v0}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$1;

    invoke-direct {v1, p0, p1, p3}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$1;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/lang/String;Ljava/util/Observer;)V

    invoke-virtual {v0, p1, p1, v3, v1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getItems(Ljava/lang/String;Ljava/lang/String;ILjava/util/Observer;)V

    goto :goto_0
.end method

.method public requestUpdate(Ljava/lang/String;Ljava/util/Observer;)V
    .locals 3
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "observer"    # Ljava/util/Observer;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    if-nez v0, :cond_0

    .line 182
    const-string v0, "Headlines"

    const-string v1, "requestUpdate.  mRequest is not initialized."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2, p0, v0}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 209
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    const/4 v1, 0x1

    new-instance v2, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$2;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$2;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/lang/String;Ljava/util/Observer;)V

    invoke-virtual {v0, p1, p1, v1, v2}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getItems(Ljava/lang/String;Ljava/lang/String;ILjava/util/Observer;)V

    goto :goto_0
.end method

.method public requestUpdateNewsWelcomeCard(Landroid/content/Intent;Ljava/util/Set;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 532
    .local p2, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 533
    :cond_0
    const-string v3, "HeadlinesDataCenter"

    const-string v4, "requestUpdateNewsWelcomeCard !!! no welcome card"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    :cond_1
    return-void

    .line 536
    :cond_2
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {p2, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 537
    .local v0, "array":[Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->init(Landroid/content/Intent;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 538
    const-string v3, "HeadlinesDataCenter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestUpdateNewsWelcomeCard size"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v1

    .line 541
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 543
    const-string v3, "Headlines"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Request update welcome card to content card. : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    aget-object v3, v0, v2

    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mHeadlinesCardManager:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    aget-object v5, v0, v2

    invoke-virtual {v4, v5}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->getObserver(Ljava/lang/String;)Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestUpdate(Ljava/lang/String;Ljava/util/Observer;)V

    .line 541
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public requestUpdateSystemLanguage(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 482
    const-string v1, "Headlines"

    const-string v2, "[requestUpdateSystemLanguage] HeadlinesDataCenter."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    const-string v1, "CN"

    invoke-static {v1}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 529
    :cond_0
    :goto_0
    return-void

    .line 487
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->isChangedLocale()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 491
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 495
    invoke-virtual {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->init(Landroid/content/Intent;)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 496
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 497
    .local v0, "current":Ljava/lang/String;
    const-string v1, "CN"

    invoke-static {v1}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v3, :cond_2

    .line 498
    const-string v1, "zh_CN"

    sput-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;

    .line 500
    :cond_2
    sget-object v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 501
    sput-object v0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;

    .line 502
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mHeadlinesCardManager:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    invoke-virtual {v1}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->ClearSnapshot()V

    .line 503
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    sget-object v2, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mLocale:Ljava/lang/String;

    new-instance v3, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;

    invoke-direct {v3, p0, v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$11;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->getLocales(Ljava/lang/String;Ljava/util/Observer;)V

    goto :goto_0
.end method

.method public serviceLogOut(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V
    .locals 2
    .param p1, "socialId"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "observer"    # Ljava/util/Observer;

    .prologue
    .line 417
    invoke-virtual {p0, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->init(Landroid/content/Intent;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 419
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$8;

    invoke-direct {v1, p0, p3}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$8;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/Observer;)V

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->serviceLogOut(Ljava/lang/String;Ljava/util/Observer;)V

    .line 433
    :cond_0
    return-void
.end method

.method public serviceLogin(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V
    .locals 3
    .param p1, "socialId"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "observer"    # Ljava/util/Observer;

    .prologue
    .line 436
    const-string v0, "Headlines"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestServiceLogin socialId Type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    invoke-virtual {p0, p2}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->init(Landroid/content/Intent;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 439
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$9;

    invoke-direct {v1, p0, p1, p3}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$9;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/lang/String;Ljava/util/Observer;)V

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->serviceLogin(Ljava/lang/String;Ljava/util/Observer;)V

    .line 454
    :cond_0
    return-void
.end method

.method public showToast(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 74
    if-nez p2, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.internal.headlines.UPDATE_FEED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.magazine.intent.action.CARD_REFRESH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 82
    const-string v0, "Headlines"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeadlinesCardManager showToast : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
