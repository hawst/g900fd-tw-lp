.class Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2$1;
.super Ljava/lang/Object;
.source "HeadlinesCardManager.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;->update(Ljava/util/Observable;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;)V
    .locals 0

    .prologue
    .line 685
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 6
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 688
    if-eqz p2, :cond_0

    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 689
    const-string v0, "HeadlinesCardManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestServiceLogin :  responce of serviceLogin Card Type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;

    iget-object v2, v2, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;->val$category:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "arg1":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 691
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;

    iget-object v0, v0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$500(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;

    iget-object v1, v1, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;->val$category:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;

    iget-object v2, v2, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;->val$intent:Landroid/content/Intent;

    new-instance v3, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;

    iget-object v4, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;

    iget-object v4, v4, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    iget-object v5, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2$1;->this$1:Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;

    iget-object v5, v5, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$2;->val$category:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$updateObserver;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->requestSocialItem(Ljava/lang/String;Landroid/content/Intent;Ljava/util/Observer;)V

    .line 694
    :cond_0
    return-void
.end method
