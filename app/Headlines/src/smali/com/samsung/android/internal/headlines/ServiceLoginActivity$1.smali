.class Lcom/samsung/android/internal/headlines/ServiceLoginActivity$1;
.super Landroid/webkit/WebChromeClient;
.source "ServiceLoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/ServiceLoginActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/ServiceLoginActivity;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity$1;->this$0:Lcom/samsung/android/internal/headlines/ServiceLoginActivity;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "newProgress"    # I

    .prologue
    const/16 v2, 0x8

    .line 62
    invoke-super {p0, p1, p2}, Landroid/webkit/WebChromeClient;->onProgressChanged(Landroid/webkit/WebView;I)V

    .line 63
    const/16 v0, 0x64

    if-ne p2, v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity$1;->this$0:Lcom/samsung/android/internal/headlines/ServiceLoginActivity;

    # getter for: Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mWebView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->access$000(Lcom/samsung/android/internal/headlines/ServiceLoginActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 66
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity$1;->this$0:Lcom/samsung/android/internal/headlines/ServiceLoginActivity;

    # getter for: Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mLoadingView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->access$100(Lcom/samsung/android/internal/headlines/ServiceLoginActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 67
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity$1;->this$0:Lcom/samsung/android/internal/headlines/ServiceLoginActivity;

    # getter for: Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->access$200(Lcom/samsung/android/internal/headlines/ServiceLoginActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/ServiceLoginActivity$1;->this$0:Lcom/samsung/android/internal/headlines/ServiceLoginActivity;

    # getter for: Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->mImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/ServiceLoginActivity;->access$300(Lcom/samsung/android/internal/headlines/ServiceLoginActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 70
    :cond_0
    return-void
.end method
