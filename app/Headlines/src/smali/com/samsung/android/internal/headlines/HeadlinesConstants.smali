.class public Lcom/samsung/android/internal/headlines/HeadlinesConstants;
.super Ljava/lang/Object;
.source "HeadlinesConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/internal/headlines/HeadlinesConstants$HeadLinesColumns;,
        Lcom/samsung/android/internal/headlines/HeadlinesConstants$SectionColumns;,
        Lcom/samsung/android/internal/headlines/HeadlinesConstants$SocialCategoryColumns;
    }
.end annotation


# static fields
.field public static final ACTION:Ljava/lang/String; = "action"

.field public static final ACTION_ALIVE:Ljava/lang/String; = "com.samsung.android.internal.headlines.ALIVE"

.field public static final ACTION_ALLOW_DATA_USE:Ljava/lang/String; = "com.samsung.android.internal.headlines.ALLOW_DATA_USE"

.field public static final ACTION_CARD_DISMISSED:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CARD_DISMISSED"

.field public static final ACTION_CARD_INITIALIZE:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.INITIALIZE_CARD_PROVIDER"

.field public static final ACTION_CARD_NAME_SUBSCRIPTION_STATE_CHANGED:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CARD_NAME_SUBSCRIPTION_STATE_CHANGED"

.field public static final ACTION_CARD_REFRESH:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CARD_REFRESH"

.field public static final ACTION_LAUNCH_FLIPBOARD_APP_FOR_NEWS:Ljava/lang/String; = "com.samsung.android.internal.headlines.LAUNCH_FLIPBOARD_APP_FOR_NEWS"

.field public static final ACTION_LAUNCH_FLIPBOARD_APP_FOR_SOCIAL:Ljava/lang/String; = "com.samsung.android.internal.headlines.LAUNCH_FLIPBOARD_APP_FOR_SOCIAL"

.field public static final ACTION_LOCALE_CHANGED:Ljava/lang/String; = "android.intent.action.LOCALE_CHANGED"

.field public static final ACTION_LOGIN_HEADLINES_SOCIAL:Ljava/lang/String; = "com.samsung.android.internal.headlines.LOGIN_SOCIAL"

.field public static final ACTION_LOGOUT_HEADLINES_SOCIAL:Ljava/lang/String; = "com.samsung.android.internal.headlines.LOGOUT_SOCIAL"

.field public static final ACTION_LOGOUT_HEADLINES_SOCIAL_INTERNAL:Ljava/lang/String; = "com.samsung.android.internal.headlines.LOGOUT_SOCIAL_INTERNAL"

.field public static final ACTION_UPDATE_HEADLINES_CATEGORY_AND_WELCOME_CARD:Ljava/lang/String; = "com.samsung.android.internal.headlines.UPDATE_HEADLINES_CATEGORY_LIST"

.field public static final ACTION_UPDATE_HEADLINES_FEED:Ljava/lang/String; = "com.samsung.android.internal.headlines.UPDATE_FEED"

.field public static final ACTION_UPDATE_WELCOME_CARD:Ljava/lang/String; = "com.samsung.android.internal.headlines.UPDATE_WELCOME_CARD"

.field public static final CATEGORY_ARTS:Ljava/lang/String; = "arts"

.field public static final CATEGORY_BOOKS:Ljava/lang/String; = "books"

.field public static final CATEGORY_BUSINESS:Ljava/lang/String; = "business"

.field public static final CATEGORY_BY:Ljava/lang/String; = "by"

.field public static final CATEGORY_ENGLISHONLY:Ljava/lang/String; = "englishonly"

.field public static final CATEGORY_FIRSTLAUNCH:Ljava/lang/String; = "firstlaunch"

.field public static final CATEGORY_FIVEHUNDREDPX:Ljava/lang/String; = "fivehundredpx"

.field public static final CATEGORY_FLICKR:Ljava/lang/String; = "flickr"

.field public static final CATEGORY_FOOD:Ljava/lang/String; = "food"

.field public static final CATEGORY_GOOGLEPLUS:Ljava/lang/String; = "googleplus"

.field public static final CATEGORY_LINKEDIN:Ljava/lang/String; = "linkedin"

.field public static final CATEGORY_LIVING:Ljava/lang/String; = "living"

.field public static final CATEGORY_LOCAL:Ljava/lang/String; = "local"

.field public static final CATEGORY_MUSIC:Ljava/lang/String; = "music"

.field public static final CATEGORY_NETHERLANDS:Ljava/lang/String; = "netherlands"

.field public static final CATEGORY_NEW:Ljava/lang/String; = "new"

.field public static final CATEGORY_NEWS:Ljava/lang/String; = "news"

.field public static final CATEGORY_PHOTOS:Ljava/lang/String; = "photos"

.field public static final CATEGORY_RENREN:Ljava/lang/String; = "renren"

.field public static final CATEGORY_SPORT:Ljava/lang/String; = "sports"

.field public static final CATEGORY_STYLE:Ljava/lang/String; = "style"

.field public static final CATEGORY_TECH:Ljava/lang/String; = "tech"

.field public static final CATEGORY_TRAVEL:Ljava/lang/String; = "travel"

.field public static final CATEGORY_TUMBLR:Ljava/lang/String; = "tumblr"

.field public static final CATEGORY_TWITTER:Ljava/lang/String; = "twitter"

.field public static final CATEGORY_WEIBO:Ljava/lang/String; = "weibo"

.field public static final CATEGORY_YOUTUBE:Ljava/lang/String; = "youtube"

.field public static final DELETE:Ljava/lang/String; = "delete"

.field public static final DELETE_BULK:Ljava/lang/String; = "delete_bulk"

.field public static final EXTRA_CARD_ID:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.CARD_ID"

.field public static final EXTRA_CARD_NAME:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.CARD_NAME"

.field public static final EXTRA_CARD_NAME_SUBSCRIBED:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.CARD_NAME_SUBSCRIBED"

.field public static final EXTRA_DIALOG_TYPE:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.DIALOG_TYPE"

.field public static final EXTRA_DIALOG_TYPE_NETWORK_NOT_AVAILABLE:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_NETWORK_NOT_AVAILABLE"

.field public static final EXTRA_DIALOG_TYPE_NO_MORE_CONTENT:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_NO_MORE_CONTENT"

.field public static final EXTRA_DIALOG_TYPE_OPEN_APPINFO:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.EXTRA_DIALOG_TYPE_OPEN_APPINFO"

.field public static final EXTRA_DIALOG_TYPE_OPEN_MARKET:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_OPEN_MARKET"

.field public static final EXTRA_DIALOG_TYPE_SERVER_ERROR:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_SERVER_ERROR"

.field public static final EXTRA_DIALOG_TYPE_SHOW_LOADING_POPUP:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_SHOW_LOADING_POPUP"

.field public static final EXTRA_DIALOG_TYPE_SOCIAL_LOGOUT:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.DIALOG_TYPE_SOCIAL_LOGOUT"

.field public static final HEADLINES:Ljava/lang/String; = "headlines"

.field public static final HEADLINES_FLIPBOARD_USER_SYNC:Ljava/lang/String; = "flipboard.app.broadcast.SYNC_USER_CHANGE"

.field public static final HEADLINES_SECTION:Ljava/lang/String; = "headlines_section"

.field public static final INDEX_AUTHOR_NAME:I = 0x2

.field public static final INDEX_AUTHOR_URL:I = 0x3

.field public static final INDEX_CATEGORY:I = 0x1

.field public static final INDEX_ID:I = 0x0

.field public static final INDEX_IMAGE_HEIGHT:I = 0xb

.field public static final INDEX_IMAGE_URL:I = 0x9

.field public static final INDEX_IMAGE_WIDTH:I = 0xa

.field public static final INDEX_LINK_URL:I = 0x6

.field public static final INDEX_SERVICE:I = 0x7

.field public static final INDEX_TEXT:I = 0x4

.field public static final INDEX_TIME_STAMP:I = 0xc

.field public static final INDEX_TITLE:I = 0x5

.field public static final INDEX_TYPE:I = 0x8

.field public static final INSERT:Ljava/lang/String; = "insert"

.field public static final INSERT_BULK:Ljava/lang/String; = "insert_bulk"

.field public static final MORE:I = -0x1

.field public static final NEWS_PREFIX:Ljava/lang/String; = "news "

.field public static final PREF_FILE_NAME:Ljava/lang/String; = "mymagazine"

.field public static final PREF_FILE_NAME_SERVICE:Ljava/lang/String; = "mymagazineService"

.field public static final PREF_KEY_AUTO_REFRESH_ON:Ljava/lang/String; = "AUTO_REFRESH_ON"

.field public static final PREF_KEY_AUTO_REFRESH_POPUP_SHOW:Ljava/lang/String; = "AUTO_REFRESH_POPUP_SHOW"

.field public static final PREF_KEY_AUTO_REFRESH_SET_FIRST_TIME:Ljava/lang/String; = "AUTO_REFRESH_SET_FIRST_TIME"

.field public static final PREF_KEY_CUSTOMIZE_SETTING:Ljava/lang/String; = "CUSTOMIZE_SETTING"

.field public static final PREF_KEY_DATA_ALLOWED:Ljava/lang/String; = "DATA_ALLOWED"

.field public static final PREF_KEY_DO_NOT_SHOW_CUSTOMIZE_SETTING_POPUP_SHOW_FOR_ATT:Ljava/lang/String; = "DO_NOT_SHOW_CUSTOMIZE_SETTING_POPUP_SHOW_FOR_ATT"

.field public static final PREF_KEY_ENABLE_NEW_STORY_NOTIFICATION:Ljava/lang/String; = "ENABLE_NEW_STORY_NOTIFICATION"

.field public static final PREF_KEY_LAST_UPDATED_TIME:Ljava/lang/String; = "LAST_UPDATED_TIME"

.field public static final PREF_KEY_LAST_UPGRADE_CHECK_TIME:Ljava/lang/String; = "LAST_UPGRADE_CHECK_TIME"

.field public static final PREF_KEY_MOBILE_DATA_USE_ALLOWED:Ljava/lang/String; = "MOBILE_DATA_USE_ALLOWED"

.field public static final PREF_KEY_NEWS_WELCOME_LIST:Ljava/lang/String; = "NEWS_WELCOME_LIST"

.field public static final PREF_KEY_PAUSE_TIME:Ljava/lang/String; = "PAUSE_TIME"

.field public static final PREF_KEY_REGISTER_DEFAULT_CARD_NAME:Ljava/lang/String; = "REGISTER_DEFAULT_CARD_NAME"

.field public static final PREF_KEY_SOCIAL_WELCOME_LIST:Ljava/lang/String; = "SOCIAL_WELCOME_LIST"

.field public static final PREF_KEY_SUBSCRIBE_DEFAULT_CARD_NAME:Ljava/lang/String; = "SUBSCRIBE_DEFAULT_CARD_NAME"

.field public static final PREF_KEY_USER_DATA_ALLOWED:Ljava/lang/String; = "USER_DATA_ALLOWED"

.field public static final PREF_KEY_VERSION_IN_SAMSUNG_APPS:Ljava/lang/String; = "VERSION_IN_SAMSUNG_APPS"

.field public static final PREF_KEY_WELCOME_DATA_POPUP_SHOW_FOR_VZW:Ljava/lang/String; = "WELCOME_DATA_POPUP_SHOW_FOR_VZW"

.field public static final PREF_KEY_WELCOME_NEXT_BUTTON_PRESSED:Ljava/lang/String; = "WELCOME_NEXT_BUTTON_PRESSED"

.field public static final PREF_KEY_WLAN_DATA_USE_ALLOWED:Ljava/lang/String; = "WLAN_DATA_USE_ALLOWED"

.field public static final SOCIAL_PREFIX:Ljava/lang/String; = "social "

.field public static final TOAST_SHOW_TIME:I = 0x3e8

.field public static final UPDATE:Ljava/lang/String; = "update"

.field public static final USE_SYSTEM_LOCALE:Ljava/lang/String; = "system_locale"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method
