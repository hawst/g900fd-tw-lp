.class public Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;
.super Ljava/util/concurrent/ConcurrentHashMap;
.source "HeadlinesSnapshot.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/ConcurrentHashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/samsung/android/internal/headlines/HeadlinesItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x10f4918387b8e41cL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p1, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesItem;>;"
    invoke-direct {p0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 27
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 29
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .line 30
    .local v1, "item":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    invoke-virtual {p0, v1}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->put(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Z

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    .end local v1    # "item":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    :cond_0
    return-void
.end method


# virtual methods
.method public getLatestItems()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/internal/headlines/HeadlinesItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 17
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesItem;>;"
    invoke-virtual {p0}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 18
    .local v0, "col":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/internal/headlines/HeadlinesItem;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 20
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 22
    :cond_0
    return-object v1
.end method

.method public put(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Z
    .locals 6
    .param p1, "item"    # Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .prologue
    .line 47
    const/4 v1, 0x0

    .line 48
    .local v1, "insert":Z
    iget-object v2, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 50
    const/4 v1, 0x1

    .line 61
    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    .line 63
    iget-object v2, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {p0, v2, p1}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    const/4 v2, 0x1

    .line 66
    :goto_1
    return v2

    .line 54
    :cond_1
    iget-object v2, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .line 55
    .local v0, "currentItem":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    iget-wide v2, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->dataCreated:J

    iget-wide v4, v0, Lcom/samsung/android/internal/headlines/HeadlinesItem;->dataCreated:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 57
    iget-object v2, p1, Lcom/samsung/android/internal/headlines/HeadlinesItem;->category:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    const/4 v1, 0x1

    goto :goto_0

    .line 66
    .end local v0    # "currentItem":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public refresh(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Lcom/samsung/android/internal/headlines/HeadlinesItem;
    .locals 2
    .param p1, "item"    # Lcom/samsung/android/internal/headlines/HeadlinesItem;

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesSnapshot;->put(Lcom/samsung/android/internal/headlines/HeadlinesItem;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 42
    .end local p1    # "item":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    :goto_0
    return-object p1

    .restart local p1    # "item":Lcom/samsung/android/internal/headlines/HeadlinesItem;
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method
