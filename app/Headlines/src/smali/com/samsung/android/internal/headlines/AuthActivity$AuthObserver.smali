.class Lcom/samsung/android/internal/headlines/AuthActivity$AuthObserver;
.super Ljava/lang/Object;
.source "AuthActivity.java"

# interfaces
.implements Lcom/flipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/internal/headlines/AuthActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AuthObserver"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/flipboard/util/Observer",
        "<",
        "Lcom/flipboard/data/AuthorizationClient;",
        "Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;",
        "Lcom/flipboard/data/AuthorizationClient$Response;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public notify(Lcom/flipboard/data/AuthorizationClient;Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;Lcom/flipboard/data/AuthorizationClient$Response;)V
    .locals 4
    .param p1, "observable"    # Lcom/flipboard/data/AuthorizationClient;
    .param p2, "msg"    # Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;
    .param p3, "response"    # Lcom/flipboard/data/AuthorizationClient$Response;

    .prologue
    const/4 v3, 0x0

    .line 21
    sget-object v0, Lcom/samsung/android/internal/headlines/AuthActivity$1;->$SwitchMap$com$flipboard$data$AuthorizationClient$AuthorizeMessage:[I

    invoke-virtual {p2}, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 29
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "unsupported message"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    :goto_0
    invoke-virtual {p1, p0}, Lcom/flipboard/data/AuthorizationClient;->removeObserver(Lcom/flipboard/util/Observer;)V

    .line 34
    return-void

    .line 23
    :pswitch_0
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "success message"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 26
    :pswitch_1
    sget-object v0, Lcom/flipboard/util/Log;->main:Lcom/flipboard/util/Log;

    const-string v1, "error message: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/flipboard/util/Log;->error(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 21
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic notify(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, Lcom/flipboard/data/AuthorizationClient;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;

    .end local p2    # "x1":Ljava/lang/Object;
    check-cast p3, Lcom/flipboard/data/AuthorizationClient$Response;

    .end local p3    # "x2":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/internal/headlines/AuthActivity$AuthObserver;->notify(Lcom/flipboard/data/AuthorizationClient;Lcom/flipboard/data/AuthorizationClient$AuthorizeMessage;Lcom/flipboard/data/AuthorizationClient$Response;)V

    return-void
.end method
