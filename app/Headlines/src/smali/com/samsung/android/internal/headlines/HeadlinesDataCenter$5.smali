.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->queryCategory(Ljava/util/Observer;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

.field final synthetic val$observer:Ljava/util/Observer;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/Observer;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->val$observer:Ljava/util/Observer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 2
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 279
    if-nez p2, :cond_0

    .line 280
    const-string v0, "Headlines"

    const-string v1, "queryCategory.  mRequest.getLocales returns null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->val$observer:Ljava/util/Observer;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 322
    .end local p2    # "arg1":Ljava/lang/Object;
    :goto_0
    return-void

    .line 284
    .restart local p2    # "arg1":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mRequest:Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    invoke-static {v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$500(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;)Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    .end local p2    # "arg1":Ljava/lang/Object;
    new-instance v1, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5$1;-><init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$5;)V

    invoke-virtual {v0, p2, v1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->queryCategory(Ljava/lang/String;Ljava/util/Observer;)V

    goto :goto_0
.end method
