.class Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;
.super Ljava/lang/Object;
.source "HeadlinesCardManager.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestSocialAction(Ljava/lang/String;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

.field final synthetic val$category:Ljava/lang/String;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1378
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;->val$category:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 11
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    const/4 v10, 0x1

    .line 1383
    if-eqz p2, :cond_2

    instance-of v8, p2, Ljava/util/List;

    if-eqz v8, :cond_2

    move-object v6, p2

    .line 1385
    check-cast v6, Ljava/util/List;

    .line 1386
    .local v6, "socialList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    if-ge v2, v8, :cond_2

    .line 1387
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    .line 1388
    .local v5, "social":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    if-nez v5, :cond_1

    .line 1386
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1391
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;->val$category:Ljava/lang/String;

    iget-object v9, v5, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-ne v8, v10, :cond_0

    .line 1392
    iget-boolean v8, v5, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->active:Z

    if-ne v8, v10, :cond_5

    .line 1393
    const-string v8, "HeadlinesCardManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "requestSocialAction : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v5, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v5, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->name:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") is login."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1395
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;->val$intent:Landroid/content/Intent;

    const-string v9, "jsonObject"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1396
    .local v7, "stringJsonObject":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;->val$intent:Landroid/content/Intent;

    const-string v9, "displayName"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1397
    .local v0, "displayName":Ljava/lang/String;
    if-eqz v7, :cond_4

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_4

    .line 1399
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1400
    .local v3, "jsonObject":Lorg/json/JSONObject;
    iget-object v8, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$200(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mHeadlinesDataCenter:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    invoke-static {v9}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$500(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->getServiceForItem(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v9

    invoke-static {v8, v3, v9, v0}, Lcom/flipboard/util/IntentUtil;->getDetailIntent(Landroid/content/Context;Lorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 1402
    .local v4, "sIntent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$200(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v4}, Lcom/flipboard/util/IntentUtil;->activityExists(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1403
    const-string v8, "HeadlinesCardManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "requestSocialAction : startActivity - intent = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1404
    iget-object v8, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->access$200(Lcom/samsung/android/internal/headlines/HeadlinesCardManager;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1424
    .end local v0    # "displayName":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .end local v4    # "sIntent":Landroid/content/Intent;
    .end local v5    # "social":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .end local v6    # "socialList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    .end local v7    # "stringJsonObject":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 1406
    .restart local v0    # "displayName":Ljava/lang/String;
    .restart local v2    # "i":I
    .restart local v3    # "jsonObject":Lorg/json/JSONObject;
    .restart local v4    # "sIntent":Landroid/content/Intent;
    .restart local v5    # "social":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .restart local v6    # "socialList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    .restart local v7    # "stringJsonObject":Ljava/lang/String;
    :cond_3
    const-string v8, "HeadlinesCardManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "requestSocialAction : checkServiceLogInState - No activity found for intent = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1412
    .end local v0    # "displayName":Ljava/lang/String;
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .end local v4    # "sIntent":Landroid/content/Intent;
    .end local v7    # "stringJsonObject":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1413
    .local v1, "e":Lorg/json/JSONException;
    const-string v8, "HeadlinesCardManager"

    const-string v9, "requestSocialAction : lauching flipboard app is failed."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1414
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 1409
    .end local v1    # "e":Lorg/json/JSONException;
    .restart local v0    # "displayName":Ljava/lang/String;
    .restart local v7    # "stringJsonObject":Ljava/lang/String;
    :cond_4
    :try_start_1
    const-string v8, "HeadlinesCardManager"

    const-string v9, "requestSocialAction : checkServiceLogInState - No JSONObject"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1417
    .end local v0    # "displayName":Ljava/lang/String;
    .end local v7    # "stringJsonObject":Ljava/lang/String;
    :cond_5
    const-string v8, "HeadlinesCardManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "requestSocialAction : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v5, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is not login."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1418
    iget-object v8, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesCardManager;

    iget-object v9, p0, Lcom/samsung/android/internal/headlines/HeadlinesCardManager$4;->val$category:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/internal/headlines/HeadlinesCardManager;->requestServiceLogin(Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_1
.end method
