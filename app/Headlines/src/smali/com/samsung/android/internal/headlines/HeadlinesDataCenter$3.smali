.class Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$3;
.super Ljava/lang/Object;
.source "HeadlinesDataCenter.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->getService(Ljava/util/Observer;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

.field final synthetic val$observer:Ljava/util/Observer;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/Observer;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$3;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$3;->val$observer:Ljava/util/Observer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3
    .param p1, "arg0"    # Ljava/util/Observable;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 228
    if-eqz p2, :cond_0

    instance-of v0, p2, Ljava/util/List;

    if-nez v0, :cond_1

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$3;->val$observer:Ljava/util/Observer;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->instance:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$200()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 231
    const-string v0, "Headlines"

    const-string v1, "getService. return null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$3;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-object v0, p2

    check-cast v0, Ljava/util/List;

    # setter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->mSocialList:Ljava/util/List;
    invoke-static {v1, v0}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$302(Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;Ljava/util/List;)Ljava/util/List;

    .line 235
    iget-object v0, p0, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter$3;->val$observer:Ljava/util/Observer;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->instance:Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;->access$200()Lcom/samsung/android/internal/headlines/HeadlinesDataCenter;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    goto :goto_0
.end method
