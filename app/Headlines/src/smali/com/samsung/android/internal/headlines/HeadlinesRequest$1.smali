.class Lcom/samsung/android/internal/headlines/HeadlinesRequest$1;
.super Lcom/flipboard/data/Request$FLSuccessListener;
.source "HeadlinesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/internal/headlines/HeadlinesRequest;->queryCategory(Ljava/lang/String;Ljava/util/Observer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

.field final synthetic val$observer:Ljava/util/Observer;


# direct methods
.method constructor <init>(Lcom/samsung/android/internal/headlines/HeadlinesRequest;Ljava/util/Observer;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$1;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    iput-object p2, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$1;->val$observer:Ljava/util/Observer;

    invoke-direct {p0}, Lcom/flipboard/data/Request$FLSuccessListener;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 145
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/internal/headlines/HeadlinesRequest$1;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 12
    .param p1, "response"    # Lorg/json/JSONObject;

    .prologue
    const/4 v11, 0x1

    .line 148
    const-string v8, "HeadlinesRequest"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "HeadlinesRequest response of Request.getCategories"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :try_start_0
    const-string v8, "categories"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 152
    .local v0, "categories":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    .line 153
    .local v2, "count":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 154
    .local v1, "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v2, :cond_1

    .line 157
    :try_start_1
    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v7

    .line 163
    .local v7, "obj":Lorg/json/JSONObject;
    :try_start_2
    new-instance v6, Lcom/samsung/android/internal/headlines/HeadlinesCategory;

    invoke-direct {v6}, Lcom/samsung/android/internal/headlines/HeadlinesCategory;-><init>()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 165
    .local v6, "item":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    :try_start_3
    const-string v8, "id"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    .line 172
    :try_start_4
    const-string v8, "name"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->name:Ljava/lang/String;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_3

    .line 180
    const/4 v8, 0x1

    :try_start_5
    iput-boolean v8, v6, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->active:Z

    .line 182
    iget-object v8, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$1;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    invoke-virtual {v8, v6}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->checkSupportedCategory(Lcom/samsung/android/internal/headlines/HeadlinesCategory;)Z

    move-result v8

    if-ne v8, v11, :cond_0

    .line 183
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    const-string v8, "HeadlinesRequest"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Category was added. id : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v6, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " name : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v6, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->name:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    .end local v6    # "item":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .end local v7    # "obj":Lorg/json/JSONObject;
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 158
    :catch_0
    move-exception v3

    .line 160
    .local v3, "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 191
    .end local v0    # "categories":Lorg/json/JSONArray;
    .end local v1    # "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    .end local v2    # "count":I
    .end local v3    # "e":Lorg/json/JSONException;
    .end local v5    # "i":I
    :catch_1
    move-exception v4

    .line 193
    .local v4, "e1":Lorg/json/JSONException;
    const-string v8, "HeadlinesRequest"

    const-string v9, "Category json data is wrong. There is no categories field."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v8, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$1;->val$observer:Ljava/util/Observer;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$000()Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 195
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    .line 198
    .end local v4    # "e1":Lorg/json/JSONException;
    :goto_2
    return-void

    .line 166
    .restart local v0    # "categories":Lorg/json/JSONArray;
    .restart local v1    # "categoryList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/internal/headlines/HeadlinesCategory;>;"
    .restart local v2    # "count":I
    .restart local v5    # "i":I
    .restart local v6    # "item":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .restart local v7    # "obj":Lorg/json/JSONObject;
    :catch_2
    move-exception v3

    .line 168
    .restart local v3    # "e":Lorg/json/JSONException;
    :try_start_6
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 173
    .end local v3    # "e":Lorg/json/JSONException;
    :catch_3
    move-exception v3

    .line 175
    .restart local v3    # "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 186
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_0
    const-string v8, "HeadlinesRequest"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Category was not supported. id : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v6, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->id:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " name : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v6, Lcom/samsung/android/internal/headlines/HeadlinesCategory;->name:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 189
    .end local v6    # "item":Lcom/samsung/android/internal/headlines/HeadlinesCategory;
    .end local v7    # "obj":Lorg/json/JSONObject;
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$1;->this$0:Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    iput-object v1, v8, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->mCategoryList:Ljava/util/List;

    .line 190
    iget-object v8, p0, Lcom/samsung/android/internal/headlines/HeadlinesRequest$1;->val$observer:Ljava/util/Observer;

    # getter for: Lcom/samsung/android/internal/headlines/HeadlinesRequest;->instance:Lcom/samsung/android/internal/headlines/HeadlinesRequest;
    invoke-static {}, Lcom/samsung/android/internal/headlines/HeadlinesRequest;->access$000()Lcom/samsung/android/internal/headlines/HeadlinesRequest;

    move-result-object v9

    invoke-interface {v8, v9, v1}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2
.end method
