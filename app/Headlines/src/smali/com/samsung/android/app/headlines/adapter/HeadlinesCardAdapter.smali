.class public Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;
.super Landroid/widget/BaseAdapter;
.source "HeadlinesCardAdapter.java"

# interfaces
.implements Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;
.implements Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$2;,
        Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;,
        Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;
    }
.end annotation


# static fields
.field private static SELF_REFRESH_ANIMATION_STOP_TIMER:I = 0x0

.field private static final TAG:Ljava/lang/String; = "HeadlinesCardAdapter"


# instance fields
.field private mCardList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/Card;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mEventQueue:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mLock:Ljava/lang/Object;

.field private mNewCardNotification:Lcom/samsung/android/app/headlines/NewCardNotification;

.field private mNonDroppablePositions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPrevIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/16 v0, 0x2710

    sput v0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->SELF_REFRESH_ANIMATION_STOP_TIMER:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 48
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 30
    iput-object v11, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mContext:Landroid/content/Context;

    .line 31
    const/4 v8, -0x1

    iput v8, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mPrevIndex:I

    .line 33
    iput-object v11, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    .line 36
    new-instance v8, Ljava/util/Vector;

    invoke-direct {v8}, Ljava/util/Vector;-><init>()V

    iput-object v8, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mEventQueue:Ljava/util/Vector;

    .line 37
    new-instance v8, Ljava/lang/Object;

    invoke-direct {v8}, Ljava/lang/Object;-><init>()V

    iput-object v8, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mLock:Ljava/lang/Object;

    .line 40
    iput-object v11, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mNonDroppablePositions:Ljava/util/ArrayList;

    .line 42
    iput-object v11, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mNewCardNotification:Lcom/samsung/android/app/headlines/NewCardNotification;

    .line 49
    const-string v8, "Headlines"

    const-string v9, "HeadlinesCardAdapter constructor"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iput-object p1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mContext:Landroid/content/Context;

    .line 52
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v8

    invoke-virtual {v8, p0}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->addCardChangeEventListener(Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;)V

    .line 53
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getCards()Ljava/util/List;

    move-result-object v1

    .line 55
    .local v1, "cardList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    iget-object v8, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mContext:Landroid/content/Context;

    const-string v9, "CardOrder"

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 56
    .local v6, "pref":Landroid/content/SharedPreferences;
    const-string v8, "IsFirstLaunch"

    invoke-interface {v6, v8, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 57
    .local v4, "isFirstLaunch":Ljava/lang/Boolean;
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-ne v8, v12, :cond_0

    .line 58
    const-string v8, "HeadlinesCardAdapter"

    const-string v9, "HeadlinesCardAdapter() : Application is launched for the first."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    iput-object v1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    .line 118
    :goto_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mNonDroppablePositions:Ljava/util/ArrayList;

    .line 119
    return-void

    .line 62
    :cond_0
    const-string v8, "HeadlinesCardAdapter"

    const-string v9, "HeadlinesCardAdapter() : Application is not launched for the first."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    new-instance v8, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v8, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    .line 65
    const-string v8, "CardCount"

    invoke-interface {v6, v8, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 67
    .local v7, "savedCardCount":I
    const-string v8, "HeadlinesCardAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "HeadlinesCardAdapter() : DB saved Category Count : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const-string v8, "HeadlinesCardAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "HeadlinesCardAdapter() : Current Category Count : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const-string v8, "HeadlinesCardAdapter"

    const-string v9, "HeadlinesCardAdapter() : ============== Card Order =============="

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v7, :cond_3

    .line 72
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 73
    .local v2, "category":Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v5, v8, :cond_1

    .line 74
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 75
    .local v0, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 76
    iget-object v8, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 78
    const-string v8, "HeadlinesCardAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "HeadlinesCardAdapter() : CardName = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is added."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    .end local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 73
    .restart local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 84
    .end local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    .end local v2    # "category":Ljava/lang/String;
    .end local v5    # "j":I
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v8, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 85
    const/4 v3, 0x0

    :goto_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v3, v8, :cond_5

    .line 86
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 87
    .restart local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 88
    const-string v8, "HeadlinesCardAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "HeadlinesCardAdapter() : CardName = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is added."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 92
    .end local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 115
    const-string v8, "HeadlinesCardAdapter"

    const-string v9, "HeadlinesCardAdapter() : ========================================"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mEventQueue:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;
    .param p1, "x1"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->addCard(Lcom/samsung/android/magazine/cardchannel/Card;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;
    .param p1, "x1"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->updateCard(Lcom/samsung/android/magazine/cardchannel/Card;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->removeCard(I)V

    return-void
.end method

.method private addCard(Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 5
    .param p1, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 251
    if-eqz p1, :cond_1

    .line 252
    const/4 v1, 0x0

    .line 253
    .local v1, "found":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 254
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v3

    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 255
    const/4 v1, 0x1

    .line 259
    :cond_0
    if-nez v1, :cond_1

    .line 260
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mNewCardNotification:Lcom/samsung/android/app/headlines/NewCardNotification;

    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/samsung/android/app/headlines/NewCardNotification;->onNewNotificationCardAdded(I)V

    .line 263
    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardchannel/Card;->getTemplate()Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "cardTemplate":Ljava/lang/String;
    const-string v3, "template_welcome_1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardchannel/Card;->getProviderName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.samsung.android.app.headlines"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 265
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->requestRefresh(I)V

    .line 269
    .end local v0    # "cardTemplate":Ljava/lang/String;
    .end local v1    # "found":Z
    .end local v2    # "i":I
    :cond_1
    return-void

    .line 253
    .restart local v1    # "found":Z
    .restart local v2    # "i":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private preloadImage(IZ)V
    .locals 4
    .param p1, "currentIndex"    # I
    .param p2, "scrollDown"    # Z

    .prologue
    .line 406
    const/4 v2, 0x1

    .line 407
    .local v2, "step":I
    const/4 v1, -0x1

    .line 408
    .local v1, "preloadIndex":I
    :goto_0
    const/4 v3, 0x5

    if-ge v2, v3, :cond_0

    .line 409
    if-nez p2, :cond_1

    const/4 v3, -0x1

    :goto_1
    mul-int/2addr v3, v2

    add-int v1, p1, v3

    .line 410
    if-gez v1, :cond_2

    .line 418
    :cond_0
    return-void

    .line 409
    :cond_1
    const/4 v3, 0x1

    goto :goto_1

    .line 412
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-gt v1, v3, :cond_0

    .line 414
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 415
    .local v0, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->preloadImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    .line 416
    add-int/lit8 v2, v2, 0x1

    .line 417
    goto :goto_0
.end method

.method private processCardEvent()V
    .locals 2

    .prologue
    .line 313
    const-string v0, "HeadlinesCardAdapter"

    const-string v1, "processCardEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;-><init>(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 352
    return-void
.end method

.method private removeCard(I)V
    .locals 4
    .param p1, "cardId"    # I

    .prologue
    .line 271
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 272
    iget-object v2, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 273
    .local v0, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 274
    iget-object v2, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 275
    iget-object v2, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mNewCardNotification:Lcom/samsung/android/app/headlines/NewCardNotification;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/samsung/android/app/headlines/NewCardNotification;->onNewNotificationCardRemoved(I)V

    .line 279
    .end local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :cond_0
    return-void

    .line 271
    .restart local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private updateCard(Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 4
    .param p1, "updatedCard"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 281
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 282
    iget-object v2, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 283
    .local v0, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v2

    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 284
    iget-object v2, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v2, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 288
    .end local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :cond_0
    return-void

    .line 281
    .restart local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public allowDrag(I)Z
    .locals 2
    .param p1, "startPos"    # I

    .prologue
    .line 292
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mNonDroppablePositions:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public allowDrop(II)Z
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "destPos"    # I

    .prologue
    .line 297
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mNonDroppablePositions:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->removeCardChangeEventListener(Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;)V

    .line 46
    return-void
.end method

.method public dropDone(II)V
    .locals 4
    .param p1, "startPos"    # I
    .param p2, "destPos"    # I

    .prologue
    .line 302
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->getItem(I)Lcom/samsung/android/magazine/cardchannel/Card;

    move-result-object v0

    .line 304
    .local v0, "draggedCard":Lcom/samsung/android/magazine/cardchannel/Card;
    iget-object v1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 305
    iget-object v1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 307
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->notifyDataSetChanged()V

    .line 309
    const-string v1, "HeadlinesCardAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dropDone() : startPos = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " destPos = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    return-void
.end method

.method public getCardList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/Card;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 132
    const/4 v0, 0x0

    .line 134
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/samsung/android/magazine/cardchannel/Card;
    .locals 3
    .param p1, "listIndex"    # I

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 155
    const/4 v0, 0x0

    .line 159
    :goto_0
    return-object v0

    .line 157
    :cond_0
    const-string v0, "HeadlinesCardAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[getItem] index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->getItem(I)Lcom/samsung/android/magazine/cardchannel/Card;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 3
    .param p1, "listIndex"    # I

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 165
    const-wide/16 v0, 0x0

    .line 169
    :goto_0
    return-wide v0

    .line 167
    :cond_0
    const-string v0, "HeadlinesCardAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[getItemId] index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 139
    iget-object v1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    if-nez v1, :cond_0

    .line 140
    const/4 v1, 0x0

    .line 144
    :goto_0
    return v1

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 144
    .local v0, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getTemplate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/headlines/template/CardTemplateFactory;->getTemplateType(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "listIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x1

    .line 175
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 176
    .local v0, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    iget v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mPrevIndex:I

    sub-int/2addr v3, p1

    if-lez v3, :cond_2

    const/4 v2, 0x0

    .line 177
    .local v2, "scrollDown":Z
    :goto_0
    iput p1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mPrevIndex:I

    .line 178
    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getTemplate()Ljava/lang/String;

    move-result-object v1

    .line 180
    .local v1, "cardTemplate":Ljava/lang/String;
    if-nez p2, :cond_3

    .line 182
    new-instance p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    .end local p2    # "convertView":Landroid/view/View;
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p2, v3, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    .line 196
    .restart local p2    # "convertView":Landroid/view/View;
    :goto_1
    invoke-direct {p0, p1, v2}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->preloadImage(IZ)V

    .line 199
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getRefreshState()Z

    move-result v3

    if-ne v3, v4, :cond_0

    move-object v3, p2

    .line 200
    check-cast v3, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->showLoadingViews()V

    .line 203
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getLastModifiedTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    sget v3, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->SELF_REFRESH_ANIMATION_STOP_TIMER:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-gez v3, :cond_1

    .line 204
    const-string v3, "template_welcome_1"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v3, p2

    .line 205
    check-cast v3, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->showLoadingViews()V

    .line 209
    :cond_1
    return-object p2

    .end local v1    # "cardTemplate":Ljava/lang/String;
    .end local v2    # "scrollDown":Z
    :cond_2
    move v2, v4

    .line 176
    goto :goto_0

    .restart local v1    # "cardTemplate":Ljava/lang/String;
    .restart local v2    # "scrollDown":Z
    :cond_3
    move-object v3, p2

    .line 185
    check-cast v3, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->getTemplate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object v3, p2

    .line 187
    check-cast v3, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    invoke-virtual {v3, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->updateCardView(Lcom/samsung/android/magazine/cardchannel/Card;)V

    goto :goto_1

    .line 191
    :cond_4
    new-instance p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    .end local p2    # "convertView":Landroid/view/View;
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p2, v3, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    .restart local p2    # "convertView":Landroid/view/View;
    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 149
    invoke-static {}, Lcom/samsung/android/app/headlines/template/CardTemplateFactory;->getTemplateCount()I

    move-result v0

    return v0
.end method

.method public onCardAdded(I)V
    .locals 5
    .param p1, "cardId"    # I

    .prologue
    .line 214
    iget-object v1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    if-nez v1, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    const-string v1, "HeadlinesCardAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onCardAdded] CardId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getCard(I)Lcom/samsung/android/magazine/cardchannel/Card;

    move-result-object v0

    .line 220
    .local v0, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    if-eqz v0, :cond_0

    .line 222
    iget-object v2, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 223
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mEventQueue:Ljava/util/Vector;

    new-instance v3, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;

    sget-object v4, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;->ADD:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;

    invoke-direct {v3, v0, p1, v4}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;-><init>(Lcom/samsung/android/magazine/cardchannel/Card;ILcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;)V

    invoke-virtual {v1, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 224
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->processCardEvent()V

    goto :goto_0

    .line 224
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onCardRemoved(I)V
    .locals 5
    .param p1, "cardId"    # I

    .prologue
    .line 230
    const-string v0, "HeadlinesCardAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onCardRemoved] CardId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    iget-object v1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 233
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mEventQueue:Ljava/util/Vector;

    new-instance v2, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;

    const/4 v3, 0x0

    sget-object v4, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;->REMOVE:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;

    invoke-direct {v2, v3, p1, v4}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;-><init>(Lcom/samsung/android/magazine/cardchannel/Card;ILcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;)V

    invoke-virtual {v0, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 234
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->processCardEvent()V

    .line 236
    return-void

    .line 234
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onCardUpdated(I)V
    .locals 5
    .param p1, "cardId"    # I

    .prologue
    .line 240
    const-string v1, "HeadlinesCardAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onCardUpdated] CardId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getCard(I)Lcom/samsung/android/magazine/cardchannel/Card;

    move-result-object v0

    .line 242
    .local v0, "updatedCard":Lcom/samsung/android/magazine/cardchannel/Card;
    if-nez v0, :cond_0

    .line 248
    :goto_0
    return-void

    .line 244
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 245
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mEventQueue:Ljava/util/Vector;

    new-instance v3, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;

    sget-object v4, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;->UPDATE:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;

    invoke-direct {v3, v0, p1, v4}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;-><init>(Lcom/samsung/android/magazine/cardchannel/Card;ILcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;)V

    invoke-virtual {v1, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 246
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->processCardEvent()V

    goto :goto_0

    .line 246
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public setCardList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/Card;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p1, "cardList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    iput-object p1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mCardList:Ljava/util/List;

    .line 127
    return-void
.end method

.method public setOnNewCardNotification(Lcom/samsung/android/app/headlines/NewCardNotification;)V
    .locals 2
    .param p1, "notification"    # Lcom/samsung/android/app/headlines/NewCardNotification;

    .prologue
    .line 397
    if-nez p1, :cond_0

    .line 398
    const-string v0, "HeadlinesCardAdapter"

    const-string v1, "Failed to add notification. notification is null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    :goto_0
    return-void

    .line 402
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mNewCardNotification:Lcom/samsung/android/app/headlines/NewCardNotification;

    goto :goto_0
.end method
