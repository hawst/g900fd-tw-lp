.class final enum Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;
.super Ljava/lang/Enum;
.source "HeadlinesPortraitMainContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ScrollDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

.field public static final enum DIRECTION_HORIZONTAL:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

.field public static final enum DIRECTION_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

.field public static final enum DIRECTION_VERTICAL:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 97
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    const-string v1, "DIRECTION_NONE"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    .line 98
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    const-string v1, "DIRECTION_HORIZONTAL"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_HORIZONTAL:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    .line 99
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    const-string v1, "DIRECTION_VERTICAL"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_VERTICAL:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    .line 96
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_HORIZONTAL:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_VERTICAL:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->$VALUES:[Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 96
    const-class v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->$VALUES:[Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    invoke-virtual {v0}, [Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    return-object v0
.end method
