.class public Lcom/samsung/android/app/headlines/update/VersionParser;
.super Ljava/lang/Object;
.source "VersionParser.java"


# static fields
.field public static final MARKET_GOOGLE_PLAY:Ljava/lang/String; = "GooglePlay"

.field public static final MARKET_SAMSUNG_APPS:Ljava/lang/String; = "SamsungApps"

.field public static final MARKET_UNKNOWN:Ljava/lang/String; = "Unknown"

.field private static final TAG:Ljava/lang/String;

.field private static final UNKNOWN:Ljava/lang/String; = "-1"


# instance fields
.field private forceUpdateVersion:Ljava/lang/String;

.field private globalMarketIndex:I

.field private globalMarkets:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private googlePlayGlobalVersion:Ljava/lang/String;

.field private googlePlayModelVersion:Ljava/lang/String;

.field private googlePlayModelVersionByOsVersion:Ljava/lang/String;

.field private modelMarketIndex:I

.field private modelMarkets:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/samsung/android/app/headlines/update/VersionParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/headlines/update/VersionParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;I)V
    .locals 10
    .param p1, "verTxtUri"    # Landroid/net/Uri;
    .param p2, "myDeviceModel"    # Ljava/lang/String;
    .param p3, "osVersion"    # I

    .prologue
    const/4 v7, 0x1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string v6, "-1"

    iput-object v6, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->forceUpdateVersion:Ljava/lang/String;

    .line 27
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->globalMarkets:Ljava/util/HashMap;

    .line 28
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->modelMarkets:Ljava/util/HashMap;

    .line 29
    const-string v6, "-1"

    iput-object v6, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayGlobalVersion:Ljava/lang/String;

    .line 30
    const-string v6, "-1"

    iput-object v6, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayModelVersion:Ljava/lang/String;

    .line 31
    const-string v6, "-1"

    iput-object v6, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayModelVersionByOsVersion:Ljava/lang/String;

    .line 32
    iput v7, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->globalMarketIndex:I

    .line 33
    iput v7, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->modelMarketIndex:I

    .line 47
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/update/VersionParser;->initValue()V

    .line 52
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v6

    invoke-virtual {v6}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v1

    .line 53
    .local v1, "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilder;->reset()V

    .line 54
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 55
    .local v0, "doc":Lorg/w3c/dom/Document;
    const-string v6, "update"

    invoke-direct {p0, v0, v6}, Lcom/samsung/android/app/headlines/update/VersionParser;->getSingleElement(Lorg/w3c/dom/Document;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 56
    .local v4, "node":Lorg/w3c/dom/Node;
    if-eqz v4, :cond_2

    .line 57
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 58
    .local v3, "nl":Lorg/w3c/dom/NodeList;
    const-string v6, "force"

    invoke-direct {p0, v3, v6}, Lcom/samsung/android/app/headlines/update/VersionParser;->getSingleContent(Lorg/w3c/dom/NodeList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->forceUpdateVersion:Ljava/lang/String;

    .line 60
    const-string v6, "market-config"

    invoke-direct {p0, v3, v6}, Lcom/samsung/android/app/headlines/update/VersionParser;->getSingleElement(Lorg/w3c/dom/NodeList;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 61
    if-eqz v4, :cond_0

    .line 62
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 63
    .local v5, "nodelist":Lorg/w3c/dom/NodeList;
    const-string v6, "global"

    invoke-direct {p0, v5, v6}, Lcom/samsung/android/app/headlines/update/VersionParser;->getSingleElement(Lorg/w3c/dom/NodeList;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 64
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 65
    const-string v6, "market"

    const-string v7, "index"

    invoke-direct {p0, v5, v6, v7}, Lcom/samsung/android/app/headlines/update/VersionParser;->getMultipleContent(Lorg/w3c/dom/NodeList;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->globalMarkets:Ljava/util/HashMap;

    .line 68
    .end local v5    # "nodelist":Lorg/w3c/dom/NodeList;
    :cond_0
    const-string v6, "market-config"

    invoke-direct {p0, v3, v6}, Lcom/samsung/android/app/headlines/update/VersionParser;->getSingleElement(Lorg/w3c/dom/NodeList;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 69
    if-eqz v4, :cond_1

    .line 70
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 71
    .restart local v5    # "nodelist":Lorg/w3c/dom/NodeList;
    const-string v6, "target"

    const-string v7, "model"

    invoke-direct {p0, v5, v6, v7, p2}, Lcom/samsung/android/app/headlines/update/VersionParser;->getSingleElementByAttribute(Lorg/w3c/dom/NodeList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 72
    if-eqz v4, :cond_1

    .line 73
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 74
    const-string v6, "market"

    const-string v7, "index"

    invoke-direct {p0, v5, v6, v7}, Lcom/samsung/android/app/headlines/update/VersionParser;->getMultipleContent(Lorg/w3c/dom/NodeList;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->modelMarkets:Ljava/util/HashMap;

    .line 78
    .end local v5    # "nodelist":Lorg/w3c/dom/NodeList;
    :cond_1
    const-string v6, "google"

    invoke-direct {p0, v3, v6}, Lcom/samsung/android/app/headlines/update/VersionParser;->getSingleElement(Lorg/w3c/dom/NodeList;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 79
    if-eqz v4, :cond_2

    .line 80
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 81
    const-string v6, "global"

    invoke-direct {p0, v3, v6}, Lcom/samsung/android/app/headlines/update/VersionParser;->getSingleContent(Lorg/w3c/dom/NodeList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayGlobalVersion:Ljava/lang/String;

    .line 82
    const-string v6, "target"

    const-string v7, "model"

    invoke-direct {p0, v3, v6, v7, p2}, Lcom/samsung/android/app/headlines/update/VersionParser;->getSingleContentByAttribute(Lorg/w3c/dom/NodeList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayModelVersion:Ljava/lang/String;

    .line 83
    const-string v6, "os"

    const-string v7, "level"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v3, v6, v7, v8}, Lcom/samsung/android/app/headlines/update/VersionParser;->getSingleElementByAttribute(Lorg/w3c/dom/NodeList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 84
    if-eqz v4, :cond_2

    .line 85
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 86
    .restart local v5    # "nodelist":Lorg/w3c/dom/NodeList;
    const-string v6, "target"

    const-string v7, "model"

    invoke-direct {p0, v5, v6, v7, p2}, Lcom/samsung/android/app/headlines/update/VersionParser;->getSingleContentByAttribute(Lorg/w3c/dom/NodeList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayModelVersionByOsVersion:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    .end local v0    # "doc":Lorg/w3c/dom/Document;
    .end local v1    # "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v3    # "nl":Lorg/w3c/dom/NodeList;
    .end local v4    # "node":Lorg/w3c/dom/Node;
    .end local v5    # "nodelist":Lorg/w3c/dom/NodeList;
    :cond_2
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v2

    .line 92
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getAttribute(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "node"    # Lorg/w3c/dom/Node;
    .param p2, "attrName"    # Ljava/lang/String;

    .prologue
    .line 267
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v1

    .line 268
    .local v1, "nnm":Lorg/w3c/dom/NamedNodeMap;
    invoke-interface {v1, p2}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 269
    .local v0, "n":Lorg/w3c/dom/Node;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getMultipleContent(Lorg/w3c/dom/NodeList;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 7
    .param p1, "nl"    # Lorg/w3c/dom/NodeList;
    .param p2, "tagName"    # Ljava/lang/String;
    .param p3, "attrName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/NodeList;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    const/4 v4, 0x0

    .line 235
    .local v4, "retVal":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 236
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 237
    invoke-interface {p1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 238
    .local v3, "n":Lorg/w3c/dom/Node;
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 239
    invoke-direct {p0, v3, p3}, Lcom/samsung/android/app/headlines/update/VersionParser;->getAttribute(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 240
    .local v2, "model":Ljava/lang/String;
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v5

    .line 241
    .local v5, "ver":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 242
    if-nez v4, :cond_0

    .line 243
    new-instance v4, Ljava/util/HashMap;

    .end local v4    # "retVal":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 245
    .restart local v4    # "retVal":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {v4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    .end local v2    # "model":Ljava/lang/String;
    .end local v5    # "ver":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 249
    .end local v3    # "n":Lorg/w3c/dom/Node;
    :cond_2
    return-object v4
.end method

.method private getSingleContent(Lorg/w3c/dom/NodeList;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "nl"    # Lorg/w3c/dom/NodeList;
    .param p2, "tagName"    # Ljava/lang/String;

    .prologue
    .line 222
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 223
    .local v1, "len":I
    const/4 v3, 0x0

    .line 224
    .local v3, "retStr":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 225
    invoke-interface {p1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 226
    .local v2, "n":Lorg/w3c/dom/Node;
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 227
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v3

    .line 224
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 230
    .end local v2    # "n":Lorg/w3c/dom/Node;
    :cond_1
    return-object v3
.end method

.method private getSingleContentByAttribute(Lorg/w3c/dom/NodeList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "nl"    # Lorg/w3c/dom/NodeList;
    .param p2, "tagName"    # Ljava/lang/String;
    .param p3, "attrName"    # Ljava/lang/String;
    .param p4, "myDeviceModel"    # Ljava/lang/String;

    .prologue
    .line 253
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 254
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 255
    invoke-interface {p1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 256
    .local v3, "n":Lorg/w3c/dom/Node;
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 257
    invoke-direct {p0, v3, p3}, Lcom/samsung/android/app/headlines/update/VersionParser;->getAttribute(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 258
    .local v2, "model":Ljava/lang/String;
    invoke-virtual {p4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 259
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v4

    .line 263
    .end local v2    # "model":Ljava/lang/String;
    .end local v3    # "n":Lorg/w3c/dom/Node;
    :goto_1
    return-object v4

    .line 254
    .restart local v3    # "n":Lorg/w3c/dom/Node;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 263
    .end local v3    # "n":Lorg/w3c/dom/Node;
    :cond_1
    const-string v4, "-1"

    goto :goto_1
.end method

.method private getSingleElement(Lorg/w3c/dom/Document;Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 2
    .param p1, "doc"    # Lorg/w3c/dom/Document;
    .param p2, "tagName"    # Ljava/lang/String;

    .prologue
    .line 191
    invoke-interface {p1, p2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 192
    .local v0, "nodeList":Lorg/w3c/dom/NodeList;
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getSingleElement(Lorg/w3c/dom/NodeList;Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 5
    .param p1, "nl"    # Lorg/w3c/dom/NodeList;
    .param p2, "tagName"    # Ljava/lang/String;

    .prologue
    .line 196
    const/4 v3, 0x0

    .line 197
    .local v3, "retVal":Lorg/w3c/dom/Node;
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 198
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 199
    invoke-interface {p1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 200
    .local v2, "n":Lorg/w3c/dom/Node;
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 201
    move-object v3, v2

    .line 198
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 204
    .end local v2    # "n":Lorg/w3c/dom/Node;
    :cond_1
    return-object v3
.end method

.method private getSingleElementByAttribute(Lorg/w3c/dom/NodeList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 5
    .param p1, "nl"    # Lorg/w3c/dom/NodeList;
    .param p2, "tagName"    # Ljava/lang/String;
    .param p3, "attrName"    # Ljava/lang/String;
    .param p4, "attrValue"    # Ljava/lang/String;

    .prologue
    .line 208
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 209
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 210
    invoke-interface {p1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 211
    .local v3, "n":Lorg/w3c/dom/Node;
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 212
    invoke-direct {p0, v3, p3}, Lcom/samsung/android/app/headlines/update/VersionParser;->getAttribute(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 213
    .local v2, "model":Ljava/lang/String;
    invoke-virtual {p4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 218
    .end local v2    # "model":Ljava/lang/String;
    .end local v3    # "n":Lorg/w3c/dom/Node;
    :goto_1
    return-object v3

    .line 209
    .restart local v3    # "n":Lorg/w3c/dom/Node;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 218
    .end local v3    # "n":Lorg/w3c/dom/Node;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private initValue()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 36
    const-string v0, "-1"

    iput-object v0, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->forceUpdateVersion:Ljava/lang/String;

    .line 37
    iget-object v0, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->globalMarkets:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 38
    iget-object v0, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->modelMarkets:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 39
    const-string v0, "-1"

    iput-object v0, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayGlobalVersion:Ljava/lang/String;

    .line 40
    const-string v0, "-1"

    iput-object v0, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayModelVersion:Ljava/lang/String;

    .line 41
    const-string v0, "-1"

    iput-object v0, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayModelVersionByOsVersion:Ljava/lang/String;

    .line 42
    iput v1, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->globalMarketIndex:I

    .line 43
    iput v1, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->modelMarketIndex:I

    .line 44
    return-void
.end method


# virtual methods
.method public clearMarketIndex()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 273
    iput v0, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->globalMarketIndex:I

    .line 274
    iput v0, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->modelMarketIndex:I

    .line 275
    return-void
.end method

.method public getForceUpdateVer()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 98
    iget-object v2, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->forceUpdateVersion:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 100
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->forceUpdateVersion:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 107
    :cond_0
    :goto_0
    return v1

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 103
    sget-object v2, Lcom/samsung/android/app/headlines/update/VersionParser;->TAG:Ljava/lang/String;

    const-string v3, "Unable to parse mForceUpdateVer"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getGooglePlayGlobalVersion()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 111
    iget-object v2, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayGlobalVersion:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 113
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayGlobalVersion:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 120
    :cond_0
    :goto_0
    return v1

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 116
    sget-object v2, Lcom/samsung/android/app/headlines/update/VersionParser;->TAG:Ljava/lang/String;

    const-string v3, "Unable to parse googlePlayGlobalVersion"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getGooglePlayLastestVersion()I
    .locals 5

    .prologue
    const/4 v3, -0x1

    .line 148
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/update/VersionParser;->getGooglePlayGlobalVersion()I

    move-result v0

    .line 149
    .local v0, "googlePlayGlobalVersion":I
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/update/VersionParser;->getGooglePlayModelVersion()I

    move-result v1

    .line 150
    .local v1, "googlePlayModelVersion":I
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/update/VersionParser;->getGooglePlayModelVersionByOsVersion()I

    move-result v2

    .line 152
    .local v2, "googlePlayModelVersionByOsVersion":I
    if-eq v2, v3, :cond_0

    .line 160
    .end local v2    # "googlePlayModelVersionByOsVersion":I
    :goto_0
    return v2

    .line 154
    .restart local v2    # "googlePlayModelVersionByOsVersion":I
    :cond_0
    if-eq v1, v3, :cond_1

    move v2, v1

    .line 155
    goto :goto_0

    .line 156
    :cond_1
    if-eq v0, v3, :cond_2

    move v2, v0

    .line 157
    goto :goto_0

    .line 159
    :cond_2
    const-string v3, "hwang"

    const-string v4, "::getGooglePlayLastestVersion :: never happen!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getGooglePlayModelVersion()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 124
    iget-object v2, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayModelVersion:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 126
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayModelVersion:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 132
    :cond_0
    :goto_0
    return v1

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/samsung/android/app/headlines/update/VersionParser;->TAG:Ljava/lang/String;

    const-string v3, "Unable to parse googlePlayModelVersion"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getGooglePlayModelVersionByOsVersion()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 136
    iget-object v2, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayModelVersionByOsVersion:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 138
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->googlePlayModelVersionByOsVersion:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 144
    :cond_0
    :goto_0
    return v1

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/samsung/android/app/headlines/update/VersionParser;->TAG:Ljava/lang/String;

    const-string v3, "Unable to parse googlePlayModelVersionByOsVersion"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public hasModelMarket()Z
    .locals 3

    .prologue
    .line 183
    iget-object v1, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->modelMarkets:Ljava/util/HashMap;

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 184
    .local v0, "market":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 185
    const/4 v1, 0x0

    .line 187
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public popGlobalMarket()Ljava/lang/String;
    .locals 4

    .prologue
    .line 165
    iget-object v1, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->globalMarkets:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->globalMarketIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 166
    .local v0, "market":Ljava/lang/String;
    iget v1, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->globalMarketIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->globalMarketIndex:I

    .line 167
    if-nez v0, :cond_0

    .line 168
    const-string v0, "Unknown"

    .line 170
    .end local v0    # "market":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public popModelMarket()Ljava/lang/String;
    .locals 4

    .prologue
    .line 174
    iget-object v1, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->modelMarkets:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->modelMarketIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 175
    .local v0, "market":Ljava/lang/String;
    iget v1, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->modelMarketIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/app/headlines/update/VersionParser;->modelMarketIndex:I

    .line 176
    if-nez v0, :cond_0

    .line 177
    const-string v0, "Unknown"

    .line 179
    .end local v0    # "market":Ljava/lang/String;
    :cond_0
    return-object v0
.end method
