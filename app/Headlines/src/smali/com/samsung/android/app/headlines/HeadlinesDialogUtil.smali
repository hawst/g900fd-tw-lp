.class public Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;
.super Ljava/lang/Object;
.source "HeadlinesDialogUtil.java"


# static fields
.field public static final ACTION_DO_ACTION1:I = 0x2

.field public static final ACTION_DO_ACTION2:I = 0x3

.field public static final ACTION_ENTER_SETTING:I = 0x5

.field public static final ACTION_ENTER_TERMSOFUSE:I = 0x6

.field public static final ACTION_PULL_DOWN_REFRESH:I = 0x7

.field public static final ACTION_PULL_TO_REFRESH:I = 0x4

.field public static final ACTION_REFRESH:I = 0x1

.field public static final ACTION_UPDATE_WELCOME:I = 0x0

.field public static final DATA_TYPE_MOBILE:I = 0x0

.field public static final DATA_TYPE_WLAN:I = 0x1

.field private static final TAG:Ljava/lang/String; = "HeadlinesDialogUtil"

.field static mDataType:I

.field private static mDontShowAgain:Landroid/widget/CheckBox;

.field private static mHandlerPullToRefresh:Landroid/os/Handler;

.field private static mIsPopupDisplaying:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mHandlerPullToRefresh:Landroid/os/Handler;

    .line 41
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mIsPopupDisplaying:Z

    .line 43
    const/4 v0, -0x1

    sput v0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mDataType:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mDontShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mHandlerPullToRefresh:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$102(Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Landroid/os/Handler;

    .prologue
    .line 24
    sput-object p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mHandlerPullToRefresh:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 24
    sput-boolean p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mIsPopupDisplaying:Z

    return p0
.end method

.method static synthetic access$300()V
    .locals 0

    .prologue
    .line 24
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->clearVariables()V

    return-void
.end method

.method public static checkDataUseCHN(Landroid/content/Context;I)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "action_type"    # I

    .prologue
    const/4 v0, 0x0

    .line 52
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-static {p0, p1, v0, v0}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->dialogDataAlertPopup(Landroid/app/Activity;ILcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static checkDataUseCHN(Landroid/content/Context;ILcom/samsung/android/magazine/cardchannel/Card;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "actionType"    # I
    .param p2, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 56
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->dialogDataAlertPopup(Landroid/app/Activity;ILcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static checkDataUseCHN(Landroid/content/Context;ILcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "actionType"    # I
    .param p2, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    .line 60
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->dialogDataAlertPopup(Landroid/app/Activity;ILcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static clearVariables()V
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mHandlerPullToRefresh:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 47
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mHandlerPullToRefresh:Landroid/os/Handler;

    .line 48
    :cond_0
    return-void
.end method

.method private static dialogDataAlertPopup(Landroid/app/Activity;ILcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/Object;)Z
    .locals 12
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "action_type"    # I
    .param p2, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 98
    sget-boolean v8, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mIsPopupDisplaying:Z

    if-ne v8, v10, :cond_0

    move v8, v9

    .line 299
    :goto_0
    return v8

    .line 101
    :cond_0
    const-string v8, "mymagazine"

    invoke-virtual {p0, v8, v9}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v11, "WLAN_DATA_USE_ALLOWED"

    invoke-interface {v8, v11, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 102
    .local v7, "wlanDataEnabled":Z
    const-string v8, "mymagazine"

    invoke-virtual {p0, v8, v9}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v11, "MOBILE_DATA_USE_ALLOWED"

    invoke-interface {v8, v11, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 104
    .local v4, "mobileDataEnabled":Z
    const-string v8, "connectivity"

    invoke-virtual {p0, v8}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    .line 105
    .local v3, "manager":Landroid/net/ConnectivityManager;
    invoke-virtual {v3, v10}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    .line 107
    .local v6, "wifi":Landroid/net/NetworkInfo;
    new-instance v5, Landroid/view/ContextThemeWrapper;

    const v8, 0x7f0a0005

    invoke-direct {v5, p0, v8}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 108
    .local v5, "themeContext":Landroid/content/Context;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 109
    .local v1, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 110
    .local v0, "adbInflater":Landroid/view/LayoutInflater;
    const v8, 0x7f03000b

    const/4 v11, 0x0

    invoke-virtual {v0, v8, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 111
    .local v2, "eulaLayout":Landroid/view/View;
    const v8, 0x7f0c0029

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckBox;

    sput-object v8, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mDontShowAgain:Landroid/widget/CheckBox;

    .line 112
    sget-object v8, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mDontShowAgain:Landroid/widget/CheckBox;

    new-instance v11, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$1;

    invoke-direct {v11}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$1;-><init>()V

    invoke-virtual {v8, v11}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 119
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 120
    if-nez v7, :cond_3

    .line 121
    const v8, 0x7f09002c

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 122
    const v8, 0x7f09002e

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 123
    sput v10, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mDataType:I

    .line 137
    :goto_1
    const/4 v8, 0x4

    if-eq p1, v8, :cond_1

    const/4 v8, 0x7

    if-ne p1, v8, :cond_6

    :cond_1
    move-object v8, p3

    .line 138
    check-cast v8, Landroid/os/Handler;

    sput-object v8, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mHandlerPullToRefresh:Landroid/os/Handler;

    .line 147
    :cond_2
    sput-boolean v10, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mIsPopupDisplaying:Z

    .line 149
    const v8, 0x104000a

    new-instance v10, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;

    invoke-direct {v10, p0, p1, p2, p3}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;-><init>(Landroid/app/Activity;ILcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/Object;)V

    invoke-virtual {v1, v8, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 259
    const/high16 v8, 0x1040000

    new-instance v10, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$3;

    invoke-direct {v10, p1, p0}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$3;-><init>(ILandroid/app/Activity;)V

    invoke-virtual {v1, v8, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 279
    new-instance v8, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$4;

    invoke-direct {v8, p1, p0}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$4;-><init>(ILandroid/app/Activity;)V

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 297
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move v8, v9

    .line 299
    goto/16 :goto_0

    :cond_3
    move v8, v10

    .line 125
    goto/16 :goto_0

    .line 128
    :cond_4
    if-nez v4, :cond_5

    .line 129
    const v8, 0x7f09002b

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 130
    const v8, 0x7f09002d

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 131
    sput v9, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mDataType:I

    goto :goto_1

    :cond_5
    move v8, v10

    .line 133
    goto/16 :goto_0

    .line 139
    :cond_6
    const/4 v8, 0x2

    if-ne p1, v8, :cond_8

    .line 140
    if-eqz p2, :cond_7

    const-string v8, "action1"

    invoke-virtual {p2, v8}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardAction(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardAction;

    move-result-object v8

    if-nez v8, :cond_2

    :cond_7
    move v8, v10

    .line 141
    goto/16 :goto_0

    .line 142
    :cond_8
    const/4 v8, 0x3

    if-ne p1, v8, :cond_2

    .line 143
    if-eqz p2, :cond_9

    const-string v8, "action2"

    invoke-virtual {p2, v8}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardAction(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardAction;

    move-result-object v8

    if-nez v8, :cond_2

    :cond_9
    move v8, v10

    .line 144
    goto/16 :goto_0
.end method

.method public static dialogNoNetworkPopup(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 303
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 305
    .local v0, "noNetworkDialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x104000a

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$5;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 314
    new-instance v1, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$6;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 323
    const v1, 0x7f090059

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 324
    const v1, 0x7f090045

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 325
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 327
    const/4 v1, 0x0

    return v1
.end method

.method public static isDataUseAgreedCHN(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 64
    const-string v6, "mymagazine"

    invoke-virtual {p0, v6, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "WLAN_DATA_USE_ALLOWED"

    invoke-interface {v6, v7, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 65
    .local v3, "wlanDataEnabled":Z
    const-string v6, "mymagazine"

    invoke-virtual {p0, v6, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "MOBILE_DATA_USE_ALLOWED"

    invoke-interface {v6, v7, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 67
    .local v1, "mobileDataEnabled":Z
    const-string v6, "connectivity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 68
    .local v0, "manager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 70
    .local v2, "wifi":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 71
    if-eqz v3, :cond_2

    .line 76
    :cond_0
    :goto_0
    return v4

    .line 73
    :cond_1
    if-nez v1, :cond_0

    :cond_2
    move v4, v5

    .line 76
    goto :goto_0
.end method

.method public static isDataUseAgreedInternalCHN(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 81
    const-string v6, "mymagazineService"

    invoke-virtual {p0, v6, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "WLAN_DATA_USE_ALLOWED"

    invoke-interface {v6, v7, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 82
    .local v3, "wlanDataEnabled":Z
    const-string v6, "mymagazineService"

    invoke-virtual {p0, v6, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "MOBILE_DATA_USE_ALLOWED"

    invoke-interface {v6, v7, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 84
    .local v1, "mobileDataEnabled":Z
    const-string v6, "connectivity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 85
    .local v0, "manager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 87
    .local v2, "wifi":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 88
    if-eqz v3, :cond_2

    .line 93
    :cond_0
    :goto_0
    return v4

    .line 90
    :cond_1
    if-nez v1, :cond_0

    :cond_2
    move v4, v5

    .line 93
    goto :goto_0
.end method
