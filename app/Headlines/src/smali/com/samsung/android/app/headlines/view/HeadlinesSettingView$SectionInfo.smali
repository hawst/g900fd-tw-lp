.class Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;
.super Ljava/lang/Object;
.source "HeadlinesSettingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SectionInfo"
.end annotation


# instance fields
.field private mAdapter:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

.field private mSection:Ljava/lang/String;

.field private mSelectedMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Ljava/lang/String;Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;)V
    .locals 1
    .param p2, "section"    # Ljava/lang/String;
    .param p3, "adapter"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->mSection:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->mAdapter:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    .line 61
    invoke-virtual {p3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->getSelectedMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->mSelectedMap:Ljava/util/HashMap;

    .line 62
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->mSection:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;)Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->mAdapter:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->mSelectedMap:Ljava/util/HashMap;

    return-object v0
.end method
