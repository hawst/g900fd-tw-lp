.class public Lcom/samsung/android/app/headlines/template/CardTemplate_2;
.super Lcom/samsung/android/app/headlines/template/CardTemplate;
.source "CardTemplate_2.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/headlines/template/CardTemplate;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    .line 18
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTemplateType:I

    .line 19
    return-void
.end method


# virtual methods
.method public getCardView(Landroid/view/LayoutInflater;Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;Lcom/samsung/android/magazine/cardchannel/Card;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "viewHolder"    # Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;
    .param p3, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x1

    .line 27
    iput-object p3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 29
    invoke-virtual {p2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->isEnabled()Z

    move-result v0

    if-ne v0, v2, :cond_4

    .line 30
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 31
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView1:Landroid/widget/TextView;

    .line 32
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView2:Landroid/widget/TextView;

    .line 33
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView1:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mImageView1:Landroid/widget/ImageView;

    .line 34
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView2:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mImageView2:Landroid/widget/ImageView;

    .line 55
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 57
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView1:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text1"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text1"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView2:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text2"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text2"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mImageView1:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 68
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mImageView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "image1"

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mImageView1:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 72
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mImageView2:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 73
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mImageView2:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 74
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "image2"

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mImageView2:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 78
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTemplateContainer:Landroid/view/ViewGroup;

    return-object v0

    .line 36
    :cond_4
    const v0, 0x7f030016

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 37
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_5

    .line 38
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c0049

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView1:Landroid/widget/TextView;

    .line 39
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c0047

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView2:Landroid/widget/TextView;

    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c0040

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mImageView1:Landroid/widget/ImageView;

    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c0043

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mImageView2:Landroid/widget/ImageView;

    .line 44
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView1:Landroid/widget/TextView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView2:Landroid/widget/TextView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    .line 47
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView3:Landroid/widget/TextView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView3:Landroid/widget/TextView;

    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mTextView4:Landroid/widget/TextView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView4:Landroid/widget/TextView;

    .line 49
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mImageView1:Landroid/widget/ImageView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView1:Landroid/widget/ImageView;

    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_2;->mImageView2:Landroid/widget/ImageView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView2:Landroid/widget/ImageView;

    .line 52
    invoke-virtual {p2, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->setEnabled(Z)V

    goto/16 :goto_0
.end method

.method public onCardCollapse()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public onCardExpand()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method
