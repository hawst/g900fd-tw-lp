.class Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$2;
.super Landroid/view/View$AccessibilityDelegate;
.source "HeadlinesSettingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->initTitleView(Landroid/view/View;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

.field final synthetic val$parent:Landroid/view/View;

.field final synthetic val$sectionDisplayName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Landroid/view/View;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    iput-object p2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$2;->val$parent:Landroid/view/View;

    iput-object p3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$2;->val$sectionDisplayName:Ljava/lang/String;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 9
    .param p1, "host"    # Landroid/view/View;
    .param p2, "action"    # I
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 183
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$2;->val$parent:Landroid/view/View;

    const v8, 0x7f0c003b

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/GridView;

    .line 184
    .local v3, "gridView":Landroid/widget/GridView;
    invoke-virtual {v3}, Landroid/widget/GridView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_1

    move v1, v5

    .line 186
    .local v1, "expand":Z
    :goto_0
    const/16 v7, 0x40

    if-ne p2, v7, :cond_0

    .line 187
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090040

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 188
    .local v4, "titleContent":Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .line 189
    .local v2, "expandString":Ljava/lang/CharSequence;
    if-ne v1, v5, :cond_2

    .line 190
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090042

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 194
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 195
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$2;->val$sectionDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    const-string v7, ","

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 198
    const-string v7, ","

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 200
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 201
    const-string v6, "HeadlinesSettingView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "performAccessibilityAction : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "expandString":Ljava/lang/CharSequence;
    .end local v4    # "titleContent":Ljava/lang/CharSequence;
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/View$AccessibilityDelegate;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    .line 205
    return v5

    .end local v1    # "expand":Z
    :cond_1
    move v1, v6

    .line 184
    goto :goto_0

    .line 192
    .restart local v1    # "expand":Z
    .restart local v2    # "expandString":Ljava/lang/CharSequence;
    .restart local v4    # "titleContent":Ljava/lang/CharSequence;
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090041

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public sendAccessibilityEvent(Landroid/view/View;I)V
    .locals 1
    .param p1, "host"    # Landroid/view/View;
    .param p2, "eventType"    # I

    .prologue
    .line 210
    const/high16 v0, 0x10000

    if-eq p2, v0, :cond_0

    .line 211
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->sendAccessibilityEvent(Landroid/view/View;I)V

    .line 212
    :cond_0
    return-void
.end method
