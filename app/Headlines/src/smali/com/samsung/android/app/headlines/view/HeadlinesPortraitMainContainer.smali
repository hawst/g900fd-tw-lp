.class public Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;
.super Landroid/widget/FrameLayout;
.source "HeadlinesPortraitMainContainer.java"

# interfaces
.implements Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;
.implements Lcom/samsung/android/app/headlines/NewCardNotification;
.implements Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;
.implements Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;
.implements Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndListener;
.implements Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$CustomGestureListener;,
        Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;,
        Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;
    }
.end annotation


# static fields
.field private static final AUTO_REFRESH_INTERVAL:I = 0x493e0

.field private static final AUTO_REFRESH_INTERVAL_NOT_SET:I = -0x1

.field private static final DISPLAYABLE_MINIMUM_CARD_COUNT:I = 0x3

.field private static final FRICTION:F = 1.8f

.field private static final GRAVITY:F = 0.65f

.field private static final HEADER_ANIMATION_DURATION:F = 300.0f

.field private static final SELP_STOP_TIMEOUT:I = 0x1388

.field private static final SLOPE_FLICK_SCROLL_THRESHOLD:F = 0.8f

.field private static final TAG:Ljava/lang/String; = "HeadlinesPortraitMainContainer"


# instance fields
.field private MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

.field private PULL_TO_REFRESH_BACK_HEIGHT:I

.field private PULL_TO_REFRESH_TURNING_HEIGHT:I

.field private TOUCH_SLOP:F

.field private mAddedCategoryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCachedVerticalScrollRange:I

.field private mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

.field private mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

.field private mContext:Landroid/content/Context;

.field private mGestureDetecter:Landroid/view/GestureDetector;

.field private mHeaderAnimator:Landroid/animation/ObjectAnimator;

.field private mHeaderInitialTopPos:I

.field private mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

.field private mHeaderViewHeight:I

.field private mInDragAndDragMode:Z

.field private mInPullDownRefreshMode:Z

.field private mInitialMotionX:F

.field private mInitialMotionY:F

.field private mInitialOffsetY:I

.field private mIsAccessibilityEnabled:Z

.field private mIsHomeLaunching:Z

.field private mIsOutOfTouchSlopRange:Z

.field private mIsPullDownRefreshEnabled:Z

.field private mIsStatusBarShown:Z

.field private mIsTouchIntercepted:Z

.field private mNoticationButton:Landroid/widget/Button;

.field private mPrevScrollState:I

.field private mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

.field private mScrollDirection:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

.field private mScrollFlag:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

.field private mScrollOffset:I

.field private mScrollState:I

.field private mTouchPressed:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 109
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 48
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    .line 50
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    .line 51
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    .line 52
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    .line 53
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    .line 54
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mGestureDetecter:Landroid/view/GestureDetector;

    .line 55
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    .line 56
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    .line 57
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mAddedCategoryList:Ljava/util/List;

    .line 59
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mTouchPressed:Z

    .line 60
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsHomeLaunching:Z

    .line 61
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsTouchIntercepted:Z

    .line 62
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsOutOfTouchSlopRange:Z

    .line 63
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInDragAndDragMode:Z

    .line 64
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInPullDownRefreshMode:Z

    .line 65
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsPullDownRefreshEnabled:Z

    .line 66
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsAccessibilityEnabled:Z

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsStatusBarShown:Z

    .line 78
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->TOUCH_SLOP:F

    .line 79
    const/16 v0, 0x12c

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    .line 80
    const/16 v0, 0xf0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->PULL_TO_REFRESH_TURNING_HEIGHT:I

    .line 81
    const/16 v0, 0x96

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->PULL_TO_REFRESH_BACK_HEIGHT:I

    .line 83
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialMotionX:F

    .line 84
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialMotionY:F

    .line 85
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderInitialTopPos:I

    .line 86
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollOffset:I

    .line 87
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    .line 88
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    .line 89
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCachedVerticalScrollRange:I

    .line 91
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPrevScrollState:I

    .line 92
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollState:I

    .line 93
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;->NONE_SCROLLING:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollFlag:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    .line 94
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollDirection:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    .line 110
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    .line 112
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->init()V

    .line 113
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 116
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    .line 50
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    .line 51
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    .line 52
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    .line 53
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    .line 54
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mGestureDetecter:Landroid/view/GestureDetector;

    .line 55
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    .line 56
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    .line 57
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mAddedCategoryList:Ljava/util/List;

    .line 59
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mTouchPressed:Z

    .line 60
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsHomeLaunching:Z

    .line 61
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsTouchIntercepted:Z

    .line 62
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsOutOfTouchSlopRange:Z

    .line 63
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInDragAndDragMode:Z

    .line 64
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInPullDownRefreshMode:Z

    .line 65
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsPullDownRefreshEnabled:Z

    .line 66
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsAccessibilityEnabled:Z

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsStatusBarShown:Z

    .line 78
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->TOUCH_SLOP:F

    .line 79
    const/16 v0, 0x12c

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    .line 80
    const/16 v0, 0xf0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->PULL_TO_REFRESH_TURNING_HEIGHT:I

    .line 81
    const/16 v0, 0x96

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->PULL_TO_REFRESH_BACK_HEIGHT:I

    .line 83
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialMotionX:F

    .line 84
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialMotionY:F

    .line 85
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderInitialTopPos:I

    .line 86
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollOffset:I

    .line 87
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    .line 88
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    .line 89
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCachedVerticalScrollRange:I

    .line 91
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPrevScrollState:I

    .line 92
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollState:I

    .line 93
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;->NONE_SCROLLING:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollFlag:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    .line 94
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollDirection:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    .line 117
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    .line 119
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->init()V

    .line 120
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    return v0
.end method

.method static synthetic access$502(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsHomeLaunching:Z

    return p1
.end method

.method static synthetic access$602(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCachedVerticalScrollRange:I

    return p1
.end method

.method private animateHeader(I)V
    .locals 5
    .param p1, "toY"    # I

    .prologue
    .line 737
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-nez v0, :cond_0

    .line 747
    :goto_0
    return-void

    .line 744
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    const-string v1, "translationY"

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    int-to-float v4, p1

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    .line 745
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 746
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method private init()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 123
    const-string v1, "HeadlinesPortraitMainContainer"

    const-string v2, "init()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const v1, 0x7f07000b

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->setBackgroundResource(I)V

    .line 127
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08007e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->PULL_TO_REFRESH_TURNING_HEIGHT:I

    .line 128
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08007f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->PULL_TO_REFRESH_BACK_HEIGHT:I

    .line 129
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08003c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    .line 131
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$CustomGestureListener;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$CustomGestureListener;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$1;)V

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mGestureDetecter:Landroid/view/GestureDetector;

    .line 133
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 134
    .local v0, "inflater":Landroid/view/LayoutInflater;
    if-eqz v0, :cond_1

    .line 135
    const v1, 0x7f030008

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 137
    const v1, 0x7f0c0024

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    .line 138
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-eqz v1, :cond_0

    .line 139
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->measure(II)V

    .line 140
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v1, v5, v5, v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->layout(IIII)V

    .line 142
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    .line 145
    :cond_0
    const v1, 0x7f0c0023

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    .line 146
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    if-eqz v1, :cond_1

    .line 147
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getTouchSlop()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->TOUCH_SLOP:F

    .line 151
    :cond_1
    const v1, 0x7f0c0020

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    .line 152
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    if-eqz v1, :cond_2

    .line 153
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/Button;->measure(II)V

    .line 154
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/widget/Button;->layout(IIII)V

    .line 156
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    new-instance v2, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$1;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mAddedCategoryList:Ljava/util/List;

    .line 170
    return-void
.end method

.method private isPullToRefreshEnabled()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 759
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    if-nez v3, :cond_1

    move v0, v2

    .line 776
    :cond_0
    :goto_0
    return v0

    .line 764
    :cond_1
    const/4 v0, 0x0

    .line 765
    .local v0, "enabled":Z
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v3, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 766
    .local v1, "firstChildView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 767
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getFirstVisiblePosition()I

    move-result v2

    if-nez v2, :cond_2

    .line 768
    const/4 v0, 0x1

    goto :goto_0

    .line 771
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private refreshByPullToRefresh()V
    .locals 8

    .prologue
    .line 780
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->setRefreshState(Z)V

    .line 782
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getCards()Ljava/util/List;

    move-result-object v2

    .line 783
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 784
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 785
    .local v0, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->requestRefresh(I)V

    .line 783
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 789
    .end local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :cond_0
    const/4 v1, 0x0

    :goto_1
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v5}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 791
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v5, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 792
    .local v4, "view":Landroid/view/View;
    if-eqz v4, :cond_1

    instance-of v5, v4, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    if-eqz v5, :cond_1

    .line 793
    check-cast v4, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    .end local v4    # "view":Landroid/view/View;
    invoke-virtual {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->showLoadingViews()V

    .line 789
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 797
    :cond_2
    new-instance v3, Ljava/util/Timer;

    invoke-direct {v3}, Ljava/util/Timer;-><init>()V

    .line 798
    .local v3, "timer":Ljava/util/Timer;
    new-instance v5, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$2;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$2;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)V

    const-wide/16 v6, 0x1388

    invoke-virtual {v3, v5, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 804
    return-void
.end method

.method private resetHeaderInitialOffset(I)V
    .locals 4
    .param p1, "offset"    # I

    .prologue
    const/4 v3, 0x0

    .line 489
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderInitialTopPos:I

    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    sub-int/2addr v2, p1

    add-int v0, v1, v2

    .line 490
    .local v0, "newHeaderTopY":I
    if-ltz v0, :cond_1

    .line 491
    iput v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderInitialTopPos:I

    .line 492
    const/4 v0, 0x0

    .line 493
    iput v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    .line 499
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    neg-int v1, v1

    if-ge v0, v1, :cond_0

    .line 495
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    neg-int v1, v1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderInitialTopPos:I

    .line 496
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    neg-int v0, v1

    .line 497
    iput v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    goto :goto_0
.end method

.method private stopHeaderAnimation()V
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 694
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 696
    :cond_0
    return-void
.end method

.method private updateHeader()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 645
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCachedVerticalScrollRange:I

    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollOffset:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 646
    .local v0, "offsetY":I
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    if-ge v0, v1, :cond_1

    .line 648
    invoke-direct {p0, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->animateHeader(I)V

    .line 649
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->showStatusBar(Z)V

    .line 675
    :cond_0
    :goto_0
    return-void

    .line 659
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollFlag:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    sget-object v2, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;->DOWN_SCROLLING:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    if-ne v1, v2, :cond_2

    .line 661
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    neg-int v1, v1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->animateHeader(I)V

    .line 662
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->showStatusBar(Z)V

    goto :goto_0

    .line 663
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollFlag:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    sget-object v2, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;->UP_SCROLLING:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    if-ne v1, v2, :cond_0

    .line 664
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getY()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    mul-int/lit8 v2, v2, 0x8

    div-int/lit8 v2, v2, 0x9

    mul-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    .line 666
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    neg-int v1, v1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->animateHeader(I)V

    .line 667
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->showStatusBar(Z)V

    goto :goto_0

    .line 670
    :cond_3
    invoke-direct {p0, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->animateHeader(I)V

    .line 671
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->showStatusBar(Z)V

    goto :goto_0
.end method

.method private updateHeaderPosition(I)V
    .locals 6
    .param p1, "offsetY"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 700
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderInitialTopPos:I

    iget v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    sub-int/2addr v3, p1

    add-int v0, v2, v3

    .line 701
    .local v0, "newHeaderTopY":I
    if-ltz v0, :cond_3

    .line 702
    iput v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderInitialTopPos:I

    .line 703
    const/4 v0, 0x0

    .line 704
    iput p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    .line 711
    :cond_0
    :goto_0
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCachedVerticalScrollRange:I

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollOffset:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 712
    .local v1, "offset":I
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    if-ge v1, v2, :cond_5

    .line 713
    iget-boolean v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mTouchPressed:Z

    if-nez v2, :cond_1

    .line 714
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    neg-int v2, v2

    mul-int/lit8 v2, v2, 0x1

    div-int/lit8 v2, v2, 0x5

    if-ge v0, v2, :cond_4

    .line 715
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->showStatusBar(Z)V

    .line 728
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->stopHeaderAnimation()V

    .line 730
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-eqz v2, :cond_2

    .line 731
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    int-to-float v3, v0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->setTranslationY(F)V

    .line 734
    :cond_2
    return-void

    .line 705
    .end local v1    # "offset":I
    :cond_3
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    neg-int v2, v2

    if-ge v0, v2, :cond_0

    .line 706
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    neg-int v2, v2

    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderInitialTopPos:I

    .line 707
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    neg-int v0, v2

    .line 708
    iput p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    goto :goto_0

    .line 717
    .restart local v1    # "offset":I
    :cond_4
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->showStatusBar(Z)V

    goto :goto_1

    .line 721
    :cond_5
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    neg-int v2, v2

    mul-int/lit8 v2, v2, 0x1

    div-int/lit8 v2, v2, 0x5

    if-ge v0, v2, :cond_6

    .line 722
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->showStatusBar(Z)V

    goto :goto_1

    .line 724
    :cond_6
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->showStatusBar(Z)V

    goto :goto_1
.end method


# virtual methods
.method public animateHeader(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 751
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 752
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->animateHeader(I)V

    .line 756
    :goto_0
    return-void

    .line 754
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    neg-int v0, v0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->animateHeader(I)V

    goto :goto_0
.end method

.method public dismissMoreMenu()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-nez v0, :cond_0

    .line 188
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->dismissMoreMenu()V

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 282
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    if-nez v9, :cond_0

    .line 284
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v9

    .line 469
    :goto_0
    return v9

    .line 287
    :cond_0
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    if-nez v9, :cond_1

    .line 289
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v9

    goto :goto_0

    .line 292
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mGestureDetecter:Landroid/view/GestureDetector;

    if-eqz v9, :cond_2

    .line 293
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mGestureDetecter:Landroid/view/GestureDetector;

    invoke-virtual {v9, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 296
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v7

    .line 297
    .local v7, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v8

    .line 299
    .local v8, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 300
    .local v2, "action":I
    packed-switch v2, :pswitch_data_0

    .line 469
    :cond_3
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v9

    goto :goto_0

    .line 303
    :pswitch_0
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mTouchPressed:Z

    .line 304
    iput v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialMotionX:F

    .line 305
    iput v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialMotionY:F

    .line 306
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsTouchIntercepted:Z

    .line 307
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsOutOfTouchSlopRange:Z

    .line 308
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->isPullToRefreshEnabled()Z

    move-result v9

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsPullDownRefreshEnabled:Z

    .line 310
    iget-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsPullDownRefreshEnabled:Z

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    .line 311
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    invoke-virtual {v9}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->isRefreshing()Z

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_4

    .line 313
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsPullDownRefreshEnabled:Z

    goto :goto_1

    .line 317
    :cond_4
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    sget-object v10, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_READY:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    invoke-virtual {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->setPullDownState(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;)V

    goto :goto_1

    .line 325
    :pswitch_1
    iget v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialMotionX:F

    sub-float v3, v7, v9

    .line 326
    .local v3, "diffX":F
    iget v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialMotionY:F

    sub-float v4, v8, v9

    .line 327
    .local v4, "diffY":F
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 328
    .local v0, "absDiffX":F
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 330
    .local v1, "absDiffY":F
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v9}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getCount()I

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_3

    .line 335
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    invoke-virtual {v9}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->getPullDownRefreshState()Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    move-result-object v9

    sget-object v10, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_READY:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    if-ne v9, v10, :cond_5

    .line 336
    const/4 v9, 0x0

    cmpg-float v9, v4, v9

    if-gez v9, :cond_5

    .line 337
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsPullDownRefreshEnabled:Z

    .line 342
    :cond_5
    iget-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsOutOfTouchSlopRange:Z

    const/4 v10, 0x1

    if-eq v9, v10, :cond_6

    iget v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->TOUCH_SLOP:F

    cmpl-float v9, v0, v9

    if-gtz v9, :cond_6

    iget v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->TOUCH_SLOP:F

    cmpl-float v9, v1, v9

    if-lez v9, :cond_12

    .line 343
    :cond_6
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsOutOfTouchSlopRange:Z

    .line 345
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollDirection:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    sget-object v10, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    if-ne v9, v10, :cond_a

    .line 348
    const/4 v9, 0x0

    cmpl-float v9, v0, v9

    if-eqz v9, :cond_7

    div-float v9, v1, v0

    const v10, 0x3f4ccccd    # 0.8f

    cmpl-float v9, v9, v10

    if-lez v9, :cond_9

    .line 350
    :cond_7
    sget-object v9, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_VERTICAL:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    iput-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollDirection:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    .line 357
    :cond_8
    :goto_2
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 352
    :cond_9
    iget-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInDragAndDragMode:Z

    const/4 v10, 0x1

    if-eq v9, v10, :cond_8

    .line 354
    sget-object v9, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_HORIZONTAL:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    iput-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollDirection:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    goto :goto_2

    .line 359
    :cond_a
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollDirection:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    sget-object v10, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_VERTICAL:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    if-ne v9, v10, :cond_11

    .line 362
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    invoke-virtual {v9}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->getPullDownRefreshState()Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    move-result-object v9

    sget-object v10, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_READY:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    if-ne v9, v10, :cond_b

    .line 363
    const/4 v9, 0x0

    cmpg-float v9, v4, v9

    if-gez v9, :cond_b

    .line 364
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsPullDownRefreshEnabled:Z

    .line 369
    :cond_b
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    invoke-virtual {v9}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->isRefreshing()Z

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_c

    .line 371
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsPullDownRefreshEnabled:Z

    .line 372
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsTouchIntercepted:Z

    .line 373
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 376
    :cond_c
    iget-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInDragAndDragMode:Z

    const/4 v10, 0x1

    if-ne v9, v10, :cond_d

    .line 377
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsPullDownRefreshEnabled:Z

    .line 381
    :cond_d
    iget-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsPullDownRefreshEnabled:Z

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    iget-boolean v9, v9, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mTwScrollingByScrollbar:Z

    const/4 v10, 0x1

    if-eq v9, v10, :cond_3

    .line 383
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    invoke-virtual {v9}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->getPullDownRefreshState()Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    move-result-object v9

    sget-object v10, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_READY:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    if-ne v9, v10, :cond_e

    .line 387
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    sget-object v10, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_REFRESH_NOT_REQUIRED:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    invoke-virtual {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->setPullDownState(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;)V

    .line 390
    :cond_e
    const v9, 0x3fe66666    # 1.8f

    div-float v9, v4, v9

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    int-to-float v9, v9

    const v10, 0x3f266666    # 0.65f

    mul-float/2addr v9, v10

    float-to-int v5, v9

    .line 391
    .local v5, "pullDownIncremental":I
    if-gtz v5, :cond_f

    .line 392
    const/4 v5, 0x0

    .line 399
    :goto_3
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setDndMode(Z)V

    .line 400
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    int-to-float v10, v5

    invoke-virtual {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setTranslationY(F)V

    .line 401
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setOverScrollMode(I)V

    .line 403
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInPullDownRefreshMode:Z

    .line 404
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsTouchIntercepted:Z

    .line 406
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 393
    :cond_f
    iget v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->PULL_TO_REFRESH_TURNING_HEIGHT:I

    if-ge v5, v9, :cond_10

    .line 394
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    sget-object v10, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_REFRESH_NOT_REQUIRED:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    invoke-virtual {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->setPullDownState(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;)V

    goto :goto_3

    .line 396
    :cond_10
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    sget-object v10, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_REFRESH_REQUIRED:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    invoke-virtual {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->setPullDownState(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;)V

    goto :goto_3

    .line 409
    .end local v5    # "pullDownIncremental":I
    :cond_11
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollDirection:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    sget-object v10, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_HORIZONTAL:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    if-ne v9, v10, :cond_3

    goto/16 :goto_1

    .line 414
    :cond_12
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsTouchIntercepted:Z

    goto/16 :goto_1

    .line 423
    .end local v0    # "absDiffX":F
    .end local v1    # "absDiffY":F
    .end local v3    # "diffX":F
    .end local v4    # "diffY":F
    :pswitch_2
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mTouchPressed:Z

    .line 425
    iget-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsPullDownRefreshEnabled:Z

    const/4 v10, 0x1

    if-ne v9, v10, :cond_13

    .line 426
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    const-string v10, "translationY"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    iget v13, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->PULL_TO_REFRESH_BACK_HEIGHT:I

    int-to-float v13, v13

    aput v13, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 427
    .local v6, "refreshBackAnimator":Landroid/animation/ObjectAnimator;
    const-wide/16 v10, 0x12c

    invoke-virtual {v6, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 428
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    const v10, 0x10c0034

    invoke-static {v9, v10}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 429
    invoke-virtual {v6}, Landroid/animation/ObjectAnimator;->start()V

    .line 431
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    sget-object v10, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_REFRESH:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    invoke-virtual {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->setPullDownState(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;)V

    .line 432
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setOverScrollMode(I)V

    .line 435
    .end local v6    # "refreshBackAnimator":Landroid/animation/ObjectAnimator;
    :cond_13
    iget-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsHomeLaunching:Z

    const/4 v10, 0x1

    if-ne v9, v10, :cond_14

    .line 436
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setListItemClickable(Z)V

    .line 439
    :cond_14
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsOutOfTouchSlopRange:Z

    .line 440
    sget-object v9, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    iput-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollDirection:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    goto/16 :goto_1

    .line 447
    :pswitch_3
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mTouchPressed:Z

    .line 449
    iget-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsPullDownRefreshEnabled:Z

    const/4 v10, 0x1

    if-ne v9, v10, :cond_15

    .line 450
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    const-string v10, "translationY"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    iget v13, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->PULL_TO_REFRESH_BACK_HEIGHT:I

    int-to-float v13, v13

    aput v13, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 451
    .restart local v6    # "refreshBackAnimator":Landroid/animation/ObjectAnimator;
    const-wide/16 v10, 0x12c

    invoke-virtual {v6, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 452
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    const v10, 0x10c0034

    invoke-static {v9, v10}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 453
    invoke-virtual {v6}, Landroid/animation/ObjectAnimator;->start()V

    .line 455
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    sget-object v10, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_REFRESH:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    invoke-virtual {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->setPullDownState(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;)V

    .line 456
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setOverScrollMode(I)V

    .line 459
    .end local v6    # "refreshBackAnimator":Landroid/animation/ObjectAnimator;
    :cond_15
    iget-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsHomeLaunching:Z

    const/4 v10, 0x1

    if-ne v9, v10, :cond_16

    .line 463
    :cond_16
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsOutOfTouchSlopRange:Z

    .line 464
    sget-object v9, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;->DIRECTION_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    iput-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollDirection:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollDirection;

    goto/16 :goto_1

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .locals 1

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    return-object v0
.end method

.method public getCardList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/Card;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->getCardList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hideNotificationButton()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 906
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 907
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 908
    .local v0, "animation":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 909
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 910
    new-instance v1, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$5;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 925
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->clearAnimation()V

    .line 926
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->startAnimation(Landroid/view/animation/Animation;)V

    .line 929
    .end local v0    # "animation":Landroid/view/animation/TranslateAnimation;
    :cond_0
    return-void
.end method

.method public isAccessibilityEnabled(Z)V
    .locals 1
    .param p1, "isEnabled"    # Z

    .prologue
    .line 888
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsAccessibilityEnabled:Z

    .line 889
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsAccessibilityEnabled:Z

    if-eqz v0, :cond_0

    .line 890
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->updateHeader()V

    .line 892
    :cond_0
    return-void
.end method

.method public isPopupShown()Z
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-nez v0, :cond_0

    .line 193
    const/4 v0, 0x0

    .line 196
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->isPopupShown()Z

    move-result v0

    goto :goto_0
.end method

.method public onAccessibilityStateChanged(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 884
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->isAccessibilityEnabled(Z)V

    .line 885
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setOnScrollListener(Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;)V

    .line 235
    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 236
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 240
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    if-eqz v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->setPullDownRefreshEventListener(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;)V

    .line 244
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    const-string v2, "accessibility"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 245
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    if-eqz v0, :cond_1

    .line 246
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->isAccessibilityEnabled(Z)V

    .line 247
    invoke-virtual {v0, p0}, Landroid/view/accessibility/AccessibilityManager;->removeAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    .line 250
    :cond_1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 251
    return-void
.end method

.method public onDragAndDropStart()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 475
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInDragAndDragMode:Z

    .line 477
    invoke-direct {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->resetHeaderInitialOffset(I)V

    .line 478
    invoke-direct {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->animateHeader(I)V

    .line 479
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->showStatusBar(Z)V

    .line 480
    return-void
.end method

.method public onDragAndDropStop()V
    .locals 1

    .prologue
    .line 484
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInDragAndDragMode:Z

    .line 485
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 255
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 256
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 277
    :cond_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    :goto_0
    return v1

    .line 268
    :pswitch_1
    iget-boolean v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsTouchIntercepted:Z

    if-ne v2, v1, :cond_0

    .line 270
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsTouchIntercepted:Z

    goto :goto_0

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onNewNotificationCardAdded(I)V
    .locals 5
    .param p1, "cardId"    # I

    .prologue
    const/4 v4, 0x0

    .line 941
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    const-string v3, "mymagazine"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 942
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "ENABLE_NEW_STORY_NOTIFICATION"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 943
    .local v0, "enable":Z
    if-eqz v0, :cond_0

    .line 944
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getCount()I

    move-result v2

    const/4 v3, 0x3

    if-le v2, v3, :cond_0

    .line 948
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 949
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->showNotificationButton()V

    .line 952
    :cond_0
    return-void
.end method

.method public onNewNotificationCardRemoved(I)V
    .locals 5
    .param p1, "cardId"    # I

    .prologue
    const/4 v4, 0x0

    .line 956
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    const-string v3, "mymagazine"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 957
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "ENABLE_NEW_STORY_NOTIFICATION"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 958
    .local v0, "enable":Z
    if-eqz v0, :cond_0

    .line 960
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 961
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->hideNotificationButton()V

    .line 963
    :cond_0
    return-void
.end method

.method public onPullDownAnimationFinished()V
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_READY:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->setPullDownState(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;)V

    .line 510
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInPullDownRefreshMode:Z

    .line 511
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setDndMode(Z)V

    .line 513
    :cond_0
    return-void
.end method

.method public onPullDownItemRefreshRequested()V
    .locals 0

    .prologue
    .line 503
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->refreshByPullToRefresh()V

    .line 504
    return-void
.end method

.method public onScroll(Lcom/sec/android/touchwiz/widget/TwAbsListView;III)V
    .locals 10
    .param p1, "view"    # Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    const/4 v9, 0x1

    .line 565
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollOffset:I

    .line 567
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    if-nez v8, :cond_1

    .line 641
    :cond_0
    return-void

    .line 572
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    iget-boolean v8, v8, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mTwScrollingByScrollbar:Z

    if-eq v8, v9, :cond_0

    .line 577
    iget-boolean v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInPullDownRefreshMode:Z

    if-eq v8, v9, :cond_0

    .line 582
    iget-boolean v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInDragAndDragMode:Z

    if-eq v8, v9, :cond_0

    .line 587
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v8}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->scrollYIsComputed()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 588
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v8}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getComputedScrollY()I

    move-result v8

    iput v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollOffset:I

    .line 596
    :cond_2
    iget v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCachedVerticalScrollRange:I

    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v9}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getHeight()I

    move-result v9

    sub-int/2addr v8, v9

    iget v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollOffset:I

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 599
    .local v7, "offsetY":I
    iget v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollState:I

    const/4 v9, 0x2

    if-eq v8, v9, :cond_3

    .line 600
    iget v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    sub-int/2addr v8, v7

    if-lez v8, :cond_7

    .line 601
    sget-object v8, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;->UP_SCROLLING:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    iput-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollFlag:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    .line 612
    :cond_3
    :goto_0
    iget v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollState:I

    if-eqz v8, :cond_4

    .line 613
    invoke-direct {p0, v7}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->updateHeaderPosition(I)V

    .line 616
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_0

    .line 617
    move v5, p2

    .local v5, "i":I
    :goto_1
    add-int v8, p2, p3

    if-ge v5, v8, :cond_0

    .line 618
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    sub-int v9, v5, p2

    invoke-virtual {v8, v9}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 619
    .local v2, "cardView":Landroid/view/View;
    if-eqz v2, :cond_6

    .line 620
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    .line 621
    .local v4, "displaymetrics":Landroid/util/DisplayMetrics;
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v8

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 622
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v8}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getY()F

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v9}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getHeight()I

    move-result v9

    int-to-float v9, v9

    add-float v3, v8, v9

    .line 624
    .local v3, "displayedHeaderHeight":F
    invoke-virtual {v2}, Landroid/view/View;->getY()F

    move-result v8

    iget v9, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v9, v9

    sub-float/2addr v9, v3

    cmpg-float v8, v8, v9

    if-gez v8, :cond_0

    .line 625
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v8, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    .line 626
    .local v0, "card":Ljava/lang/Object;
    if-eqz v0, :cond_6

    instance-of v8, v0, Lcom/samsung/android/magazine/cardchannel/Card;

    if-eqz v8, :cond_6

    .line 627
    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    .end local v0    # "card":Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v1

    .line 628
    .local v1, "cardId":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 629
    .local v6, "id":Ljava/lang/Integer;
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 631
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 633
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->hideNotificationButton()V

    .line 617
    .end local v1    # "cardId":I
    .end local v3    # "displayedHeaderHeight":F
    .end local v4    # "displaymetrics":Landroid/util/DisplayMetrics;
    .end local v6    # "id":Ljava/lang/Integer;
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 603
    .end local v2    # "cardView":Landroid/view/View;
    .end local v5    # "i":I
    :cond_7
    iget v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    sub-int/2addr v8, v7

    if-gez v8, :cond_8

    .line 604
    sget-object v8, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;->DOWN_SCROLLING:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    iput-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollFlag:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    goto :goto_0

    .line 607
    :cond_8
    sget-object v8, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;->NONE_SCROLLING:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    iput-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollFlag:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$ScrollFlag;

    goto/16 :goto_0
.end method

.method public onScrollStateChanged(Lcom/sec/android/touchwiz/widget/TwAbsListView;I)V
    .locals 4
    .param p1, "view"    # Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .param p2, "scrollState"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 517
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-nez v0, :cond_1

    .line 561
    :cond_0
    :goto_0
    return-void

    .line 522
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    if-eqz v0, :cond_0

    .line 527
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollState:I

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPrevScrollState:I

    .line 529
    if-nez p2, :cond_5

    .line 532
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPrevScrollState:I

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPrevScrollState:I

    if-ne v0, v3, :cond_3

    .line 533
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->updateHeader()V

    .line 536
    :cond_3
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderInitialTopPos:I

    .line 537
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    .line 560
    :cond_4
    :goto_1
    iput p2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mScrollState:I

    goto :goto_0

    .line 539
    :cond_5
    if-ne p2, v2, :cond_6

    .line 541
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPrevScrollState:I

    if-nez v0, :cond_4

    .line 542
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getTranslationY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderInitialTopPos:I

    .line 545
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCachedVerticalScrollRange:I

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getComputedScrollY()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    goto :goto_1

    .line 548
    :cond_6
    if-ne p2, v3, :cond_4

    .line 551
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPrevScrollState:I

    if-nez v0, :cond_4

    .line 552
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getTranslationY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderInitialTopPos:I

    .line 555
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCachedVerticalScrollRange:I

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getComputedScrollY()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mInitialOffsetY:I

    goto :goto_1
.end method

.method public openHelp()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-nez v0, :cond_0

    .line 215
    :goto_0
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->openHelp()V

    goto :goto_0
.end method

.method public openSetting()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-nez v0, :cond_0

    .line 206
    :goto_0
    return-void

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->openSetting()V

    goto :goto_0
.end method

.method public removeAllNewCategoryList()V
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 936
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->hideNotificationButton()V

    .line 937
    return-void
.end method

.method public requestAllRefresh()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 807
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    const-string v4, "mymagazine"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 809
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v3, "AUTO_REFRESH_SET_FIRST_TIME"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 810
    .local v0, "auto_refresh_set_first_time":Z
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 811
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->requestAllRefresh(I)V

    .line 812
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 813
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "AUTO_REFRESH_SET_FIRST_TIME"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 814
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 819
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :goto_0
    return-void

    .line 817
    :cond_0
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->requestAllRefresh(I)V

    goto :goto_0
.end method

.method public requestAllRefresh(I)V
    .locals 14
    .param p1, "gap"    # I

    .prologue
    .line 822
    iget-object v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    const-string v11, "mymagazine"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 824
    .local v6, "pref":Landroid/content/SharedPreferences;
    const-string v10, "AUTO_REFRESH_ON"

    const/4 v11, 0x0

    invoke-interface {v6, v10, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_3

    .line 826
    const-string v10, "PAUSE_TIME"

    const-wide/16 v12, 0x0

    invoke-interface {v6, v10, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 827
    .local v8, "time":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 828
    .local v2, "current":J
    const v0, 0x493e0

    .line 830
    .local v0, "_gap":I
    const/4 v10, -0x1

    if-eq p1, v10, :cond_0

    .line 831
    move v0, p1

    .line 833
    :cond_0
    sub-long v10, v2, v8

    int-to-long v12, v0

    cmp-long v10, v10, v12

    if-lez v10, :cond_2

    .line 834
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->setRefreshState(Z)V

    .line 835
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getCards()Ljava/util/List;

    move-result-object v5

    .line 836
    .local v5, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    if-ge v4, v10, :cond_1

    .line 837
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 838
    .local v1, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v10

    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->requestRefresh(I)V

    .line 836
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 843
    .end local v1    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    if-eqz v10, :cond_2

    .line 844
    new-instance v7, Ljava/util/Timer;

    invoke-direct {v7}, Ljava/util/Timer;-><init>()V

    .line 845
    .local v7, "timer":Ljava/util/Timer;
    new-instance v10, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$3;

    invoke-direct {v10, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$3;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)V

    const-wide/16 v12, 0x1f4

    invoke-virtual {v7, v10, v12, v13}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 870
    .end local v4    # "i":I
    .end local v5    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    .end local v7    # "timer":Ljava/util/Timer;
    :cond_2
    new-instance v7, Ljava/util/Timer;

    invoke-direct {v7}, Ljava/util/Timer;-><init>()V

    .line 871
    .restart local v7    # "timer":Ljava/util/Timer;
    new-instance v10, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$4;

    invoke-direct {v10, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$4;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)V

    const-wide/16 v12, 0x1388

    invoke-virtual {v7, v10, v12, v13}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 880
    .end local v0    # "_gap":I
    .end local v2    # "current":J
    .end local v7    # "timer":Ljava/util/Timer;
    .end local v8    # "time":J
    :cond_3
    return-void
.end method

.method public setAdapter(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)V
    .locals 4
    .param p1, "adapter"    # Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    .prologue
    .line 985
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    .line 987
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    if-eqz v0, :cond_1

    .line 988
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->setOnNewCardNotification(Lcom/samsung/android/app/headlines/NewCardNotification;)V

    .line 989
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    new-instance v1, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$6;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 997
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    .line 998
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    if-eqz v0, :cond_0

    .line 999
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    new-instance v1, Lcom/sec/android/touchwiz/widget/TwAbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderViewHeight:I

    invoke-direct {v1, v2, v3}, Lcom/sec/android/touchwiz/widget/TwAbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1000
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->setPullDownRefreshEventListener(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;)V

    .line 1001
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 1003
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mPullDownRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->setParentGridView(Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;)V

    .line 1006
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    if-eqz v0, :cond_1

    .line 1007
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1008
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setDndController(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;)V

    .line 1009
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setDndMode(Z)V

    .line 1010
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setDndListener(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndListener;)V

    .line 1012
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCardListView:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->resetScrollRange()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mCachedVerticalScrollRange:I

    .line 1015
    :cond_1
    return-void
.end method

.method public setNewCategoryNotificationEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 932
    return-void
.end method

.method public setOrientaionPortraitMode(Z)V
    .locals 1
    .param p1, "mode"    # Z

    .prologue
    .line 223
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->animateHeader(I)V

    .line 224
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->showStatusBar(Z)V

    .line 225
    return-void
.end method

.method public showMoreMenu()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-nez v0, :cond_0

    .line 179
    :goto_0
    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->showMoreMenu()V

    goto :goto_0
.end method

.method public showNotificationButton()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 895
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 896
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 897
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->clearAnimation()V

    .line 898
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v3, v3, v1, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 899
    .local v0, "animation":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 900
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 901
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->startAnimation(Landroid/view/animation/Animation;)V

    .line 903
    .end local v0    # "animation":Landroid/view/animation/TranslateAnimation;
    :cond_0
    return-void
.end method

.method showStatusBar(Z)V
    .locals 2
    .param p1, "set"    # Z

    .prologue
    const/16 v1, 0x400

    .line 678
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsStatusBarShown:Z

    if-ne v0, p1, :cond_0

    .line 689
    :goto_0
    return-void

    .line 681
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 682
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 688
    :goto_1
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsStatusBarShown:Z

    goto :goto_0

    .line 685
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto :goto_1
.end method
