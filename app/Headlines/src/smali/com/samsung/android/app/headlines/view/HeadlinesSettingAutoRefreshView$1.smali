.class Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$1;
.super Ljava/lang/Object;
.source "HeadlinesSettingAutoRefreshView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

.field final synthetic val$refreshView:Landroid/widget/RelativeLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Landroid/widget/RelativeLayout;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$1;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    iput-object p2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$1;->val$refreshView:Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 81
    const-string v3, "HeadlinesSettingAutoRefreshView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onClick mRefresh "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$1;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mRefresh:Z
    invoke-static {v5}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$000(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$1;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->toggle()V

    .line 84
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$1;->val$refreshView:Landroid/widget/RelativeLayout;

    const v4, 0x7f0c0030

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 85
    .local v1, "checkBox":Landroid/widget/CheckBox;
    if-eqz v1, :cond_0

    .line 86
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$1;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "accessibility"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 87
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 88
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v2

    .line 89
    .local v2, "event":Landroid/view/accessibility/AccessibilityEvent;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/accessibility/AccessibilityEvent;->setEventType(I)V

    .line 90
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 91
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 92
    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 95
    .end local v0    # "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    .end local v2    # "event":Landroid/view/accessibility/AccessibilityEvent;
    :cond_0
    return-void
.end method
