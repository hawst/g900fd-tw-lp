.class public final Lcom/samsung/android/app/headlines/Manifest$permission;
.super Ljava/lang/Object;
.source "Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "permission"
.end annotation


# static fields
.field public static final BROADCAST:Ljava/lang/String; = "com.samsung.android.internal.headlines.permission.BROADCAST"

.field public static final READ_ADMINISTRATOR:Ljava/lang/String; = "com.samsung.android.magazine.permission.READ_ADMINISTRATOR"

.field public static final READ_CARD_CHANNEL:Ljava/lang/String; = "com.samsung.android.magazine.permission.READ_CARD_CHANNEL"

.field public static final READ_CARD_PROVIDER:Ljava/lang/String; = "com.samsung.android.magazine.permission.READ_CARD_PROVIDER"

.field public static final SERVICE:Ljava/lang/String; = "com.samsung.android.internal.headlines.permission.SERVICE"

.field public static final SYNC_FDL_USER:Ljava/lang/String; = "flipboard.fdl.broadcast.SYNC_FDL_USER"

.field public static final SYNC_USER:Ljava/lang/String; = "sstream.app.broadcast.SYNC_USER"

.field public static final WRITE_ADMINISTRATOR:Ljava/lang/String; = "com.samsung.android.magazine.permission.WRITE_ADMINISTRATOR"

.field public static final WRITE_CARD_CHANNEL:Ljava/lang/String; = "com.samsung.android.magazine.permission.WRITE_CARD_CHANNEL"

.field public static final WRITE_CARD_PROVIDER:Ljava/lang/String; = "com.samsung.android.magazine.permission.WRITE_CARD_PROVIDER"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
