.class Lcom/samsung/android/app/headlines/UpgradeManager$5;
.super Ljava/lang/Object;
.source "UpgradeManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/UpgradeManager;->dialogNormalUpdate(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/UpgradeManager;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->this$0:Lcom/samsung/android/app/headlines/UpgradeManager;

    iput-object p2, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const v12, 0x7f090004

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 290
    const-string v5, "com.sec.android.app.samsungapps"

    .line 291
    .local v5, "packageName":Ljava/lang/String;
    const/4 v7, 0x1

    .line 293
    .local v7, "vendingEnabled":Z
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v5, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 294
    .local v4, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v8, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v7, v8, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 308
    if-ne v7, v10, :cond_1

    .line 309
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 310
    .local v2, "intent":Landroid/content/Intent;
    const-string v8, "com.sec.android.app.samsungapps"

    const-string v9, "com.sec.android.app.samsungapps.Main"

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 311
    const-string v8, "directcall"

    invoke-virtual {v2, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 312
    const-string v8, "GUID"

    iget-object v9, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 313
    const v8, 0x14000020

    invoke-virtual {v2, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 314
    iget-object v8, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    invoke-virtual {v8, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 315
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 316
    # setter for: Lcom/samsung/android/app/headlines/UpgradeManager;->popup_show:Z
    invoke-static {v11}, Lcom/samsung/android/app/headlines/UpgradeManager;->access$602(Z)Z

    .line 317
    iget-object v8, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    instance-of v8, v8, Landroid/app/Activity;

    if-ne v8, v10, :cond_0

    .line 318
    iget-object v8, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->finish()V

    .line 326
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v4    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return-void

    .line 295
    :catch_0
    move-exception v1

    .line 296
    .local v1, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 297
    iget-object v8, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 298
    .local v0, "appName":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090063

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 299
    .local v3, "isNotInstalled":Ljava/lang/String;
    new-array v8, v10, [Ljava/lang/Object;

    aput-object v0, v8, v11

    invoke-static {v3, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 300
    .local v6, "text":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    invoke-static {v8, v6, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 301
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 302
    iget-object v8, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    instance-of v8, v8, Landroid/app/Activity;

    if-ne v8, v10, :cond_0

    .line 303
    iget-object v8, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    check-cast v8, Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 322
    .end local v0    # "appName":Ljava/lang/String;
    .end local v1    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v3    # "isNotInstalled":Ljava/lang/String;
    .end local v6    # "text":Ljava/lang/String;
    .restart local v4    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->this$0:Lcom/samsung/android/app/headlines/UpgradeManager;

    iget-object v9, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    iget-object v10, p0, Lcom/samsung/android/app/headlines/UpgradeManager$5;->val$context:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "com.sec.android.app.samsungapps"

    # invokes: Lcom/samsung/android/app/headlines/UpgradeManager;->dialogOpenAppInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v8, v9, v10, v11}, Lcom/samsung/android/app/headlines/UpgradeManager;->access$700(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0
.end method
