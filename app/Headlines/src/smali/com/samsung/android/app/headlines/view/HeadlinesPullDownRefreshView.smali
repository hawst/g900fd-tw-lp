.class public Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;
.super Landroid/widget/FrameLayout;
.source "HeadlinesPullDownRefreshView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$3;,
        Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;,
        Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;
    }
.end annotation


# static fields
.field public static final REFRESH_LOADING_DELAY:I = 0x2bc

.field private static final TAG:Ljava/lang/String; = "HeadlinesPullDownRefreshView"


# instance fields
.field private mArrowView:Landroid/widget/ImageView;

.field private mLoaingProgressBar:Landroid/widget/ProgressBar;

.field private mParentContainer:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

.field private mPrevPullDownRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

.field private mProgressBardAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;

.field private mPullDownRefreshEventListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;

.field private mPullDownRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

.field private mRefreshBackRunnable:Ljava/lang/Runnable;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 28
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mParentContainer:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    .line 30
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mTextView:Landroid/widget/TextView;

    .line 31
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mArrowView:Landroid/widget/ImageView;

    .line 32
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mLoaingProgressBar:Landroid/widget/ProgressBar;

    .line 34
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mRefreshBackRunnable:Ljava/lang/Runnable;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mProgressBardAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;

    .line 36
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshEventListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;

    .line 38
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPrevPullDownRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    .line 39
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    .line 51
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mContext:Landroid/content/Context;

    .line 53
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->init()V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mParentContainer:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    .line 30
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mTextView:Landroid/widget/TextView;

    .line 31
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mArrowView:Landroid/widget/ImageView;

    .line 32
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mLoaingProgressBar:Landroid/widget/ProgressBar;

    .line 34
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mRefreshBackRunnable:Ljava/lang/Runnable;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mProgressBardAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;

    .line 36
    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshEventListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;

    .line 38
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPrevPullDownRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    .line 39
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    .line 58
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mContext:Landroid/content/Context;

    .line 60
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->init()V

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->startRefreshBackAnimation()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshEventListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mRefreshBackRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->dismissLoadingProgressBar()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->initPullDownRefreshView()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mLoaingProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private dismissLoadingProgressBar()V
    .locals 3

    .prologue
    .line 227
    const-string v1, "HeadlinesPullDownRefreshView"

    const-string v2, "dismissLoadingProgressBar(): Dismiss a Progress Bar."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mLoaingProgressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 230
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mProgressBardAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;

    if-eqz v1, :cond_0

    .line 232
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mProgressBardAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 233
    :catch_0
    move-exception v0

    .line 234
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 64
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 65
    .local v0, "inflater":Landroid/view/LayoutInflater;
    if-eqz v0, :cond_2

    .line 66
    const v1, 0x7f03000a

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 68
    const v1, 0x7f0c0027

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mArrowView:Landroid/widget/ImageView;

    .line 69
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mArrowView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mArrowView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setRotation(F)V

    .line 73
    :cond_0
    const v1, 0x7f0c0026

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mLoaingProgressBar:Landroid/widget/ProgressBar;

    .line 74
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mLoaingProgressBar:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_1

    .line 75
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mLoaingProgressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 76
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mLoaingProgressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x2710

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 79
    :cond_1
    const v1, 0x7f0c0028

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mTextView:Landroid/widget/TextView;

    .line 81
    :cond_2
    return-void
.end method

.method private initPullDownRefreshView()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mArrowView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mArrowView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 204
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mTextView:Landroid/widget/TextView;

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 205
    return-void
.end method

.method private showLoadingProgressBar()V
    .locals 2

    .prologue
    .line 217
    const-string v0, "HeadlinesPullDownRefreshView"

    const-string v1, "showLoadingProgressBar(): Show a Progress Bar."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mLoaingProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 220
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$1;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mProgressBardAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;

    .line 221
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mProgressBardAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mProgressBardAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$ProgressBarAnimationThread;->start()V

    .line 224
    :cond_0
    return-void
.end method

.method private startArrowRotateAnimation(I)V
    .locals 7
    .param p1, "degree"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 208
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mArrowView:Landroid/widget/ImageView;

    new-array v2, v4, [Landroid/animation/PropertyValuesHolder;

    sget-object v3, Landroid/view/View;->ROTATION:Landroid/util/Property;

    new-array v4, v4, [F

    int-to-float v5, p1

    aput v5, v4, v6

    invoke-static {v3, v4}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 209
    .local v0, "iconRotateAnimator":Landroid/animation/ObjectAnimator;
    if-eqz v0, :cond_0

    .line 210
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 211
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mContext:Landroid/content/Context;

    const v2, 0x10c0034

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 212
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 214
    :cond_0
    return-void
.end method

.method private startRefreshBackAnimation()V
    .locals 6

    .prologue
    .line 159
    const-string v1, "HeadlinesPullDownRefreshView"

    const-string v2, "startRefreshBackAnimation()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mParentContainer:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    if-nez v1, :cond_0

    .line 162
    const-string v1, "HeadlinesPullDownRefreshView"

    const-string v2, "startRefreshBackAnimation(): Parent Container is null."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :goto_0
    return-void

    .line 166
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mParentContainer:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    const-string v2, "translationY"

    const/4 v3, 0x1

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 167
    .local v0, "refreshBackAnimator":Landroid/animation/ObjectAnimator;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 168
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mContext:Landroid/content/Context;

    const v2, 0x10c0002

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 169
    new-instance v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$2;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 198
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method


# virtual methods
.method public getPullDownRefreshState()Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    return-object v0
.end method

.method public isRefreshing()Z
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mRefreshBackRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setParentGridView(Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;)V
    .locals 0
    .param p1, "cardGridView"    # Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mParentContainer:Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;

    .line 260
    return-void
.end method

.method public setPullDownRefreshEventListener(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;)V
    .locals 0
    .param p1, "pullDownRefreshEventListener"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshEventListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;

    .line 156
    return-void
.end method

.method public setPullDownState(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;)V
    .locals 4
    .param p1, "state"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    if-ne v0, p1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPrevPullDownRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    .line 90
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    .line 92
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$3;->$SwitchMap$com$samsung$android$app$headlines$view$HeadlinesPullDownRefreshView$PullDownRefreshState:[I

    invoke-virtual {p1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 94
    :pswitch_0
    const-string v0, "HeadlinesPullDownRefreshView"

    const-string v1, "setPullDownState(): [STATE_NONE]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 98
    :pswitch_1
    const-string v0, "HeadlinesPullDownRefreshView"

    const-string v1, "setPullDownState(): [STATE_READY]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->initPullDownRefreshView()V

    goto :goto_0

    .line 103
    :pswitch_2
    const-string v0, "HeadlinesPullDownRefreshView"

    const-string v1, "setPullDownState(): [STATE_REFRESH_NOT_REQUIRED]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->startArrowRotateAnimation(I)V

    .line 105
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mTextView:Landroid/widget/TextView;

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 109
    :pswitch_3
    const-string v0, "HeadlinesPullDownRefreshView"

    const-string v1, "setPullDownState(): [STATE_REFRESH_REQUIRED]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const/16 v0, -0xb4

    invoke-direct {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->startArrowRotateAnimation(I)V

    .line 111
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mTextView:Landroid/widget/TextView;

    const v1, 0x7f09000c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 115
    :pswitch_4
    const-string v0, "HeadlinesPullDownRefreshView"

    const-string v1, "setPullDownState(): [STATE_REFRESH]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPrevPullDownRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_REFRESH_REQUIRED:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    if-ne v0, v1, :cond_2

    .line 117
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mArrowView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mTextView:Landroid/widget/TextView;

    const v1, 0x7f09003a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 119
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->showLoadingProgressBar()V

    .line 121
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$1;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mRefreshBackRunnable:Ljava/lang/Runnable;

    .line 129
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mRefreshBackRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2bc

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 131
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshEventListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshEventListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;

    invoke-interface {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;->onPullDownItemRefreshRequested()V

    goto/16 :goto_0

    .line 136
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->startRefreshBackAnimation()V

    goto/16 :goto_0

    .line 92
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
