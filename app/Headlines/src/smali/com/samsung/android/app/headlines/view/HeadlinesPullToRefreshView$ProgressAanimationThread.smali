.class Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;
.super Ljava/lang/Thread;
.source "HeadlinesPullToRefreshView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgressAanimationThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;
    .param p2, "x1"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$1;

    .prologue
    .line 325
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 330
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    .line 331
    move v1, v0

    .line 332
    .local v1, "step":I
    const-wide/16 v2, 0x1a

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 333
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->access$500(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread$1;

    invoke-direct {v3, p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread$1;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;I)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 341
    :catch_0
    move-exception v2

    .line 343
    .end local v1    # "step":I
    :cond_0
    return-void
.end method
