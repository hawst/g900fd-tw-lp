.class public interface abstract Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;
.super Ljava/lang/Object;
.source "HeadlinesMainContainerInterface.java"


# virtual methods
.method public abstract animateHeader(Z)V
.end method

.method public abstract dismissMoreMenu()V
.end method

.method public abstract getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;
.end method

.method public abstract getCardList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/Card;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isPopupShown()Z
.end method

.method public abstract openHelp()V
.end method

.method public abstract openSetting()V
.end method

.method public abstract removeAllNewCategoryList()V
.end method

.method public abstract requestAllRefresh()V
.end method

.method public abstract setAdapter(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)V
.end method

.method public abstract setNewCategoryNotificationEnabled(Z)V
.end method

.method public abstract showMoreMenu()V
.end method
