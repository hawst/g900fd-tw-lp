.class public final Lcom/samsung/android/app/headlines/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final about_description_text_color:I = 0x7f070017

.field public static final action_bar_textcolor:I = 0x7f070010

.field public static final author:I = 0x7f07000a

.field public static final author_color:I = 0x7f070008

.field public static final background_text:I = 0x7f07000d

.field public static final black:I = 0x7f070001

.field public static final black_alpha66:I = 0x7f070002

.field public static final black_shadow:I = 0x7f070015

.field public static final body_color:I = 0x7f070007

.field public static final flickr:I = 0x7f070013

.field public static final gray:I = 0x7f070003

.field public static final gray_light_color:I = 0x7f070012

.field public static final image_stroke:I = 0x7f070014

.field public static final listview_card_background:I = 0x7f07000b

.field public static final listview_card_divider:I = 0x7f07000c

.field public static final loading_text:I = 0x7f070018

.field public static final more_menu_textcolor:I = 0x7f070011

.field public static final notification_text_color:I = 0x7f070019

.field public static final setting_section_item_text_color:I = 0x7f07001e

.field public static final setting_section_text:I = 0x7f07001d

.field public static final terms_start_text_color:I = 0x7f07001f

.field public static final title_color:I = 0x7f070006

.field public static final title_outer:I = 0x7f070005

.field public static final title_shadow:I = 0x7f070004

.field public static final transparent:I = 0x7f070009

.field public static final welcome_body_text_color:I = 0x7f07000f

.field public static final welcome_category_text_color:I = 0x7f07000e

.field public static final welcome_next_text_color:I = 0x7f070020

.field public static final welcome_title_shadow:I = 0x7f070016

.field public static final white:I = 0x7f070000

.field public static final white_f5:I = 0x7f07001a

.field public static final white_f5_alpha66:I = 0x7f07001c

.field public static final white_fa:I = 0x7f07001b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
