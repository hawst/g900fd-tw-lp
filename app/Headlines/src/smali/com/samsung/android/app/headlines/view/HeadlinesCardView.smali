.class public Lcom/samsung/android/app/headlines/view/HeadlinesCardView;
.super Landroid/widget/RelativeLayout;
.source "HeadlinesCardView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field protected static final SELP_STOP_TIMEOUT:J = 0x4e20L

.field private static final TAG:Ljava/lang/String; = "HeadlinesCardView"


# instance fields
.field private isUpdating:Z

.field private mCard:Lcom/samsung/android/magazine/cardchannel/Card;

.field private mCardTemplate:Lcom/samsung/android/app/headlines/template/CardTemplate;

.field private mCardView:Landroid/view/View;

.field private mContainer:Landroid/view/ViewGroup;

.field private mContext:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mProgress:Landroid/widget/ImageView;

.field private mRefresh:Landroid/widget/Button;

.field private mRotateAnimation:Landroid/view/animation/RotateAnimation;

.field private mSettingLayout:Landroid/widget/RelativeLayout;

.field private mViewHolder:Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 110
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContext:Landroid/content/Context;

    .line 35
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 36
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardView:Landroid/view/View;

    .line 37
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardTemplate:Lcom/samsung/android/app/headlines/template/CardTemplate;

    .line 38
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mInflater:Landroid/view/LayoutInflater;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mSettingLayout:Landroid/widget/RelativeLayout;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    .line 41
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 44
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContainer:Landroid/view/ViewGroup;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->isUpdating:Z

    .line 48
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mViewHolder:Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContext:Landroid/content/Context;

    .line 35
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 36
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardView:Landroid/view/View;

    .line 37
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardTemplate:Lcom/samsung/android/app/headlines/template/CardTemplate;

    .line 38
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mInflater:Landroid/view/LayoutInflater;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mSettingLayout:Landroid/widget/RelativeLayout;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    .line 41
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 44
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContainer:Landroid/view/ViewGroup;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->isUpdating:Z

    .line 48
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mViewHolder:Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 33
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContext:Landroid/content/Context;

    .line 35
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 36
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardView:Landroid/view/View;

    .line 37
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardTemplate:Lcom/samsung/android/app/headlines/template/CardTemplate;

    .line 38
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mInflater:Landroid/view/LayoutInflater;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mSettingLayout:Landroid/widget/RelativeLayout;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    .line 41
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 44
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContainer:Landroid/view/ViewGroup;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->isUpdating:Z

    .line 48
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mViewHolder:Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;

    .line 53
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContext:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 55
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContext:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/samsung/android/app/headlines/template/CardTemplateFactory;->createTemplate(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)Lcom/samsung/android/app/headlines/template/CardTemplate;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardTemplate:Lcom/samsung/android/app/headlines/template/CardTemplate;

    .line 57
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mInflater:Landroid/view/LayoutInflater;

    .line 58
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03001b

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 60
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;

    invoke-direct {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mViewHolder:Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;

    .line 61
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardTemplate:Lcom/samsung/android/app/headlines/template/CardTemplate;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mInflater:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mViewHolder:Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;

    invoke-virtual {v0, v1, v2, p2}, Lcom/samsung/android/app/headlines/template/CardTemplate;->getCardView(Landroid/view/LayoutInflater;Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;Lcom/samsung/android/magazine/cardchannel/Card;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardView:Landroid/view/View;

    .line 63
    const v0, 0x7f0c004c

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContainer:Landroid/view/ViewGroup;

    .line 64
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setLongClickable(Z)V

    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 68
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c004d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mSettingLayout:Landroid/widget/RelativeLayout;

    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mSettingLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mSettingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 71
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mSettingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mSettingLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setImportantForAccessibility(I)V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c004e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    .line 76
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    const v1, 0x7f020079

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 78
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImportantForAccessibility(I)V

    .line 80
    :cond_1
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43b40000    # 360.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 81
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/headlines/view/HeadlinesCardView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private doAction(Ljava/lang/String;)V
    .locals 8
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 137
    const-string v5, "HeadlinesCardView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[doAction] action key: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v5, p1}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardAction(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardAction;

    move-result-object v0

    .line 140
    .local v0, "action":Lcom/samsung/android/magazine/cardchannel/CardAction;
    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardAction;->getData()Landroid/content/Intent;

    move-result-object v2

    .line 142
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 143
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "displayName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContext:Landroid/content/Context;

    invoke-static {v5, v1}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getCardDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 145
    .local v3, "localCardName":Ljava/lang/String;
    const-string v5, "displayName"

    invoke-virtual {v2, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardAction;->getActionType()Ljava/lang/String;

    move-result-object v4

    .line 148
    .local v4, "type":Ljava/lang/String;
    if-eqz v4, :cond_0

    if-eqz v2, :cond_0

    .line 149
    const-string v5, "activity"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 150
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 159
    .end local v1    # "displayName":Ljava/lang/String;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "localCardName":Ljava/lang/String;
    .end local v4    # "type":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 151
    .restart local v1    # "displayName":Ljava/lang/String;
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v3    # "localCardName":Ljava/lang/String;
    .restart local v4    # "type":Ljava/lang/String;
    :cond_1
    const-string v5, "service"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 152
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 153
    :cond_2
    const-string v5, "broadcast"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 154
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public AdjustSettingButtonByTemplate()V
    .locals 4

    .prologue
    const v3, 0x7f020049

    const/4 v2, 0x0

    .line 87
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardTemplate:Lcom/samsung/android/app/headlines/template/CardTemplate;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/template/CardTemplate;->getTemplateType()I

    move-result v0

    .line 89
    .local v0, "templateType":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 90
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 103
    .end local v0    # "templateType":I
    :cond_0
    :goto_0
    return-void

    .line 91
    .restart local v0    # "templateType":I
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 92
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 93
    :cond_2
    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 94
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 95
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 96
    :cond_3
    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 97
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 98
    :cond_4
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 100
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public getCardId()I
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v0

    return v0
.end method

.method public getTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getTemplate()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 128
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 134
    :goto_0
    return-void

    .line 131
    :pswitch_0
    const-string v0, "action1"

    invoke-direct {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->doAction(Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :pswitch_data_0
    .packed-switch 0x7f0c004c
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 115
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 116
    return-void
.end method

.method public showLoadingViews()V
    .locals 4

    .prologue
    .line 162
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardTemplate:Lcom/samsung/android/app/headlines/template/CardTemplate;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/template/CardTemplate;->getRefreshView()Z

    move-result v2

    if-nez v2, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->isUpdating:Z

    if-nez v2, :cond_0

    .line 165
    const-string v2, "HeadlinesCardView"

    const-string v3, "progress animation is started."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    if-eqz v2, :cond_2

    .line 167
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 169
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    if-eqz v2, :cond_3

    .line 170
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 171
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 174
    :cond_3
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->isUpdating:Z

    .line 176
    new-instance v1, Lcom/samsung/android/app/headlines/view/HeadlinesCardView$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView$1;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesCardView;)V

    .line 189
    .local v1, "timerTask":Ljava/util/TimerTask;
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 190
    .local v0, "Timer":Ljava/util/Timer;
    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method

.method public showLoadingViews(Z)V
    .locals 5
    .param p1, "gone"    # Z

    .prologue
    const/4 v4, 0x4

    .line 195
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    if-eqz v2, :cond_0

    .line 196
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 198
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardTemplate:Lcom/samsung/android/app/headlines/template/CardTemplate;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/template/CardTemplate;->getRefreshView()Z

    move-result v2

    if-nez v2, :cond_2

    .line 229
    :cond_1
    :goto_0
    return-void

    .line 201
    :cond_2
    iget-boolean v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->isUpdating:Z

    if-nez v2, :cond_1

    .line 202
    const-string v2, "HeadlinesCardView"

    const-string v3, "progress animation is started."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    if-eqz v2, :cond_3

    .line 204
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRefresh:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 206
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    if-eqz v2, :cond_4

    .line 207
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mRotateAnimation:Landroid/view/animation/RotateAnimation;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 208
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 211
    :cond_4
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->isUpdating:Z

    .line 213
    new-instance v1, Lcom/samsung/android/app/headlines/view/HeadlinesCardView$2;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView$2;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesCardView;Z)V

    .line 226
    .local v1, "timerTask":Ljava/util/TimerTask;
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 227
    .local v0, "Timer":Ljava/util/Timer;
    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method

.method public updateAfterLoadingViews()V
    .locals 2

    .prologue
    .line 232
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->isUpdating:Z

    if-eqz v0, :cond_0

    .line 233
    const-string v0, "HeadlinesCardView"

    const-string v1, "progress animation is finished."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->isUpdating:Z

    .line 235
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 237
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 242
    :cond_0
    return-void
.end method

.method public updateAfterLoadingViews(Z)V
    .locals 2
    .param p1, "gone"    # Z

    .prologue
    .line 245
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->isUpdating:Z

    if-eqz v0, :cond_1

    .line 246
    const-string v0, "HeadlinesCardView"

    const-string v1, "progress animation is finished."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->isUpdating:Z

    .line 249
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 251
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mProgress:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 253
    :cond_0
    if-nez p1, :cond_1

    .line 256
    :cond_1
    return-void
.end method

.method public updateCardView(Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 3
    .param p1, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 120
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardTemplate:Lcom/samsung/android/app/headlines/template/CardTemplate;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mInflater:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mViewHolder:Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;

    invoke-virtual {v0, v1, v2, p1}, Lcom/samsung/android/app/headlines/template/CardTemplate;->getCardView(Landroid/view/LayoutInflater;Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;Lcom/samsung/android/magazine/cardchannel/Card;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mCardView:Landroid/view/View;

    .line 121
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->updateAfterLoadingViews()V

    .line 122
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mSettingLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->mSettingLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 124
    :cond_0
    return-void
.end method
