.class public Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;
.super Landroid/app/Activity;
.source "HeadlinesTermsOfUseActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity$DownloadHandler;
    }
.end annotation


# static fields
.field public static final MESSAGE_TERMS_DOWNLOADING_COMPLETED:I = 0x0

.field public static final MESSAGE_TERMS_DOWNLOADING_TIMEOUT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "HeadlinesTermsOfUseActivity"


# instance fields
.field private mBottomActionBar:Landroid/widget/LinearLayout;

.field private mButtonStart:Landroid/widget/TextView;

.field private mCheckPrivacy:Landroid/widget/CheckBox;

.field private mCheckTerms:Landroid/widget/CheckBox;

.field private mDivider:Landroid/widget/ImageView;

.field private mIsAgreed:Ljava/lang/Boolean;

.field private mLayoutStart:Landroid/widget/LinearLayout;

.field private mPrivacyAgreed:Z

.field private mPrivacyDownloaded:Z

.field private mPrivacyPolicy:Ljava/lang/String;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mScrollPolicy:Landroid/widget/ScrollView;

.field private mScrollTerms:Landroid/widget/ScrollView;

.field private mTermsAgreed:Z

.field private mTermsAndConditions:Ljava/lang/String;

.field private mTermsDownloaded:Z

.field private mTermsDownloader:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 47
    iput-boolean v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsAgreed:Z

    .line 48
    iput-boolean v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyAgreed:Z

    .line 49
    iput-boolean v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloaded:Z

    .line 50
    iput-boolean v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyDownloaded:Z

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsAndConditions:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyPolicy:Ljava/lang/String;

    .line 55
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mLayoutStart:Landroid/widget/LinearLayout;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckTerms:Landroid/widget/CheckBox;

    .line 59
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckPrivacy:Landroid/widget/CheckBox;

    .line 60
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mScrollTerms:Landroid/widget/ScrollView;

    .line 61
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mScrollPolicy:Landroid/widget/ScrollView;

    .line 63
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mDivider:Landroid/widget/ImageView;

    .line 64
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mBottomActionBar:Landroid/widget/LinearLayout;

    .line 65
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mButtonStart:Landroid/widget/TextView;

    .line 67
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mIsAgreed:Ljava/lang/Boolean;

    .line 68
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloader:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    .line 330
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->handleMessage(Landroid/os/Message;)V

    return-void
.end method

.method private handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 238
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 275
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->updateTerms()V

    .line 276
    return-void

    .line 240
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 242
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v3, "body"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "body":Ljava/lang/String;
    const-string v3, "type"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 244
    .local v2, "type":Ljava/lang/String;
    const-string v3, "TermsAndConditions"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 245
    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsAndConditions:Ljava/lang/String;

    .line 246
    iput-boolean v5, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloaded:Z

    .line 251
    :cond_0
    :goto_1
    const-string v3, "HeadlinesTermsOfUseActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleMessage() : Type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " data comes."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 247
    :cond_1
    const-string v3, "PrivacyPolicy"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 248
    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyPolicy:Ljava/lang/String;

    .line 249
    iput-boolean v5, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyDownloaded:Z

    goto :goto_1

    .line 255
    .end local v0    # "body":Ljava/lang/String;
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "type":Ljava/lang/String;
    :pswitch_1
    const-string v3, "HeadlinesTermsOfUseActivity"

    const-string v4, "handleMessage() : No Response from Server. TIMEOUT !!!. ScrollView is set to empty string."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    const-string v3, ""

    iput-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsAndConditions:Ljava/lang/String;

    .line 258
    const-string v3, ""

    iput-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyPolicy:Ljava/lang/String;

    .line 260
    iput-boolean v6, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloaded:Z

    .line 261
    iput-boolean v6, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyDownloaded:Z

    .line 263
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloader:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    if-eqz v3, :cond_2

    .line 264
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloader:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->setDownloadHandler(Landroid/os/Handler;)V

    .line 267
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->dismissLoadingDialog()V

    .line 269
    const v3, 0x7f090050

    invoke-static {p0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 270
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->finish()V

    goto :goto_0

    .line 238
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private showLoadingDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 307
    const/4 v0, 0x0

    const v1, 0x7f09000b

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 308
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 309
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 310
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity$3;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 316
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 317
    return-void
.end method

.method private updateLayout()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x4

    const/16 v2, 0x8

    .line 113
    const v0, 0x7f0c003c

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mLayoutStart:Landroid/widget/LinearLayout;

    .line 114
    const v0, 0x7f0c000c

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckTerms:Landroid/widget/CheckBox;

    .line 115
    const v0, 0x7f0c0011

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckPrivacy:Landroid/widget/CheckBox;

    .line 116
    const v0, 0x7f0c0009

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mScrollTerms:Landroid/widget/ScrollView;

    .line 117
    const v0, 0x7f0c000e

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mScrollPolicy:Landroid/widget/ScrollView;

    .line 118
    const v0, 0x7f0c000d

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mDivider:Landroid/widget/ImageView;

    .line 119
    const v0, 0x7f0c0012

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mBottomActionBar:Landroid/widget/LinearLayout;

    .line 120
    const v0, 0x7f0c003d

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mButtonStart:Landroid/widget/TextView;

    .line 122
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mButtonStart:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckTerms:Landroid/widget/CheckBox;

    new-instance v1, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity$1;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckPrivacy:Landroid/widget/CheckBox;

    new-instance v1, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity$2;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mIsAgreed:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 137
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mLayoutStart:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckTerms:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckPrivacy:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mBottomActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mDivider:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mDivider:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mScrollTerms:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mScrollPolicy:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 163
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckTerms:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsAgreed:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 164
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckPrivacy:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyAgreed:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 165
    return-void

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mButtonStart:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 147
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mLayoutStart:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mLayoutStart:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 149
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mLayoutStart:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckTerms:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 152
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckTerms:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckPrivacy:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 155
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckPrivacy:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mScrollTerms:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mScrollPolicy:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mDivider:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mDivider:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateTerms()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 279
    iget-boolean v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloaded:Z

    if-ne v2, v4, :cond_3

    iget-boolean v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyDownloaded:Z

    if-ne v2, v4, :cond_3

    .line 280
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mScrollTerms:Landroid/widget/ScrollView;

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 281
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mScrollPolicy:Landroid/widget/ScrollView;

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 282
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mDivider:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 283
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mDivider:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 285
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mIsAgreed:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 286
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mLayoutStart:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 287
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckTerms:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 288
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mCheckPrivacy:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 289
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mBottomActionBar:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 292
    :cond_1
    const v2, 0x7f0c000b

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 293
    .local v1, "termsTextView":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsAndConditions:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    const v2, 0x7f0c0010

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 296
    .local v0, "privactyTextView":Landroid/widget/TextView;
    const-string v2, "CN"

    invoke-static {v2}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyPolicy:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 297
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyPolicy:Ljava/lang/String;

    const-string v3, "Facebook"

    const v4, 0x7f090034

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyPolicy:Ljava/lang/String;

    .line 298
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyPolicy:Ljava/lang/String;

    const-string v3, "Twitter"

    const v4, 0x7f090035

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyPolicy:Ljava/lang/String;

    .line 300
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyPolicy:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 302
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->dismissLoadingDialog()V

    .line 304
    .end local v0    # "privactyTextView":Landroid/widget/TextView;
    .end local v1    # "termsTextView":Landroid/widget/TextView;
    :cond_3
    return-void
.end method


# virtual methods
.method public dismissLoadingDialog()V
    .locals 2

    .prologue
    .line 320
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 322
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 328
    return-void

    .line 323
    :catch_0
    move-exception v0

    .line 324
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 193
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloaded:Z

    .line 194
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyDownloaded:Z

    .line 196
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloader:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloader:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->setDownloadHandler(Landroid/os/Handler;)V

    .line 200
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 201
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "button"    # Landroid/widget/CompoundButton;
    .param p2, "enabled"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 217
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 227
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsAgreed:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyAgreed:Z

    if-ne v0, v1, :cond_0

    .line 228
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mLayoutStart:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 229
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mButtonStart:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 234
    :goto_1
    return-void

    .line 219
    :sswitch_0
    iput-boolean p2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsAgreed:Z

    goto :goto_0

    .line 223
    :sswitch_1
    iput-boolean p2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyAgreed:Z

    goto :goto_0

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mLayoutStart:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 232
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mButtonStart:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    .line 217
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0c000c -> :sswitch_0
        0x7f0c0011 -> :sswitch_1
    .end sparse-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 169
    const-string v4, "HeadlinesTermsOfUseActivity"

    const-string v5, "onClick() : Start Button is pressed. My Magazine is started."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const-string v4, "mymagazine"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 172
    .local v3, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 173
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "WELCOME_NEXT_BUTTON_PRESSED"

    const/4 v5, 0x1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 174
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 176
    const-string v4, "CN"

    invoke-static {v4}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 177
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 178
    .local v1, "i":Landroid/content/Intent;
    const-string v4, "com.samsung.android.internal.headlines.ALLOW_DATA_USE"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 182
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 183
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->startActivity(Landroid/content/Intent;)V

    .line 185
    const-string v4, "HeadlinesTermsOfUseActivity"

    const-string v5, "onClick() : finish()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    const/4 v4, -0x1

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->setResult(I)V

    .line 188
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->finish()V

    .line 189
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 360
    const-string v0, "HeadlinesTermsOfUseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConfigurationChanged() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 371
    :pswitch_0
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->setContentView(I)V

    .line 373
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->updateLayout()V

    .line 374
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->updateTerms()V

    .line 376
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 377
    return-void

    .line 361
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 73
    const-string v3, "HeadlinesTermsOfUseActivity"

    const-string v4, "onCreate()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const-string v3, "mymagazine"

    invoke-virtual {p0, v3, v6}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 76
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v3, "WELCOME_NEXT_BUTTON_PRESSED"

    invoke-interface {v1, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mIsAgreed:Ljava/lang/Boolean;

    .line 78
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mIsAgreed:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 80
    .local v2, "window":Landroid/view/Window;
    const/16 v3, 0x400

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 83
    .end local v2    # "window":Landroid/view/Window;
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mIsAgreed:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_2

    .line 85
    const v3, 0x7f030003

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->setContentView(I)V

    .line 90
    :goto_0
    invoke-static {p0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isOrientationSupport(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 91
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->setRequestedOrientation(I)V

    .line 93
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 94
    .local v0, "actionbar":Landroid/app/ActionBar;
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 95
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 96
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 97
    const v3, 0x7f090047

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setTitle(I)V

    .line 99
    invoke-static {p0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isConnected(Landroid/content/Context;)Z

    move-result v3

    if-ne v3, v5, :cond_3

    .line 100
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->showLoadingDialog()V

    .line 102
    new-instance v3, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloader:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    .line 103
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloader:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    new-instance v4, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity$DownloadHandler;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity$DownloadHandler;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->setDownloadHandler(Landroid/os/Handler;)V

    .line 104
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloader:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->start()V

    .line 109
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->updateLayout()V

    .line 110
    return-void

    .line 87
    .end local v0    # "actionbar":Landroid/app/ActionBar;
    :cond_2
    const v3, 0x7f030004

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->setContentView(I)V

    goto :goto_0

    .line 106
    .restart local v0    # "actionbar":Landroid/app/ActionBar;
    :cond_3
    invoke-static {p0}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->dialogNoNetworkPopup(Landroid/content/Context;)Z

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 205
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloaded:Z

    .line 206
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mPrivacyDownloaded:Z

    .line 208
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloader:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->mTermsDownloader:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->setDownloadHandler(Landroid/os/Handler;)V

    .line 212
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 213
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 348
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 355
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 350
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->finish()V

    .line 351
    const/4 v0, 0x1

    goto :goto_0

    .line 348
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
