.class public Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;
.super Landroid/os/AsyncTask;
.source "CardTemplateUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/template/CardTemplateUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "decodeBitmapTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/samsung/android/magazine/cardchannel/Card;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field mElementKey:Ljava/lang/String;

.field mGradientView:Landroid/widget/ImageView;

.field mUrl:Ljava/lang/String;

.field mView:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "elementKey"    # Ljava/lang/String;
    .param p3, "v"    # Landroid/widget/ImageView;
    .param p4, "gradientView"    # Landroid/widget/ImageView;

    .prologue
    const/4 v0, 0x0

    .line 108
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 102
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mContext:Landroid/content/Context;

    .line 103
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mElementKey:Ljava/lang/String;

    .line 104
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mView:Landroid/widget/ImageView;

    .line 105
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mGradientView:Landroid/widget/ImageView;

    .line 106
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mUrl:Ljava/lang/String;

    .line 109
    iput-object p1, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mContext:Landroid/content/Context;

    .line 110
    iput-object p2, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mElementKey:Ljava/lang/String;

    .line 111
    iput-object p3, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mView:Landroid/widget/ImageView;

    .line 112
    iput-object p4, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mGradientView:Landroid/widget/ImageView;

    .line 113
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/samsung/android/magazine/cardchannel/Card;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "params"    # [Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 134
    aget-object v1, p1, v5

    .line 135
    .local v1, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    const/4 v0, 0x0

    .line 136
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mElementKey:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardElement(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardElement;

    move-result-object v2

    .line 137
    .local v2, "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    instance-of v5, v2, Lcom/samsung/android/magazine/cardchannel/CardImage;

    if-eqz v5, :cond_2

    .line 138
    check-cast v2, Lcom/samsung/android/magazine/cardchannel/CardImage;

    .end local v2    # "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    invoke-virtual {v2}, Lcom/samsung/android/magazine/cardchannel/CardImage;->getImageUri()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mUrl:Ljava/lang/String;

    .line 139
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mUrl:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 141
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mElementKey:Ljava/lang/String;

    invoke-static {v1, v5}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getCacheBitmap(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    .line 142
    if-eqz v0, :cond_1

    move-object v4, v0

    .line 169
    :cond_0
    :goto_0
    return-object v4

    .line 145
    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mUrl:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 153
    :goto_1
    if-eqz v0, :cond_0

    .line 167
    :cond_2
    if-eqz v0, :cond_3

    .line 168
    const-string v4, "CardTemplateUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "decodeBitmapTask bitmap size : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "x"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move-object v4, v0

    .line 169
    goto :goto_0

    .line 146
    :catch_0
    move-exception v3

    .line 147
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 148
    const-string v5, "CardTemplateUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "decodeBitmapTask cache trim size. current size : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # getter for: Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;
    invoke-static {}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->access$000()Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    # getter for: Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;
    invoke-static {}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->access$000()Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->trimToSize(I)V

    .line 150
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mUrl:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v0

    goto :goto_1

    .line 155
    .end local v3    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v3

    .line 156
    .local v3, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 158
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v3

    .line 159
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 161
    .end local v3    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v3

    .line 162
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v3}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 101
    check-cast p1, [Lcom/samsung/android/magazine/cardchannel/Card;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->doInBackground([Lcom/samsung/android/magazine/cardchannel/Card;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceAsColor"
        }
    .end annotation

    .prologue
    .line 118
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 119
    if-nez p1, :cond_1

    .line 120
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mView:Landroid/widget/ImageView;

    const v1, 0x7f07000b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    # getter for: Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;
    invoke-static {}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->access$000()Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 126
    # getter for: Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;
    invoke-static {}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->access$000()Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->mGradientView:Landroid/widget/ImageView;

    # invokes: Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->setImage(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    invoke-static {v0, p1, v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->access$100(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 101
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
