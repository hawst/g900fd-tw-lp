.class public Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;
.super Lcom/samsung/android/app/headlines/template/CardTemplate;
.source "CardTemplate_NoContent.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "HeadlinesCardTemplate_NoContent"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/headlines/template/CardTemplate;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    .line 28
    iput-object p1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mContext:Landroid/content/Context;

    .line 30
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTemplateType:I

    .line 31
    return-void
.end method


# virtual methods
.method public getCardView(Landroid/view/LayoutInflater;Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;Lcom/samsung/android/magazine/cardchannel/Card;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "viewHolder"    # Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;
    .param p3, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceAsColor"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 42
    iput-object p3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 44
    invoke-virtual {p2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->isEnabled()Z

    move-result v5

    if-ne v5, v7, :cond_3

    .line 45
    iget-object v5, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 46
    iget-object v5, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    iput-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTextView1:Landroid/widget/TextView;

    .line 47
    iget-object v5, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    iput-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTextView2:Landroid/widget/TextView;

    .line 48
    iget-object v5, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView1:Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageView1:Landroid/widget/ImageView;

    .line 49
    iget-object v5, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageViewBg:Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageViewBg:Landroid/widget/ImageView;

    .line 73
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v5, :cond_2

    .line 75
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTextView1:Landroid/widget/TextView;

    if-eqz v5, :cond_0

    .line 76
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/cardchannel/Card;->getProviderName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.samsung.android.app.headlines"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 77
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "cardName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mContext:Landroid/content/Context;

    invoke-static {v5, v1}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getCardDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 79
    .local v2, "localCardName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTextView1:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    .end local v1    # "cardName":Ljava/lang/String;
    .end local v2    # "localCardName":Ljava/lang/String;
    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTextView2:Landroid/widget/TextView;

    if-eqz v5, :cond_1

    .line 86
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTextView2:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mContext:Landroid/content/Context;

    const v7, 0x7f090011

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageViewBg:Landroid/widget/ImageView;

    if-eqz v5, :cond_2

    .line 90
    const-string v5, "image1"

    invoke-virtual {p3, v5}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardElement(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardElement;

    move-result-object v0

    .line 91
    .local v0, "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    if-eqz v0, :cond_6

    .line 92
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v7, "image1"

    iget-object v8, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageViewBg:Landroid/widget/ImageView;

    invoke-static {v5, v6, v7, v8}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 115
    .end local v0    # "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    :cond_2
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTemplateContainer:Landroid/view/ViewGroup;

    return-object v5

    .line 51
    :cond_3
    const v5, 0x7f03001c

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iput-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 52
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v5, :cond_4

    .line 53
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTemplateContainer:Landroid/view/ViewGroup;

    const v6, 0x7f0c0049

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTextView1:Landroid/widget/TextView;

    .line 54
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTemplateContainer:Landroid/view/ViewGroup;

    const v6, 0x7f0c004b

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTextView2:Landroid/widget/TextView;

    .line 55
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTemplateContainer:Landroid/view/ViewGroup;

    const v6, 0x7f0c004f

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageView1:Landroid/widget/ImageView;

    .line 56
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTemplateContainer:Landroid/view/ViewGroup;

    const v6, 0x7f0c0040

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageViewBg:Landroid/widget/ImageView;

    .line 64
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v5, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 65
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTextView1:Landroid/widget/TextView;

    iput-object v5, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    .line 66
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTextView2:Landroid/widget/TextView;

    iput-object v5, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    .line 67
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageView1:Landroid/widget/ImageView;

    iput-object v5, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView1:Landroid/widget/ImageView;

    .line 68
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageViewBg:Landroid/widget/ImageView;

    iput-object v5, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageViewBg:Landroid/widget/ImageView;

    .line 70
    invoke-virtual {p2, v7}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->setEnabled(Z)V

    goto/16 :goto_0

    .line 81
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mTextView1:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v7, "text1"

    invoke-static {v6, v7}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 94
    .restart local v0    # "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v1

    .line 95
    .restart local v1    # "cardName":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getWelcomeBackgroundImage(Ljava/lang/String;)I

    move-result v3

    .line 96
    .local v3, "resId":I
    if-lez v3, :cond_7

    .line 97
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageViewBg:Landroid/widget/ImageView;

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 99
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageViewBg:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-static {v6}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getBackgroundColor(Lcom/samsung/android/magazine/cardchannel/Card;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 100
    const-string v5, "HeadlinesCardTemplate_NoContent"

    const-string v6, "getCardView : using bg color"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageView1:Landroid/widget/ImageView;

    if-eqz v5, :cond_2

    .line 104
    invoke-static {v1}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getWelcomeSocialIcon(Ljava/lang/String;)I

    move-result v4

    .line 105
    .local v4, "resSocialId":I
    if-lez v4, :cond_2

    .line 106
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageView1:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 107
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;->mImageView1:Landroid/widget/ImageView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public onCardCollapse()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method public onCardExpand()Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method
