.class public final Lcom/samsung/android/app/headlines/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final a_new_version_is_available:I = 0x7f09005b

.field public static final about:I = 0x7f090046

.field public static final action_help:I = 0x7f090014

.field public static final an_error_has_occurred:I = 0x7f090008

.field public static final arts:I = 0x7f09001d

.field public static final at_least_one_news_topic_or_social:I = 0x7f090051

.field public static final at_least_one_news_topic_or_social_network_selection:I = 0x7f09005d

.field public static final auto_refrash_detail:I = 0x7f09000d

.field public static final auto_refresh_on_opening:I = 0x7f090015

.field public static final auto_refresh_title:I = 0x7f09003d

.field public static final background_data_check_msg_chn:I = 0x7f090029

.field public static final books:I = 0x7f090023

.field public static final business:I = 0x7f090019

.field public static final checked:I = 0x7f09003e

.field public static final connect_to_network:I = 0x7f090059

.field public static final content_will_be_available_later:I = 0x7f09004b

.field public static final customise:I = 0x7f090061

.field public static final customize_my_magazine:I = 0x7f090060

.field public static final disabled_application:I = 0x7f09000a

.field public static final do_not_show:I = 0x7f09002a

.field public static final double_tap_to_collapse_the_list:I = 0x7f090042

.field public static final double_tap_to_expand_the_list:I = 0x7f090041

.field public static final download:I = 0x7f090003

.field public static final download_application:I = 0x7f090002

.field public static final drag_down_to_refresh:I = 0x7f090012

.field public static final english_content:I = 0x7f090024

.field public static final fivehundredpx:I = 0x7f090030

.field public static final flickr:I = 0x7f090031

.field public static final food:I = 0x7f09001f

.field public static final googpleplus:I = 0x7f090032

.field public static final header:I = 0x7f090040

.field public static final i_agree:I = 0x7f090058

.field public static final information:I = 0x7f090057

.field public static final is_not_installed:I = 0x7f090063

.field public static final linkedin:I = 0x7f090033

.field public static final living:I = 0x7f09001e

.field public static final loading:I = 0x7f09000b

.field public static final local:I = 0x7f090027

.field public static final menu:I = 0x7f09003b

.field public static final mobile_data_check_popup_msg:I = 0x7f09002d

.field public static final mobile_data_check_popup_title:I = 0x7f09002b

.field public static final more_options:I = 0x7f090054

.field public static final music:I = 0x7f090022

.field public static final my_magazine_requires_a_network_connection:I = 0x7f09004d

.field public static final my_magazine_requires_a_network_connection_chn:I = 0x7f09004e

.field public static final my_magazine_requires_a_network_connection_vzw:I = 0x7f09005f

.field public static final mymagazine:I = 0x7f090039

.field public static final navigate_up:I = 0x7f090053

.field public static final netherlands:I = 0x7f090025

.field public static final new_and_noteworthy:I = 0x7f090017

.field public static final new_story_added:I = 0x7f09005e

.field public static final news:I = 0x7f090018

.field public static final no_content_to_display:I = 0x7f090011

.field public static final no_network_connect:I = 0x7f090045

.field public static final no_network_connection:I = 0x7f090000

.field public static final no_response_from_server:I = 0x7f090050

.field public static final not_checked:I = 0x7f09003f

.field public static final not_now:I = 0x7f09005a

.field public static final open_app_info:I = 0x7f090007

.field public static final personalize_my_magazine:I = 0x7f090062

.field public static final photos:I = 0x7f09001c

.field public static final privacy_policy:I = 0x7f090049

.field public static final pull_to_refresh_release_label:I = 0x7f09000c

.field public static final refresh:I = 0x7f09003c

.field public static final renren:I = 0x7f090034

.field public static final samsung_apps:I = 0x7f090004

.field public static final section_news:I = 0x7f090055

.field public static final section_social:I = 0x7f090056

.field public static final select_at_least_one_item:I = 0x7f090016

.field public static final settings:I = 0x7f090013

.field public static final sign_in_to:I = 0x7f09000f

.field public static final sina_weibo:I = 0x7f090035

.field public static final sports:I = 0x7f09001b

.field public static final style:I = 0x7f090021

.field public static final tap_to_get_started:I = 0x7f09000e

.field public static final technology:I = 0x7f09001a

.field public static final terms_and_conditions:I = 0x7f090048

.field public static final terms_of_use:I = 0x7f090047

.field public static final to_use_this_function:I = 0x7f090005

.field public static final todays_picks:I = 0x7f09002f

.field public static final transfer_data_in_background:I = 0x7f090028

.field public static final travel:I = 0x7f090020

.field public static final tumblr:I = 0x7f090036

.field public static final turn_off_my_magazine:I = 0x7f090052

.field public static final twitter:I = 0x7f090037

.field public static final uk:I = 0x7f090026

.field public static final unable_to_open_this_application:I = 0x7f090006

.field public static final unable_to_sign_in:I = 0x7f090009

.field public static final update:I = 0x7f09005c

.field public static final updating:I = 0x7f09003a

.field public static final welcome_next:I = 0x7f090044

.field public static final welcome_start:I = 0x7f09004a

.field public static final welcome_title:I = 0x7f090043

.field public static final will_be_displayed_chn:I = 0x7f090001

.field public static final wlan_connection_required:I = 0x7f09004f

.field public static final wlan_data_check_popup_msg:I = 0x7f09002e

.field public static final wlan_data_check_popup_title:I = 0x7f09002c

.field public static final your_personal_magazine:I = 0x7f090010

.field public static final your_personal_magazine_that_delivers_content:I = 0x7f09004c

.field public static final youtube:I = 0x7f090038


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 534
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
