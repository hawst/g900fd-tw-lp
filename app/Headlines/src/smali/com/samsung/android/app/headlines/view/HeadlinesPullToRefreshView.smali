.class public Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;
.super Landroid/widget/RelativeLayout;
.source "HeadlinesPullToRefreshView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$4;,
        Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;,
        Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;
    }
.end annotation


# static fields
.field private static final FRICTION:F = 1.8f

.field private static final GRAVITY:F = 0.65f

.field private static final REFRESH_LOADING_DELAY:I = 0x190

.field private static final TAG:Ljava/lang/String; = "HeadlinesPullToRefreshView"


# instance fields
.field private PULL_TO_REFRESH_BACK_HEIGHT:I

.field private PULL_TO_REFRESH_MAXIMUM_HEIGHT:I

.field private PULL_TO_REFRESH_STATE_HEIGHT:I

.field private mContainer:Landroid/widget/FrameLayout;

.field private mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mInitialContainerY:F

.field private mInitialY:F

.field private mProgressAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;

.field private mPropertyListener:Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;

.field private mPullToRefreshModeChangeListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshModeChangeListener;

.field private mRefreshLoadingProgressBar:Landroid/widget/ProgressBar;

.field private mRefreshRunnable:Ljava/lang/Runnable;

.field private mRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

.field private mRefreshView:Landroid/widget/RelativeLayout;

.field private mRefreshViewAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mRefreshViewImage:Landroid/widget/ImageView;

.field private mRefreshViewText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 73
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 31
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContext:Landroid/content/Context;

    .line 33
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    .line 36
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mInitialContainerY:F

    .line 37
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContainer:Landroid/widget/FrameLayout;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshView:Landroid/widget/RelativeLayout;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewImage:Landroid/widget/ImageView;

    .line 41
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewText:Landroid/widget/TextView;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshLoadingProgressBar:Landroid/widget/ProgressBar;

    .line 45
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mProgressAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshRunnable:Ljava/lang/Runnable;

    .line 52
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mInitialY:F

    .line 57
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mHandler:Landroid/os/Handler;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mPullToRefreshModeChangeListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshModeChangeListener;

    .line 60
    const/16 v0, 0xb4

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_STATE_HEIGHT:I

    .line 61
    const/16 v0, 0xf0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_MAXIMUM_HEIGHT:I

    .line 62
    const/16 v0, 0x96

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_BACK_HEIGHT:I

    .line 296
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$2;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mPropertyListener:Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;

    .line 304
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$3;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 74
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContext:Landroid/content/Context;

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 78
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContext:Landroid/content/Context;

    .line 33
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    .line 36
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mInitialContainerY:F

    .line 37
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContainer:Landroid/widget/FrameLayout;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshView:Landroid/widget/RelativeLayout;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewImage:Landroid/widget/ImageView;

    .line 41
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewText:Landroid/widget/TextView;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshLoadingProgressBar:Landroid/widget/ProgressBar;

    .line 45
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mProgressAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshRunnable:Ljava/lang/Runnable;

    .line 52
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mInitialY:F

    .line 57
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mHandler:Landroid/os/Handler;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mPullToRefreshModeChangeListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshModeChangeListener;

    .line 60
    const/16 v0, 0xb4

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_STATE_HEIGHT:I

    .line 61
    const/16 v0, 0xf0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_MAXIMUM_HEIGHT:I

    .line 62
    const/16 v0, 0x96

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_BACK_HEIGHT:I

    .line 296
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$2;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mPropertyListener:Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;

    .line 304
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$3;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 79
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContext:Landroid/content/Context;

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->startRefreshViewBackAnimation()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContainer:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshLoadingProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContainer:Landroid/widget/FrameLayout;

    .line 84
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mInitialContainerY:F

    .line 86
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 87
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03000a

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 88
    const v1, 0x7f0c0025

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshView:Landroid/widget/RelativeLayout;

    .line 89
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshView:Landroid/widget/RelativeLayout;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setImportantForAccessibility(I)V

    .line 91
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0c0027

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewImage:Landroid/widget/ImageView;

    .line 92
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0c0028

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewText:Landroid/widget/TextView;

    .line 93
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0c0026

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshLoadingProgressBar:Landroid/widget/ProgressBar;

    .line 94
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshLoadingProgressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 96
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080081

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_STATE_HEIGHT:I

    .line 97
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08007d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_MAXIMUM_HEIGHT:I

    .line 98
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08007f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_BACK_HEIGHT:I

    .line 99
    return-void
.end method

.method private startRefreshViewBackAnimation()V
    .locals 4

    .prologue
    .line 287
    new-instance v0, Lcom/samsung/android/app/headlines/PropertyAnimation;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getY()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mInitialContainerY:F

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mPropertyListener:Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/headlines/PropertyAnimation;-><init>(FFLcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;)V

    .line 288
    .local v0, "refreshViewAnimation":Lcom/samsung/android/app/headlines/PropertyAnimation;
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/PropertyAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 289
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/app/headlines/PropertyAnimation;->setDuration(J)V

    .line 290
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/PropertyAnimation;->setFillAfter(Z)V

    .line 292
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContext:Landroid/content/Context;

    const v2, 0x10c0002

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/PropertyAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 293
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 294
    return-void
.end method


# virtual methods
.method public MoveToZeroPosition()V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContainer:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContainer:Landroid/widget/FrameLayout;

    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mInitialContainerY:F

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setY(F)V

    .line 284
    :cond_0
    return-void
.end method

.method public changeRefreshState(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;)V
    .locals 10
    .param p1, "refreshState"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    .prologue
    const-wide/16 v8, 0xc8

    const/4 v6, 0x0

    const/16 v4, 0x8

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 223
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    if-ne v2, p1, :cond_1

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    .line 228
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshRunnable:Ljava/lang/Runnable;

    if-nez v2, :cond_0

    .line 231
    sget-object v2, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$4;->$SwitchMap$com$samsung$android$app$headlines$view$HeadlinesPullToRefreshView$PullToRefreshState:[I

    invoke-virtual {p1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 275
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mPullToRefreshModeChangeListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshModeChangeListener;

    if-eqz v2, :cond_0

    .line 276
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mPullToRefreshModeChangeListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshModeChangeListener;

    invoke-interface {v2, p1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshModeChangeListener;->onPullToRefreshModeChanged(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;)V

    goto :goto_0

    .line 233
    :pswitch_0
    const-string v2, "HeadlinesPullToRefreshView"

    const-string v3, "changeRefreshState() : STATE_NONE."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 237
    :pswitch_1
    const-string v2, "HeadlinesPullToRefreshView"

    const-string v3, "changeRefreshState() : STATE_INIT."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->setVisibility(I)V

    .line 239
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setRotation(F)V

    goto :goto_1

    .line 243
    :pswitch_2
    const-string v2, "HeadlinesPullToRefreshView"

    const-string v3, "changeRefreshState() : STATE_PULL."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->setVisibility(I)V

    .line 245
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshLoadingProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 246
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 247
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewText:Landroid/widget/TextView;

    const v3, 0x7f090012

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 249
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewImage:Landroid/widget/ImageView;

    new-array v3, v5, [Landroid/animation/PropertyValuesHolder;

    sget-object v4, Landroid/view/View;->ROTATION:Landroid/util/Property;

    new-array v5, v5, [F

    aput v6, v5, v7

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 250
    .local v1, "iconRotateAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 251
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_1

    .line 255
    .end local v1    # "iconRotateAnimator":Landroid/animation/ObjectAnimator;
    :pswitch_3
    const-string v2, "HeadlinesPullToRefreshView"

    const-string v3, "changeRefreshState() : STATE_RELEASE."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshLoadingProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 257
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewText:Landroid/widget/TextView;

    const v3, 0x7f09000c

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 259
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewImage:Landroid/widget/ImageView;

    new-array v3, v5, [Landroid/animation/PropertyValuesHolder;

    sget-object v4, Landroid/view/View;->ROTATION:Landroid/util/Property;

    new-array v5, v5, [F

    const/high16 v6, -0x3ccc0000    # -180.0f

    aput v6, v5, v7

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 260
    .local v0, "iconReverseRotateAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 261
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto/16 :goto_1

    .line 265
    .end local v0    # "iconReverseRotateAnimator":Landroid/animation/ObjectAnimator;
    :pswitch_4
    const-string v2, "HeadlinesPullToRefreshView"

    const-string v3, "changeRefreshState() : STATE_REFRESHING."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshLoadingProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 267
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 268
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshViewText:Landroid/widget/TextView;

    const v3, 0x7f09003a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 270
    new-instance v2, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$1;)V

    iput-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mProgressAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;

    .line 271
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mProgressAnimationThread:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$ProgressAanimationThread;->start()V

    goto/16 :goto_1

    .line 231
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getRefreshState()Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    return-object v0
.end method

.method public isRefreshing()Z
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 105
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->init()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setPullToRefreshModeChangeListener(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshModeChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshModeChangeListener;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mPullToRefreshModeChangeListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshModeChangeListener;

    .line 113
    return-void
.end method

.method public touchDelegate(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 120
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    .line 122
    .local v4, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 123
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move v5, v6

    .line 219
    :goto_1
    return v5

    .line 125
    :pswitch_1
    const-string v5, "HeadlinesPullToRefreshView"

    const-string v7, "touchDelegate() - MotionEvent.ACTION_DOWN."

    invoke-static {v5, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshRunnable:Ljava/lang/Runnable;

    if-nez v5, :cond_0

    .line 129
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mInitialY:F

    .line 131
    sget-object v5, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$4;->$SwitchMap$com$samsung$android$app$headlines$view$HeadlinesPullToRefreshView$PullToRefreshState:[I

    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-virtual {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->ordinal()I

    move-result v7

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_1

    goto :goto_0

    .line 133
    :pswitch_2
    sget-object v5, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_INIT:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->changeRefreshState(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;)V

    goto :goto_0

    .line 147
    :pswitch_3
    const-string v7, "HeadlinesPullToRefreshView"

    const-string v8, "touchDelegate() : MotionEvent.ACTION_MOVE."

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshRunnable:Ljava/lang/Runnable;

    if-eqz v7, :cond_1

    .line 149
    const-string v5, "HeadlinesPullToRefreshView"

    const-string v7, "touchDelegate() : MotionEvent.ACTION_MOVE - Refresh is Running."

    invoke-static {v5, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 153
    :cond_1
    sget-object v7, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$4;->$SwitchMap$com$samsung$android$app$headlines$view$HeadlinesPullToRefreshView$PullToRefreshState:[I

    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-virtual {v8}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_2

    goto :goto_0

    .line 157
    :pswitch_4
    iget v6, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mInitialY:F

    sub-float v1, v4, v6

    .line 159
    .local v1, "diff":F
    const v6, 0x3fe66666    # 1.8f

    div-float v6, v1, v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    int-to-float v6, v6

    const v7, 0x3f266666    # 0.65f

    mul-float v2, v6, v7

    .line 160
    .local v2, "gap":F
    const-string v6, "HeadlinesPullToRefreshView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "touchDelegate() : ACTION_MOVE. gap = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    const/4 v6, 0x0

    cmpg-float v6, v2, v6

    if-gtz v6, :cond_2

    .line 162
    const/4 v2, 0x0

    .line 163
    sget-object v6, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_INIT:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->changeRefreshState(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;)V

    .line 172
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v2}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    goto/16 :goto_1

    .line 164
    :cond_2
    iget v6, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_STATE_HEIGHT:I

    int-to-float v6, v6

    cmpg-float v6, v2, v6

    if-gez v6, :cond_3

    .line 165
    sget-object v6, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_PULL:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->changeRefreshState(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;)V

    goto :goto_2

    .line 167
    :cond_3
    iget v6, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_MAXIMUM_HEIGHT:I

    int-to-float v6, v6

    cmpl-float v6, v2, v6

    if-lez v6, :cond_4

    .line 168
    iget v6, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_MAXIMUM_HEIGHT:I

    int-to-float v2, v6

    .line 170
    :cond_4
    sget-object v6, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_RELEASE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->changeRefreshState(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;)V

    goto :goto_2

    .line 183
    .end local v1    # "diff":F
    .end local v2    # "gap":F
    :pswitch_5
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshRunnable:Ljava/lang/Runnable;

    if-nez v7, :cond_0

    .line 186
    sget-object v7, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$4;->$SwitchMap$com$samsung$android$app$headlines$view$HeadlinesPullToRefreshView$PullToRefreshState:[I

    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshState:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-virtual {v8}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_3

    goto/16 :goto_0

    .line 189
    :pswitch_6
    sget-object v5, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->changeRefreshState(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;)V

    goto/16 :goto_0

    .line 193
    :pswitch_7
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->startRefreshViewBackAnimation()V

    .line 194
    sget-object v5, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->changeRefreshState(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;)V

    goto/16 :goto_0

    .line 198
    :pswitch_8
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContainer:Landroid/widget/FrameLayout;

    const-string v8, "translationY"

    new-array v5, v5, [F

    iget v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->PULL_TO_REFRESH_BACK_HEIGHT:I

    int-to-float v9, v9

    aput v9, v5, v6

    invoke-static {v7, v8, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 199
    .local v3, "refreshBackAnimator":Landroid/animation/ObjectAnimator;
    const-wide/16 v8, 0x12c

    invoke-virtual {v3, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 200
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContext:Landroid/content/Context;

    const v7, 0x10c0034

    invoke-static {v5, v7}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 201
    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->start()V

    .line 203
    sget-object v5, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_REFRESHING:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->changeRefreshState(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;)V

    .line 205
    new-instance v5, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$1;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)V

    iput-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshRunnable:Ljava/lang/Runnable;

    .line 211
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshRunnable:Ljava/lang/Runnable;

    const-wide/16 v8, 0x190

    invoke-virtual {v5, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_5
    .end packed-switch

    .line 131
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 153
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 186
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
