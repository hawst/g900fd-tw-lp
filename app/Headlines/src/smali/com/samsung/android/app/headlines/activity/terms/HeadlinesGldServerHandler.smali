.class public Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;
.super Ljava/lang/Object;
.source "HeadlinesGldServerHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final _GLD_API:Ljava/lang/String; = "/always-gld-svc/GLD"

.field private static final _GLD_Hub_Port_default:I = 0x50

.field private static final _GLD_Hub_URL_1:Ljava/lang/String; = "http://gldhub.samsungalways.com"

.field private static final prefix_plainText:Ljava/lang/String; = "always.service"


# instance fields
.field private _GLD_Hub_URLs:[Ljava/lang/String;

.field private mResponse:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;

.field requestSocket:Ljava/net/Socket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "http://gldhub.samsungalways.com"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->_GLD_Hub_URLs:[Ljava/lang/String;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->mResponse:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;

    .line 192
    return-void
.end method

.method private communicateToServer(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "_GLD_Hub_URL"    # Ljava/lang/String;
    .param p2, "_GLD_Hub_Port"    # I
    .param p3, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;,
            Ljava/io/IOException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 94
    const/4 v5, 0x0

    .line 96
    .local v5, "m_receiveData":Ljava/lang/String;
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/always-gld-svc/GLD"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 98
    .local v4, "httpPost":Lorg/apache/http/client/methods/HttpPost;
    new-instance v7, Lorg/apache/http/entity/StringEntity;

    const-string v8, "UTF8"

    invoke-direct {v7, p3, v8}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 99
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 100
    .local v0, "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    invoke-virtual {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v7

    const/16 v8, 0xbb8

    invoke-static {v7, v8}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 102
    invoke-virtual {v0, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    .line 103
    .local v6, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    .line 104
    .local v3, "entity":Lorg/apache/http/HttpEntity;
    const-string v2, "UTF-8"

    .line 107
    .local v2, "encoding":Ljava/lang/String;
    :try_start_0
    invoke-static {v3, v2}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 114
    :goto_0
    return-object v5

    .line 108
    :catch_0
    move-exception v1

    .line 109
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 110
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v1

    .line 111
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private getEncodedMCCode()Ljava/lang/String;
    .locals 5

    .prologue
    .line 167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    .local v1, "result":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->getMCCode()Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "mmc":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 170
    const/16 v2, 0x4d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 171
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    :cond_0
    sget-object v2, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getEncodedMMCCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getMCCode()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 178
    sget-object v3, Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;->instance:Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 180
    .local v2, "mTelMgr":Landroid/telephony/TelephonyManager;
    const/4 v1, 0x0

    .line 181
    .local v1, "mNetworkOperator":Ljava/lang/String;
    const/4 v0, 0x0

    .line 182
    .local v0, "lMCCCode":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_0

    .line 183
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 185
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v5, :cond_1

    .line 186
    const/4 v3, 0x0

    invoke-virtual {v1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 189
    :cond_1
    return-object v0
.end method

.method private getRequestBody()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x7c

    .line 142
    const-string v0, ""

    .line 143
    .local v0, "inputParams":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->getEncodedMCCode()Ljava/lang/String;

    move-result-object v1

    .line 145
    .local v1, "lMCCCode":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-gtz v3, :cond_2

    .line 156
    :cond_0
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    .local v2, "str":Ljava/lang/StringBuilder;
    const-string v3, "always.service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 159
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 160
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    :cond_1
    sget-object v3, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getRequestBody : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 153
    .end local v2    # "str":Ljava/lang/StringBuilder;
    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private postFindAddress(Ljava/lang/String;)Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;
    .locals 5
    .param p1, "serverResponse"    # Ljava/lang/String;

    .prologue
    .line 118
    const/4 v2, 0x0

    .line 120
    .local v2, "responseBody":Ljava/lang/String;
    new-instance v3, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;

    invoke-direct {v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->mResponse:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;

    .line 123
    :try_start_0
    invoke-static {p1}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldCipher;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 124
    const-string v3, "[|]"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 126
    .local v1, "lines":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v3, v1

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 127
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->mResponse:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;

    const/4 v4, 0x0

    aget-object v4, v1, v4

    iput-object v4, v3, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;->domainServerAddr:Ljava/lang/String;

    .line 128
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->mResponse:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;

    const/4 v4, 0x1

    aget-object v4, v1, v4

    iput-object v4, v3, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;->countryCode:Ljava/lang/String;

    .line 129
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->mResponse:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;

    const/4 v4, 0x2

    aget-object v4, v1, v4

    iput-object v4, v3, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;->mccCode:Ljava/lang/String;

    .line 130
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->mResponse:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;

    const/4 v4, 0x3

    aget-object v4, v1, v4

    iput-object v4, v3, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;->available:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    .end local v1    # "lines":[Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->mResponse:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;

    return-object v3

    .line 133
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public tryFindAddress()Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;
    .locals 11

    .prologue
    .line 44
    const/4 v2, 0x0

    .line 45
    .local v2, "encryptedRequest":Ljava/lang/String;
    const/4 v5, 0x0

    .line 46
    .local v5, "serverResponse":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->getRequestBody()Ljava/lang/String;

    move-result-object v4

    .line 49
    .local v4, "requestPlainBody":Ljava/lang/String;
    :try_start_0
    invoke-static {v4}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldCipher;->encrypt(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 56
    :goto_0
    iget-object v8, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->_GLD_Hub_URLs:[Ljava/lang/String;

    .line 58
    .local v8, "uri":[Ljava/lang/String;
    if-nez v8, :cond_0

    .line 59
    iget-object v9, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->mResponse:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;

    .line 87
    :goto_1
    return-object v9

    .line 50
    .end local v8    # "uri":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 62
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v8    # "uri":[Ljava/lang/String;
    :cond_0
    array-length v6, v8

    .line 63
    .local v6, "size":I
    const/4 v0, 0x0

    .line 64
    .local v0, "count":I
    :goto_2
    if-ge v0, v6, :cond_1

    .line 66
    :try_start_1
    aget-object v9, v8, v0

    const/16 v10, 0x50

    invoke-direct {p0, v9, v10, v2}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->communicateToServer(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 68
    invoke-direct {p0, v5}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->postFindAddress(Ljava/lang/String;)Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 84
    :cond_1
    :goto_3
    if-ne v0, v6, :cond_2

    .line 87
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->mResponse:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;

    goto :goto_1

    .line 70
    :catch_1
    move-exception v7

    .line 71
    .local v7, "ue":Ljava/net/UnknownHostException;
    sget-object v9, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->TAG:Ljava/lang/String;

    const-string v10, "tryFindAddress: UnknownHostException"

    invoke-static {v9, v10, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 72
    invoke-virtual {v7}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 73
    add-int/lit8 v0, v0, 0x1

    .line 81
    goto :goto_2

    .line 74
    .end local v7    # "ue":Ljava/net/UnknownHostException;
    :catch_2
    move-exception v3

    .line 75
    .local v3, "ie":Ljava/io/IOException;
    sget-object v9, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->TAG:Ljava/lang/String;

    const-string v10, "tryFindAddress: IOException"

    invoke-static {v9, v10, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 76
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 77
    add-int/lit8 v0, v0, 0x1

    .line 81
    goto :goto_2

    .line 78
    .end local v3    # "ie":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 79
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method
