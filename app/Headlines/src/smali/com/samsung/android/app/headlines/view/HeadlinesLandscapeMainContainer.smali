.class public Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;
.super Landroid/widget/FrameLayout;
.source "HeadlinesLandscapeMainContainer.java"

# interfaces
.implements Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;
.implements Lcom/samsung/android/app/headlines/NewCardNotification;
.implements Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;
.implements Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshModeChangeListener;
.implements Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$7;,
        Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$FlickGestureListener;,
        Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;
    }
.end annotation


# static fields
.field private static final AUTO_REFRESH_INTERVAL:I = 0x493e0

.field private static final AUTO_REFRESH_INTERVAL_NOT_SET:I = -0x1

.field private static DISPLAYABLE_MINIMUM_CARD_COUNT:I = 0x0

.field private static final SLOPE_FLICK_SCROLL_THRESHOLD:F = 0.8f

.field private static final TAG:Ljava/lang/String; = "HeadlinesLandscapeMainContainer"


# instance fields
.field private MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

.field private final SELP_STOP_TIMEOUT:I

.field private TOUCH_DELTA_X_THRESHOLD:I

.field private TOUCH_DELTA_Y_THRESHOLD:I

.field private mActionBarHeight:I

.field private mAddedCategoryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mContentAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

.field private mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

.field private mContext:Landroid/content/Context;

.field private mGestureDetecter:Landroid/view/GestureDetector;

.field private mHeaderAnimator:Landroid/animation/ObjectAnimator;

.field private mHeaderInitialTopPos:F

.field private mHeaderInitialY:F

.field private mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

.field private mInDragAndDragMode:Z

.field private mInitialX:F

.field private mInitialY:F

.field private mIsAccessibilityEnabled:Z

.field private mIsOutThresHoldRange:Z

.field private mIsPullToRefreshEnabled:Z

.field private mIsPullToRefreshMode:Z

.field private mIsStatusBarShown:Z

.field private mIsTouchIntercepted:Z

.field private mListViewInitialTopPos:F

.field private mListViewInitialY:F

.field private mNoticationButton:Landroid/widget/Button;

.field private mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

.field private mThresHoldHeight:F

.field private mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x3

    sput v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->DISPLAYABLE_MINIMUM_CARD_COUNT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v0, 0x32

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 95
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 46
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    .line 48
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    .line 49
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    .line 50
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    .line 51
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    .line 53
    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->TOUCH_DELTA_X_THRESHOLD:I

    .line 54
    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->TOUCH_DELTA_Y_THRESHOLD:I

    .line 57
    const/16 v0, 0x12c

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    .line 59
    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInDragAndDragMode:Z

    .line 60
    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsOutThresHoldRange:Z

    .line 61
    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    .line 62
    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshMode:Z

    .line 64
    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsTouchIntercepted:Z

    .line 65
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInitialX:F

    .line 66
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInitialY:F

    .line 67
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mThresHoldHeight:F

    .line 69
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    .line 70
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mGestureDetecter:Landroid/view/GestureDetector;

    .line 71
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_INITIAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    .line 74
    const/16 v0, 0x1388

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->SELP_STOP_TIMEOUT:I

    .line 78
    iput v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    .line 80
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialY:F

    .line 81
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialY:F

    .line 82
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialTopPos:F

    .line 83
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialTopPos:F

    .line 84
    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsAccessibilityEnabled:Z

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsStatusBarShown:Z

    .line 87
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mAddedCategoryList:Ljava/util/List;

    .line 96
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    .line 98
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->init()V

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v0, 0x32

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 102
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    .line 48
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    .line 49
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    .line 50
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    .line 51
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    .line 53
    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->TOUCH_DELTA_X_THRESHOLD:I

    .line 54
    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->TOUCH_DELTA_Y_THRESHOLD:I

    .line 57
    const/16 v0, 0x12c

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    .line 59
    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInDragAndDragMode:Z

    .line 60
    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsOutThresHoldRange:Z

    .line 61
    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    .line 62
    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshMode:Z

    .line 64
    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsTouchIntercepted:Z

    .line 65
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInitialX:F

    .line 66
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInitialY:F

    .line 67
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mThresHoldHeight:F

    .line 69
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    .line 70
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mGestureDetecter:Landroid/view/GestureDetector;

    .line 71
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_INITIAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    .line 74
    const/16 v0, 0x1388

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->SELP_STOP_TIMEOUT:I

    .line 78
    iput v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    .line 80
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialY:F

    .line 81
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialY:F

    .line 82
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialTopPos:F

    .line 83
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialTopPos:F

    .line 84
    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsAccessibilityEnabled:Z

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsStatusBarShown:Z

    .line 87
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mAddedCategoryList:Ljava/util/List;

    .line 103
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    .line 105
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->init()V

    .line 106
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mAddedCategoryList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    return-object v0
.end method

.method private animateHeader(I)V
    .locals 5
    .param p1, "toY"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 698
    if-nez p1, :cond_0

    .line 699
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->showStatusBar(Z)V

    .line 704
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    const-string v1, "translationY"

    new-array v2, v2, [F

    int-to-float v3, p1

    aput v3, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    .line 705
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 706
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 707
    return-void

    .line 701
    :cond_0
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->showStatusBar(Z)V

    goto :goto_0
.end method

.method private init()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 109
    const-string v2, "HeadlinesLandscapeMainContainer"

    const-string v3, "init()"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 111
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030006

    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 113
    new-instance v2, Landroid/view/GestureDetector;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    new-instance v4, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$FlickGestureListener;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$FlickGestureListener;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$1;)V

    invoke-direct {v2, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mGestureDetecter:Landroid/view/GestureDetector;

    .line 114
    const v2, 0x7f0c001c

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    .line 115
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    invoke-virtual {v2, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->setPullToRefreshModeChangeListener(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshModeChangeListener;)V

    .line 117
    const v2, 0x7f0c001f

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    .line 118
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-eqz v2, :cond_0

    .line 119
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->measure(II)V

    .line 120
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v2, v6, v6, v3, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->layout(IIII)V

    .line 121
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->setY(F)V

    .line 123
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    .line 126
    :cond_0
    const v2, 0x7f0c0020

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    .line 127
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    if-eqz v2, :cond_1

    .line 128
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/Button;->measure(II)V

    .line 129
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v2, v6, v6, v3, v4}, Landroid/widget/Button;->layout(IIII)V

    .line 131
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    new-instance v3, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$1;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mAddedCategoryList:Ljava/util/List;

    .line 143
    const v2, 0x7f0c001d

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    .line 144
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    if-eqz v2, :cond_2

    .line 145
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    new-instance v3, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setOnScrollListener(Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;)V

    .line 180
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    iget v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setY(F)V

    .line 181
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getTranslationY()F

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialTopPos:F

    .line 182
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v5}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getPaddingRight()I

    move-result v5

    iget v6, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialTopPos:F

    float-to-int v6, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setPadding(IIII)V

    .line 185
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    const-string v3, "accessibility"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 186
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    if-eqz v0, :cond_3

    .line 187
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->isAccessibilityEnabled(Z)V

    .line 188
    invoke-virtual {v0, p0}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    .line 191
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08003c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    .line 192
    const-string v2, "HeadlinesLandscapeMainContainer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init() : MIN_LENGTH_X_FOR_LAUNCHING_HOME = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->TOUCH_DELTA_Y_THRESHOLD:I

    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->TOUCH_DELTA_X_THRESHOLD:I

    .line 195
    return-void
.end method

.method private stopHeaderAnimation()V
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 695
    :cond_0
    return-void
.end method


# virtual methods
.method public animateHeader(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 724
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getTranslationY()F

    move-result v0

    .line 725
    .local v0, "HeaderTop":F
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    .line 735
    :cond_0
    :goto_0
    return-void

    .line 730
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 731
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->animateHeader(I)V

    goto :goto_0

    .line 733
    :cond_2
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    neg-int v1, v1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->animateHeader(I)V

    goto :goto_0
.end method

.method public dismissMoreMenu()V
    .locals 2

    .prologue
    .line 625
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-nez v0, :cond_0

    .line 626
    const-string v0, "HeadlinesLandscapeMainContainer"

    const-string v1, "dismissMoreMenu(): HeaderView is null. Return."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    :goto_0
    return-void

    .line 630
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->dismissMoreMenu()V

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 30
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 213
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v20

    .line 215
    .local v20, "action":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v28

    .line 216
    .local v28, "x":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v29

    .line 218
    .local v29, "y":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInitialX:F

    sub-float v22, v28, v2

    .line 219
    .local v22, "diffX":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInitialY:F

    sub-float v23, v29, v2

    .line 226
    .local v23, "diffY":F
    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->abs(F)F

    move-result v18

    .line 227
    .local v18, "absDiffX":F
    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    move-result v19

    .line 229
    .local v19, "absDiffY":F
    packed-switch v20, :pswitch_data_0

    .line 424
    :cond_0
    :goto_0
    invoke-super/range {p0 .. p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v27

    :goto_1
    return v27

    .line 231
    :pswitch_0
    const-string v2, "HeadlinesLandscapeMainContainer"

    const-string v3, "dispatchTouchEvent() : ACTION_DOWN"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    sget-object v2, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_INITIAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    .line 233
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsTouchIntercepted:Z

    .line 234
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsOutThresHoldRange:Z

    .line 236
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInitialX:F

    .line 237
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInitialY:F

    .line 238
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInitialY:F

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialY:F

    .line 239
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInitialY:F

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialY:F

    .line 241
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->stopHeaderAnimation()V

    .line 243
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialTopPos:F

    .line 244
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialTopPos:F

    .line 245
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mGestureDetecter:Landroid/view/GestureDetector;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 247
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshMode:Z

    .line 248
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->getRefreshState()Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    if-eq v2, v3, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->getRefreshState()Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_INIT:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    if-eq v2, v3, :cond_3

    .line 250
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshMode:Z

    .line 252
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->isRefreshing()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 253
    const-string v2, "HeadlinesLandscapeMainContainer"

    const-string v3, "dispatchTouchEvent() : [ACTION_DOWN] List Header View is Refresing."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    goto/16 :goto_0

    .line 243
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getTranslationY()F

    move-result v2

    goto :goto_2

    .line 244
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getTranslationY()F

    move-result v2

    goto :goto_3

    .line 259
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    if-eqz v2, :cond_4

    .line 260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getChildAt(I)Landroid/view/View;

    move-result-object v24

    .line 261
    .local v24, "firstChildView":Landroid/view/View;
    if-eqz v24, :cond_4

    .line 262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getTranslationY()F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_5

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getTop()I

    move-result v2

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getFirstVisiblePosition()I

    move-result v2

    if-nez v2, :cond_5

    .line 263
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    .line 270
    .end local v24    # "firstChildView":Landroid/view/View;
    :cond_4
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInDragAndDragMode:Z

    if-nez v2, :cond_0

    .line 271
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->touchDelegate(Landroid/view/MotionEvent;)Z

    goto/16 :goto_0

    .line 265
    .restart local v24    # "firstChildView":Landroid/view/View;
    :cond_5
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    goto :goto_4

    .line 277
    .end local v24    # "firstChildView":Landroid/view/View;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 280
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsOutThresHoldRange:Z

    const/4 v3, 0x1

    if-eq v2, v3, :cond_6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->TOUCH_DELTA_X_THRESHOLD:I

    int-to-float v2, v2

    cmpl-float v2, v18, v2

    if-gtz v2, :cond_6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->TOUCH_DELTA_Y_THRESHOLD:I

    int-to-float v2, v2

    cmpl-float v2, v19, v2

    if-lez v2, :cond_13

    .line 281
    :cond_6
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsOutThresHoldRange:Z

    .line 283
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    sget-object v3, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_INITIAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    if-ne v2, v3, :cond_a

    .line 284
    const/4 v2, 0x0

    cmpl-float v2, v18, v2

    if-eqz v2, :cond_7

    div-float v2, v19, v18

    const v3, 0x3f4ccccd    # 0.8f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_9

    .line 285
    :cond_7
    const-string v2, "HeadlinesLandscapeMainContainer"

    const-string v3, "dispatchTouchEvent() : ACTION_MOVE - IN THRESHOLD RANGE - Mode Change : STATE_SCROLL_VERTICAL"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    sget-object v2, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_SCROLL_VERTICAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    .line 287
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInitialY:F

    sub-float v2, v29, v2

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_8

    .line 288
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->TOUCH_DELTA_Y_THRESHOLD:I

    mul-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mThresHoldHeight:F

    .line 292
    :goto_5
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsTouchIntercepted:Z

    .line 293
    const/16 v27, 0x1

    goto/16 :goto_1

    .line 290
    :cond_8
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->TOUCH_DELTA_Y_THRESHOLD:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mThresHoldHeight:F

    goto :goto_5

    .line 297
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInDragAndDragMode:Z

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 298
    const-string v2, "HeadlinesLandscapeMainContainer"

    const-string v3, "dispatchTouchEvent() : ACTION_MOVE - IN THRESHOLD RANGE - Mode Change : STATE_FLICK_HORIZONTAL"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    sget-object v2, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_FLICK_HORIZONTAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    .line 300
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsTouchIntercepted:Z

    .line 301
    const/16 v27, 0x1

    goto/16 :goto_1

    .line 305
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    sget-object v3, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_SCROLL_VERTICAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    if-ne v2, v3, :cond_11

    .line 306
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->isRefreshing()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_b

    .line 307
    const-string v2, "HeadlinesLandscapeMainContainer"

    const-string v3, "dispatchTouchEvent() : [ACTION_MOVE] List Header View is Refresing."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    .line 309
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsTouchIntercepted:Z

    .line 310
    const/16 v27, 0x1

    goto/16 :goto_1

    .line 313
    :cond_b
    const/16 v27, 0x0

    .line 315
    .local v27, "ret":Z
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshMode:Z

    const/4 v3, 0x1

    if-eq v2, v3, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInDragAndDragMode:Z

    const/4 v3, 0x1

    if-eq v2, v3, :cond_e

    .line 317
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setDndMode(Z)V

    .line 318
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mThresHoldHeight:F

    sub-float v2, v29, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->updateHeaderPosition(F)V

    .line 320
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getChildAt(I)Landroid/view/View;

    move-result-object v25

    .line 321
    .local v25, "firstChildView2":Landroid/view/View;
    if-eqz v25, :cond_c

    .line 322
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getTranslationY()F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_d

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getTop()I

    move-result v2

    if-nez v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getFirstVisiblePosition()I

    move-result v2

    if-nez v2, :cond_d

    .line 331
    .end local v25    # "firstChildView2":Landroid/view/View;
    :cond_c
    :goto_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    new-array v8, v2, [Landroid/view/MotionEvent$PointerProperties;

    .line 332
    .local v8, "outPointerProperties":[Landroid/view/MotionEvent$PointerProperties;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    new-array v9, v2, [Landroid/view/MotionEvent$PointerCoords;

    .line 334
    .local v9, "outPointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    const/16 v26, 0x0

    .local v26, "i":I
    :goto_7
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    move/from16 v0, v26

    if-ge v0, v2, :cond_f

    .line 335
    new-instance v2, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct {v2}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    aput-object v2, v8, v26

    .line 336
    new-instance v2, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v2}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v2, v9, v26

    .line 337
    aget-object v2, v8, v26

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->getPointerProperties(ILandroid/view/MotionEvent$PointerProperties;)V

    .line 338
    aget-object v2, v9, v26

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    .line 340
    aget-object v2, v9, v26

    aget-object v3, v9, v26

    iget v3, v3, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mThresHoldHeight:F

    sub-float/2addr v3, v4

    iput v3, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 334
    add-int/lit8 v26, v26, 0x1

    goto :goto_7

    .line 324
    .end local v8    # "outPointerProperties":[Landroid/view/MotionEvent$PointerProperties;
    .end local v9    # "outPointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    .end local v26    # "i":I
    .restart local v25    # "firstChildView2":Landroid/view/View;
    :cond_d
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    goto :goto_6

    .line 328
    .end local v25    # "firstChildView2":Landroid/view/View;
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setDndMode(Z)V

    goto :goto_6

    .line 343
    .restart local v8    # "outPointerProperties":[Landroid/view/MotionEvent$PointerProperties;
    .restart local v9    # "outPointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    .restart local v26    # "i":I
    :cond_f
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v10

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v12

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v13

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v15

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v16

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getFlags()I

    move-result v17

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v21

    .line 345
    .local v21, "clonedEvent":Landroid/view/MotionEvent;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 351
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_10

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInDragAndDragMode:Z

    const/4 v3, 0x1

    if-eq v2, v3, :cond_10

    .line 352
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshMode:Z

    .line 353
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setDndMode(Z)V

    .line 354
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setOverScrollMode(I)V

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->touchDelegate(Landroid/view/MotionEvent;)Z

    move-result v27

    .line 357
    const/4 v2, 0x1

    move/from16 v0, v27

    if-ne v0, v2, :cond_10

    .line 358
    invoke-virtual/range {v21 .. v21}, Landroid/view/MotionEvent;->recycle()V

    .line 359
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsTouchIntercepted:Z

    .line 360
    const/16 v27, 0x1

    goto/16 :goto_1

    .line 364
    :cond_10
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-super {v0, v1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v27

    .line 365
    invoke-virtual/range {v21 .. v21}, Landroid/view/MotionEvent;->recycle()V

    goto/16 :goto_1

    .line 368
    .end local v8    # "outPointerProperties":[Landroid/view/MotionEvent$PointerProperties;
    .end local v9    # "outPointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    .end local v21    # "clonedEvent":Landroid/view/MotionEvent;
    .end local v26    # "i":I
    .end local v27    # "ret":Z
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    sget-object v3, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_FLICK_HORIZONTAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    if-ne v2, v3, :cond_0

    .line 369
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mGestureDetecter:Landroid/view/GestureDetector;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 371
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->TOUCH_DELTA_X_THRESHOLD:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    cmpg-float v2, v18, v2

    if-gez v2, :cond_12

    .line 372
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsTouchIntercepted:Z

    .line 376
    :goto_8
    const/16 v27, 0x1

    goto/16 :goto_1

    .line 374
    :cond_12
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsTouchIntercepted:Z

    goto :goto_8

    .line 380
    :cond_13
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsTouchIntercepted:Z

    .line 381
    const-string v2, "HeadlinesLandscapeMainContainer"

    const-string v3, "[ACTION_MOVE] NOTNOTNOT IN THRESHOLD RANGE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 387
    :pswitch_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsOutThresHoldRange:Z

    .line 388
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshMode:Z

    .line 389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setDndMode(Z)V

    .line 390
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setOverScrollMode(I)V

    .line 392
    sget-object v2, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_INITIAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    .line 394
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsAccessibilityEnabled:Z

    if-nez v2, :cond_14

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    if-nez v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-eqz v2, :cond_14

    .line 395
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->updateHeader()V

    .line 397
    :cond_14
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    goto/16 :goto_0

    .line 401
    :pswitch_3
    const-string v2, "HeadlinesLandscapeMainContainer"

    const-string v3, "dispatchTouchEvenet() : [ACTION_UP]"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsOutThresHoldRange:Z

    .line 403
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshMode:Z

    .line 404
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setDndMode(Z)V

    .line 405
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setOverScrollMode(I)V

    .line 407
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    sget-object v3, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_SCROLL_VERTICAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    if-ne v2, v3, :cond_17

    .line 412
    :cond_15
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsAccessibilityEnabled:Z

    if-nez v2, :cond_16

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    if-nez v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-eqz v2, :cond_16

    .line 413
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->updateHeader()V

    .line 416
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInDragAndDragMode:Z

    if-nez v2, :cond_0

    .line 417
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->touchDelegate(Landroid/view/MotionEvent;)Z

    .line 418
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsPullToRefreshEnabled:Z

    goto/16 :goto_0

    .line 408
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    sget-object v3, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_FLICK_HORIZONTAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    if-ne v2, v3, :cond_15

    .line 409
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mGestureDetecter:Landroid/view/GestureDetector;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_9

    .line 229
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .locals 1

    .prologue
    .line 854
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    return-object v0
.end method

.method public getCardList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/Card;",
            ">;"
        }
    .end annotation

    .prologue
    .line 746
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->getCardList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getStatusBarHeight()I
    .locals 6

    .prologue
    .line 762
    const/4 v1, 0x0

    .line 763
    .local v1, "result":I
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "status_bar_height"

    const-string v4, "dimen"

    const-string v5, "android"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 764
    .local v0, "resourceId":I
    if-lez v0, :cond_0

    .line 765
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 767
    :cond_0
    return v1
.end method

.method public hideNotificationButton()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 782
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 783
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 784
    .local v0, "animation":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 785
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 786
    new-instance v1, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$6;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 801
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->clearAnimation()V

    .line 802
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->startAnimation(Landroid/view/animation/Animation;)V

    .line 805
    .end local v0    # "animation":Landroid/view/animation/TranslateAnimation;
    :cond_0
    return-void
.end method

.method public isAccessibilityEnabled(Z)V
    .locals 1
    .param p1, "isEnabled"    # Z

    .prologue
    .line 755
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsAccessibilityEnabled:Z

    .line 756
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsAccessibilityEnabled:Z

    if-eqz v0, :cond_0

    .line 757
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->updateHeader()V

    .line 759
    :cond_0
    return-void
.end method

.method public isPopupShown()Z
    .locals 2

    .prologue
    .line 634
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-nez v0, :cond_0

    .line 635
    const-string v0, "HeadlinesLandscapeMainContainer"

    const-string v1, "isPopupShown(): HeaderView is null. Return False."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    const/4 v0, 0x0

    .line 639
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->isPopupShown()Z

    move-result v0

    goto :goto_0
.end method

.method public onAccessibilityStateChanged(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 751
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->isAccessibilityEnabled(Z)V

    .line 752
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 199
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    if-eqz v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mPullToRefreshView:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->setPullToRefreshModeChangeListener(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshModeChangeListener;)V

    .line 202
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    const-string v2, "accessibility"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 203
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    if-eqz v0, :cond_1

    .line 204
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->isAccessibilityEnabled(Z)V

    .line 205
    invoke-virtual {v0, p0}, Landroid/view/accessibility/AccessibilityManager;->removeAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    .line 208
    :cond_1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 209
    return-void
.end method

.method public onDragAndDropStart()V
    .locals 1

    .prologue
    .line 480
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInDragAndDragMode:Z

    .line 482
    return-void
.end method

.method public onDragAndDropStop()V
    .locals 1

    .prologue
    .line 486
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mInDragAndDragMode:Z

    .line 488
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 429
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 430
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 451
    :cond_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    :goto_0
    return v1

    .line 442
    :pswitch_1
    iget-boolean v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsTouchIntercepted:Z

    if-ne v2, v1, :cond_0

    .line 443
    const-string v2, "HeadlinesLandscapeMainContainer"

    const-string v3, "onInterceptTouchEvent() : invoke an ACTION_CANCEL event to children "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsTouchIntercepted:Z

    goto :goto_0

    .line 430
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onNewNotificationCardAdded(I)V
    .locals 5
    .param p1, "cardId"    # I

    .prologue
    const/4 v4, 0x0

    .line 809
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    const-string v3, "mymagazine"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 810
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "ENABLE_NEW_STORY_NOTIFICATION"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 811
    .local v0, "enable":Z
    if-eqz v0, :cond_0

    .line 812
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getCount()I

    move-result v2

    sget v3, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->DISPLAYABLE_MINIMUM_CARD_COUNT:I

    if-le v2, v3, :cond_0

    .line 813
    const-string v2, "HeadlinesLandscapeMainContainer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNewNotificationCardAdded() : Add from addedCategoryList. Cardid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 815
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->showNotificationButton()V

    .line 818
    :cond_0
    return-void
.end method

.method public onNewNotificationCardRemoved(I)V
    .locals 5
    .param p1, "cardId"    # I

    .prologue
    const/4 v4, 0x0

    .line 822
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    const-string v3, "mymagazine"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 823
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "ENABLE_NEW_STORY_NOTIFICATION"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 824
    .local v0, "enable":Z
    if-eqz v0, :cond_0

    .line 825
    const-string v2, "HeadlinesLandscapeMainContainer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNewNotificationCardRemoved() : Remove from addedCategoryList. Cardid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 827
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->hideNotificationButton()V

    .line 829
    :cond_0
    return-void
.end method

.method public onPullToRefreshModeChanged(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;)V
    .locals 3
    .param p1, "refreshMode"    # Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    .prologue
    const/4 v2, 0x0

    .line 493
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$7;->$SwitchMap$com$samsung$android$app$headlines$view$HeadlinesPullToRefreshView$PullToRefreshState:[I

    invoke-virtual {p1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 512
    :goto_0
    return-void

    .line 495
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setTranscriptMode(I)V

    goto :goto_0

    .line 501
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setTranscriptMode(I)V

    goto :goto_0

    .line 506
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->refreshByPullToRefresh()V

    goto :goto_0

    .line 493
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public openHelp()V
    .locals 1

    .prologue
    .line 742
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->openHelp()V

    .line 743
    return-void
.end method

.method public openSetting()V
    .locals 1

    .prologue
    .line 738
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->openSetting()V

    .line 739
    return-void
.end method

.method public refreshByPullToRefresh()V
    .locals 8

    .prologue
    .line 515
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->setRefreshState(Z)V

    .line 517
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getCards()Ljava/util/List;

    move-result-object v2

    .line 518
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 519
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 520
    .local v0, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->requestRefresh(I)V

    .line 521
    const-string v5, "HeadlinesLandscapeMainContainer"

    const-string v6, "refreshByPullToRefresh() : Refresh all start."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 524
    .end local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :cond_0
    const/4 v1, 0x0

    :goto_1
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v5}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 526
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v5, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 527
    .local v4, "view":Landroid/view/View;
    if-eqz v4, :cond_1

    instance-of v5, v4, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    if-eqz v5, :cond_1

    .line 528
    check-cast v4, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    .end local v4    # "view":Landroid/view/View;
    invoke-virtual {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->showLoadingViews()V

    .line 524
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 532
    :cond_2
    new-instance v3, Ljava/util/Timer;

    invoke-direct {v3}, Ljava/util/Timer;-><init>()V

    .line 533
    .local v3, "timer":Ljava/util/Timer;
    new-instance v5, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$3;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$3;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)V

    const-wide/16 v6, 0x1388

    invoke-virtual {v3, v5, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 539
    return-void
.end method

.method public removeAllNewCategoryList()V
    .locals 1

    .prologue
    .line 832
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 833
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->hideNotificationButton()V

    .line 834
    return-void
.end method

.method public requestAllRefresh()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 542
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    const-string v4, "mymagazine"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 544
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v3, "AUTO_REFRESH_SET_FIRST_TIME"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 545
    .local v0, "auto_refresh_set_first_time":Z
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 546
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->requestAllRefresh(I)V

    .line 547
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 548
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "AUTO_REFRESH_SET_FIRST_TIME"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 549
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 553
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :goto_0
    return-void

    .line 551
    :cond_0
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->requestAllRefresh(I)V

    goto :goto_0
.end method

.method public requestAllRefresh(I)V
    .locals 14
    .param p1, "gap"    # I

    .prologue
    .line 556
    iget-object v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    const-string v11, "mymagazine"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 558
    .local v6, "pref":Landroid/content/SharedPreferences;
    const-string v10, "AUTO_REFRESH_ON"

    const/4 v11, 0x0

    invoke-interface {v6, v10, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_3

    .line 559
    const-string v10, "HeadlinesLandscapeMainContainer"

    const-string v11, "RequestAllRefresh() : Refresh all start."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    const-string v10, "PAUSE_TIME"

    const-wide/16 v12, 0x0

    invoke-interface {v6, v10, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 561
    .local v8, "time":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 562
    .local v2, "current":J
    const v0, 0x493e0

    .line 564
    .local v0, "_gap":I
    const/4 v10, -0x1

    if-eq p1, v10, :cond_0

    .line 565
    move v0, p1

    .line 567
    :cond_0
    sub-long v10, v2, v8

    int-to-long v12, v0

    cmp-long v10, v10, v12

    if-lez v10, :cond_2

    .line 568
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->setRefreshState(Z)V

    .line 569
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getCards()Ljava/util/List;

    move-result-object v5

    .line 570
    .local v5, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    if-ge v4, v10, :cond_1

    .line 571
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 572
    .local v1, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v10

    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->requestRefresh(I)V

    .line 573
    const-string v10, "HeadlinesLandscapeMainContainer"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Refresh :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 577
    .end local v1    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    if-eqz v10, :cond_2

    .line 578
    new-instance v7, Ljava/util/Timer;

    invoke-direct {v7}, Ljava/util/Timer;-><init>()V

    .line 579
    .local v7, "timer":Ljava/util/Timer;
    new-instance v10, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4;

    invoke-direct {v10, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)V

    const-wide/16 v12, 0x1f4

    invoke-virtual {v7, v10, v12, v13}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 601
    .end local v4    # "i":I
    .end local v5    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    .end local v7    # "timer":Ljava/util/Timer;
    :cond_2
    const-string v10, "HeadlinesLandscapeMainContainer"

    const-string v11, "RequestAllRefresh() : Refresh all End."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    new-instance v7, Ljava/util/Timer;

    invoke-direct {v7}, Ljava/util/Timer;-><init>()V

    .line 604
    .restart local v7    # "timer":Ljava/util/Timer;
    new-instance v10, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$5;

    invoke-direct {v10, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$5;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)V

    const-wide/16 v12, 0x1388

    invoke-virtual {v7, v10, v12, v13}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 613
    .end local v0    # "_gap":I
    .end local v2    # "current":J
    .end local v7    # "timer":Ljava/util/Timer;
    .end local v8    # "time":J
    :goto_1
    return-void

    .line 611
    :cond_3
    const-string v10, "HeadlinesLandscapeMainContainer"

    const-string v11, "RequestAllRefresh() : Refresh is not activated."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setAdapter(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    .prologue
    .line 838
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    .line 840
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    if-eqz v0, :cond_0

    .line 841
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->setOnNewCardNotification(Lcom/samsung/android/app/headlines/NewCardNotification;)V

    .line 843
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    if-eqz v0, :cond_0

    .line 844
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 845
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setDndController(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;)V

    .line 846
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setDndListener(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndListener;)V

    .line 847
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setDndMode(Z)V

    .line 850
    :cond_0
    return-void
.end method

.method public setNewCategoryNotificationEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 859
    return-void
.end method

.method public showMoreMenu()V
    .locals 2

    .prologue
    .line 616
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    if-nez v0, :cond_0

    .line 617
    const-string v0, "HeadlinesLandscapeMainContainer"

    const-string v1, "showMoreMenu(): HeaderView is null. Return."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    :goto_0
    return-void

    .line 621
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->showMoreMenu()V

    goto :goto_0
.end method

.method public showNotificationButton()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 771
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mAddedCategoryList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 772
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 773
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->clearAnimation()V

    .line 774
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v3, v3, v1, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 775
    .local v0, "animation":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 776
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 777
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mNoticationButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->startAnimation(Landroid/view/animation/Animation;)V

    .line 779
    .end local v0    # "animation":Landroid/view/animation/TranslateAnimation;
    :cond_0
    return-void
.end method

.method showStatusBar(Z)V
    .locals 2
    .param p1, "set"    # Z

    .prologue
    const/16 v1, 0x400

    .line 710
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsStatusBarShown:Z

    if-ne v0, p1, :cond_0

    .line 720
    :goto_0
    return-void

    .line 712
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 713
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 719
    :goto_1
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsStatusBarShown:Z

    goto :goto_0

    .line 716
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto :goto_1
.end method

.method public updateHeader()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 682
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getTranslationY()F

    move-result v0

    .line 683
    .local v0, "HeaderTop":F
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-lez v1, :cond_0

    cmpl-float v1, v0, v2

    if-ltz v1, :cond_1

    .line 689
    :cond_0
    :goto_0
    return-void

    .line 688
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getTranslationY()F

    move-result v1

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mIsAccessibilityEnabled:Z

    if-eqz v1, :cond_3

    :cond_2
    const/4 v1, 0x0

    :goto_1
    invoke-direct {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->animateHeader(I)V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    neg-int v1, v1

    goto :goto_1
.end method

.method public updateHeaderPosition(F)V
    .locals 7
    .param p1, "newY"    # F

    .prologue
    const/4 v4, 0x0

    .line 644
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialTopPos:F

    iget v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialY:F

    sub-float v3, p1, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v0, v2

    .line 645
    .local v0, "newHeaderTopPos":F
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialTopPos:F

    iget v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialY:F

    sub-float v3, p1, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v1, v2

    .line 647
    .local v1, "newListViewTopPos":F
    cmpl-float v2, v0, v4

    if-ltz v2, :cond_2

    .line 648
    iput v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialTopPos:F

    .line 649
    iput p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialY:F

    .line 650
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialTopPos:F

    .line 657
    :cond_0
    :goto_0
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_3

    .line 658
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    int-to-float v2, v2

    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialTopPos:F

    .line 659
    iput p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialY:F

    .line 660
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialTopPos:F

    .line 667
    :cond_1
    :goto_1
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    int-to-float v2, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sub-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->getStatusBarHeight()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_4

    .line 668
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->showStatusBar(Z)V

    .line 673
    :goto_2
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->stopHeaderAnimation()V

    .line 675
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->setTranslationY(F)V

    .line 677
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setTranslationY(F)V

    .line 678
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    invoke-virtual {v5}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getPaddingRight()I

    move-result v5

    float-to-int v6, v1

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setPadding(IIII)V

    .line 679
    return-void

    .line 651
    :cond_2
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    neg-int v2, v2

    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    .line 652
    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mActionBarHeight:I

    neg-int v2, v2

    int-to-float v2, v2

    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialTopPos:F

    .line 653
    iput p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialY:F

    .line 654
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderInitialTopPos:F

    goto :goto_0

    .line 661
    :cond_3
    cmpg-float v2, v1, v4

    if-gez v2, :cond_1

    .line 662
    iput v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialTopPos:F

    .line 663
    iput p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialY:F

    .line 664
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mListViewInitialTopPos:F

    goto :goto_1

    .line 670
    :cond_4
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->showStatusBar(Z)V

    goto :goto_2
.end method
