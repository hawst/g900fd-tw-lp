.class public Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;
.super Ljava/lang/Thread;
.source "HeadlinesTermsDownloader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;
    }
.end annotation


# static fields
.field private static final MAGAZINE_PRIVACY_POLICY:Ljava/lang/String; = "MagazinePrivacyNotice.txt"

.field private static final MAGAZINE_TERMS_AND_CONDITIONS:Ljava/lang/String; = "MagazineTermsAndConditions.txt"

.field private static final TAG:Ljava/lang/String; = "HeadlinesTermsDownloader"

.field private static final TERMS_HOST_ADDRESS:Ljava/lang/String; = "http://static.bada.com/contents/legal/"

.field public static final TYPE_MAGAZINE_PRIVACY_POLICY:Ljava/lang/String; = "PrivacyPolicy"

.field public static final TYPE_MAGAZINE_TERMS_AND_CONDITIONS:Ljava/lang/String; = "TermsAndConditions"

.field public static final TYPE_NO_RESPONSE:Ljava/lang/String; = "NoResponse"

.field public static final TYPE_URL_COUNTRY_LANGUAGE:I = 0x0

.field public static final TYPE_URL_GLOBAL_ENG:I = 0x2

.field public static final TYPE_URL_GLOBAL_LANGUAGE:I = 0x1

.field private static final VALUE_TIMEOUT:J = 0x15f90L


# instance fields
.field private mAvailable:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field mCountryMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDownloadEventHandler:Landroid/os/Handler;

.field private mMcc:Ljava/lang/String;

.field private mTimeOutTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 43
    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mContext:Landroid/content/Context;

    .line 44
    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mDownloadEventHandler:Landroid/os/Handler;

    .line 46
    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mMcc:Ljava/lang/String;

    .line 47
    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mAvailable:Ljava/lang/String;

    .line 48
    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mTimeOutTimer:Ljava/util/Timer;

    .line 50
    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    .line 53
    iput-object p1, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mContext:Landroid/content/Context;

    .line 55
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->createmCountryMap()V

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mDownloadEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mTimeOutTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->createURL(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private checkValidChinaCode(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "countryCode"    # Ljava/lang/String;
    .param p2, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 311
    const-string v0, "cn"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "zho"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    const/4 v0, 0x1

    .line 314
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createURL(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p1, "messageToReturn"    # Ljava/lang/String;
    .param p2, "defaultType"    # I

    .prologue
    const/4 v4, 0x1

    .line 250
    const/4 v2, 0x0

    .line 251
    .local v2, "url":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->getLocaleISO3Language(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 253
    .local v1, "languageCode":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mMcc:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mMcc:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v4, :cond_0

    .line 254
    const/4 p2, 0x2

    .line 257
    :cond_0
    if-nez p2, :cond_4

    .line 258
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mMcc:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->getMccToCountryNameAlpha2(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "countryCode":Ljava/lang/String;
    const-string v3, "CN"

    invoke-static {v3}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 261
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->checkValidChinaCode(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 262
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http://static.bada.com/contents/legal/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 288
    .end local v0    # "countryCode":Ljava/lang/String;
    :goto_0
    const-string v3, "TermsAndConditions"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 289
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "MagazineTermsAndConditions.txt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 296
    :cond_1
    :goto_1
    const-string v3, "HeadlinesTermsDownloader"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createURL() : URL = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    return-object v2

    .line 264
    .restart local v0    # "countryCode":Ljava/lang/String;
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http://static.bada.com/contents/legal/global/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 267
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http://static.bada.com/contents/legal/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 270
    .end local v0    # "countryCode":Ljava/lang/String;
    :cond_4
    if-ne p2, v4, :cond_6

    .line 271
    const-string v3, "TW"

    invoke-static {v3}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "zho"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 274
    const-string v2, "http://static.bada.com/contents/legal/global/eng/"

    goto/16 :goto_0

    .line 276
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http://static.bada.com/contents/legal/global/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 280
    :cond_6
    const-string v3, "CN"

    invoke-static {v3}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 282
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http://static.bada.com/contents/legal/global/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 284
    :cond_7
    const-string v2, "http://static.bada.com/contents/legal/global/eng/"

    goto/16 :goto_0

    .line 291
    :cond_8
    const-string v3, "PrivacyPolicy"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 292
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "MagazinePrivacyNotice.txt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1
.end method

.method private createmCountryMap()V
    .locals 3

    .prologue
    .line 330
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    .line 332
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "412"

    const-string v2, "af"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "276"

    const-string v2, "al"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "603"

    const-string v2, "dz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "544"

    const-string v2, "as"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "213"

    const-string v2, "ad"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "631"

    const-string v2, "ao"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "365"

    const-string v2, "ai"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "344"

    const-string v2, "ag"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "722"

    const-string v2, "ar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "283"

    const-string v2, "am"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "363"

    const-string v2, "aw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "505"

    const-string v2, "au"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "232"

    const-string v2, "at"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "400"

    const-string v2, "az"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "364"

    const-string v2, "bs"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "426"

    const-string v2, "bh"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "470"

    const-string v2, "bd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "342"

    const-string v2, "bb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "257"

    const-string v2, "by"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "206"

    const-string v2, "be"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "702"

    const-string v2, "bz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "616"

    const-string v2, "bj"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "350"

    const-string v2, "bm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "402"

    const-string v2, "bt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "736"

    const-string v2, "bo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "218"

    const-string v2, "ba"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "652"

    const-string v2, "bw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "724"

    const-string v2, "br"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "348"

    const-string v2, "vg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "528"

    const-string v2, "bn"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "284"

    const-string v2, "bg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "613"

    const-string v2, "bf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "642"

    const-string v2, "bi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "456"

    const-string v2, "kh"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "624"

    const-string v2, "cm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "302"

    const-string v2, "ca"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "625"

    const-string v2, "cv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "346"

    const-string v2, "ky"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "623"

    const-string v2, "cf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "622"

    const-string v2, "td"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "730"

    const-string v2, "cl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "460"

    const-string v2, "cn"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "461"

    const-string v2, "cn"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "732"

    const-string v2, "co"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "654"

    const-string v2, "km"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "629"

    const-string v2, "cg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "548"

    const-string v2, "ck"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "712"

    const-string v2, "cr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "612"

    const-string v2, "ci"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "219"

    const-string v2, "hr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "368"

    const-string v2, "cu"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "362"

    const-string v2, "cw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "280"

    const-string v2, "cy"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "230"

    const-string v2, "cz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "630"

    const-string v2, "cd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "238"

    const-string v2, "dk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "638"

    const-string v2, "dj"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "366"

    const-string v2, "dm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "370"

    const-string v2, "do"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "514"

    const-string v2, "kh"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "740"

    const-string v2, "ec"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "602"

    const-string v2, "eg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "706"

    const-string v2, "sv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "627"

    const-string v2, "gq"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "657"

    const-string v2, "er"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "248"

    const-string v2, "ee"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "636"

    const-string v2, "et"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "750"

    const-string v2, "fk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "288"

    const-string v2, "fo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "542"

    const-string v2, "fj"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "244"

    const-string v2, "fi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "208"

    const-string v2, "fr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "742"

    const-string v2, "gf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "547"

    const-string v2, "pf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "628"

    const-string v2, "ga"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "607"

    const-string v2, "gm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "282"

    const-string v2, "ge"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "262"

    const-string v2, "de"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "620"

    const-string v2, "gh"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "266"

    const-string v2, "gi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "202"

    const-string v2, "gr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "290"

    const-string v2, "gl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "352"

    const-string v2, "gd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "340"

    const-string v2, "gp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "535"

    const-string v2, "gu"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "704"

    const-string v2, "gt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "611"

    const-string v2, "gn"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "632"

    const-string v2, "gw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "738"

    const-string v2, "gy"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "372"

    const-string v2, "ht"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "708"

    const-string v2, "hn"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "454"

    const-string v2, "hk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "216"

    const-string v2, "hu"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "274"

    const-string v2, "is"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "404"

    const-string v2, "in"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "405"

    const-string v2, "in"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "510"

    const-string v2, "id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "432"

    const-string v2, "ir"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "418"

    const-string v2, "iq"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "272"

    const-string v2, "ie"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "425"

    const-string v2, "il"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "222"

    const-string v2, "it"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "338"

    const-string v2, "jm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "441"

    const-string v2, "jp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "440"

    const-string v2, "jp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "416"

    const-string v2, "jo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "401"

    const-string v2, "kz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "639"

    const-string v2, "ke"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "545"

    const-string v2, "ki"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "467"

    const-string v2, "kp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "450"

    const-string v2, "kr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "419"

    const-string v2, "kw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "437"

    const-string v2, "kg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "457"

    const-string v2, "la"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "247"

    const-string v2, "lv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "415"

    const-string v2, "lb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "651"

    const-string v2, "ls"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "618"

    const-string v2, "lr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "606"

    const-string v2, "ly"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "295"

    const-string v2, "li"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "246"

    const-string v2, "lt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "270"

    const-string v2, "lu"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "455"

    const-string v2, "mo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "294"

    const-string v2, "mk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 456
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "646"

    const-string v2, "mg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "650"

    const-string v2, "mw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "502"

    const-string v2, "my"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "472"

    const-string v2, "mv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "610"

    const-string v2, "ml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "278"

    const-string v2, "mt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "551"

    const-string v2, "mh"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "340"

    const-string v2, "mq"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "609"

    const-string v2, "mr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "617"

    const-string v2, "mu"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 466
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "334"

    const-string v2, "mx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "550"

    const-string v2, "fm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "259"

    const-string v2, "md"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "212"

    const-string v2, "mc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "428"

    const-string v2, "mn"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "297"

    const-string v2, "me"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "354"

    const-string v2, "ms"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "604"

    const-string v2, "ma"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "643"

    const-string v2, "mz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "414"

    const-string v2, "mm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "649"

    const-string v2, "na"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 477
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "536"

    const-string v2, "nr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "429"

    const-string v2, "np"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "204"

    const-string v2, "nl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 480
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "546"

    const-string v2, "nc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "530"

    const-string v2, "nz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 482
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "710"

    const-string v2, "ni"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "614"

    const-string v2, "ne"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "621"

    const-string v2, "ng"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "555"

    const-string v2, "nu"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "534"

    const-string v2, "mp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "242"

    const-string v2, "no"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 488
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "422"

    const-string v2, "om"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "410"

    const-string v2, "pk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "552"

    const-string v2, "pw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "423"

    const-string v2, "ps"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "714"

    const-string v2, "pa"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "537"

    const-string v2, "pg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "744"

    const-string v2, "py"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "716"

    const-string v2, "pe"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "515"

    const-string v2, "ph"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "260"

    const-string v2, "pl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "268"

    const-string v2, "pt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "330"

    const-string v2, "pr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "427"

    const-string v2, "qa"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "647"

    const-string v2, "re"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "226"

    const-string v2, "ro"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 503
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "250"

    const-string v2, "ru"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "635"

    const-string v2, "rw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 505
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "356"

    const-string v2, "kn"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "358"

    const-string v2, "lc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 507
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "308"

    const-string v2, "pm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "360"

    const-string v2, "vc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 509
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "549"

    const-string v2, "ws"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "292"

    const-string v2, "sm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "626"

    const-string v2, "st"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "420"

    const-string v2, "sa"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "608"

    const-string v2, "sn"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 514
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "220"

    const-string v2, "rs"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "633"

    const-string v2, "sc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "619"

    const-string v2, "sl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "525"

    const-string v2, "sg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 518
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "231"

    const-string v2, "sk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "293"

    const-string v2, "si"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "540"

    const-string v2, "sb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "637"

    const-string v2, "so"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "655"

    const-string v2, "za"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 523
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "214"

    const-string v2, "es"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 524
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "413"

    const-string v2, "lk"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 525
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "634"

    const-string v2, "sd"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "746"

    const-string v2, "sr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "653"

    const-string v2, "sz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "240"

    const-string v2, "se"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 529
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "228"

    const-string v2, "ch"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "417"

    const-string v2, "sy"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "466"

    const-string v2, "tw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "436"

    const-string v2, "tj"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "640"

    const-string v2, "tz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "520"

    const-string v2, "th"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "615"

    const-string v2, "tg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "539"

    const-string v2, "to"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "374"

    const-string v2, "tt"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "605"

    const-string v2, "tn"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "286"

    const-string v2, "tr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "438"

    const-string v2, "tm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 541
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "376"

    const-string v2, "tc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "553"

    const-string v2, "tv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "641"

    const-string v2, "ug"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "255"

    const-string v2, "ua"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "424"

    const-string v2, "ae"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "430"

    const-string v2, "ae"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "431"

    const-string v2, "ae"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "235"

    const-string v2, "gb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "234"

    const-string v2, "gb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "310"

    const-string v2, "us"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "311"

    const-string v2, "us"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "312"

    const-string v2, "us"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "313"

    const-string v2, "us"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "314"

    const-string v2, "us"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 555
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "315"

    const-string v2, "us"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "316"

    const-string v2, "us"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 557
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "332"

    const-string v2, "vi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 558
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "748"

    const-string v2, "uy"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "434"

    const-string v2, "uz"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "541"

    const-string v2, "vu"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "225"

    const-string v2, "va"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "734"

    const-string v2, "ve"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "452"

    const-string v2, "vn"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "543"

    const-string v2, "wf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "421"

    const-string v2, "ye"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "645"

    const-string v2, "zm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 567
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    const-string v1, "648"

    const-string v2, "zw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    return-void
.end method

.method private getLocaleISO3Language(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 302
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v0

    .line 303
    .local v0, "language":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 304
    const-string v0, "eng"

    .line 307
    :cond_0
    return-object v0
.end method

.method private setTimeOutTimer()V
    .locals 4

    .prologue
    .line 84
    new-instance v0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$1;-><init>(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;)V

    .line 104
    .local v0, "task":Ljava/util/TimerTask;
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mTimeOutTimer:Ljava/util/Timer;

    .line 105
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mTimeOutTimer:Ljava/util/Timer;

    const-wide/32 v2, 0x15f90

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 106
    return-void
.end method


# virtual methods
.method public getMccToCountryNameAlpha2(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mcc"    # Ljava/lang/String;

    .prologue
    .line 319
    const/4 v0, 0x0

    .line 321
    .local v0, "result":Ljava/lang/String;
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 322
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mCountryMap:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "result":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 326
    .restart local v0    # "result":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 324
    :cond_0
    const-string v0, "global"

    goto :goto_0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->setTimeOutTimer()V

    .line 62
    new-instance v3, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;

    invoke-direct {v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;-><init>()V

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler;->tryFindAddress()Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;

    move-result-object v1

    .line 63
    .local v1, "response":Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;
    if-eqz v1, :cond_0

    iget-object v3, v1, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;->mccCode:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;->available:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 64
    iget-object v3, v1, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;->mccCode:Ljava/lang/String;

    iput-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mMcc:Ljava/lang/String;

    .line 65
    iget-object v3, v1, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldServerHandler$GldServerResponse;->available:Ljava/lang/String;

    iput-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mAvailable:Ljava/lang/String;

    .line 67
    const-string v3, "HeadlinesTermsDownloader"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "run() : mMcc ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mMcc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mAvailable = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mAvailable:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :goto_0
    new-instance v2, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;

    const-string v3, "TermsAndConditions"

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;-><init>(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;Ljava/lang/String;)V

    .line 73
    .local v2, "termsDownloadThread":Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;
    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->start()V

    .line 75
    new-instance v0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;

    const-string v3, "PrivacyPolicy"

    invoke-direct {v0, p0, v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;-><init>(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;Ljava/lang/String;)V

    .line 76
    .local v0, "privactyDownloadThread":Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;
    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->start()V

    .line 77
    return-void

    .line 69
    .end local v0    # "privactyDownloadThread":Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;
    .end local v2    # "termsDownloadThread":Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;
    :cond_0
    const-string v3, "HeadlinesTermsDownloader"

    const-string v4, "run() : mResponse is NULL."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDownloadHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "downloadHandler"    # Landroid/os/Handler;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mDownloadEventHandler:Landroid/os/Handler;

    .line 81
    return-void
.end method
