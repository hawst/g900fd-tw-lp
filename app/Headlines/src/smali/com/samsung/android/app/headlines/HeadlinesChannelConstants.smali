.class public Lcom/samsung/android/app/headlines/HeadlinesChannelConstants;
.super Ljava/lang/Object;
.source "HeadlinesChannelConstants.java"


# static fields
.field public static final CHANNEL_HEADLINES_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.app.headlines"

.field public static final CHANNEL_TYPE_HEADLINES:Ljava/lang/String; = "headlines"

.field public static final HEADLINES_PACKAGE:Ljava/lang/String; = "com.samsung.android.app.headlines"

.field public static final PREFERENCE_CARD_SET:Ljava/lang/String; = "CardOrder"

.field public static final PREFERENCE_IS_FIRST_LAUNCH:Ljava/lang/String; = "IsFirstLaunch"

.field public static final PREFERENCE_LOCALE:Ljava/lang/String; = "currentLocale"

.field public static final PREFERENCE_SAVED_CARD_COUNT:Ljava/lang/String; = "CardCount"

.field public static final SETTING_TAG:Ljava/lang/String; = "HeadlinesChannelSetting"

.field public static final TAG:Ljava/lang/String; = "HeadlinesChannel"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
