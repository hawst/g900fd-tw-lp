.class public Lcom/samsung/android/app/headlines/activity/HeadlinesAboutActivity;
.super Landroid/app/Activity;
.source "HeadlinesAboutActivity.java"


# instance fields
.field mLayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesAboutActivity;->mLayout:Landroid/widget/LinearLayout;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 23
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const/high16 v2, 0x7f030000

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/activity/HeadlinesAboutActivity;->setContentView(I)V

    .line 26
    invoke-static {p0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isOrientationSupport(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 27
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesAboutActivity;->setRequestedOrientation(I)V

    .line 29
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesAboutActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 30
    .local v0, "actionbar":Landroid/app/ActionBar;
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 31
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 32
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 33
    const v2, 0x7f090046

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(I)V

    .line 35
    const v2, 0x7f0c0003

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/activity/HeadlinesAboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 36
    .local v1, "termsButton":Landroid/widget/Button;
    new-instance v2, Lcom/samsung/android/app/headlines/activity/HeadlinesAboutActivity$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesAboutActivity$1;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesAboutActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 52
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 54
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesAboutActivity;->finish()V

    .line 55
    const/4 v0, 0x1

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
