.class Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;
.super Ljava/lang/Object;
.source "HeadlinesHeaderView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/PopupMenu$OnDismissListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionBarEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;
    .param p2, "x1"    # Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$1;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 138
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mIsPopupShown:Z
    invoke-static {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;)Z

    move-result v0

    if-eq v0, v2, :cond_1

    .line 139
    const-string v0, "HeadlinesHeaderView"

    const-string v1, "onClick(): Show POPUP"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;
    invoke-static {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;)Landroid/widget/PopupMenu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;
    invoke-static {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;)Landroid/widget/PopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    # setter for: Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mIsPopupShown:Z
    invoke-static {v0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->access$102(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;Z)Z

    .line 149
    :goto_0
    return-void

    .line 146
    :cond_1
    const-string v0, "HeadlinesHeaderView"

    const-string v1, "onClick(): POPUP is already shown."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDismiss(Landroid/widget/PopupMenu;)V
    .locals 2
    .param p1, "popupMenu"    # Landroid/widget/PopupMenu;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mIsPopupShown:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->access$102(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;Z)Z

    .line 115
    const-string v0, "HeadlinesHeaderView"

    const-string v1, "onClick(): POPUP is DISMISSED."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 121
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 133
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 123
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->openSetting()V

    goto :goto_0

    .line 126
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->openAbout()V

    goto :goto_0

    .line 129
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->openHelp()V

    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c0051
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
