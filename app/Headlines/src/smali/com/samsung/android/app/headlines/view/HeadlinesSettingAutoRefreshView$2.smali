.class Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$2;
.super Landroid/view/View$AccessibilityDelegate;
.source "HeadlinesSettingAutoRefreshView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

.field final synthetic val$refreshView:Landroid/widget/RelativeLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Landroid/widget/RelativeLayout;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    iput-object p2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$2;->val$refreshView:Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 11
    .param p1, "host"    # Landroid/view/View;
    .param p2, "action"    # I
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x1

    .line 101
    const/16 v8, 0x40

    if-ne p2, v8, :cond_2

    .line 102
    const/4 v7, 0x0

    .line 103
    .local v7, "titleContent":Ljava/lang/CharSequence;
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$2;->val$refreshView:Landroid/widget/RelativeLayout;

    const v9, 0x7f0c0031

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 104
    .local v6, "title":Landroid/widget/TextView;
    if-eqz v6, :cond_0

    .line 105
    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    .line 107
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$2;->val$refreshView:Landroid/widget/RelativeLayout;

    const v9, 0x7f0c0032

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 108
    .local v2, "detail":Landroid/widget/TextView;
    const/4 v3, 0x0

    .line 109
    .local v3, "detailContent":Ljava/lang/CharSequence;
    if-eqz v6, :cond_1

    .line 110
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    .line 113
    :cond_1
    const/4 v1, 0x0

    .line 114
    .local v1, "checkContent":Ljava/lang/CharSequence;
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mRefresh:Z
    invoke-static {v8}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$000(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Z

    move-result v8

    if-ne v8, v10, :cond_3

    .line 115
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09003e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 119
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 121
    const-string v8, " "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 123
    const-string v8, " "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 125
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 126
    .local v4, "result":Ljava/lang/String;
    const/4 v8, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v4, v8, v9}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    .line 127
    .local v5, "result2":Ljava/lang/CharSequence;
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$2;->val$refreshView:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v5}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 130
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "checkContent":Ljava/lang/CharSequence;
    .end local v2    # "detail":Landroid/widget/TextView;
    .end local v3    # "detailContent":Ljava/lang/CharSequence;
    .end local v4    # "result":Ljava/lang/String;
    .end local v5    # "result2":Ljava/lang/CharSequence;
    .end local v6    # "title":Landroid/widget/TextView;
    .end local v7    # "titleContent":Ljava/lang/CharSequence;
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/view/View$AccessibilityDelegate;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    .line 132
    return v10

    .line 117
    .restart local v1    # "checkContent":Ljava/lang/CharSequence;
    .restart local v2    # "detail":Landroid/widget/TextView;
    .restart local v3    # "detailContent":Ljava/lang/CharSequence;
    .restart local v6    # "title":Landroid/widget/TextView;
    .restart local v7    # "titleContent":Ljava/lang/CharSequence;
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09003f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
