.class public Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;
.super Landroid/app/Application;
.source "HeadlinesChannelApplication.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "HeadlinesChannelApplication"

.field public static instance:Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;->instance:Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 17
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;->instance:Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;

    if-eqz v0, :cond_0

    .line 18
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "More than one MainApp instance!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    sput-object p0, Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;->instance:Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;

    .line 21
    return-void
.end method

.method public static getInstance()Landroid/app/Application;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;->instance:Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 29
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/internal/headlines/HeadlinesService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 30
    .local v0, "sIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 31
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 32
    const-string v1, "HeadlinesChannelApplication"

    const-string v2, "[onCreate]"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 37
    const-string v0, "HeadlinesChannelApplication"

    const-string v1, "HeadlinesChannelApplication.onLowMemory()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const/16 v0, 0x50

    invoke-static {v0}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->trim(I)V

    .line 39
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 43
    const-string v0, "HeadlinesChannelApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeadlinesChannelApplication.onTrimMemory(). level : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-static {p1}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->trim(I)V

    .line 45
    return-void
.end method
