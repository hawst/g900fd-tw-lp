.class public Lcom/samsung/android/app/headlines/HeadlinesCardManager;
.super Ljava/lang/Object;
.source "HeadlinesCardManager.java"

# interfaces
.implements Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;


# static fields
.field private static final CHANNEL_TYPE_HEADLINES:Ljava/lang/String; = "headlines"

.field private static final CHANNEL_TYPE_HEADLINES_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.app.headlines"

.field private static final SECTION_NEWS:Ljava/lang/String; = "news"

.field private static final SECTION_SOCIAL:Ljava/lang/String; = "social"

.field private static final TAG:Ljava/lang/String; = "HeadlinesCardManager"

.field private static mHeadlinesCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;


# instance fields
.field private mAutoRefresh:Z

.field private mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

.field private mCardManager:Lcom/samsung/android/magazine/cardchannel/CardManager;

.field private mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mHeadlinesCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    .line 30
    iput-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    .line 31
    iput-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardchannel/CardManager;

    .line 32
    iput-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mAutoRefresh:Z

    .line 45
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesChannelApplication;->getInstance()Landroid/app/Application;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->init(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mHeadlinesCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    invoke-direct {v0}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;-><init>()V

    sput-object v0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mHeadlinesCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    .line 41
    :cond_0
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mHeadlinesCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    return-object v0
.end method

.method private getLastUpdatedTime()J
    .locals 4

    .prologue
    .line 196
    iget-object v1, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v2, "mymagazine"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 197
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "LAST_UPDATED_TIME"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    return-wide v2
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    const-string v0, "HeadlinesCardManager"

    const-string v1, "init()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iput-object p1, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    .line 53
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannel;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v2, "headlines"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/magazine/cardchannel/CardChannel;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    .line 54
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardManager;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardManager;-><init>(Lcom/samsung/android/magazine/cardchannel/CardChannel;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardchannel/CardManager;

    .line 56
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;-><init>(Lcom/samsung/android/magazine/cardchannel/CardChannel;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    .line 57
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->registerCardChannel()Z

    .line 58
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    invoke-virtual {v0, p0}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->addCardProviderChangeListener(Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;)V

    .line 60
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    if-nez v0, :cond_0

    .line 61
    const-string v0, "HeadlinesCardManager"

    const-string v1, "init() : mConfigurationManager is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->registerAllCardNames()V

    .line 68
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->surbscribeDefaultCardNames()V

    goto :goto_0
.end method

.method private registerCardNames(Ljava/lang/String;)V
    .locals 11
    .param p1, "cardProviderName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 107
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getLastUpdatedTime()J

    move-result-wide v6

    .line 108
    .local v6, "lastUpdatedTime":J
    const-string v5, "HeadlinesCardManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "registerCardNames : cardProviderName = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", lastUpdatedTime = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "ASC"

    invoke-virtual {v5, p1, v6, v7, v8}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->getUpdatedCardNames(Ljava/lang/String;JLjava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 111
    .local v2, "cardNamesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v2, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    const-string v5, "HeadlinesCardManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "registerCardNames : card names size "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const/4 v0, 0x0

    .line 117
    .local v0, "cardInfo":Lcom/samsung/android/magazine/cardchannel/CardInfo;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 118
    .local v1, "cardName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    invoke-virtual {v5, p1, v1}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->isRegistered(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    .line 120
    .local v4, "isRegistered":Z
    if-nez v4, :cond_2

    .line 121
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    invoke-virtual {v5, p1, v1}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->getCardInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardInfo;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_2

    .line 123
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->getSection()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v5, p1, v1, v8, v9}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->registerCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 124
    const-string v5, "HeadlinesCardManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "registerCardNames : Section name = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->getSection()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", cardName = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 130
    .end local v1    # "cardName":Ljava/lang/String;
    .end local v4    # "isRegistered":Z
    :cond_3
    const-string v5, "VZW"

    invoke-static {v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isOperatorModel(Ljava/lang/String;)Z

    move-result v5

    if-eq v5, v10, :cond_4

    const-string v5, "LRA"

    invoke-static {v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isOperatorModel(Ljava/lang/String;)Z

    move-result v5

    if-ne v5, v10, :cond_5

    .line 131
    :cond_4
    const-string v5, "HeadlinesCardManager"

    const-string v8, "updateRegisteredCardType() : VZW / LRA. unregister card name : SOCIAL_FIVEHUNDREDPX, SOCIAL_RENREN, SOCIAL_WEIBO, SOCIAL_TUMBLR"

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "com.samsung.android.app.headlines"

    const-string v9, "social fivehundredpx"

    const-string v10, "social"

    invoke-virtual {v5, v8, v9, v10}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->unregisterCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "com.samsung.android.app.headlines"

    const-string v9, "social renren"

    const-string v10, "social"

    invoke-virtual {v5, v8, v9, v10}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->unregisterCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "com.samsung.android.app.headlines"

    const-string v9, "social weibo"

    const-string v10, "social"

    invoke-virtual {v5, v8, v9, v10}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->unregisterCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "com.samsung.android.app.headlines"

    const-string v9, "social tumblr"

    const-string v10, "social"

    invoke-virtual {v5, v8, v9, v10}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->unregisterCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 136
    :cond_5
    const-string v5, "CN"

    invoke-static {v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v5

    if-ne v5, v10, :cond_0

    .line 137
    const-string v5, "HeadlinesCardManager"

    const-string v8, "updateRegisteredCardType() : VZW. unregister card name : SOCIAL_FIVEHUNDREDPX, SOCIAL_RENREN, SOCIAL_WEIBO, SOCIAL_TUMBLR"

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "com.samsung.android.app.headlines"

    const-string v9, "social twitter"

    const-string v10, "social"

    invoke-virtual {v5, v8, v9, v10}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->unregisterCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "com.samsung.android.app.headlines"

    const-string v9, "social googleplus"

    const-string v10, "social"

    invoke-virtual {v5, v8, v9, v10}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->unregisterCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "com.samsung.android.app.headlines"

    const-string v9, "social linkedin"

    const-string v10, "social"

    invoke-virtual {v5, v8, v9, v10}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->unregisterCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "com.samsung.android.app.headlines"

    const-string v9, "social flickr"

    const-string v10, "social"

    invoke-virtual {v5, v8, v9, v10}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->unregisterCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "com.samsung.android.app.headlines"

    const-string v9, "social tumblr"

    const-string v10, "social"

    invoke-virtual {v5, v8, v9, v10}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->unregisterCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "com.samsung.android.app.headlines"

    const-string v9, "social fivehundredpx"

    const-string v10, "social"

    invoke-virtual {v5, v8, v9, v10}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->unregisterCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "com.samsung.android.app.headlines"

    const-string v9, "social youtube"

    const-string v10, "social"

    invoke-virtual {v5, v8, v9, v10}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->unregisterCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private setLastUpdateTime(J)V
    .locals 5
    .param p1, "lastUpdatedTime"    # J

    .prologue
    .line 201
    iget-object v2, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v3, "mymagazine"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 202
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 203
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "LAST_UPDATED_TIME"

    invoke-interface {v0, v2, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 204
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 205
    return-void
.end method

.method private surbscribeDefaultCardNames()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 149
    iget-object v7, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    const-string v8, "mymagazine"

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 150
    .local v5, "prefs":Landroid/content/SharedPreferences;
    const-string v7, "SUBSCRIBE_DEFAULT_CARD_NAME"

    invoke-interface {v5, v7, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 152
    .local v3, "need":Z
    if-ne v3, v10, :cond_2

    .line 153
    const-string v7, "HeadlinesCardManager"

    const-string v8, "surbscribeDefaultCardNames : First launch. Default cards subscribe broadcast."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getDefaultCardNames()Ljava/util/List;

    move-result-object v1

    .line 156
    .local v1, "cardNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_1

    .line 157
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 158
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 159
    .local v0, "cardName":Ljava/lang/String;
    const-string v7, "news"

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v6, "news"

    .line 160
    .local v6, "section":Ljava/lang/String;
    :goto_1
    iget-object v7, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v8, "com.samsung.android.app.headlines"

    invoke-virtual {v7, v8, v0, v6}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->subscribeToCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 159
    .end local v6    # "section":Ljava/lang/String;
    :cond_0
    const-string v6, "social"

    goto :goto_1

    .line 164
    .end local v0    # "cardName":Ljava/lang/String;
    .end local v2    # "i":I
    :cond_1
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 165
    .local v4, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v7, "SUBSCRIBE_DEFAULT_CARD_NAME"

    invoke-interface {v4, v7, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 166
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 170
    .end local v1    # "cardNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :goto_2
    return-void

    .line 168
    :cond_2
    const-string v7, "HeadlinesCardManager"

    const-string v8, "surbscribeDefaultCardNames : This time is not first lauching."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public addCardChangeEventListener(Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardchannel/CardManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/magazine/cardchannel/CardManager;->addCardChangeListener(Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;)V

    .line 73
    return-void
.end method

.method public getCard(I)Lcom/samsung/android/magazine/cardchannel/Card;
    .locals 1
    .param p1, "cardId"    # I

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardchannel/CardManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/magazine/cardchannel/CardManager;->getCard(I)Lcom/samsung/android/magazine/cardchannel/Card;

    move-result-object v0

    return-object v0
.end method

.method public getCardTypeList(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "section"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/CardInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 216
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v1, "ASC"

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->getRegisteredCardInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 220
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCards()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/Card;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardchannel/CardManager;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardManager;->getCards()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRefreshState()Z
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mAutoRefresh:Z

    return v0
.end method

.method public getSectionDisplayName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "section"    # Ljava/lang/String;

    .prologue
    .line 252
    const-string v0, ""

    .line 254
    .local v0, "sectionDisplayName":Ljava/lang/String;
    const-string v1, "news"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 255
    iget-object v1, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 264
    :cond_0
    :goto_0
    return-object v0

    .line 256
    :cond_1
    const-string v1, "social"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 257
    iget-object v1, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 259
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    if-eqz v1, :cond_0

    .line 260
    iget-object v1, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    invoke-virtual {v1, p1}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->getSectionDisplayName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSectionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->getSections()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isSubscribed(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "cardType"    # Ljava/lang/String;
    .param p2, "section"    # Ljava/lang/String;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1, p2}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->isSubscribed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 240
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onChanged(Ljava/lang/String;)V
    .locals 5
    .param p1, "cardProviderName"    # Ljava/lang/String;

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getLastUpdatedTime()J

    move-result-wide v0

    .line 175
    .local v0, "lastUpdatedTime":J
    const-string v2, "HeadlinesCardManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onChanged : cardProviderName = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", lastUpdatedTime = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v2, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    if-nez v2, :cond_0

    .line 178
    const-string v2, "HeadlinesCardManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onChanged : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :goto_0
    return-void

    .line 182
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->registerCardNames(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public registerAllCardNames()V
    .locals 8

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getLastUpdatedTime()J

    move-result-wide v2

    .line 89
    .local v2, "lastUpdatedTime":J
    const-string v5, "HeadlinesCardManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "registerAllCardNames : lastUpdatedTime = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v5, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const-string v6, "ASC"

    invoke-virtual {v5, v2, v3, v6}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->getUpdatedCardProviders(JLjava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 92
    .local v4, "providerList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v4, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 96
    .local v0, "cardProviderName":Ljava/lang/String;
    const-string v5, "HeadlinesCardManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "registerAllCardNames : provider "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-direct {p0, v0}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->registerCardNames(Ljava/lang/String;)V

    goto :goto_1

    .line 100
    .end local v0    # "cardProviderName":Ljava/lang/String;
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 101
    const-string v5, "HeadlinesCardManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "registerAllCardNames : providerList size "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->setLastUpdateTime(J)V

    goto :goto_0
.end method

.method public removeCardChangeEventListener(Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardchannel/CardManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/magazine/cardchannel/CardManager;->removeCardChangeListener(Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;)V

    .line 77
    return-void
.end method

.method public requestRefresh(I)V
    .locals 1
    .param p1, "cardId"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mCardManager:Lcom/samsung/android/magazine/cardchannel/CardManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/magazine/cardchannel/CardManager;->requestCardRefresh(I)V

    .line 85
    return-void
.end method

.method public setRefreshState(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 244
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mAutoRefresh:Z

    .line 245
    return-void
.end method

.method public subscribeToCardType(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "cardType"    # Ljava/lang/String;
    .param p2, "section"    # Ljava/lang/String;

    .prologue
    .line 224
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1, p2}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->subscribeToCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_0
    return-void
.end method

.method public unsubscribeToCardType(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "cardType"    # Ljava/lang/String;
    .param p2, "section"    # Ljava/lang/String;

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1, p2}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->unsubscribeToCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_0
    return-void
.end method

.method public update()V
    .locals 2

    .prologue
    .line 186
    const-string v0, "HeadlinesCardManager"

    const-string v1, "update : update sections, all card names."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->mConfigurationManager:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    if-nez v0, :cond_0

    .line 193
    :goto_0
    return-void

    .line 192
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->registerAllCardNames()V

    goto :goto_0
.end method
