.class Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$6;
.super Ljava/lang/Object;
.source "HeadlinesSettingAutoRefreshView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->dialogAutoRefreshAlertPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$6;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    iput-object p2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$6;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v6, 0x1

    .line 199
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$6;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # setter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mAlreadyConfirm:Z
    invoke-static {v3, v6}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$402(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Z)Z

    .line 200
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$6;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$6;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    # setter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mDoNotShow:Z
    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$302(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Z)Z

    .line 201
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$6;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 202
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$6;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "mymagazine"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 203
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 204
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "AUTO_REFRESH_POPUP_SHOW"

    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$6;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mDoNotShow:Z
    invoke-static {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$300(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Z

    move-result v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 205
    const-string v3, "AUTO_REFRESH_SET_FIRST_TIME"

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 206
    const-string v3, "AUTO_REFRESH_ON"

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 207
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 209
    const-string v3, "CN"

    invoke-static {v3}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 210
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 211
    .local v1, "i":Landroid/content/Intent;
    const-string v3, "com.samsung.android.internal.headlines.ALLOW_DATA_USE"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$6;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 215
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 216
    return-void
.end method
