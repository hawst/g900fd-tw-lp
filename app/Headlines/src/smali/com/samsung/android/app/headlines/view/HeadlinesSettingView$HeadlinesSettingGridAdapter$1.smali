.class Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter$1;
.super Landroid/view/View$AccessibilityDelegate;
.source "HeadlinesSettingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

.field final synthetic val$currentItem:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter$1;->this$1:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    iput-object p2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter$1;->val$currentItem:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 6
    .param p1, "host"    # Landroid/view/View;
    .param p2, "action"    # I
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 398
    invoke-super {p0, p1, p2, p3}, Landroid/view/View$AccessibilityDelegate;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    .line 400
    const/16 v2, 0x40

    if-ne p2, v2, :cond_0

    .line 401
    const/4 v1, 0x0

    .line 403
    .local v1, "checkContent":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter$1;->val$currentItem:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->isChecked()Z

    move-result v2

    if-ne v2, v5, :cond_1

    .line 404
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter$1;->this$1:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    iget-object v2, v2, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09003e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 408
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 409
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter$1;->val$currentItem:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 412
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v4, v2}, Ljava/lang/StringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 413
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v4, v2}, Ljava/lang/StringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 416
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "checkContent":Ljava/lang/CharSequence;
    :cond_0
    return v5

    .line 406
    .restart local v1    # "checkContent":Ljava/lang/CharSequence;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter$1;->this$1:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    iget-object v2, v2, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09003f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
