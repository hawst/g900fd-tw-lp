.class Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$1;
.super Ljava/lang/Object;
.source "HeadlinesLandscapeMainContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$1;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 134
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$1;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;
    invoke-static {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getCount()I

    move-result v0

    .line 135
    .local v0, "lastCardIndex":I
    const-string v1, "HeadlinesLandscapeMainContainer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setOnClickListener() : Scroll to the card. CardIndex = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$1;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;
    invoke-static {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->smoothScrollToPosition(I)V

    .line 137
    return-void
.end method
