.class Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;
.super Ljava/lang/Object;
.source "HeadlinesCardAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->processCardEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;->this$0:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 317
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;->this$0:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    # getter for: Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->access$000(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 318
    :try_start_0
    const-string v3, "HeadlinesCardAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "processCardEvent mEventQueqkdue. total count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;->this$0:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    # getter for: Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mEventQueue:Ljava/util/Vector;
    invoke-static {v6}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->access$100(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;->this$0:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    # getter for: Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mEventQueue:Ljava/util/Vector;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->access$100(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 320
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;->this$0:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    # getter for: Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mEventQueue:Ljava/util/Vector;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->access$100(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Vector;->firstElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;

    .line 321
    .local v2, "event":Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;
    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;->getCard()Lcom/samsung/android/magazine/cardchannel/Card;

    move-result-object v0

    .line 322
    .local v0, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;->getId()I

    move-result v1

    .line 323
    .local v1, "cardId":I
    sget-object v3, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$2;->$SwitchMap$com$samsung$android$app$headlines$adapter$HeadlinesCardAdapter$COMMAND_TYPE:[I

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;->getType()Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    .line 343
    const-string v3, "HeadlinesCardAdapter"

    const-string v5, "processCardEvent default :("

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;->this$0:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    # getter for: Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->mEventQueue:Ljava/util/Vector;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->access$100(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)Ljava/util/Vector;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 348
    .end local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    .end local v1    # "cardId":I
    .end local v2    # "event":Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 325
    .restart local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    .restart local v1    # "cardId":I
    .restart local v2    # "event":Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;
    :pswitch_0
    if-eqz v0, :cond_0

    .line 326
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;->this$0:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    # invokes: Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->addCard(Lcom/samsung/android/magazine/cardchannel/Card;)V
    invoke-static {v3, v0}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->access$200(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;Lcom/samsung/android/magazine/cardchannel/Card;)V

    .line 327
    const-string v3, "HeadlinesCardAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "processCardEvent add card : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 331
    :pswitch_1
    if-eqz v0, :cond_0

    .line 332
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;->this$0:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;->getCard()Lcom/samsung/android/magazine/cardchannel/Card;

    move-result-object v5

    # invokes: Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->updateCard(Lcom/samsung/android/magazine/cardchannel/Card;)V
    invoke-static {v3, v5}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->access$300(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;Lcom/samsung/android/magazine/cardchannel/Card;)V

    .line 333
    const-string v3, "HeadlinesCardAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "processCardEvent update card : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 337
    :pswitch_2
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    .line 338
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;->this$0:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    # invokes: Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->removeCard(I)V
    invoke-static {v3, v1}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->access$400(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;I)V

    .line 339
    const-string v3, "HeadlinesCardAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "processCardEvent remove card : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 348
    .end local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    .end local v1    # "cardId":I
    .end local v2    # "event":Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;
    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349
    iget-object v3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$1;->this$0:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;->notifyDataSetChanged()V

    .line 350
    return-void

    .line 323
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
