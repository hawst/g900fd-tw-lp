.class public Lcom/samsung/android/app/headlines/UpgradeManager;
.super Ljava/lang/Object;
.source "UpgradeManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;
    }
.end annotation


# static fields
.field private static final CRITICAL_POPUP:I = 0x1

.field private static final NORMAL_POPUP:I = 0x2

.field private static final NO_POPUP:I = 0x0

.field private static final PD_TEST_PATH:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static final UPGRADE_MIN_CHECK:J = 0x5265c00L

.field private static popup_show:Z


# instance fields
.field public mUpdateHandler:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/go_to_andromeda.test"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/headlines/UpgradeManager;->PD_TEST_PATH:Ljava/lang/String;

    .line 35
    const-class v0, Lcom/samsung/android/app/headlines/UpgradeManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;

    .line 41
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/headlines/UpgradeManager;->popup_show:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Lcom/samsung/android/app/headlines/UpgradeManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/headlines/UpgradeManager$1;-><init>(Lcom/samsung/android/app/headlines/UpgradeManager;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/UpgradeManager;->mUpdateHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-static {p0, p1}, Lcom/samsung/android/app/headlines/UpgradeManager;->isForceUpdate(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lcom/samsung/android/app/headlines/UpgradeManager;->isTestMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/UpgradeManager;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/headlines/UpgradeManager;->checkUpdate(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/UpgradeManager;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/UpgradeManager;->dialogCritialUpdate(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/UpgradeManager;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/UpgradeManager;->dialogNormalUpdate(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$602(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 33
    sput-boolean p0, Lcom/samsung/android/app/headlines/UpgradeManager;->popup_show:Z

    return p0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/UpgradeManager;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/headlines/UpgradeManager;->dialogOpenAppInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private checkUpdate(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "forceTest"    # Z

    .prologue
    .line 132
    const/4 v9, 0x0

    .line 135
    .local v9, "packageInfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v14, v0, v15}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 140
    :goto_0
    if-eqz v9, :cond_5

    .line 141
    iget-object v13, v9, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 145
    .local v13, "versionName":Ljava/lang/String;
    :goto_1
    const-string v14, "\\."

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 148
    .local v1, "array":[Ljava/lang/String;
    :try_start_1
    invoke-static/range {p1 .. p3}, Lcom/samsung/android/app/headlines/update/UpdateUtil;->getUpdateCheckUrl(Landroid/content/Context;Ljava/lang/String;Z)Ljava/net/URL;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v12

    .line 154
    .local v12, "url":Ljava/net/URL;
    invoke-static {v12}, Lcom/samsung/android/app/headlines/update/UpdateParser;->checkUpdate(Ljava/net/URL;)Lcom/samsung/android/app/headlines/update/UpdateInfo;

    move-result-object v4

    .line 155
    .local v4, "info":Lcom/samsung/android/app/headlines/update/UpdateInfo;
    sget-object v14, Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "## Samsung Apps Update Info ## : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-virtual {v4}, Lcom/samsung/android/app/headlines/update/UpdateInfo;->getResultCode()I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_4

    .line 157
    if-eqz p2, :cond_4

    invoke-virtual {v4}, Lcom/samsung/android/app/headlines/update/UpdateInfo;->getAppId()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 158
    invoke-virtual {v4}, Lcom/samsung/android/app/headlines/update/UpdateInfo;->getVersionName()Ljava/lang/String;

    move-result-object v14

    const-string v15, "\\."

    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 159
    .local v6, "newVer":[Ljava/lang/String;
    const-string v14, "mymagazine"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 160
    .local v10, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 161
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v14, "VERSION_IN_SAMSUNG_APPS"

    invoke-virtual {v4}, Lcom/samsung/android/app/headlines/update/UpdateInfo;->getVersionName()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v3, v14, v15}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 162
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 164
    array-length v14, v1

    const/4 v15, 0x3

    if-ne v14, v15, :cond_4

    array-length v14, v6

    const/4 v15, 0x3

    if-ne v14, v15, :cond_4

    .line 165
    const/4 v11, 0x0

    .line 166
    .local v11, "result":I
    const/4 v14, 0x0

    aget-object v14, v1, v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 167
    .local v8, "oldVersion":I
    const/4 v14, 0x0

    aget-object v14, v6, v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 168
    .local v7, "newVersion":I
    if-le v8, v7, :cond_6

    .line 169
    sget-object v14, Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v15, "no update"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    const/4 v11, 0x0

    .line 175
    :cond_0
    :goto_2
    const/4 v14, 0x1

    aget-object v14, v1, v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 176
    const/4 v14, 0x1

    aget-object v14, v6, v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 177
    if-le v8, v7, :cond_7

    .line 178
    sget-object v14, Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v15, "No update"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const/4 v11, 0x0

    .line 185
    :cond_1
    :goto_3
    const/4 v14, 0x2

    aget-object v14, v1, v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 186
    const/4 v14, 0x2

    aget-object v14, v6, v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 187
    if-le v8, v7, :cond_2

    .line 188
    sget-object v14, Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v15, "no update"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    const/4 v11, 0x0

    .line 191
    :cond_2
    if-ge v8, v7, :cond_3

    .line 192
    sget-object v14, Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v15, "normal update"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const/4 v11, 0x2

    .line 196
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/headlines/UpgradeManager;->mUpdateHandler:Landroid/os/Handler;

    if-eqz v14, :cond_4

    .line 197
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/headlines/UpgradeManager;->mUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v14}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v5

    .line 198
    .local v5, "msg":Landroid/os/Message;
    iput v11, v5, Landroid/os/Message;->what:I

    .line 199
    move-object/from16 v0, p1

    iput-object v0, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 200
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/headlines/UpgradeManager;->mUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v14, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 205
    .end local v3    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v4    # "info":Lcom/samsung/android/app/headlines/update/UpdateInfo;
    .end local v5    # "msg":Landroid/os/Message;
    .end local v6    # "newVer":[Ljava/lang/String;
    .end local v7    # "newVersion":I
    .end local v8    # "oldVersion":I
    .end local v10    # "pref":Landroid/content/SharedPreferences;
    .end local v11    # "result":I
    .end local v12    # "url":Ljava/net/URL;
    :cond_4
    :goto_4
    return-void

    .line 136
    .end local v1    # "array":[Ljava/lang/String;
    .end local v13    # "versionName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 137
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 143
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_5
    const-string v13, "1.0.0"

    .restart local v13    # "versionName":Ljava/lang/String;
    goto/16 :goto_1

    .line 150
    .restart local v1    # "array":[Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 151
    .local v2, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v2}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_4

    .line 171
    .end local v2    # "e":Ljava/net/MalformedURLException;
    .restart local v3    # "editor":Landroid/content/SharedPreferences$Editor;
    .restart local v4    # "info":Lcom/samsung/android/app/headlines/update/UpdateInfo;
    .restart local v6    # "newVer":[Ljava/lang/String;
    .restart local v7    # "newVersion":I
    .restart local v8    # "oldVersion":I
    .restart local v10    # "pref":Landroid/content/SharedPreferences;
    .restart local v11    # "result":I
    .restart local v12    # "url":Ljava/net/URL;
    :cond_6
    if-ge v8, v7, :cond_0

    .line 172
    sget-object v14, Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v15, "Critical update"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    const/4 v11, 0x1

    goto :goto_2

    .line 181
    :cond_7
    if-ge v8, v7, :cond_1

    .line 182
    sget-object v14, Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v15, "Critical update"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    const/4 v11, 0x1

    goto :goto_3
.end method

.method private dialogCritialUpdate(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 208
    sget-boolean v1, Lcom/samsung/android/app/headlines/UpgradeManager;->popup_show:Z

    if-ne v1, v3, :cond_1

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    if-eqz p1, :cond_0

    instance-of v1, p1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 212
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x7f0a0005

    invoke-direct {v1, p1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 213
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 214
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 215
    const v1, 0x104000a

    new-instance v2, Lcom/samsung/android/app/headlines/UpgradeManager$2;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/UpgradeManager$2;-><init>(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 256
    const/high16 v1, 0x1040000

    new-instance v2, Lcom/samsung/android/app/headlines/UpgradeManager$3;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/UpgradeManager$3;-><init>(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 266
    new-instance v1, Lcom/samsung/android/app/headlines/UpgradeManager$4;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/headlines/UpgradeManager$4;-><init>(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 276
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 277
    sput-boolean v3, Lcom/samsung/android/app/headlines/UpgradeManager;->popup_show:Z

    goto :goto_0
.end method

.method private dialogNormalUpdate(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 282
    sget-boolean v1, Lcom/samsung/android/app/headlines/UpgradeManager;->popup_show:Z

    if-ne v1, v3, :cond_0

    .line 344
    :goto_0
    return-void

    .line 284
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x7f0a0005

    invoke-direct {v1, p1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 285
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 286
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 287
    const v1, 0x104000a

    new-instance v2, Lcom/samsung/android/app/headlines/UpgradeManager$5;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/UpgradeManager$5;-><init>(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 328
    const v1, 0x7f09005a

    new-instance v2, Lcom/samsung/android/app/headlines/UpgradeManager$6;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/headlines/UpgradeManager$6;-><init>(Lcom/samsung/android/app/headlines/UpgradeManager;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 335
    new-instance v1, Lcom/samsung/android/app/headlines/UpgradeManager$7;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/headlines/UpgradeManager$7;-><init>(Lcom/samsung/android/app/headlines/UpgradeManager;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 342
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 343
    sput-boolean v3, Lcom/samsung/android/app/headlines/UpgradeManager;->popup_show:Z

    goto :goto_0
.end method

.method private dialogOpenAppInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 381
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v3, Landroid/view/ContextThemeWrapper;

    const v4, 0x7f0a0005

    invoke-direct {v3, p1, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 383
    .local v0, "OpenAppInfoDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 385
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 386
    .local v2, "unable_to_open_this_application":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 387
    .local v1, "text":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 390
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/app/headlines/UpgradeManager$8;

    invoke-direct {v4, p0, p3, p1}, Lcom/samsung/android/app/headlines/UpgradeManager$8;-><init>(Lcom/samsung/android/app/headlines/UpgradeManager;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 403
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x104000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/app/headlines/UpgradeManager$9;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/headlines/UpgradeManager$9;-><init>(Lcom/samsung/android/app/headlines/UpgradeManager;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 412
    new-instance v3, Lcom/samsung/android/app/headlines/UpgradeManager$10;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/headlines/UpgradeManager$10;-><init>(Lcom/samsung/android/app/headlines/UpgradeManager;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 419
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 420
    return-void
.end method

.method private static isForceUpdate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p0, "oldVersion"    # Ljava/lang/String;
    .param p1, "newVersion"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 348
    const-string v5, "\\."

    invoke-virtual {p0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 349
    .local v3, "oldVer":[Ljava/lang/String;
    const-string v5, "\\."

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 350
    .local v1, "newVer":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 351
    .local v4, "r":Z
    array-length v5, v3

    if-ne v5, v8, :cond_1

    array-length v5, v1

    if-ne v5, v8, :cond_1

    .line 352
    aget-object v5, v3, v6

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 353
    .local v2, "oldV":I
    aget-object v5, v1, v6

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 354
    .local v0, "newV":I
    if-ge v2, v0, :cond_0

    .line 355
    sget-object v5, Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v6, "Critical update"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    const/4 v4, 0x1

    .line 358
    :cond_0
    aget-object v5, v3, v7

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 359
    aget-object v5, v1, v7

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 360
    if-ge v2, v0, :cond_1

    .line 361
    sget-object v5, Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;

    const-string v6, "Critical update"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    const/4 v4, 0x1

    .line 365
    .end local v0    # "newV":I
    .end local v2    # "oldV":I
    :cond_1
    return v4
.end method

.method private static isTestMode()Z
    .locals 2

    .prologue
    .line 370
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/samsung/android/app/headlines/UpgradeManager;->PD_TEST_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 372
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 376
    :goto_0
    return v1

    .line 374
    :catch_0
    move-exception v1

    .line 376
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static updateCheck(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    new-instance v0, Lcom/samsung/android/app/headlines/UpgradeManager;

    invoke-direct {v0}, Lcom/samsung/android/app/headlines/UpgradeManager;-><init>()V

    .line 125
    .local v0, "mgr":Lcom/samsung/android/app/headlines/UpgradeManager;
    invoke-static {p0}, Lcom/samsung/android/internal/headlines/HeadlinesConnection;->isConnected(Landroid/content/Context;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 126
    new-instance v1, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;

    invoke-direct {v1, p0, v0}, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;-><init>(Landroid/content/Context;Lcom/samsung/android/app/headlines/UpgradeManager;)V

    .line 127
    .local v1, "thread":Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;
    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;->start()V

    .line 129
    .end local v1    # "thread":Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;
    :cond_0
    return-void
.end method
