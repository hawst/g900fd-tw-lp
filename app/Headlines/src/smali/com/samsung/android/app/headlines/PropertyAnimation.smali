.class public Lcom/samsung/android/app/headlines/PropertyAnimation;
.super Landroid/view/animation/Animation;
.source "PropertyAnimation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;
    }
.end annotation


# instance fields
.field private end:F

.field private listener:Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;

.field private now:F

.field private start:F


# direct methods
.method public constructor <init>(FFLcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;)V
    .locals 2
    .param p1, "start"    # F
    .param p2, "end"    # F
    .param p3, "listener"    # Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;

    .prologue
    const/4 v1, 0x0

    .line 15
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->listener:Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;

    .line 12
    iput v1, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->start:F

    .line 13
    iput v1, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->end:F

    .line 14
    iput v1, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->now:F

    .line 16
    iput p1, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->start:F

    .line 17
    iput p2, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->end:F

    .line 18
    iput-object p3, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->listener:Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;

    .line 19
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 44
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    .line 45
    iget v1, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->end:F

    iget v2, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->start:F

    sub-float v0, v1, v2

    .line 46
    .local v0, "gap":F
    iget v1, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->start:F

    mul-float v2, p1, v0

    add-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->now:F

    .line 47
    iget-object v1, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->listener:Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->listener:Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;

    iget v2, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->now:F

    invoke-interface {v1, p0, v2}, Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;->onPropertyChanged(Lcom/samsung/android/app/headlines/PropertyAnimation;F)V

    .line 50
    :cond_0
    return-void
.end method

.method public getEnd()F
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->end:F

    return v0
.end method

.method public getNow()F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->now:F

    return v0
.end method

.method public getStart()F
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->start:F

    return v0
.end method

.method public setEnd(F)V
    .locals 0
    .param p1, "end"    # F

    .prologue
    .line 28
    iput p1, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->end:F

    .line 29
    return-void
.end method

.method public setListener(Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->listener:Lcom/samsung/android/app/headlines/PropertyAnimation$PropertyListener;

    .line 40
    return-void
.end method

.method public setStart(F)V
    .locals 0
    .param p1, "start"    # F

    .prologue
    .line 21
    iput p1, p0, Lcom/samsung/android/app/headlines/PropertyAnimation;->start:F

    .line 22
    return-void
.end method
