.class public Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;
.super Landroid/widget/RelativeLayout;
.source "HeadlinesSettingAutoRefreshView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "HeadlinesSettingAutoRefreshView"


# instance fields
.field private mAlreadyConfirm:Z

.field private mCheckButton:Landroid/widget/CheckBox;

.field private mContext:Landroid/content/Context;

.field private mDoNotShow:Z

.field private mRefresh:Z

.field private mShowPopup:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 33
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    .line 34
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mRefresh:Z

    .line 35
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mDoNotShow:Z

    .line 36
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;

    .line 37
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mAlreadyConfirm:Z

    .line 38
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mShowPopup:Z

    .line 43
    const-string v0, "HeadlinesSettingAutoRefreshView"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    .line 47
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->init()V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    .line 34
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mRefresh:Z

    .line 35
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mDoNotShow:Z

    .line 36
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;

    .line 37
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mAlreadyConfirm:Z

    .line 38
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mShowPopup:Z

    .line 53
    const-string v0, "HeadlinesSettingAutoRefreshView"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    .line 57
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->init()V

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mRefresh:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mRefresh:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mDoNotShow:Z

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mDoNotShow:Z

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mAlreadyConfirm:Z

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mAlreadyConfirm:Z

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->dialogAutoRefreshAlertPopup()V

    return-void
.end method

.method static synthetic access$602(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mShowPopup:Z

    return p1
.end method

.method private dialogAutoRefreshAlertPopup()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 169
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 170
    .local v2, "dialog":Landroid/app/AlertDialog$Builder;
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 171
    .local v0, "adbInflater":Landroid/view/LayoutInflater;
    const v4, 0x7f03000e

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 173
    .local v3, "eulaLayout":Landroid/view/View;
    const v4, 0x7f0c0029

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 174
    .local v1, "checkBox":Landroid/widget/CheckBox;
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 175
    new-instance v4, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$5;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$5;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)V

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 183
    const-string v4, "CN"

    invoke-static {v4}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 184
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090057

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 185
    const-string v4, "VZW"

    invoke-static {v4}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isOperatorModel(Ljava/lang/String;)Z

    move-result v4

    if-ne v4, v6, :cond_0

    .line 186
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09005f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 196
    :goto_0
    const v4, 0x104000a

    new-instance v5, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$6;

    invoke-direct {v5, p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$6;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 218
    const/high16 v4, 0x1040000

    new-instance v5, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$7;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$7;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 224
    new-instance v4, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$8;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$8;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)V

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 230
    new-instance v4, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$9;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$9;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)V

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 237
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 238
    iput-boolean v6, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mShowPopup:Z

    .line 239
    return-void

    .line 188
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09004d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 191
    :cond_1
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 192
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090028

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 193
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090029

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private init()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 61
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 62
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030010

    invoke-virtual {v0, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 64
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;

    const-string v4, "mymagazine"

    invoke-virtual {v3, v4, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 65
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v3, "AUTO_REFRESH_ON"

    invoke-interface {v1, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mRefresh:Z

    .line 66
    const-string v3, "AUTO_REFRESH_POPUP_SHOW"

    invoke-interface {v1, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mDoNotShow:Z

    .line 67
    const-string v3, "HeadlinesSettingAutoRefreshView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mRefresh : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mRefresh:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const v3, 0x7f0c0030

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;

    .line 70
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;

    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 71
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;

    iget-boolean v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mRefresh:Z

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 72
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setImportantForAccessibility(I)V

    .line 74
    const v3, 0x7f0c002f

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 76
    .local v2, "refreshView":Landroid/widget/RelativeLayout;
    new-instance v3, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$1;

    invoke-direct {v3, p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$1;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    new-instance v3, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$2;

    invoke-direct {v3, p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$2;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 135
    new-instance v3, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$3;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$3;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 144
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;

    new-instance v4, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 165
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 244
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mShowPopup:Z

    if-nez v0, :cond_0

    .line 245
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 247
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
