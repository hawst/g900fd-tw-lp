.class Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;
.super Ljava/lang/Object;
.source "HeadlinesSettingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ItemInfo"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HeadlinesItemInfo"


# instance fields
.field private mCardInfo:Lcom/samsung/android/magazine/cardchannel/CardInfo;

.field private mChecked:Z

.field private mImageResId:I

.field private mName:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;)V
    .locals 1
    .param p2, "another"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;

    .prologue
    .line 478
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mImageResId:I

    .line 479
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mName:Ljava/lang/String;

    .line 480
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mCardInfo:Lcom/samsung/android/magazine/cardchannel/CardInfo;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mCardInfo:Lcom/samsung/android/magazine/cardchannel/CardInfo;

    .line 481
    iget-boolean v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mChecked:Z

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mChecked:Z

    .line 482
    iget v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mImageResId:I

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mImageResId:I

    .line 483
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Lcom/samsung/android/magazine/cardchannel/CardInfo;Z)V
    .locals 1
    .param p2, "cardInfo"    # Lcom/samsung/android/magazine/cardchannel/CardInfo;
    .param p3, "checked"    # Z

    .prologue
    .line 454
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mImageResId:I

    .line 455
    invoke-virtual {p2}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mName:Ljava/lang/String;

    .line 456
    iput-object p2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mCardInfo:Lcom/samsung/android/magazine/cardchannel/CardInfo;

    .line 457
    iput-boolean p3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mChecked:Z

    .line 459
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->initIcon()V

    .line 460
    return-void
.end method

.method private initIcon()V
    .locals 8

    .prologue
    .line 463
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mCardInfo:Lcom/samsung/android/magazine/cardchannel/CardInfo;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->getIconUri()Ljava/lang/String;

    move-result-object v0

    .line 464
    .local v0, "iconUri":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 465
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->setIconImage(Landroid/content/Context;Ljava/lang/String;)V

    .line 476
    :goto_0
    return-void

    .line 467
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mCardInfo:Lcom/samsung/android/magazine/cardchannel/CardInfo;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getSettingPreviewIcon(Ljava/lang/String;)I

    move-result v2

    .line 468
    .local v2, "resId":I
    if-gtz v2, :cond_1

    .line 469
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    const-wide/high16 v6, 0x4014000000000000L    # 5.0

    rem-double/2addr v4, v6

    double-to-int v1, v4

    .line 470
    .local v1, "index":I
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mDefaultImage:[I
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$1100(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)[I

    move-result-object v3

    aget v2, v3, v1

    .line 471
    const-string v3, "HeadlinesItemInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initIcon : use default icon : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    .end local v1    # "index":I
    :cond_1
    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mImageResId:I

    goto :goto_0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 498
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mCardInfo:Lcom/samsung/android/magazine/cardchannel/CardInfo;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->getCardProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.app.headlines"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getCardDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 501
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mCardInfo:Lcom/samsung/android/magazine/cardchannel/CardInfo;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getIconUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mCardInfo:Lcom/samsung/android/magazine/cardchannel/CardInfo;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->getIconUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 510
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mImageResId:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 490
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mChecked:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 486
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->mChecked:Z

    .line 487
    return-void
.end method
