.class public Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;
.super Lcom/sec/android/touchwiz/widget/TwListView;
.source "HeadlinesCardListView.java"


# instance fields
.field private mCardViewHeight:I

.field private mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

.field private mHeaderViewHeight:I

.field private mItemCount:I

.field private mItemOffsetY:[I

.field private mScrollRange:I

.field private scrollIsComputed:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwListView;-><init>(Landroid/content/Context;)V

    .line 19
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mScrollRange:I

    .line 20
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mItemCount:I

    .line 22
    const/16 v0, 0x279

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mCardViewHeight:I

    .line 23
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mHeaderViewHeight:I

    .line 25
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->scrollIsComputed:Z

    .line 31
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mContext:Landroid/content/Context;

    .line 33
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->init()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/sec/android/touchwiz/widget/TwListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mScrollRange:I

    .line 20
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mItemCount:I

    .line 22
    const/16 v0, 0x279

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mCardViewHeight:I

    .line 23
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mHeaderViewHeight:I

    .line 25
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->scrollIsComputed:Z

    .line 38
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mContext:Landroid/content/Context;

    .line 40
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->init()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/touchwiz/widget/TwListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mScrollRange:I

    .line 20
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mItemCount:I

    .line 22
    const/16 v0, 0x279

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mCardViewHeight:I

    .line 23
    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mHeaderViewHeight:I

    .line 25
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->scrollIsComputed:Z

    .line 45
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mContext:Landroid/content/Context;

    .line 47
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->init()V

    .line 48
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mCardViewHeight:I

    .line 52
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mHeaderViewHeight:I

    .line 53
    new-instance v0, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;-><init>(Landroid/content/Context;Lcom/sec/android/touchwiz/widget/TwListView;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    .line 54
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setLongClickable(Z)V

    .line 55
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->setCacheColorHint(I)V

    .line 56
    return-void
.end method


# virtual methods
.method public getComputedScrollY()I
    .locals 4

    .prologue
    .line 83
    const/4 v0, 0x0

    .line 85
    .local v0, "nScrollY":I
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 86
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 87
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mItemOffsetY:[I

    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getFirstVisiblePosition()I

    move-result v3

    aget v2, v2, v3

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v0, v2, v3

    .line 90
    :cond_0
    return v0
.end method

.method public getScaledTouchSlop()F
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 111
    const-string v0, "CN"

    invoke-static {v0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    const/16 v0, 0x14

    if-eq p1, v0, :cond_0

    const/16 v0, 0x13

    if-eq p1, v0, :cond_0

    const/16 v0, 0x15

    if-eq p1, v0, :cond_0

    const/16 v0, 0x16

    if-ne p1, v0, :cond_1

    .line 114
    :cond_0
    const/4 v0, 0x0

    .line 117
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sec/android/touchwiz/widget/TwListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public resetScrollRange()I
    .locals 3

    .prologue
    .line 60
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mScrollRange:I

    .line 61
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mItemCount:I

    .line 62
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mItemCount:I

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mItemOffsetY:[I

    .line 64
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mItemCount:I

    if-ge v0, v1, :cond_1

    .line 65
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mItemOffsetY:[I

    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mScrollRange:I

    aput v2, v1, v0

    .line 67
    if-nez v0, :cond_0

    .line 68
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mScrollRange:I

    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mHeaderViewHeight:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mScrollRange:I

    .line 64
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_0
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mScrollRange:I

    iget v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mCardViewHeight:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mScrollRange:I

    goto :goto_1

    .line 73
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->scrollIsComputed:Z

    .line 75
    iget v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mScrollRange:I

    return v1
.end method

.method public scrollYIsComputed()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->scrollIsComputed:Z

    return v0
.end method

.method setDndController(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;)V
    .locals 1
    .param p1, "dndController"    # Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDndController(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;)V

    .line 122
    return-void
.end method

.method setDndListener(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndListener;)V
    .locals 1
    .param p1, "dndListener"    # Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndListener;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDndListener(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndListener;)V

    .line 126
    return-void
.end method

.method setDndMode(Z)V
    .locals 1
    .param p1, "set"    # Z

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDndMode(Z)V

    .line 130
    return-void
.end method

.method public setListItemClickable(Z)V
    .locals 3
    .param p1, "clickable"    # Z

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getCount()I

    move-result v2

    .line 97
    .local v2, "visibleItemCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 98
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesCardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 99
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 100
    invoke-virtual {v1, p1}, Landroid/view/View;->setClickable(Z)V

    .line 97
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 103
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method
