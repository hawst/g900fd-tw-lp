.class Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;
.super Ljava/lang/Object;
.source "HeadlinesCardAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CardEvent"
.end annotation


# instance fields
.field private mCard:Lcom/samsung/android/magazine/cardchannel/Card;

.field private mId:I

.field private mType:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;


# direct methods
.method public constructor <init>(Lcom/samsung/android/magazine/cardchannel/Card;ILcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;)V
    .locals 1
    .param p1, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .param p2, "cardId"    # I
    .param p3, "type"    # Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;

    .prologue
    .line 372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 368
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 369
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;->mId:I

    .line 373
    iput-object p1, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 374
    iput p2, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;->mId:I

    .line 375
    iput-object p3, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;->mType:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;

    .line 376
    return-void
.end method


# virtual methods
.method public getCard()Lcom/samsung/android/magazine/cardchannel/Card;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 383
    iget v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;->mId:I

    return v0
.end method

.method public getType()Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$CardEvent;->mType:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter$COMMAND_TYPE;

    return-object v0
.end method
