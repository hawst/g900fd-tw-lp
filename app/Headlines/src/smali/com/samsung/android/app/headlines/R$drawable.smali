.class public final Lcom/samsung/android/app/headlines/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final add_01:I = 0x7f020000

.field public static final add_02:I = 0x7f020001

.field public static final add_03:I = 0x7f020002

.field public static final add_04:I = 0x7f020003

.field public static final add_05:I = 0x7f020004

.field public static final art_culture:I = 0x7f020005

.field public static final auto_refresh_background_selector:I = 0x7f020006

.field public static final bg_default:I = 0x7f020007

.field public static final books:I = 0x7f020008

.field public static final business:I = 0x7f020009

.field public static final empty:I = 0x7f02000a

.field public static final english_content:I = 0x7f02000b

.field public static final food_dining:I = 0x7f02000c

.field public static final gallery_btn_check_off:I = 0x7f02000d

.field public static final gallery_btn_check_on:I = 0x7f02000e

.field public static final gradation:I = 0x7f02000f

.field public static final header_menu_background_selector:I = 0x7f020010

.field public static final header_menu_src_selector:I = 0x7f020011

.field public static final headline_social_ic_500px:I = 0x7f020012

.field public static final headline_social_ic_flickr:I = 0x7f020013

.field public static final headline_social_ic_google:I = 0x7f020014

.field public static final headline_social_ic_linkedln:I = 0x7f020015

.field public static final headline_social_ic_renren:I = 0x7f020016

.field public static final headline_social_ic_tumblr:I = 0x7f020017

.field public static final headline_social_ic_twitter:I = 0x7f020018

.field public static final headline_social_ic_weibo:I = 0x7f020019

.field public static final headline_social_ic_youtube:I = 0x7f02001a

.field public static final headline_time_divider_01:I = 0x7f02001b

.field public static final headline_time_divider_02:I = 0x7f02001c

.field public static final ic_launcher:I = 0x7f02001d

.field public static final living:I = 0x7f02001e

.field public static final local:I = 0x7f02001f

.field public static final magazine_bg_img:I = 0x7f020020

.field public static final magazine_btn_reflash_01:I = 0x7f020021

.field public static final magazine_btn_reflash_01_press:I = 0x7f020022

.field public static final magazine_btn_reflash_02:I = 0x7f020023

.field public static final magazine_btn_reflash_02_press:I = 0x7f020024

.field public static final magazine_ic_welcome:I = 0x7f020025

.field public static final magazine_refresh:I = 0x7f020026

.field public static final magazine_setup_btn_arrow_disabled:I = 0x7f020027

.field public static final magazine_setup_btn_arrow_focused:I = 0x7f020028

.field public static final magazine_setup_btn_arrow_focused_pressed:I = 0x7f020029

.field public static final magazine_setup_btn_arrow_normal:I = 0x7f02002a

.field public static final magazine_setup_btn_arrow_pressed:I = 0x7f02002b

.field public static final menu_btn:I = 0x7f02002c

.field public static final menu_btn_press:I = 0x7f02002d

.field public static final mg_500px_preview:I = 0x7f02002e

.field public static final mg_arts_preview:I = 0x7f02002f

.field public static final mg_books_preview:I = 0x7f020030

.field public static final mg_business_preview:I = 0x7f020031

.field public static final mg_english_preview:I = 0x7f020032

.field public static final mg_filckr_preview:I = 0x7f020033

.field public static final mg_food_preview:I = 0x7f020034

.field public static final mg_google_preview:I = 0x7f020035

.field public static final mg_linkeln_preview:I = 0x7f020036

.field public static final mg_living_preview:I = 0x7f020037

.field public static final mg_local_preview:I = 0x7f020038

.field public static final mg_music_preview:I = 0x7f020039

.field public static final mg_netherlands_preview:I = 0x7f02003a

.field public static final mg_new_preview:I = 0x7f02003b

.field public static final mg_news_preview:I = 0x7f02003c

.field public static final mg_photo_preview:I = 0x7f02003d

.field public static final mg_picks_preview:I = 0x7f02003e

.field public static final mg_renren_preview:I = 0x7f02003f

.field public static final mg_sina_weibo_preview:I = 0x7f020040

.field public static final mg_sports_preview:I = 0x7f020041

.field public static final mg_style_preview:I = 0x7f020042

.field public static final mg_tech_preview:I = 0x7f020043

.field public static final mg_travel_preview:I = 0x7f020044

.field public static final mg_tumblr_preview:I = 0x7f020045

.field public static final mg_twitter_preview:I = 0x7f020046

.field public static final mg_uk_preview:I = 0x7f020047

.field public static final mg_youtube_preview:I = 0x7f020048

.field public static final mm_refresh_b:I = 0x7f020049

.field public static final mm_refresh_w:I = 0x7f02004a

.field public static final music:I = 0x7f02004b

.field public static final my_magazine_blank_bg:I = 0x7f02004c

.field public static final my_magazine_header:I = 0x7f02004d

.field public static final netherland:I = 0x7f02004e

.field public static final new_noteworthy:I = 0x7f02004f

.field public static final news:I = 0x7f020050

.field public static final photos_design:I = 0x7f020051

.field public static final setting_checkbox_background_selector:I = 0x7f020052

.field public static final setting_checkbox_selector:I = 0x7f020053

.field public static final setting_expandable_group_button:I = 0x7f020054

.field public static final sports:I = 0x7f020055

.field public static final stroke:I = 0x7f020056

.field public static final style:I = 0x7f020057

.field public static final technology_science:I = 0x7f020058

.field public static final term_of_use_start_button_background_selector:I = 0x7f020059

.field public static final terms_of_use_next_button_selector:I = 0x7f02005a

.field public static final today_pick:I = 0x7f02005b

.field public static final travel:I = 0x7f02005c

.field public static final tw_ab_bottom_transparent_light:I = 0x7f02005d

.field public static final tw_ab_bottom_transparent_mtrl:I = 0x7f02005e

.field public static final tw_action_item_background_focused_holo_light:I = 0x7f02005f

.field public static final tw_action_item_background_pressed_holo_light:I = 0x7f020060

.field public static final tw_action_item_background_selected_holo_light:I = 0x7f020061

.field public static final tw_btn_icon_next_mtrl:I = 0x7f020062

.field public static final tw_checkbox_off_enabled_default_holo_light:I = 0x7f020063

.field public static final tw_checkbox_off_enabled_pressed_holo_light:I = 0x7f020064

.field public static final tw_checkbox_on_enabled_default_holo_light:I = 0x7f020065

.field public static final tw_checkbox_on_enabled_pressed_holo_light:I = 0x7f020066

.field public static final tw_dialog_full_holo_light:I = 0x7f020067

.field public static final tw_divider_ab_holo_light:I = 0x7f020068

.field public static final tw_drawer_list_line_holo_light:I = 0x7f020069

.field public static final tw_expander_close_01_holo_light:I = 0x7f02006a

.field public static final tw_expander_close_mtrl_alpha:I = 0x7f02006b

.field public static final tw_expander_open_01_holo_light:I = 0x7f02006c

.field public static final tw_expander_open_mtrl_alpha:I = 0x7f02006d

.field public static final tw_holo_light_progressbar:I = 0x7f02006e

.field public static final tw_list_divider_holo_light:I = 0x7f02006f

.field public static final tw_list_focused_holo_dark:I = 0x7f020070

.field public static final tw_list_focused_holo_light:I = 0x7f020071

.field public static final tw_list_pressed_holo_dark:I = 0x7f020072

.field public static final tw_list_pressed_holo_light:I = 0x7f020073

.field public static final tw_list_section_divider_index_holo_light:I = 0x7f020074

.field public static final tw_list_selected_holo_light:I = 0x7f020075

.field public static final tw_scrollbar_holo_light:I = 0x7f020076

.field public static final tw_widget_progressbar_holo_light:I = 0x7f020077

.field public static final tw_widget_progressbar_small_effect_holo_light:I = 0x7f020078

.field public static final tw_widget_progressbar_small_holo_light:I = 0x7f020079

.field public static final uk:I = 0x7f02007a

.field public static final welcome_next_button_selector:I = 0x7f02007b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
