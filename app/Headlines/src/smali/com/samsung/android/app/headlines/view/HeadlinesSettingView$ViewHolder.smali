.class Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;
.super Ljava/lang/Object;
.source "HeadlinesSettingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ViewHolder"
.end annotation


# instance fields
.field private checkbox:Landroid/widget/CheckBox;

.field private image:Landroid/widget/ImageView;

.field private text:Landroid/widget/TextView;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->text:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 440
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->text:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$800(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->image:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$802(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 440
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->image:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$900(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$902(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 440
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    return-object p1
.end method
