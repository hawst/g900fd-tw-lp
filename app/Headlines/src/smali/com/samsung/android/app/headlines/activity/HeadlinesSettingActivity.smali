.class public Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;
.super Landroid/app/Activity;
.source "HeadlinesSettingActivity.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mScrollView:Landroid/widget/ScrollView;

.field private mSettingView:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 22
    const-string v0, "HeadlinesSettingActivity"

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 29
    const-string v3, "HeadlinesSettingActivity"

    const-string v4, "onCreate()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v3, 0x7f030002

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->setContentView(I)V

    .line 34
    invoke-static {p0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isOrientationSupport(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 35
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->setRequestedOrientation(I)V

    .line 37
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 38
    .local v0, "actionbar":Landroid/app/ActionBar;
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 39
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 40
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 41
    const v3, 0x7f090013

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setTitle(I)V

    .line 47
    const v3, 0x7f0c0007

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    iput-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->mSettingView:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    .line 48
    const v3, 0x7f0c0006

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    iput-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->mScrollView:Landroid/widget/ScrollView;

    .line 50
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->mScrollView:Landroid/widget/ScrollView;

    new-instance v4, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity$1;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 57
    const-string v3, "mymagazine"

    invoke-virtual {p0, v3, v6}, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 58
    .local v2, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 59
    .local v1, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "ENABLE_NEW_STORY_NOTIFICATION"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 60
    const-string v3, "CUSTOMIZE_SETTING"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 61
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 62
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 66
    const-string v0, "HeadlinesSettingActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 70
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->mSettingView:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->mSettingView:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->changeSubcription()V

    .line 73
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 87
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 94
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 89
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->finish()V

    .line 90
    const/4 v0, 0x1

    goto :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 77
    const-string v0, "HeadlinesSettingActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 80
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->mSettingView:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;->mSettingView:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->update()V

    .line 83
    :cond_0
    return-void
.end method
