.class Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$3;
.super Ljava/lang/Object;
.source "HeadlinesPullToRefreshView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 316
    const-string v0, "HeadlinesPullToRefreshView"

    const-string v1, "onAnimationEnd() : pull to refresh finished."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->changeRefreshState(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;)V

    .line 318
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mContainer:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearAnimation()V

    .line 319
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->mRefreshRunnable:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->access$302(Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 321
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;->setVisibility(I)V

    .line 322
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 312
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 307
    const-string v0, "HeadlinesPullToRefreshView"

    const-string v1, "onAnimationStart(): pull to refresh animation started."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    return-void
.end method
