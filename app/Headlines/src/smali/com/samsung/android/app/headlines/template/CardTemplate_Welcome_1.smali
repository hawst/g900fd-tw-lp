.class public Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;
.super Lcom/samsung/android/app/headlines/template/CardTemplate;
.source "CardTemplate_Welcome_1.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/headlines/template/CardTemplate;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    .line 22
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTemplateType:I

    .line 23
    return-void
.end method


# virtual methods
.method public getCardView(Landroid/view/LayoutInflater;Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;Lcom/samsung/android/magazine/cardchannel/Card;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "viewHolder"    # Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;
    .param p3, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceAsColor"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 32
    iput-object p3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 34
    invoke-virtual {p2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->isEnabled()Z

    move-result v6

    if-ne v6, v8, :cond_3

    .line 35
    iget-object v6, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 36
    iget-object v6, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    iput-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTextView1:Landroid/widget/TextView;

    .line 37
    iget-object v6, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    iput-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTextView2:Landroid/widget/TextView;

    .line 38
    iget-object v6, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageViewBg:Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mImageViewBg:Landroid/widget/ImageView;

    .line 58
    :goto_0
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v6, :cond_2

    .line 60
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTextView1:Landroid/widget/TextView;

    if-eqz v6, :cond_0

    .line 61
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v6}, Lcom/samsung/android/magazine/cardchannel/Card;->getProviderName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.samsung.android.app.headlines"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 62
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v6}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "displayName":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mContext:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getCardDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 64
    .local v3, "localCardName":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTextView1:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    .end local v2    # "displayName":Ljava/lang/String;
    .end local v3    # "localCardName":Ljava/lang/String;
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTextView1:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f07000e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 71
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTextView2:Landroid/widget/TextView;

    if-eqz v6, :cond_1

    .line 72
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mContext:Landroid/content/Context;

    const v7, 0x7f09004b

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 73
    .local v5, "text":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTextView2:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    .end local v5    # "text":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mImageViewBg:Landroid/widget/ImageView;

    if-eqz v6, :cond_2

    .line 77
    const-string v6, "image1"

    invoke-virtual {p3, v6}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardElement(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardElement;

    move-result-object v0

    .line 78
    .local v0, "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    if-eqz v0, :cond_6

    .line 79
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v8, "image1"

    iget-object v9, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mImageViewBg:Landroid/widget/ImageView;

    invoke-static {v6, v7, v8, v9}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 90
    .end local v0    # "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    :cond_2
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTemplateContainer:Landroid/view/ViewGroup;

    return-object v6

    .line 40
    :cond_3
    const v6, 0x7f03001e

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    iput-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 41
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v6, :cond_4

    .line 42
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTemplateContainer:Landroid/view/ViewGroup;

    const v7, 0x7f0c0049

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTextView1:Landroid/widget/TextView;

    .line 43
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTemplateContainer:Landroid/view/ViewGroup;

    const v7, 0x7f0c004b

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTextView2:Landroid/widget/TextView;

    .line 44
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTemplateContainer:Landroid/view/ViewGroup;

    const v7, 0x7f0c0040

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mImageViewBg:Landroid/widget/ImageView;

    .line 50
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v6, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 51
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTextView1:Landroid/widget/TextView;

    iput-object v6, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    .line 52
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTextView2:Landroid/widget/TextView;

    iput-object v6, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    .line 53
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mImageViewBg:Landroid/widget/ImageView;

    iput-object v6, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageViewBg:Landroid/widget/ImageView;

    .line 55
    invoke-virtual {p2, v8}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->setEnabled(Z)V

    goto/16 :goto_0

    .line 66
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mTextView1:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v8, "text1"

    invoke-static {v7, v8}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 81
    .restart local v0    # "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v6}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v1

    .line 82
    .local v1, "cardName":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getWelcomeBackgroundImage(Ljava/lang/String;)I

    move-result v4

    .line 83
    .local v4, "resId":I
    if-lez v4, :cond_2

    .line 84
    iget-object v6, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;->mImageViewBg:Landroid/widget/ImageView;

    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method

.method public getRefreshView()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method public onCardCollapse()Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public onCardExpand()Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method
