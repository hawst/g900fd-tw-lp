.class public Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;
.super Ljava/lang/Object;
.source "HeadlinesCardViewHolder.java"


# instance fields
.field private mEnabled:Z

.field public mImageGradient:Landroid/widget/ImageView;

.field public mImageView1:Landroid/widget/ImageView;

.field public mImageView2:Landroid/widget/ImageView;

.field public mImageViewBg:Landroid/widget/ImageView;

.field public mImageViewMore:Landroid/widget/ImageView;

.field public mImageViewRefresh:Landroid/widget/ImageView;

.field public mTemplateContainer:Landroid/view/ViewGroup;

.field public mTextView1:Landroid/widget/TextView;

.field public mTextView2:Landroid/widget/TextView;

.field public mTextView3:Landroid/widget/TextView;

.field public mTextView4:Landroid/widget/TextView;

.field public mTextView5:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mEnabled:Z

    .line 10
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 11
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    .line 12
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    .line 13
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView3:Landroid/widget/TextView;

    .line 14
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView4:Landroid/widget/TextView;

    .line 15
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView5:Landroid/widget/TextView;

    .line 17
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView1:Landroid/widget/ImageView;

    .line 18
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView2:Landroid/widget/ImageView;

    .line 19
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageGradient:Landroid/widget/ImageView;

    .line 20
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageViewBg:Landroid/widget/ImageView;

    .line 21
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageViewRefresh:Landroid/widget/ImageView;

    .line 22
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageViewMore:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method public isEnabled()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mEnabled:Z

    return v0
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mEnabled:Z

    .line 26
    return-void
.end method
