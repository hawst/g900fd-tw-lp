.class public Lcom/samsung/android/app/headlines/template/CardTemplateFactory;
.super Ljava/lang/Object;
.source "CardTemplateFactory.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static final TEMPLATE:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 10
    const-string v0, "HeadlinesCardTemplateFactory"

    sput-object v0, Lcom/samsung/android/app/headlines/template/CardTemplateFactory;->TAG:Ljava/lang/String;

    .line 12
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "template_1"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "template_2"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "template_3"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "template_4"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "template_5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "template_6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "template_no_content"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "template_welcome_1"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "template_welcome_2"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "template_welcome_3"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "template_not_supported"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/headlines/template/CardTemplateFactory;->TEMPLATE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createTemplate(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)Lcom/samsung/android/app/headlines/template/CardTemplate;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 20
    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardchannel/Card;->getTemplate()Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "template":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardchannel/Card;->getTemplate()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/headlines/template/CardTemplateFactory;->getTemplateType(Ljava/lang/String;)I

    move-result v1

    .line 23
    .local v1, "type":I
    sget-object v2, Lcom/samsung/android/app/headlines/template/CardTemplateFactory;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createTemplate : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    packed-switch v1, :pswitch_data_0

    .line 47
    sget-object v2, Lcom/samsung/android/app/headlines/template/CardTemplateFactory;->TAG:Ljava/lang/String;

    const-string v3, "createTemplate : no template dones\'t exist"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    new-instance v2, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    :goto_0
    return-object v2

    .line 27
    :pswitch_0
    new-instance v2, Lcom/samsung/android/app/headlines/template/CardTemplate_1;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplate_1;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    goto :goto_0

    .line 29
    :pswitch_1
    new-instance v2, Lcom/samsung/android/app/headlines/template/CardTemplate_2;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplate_2;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    goto :goto_0

    .line 31
    :pswitch_2
    new-instance v2, Lcom/samsung/android/app/headlines/template/CardTemplate_3;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplate_3;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    goto :goto_0

    .line 33
    :pswitch_3
    new-instance v2, Lcom/samsung/android/app/headlines/template/CardTemplate_4;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplate_4;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    goto :goto_0

    .line 35
    :pswitch_4
    new-instance v2, Lcom/samsung/android/app/headlines/template/CardTemplate_5;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplate_5;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    goto :goto_0

    .line 37
    :pswitch_5
    new-instance v2, Lcom/samsung/android/app/headlines/template/CardTemplate_6;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplate_6;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    goto :goto_0

    .line 39
    :pswitch_6
    new-instance v2, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplate_NoContent;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    goto :goto_0

    .line 41
    :pswitch_7
    new-instance v2, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_1;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    goto :goto_0

    .line 43
    :pswitch_8
    new-instance v2, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    goto :goto_0

    .line 45
    :pswitch_9
    new-instance v2, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_3;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_3;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    goto :goto_0

    .line 25
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static getTemplateCount()I
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/samsung/android/app/headlines/template/CardTemplateFactory;->TEMPLATE:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public static getTemplateType(Ljava/lang/String;)I
    .locals 4
    .param p0, "template"    # Ljava/lang/String;

    .prologue
    .line 54
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/samsung/android/app/headlines/template/CardTemplateFactory;->TEMPLATE:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 55
    sget-object v1, Lcom/samsung/android/app/headlines/template/CardTemplateFactory;->TEMPLATE:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 54
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    :cond_1
    sget-object v1, Lcom/samsung/android/app/headlines/template/CardTemplateFactory;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTempateType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is invalid template."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const/4 v0, 0x0

    goto :goto_1
.end method
