.class Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;
.super Landroid/widget/ArrayAdapter;
.source "HeadlinesSettingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "HeadlinesSettingGridAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Landroid/content/Context;ILjava/util/List;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 352
    .local p4, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;>;"
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    .line 353
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 354
    iput-object p2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->mContext:Landroid/content/Context;

    .line 356
    const-string v0, "HeadlinesSettingView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HeadlinesSettingGridAdapter : size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    return-void
.end method


# virtual methods
.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 425
    int-to-long v0, p1

    return-wide v0
.end method

.method public getSelectedMap()Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 429
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 431
    .local v2, "selectedMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 432
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;

    .line 433
    .local v1, "item":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;
    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->isChecked()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 436
    .end local v1    # "item":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;
    :cond_0
    return-object v2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 362
    const/4 v3, 0x0

    .line 363
    .local v3, "viewHolder":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;
    instance-of v4, p2, Landroid/view/View;

    if-eqz v4, :cond_0

    .line 364
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "viewHolder":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;
    check-cast v3, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;

    .line 365
    .restart local v3    # "viewHolder":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;
    const-string v4, "HeadlinesSettingView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getView() :  convertView "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    :goto_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;

    .line 379
    .local v0, "currentItem":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;
    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->getImageResourceId()I

    move-result v2

    .line 380
    .local v2, "resId":I
    if-lez v2, :cond_1

    .line 381
    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->image:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->access$800(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 387
    :goto_1
    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->checkbox:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->access$900(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->isChecked()Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 388
    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->checkbox:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->access$900(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 389
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    const/16 v4, 0xcc

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 391
    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->checkbox:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->access$900(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;)Landroid/widget/CheckBox;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setImportantForAccessibility(I)V

    .line 393
    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->text:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->access$1000(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    new-instance v4, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter$1;

    invoke-direct {v4, p0, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter$1;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;)V

    invoke-virtual {p2, v4}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 420
    return-object p2

    .line 367
    .end local v0    # "currentItem":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v2    # "resId":I
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mInflater:Landroid/view/LayoutInflater;
    invoke-static {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$700(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030012

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 369
    new-instance v3, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;

    .end local v3    # "viewHolder":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;
    invoke-direct {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;-><init>()V

    .line 370
    .restart local v3    # "viewHolder":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;
    const v4, 0x7f0c0034

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    # setter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->image:Landroid/widget/ImageView;
    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->access$802(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 371
    const v4, 0x7f0c0035

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    # setter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->checkbox:Landroid/widget/CheckBox;
    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->access$902(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 372
    const v4, 0x7f0c0036

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    # setter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->text:Landroid/widget/TextView;
    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->access$1002(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 374
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 383
    .restart local v0    # "currentItem":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;
    .restart local v2    # "resId":I
    :cond_1
    const-string v4, "HeadlinesSettingView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setCardNameIcon : get bitmap "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->getIconUri()Ljava/lang/String;

    move-result-object v5

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->image:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;->access$800(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getIconImage(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V

    goto/16 :goto_1
.end method
