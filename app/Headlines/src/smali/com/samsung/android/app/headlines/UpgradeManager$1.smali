.class Lcom/samsung/android/app/headlines/UpgradeManager$1;
.super Landroid/os/Handler;
.source "UpgradeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/UpgradeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field final synthetic this$0:Lcom/samsung/android/app/headlines/UpgradeManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/UpgradeManager;)V
    .locals 1

    .prologue
    .line 99
    iput-object p1, p0, Lcom/samsung/android/app/headlines/UpgradeManager$1;->this$0:Lcom/samsung/android/app/headlines/UpgradeManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/UpgradeManager$1;->context:Landroid/content/Context;

    return-void
.end method

.method private showDialog(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 113
    if-nez p1, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 116
    iget-object v0, p0, Lcom/samsung/android/app/headlines/UpgradeManager$1;->this$0:Lcom/samsung/android/app/headlines/UpgradeManager;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/UpgradeManager$1;->context:Landroid/content/Context;

    # invokes: Lcom/samsung/android/app/headlines/UpgradeManager;->dialogCritialUpdate(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/headlines/UpgradeManager;->access$400(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;)V

    goto :goto_0

    .line 117
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/headlines/UpgradeManager$1;->this$0:Lcom/samsung/android/app/headlines/UpgradeManager;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/UpgradeManager$1;->context:Landroid/content/Context;

    # invokes: Lcom/samsung/android/app/headlines/UpgradeManager;->dialogNormalUpdate(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/headlines/UpgradeManager;->access$500(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 104
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/content/Context;

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/UpgradeManager$1;->context:Landroid/content/Context;

    .line 106
    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v1}, Lcom/samsung/android/app/headlines/UpgradeManager$1;->showDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
