.class public Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldCipher;
.super Ljava/lang/Object;
.source "HeadlinesGldCipher.java"


# static fields
.field private static final ALGORITHM:Ljava/lang/String; = "AES"

.field private static final secretkeyValue:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldCipher;->secretkeyValue:[B

    return-void

    :array_0
    .array-data 1
        0x41t
        0x6ct
        0x57t
        0x61t
        0x59t
        0x73t
        0x32t
        0x30t
        0x31t
        0x33t
        0x29t
        0x23t
        0x21t
        0x40t
        0x48t
        0x44t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decrypt(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 29
    invoke-static {}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldCipher;->generateKey()Ljava/security/Key;

    move-result-object v4

    .line 30
    .local v4, "key":Ljava/security/Key;
    const-string v5, "AES"

    invoke-static {v5}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 31
    .local v0, "c":Ljavax/crypto/Cipher;
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 33
    const/4 v5, 0x0

    invoke-static {p0, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 34
    .local v2, "decString":[B
    invoke-virtual {v0, v2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v3

    .line 35
    .local v3, "decValue":[B
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>([B)V

    .line 37
    .local v1, "dValue":Ljava/lang/String;
    return-object v1
.end method

.method public static encrypt(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 19
    invoke-static {}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldCipher;->generateKey()Ljava/security/Key;

    move-result-object v3

    .line 20
    .local v3, "key":Ljava/security/Key;
    const-string v4, "AES"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 21
    .local v0, "c":Ljavax/crypto/Cipher;
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 23
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v2

    .line 24
    .local v2, "encValue":[B
    const/4 v4, 0x0

    invoke-static {v2, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 25
    .local v1, "eValue":Ljava/lang/String;
    return-object v1
.end method

.method private static generateKey()Ljava/security/Key;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    sget-object v1, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesGldCipher;->secretkeyValue:[B

    const-string v2, "AES"

    invoke-direct {v0, v1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 42
    .local v0, "key":Ljava/security/Key;
    return-object v0
.end method
