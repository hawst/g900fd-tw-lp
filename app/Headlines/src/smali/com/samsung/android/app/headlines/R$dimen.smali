.class public final Lcom/samsung/android/app/headlines/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final about_button_height:I = 0x7f080064

.field public static final about_button_text_size:I = 0x7f080063

.field public static final about_button_width:I = 0x7f080062

.field public static final about_description_button_gap:I = 0x7f080068

.field public static final about_icon_description_gap:I = 0x7f080067

.field public static final about_layout_marginLeft:I = 0x7f08005f

.field public static final about_text_size:I = 0x7f080061

.field public static final about_title_icon_gap:I = 0x7f080066

.field public static final about_title_marginTop:I = 0x7f080065

.field public static final about_title_text_size:I = 0x7f080060

.field public static final activity_horizontal_margin:I = 0x7f080000

.field public static final activity_vertical_margin:I = 0x7f080001

.field public static final author_divider_height:I = 0x7f080020

.field public static final author_divider_width:I = 0x7f08001f

.field public static final author_image_height_size:I = 0x7f080085

.field public static final author_image_margin_bottom:I = 0x7f08001a

.field public static final author_image_width_size:I = 0x7f080084

.field public static final author_layout_height:I = 0x7f080016

.field public static final author_layout_margin_bottom:I = 0x7f080017

.field public static final author_layout_margin_right:I = 0x7f080018

.field public static final author_layout_margin_top:I = 0x7f08001c

.field public static final author_name_margin_left:I = 0x7f080019

.field public static final author_name_margin_right:I = 0x7f08001b

.field public static final author_size:I = 0x7f080015

.field public static final author_text_height:I = 0x7f08001e

.field public static final author_text_size:I = 0x7f08001d

.field public static final body_layout_margin_bottom:I = 0x7f08000a

.field public static final body_layout_margin_right:I = 0x7f080008

.field public static final body_layout_margin_top:I = 0x7f080009

.field public static final body_text_margin_right:I = 0x7f080027

.field public static final body_text_margin_top:I = 0x7f080026

.field public static final body_text_size:I = 0x7f080025

.field public static final category_text_height:I = 0x7f08000c

.field public static final category_text_margin_bottom:I = 0x7f080012

.field public static final category_text_margin_top:I = 0x7f080013

.field public static final category_text_padding_right:I = 0x7f080014

.field public static final category_text_size:I = 0x7f08000b

.field public static final custom_actionbar_divider_height:I = 0x7f08007c

.field public static final custom_actionbar_divider_width:I = 0x7f08007b

.field public static final do_not_show_checkbox_marginBottom:I = 0x7f08005b

.field public static final do_not_show_checkbox_marginLeft:I = 0x7f080058

.field public static final do_not_show_checkbox_marginRight:I = 0x7f080059

.field public static final do_not_show_checkbox_marginTop:I = 0x7f08005a

.field public static final do_not_show_checkbox_paddingLeft:I = 0x7f08005c

.field public static final do_not_show_checkbox_textSize:I = 0x7f08005d

.field public static final full_welcome_app_name:I = 0x7f08006a

.field public static final full_welcome_title_size:I = 0x7f080069

.field public static final gradient_bottom_height:I = 0x7f080005

.field public static final gradient_top_height:I = 0x7f080004

.field public static final header_height:I = 0x7f080040

.field public static final header_more_button_marginTop:I = 0x7f080046

.field public static final header_more_button_padding:I = 0x7f080045

.field public static final header_title_marginBottom:I = 0x7f080044

.field public static final header_title_marginLeft:I = 0x7f080041

.field public static final header_title_marginRight:I = 0x7f080042

.field public static final header_title_marginTop:I = 0x7f080043

.field public static final header_view_height:I = 0x7f080099

.field public static final header_view_padding_left:I = 0x7f08009b

.field public static final header_view_padding_right:I = 0x7f08009c

.field public static final header_view_padding_top:I = 0x7f08009a

.field public static final image_and_text_layout_image_width:I = 0x7f080010

.field public static final image_layout_weight_2nd:I = 0x7f080011

.field public static final item_subtitle_size:I = 0x7f08003e

.field public static final layout_height:I = 0x7f080003

.field public static final layout_margin:I = 0x7f080007

.field public static final list_header_view_height:I = 0x7f080080

.field public static final loading_text_size:I = 0x7f080002

.field public static final main_header_title_size:I = 0x7f08003f

.field public static final main_image_height_size:I = 0x7f080083

.field public static final main_image_width_size:I = 0x7f080082

.field public static final max_author_image_height_size:I = 0x7f080089

.field public static final max_author_image_width_size:I = 0x7f080088

.field public static final max_main_image_height_size:I = 0x7f080087

.field public static final max_main_image_width_size:I = 0x7f080086

.field public static final min_length_x_for_launching_home:I = 0x7f08003c

.field public static final min_length_y_for_launching_home:I = 0x7f08003d

.field public static final notification_text_size:I = 0x7f08008a

.field public static final pull_to_refresh_back_height:I = 0x7f08007f

.field public static final pull_to_refresh_maximum_height:I = 0x7f08007d

.field public static final pull_to_refresh_state_height:I = 0x7f080081

.field public static final pull_to_refresh_turning_height:I = 0x7f08007e

.field public static final refresh_setting_icon_margin_bottom:I = 0x7f08000f

.field public static final refresh_setting_icon_margin_left:I = 0x7f08000e

.field public static final refresh_setting_icon_margin_right:I = 0x7f08000d

.field public static final refresh_setting_icon_size:I = 0x7f080006

.field public static final setting_auto_refresh_detail_size:I = 0x7f080048

.field public static final setting_auto_refresh_title_size:I = 0x7f080047

.field public static final setting_section_item_checkbox_marginBottom:I = 0x7f080056

.field public static final setting_section_item_checkbox_marginRight:I = 0x7f080057

.field public static final setting_section_item_checkbox_size:I = 0x7f080055

.field public static final setting_section_item_image_marginBottom:I = 0x7f080054

.field public static final setting_section_item_image_size:I = 0x7f080053

.field public static final setting_section_item_padding:I = 0x7f080052

.field public static final setting_section_item_text_size:I = 0x7f080050

.field public static final setting_section_item_width:I = 0x7f080051

.field public static final setting_section_list_paddingBottom:I = 0x7f08004e

.field public static final setting_section_list_paddingTop:I = 0x7f08004d

.field public static final setting_section_list_verticalSpacing:I = 0x7f08004f

.field public static final setting_section_title_height:I = 0x7f08004a

.field public static final setting_section_title_paddingLeft:I = 0x7f08004b

.field public static final setting_section_title_paddingRight:I = 0x7f08004c

.field public static final setting_section_title_text_size:I = 0x7f080049

.field public static final terms_checkbox_marginBottom:I = 0x7f080072

.field public static final terms_checkbox_marginLeft:I = 0x7f08006f

.field public static final terms_checkbox_marginRight:I = 0x7f080070

.field public static final terms_checkbox_marginTop:I = 0x7f080071

.field public static final terms_checkbox_paddingLeft:I = 0x7f080073

.field public static final terms_checkbox_textSize:I = 0x7f080074

.field public static final terms_description_width:I = 0x7f08007a

.field public static final terms_margin:I = 0x7f08006d

.field public static final terms_scroll_height:I = 0x7f08006e

.field public static final terms_start_height:I = 0x7f080079

.field public static final terms_start_text_size:I = 0x7f080078

.field public static final terms_text_marginRight:I = 0x7f080075

.field public static final terms_text_size:I = 0x7f080076

.field public static final terms_title_text_gap:I = 0x7f080077

.field public static final terms_top_margin:I = 0x7f08006b

.field public static final terms_top_margin_2:I = 0x7f08006c

.field public static final title_text_margin_bottom:I = 0x7f080021

.field public static final title_text_margin_bottom_2nd:I = 0x7f080022

.field public static final title_text_margin_right:I = 0x7f080023

.field public static final title_text_size:I = 0x7f080024

.field public static final welcome_body_text_height:I = 0x7f08003a

.field public static final welcome_body_text_martin_bottom:I = 0x7f080038

.field public static final welcome_body_text_martin_left:I = 0x7f080037

.field public static final welcome_body_text_padding_left:I = 0x7f080039

.field public static final welcome_body_text_size:I = 0x7f080035

.field public static final welcome_body_text_width:I = 0x7f080036

.field public static final welcome_category_text_size:I = 0x7f080033

.field public static final welcome_description_marginRight:I = 0x7f080090

.field public static final welcome_description_marginTop:I = 0x7f08008f

.field public static final welcome_description_text_size:I = 0x7f080092

.field public static final welcome_description_title_text_gap:I = 0x7f080091

.field public static final welcome_height:I = 0x7f080029

.field public static final welcome_icon_description_gap:I = 0x7f08008e

.field public static final welcome_icon_size:I = 0x7f08008d

.field public static final welcome_layout_margin_bottom:I = 0x7f08002b

.field public static final welcome_layout_margin_left:I = 0x7f08002c

.field public static final welcome_layout_margin_top:I = 0x7f08002a

.field public static final welcome_margin_bottom:I = 0x7f08002f

.field public static final welcome_margin_left:I = 0x7f08002d

.field public static final welcome_margin_top:I = 0x7f08002e

.field public static final welcome_next_text_size:I = 0x7f080093

.field public static final welcome_social_icon_margin_right:I = 0x7f080031

.field public static final welcome_social_icon_margin_top:I = 0x7f080032

.field public static final welcome_social_icon_size:I = 0x7f080030

.field public static final welcome_title_icon_gap:I = 0x7f08008c

.field public static final welcome_title_marginTop:I = 0x7f08008b

.field public static final welcome_title_text_margin_bottom:I = 0x7f08003b

.field public static final welcome_title_text_size:I = 0x7f080034

.field public static final welcome_width:I = 0x7f080028

.field public static final winset_button_button_marginBottom:I = 0x7f080097

.field public static final winset_button_button_marginTop:I = 0x7f080096

.field public static final winset_button_soft_2button_gap_margin:I = 0x7f080095

.field public static final winset_button_soft_2button_side_margin:I = 0x7f080094

.field public static final winset_singleline_list_layout_minHeight:I = 0x7f080098

.field public static final worldclock_reorder_grab_padding:I = 0x7f08005e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
