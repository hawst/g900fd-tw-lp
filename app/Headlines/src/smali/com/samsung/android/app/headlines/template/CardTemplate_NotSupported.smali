.class public Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;
.super Lcom/samsung/android/app/headlines/template/CardTemplate;
.source "CardTemplate_NotSupported.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/headlines/template/CardTemplate;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    .line 20
    iput-object p1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mContext:Landroid/content/Context;

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTemplateType:I

    .line 23
    return-void
.end method


# virtual methods
.method public getCardView(Landroid/view/LayoutInflater;Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;Lcom/samsung/android/magazine/cardchannel/Card;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "viewHolder"    # Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;
    .param p3, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceAsColor"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 34
    iput-object p3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 36
    invoke-virtual {p2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->isEnabled()Z

    move-result v0

    if-ne v0, v2, :cond_2

    .line 37
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 38
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTextView1:Landroid/widget/TextView;

    .line 39
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageViewBg:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mImageViewBg:Landroid/widget/ImageView;

    .line 56
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTextView1:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTextView1:Landroid/widget/TextView;

    const-string v1, "Not Supported Template"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mImageViewBg:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mImageViewBg:Landroid/widget/ImageView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTemplateContainer:Landroid/view/ViewGroup;

    return-object v0

    .line 41
    :cond_2
    const v0, 0x7f03001d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 42
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c0049

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTextView1:Landroid/widget/TextView;

    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c0040

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mImageViewBg:Landroid/widget/ImageView;

    .line 49
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mTextView1:Landroid/widget/TextView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_NotSupported;->mImageViewBg:Landroid/widget/ImageView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageViewBg:Landroid/widget/ImageView;

    .line 53
    invoke-virtual {p2, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->setEnabled(Z)V

    goto :goto_0
.end method

.method public onCardCollapse()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public onCardExpand()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method
