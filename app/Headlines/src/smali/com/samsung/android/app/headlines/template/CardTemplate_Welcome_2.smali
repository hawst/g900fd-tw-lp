.class public Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;
.super Lcom/samsung/android/app/headlines/template/CardTemplate;
.source "CardTemplate_Welcome_2.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/headlines/template/CardTemplate;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    .line 24
    iput-object p1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mContext:Landroid/content/Context;

    .line 26
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTemplateType:I

    .line 27
    return-void
.end method


# virtual methods
.method public getCardView(Landroid/view/LayoutInflater;Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;Lcom/samsung/android/magazine/cardchannel/Card;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "viewHolder"    # Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;
    .param p3, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceAsColor"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 36
    iput-object p3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 38
    invoke-virtual {p2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->isEnabled()Z

    move-result v4

    if-ne v4, v6, :cond_4

    .line 39
    iget-object v4, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 40
    iget-object v4, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView1:Landroid/widget/TextView;

    .line 41
    iget-object v4, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView2:Landroid/widget/TextView;

    .line 42
    iget-object v4, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView3:Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView3:Landroid/widget/TextView;

    .line 43
    iget-object v4, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView1:Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mImageView1:Landroid/widget/ImageView;

    .line 44
    iget-object v4, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageViewBg:Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mImageViewBg:Landroid/widget/ImageView;

    .line 67
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v4, :cond_3

    .line 69
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView1:Landroid/widget/TextView;

    if-eqz v4, :cond_0

    .line 70
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/cardchannel/Card;->getProviderName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.samsung.android.app.headlines"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 71
    invoke-virtual {p3}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "cardName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mContext:Landroid/content/Context;

    invoke-static {v4, v1}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getCardDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 73
    .local v2, "localCardName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView1:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    .end local v1    # "cardName":Ljava/lang/String;
    .end local v2    # "localCardName":Ljava/lang/String;
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView2:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    .line 80
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView2:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mContext:Landroid/content/Context;

    const v6, 0x7f09000e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mImageView1:Landroid/widget/ImageView;

    if-eqz v4, :cond_2

    .line 84
    const-string v4, "image1"

    invoke-virtual {p3, v4}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardElement(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardElement;

    move-result-object v0

    .line 85
    .local v0, "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    if-eqz v0, :cond_7

    .line 86
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v6, "image1"

    iget-object v7, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mImageView1:Landroid/widget/ImageView;

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 96
    .end local v0    # "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    :cond_2
    :goto_2
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mImageViewBg:Landroid/widget/ImageView;

    if-eqz v4, :cond_3

    .line 97
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mImageViewBg:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-static {v5}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getBackgroundColor(Lcom/samsung/android/magazine/cardchannel/Card;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 101
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTemplateContainer:Landroid/view/ViewGroup;

    return-object v4

    .line 46
    :cond_4
    const v4, 0x7f03001f

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 47
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v4, :cond_5

    .line 48
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTemplateContainer:Landroid/view/ViewGroup;

    const v5, 0x7f0c0049

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView1:Landroid/widget/TextView;

    .line 49
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTemplateContainer:Landroid/view/ViewGroup;

    const v5, 0x7f0c004b

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView2:Landroid/widget/TextView;

    .line 50
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTemplateContainer:Landroid/view/ViewGroup;

    const v5, 0x7f0c004f

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mImageView1:Landroid/widget/ImageView;

    .line 51
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTemplateContainer:Landroid/view/ViewGroup;

    const v5, 0x7f0c0040

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mImageViewBg:Landroid/widget/ImageView;

    .line 57
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v4, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 58
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView1:Landroid/widget/TextView;

    iput-object v4, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    .line 59
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView2:Landroid/widget/TextView;

    iput-object v4, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    .line 60
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView3:Landroid/widget/TextView;

    iput-object v4, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView3:Landroid/widget/TextView;

    .line 61
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mImageView1:Landroid/widget/ImageView;

    iput-object v4, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView1:Landroid/widget/ImageView;

    .line 62
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mImageViewBg:Landroid/widget/ImageView;

    iput-object v4, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageViewBg:Landroid/widget/ImageView;

    .line 64
    invoke-virtual {p2, v6}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->setEnabled(Z)V

    goto/16 :goto_0

    .line 75
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mTextView1:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v6, "text1"

    invoke-static {v5, v6}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 88
    .restart local v0    # "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v1

    .line 89
    .restart local v1    # "cardName":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getWelcomeSocialIcon(Ljava/lang/String;)I

    move-result v3

    .line 90
    .local v3, "resId":I
    if-lez v3, :cond_2

    .line 91
    iget-object v4, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_Welcome_2;->mImageView1:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2
.end method

.method public getRefreshView()Z
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method

.method public onCardCollapse()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public onCardExpand()Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method
