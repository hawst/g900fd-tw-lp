.class final enum Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;
.super Ljava/lang/Enum;
.source "HeadlinesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "LogginStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

.field public static final enum INIT:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

.field public static final enum OFF:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

.field public static final enum ON:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    new-instance v0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->INIT:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    new-instance v0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    const-string v1, "ON"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->ON:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    new-instance v0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->OFF:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    sget-object v1, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->INIT:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->ON:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->OFF:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->$VALUES:[Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 57
    const-class v0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->$VALUES:[Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    invoke-virtual {v0}, [Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    return-object v0
.end method
