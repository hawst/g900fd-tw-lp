.class public final enum Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;
.super Ljava/lang/Enum;
.source "HeadlinesPullToRefreshView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PullToRefreshState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

.field public static final enum STATE_INIT:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

.field public static final enum STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

.field public static final enum STATE_PULL:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

.field public static final enum STATE_REFRESHING:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

.field public static final enum STATE_RELEASE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    const-string v1, "STATE_NONE"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    .line 66
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    const-string v1, "STATE_INIT"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_INIT:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    .line 67
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    const-string v1, "STATE_PULL"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_PULL:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    .line 68
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    const-string v1, "STATE_RELEASE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_RELEASE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    .line 69
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    const-string v1, "STATE_REFRESHING"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_REFRESHING:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    .line 64
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_INIT:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_PULL:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_RELEASE:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->STATE_REFRESHING:Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->$VALUES:[Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    const-class v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->$VALUES:[Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    invoke-virtual {v0}, [Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/headlines/view/HeadlinesPullToRefreshView$PullToRefreshState;

    return-object v0
.end method
