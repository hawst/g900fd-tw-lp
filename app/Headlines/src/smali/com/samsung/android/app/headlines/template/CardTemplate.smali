.class public abstract Lcom/samsung/android/app/headlines/template/CardTemplate;
.super Ljava/lang/Object;
.source "CardTemplate.java"

# interfaces
.implements Lcom/samsung/android/app/headlines/template/Template;


# static fields
.field protected static final SELP_STOP_TIMEOUT:J = 0x7d0L

.field protected static final TEMPLATE_ACTION_1:Ljava/lang/String; = "action1"

.field protected static final TEMPLATE_ACTION_2:Ljava/lang/String; = "action2"

.field public static final TEMPLATE_ALWAYS_CARD:I = 0x4

.field protected static final TEMPLATE_IMAGE_1:Ljava/lang/String; = "image1"

.field protected static final TEMPLATE_IMAGE_2:Ljava/lang/String; = "image2"

.field protected static final TEMPLATE_IMAGE_BACKGROUND:Ljava/lang/String; = "background"

.field public static final TEMPLATE_NEWS_CARD:I = 0x2

.field public static final TEMPLATE_NEWS_CARD_ONLY_TEXT:I = 0x3

.field public static final TEMPLATE_NOT_SUPPORTED:I = 0x0

.field public static final TEMPLATE_NO_CONTENT:I = 0x5

.field protected static final TEMPLATE_TEXT_1:Ljava/lang/String; = "text1"

.field protected static final TEMPLATE_TEXT_2:Ljava/lang/String; = "text2"

.field protected static final TEMPLATE_TEXT_3:Ljava/lang/String; = "text3"

.field protected static final TEMPLATE_TEXT_4:Ljava/lang/String; = "text4"

.field protected static final TEMPLATE_TEXT_5:Ljava/lang/String; = "text5"

.field public static final TEMPLATE_WELCOME_CARD:I = 0x1


# instance fields
.field protected mCard:Lcom/samsung/android/magazine/cardchannel/Card;

.field protected mContext:Landroid/content/Context;

.field public mImageGradient:Landroid/widget/ImageView;

.field public mImageView1:Landroid/widget/ImageView;

.field public mImageView2:Landroid/widget/ImageView;

.field public mImageViewBg:Landroid/widget/ImageView;

.field public mTemplateContainer:Landroid/view/ViewGroup;

.field protected mTemplateType:I

.field public mTextView1:Landroid/widget/TextView;

.field public mTextView2:Landroid/widget/TextView;

.field public mTextView3:Landroid/widget/TextView;

.field public mTextView4:Landroid/widget/TextView;

.field public mTextView5:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mContext:Landroid/content/Context;

    .line 29
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 31
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 32
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mTextView1:Landroid/widget/TextView;

    .line 33
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mTextView2:Landroid/widget/TextView;

    .line 34
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mTextView3:Landroid/widget/TextView;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mTextView4:Landroid/widget/TextView;

    .line 36
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mTextView5:Landroid/widget/TextView;

    .line 38
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mImageView1:Landroid/widget/ImageView;

    .line 39
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mImageView2:Landroid/widget/ImageView;

    .line 40
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mImageViewBg:Landroid/widget/ImageView;

    .line 41
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mImageGradient:Landroid/widget/ImageView;

    .line 50
    iput v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mTemplateType:I

    .line 53
    iput-object p1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mContext:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 56
    iput v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mTemplateType:I

    .line 57
    return-void
.end method


# virtual methods
.method public abstract getCardView(Landroid/view/LayoutInflater;Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;Lcom/samsung/android/magazine/cardchannel/Card;)Landroid/view/View;
.end method

.method public getRefreshView()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    return v0
.end method

.method public getTemplateType()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate;->mTemplateType:I

    return v0
.end method

.method public abstract onCardCollapse()Z
.end method

.method public abstract onCardExpand()Z
.end method
