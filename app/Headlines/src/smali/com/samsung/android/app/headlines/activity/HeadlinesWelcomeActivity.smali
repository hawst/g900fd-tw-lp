.class public Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;
.super Landroid/app/Activity;
.source "HeadlinesWelcomeActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity$FlickGestureListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HeadlinesWelcomeActivity"


# instance fields
.field private MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

.field private final REQUEST_CODE_PRIVACY:I

.field private mGestureDetecter:Landroid/view/GestureDetector;

.field private mLayout:Landroid/widget/LinearLayout;

.field private mLayoutNext:Landroid/widget/LinearLayout;

.field private mTextNext:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 27
    const/16 v0, 0xc8

    iput v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->REQUEST_CODE_PRIVACY:I

    .line 28
    const/16 v0, 0x12c

    iput v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    .line 30
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mLayout:Landroid/widget/LinearLayout;

    .line 31
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mGestureDetecter:Landroid/view/GestureDetector;

    .line 33
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mLayoutNext:Landroid/widget/LinearLayout;

    .line 34
    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mTextNext:Landroid/widget/TextView;

    .line 153
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;

    .prologue
    .line 23
    iget v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    return v0
.end method

.method private endAnimation(Landroid/view/View;)V
    .locals 2
    .param p1, "layout"    # Landroid/view/View;

    .prologue
    .line 121
    const v1, 0x7f040006

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 122
    .local v0, "animation":Landroid/view/animation/Animation;
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 123
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 124
    return-void
.end method

.method private startAnimation(Landroid/view/View;)V
    .locals 2
    .param p1, "layout"    # Landroid/view/View;

    .prologue
    .line 113
    const v1, 0x7f040001

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 114
    .local v0, "animation":Landroid/view/animation/Animation;
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 115
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 116
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 117
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mGestureDetecter:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mGestureDetecter:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 132
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 144
    const-string v0, "HeadlinesWelcomeActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult is called :: requestCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "resultCode =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    .line 147
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 148
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->finish()V

    .line 151
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 137
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 138
    const v0, 0x7f040003

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->overridePendingTransition(II)V

    .line 139
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 100
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 101
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0xc8

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 103
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mLayoutNext:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 104
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mLayoutNext:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mTextNext:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 108
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mTextNext:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const v5, 0x7f09004c

    .line 39
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v3, 0x7f030005

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->setContentView(I)V

    .line 42
    invoke-static {p0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isOrientationSupport(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 43
    invoke-virtual {p0, v6}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->setRequestedOrientation(I)V

    .line 45
    :cond_0
    new-instance v3, Landroid/view/GestureDetector;

    new-instance v4, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity$FlickGestureListener;

    invoke-direct {v4, p0, p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity$FlickGestureListener;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;Landroid/content/Context;)V

    invoke-direct {v3, p0, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mGestureDetecter:Landroid/view/GestureDetector;

    .line 47
    const v3, 0x7f0c0014

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 48
    .local v2, "title":Landroid/widget/TextView;
    sget-object v3, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->typefaceSamsungSansBold:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 50
    const v3, 0x7f0c0016

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mLayout:Landroid/widget/LinearLayout;

    .line 51
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mLayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->startAnimation(Landroid/view/View;)V

    .line 53
    const v3, 0x7f0c0018

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 56
    .local v1, "descriptionTextView":Landroid/widget/TextView;
    const-string v3, "CN"

    invoke-static {v3}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 57
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f09004e

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "description":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08003c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    .line 69
    const-string v3, "HeadlinesWelcomeActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "init() : MIN_LENGTH_X_FOR_LAUNCHING_HOME = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    return-void

    .line 59
    .end local v0    # "description":Ljava/lang/String;
    :cond_1
    const-string v3, "VZW"

    invoke-static {v3}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isOperatorModel(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v6, :cond_2

    .line 60
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f09005f

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "description":Ljava/lang/String;
    goto :goto_0

    .line 62
    .end local v0    # "description":Ljava/lang/String;
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f09004d

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "description":Ljava/lang/String;
    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 74
    const v1, 0x7f0c0019

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mLayoutNext:Landroid/widget/LinearLayout;

    .line 75
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mLayoutNext:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 76
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mLayoutNext:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    :cond_0
    const v1, 0x7f0c001a

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mTextNext:Landroid/widget/TextView;

    .line 80
    const v1, 0x7f090044

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "next":Ljava/lang/String;
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mTextNext:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mTextNext:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 84
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->mTextNext:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 88
    return-void
.end method
