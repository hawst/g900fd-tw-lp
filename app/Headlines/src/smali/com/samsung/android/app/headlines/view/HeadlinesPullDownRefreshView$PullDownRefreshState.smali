.class public final enum Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;
.super Ljava/lang/Enum;
.source "HeadlinesPullDownRefreshView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PullDownRefreshState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

.field public static final enum STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

.field public static final enum STATE_READY:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

.field public static final enum STATE_REFRESH:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

.field public static final enum STATE_REFRESH_NOT_REQUIRED:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

.field public static final enum STATE_REFRESH_REQUIRED:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    const-string v1, "STATE_NONE"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    .line 43
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    const-string v1, "STATE_READY"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_READY:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    .line 44
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    const-string v1, "STATE_REFRESH_REQUIRED"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_REFRESH_REQUIRED:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    .line 45
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    const-string v1, "STATE_REFRESH_NOT_REQUIRED"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_REFRESH_NOT_REQUIRED:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    .line 46
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    const-string v1, "STATE_REFRESH"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_REFRESH:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    .line 41
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_NONE:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_READY:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_REFRESH_REQUIRED:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_REFRESH_NOT_REQUIRED:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->STATE_REFRESH:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->$VALUES:[Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-class v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->$VALUES:[Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    invoke-virtual {v0}, [Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$PullDownRefreshState;

    return-object v0
.end method
