.class public Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;
.super Lcom/sec/android/touchwiz/widget/TwGridView;
.source "HeadlinesContentListView.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;


# instance fields
.field private mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;

.field private mIsBeingDragged:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwGridView;-><init>(Landroid/content/Context;)V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mIsBeingDragged:Z

    .line 22
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->init()V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/sec/android/touchwiz/widget/TwGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mIsBeingDragged:Z

    .line 27
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->init()V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/touchwiz/widget/TwGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mIsBeingDragged:Z

    .line 32
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mContext:Landroid/content/Context;

    .line 34
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->init()V

    .line 35
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 51
    new-instance v0, Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;

    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;-><init>(Landroid/content/Context;Lcom/sec/android/touchwiz/widget/TwGridView;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;

    .line 52
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setOverScrollMode(I)V

    .line 53
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setLongClickable(Z)V

    .line 54
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;

    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08005e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v3, v3, v1, v3}, Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;->setDragGrabHandlePadding(IIII)V

    .line 55
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->setCacheColorHint(I)V

    .line 56
    return-void
.end method


# virtual methods
.method public getTouchSlop()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    return v0
.end method

.method public isBeingDragged()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mIsBeingDragged:Z

    return v0
.end method

.method public onItemLongClick(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJ)Z
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/touchwiz/widget/TwAdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "parent":Lcom/sec/android/touchwiz/widget/TwAdapterView;, "Lcom/sec/android/touchwiz/widget/TwAdapterView<*>;"
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;->isDndMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    const/4 v0, 0x0

    .line 63
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;->onItemLongClick(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJ)Z

    move-result v0

    goto :goto_0
.end method

.method setDndController(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;)V
    .locals 1
    .param p1, "dndController"    # Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;->setDndController(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;)V

    .line 77
    return-void
.end method

.method setDndListener(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndListener;)V
    .locals 1
    .param p1, "dndListener"    # Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndListener;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;->setDndListener(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndListener;)V

    .line 81
    return-void
.end method

.method setDndMode(Z)V
    .locals 1
    .param p1, "set"    # Z

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->mDndAnimator:Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/animator/TwDndGridAnimator;->setDndMode(Z)V

    .line 85
    return-void
.end method
