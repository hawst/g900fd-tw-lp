.class final Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$4;
.super Ljava/lang/Object;
.source "HeadlinesDialogUtil.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->dialogDataAlertPopup(Landroid/app/Activity;ILcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/Object;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$action_type:I

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(ILandroid/app/Activity;)V
    .locals 0

    .prologue
    .line 279
    iput p1, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$4;->val$action_type:I

    iput-object p2, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$4;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 282
    iget v0, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$4;->val$action_type:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$4;->val$action_type:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 283
    :cond_0
    # getter for: Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mHandlerPullToRefresh:Landroid/os/Handler;
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->access$100()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 284
    # getter for: Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mHandlerPullToRefresh:Landroid/os/Handler;
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->access$100()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 285
    const/4 v0, 0x0

    # setter for: Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mHandlerPullToRefresh:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->access$102(Landroid/os/Handler;)Landroid/os/Handler;

    .line 289
    :cond_1
    # invokes: Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->clearVariables()V
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->access$300()V

    .line 290
    const/4 v0, 0x0

    # setter for: Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mIsPopupDisplaying:Z
    invoke-static {v0}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->access$202(Z)Z

    .line 291
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 292
    iget v0, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$4;->val$action_type:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 293
    iget-object v0, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$4;->val$activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 294
    :cond_2
    return-void
.end method
