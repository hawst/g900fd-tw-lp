.class public Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;
.super Landroid/widget/LinearLayout;
.source "HeadlinesSettingView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;,
        Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ViewHolder;,
        Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;,
        Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HeadlinesSettingView"


# instance fields
.field private mCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

.field private mContext:Landroid/content/Context;

.field private final mDefaultImage:[I

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mSectionList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    .line 51
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSectionList:Ljava/util/List;

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I

    .line 69
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mDefaultImage:[I

    .line 74
    const-string v0, "HeadlinesSettingView"

    const-string v1, "HeadlinesSettingView()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    .line 77
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mInflater:Landroid/view/LayoutInflater;

    .line 79
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->init()V

    .line 80
    return-void

    .line 69
    nop

    :array_0
    .array-data 4
        0x7f020000
        0x7f020001
        0x7f020002
        0x7f020003
        0x7f020004
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    .line 51
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSectionList:Ljava/util/List;

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I

    .line 69
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mDefaultImage:[I

    .line 85
    const-string v0, "HeadlinesSettingView"

    const-string v1, "HeadlinesSettingView()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    .line 88
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mInflater:Landroid/view/LayoutInflater;

    .line 90
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->init()V

    .line 91
    return-void

    .line 69
    nop

    :array_0
    .array-data 4
        0x7f020000
        0x7f020001
        0x7f020002
        0x7f020003
        0x7f020004
    .end array-data
.end method

.method static synthetic access$000(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Z)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->getExpanderImage(Z)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mDefaultImage:[I

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I

    return v0
.end method

.method static synthetic access$408(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I

    return v0
.end method

.method static synthetic access$410(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->dialogOpenSettingforVZW()V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private dialogOpenSettingforVZW()V
    .locals 3

    .prologue
    .line 516
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 517
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 518
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 520
    const v1, 0x7f090013

    new-instance v2, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$4;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 530
    const/high16 v1, 0x1040000

    new-instance v2, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$5;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 536
    new-instance v1, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$6;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 542
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 544
    return-void
.end method

.method private getExpanderImage(Z)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "isExpanded"    # Z

    .prologue
    .line 138
    if-eqz p1, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02006b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 143
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    const/high16 v1, 0x7f000000

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 144
    return-object v0

    .line 141
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02006d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_0
.end method

.method private getScreenWidth()I
    .locals 4

    .prologue
    .line 342
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 343
    .local v0, "manager":Landroid/view/WindowManager;
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 344
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 345
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    return v2
.end method

.method private getSection(I)Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSectionList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;

    return-object v0
.end method

.method private init()V
    .locals 12

    .prologue
    .line 94
    new-instance v9, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    iget-object v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    invoke-direct {v9, v10}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v9}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->addView(Landroid/view/View;)V

    .line 96
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    invoke-virtual {v9}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getSectionList()Ljava/util/List;

    move-result-object v8

    .line 98
    .local v8, "sectionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    if-ge v5, v9, :cond_3

    .line 99
    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 101
    .local v7, "section":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    invoke-virtual {v9, v7}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getCardTypeList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 102
    .local v2, "cardNameList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/CardInfo;>;"
    if-nez v2, :cond_0

    .line 103
    const-string v9, "HeadlinesSettingView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "init() : card name is zero in "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 107
    :cond_0
    const-string v9, "HeadlinesSettingView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "init() : section "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " card name size = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v6, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    if-ge v3, v9, :cond_2

    .line 112
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/magazine/cardchannel/CardInfo;

    .line 114
    .local v1, "cardInfo":Lcom/samsung/android/magazine/cardchannel/CardInfo;
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10, v7}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->isSubscribed(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    .line 116
    .local v4, "isChecked":Z
    new-instance v9, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;

    invoke-direct {v9, p0, v1, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Lcom/samsung/android/magazine/cardchannel/CardInfo;Z)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    if-eqz v4, :cond_1

    .line 119
    iget v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I

    .line 111
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 123
    .end local v1    # "cardInfo":Lcom/samsung/android/magazine/cardchannel/CardInfo;
    .end local v4    # "isChecked":Z
    :cond_2
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    const/4 v10, 0x0

    invoke-direct {v0, p0, v9, v10, v6}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Landroid/content/Context;ILjava/util/List;)V

    .line 124
    .local v0, "adapter":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;
    iget-object v9, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSectionList:Ljava/util/List;

    new-instance v10, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;

    invoke-direct {v10, p0, v7, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Ljava/lang/String;Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->getSectionView(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 129
    .end local v0    # "adapter":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;
    .end local v2    # "cardNameList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/CardInfo;>;"
    .end local v3    # "i":I
    .end local v6    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;>;"
    .end local v7    # "section":Ljava/lang/String;
    :cond_3
    const-string v9, "HeadlinesSettingView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "init() : current selection "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    return-void
.end method

.method private initTitleView(Landroid/view/View;IZ)V
    .locals 7
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "isExpanded"    # Z

    .prologue
    .line 148
    const v6, 0x7f0c0037

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 149
    .local v5, "view":Landroid/widget/RelativeLayout;
    const v6, 0x7f0c0038

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 150
    .local v0, "imageView":Landroid/widget/ImageView;
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setImportantForAccessibility(I)V

    .line 151
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImportantForAccessibility(I)V

    .line 152
    new-instance v6, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$1;

    invoke-direct {v6, p0, p1}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$1;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Landroid/view/View;)V

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    invoke-direct {p0, p2}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->getSection(I)Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;

    move-result-object v6

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->mSection:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;)Ljava/lang/String;

    move-result-object v1

    .line 172
    .local v1, "section":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    invoke-virtual {v6, v1}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getSectionDisplayName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 174
    .local v2, "sectionDisplayName":Ljava/lang/String;
    const v6, 0x7f0c003a

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 175
    .local v4, "sectionText":Landroid/widget/TextView;
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    const v6, 0x7f0c0039

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 178
    .local v3, "sectionImage":Landroid/widget/ImageView;
    invoke-direct {p0, p3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->getExpanderImage(Z)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 180
    new-instance v6, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$2;

    invoke-direct {v6, p0, p1, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$2;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 214
    return-void
.end method


# virtual methods
.method public changeSubcription()V
    .locals 15

    .prologue
    .line 302
    const-string v12, "HeadlinesSettingView"

    const-string v13, "changeSubcription() : ===================================== "

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v12, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSectionList:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-ge v4, v12, :cond_3

    .line 305
    iget-object v12, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSectionList:Ljava/util/List;

    invoke-interface {v12, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;

    .line 307
    .local v9, "sInfo":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;
    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->mSelectedMap:Ljava/util/HashMap;
    invoke-static {v9}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->access$600(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;)Ljava/util/HashMap;

    move-result-object v2

    .line 308
    .local v2, "before":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->mAdapter:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;
    invoke-static {v9}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->access$300(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;)Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->getSelectedMap()Ljava/util/HashMap;

    move-result-object v0

    .line 310
    .local v0, "after":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v11

    .line 311
    .local v11, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/String;

    invoke-interface {v11, v12}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    .line 313
    .local v7, "keySet":[Ljava/lang/String;
    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->mSection:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;)Ljava/lang/String;

    move-result-object v10

    .line 314
    .local v10, "section":Ljava/lang/String;
    const-string v12, "HeadlinesSettingView"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "changeSubcription() : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " ("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    move-object v1, v7

    .local v1, "arr$":[Ljava/lang/String;
    array-length v8, v1

    .local v8, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v8, :cond_2

    aget-object v3, v1, v5

    .line 317
    .local v3, "cardName":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Boolean;

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 319
    .local v6, "isSubscribed":Z
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_0

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Boolean;

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    if-ne v6, v12, :cond_0

    .line 320
    const-string v12, "HeadlinesSettingView"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "changeSubcription() : same        | "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 324
    :cond_0
    const/4 v12, 0x1

    if-ne v6, v12, :cond_1

    .line 325
    const-string v12, "HeadlinesSettingView"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "changeSubcription() : subscribe   | "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v12, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    invoke-virtual {v12, v3, v10}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->subscribeToCardType(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 328
    :cond_1
    const-string v12, "HeadlinesSettingView"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "changeSubcription() : unsubscribe | "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    iget-object v12, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mCardManager:Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    invoke-virtual {v12, v3, v10}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->unsubscribeToCardType(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 304
    .end local v3    # "cardName":Ljava/lang/String;
    .end local v6    # "isSubscribed":Z
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 334
    .end local v0    # "after":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "before":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .end local v5    # "i$":I
    .end local v7    # "keySet":[Ljava/lang/String;
    .end local v8    # "len$":I
    .end local v9    # "sInfo":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;
    .end local v10    # "section":Ljava/lang/String;
    .end local v11    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_3
    const-string v12, "HeadlinesSettingView"

    const-string v13, "changeSubcription() : ===================================== "

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return-void
.end method

.method public getSectionView(I)Landroid/view/View;
    .locals 21
    .param p1, "position"    # I

    .prologue
    .line 218
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->getScreenWidth()I

    move-result v13

    .line 219
    .local v13, "parentWidth":I
    const-string v18, "HeadlinesSettingView"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getGroupView() : parent width "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v18, v0

    const v19, 0x7f030013

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v16

    .line 222
    .local v16, "view":Landroid/view/View;
    const v18, 0x7f0c003b

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/GridView;

    .line 224
    .local v5, "gridView":Landroid/widget/GridView;
    invoke-virtual {v5}, Landroid/widget/GridView;->getVisibility()I

    move-result v18

    if-nez v18, :cond_1

    const/16 v18, 0x1

    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, p1

    move/from16 v3, v18

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->initTitleView(Landroid/view/View;IZ)V

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSectionList:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->mAdapter:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;->access$300(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$SectionInfo;)Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    move-result-object v4

    .line 227
    .local v4, "adapter":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;
    invoke-virtual {v5, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 229
    const/16 v18, 0x0

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/RelativeLayout;

    .line 230
    .local v14, "relativeLayout":Landroid/widget/RelativeLayout;
    const/16 v18, 0x0

    const/16 v19, 0x0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 231
    invoke-virtual {v14}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v17

    .line 232
    .local v17, "width":I
    invoke-virtual {v14}, Landroid/widget/RelativeLayout;->getMeasuredHeight()I

    move-result v6

    .line 233
    .local v6, "height":I
    const-string v18, "HeadlinesSettingView"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getChildView : width "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " height "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    div-int v18, v13, v17

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->floor(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-int v10, v0

    .line 236
    .local v10, "numColumns":I
    invoke-virtual {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->getCount()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    int-to-float v0, v10

    move/from16 v19, v0

    div-float v18, v18, v19

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-int v11, v0

    .line 237
    .local v11, "numRows":I
    mul-int v18, v17, v10

    sub-int v18, v13, v18

    div-int/lit8 v12, v18, 0x2

    .line 238
    .local v12, "padding":I
    const-string v18, "HeadlinesSettingView"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getChildView : col "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " row "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    invoke-virtual {v5}, Landroid/widget/GridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    if-nez v18, :cond_0

    .line 242
    const-string v18, "HeadlinesSettingView"

    const-string v19, "getlayoutParams is null!"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f08004f

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    .line 245
    .local v15, "spacing":I
    move v9, v13

    .line 246
    .local v9, "newWidth":I
    mul-int v18, v6, v11

    add-int/lit8 v19, v11, -0x1

    mul-int v19, v19, v15

    add-int v18, v18, v19

    invoke-virtual {v5}, Landroid/widget/GridView;->getPaddingTop()I

    move-result v19

    add-int v18, v18, v19

    invoke-virtual {v5}, Landroid/widget/GridView;->getPaddingBottom()I

    move-result v19

    add-int v8, v18, v19

    .line 248
    .local v8, "newHeight":I
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v9, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 249
    .local v7, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v5, v7}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 250
    invoke-virtual {v5}, Landroid/widget/GridView;->getPaddingTop()I

    move-result v18

    invoke-virtual {v5}, Landroid/widget/GridView;->getPaddingBottom()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v5, v12, v0, v12, v1}, Landroid/widget/GridView;->setPadding(IIII)V

    .line 252
    new-instance v18, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;)V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 298
    return-object v16

    .line 224
    .end local v4    # "adapter":Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;
    .end local v6    # "height":I
    .end local v7    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v8    # "newHeight":I
    .end local v9    # "newWidth":I
    .end local v10    # "numColumns":I
    .end local v11    # "numRows":I
    .end local v12    # "padding":I
    .end local v14    # "relativeLayout":Landroid/widget/RelativeLayout;
    .end local v15    # "spacing":I
    .end local v17    # "width":I
    :cond_1
    const/16 v18, 0x0

    goto/16 :goto_0
.end method

.method public update()V
    .locals 0

    .prologue
    .line 339
    return-void
.end method
