.class Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;
.super Ljava/lang/Object;
.source "HeadlinesSettingView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->getSectionView(I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

.field final synthetic val$adapter:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    iput-object p2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;->val$adapter:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const v8, 0x7f0c0035

    const/4 v7, 0x1

    .line 256
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;->val$adapter:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    invoke-virtual {v4, p3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;

    invoke-virtual {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->isChecked()Z

    move-result v3

    .line 257
    .local v3, "isChecked":Z
    const-string v4, "HeadlinesSettingView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onItemClick : position "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " isChecked "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    if-ne v3, v7, :cond_4

    .line 260
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I
    invoke-static {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$400(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)I

    move-result v4

    if-ne v4, v7, :cond_3

    .line 261
    const-string v4, "VZW"

    invoke-static {v4}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isOperatorModel(Ljava/lang/String;)Z

    move-result v4

    if-eq v4, v7, :cond_0

    const-string v4, "SKC"

    invoke-static {v4}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isOperatorModel(Ljava/lang/String;)Z

    move-result v4

    if-ne v4, v7, :cond_2

    .line 262
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # invokes: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->dialogOpenSettingforVZW()V
    invoke-static {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$500(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)V

    .line 268
    :goto_0
    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 269
    .local v1, "checkbox":Landroid/widget/CheckBox;
    const-string v4, "HeadlinesSettingView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onItemClick : current checkbox "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    const-string v4, "HeadlinesSettingView"

    const-string v5, "onItemClick : select at least one item!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :cond_1
    :goto_1
    return-void

    .line 264
    .end local v1    # "checkbox":Landroid/widget/CheckBox;
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f090016

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 274
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # operator-- for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I
    invoke-static {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$410(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)I

    .line 279
    :goto_2
    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 281
    .restart local v1    # "checkbox":Landroid/widget/CheckBox;
    invoke-virtual {v1}, Landroid/widget/CheckBox;->toggle()V

    .line 282
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    .line 284
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;->val$adapter:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    invoke-virtual {v4, p3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;

    invoke-virtual {v4, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->setChecked(Z)V

    .line 285
    const-string v5, "HeadlinesSettingView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onItemClick : toggle "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;->val$adapter:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;

    invoke-virtual {v4, p3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$HeadlinesSettingGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;

    invoke-virtual {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$ItemInfo;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "accessibility"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 288
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 289
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v2

    .line 290
    .local v2, "event":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v2, v7}, Landroid/view/accessibility/AccessibilityEvent;->setEventType(I)V

    .line 291
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 292
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 293
    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto/16 :goto_1

    .line 276
    .end local v0    # "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    .end local v1    # "checkbox":Landroid/widget/CheckBox;
    .end local v2    # "event":Landroid/view/accessibility/AccessibilityEvent;
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$3;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    # operator++ for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->mSelectedCount:I
    invoke-static {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$408(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;)I

    goto :goto_2
.end method
