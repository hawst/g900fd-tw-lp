.class public Lcom/samsung/android/app/headlines/template/CardTemplate_4;
.super Lcom/samsung/android/app/headlines/template/CardTemplate;
.source "CardTemplate_4.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/headlines/template/CardTemplate;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    .line 19
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateType:I

    .line 20
    return-void
.end method


# virtual methods
.method public getCardView(Landroid/view/LayoutInflater;Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;Lcom/samsung/android/magazine/cardchannel/Card;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "viewHolder"    # Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;
    .param p3, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceAsColor"
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x1

    .line 29
    iput-object p3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 31
    invoke-virtual {p2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->isEnabled()Z

    move-result v0

    if-ne v0, v2, :cond_7

    .line 32
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 33
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView1:Landroid/widget/TextView;

    .line 34
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView2:Landroid/widget/TextView;

    .line 35
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView3:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView3:Landroid/widget/TextView;

    .line 36
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView4:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView4:Landroid/widget/TextView;

    .line 37
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView5:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView5:Landroid/widget/TextView;

    .line 38
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView1:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView1:Landroid/widget/ImageView;

    .line 39
    iget-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView2:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView2:Landroid/widget/ImageView;

    .line 64
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_6

    .line 66
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView1:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text1"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text1"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView2:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text2"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text2"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView3:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 77
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView3:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text3"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView3:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text3"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 81
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView4:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 82
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView4:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text4"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView4:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text4"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 86
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView5:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 87
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView5:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text5"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView5:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "text5"

    invoke-static {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 91
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView1:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 92
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "image1"

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView1:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 96
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView2:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 97
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView2:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v2, "image2"

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView2:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 99
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView2:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 100
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView2:Landroid/widget/ImageView;

    const v1, 0x7f020056

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 106
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    return-object v0

    .line 41
    :cond_7
    const v0, 0x7f030018

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 42
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_8

    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c0049

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView1:Landroid/widget/TextView;

    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c0047

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView2:Landroid/widget/TextView;

    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c004b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView3:Landroid/widget/TextView;

    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c0044

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView4:Landroid/widget/TextView;

    .line 47
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c0046

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView5:Landroid/widget/TextView;

    .line 48
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c0040

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView1:Landroid/widget/ImageView;

    .line 49
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0c0043

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView2:Landroid/widget/ImageView;

    .line 52
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 53
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView1:Landroid/widget/TextView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    .line 54
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView2:Landroid/widget/TextView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    .line 55
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView3:Landroid/widget/TextView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView3:Landroid/widget/TextView;

    .line 56
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView4:Landroid/widget/TextView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView4:Landroid/widget/TextView;

    .line 57
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mTextView5:Landroid/widget/TextView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView5:Landroid/widget/TextView;

    .line 58
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView1:Landroid/widget/ImageView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView1:Landroid/widget/ImageView;

    .line 59
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_4;->mImageView2:Landroid/widget/ImageView;

    iput-object v0, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView2:Landroid/widget/ImageView;

    .line 61
    invoke-virtual {p2, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->setEnabled(Z)V

    goto/16 :goto_0
.end method

.method public onCardCollapse()Z
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    return v0
.end method

.method public onCardExpand()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method
