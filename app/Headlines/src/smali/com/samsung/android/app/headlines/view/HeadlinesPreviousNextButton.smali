.class public Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;
.super Landroid/widget/Button;
.source "HeadlinesPreviousNextButton.java"


# instance fields
.field private mIsNext:Z

.field private mNextIcon:Landroid/graphics/drawable/Drawable;

.field private mTextMargin:I

.field private mTextPaintLength:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mIsNext:Z

    .line 20
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mIsNext:Z

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 16
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mIsNext:Z

    .line 25
    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mIsNext:Z

    .line 26
    return-void
.end method

.method private convertDipToPixels(F)I
    .locals 2
    .param p1, "dips"    # F

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 43
    .local v5, "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f09004a

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 44
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mIsNext:Z

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->getWidth()I

    move-result v9

    .line 47
    .local v9, "width":I
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->getHeight()I

    move-result v0

    .line 48
    .local v0, "height":I
    iget v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mTextPaintLength:I

    sub-int v10, v9, v10

    div-int/lit8 v6, v10, 0x2

    .line 49
    .local v6, "textLeft":I
    iget v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mTextPaintLength:I

    add-int v7, v6, v10

    .line 50
    .local v7, "textRight":I
    iget-object v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mNextIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 51
    .local v1, "iconHeight":I
    iget-object v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mNextIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 52
    .local v4, "iconWidth":I
    sub-int v10, v0, v1

    div-int/lit8 v3, v10, 0x2

    .line 53
    .local v3, "iconTop":I
    iget v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mTextMargin:I

    add-int v2, v7, v10

    .line 54
    .local v2, "iconLeft":I
    div-int/lit8 v10, v4, 0x2

    neg-int v8, v10

    .line 56
    .local v8, "tranx":I
    iget-boolean v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mIsNext:Z

    if-eqz v10, :cond_1

    .line 57
    int-to-float v10, v8

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->translate(FF)V

    .line 60
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/Button;->onDraw(Landroid/graphics/Canvas;)V

    .line 62
    iget-boolean v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mIsNext:Z

    if-eqz v10, :cond_2

    .line 63
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->isEnabled()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 64
    iget-object v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mNextIcon:Landroid/graphics/drawable/Drawable;

    const/16 v11, 0xff

    invoke-virtual {v10, v11}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 68
    :goto_0
    iget-object v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mNextIcon:Landroid/graphics/drawable/Drawable;

    const/high16 v11, 0x41000000    # 8.0f

    invoke-direct {p0, v11}, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->convertDipToPixels(F)I

    move-result v11

    add-int/2addr v11, v2

    const/high16 v12, 0x41000000    # 8.0f

    invoke-direct {p0, v12}, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->convertDipToPixels(F)I

    move-result v12

    add-int/2addr v12, v2

    add-int/2addr v12, v4

    add-int v13, v3, v1

    invoke-virtual {v10, v11, v3, v12, v13}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 70
    iget-object v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mNextIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 71
    neg-int v10, v8

    int-to-float v10, v10

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->translate(FF)V

    .line 73
    :cond_2
    return-void

    .line 66
    :cond_3
    iget-object v10, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mNextIcon:Landroid/graphics/drawable/Drawable;

    const/16 v11, 0x66

    invoke-virtual {v10, v11}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 30
    invoke-super {p0}, Landroid/widget/Button;->onFinishInflate()V

    .line 31
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 34
    .local v0, "p":Landroid/graphics/Paint;
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mTextPaintLength:I

    .line 35
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020062

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mNextIcon:Landroid/graphics/drawable/Drawable;

    .line 36
    const/high16 v2, 0x41000000    # 8.0f

    invoke-direct {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->convertDipToPixels(F)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPreviousNextButton;->mTextMargin:I

    .line 37
    return-void
.end method
