.class public Lcom/samsung/android/app/headlines/template/CardTemplate_3;
.super Lcom/samsung/android/app/headlines/template/CardTemplate;
.source "CardTemplate_3.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/headlines/template/CardTemplate;-><init>(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V

    .line 21
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateType:I

    .line 22
    return-void
.end method


# virtual methods
.method public getCardView(Landroid/view/LayoutInflater;Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;Lcom/samsung/android/magazine/cardchannel/Card;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "viewHolder"    # Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;
    .param p3, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceAsColor"
        }
    .end annotation

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x1

    .line 31
    iput-object p3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    .line 33
    invoke-virtual {p2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->isEnabled()Z

    move-result v2

    if-ne v2, v4, :cond_7

    .line 34
    iget-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 35
    iget-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView1:Landroid/widget/TextView;

    .line 36
    iget-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView2:Landroid/widget/TextView;

    .line 37
    iget-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView3:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView3:Landroid/widget/TextView;

    .line 38
    iget-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView4:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView4:Landroid/widget/TextView;

    .line 39
    iget-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView5:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView5:Landroid/widget/TextView;

    .line 40
    iget-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView1:Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView1:Landroid/widget/ImageView;

    .line 41
    iget-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView2:Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView2:Landroid/widget/ImageView;

    .line 66
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v2, :cond_6

    .line 68
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView1:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 69
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/cardchannel/Card;->getProviderName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.samsung.android.app.headlines"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 70
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "displayName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getCardDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "localCardName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView1:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    .end local v0    # "displayName":Ljava/lang/String;
    .end local v1    # "localCardName":Ljava/lang/String;
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView1:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v4, "text1"

    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 79
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView2:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 80
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView2:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v4, "text2"

    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView2:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v4, "text2"

    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 84
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView3:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 85
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView3:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v4, "text3"

    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView3:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v4, "text3"

    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 89
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView4:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 90
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView4:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v4, "text4"

    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView4:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v4, "text4"

    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 94
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView5:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    .line 95
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView5:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v4, "text5"

    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView5:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v4, "text5"

    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 99
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView1:Landroid/widget/ImageView;

    if-eqz v2, :cond_5

    .line 100
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView1:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v4, "image1"

    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView1:Landroid/widget/ImageView;

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 104
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView2:Landroid/widget/ImageView;

    if-eqz v2, :cond_6

    .line 105
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView2:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 106
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v4, "image2"

    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView2:Landroid/widget/ImageView;

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 107
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView2:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 108
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView2:Landroid/widget/ImageView;

    const v3, 0x7f020056

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 113
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    return-object v2

    .line 43
    :cond_7
    const v2, 0x7f030017

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 44
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    if-eqz v2, :cond_8

    .line 45
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0c0049

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView1:Landroid/widget/TextView;

    .line 46
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0c0047

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView2:Landroid/widget/TextView;

    .line 47
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0c004b

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView3:Landroid/widget/TextView;

    .line 48
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0c0044

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView4:Landroid/widget/TextView;

    .line 49
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0c0046

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView5:Landroid/widget/TextView;

    .line 50
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0c0040

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView1:Landroid/widget/ImageView;

    .line 51
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0c0043

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView2:Landroid/widget/ImageView;

    .line 54
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTemplateContainer:Landroid/view/ViewGroup;

    iput-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTemplateContainer:Landroid/view/ViewGroup;

    .line 55
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView1:Landroid/widget/TextView;

    iput-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView1:Landroid/widget/TextView;

    .line 56
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView2:Landroid/widget/TextView;

    iput-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView2:Landroid/widget/TextView;

    .line 57
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView3:Landroid/widget/TextView;

    iput-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView3:Landroid/widget/TextView;

    .line 58
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView4:Landroid/widget/TextView;

    iput-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView4:Landroid/widget/TextView;

    .line 59
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView5:Landroid/widget/TextView;

    iput-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mTextView5:Landroid/widget/TextView;

    .line 60
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView1:Landroid/widget/ImageView;

    iput-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView1:Landroid/widget/ImageView;

    .line 61
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mImageView2:Landroid/widget/ImageView;

    iput-object v2, p2, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->mImageView2:Landroid/widget/ImageView;

    .line 63
    invoke-virtual {p2, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesCardViewHolder;->setEnabled(Z)V

    goto/16 :goto_0

    .line 74
    :cond_9
    iget-object v2, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mTextView1:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/template/CardTemplate_3;->mCard:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v4, "text1"

    invoke-static {v3, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public onCardCollapse()Z
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public onCardExpand()Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method
