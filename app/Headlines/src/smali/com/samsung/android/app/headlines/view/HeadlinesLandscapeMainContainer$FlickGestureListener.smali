.class Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$FlickGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "HeadlinesLandscapeMainContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FlickGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)V
    .locals 0

    .prologue
    .line 454
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$FlickGestureListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;
    .param p2, "x1"    # Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$1;

    .prologue
    .line 454
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$FlickGestureListener;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "event1"    # Landroid/view/MotionEvent;
    .param p2, "event2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v2, 0x0

    .line 458
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 474
    :cond_0
    :goto_0
    return v2

    .line 461
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$FlickGestureListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mTouchControlState:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$500(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_FLICK_HORIZONTAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    if-ne v3, v4, :cond_0

    .line 465
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 466
    .local v0, "e1X":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 468
    .local v1, "e2X":F
    sub-float v3, v0, v1

    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$FlickGestureListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I
    invoke-static {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$600(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)I

    move-result v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 469
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$FlickGestureListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$300(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->startLauncher(Landroid/content/Context;Z)V

    .line 470
    const/4 v2, 0x1

    goto :goto_0
.end method
