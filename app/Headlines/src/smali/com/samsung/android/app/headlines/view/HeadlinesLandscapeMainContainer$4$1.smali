.class Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4$1;
.super Ljava/lang/Object;
.source "HeadlinesLandscapeMainContainer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4;)V
    .locals 0

    .prologue
    .line 582
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4$1;->this$1:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 585
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4$1;->this$1:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4;

    iget-object v3, v3, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 586
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4$1;->this$1:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4;

    iget-object v3, v3, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 587
    .local v1, "v":Landroid/view/View;
    instance-of v3, v1, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    move-object v2, v1

    .line 588
    check-cast v2, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    .line 589
    .local v2, "view":Lcom/samsung/android/app/headlines/view/HeadlinesCardView;
    if-eqz v2, :cond_0

    .line 590
    invoke-virtual {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->showLoadingViews()V

    .line 585
    .end local v2    # "view":Lcom/samsung/android/app/headlines/view/HeadlinesCardView;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 594
    .end local v1    # "v":Landroid/view/View;
    :cond_1
    return-void
.end method
