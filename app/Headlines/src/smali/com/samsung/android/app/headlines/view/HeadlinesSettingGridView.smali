.class public Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;
.super Landroid/widget/LinearLayout;
.source "HeadlinesSettingGridView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "HeadlinesSettingGridView"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mGridView:Landroid/widget/GridView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 16
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->mContext:Landroid/content/Context;

    .line 28
    const-string v1, "HeadlinesSettingGridView"

    const-string v2, "HeadlinesSettingGridView()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->mContext:Landroid/content/Context;

    .line 32
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 33
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030013

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 36
    const v1, 0x7f0c003b

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->mGridView:Landroid/widget/GridView;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ListAdapter;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "section"    # Ljava/lang/String;
    .param p3, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 16
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->mContext:Landroid/content/Context;

    .line 48
    const-string v1, "HeadlinesSettingGridView"

    const-string v2, "HeadlinesSettingGridView()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->mContext:Landroid/content/Context;

    .line 54
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 55
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030013

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 58
    const v1, 0x7f0c003b

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->mGridView:Landroid/widget/GridView;

    .line 59
    iget-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v1, p3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 60
    return-void
.end method


# virtual methods
.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->mGridView:Landroid/widget/GridView;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingGridView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 43
    :cond_0
    return-void
.end method
