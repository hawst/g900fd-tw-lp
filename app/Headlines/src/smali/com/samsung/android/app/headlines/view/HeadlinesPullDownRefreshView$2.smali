.class Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$2;
.super Ljava/lang/Object;
.source "HeadlinesPullDownRefreshView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->startRefreshBackAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 194
    const-string v0, "HeadlinesPullDownRefreshView"

    const-string v1, "onAnimationEnd(): Refresh Back Animation is canceled."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 182
    const-string v0, "HeadlinesPullDownRefreshView"

    const-string v1, "onAnimationEnd(): Refresh Back Animation is Finished."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshEventListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;
    invoke-static {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mPullDownRefreshEventListener:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;
    invoke-static {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshEventListener;->onPullDownAnimationFinished()V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->mRefreshBackRunnable:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->access$202(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 188
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    # invokes: Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->dismissLoadingProgressBar()V
    invoke-static {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->access$300(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)V

    .line 189
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;

    # invokes: Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->initPullDownRefreshView()V
    invoke-static {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;->access$400(Lcom/samsung/android/app/headlines/view/HeadlinesPullDownRefreshView;)V

    .line 190
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 178
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 173
    const-string v0, "HeadlinesPullDownRefreshView"

    const-string v1, "onAnimationEnd(): Refresh Back Animation is Started."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    return-void
.end method
