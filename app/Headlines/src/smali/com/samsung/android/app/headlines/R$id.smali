.class public final Lcom/samsung/android/app/headlines/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final LinearLayout1:I = 0x7f0c0016

.field public static final about_descripton:I = 0x7f0c0002

.field public static final action_about:I = 0x7f0c0052

.field public static final action_help:I = 0x7f0c0053

.field public static final action_settings:I = 0x7f0c0051

.field public static final author_divider_01:I = 0x7f0c0045

.field public static final author_image:I = 0x7f0c0043

.field public static final author_layout:I = 0x7f0c0042

.field public static final author_name:I = 0x7f0c0044

.field public static final auto_refresh_checkbox:I = 0x7f0c0030

.field public static final auto_refresh_detail:I = 0x7f0c0032

.field public static final auto_refresh_title:I = 0x7f0c0031

.field public static final body_layout:I = 0x7f0c004a

.field public static final body_text:I = 0x7f0c004b

.field public static final button_Welcome_next:I = 0x7f0c0019

.field public static final button_start:I = 0x7f0c003d

.field public static final card_container:I = 0x7f0c0005

.field public static final card_header:I = 0x7f0c001f

.field public static final card_list:I = 0x7f0c001d

.field public static final category_layout:I = 0x7f0c0048

.field public static final category_text:I = 0x7f0c0049

.field public static final check_box:I = 0x7f0c0029

.field public static final checkbox_policy:I = 0x7f0c0011

.field public static final checkbox_terms:I = 0x7f0c000c

.field public static final container:I = 0x7f0c001b

.field public static final description_text:I = 0x7f0c0018

.field public static final gradient_bottom:I = 0x7f0c0041

.field public static final header:I = 0x7f0c0021

.field public static final headlines_card_grid_view:I = 0x7f0c0023

.field public static final headlines_header_view:I = 0x7f0c0024

.field public static final imageView1:I = 0x7f0c0015

.field public static final image_divider:I = 0x7f0c000d

.field public static final image_magazine_ic:I = 0x7f0c0001

.field public static final image_view_more_button:I = 0x7f0c001e

.field public static final item_checkbox:I = 0x7f0c0035

.field public static final item_image:I = 0x7f0c0034

.field public static final item_layout:I = 0x7f0c0033

.field public static final item_text:I = 0x7f0c0036

.field public static final layout_bottom:I = 0x7f0c0012

.field public static final layout_setting:I = 0x7f0c0007

.field public static final layout_start:I = 0x7f0c003c

.field public static final layout_terms_condition:I = 0x7f0c0008

.field public static final linearLayout1:I = 0x7f0c0013

.field public static final loadingImage:I = 0x7f0c002d

.field public static final loadingText:I = 0x7f0c002e

.field public static final loading_window:I = 0x7f0c002c

.field public static final login_window:I = 0x7f0c002b

.field public static final main:I = 0x7f0c0004

.field public static final main_image:I = 0x7f0c0040

.field public static final main_layout:I = 0x7f0c003f

.field public static final menu_delete:I = 0x7f0c0055

.field public static final menu_modify:I = 0x7f0c0054

.field public static final new_notification_button:I = 0x7f0c0020

.field public static final progressbar:I = 0x7f0c004e

.field public static final pull_to_refresh:I = 0x7f0c001c

.field public static final pull_to_refresh_header:I = 0x7f0c0025

.field public static final pull_to_refresh_image_view_icon:I = 0x7f0c0027

.field public static final pull_to_refresh_image_view_text:I = 0x7f0c0028

.field public static final pull_to_refresh_progressbar:I = 0x7f0c0026

.field public static final refresh_setting_layout:I = 0x7f0c004d

.field public static final relativeLayout_refresh:I = 0x7f0c002f

.field public static final scroll_policy:I = 0x7f0c000e

.field public static final scroll_terms:I = 0x7f0c0009

.field public static final scroll_view_container:I = 0x7f0c0006

.field public static final section_grid:I = 0x7f0c003b

.field public static final section_title:I = 0x7f0c0037

.field public static final section_title_image:I = 0x7f0c0039

.field public static final section_title_selector:I = 0x7f0c0038

.field public static final section_title_text:I = 0x7f0c003a

.field public static final social_welcome_icon:I = 0x7f0c004f

.field public static final template_main_layout:I = 0x7f0c003e

.field public static final templated_card_container:I = 0x7f0c004c

.field public static final terms_of_use_button:I = 0x7f0c0003

.field public static final textView1:I = 0x7f0c002a

.field public static final text_mymagazine:I = 0x7f0c0000

.field public static final text_policy:I = 0x7f0c0010

.field public static final text_policy_title:I = 0x7f0c000f

.field public static final text_terms:I = 0x7f0c000b

.field public static final text_terms_title:I = 0x7f0c000a

.field public static final text_view_next:I = 0x7f0c001a

.field public static final text_view_title:I = 0x7f0c0022

.field public static final text_view_welcome_mymagazine:I = 0x7f0c0014

.field public static final text_view_welcome_title:I = 0x7f0c0017

.field public static final time_stamp:I = 0x7f0c0046

.field public static final title_text:I = 0x7f0c0047

.field public static final welcome_layout:I = 0x7f0c0050


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
