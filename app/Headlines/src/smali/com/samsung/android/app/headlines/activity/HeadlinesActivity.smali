.class public Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;
.super Landroid/app/Activity;
.source "HeadlinesActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;
    }
.end annotation


# static fields
.field public static final REQUEST_TERMS_OF_USE_AGREEMENT:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "HeadlinesActivity"

.field public static dontShowAgain:Landroid/widget/CheckBox;

.field static status:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;


# instance fields
.field private mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

.field private mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

.field private mIsWelcomePressed:Ljava/lang/Boolean;

.field private mPopupShown:Z

.field private prevOrientation:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->INIT:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    sput-object v0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->status:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 51
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mIsWelcomePressed:Ljava/lang/Boolean;

    .line 52
    iput-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    .line 53
    iput-object v2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    .line 54
    iput-boolean v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mPopupShown:Z

    .line 60
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->prevOrientation:I

    return-void
.end method

.method private Log()V
    .locals 5

    .prologue
    .line 257
    sget-object v2, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->status:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    sget-object v3, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->INIT:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    if-ne v2, v3, :cond_0

    .line 258
    const-string v2, "Headlines"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Logging enter My Magazine : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->status:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    const-string v3, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v2, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 260
    sget-object v2, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->ON:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    sput-object v2, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->status:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    .line 266
    :cond_0
    :goto_0
    sget-object v2, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->status:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    sget-object v3, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->ON:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    if-ne v2, v3, :cond_2

    .line 267
    const-string v2, "Headlines"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Logging enter My Magazine : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->status:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 269
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v2, "app_id"

    const-string v3, "com.samsung.android.app.headlines"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const-string v2, "feature"

    const-string v3, "ENTR"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 274
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    const-string v2, "data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 277
    const-string v2, "com.samsung.android.providers.context"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 279
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 283
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v1    # "cv":Landroid/content/ContentValues;
    :goto_1
    return-void

    .line 262
    :cond_1
    sget-object v2, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;->OFF:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    sput-object v2, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->status:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    goto :goto_0

    .line 281
    :cond_2
    const-string v2, "Headlines"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Logging enter My Magazine : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->status:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$LogginStatus;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static synthetic access$002(Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mPopupShown:Z

    return p1
.end method

.method private dialogCustomizeSettingPopup(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    .line 354
    iget-boolean v4, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mPopupShown:Z

    if-ne v4, v5, :cond_0

    .line 411
    :goto_0
    return-void

    .line 357
    :cond_0
    iput-boolean v5, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mPopupShown:Z

    .line 358
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 359
    .local v2, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 360
    .local v0, "adbInflater":Landroid/view/LayoutInflater;
    const v4, 0x7f03000e

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 361
    .local v3, "eulaLayout":Landroid/view/View;
    const v4, 0x7f0c0029

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 362
    .local v1, "checkBox":Landroid/widget/CheckBox;
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 363
    new-instance v4, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$2;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$2;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;)V

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 370
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 371
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090060

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 372
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090062

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 374
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090061

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$3;

    invoke-direct {v5, p0, p1}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$3;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;Landroid/content/Context;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 386
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x1040000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$4;

    invoke-direct {v5, p0, v1}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$4;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 397
    new-instance v4, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$5;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$5;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;)V

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 403
    new-instance v4, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$6;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$6;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;)V

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 410
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 246
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 248
    const-string v1, "HeadlinesActivity"

    const-string v2, "onActivityResult()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    if-eqz p3, :cond_0

    .line 251
    const-string v1, "contentGuide"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "result":Ljava/lang/String;
    const-string v1, "HeadlinesActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult() : locale = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    .end local v0    # "result":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 174
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 175
    const v0, 0x7f040003

    const v1, 0x7f040004

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->overridePendingTransition(II)V

    .line 176
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->removeAllNewCategoryList()V

    .line 179
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 8
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 286
    const-string v3, "HeadlinesActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onConfigurationChanged "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 290
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    if-nez v3, :cond_0

    .line 291
    const-string v3, "HeadlinesActivity"

    const-string v4, "onConfigurationChanged(): Container Interface is null."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    :goto_0
    return-void

    .line 295
    :cond_0
    const/4 v2, 0x0

    .line 296
    .local v2, "prevPopupStatus":Z
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->isPopupShown()Z

    move-result v3

    if-ne v3, v6, :cond_1

    .line 297
    const/4 v2, 0x1

    .line 302
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->dismissMoreMenu()V

    .line 304
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;

    move-result-object v0

    .line 305
    .local v0, "absListView":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    if-nez v0, :cond_2

    .line 306
    const-string v3, "HeadlinesActivity"

    const-string v4, "onConfigurationChanged(): Before orientation changed -> absListView is null."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 299
    .end local v0    # "absListView":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 310
    .restart local v0    # "absListView":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->getFirstVisiblePosition()I

    move-result v1

    .line 312
    .local v1, "currentPosition":I
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v3, :pswitch_data_0

    .line 334
    :goto_2
    const v3, 0x7f030001

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->setContentView(I)V

    .line 336
    const v3, 0x7f0c0005

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    iput-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    .line 337
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    iget-object v4, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-interface {v3, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->setAdapter(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)V

    .line 338
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v3, v6}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->animateHeader(Z)V

    .line 340
    if-ne v2, v6, :cond_3

    .line 341
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->showMoreMenu()V

    .line 344
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;

    move-result-object v0

    .line 345
    if-nez v0, :cond_6

    .line 346
    const-string v3, "HeadlinesActivity"

    const-string v4, "onConfigurationChanged(): After orientation changed -> absListView is null."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 314
    :pswitch_0
    iget v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->prevOrientation:I

    if-ne v3, v7, :cond_4

    .line 315
    add-int/lit8 v1, v1, 0x1

    .line 316
    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-le v1, v3, :cond_4

    .line 317
    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->getCount()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .line 320
    :cond_4
    iput v6, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->prevOrientation:I

    goto :goto_2

    .line 323
    :pswitch_1
    iget v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->prevOrientation:I

    if-ne v3, v6, :cond_5

    .line 324
    add-int/lit8 v1, v1, -0x1

    .line 325
    if-ge v1, v6, :cond_5

    .line 326
    const/4 v1, 0x1

    .line 329
    :cond_5
    iput v7, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->prevOrientation:I

    goto :goto_2

    .line 350
    :cond_6
    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setSelection(I)V

    goto/16 :goto_0

    .line 312
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/16 v7, 0x400

    invoke-virtual {v6, v7}, Landroid/view/Window;->clearFlags(I)V

    .line 73
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/high16 v7, -0x7c000000

    invoke-virtual {v6, v7}, Landroid/view/Window;->addFlags(I)V

    .line 74
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    const v7, -0x40000001    # -1.9999999f

    invoke-virtual {v6, v7}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 75
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    const v7, 0x8000

    invoke-virtual {v6, v7}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 77
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-static {p0}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isOrientationSupport(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 79
    invoke-virtual {p0, v9}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->setRequestedOrientation(I)V

    .line 81
    :cond_0
    const-string v6, "mymagazine"

    invoke-virtual {p0, v6, v8}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 82
    .local v4, "pref":Landroid/content/SharedPreferences;
    const-string v6, "WELCOME_NEXT_BUTTON_PRESSED"

    invoke-interface {v4, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mIsWelcomePressed:Ljava/lang/Boolean;

    .line 84
    iget-object v6, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mIsWelcomePressed:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-ne v6, v9, :cond_2

    .line 86
    const-string v6, "CN"

    invoke-static {v6}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isCountryModel(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 87
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 88
    .local v2, "i":Landroid/content/Intent;
    const-string v6, "com.samsung.android.internal.headlines.ALLOW_DATA_USE"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 92
    .end local v2    # "i":Landroid/content/Intent;
    :cond_1
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v0

    .line 94
    .local v0, "cardManager":Lcom/samsung/android/app/headlines/HeadlinesCardManager;
    const v6, 0x7f030001

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->setContentView(I)V

    .line 96
    new-instance v6, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    .line 97
    const v6, 0x7f0c0005

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    iput-object v6, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    .line 98
    iget-object v6, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    iget-object v7, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mCardAdapter:Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;

    invoke-interface {v6, v7}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->setAdapter(Lcom/samsung/android/app/headlines/adapter/HeadlinesCardAdapter;)V

    .line 100
    const-string v6, "window"

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 102
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v3

    .line 103
    .local v3, "orientation":I
    packed-switch v3, :pswitch_data_0

    .line 114
    :goto_0
    invoke-static {p0}, Lcom/samsung/android/app/headlines/UpgradeManager;->updateCheck(Landroid/content/Context;)V

    .line 127
    .end local v0    # "cardManager":Lcom/samsung/android/app/headlines/HeadlinesCardManager;
    .end local v1    # "display":Landroid/view/Display;
    .end local v3    # "orientation":I
    :goto_1
    return-void

    .line 106
    .restart local v0    # "cardManager":Lcom/samsung/android/app/headlines/HeadlinesCardManager;
    .restart local v1    # "display":Landroid/view/Display;
    .restart local v3    # "orientation":I
    :pswitch_0
    const/4 v3, 0x2

    .line 107
    goto :goto_0

    .line 110
    :pswitch_1
    const/4 v3, 0x1

    goto :goto_0

    .line 116
    .end local v0    # "cardManager":Lcom/samsung/android/app/headlines/HeadlinesCardManager;
    .end local v1    # "display":Landroid/view/Display;
    .end local v3    # "orientation":I
    :cond_2
    new-instance v5, Ljava/util/Timer;

    invoke-direct {v5}, Ljava/util/Timer;-><init>()V

    .line 117
    .local v5, "timer":Ljava/util/Timer;
    new-instance v6, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$1;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$1;-><init>(Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;)V

    const-wide/16 v8, 0x258

    invoke-virtual {v5, v6, v8, v9}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_1

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 137
    .local v0, "inflater":Landroid/view/MenuInflater;
    const/high16 v1, 0x7f0b0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 139
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 145
    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v0}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->showMoreMenu()V

    .line 149
    :cond_0
    const/4 v0, 0x1

    .line 152
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 157
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 169
    :pswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    .line 159
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    if-eqz v1, :cond_0

    .line 160
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->openSetting()V

    goto :goto_0

    .line 164
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    if-eqz v1, :cond_0

    .line 165
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->openHelp()V

    goto :goto_0

    .line 157
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c0051
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onPause()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 213
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 215
    const-string v7, "mymagazine"

    invoke-virtual {p0, v7, v10}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 216
    .local v4, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 217
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v7, "PAUSE_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-interface {v2, v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 218
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 220
    const-string v7, "CardOrder"

    invoke-virtual {p0, v7, v10}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 221
    .local v5, "reorder":Landroid/content/SharedPreferences;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 222
    .local v6, "reorderEditor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 223
    const-string v7, "IsFirstLaunch"

    invoke-interface {v6, v7, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 225
    iget-object v7, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mIsWelcomePressed:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    .line 226
    iget-object v7, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->getCardList()Ljava/util/List;

    move-result-object v1

    .line 227
    .local v1, "cardList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    const-string v7, "CardCount"

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 228
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 229
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 230
    .local v0, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    if-eqz v0, :cond_0

    .line 231
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 232
    const-string v7, "HeadlinesActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onPause() : The Card [Type = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] is saved to SharedPreference."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 235
    .end local v0    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :cond_1
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 237
    .end local v1    # "cardList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    .end local v3    # "i":I
    :cond_2
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 131
    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    .line 132
    return-void
.end method

.method protected onResume()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 183
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 185
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->Log()V

    .line 186
    iget-object v6, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    if-eqz v6, :cond_0

    .line 187
    iget-object v6, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v6}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->requestAllRefresh()V

    .line 189
    :cond_0
    new-instance v4, Landroid/content/Intent;

    const-class v6, Lcom/samsung/android/internal/headlines/HeadlinesService;

    invoke-direct {v4, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 190
    .local v4, "sIntent":Landroid/content/Intent;
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 191
    const-string v6, "mymagazine"

    invoke-virtual {p0, v6, v7}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 192
    .local v3, "prefs":Landroid/content/SharedPreferences;
    const-string v6, "ENABLE_NEW_STORY_NOTIFICATION"

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 193
    .local v2, "enable":Z
    if-ne v2, v8, :cond_1

    .line 194
    iget-object v6, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    if-eqz v6, :cond_1

    .line 195
    iget-object v6, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->mHeadlinesCardContainer:Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;

    invoke-interface {v6, v8}, Lcom/samsung/android/app/headlines/view/HeadlinesMainContainerInterface;->setNewCategoryNotificationEnabled(Z)V

    .line 199
    :cond_1
    const-string v6, "ATT"

    invoke-static {v6}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isOperatorModel(Ljava/lang/String;)Z

    move-result v6

    if-ne v6, v8, :cond_2

    .line 200
    const-string v6, "WELCOME_NEXT_BUTTON_PRESSED"

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 201
    .local v5, "welcomePressed":Z
    if-ne v5, v8, :cond_2

    .line 202
    const-string v6, "CUSTOMIZE_SETTING"

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 203
    .local v0, "customize":Z
    const-string v6, "DO_NOT_SHOW_CUSTOMIZE_SETTING_POPUP_SHOW_FOR_ATT"

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 204
    .local v1, "doNotShow":Z
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    .line 205
    invoke-direct {p0, p0}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->dialogCustomizeSettingPopup(Landroid/content/Context;)V

    .line 209
    .end local v0    # "customize":Z
    .end local v1    # "doNotShow":Z
    .end local v5    # "welcomePressed":Z
    :cond_2
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 241
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 242
    return-void
.end method
