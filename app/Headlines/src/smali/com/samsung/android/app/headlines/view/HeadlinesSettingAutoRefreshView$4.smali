.class Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;
.super Ljava/lang/Object;
.source "HeadlinesSettingAutoRefreshView.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "arg1"    # Z

    .prologue
    const/4 v5, 0x0

    .line 148
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mDoNotShow:Z
    invoke-static {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$300(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mAlreadyConfirm:Z
    invoke-static {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$400(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 149
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # invokes: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->dialogAutoRefreshAlertPopup()V
    invoke-static {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$500(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)V

    .line 150
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 163
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # setter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mAlreadyConfirm:Z
    invoke-static {v2, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$402(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Z)Z

    .line 155
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mCheckButton:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    # setter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mRefresh:Z
    invoke-static {v2, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$002(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;Z)Z

    .line 157
    const-string v2, "HeadlinesSettingAutoRefreshView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOnCheckedChangeListener mRefresh "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mRefresh:Z
    invoke-static {v4}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$000(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "mymagazine"

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 160
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 161
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "AUTO_REFRESH_ON"

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView$4;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->mRefresh:Z
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;->access$000(Lcom/samsung/android/app/headlines/view/HeadlinesSettingAutoRefreshView;)Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 162
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
