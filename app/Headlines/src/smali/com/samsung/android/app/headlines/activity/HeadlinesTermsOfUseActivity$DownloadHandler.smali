.class Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity$DownloadHandler;
.super Landroid/os/Handler;
.source "HeadlinesTermsOfUseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DownloadHandler"
.end annotation


# instance fields
.field private final mActivityReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;

    .prologue
    .line 333
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 334
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity$DownloadHandler;->mActivityReference:Ljava/lang/ref/WeakReference;

    .line 335
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 339
    iget-object v1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity$DownloadHandler;->mActivityReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;

    .line 340
    .local v0, "activity":Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;
    if-eqz v0, :cond_0

    .line 341
    # invokes: Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->handleMessage(Landroid/os/Message;)V
    invoke-static {v0, p1}, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;->access$000(Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;Landroid/os/Message;)V

    .line 343
    :cond_0
    return-void
.end method
