.class Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$CustomGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "HeadlinesPortraitMainContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)V
    .locals 0

    .prologue
    .line 965
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$CustomGestureListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;
    .param p2, "x1"    # Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$1;

    .prologue
    .line 965
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$CustomGestureListener;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "first"    # Landroid/view/MotionEvent;
    .param p2, "current"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 970
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v0, v1

    .line 979
    :goto_0
    return v0

    .line 973
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$CustomGestureListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I
    invoke-static {v3}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->access$400(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 974
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$CustomGestureListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;

    # setter for: Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mIsHomeLaunching:Z
    invoke-static {v2, v0}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->access$502(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;Z)Z

    .line 975
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer$CustomGestureListener;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesPortraitMainContainer;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->startLauncher(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 979
    goto :goto_0
.end method
