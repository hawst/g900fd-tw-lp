.class Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$1;
.super Ljava/lang/Object;
.source "HeadlinesSettingView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->initTitleView(Landroid/view/View;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

.field final synthetic val$parent:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$1;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    iput-object p2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$1;->val$parent:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 157
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$1;->val$parent:Landroid/view/View;

    const v6, 0x7f0c003b

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    .line 158
    .local v1, "gridView":Landroid/widget/GridView;
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$1;->val$parent:Landroid/view/View;

    const v6, 0x7f0c0039

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 160
    .local v2, "sectionImage":Landroid/widget/ImageView;
    invoke-virtual {v1}, Landroid/widget/GridView;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    move v0, v3

    .line 161
    .local v0, "expand":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 162
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/widget/GridView;->setVisibility(I)V

    .line 167
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView$1;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;

    if-nez v0, :cond_2

    :goto_2
    # invokes: Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->getExpanderImage(Z)Landroid/graphics/drawable/Drawable;
    invoke-static {v5, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;->access$000(Lcom/samsung/android/app/headlines/view/HeadlinesSettingView;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 168
    return-void

    .end local v0    # "expand":Z
    :cond_0
    move v0, v4

    .line 160
    goto :goto_0

    .line 164
    .restart local v0    # "expand":Z
    :cond_1
    invoke-virtual {v1, v4}, Landroid/widget/GridView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    move v3, v4

    .line 167
    goto :goto_2
.end method
