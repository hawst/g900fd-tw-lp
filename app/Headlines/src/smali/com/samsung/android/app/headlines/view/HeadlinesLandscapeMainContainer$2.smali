.class Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;
.super Ljava/lang/Object;
.source "HeadlinesLandscapeMainContainer.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Lcom/sec/android/touchwiz/widget/TwAbsListView;III)V
    .locals 10
    .param p1, "view"    # Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 148
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mAddedCategoryList:Ljava/util/List;
    invoke-static {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 149
    move v5, p2

    .local v5, "i":I
    :goto_0
    add-int v7, p2, p3

    if-ge v5, v7, :cond_2

    .line 150
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;
    invoke-static {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    move-result-object v7

    sub-int v8, v5, p2

    invoke-virtual {v7, v8}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 151
    .local v2, "cardView":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 152
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    .line 153
    .local v4, "displaymetrics":Landroid/util/DisplayMetrics;
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$300(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 154
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;
    invoke-static {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$400(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getY()F

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mHeaderView:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;
    invoke-static {v8}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$400(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->getHeight()I

    move-result v8

    int-to-float v8, v8

    add-float v3, v7, v8

    .line 156
    .local v3, "displayedHeaderHeight":F
    invoke-virtual {v2}, Landroid/view/View;->getY()F

    move-result v7

    iget v8, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v8, v8

    sub-float/2addr v8, v3

    cmpg-float v7, v7, v8

    if-gez v7, :cond_2

    .line 157
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mContentListView:Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;
    invoke-static {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$100(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/samsung/android/app/headlines/view/HeadlinesContentListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    .line 158
    .local v0, "card":Ljava/lang/Object;
    if-eqz v0, :cond_1

    instance-of v7, v0, Lcom/samsung/android/magazine/cardchannel/Card;

    if-eqz v7, :cond_1

    move-object v7, v0

    .line 159
    check-cast v7, Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v7}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v1

    .line 160
    .local v1, "cardId":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 161
    .local v6, "id":Ljava/lang/Integer;
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mAddedCategoryList:Ljava/util/List;
    invoke-static {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 162
    const-string v7, "HeadlinesLandscapeMainContainer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setOnScrollListener() : Remove from addedCategoryList. Cardid = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " cardName = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/Card;

    .end local v0    # "card":Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    # getter for: Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->mAddedCategoryList:Ljava/util/List;
    invoke-static {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->access$200(Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 165
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$2;->this$0:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;

    invoke-virtual {v7}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;->hideNotificationButton()V

    .line 149
    .end local v1    # "cardId":I
    .end local v3    # "displayedHeaderHeight":F
    .end local v4    # "displaymetrics":Landroid/util/DisplayMetrics;
    .end local v6    # "id":Ljava/lang/Integer;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 173
    .end local v2    # "cardView":Landroid/view/View;
    .end local v5    # "i":I
    :cond_2
    return-void
.end method

.method public onScrollStateChanged(Lcom/sec/android/touchwiz/widget/TwAbsListView;I)V
    .locals 0
    .param p1, "view"    # Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 177
    return-void
.end method
