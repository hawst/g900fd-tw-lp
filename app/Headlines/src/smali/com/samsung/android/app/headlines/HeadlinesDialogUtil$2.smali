.class final Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;
.super Ljava/lang/Object;
.source "HeadlinesDialogUtil.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->dialogDataAlertPopup(Landroid/app/Activity;ILcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/Object;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$action_type:I

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$card:Lcom/samsung/android/magazine/cardchannel/Card;

.field final synthetic val$obj:Ljava/lang/Object;


# direct methods
.method constructor <init>(Landroid/app/Activity;ILcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$activity:Landroid/app/Activity;

    iput p2, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$action_type:I

    iput-object p3, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$card:Lcom/samsung/android/magazine/cardchannel/Card;

    iput-object p4, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$obj:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 14
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 152
    const/4 v2, 0x0

    .line 153
    .local v2, "checked":Z
    # getter for: Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mDontShowAgain:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->access$000()Landroid/widget/CheckBox;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 154
    const/4 v2, 0x1

    .line 156
    :cond_0
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$activity:Landroid/app/Activity;

    const-string v12, "mymagazine"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 157
    .local v8, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 159
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    sget v11, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mDataType:I

    if-nez v11, :cond_3

    .line 160
    const-string v11, "MOBILE_DATA_USE_ALLOWED"

    invoke-interface {v4, v11, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 163
    :goto_0
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 165
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 166
    .local v5, "i":Landroid/content/Intent;
    const-string v11, "com.samsung.android.internal.headlines.ALLOW_DATA_USE"

    invoke-virtual {v5, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    const-string v11, "DataDoNotShowChecked"

    invoke-virtual {v5, v11, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 168
    const/4 v11, 0x1

    if-ne v2, v11, :cond_1

    .line 169
    const-string v11, "DataCheckedType"

    sget v12, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mDataType:I

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 171
    :cond_1
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v11, v5}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 173
    iget v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$action_type:I

    packed-switch v11, :pswitch_data_0

    .line 254
    :cond_2
    :goto_1
    :pswitch_0
    const/4 v11, 0x0

    # setter for: Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mIsPopupDisplaying:Z
    invoke-static {v11}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->access$202(Z)Z

    .line 255
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 256
    return-void

    .line 162
    .end local v5    # "i":Landroid/content/Intent;
    :cond_3
    const-string v11, "WLAN_DATA_USE_ALLOWED"

    invoke-interface {v4, v11, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 176
    .restart local v5    # "i":Landroid/content/Intent;
    :pswitch_1
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 177
    .local v7, "sIntent":Landroid/content/Intent;
    const-string v11, "com.samsung.android.internal.headlines.UPDATE_WELCOME_CARD"

    invoke-virtual {v7, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v11, v7}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 182
    .end local v7    # "sIntent":Landroid/content/Intent;
    :pswitch_2
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$card:Lcom/samsung/android/magazine/cardchannel/Card;

    if-eqz v11, :cond_2

    .line 185
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$obj:Ljava/lang/Object;

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$obj:Ljava/lang/Object;

    instance-of v11, v11, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    .line 186
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->getInstance()Lcom/samsung/android/app/headlines/HeadlinesCardManager;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$card:Lcom/samsung/android/magazine/cardchannel/Card;

    invoke-virtual {v12}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardId()I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/samsung/android/app/headlines/HeadlinesCardManager;->requestRefresh(I)V

    .line 187
    iget-object v10, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$obj:Ljava/lang/Object;

    check-cast v10, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;

    .line 188
    .local v10, "view":Lcom/samsung/android/app/headlines/view/HeadlinesCardView;
    invoke-virtual {v10}, Lcom/samsung/android/app/headlines/view/HeadlinesCardView;->showLoadingViews()V

    goto :goto_1

    .line 193
    .end local v10    # "view":Lcom/samsung/android/app/headlines/view/HeadlinesCardView;
    :pswitch_3
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$card:Lcom/samsung/android/magazine/cardchannel/Card;

    if-eqz v11, :cond_2

    .line 196
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$card:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v12, "action1"

    invoke-virtual {v11, v12}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardAction(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardAction;

    move-result-object v0

    .line 197
    .local v0, "action1":Lcom/samsung/android/magazine/cardchannel/CardAction;
    if-eqz v0, :cond_2

    .line 198
    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardAction;->getData()Landroid/content/Intent;

    move-result-object v6

    .line 199
    .local v6, "intent":Landroid/content/Intent;
    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardAction;->getActionType()Ljava/lang/String;

    move-result-object v9

    .line 201
    .local v9, "type":Ljava/lang/String;
    if-eqz v9, :cond_2

    if-eqz v6, :cond_2

    .line 202
    const-string v11, "activity"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 203
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v11, v6}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 204
    :cond_4
    const-string v11, "service"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 205
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v11, v6}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1

    .line 206
    :cond_5
    const-string v11, "broadcast"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 207
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v11, v6}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 214
    .end local v0    # "action1":Lcom/samsung/android/magazine/cardchannel/CardAction;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v9    # "type":Ljava/lang/String;
    :pswitch_4
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$card:Lcom/samsung/android/magazine/cardchannel/Card;

    if-eqz v11, :cond_2

    .line 217
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$card:Lcom/samsung/android/magazine/cardchannel/Card;

    const-string v12, "action2"

    invoke-virtual {v11, v12}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardAction(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardAction;

    move-result-object v1

    .line 218
    .local v1, "action2":Lcom/samsung/android/magazine/cardchannel/CardAction;
    if-eqz v1, :cond_2

    .line 219
    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardAction;->getData()Landroid/content/Intent;

    move-result-object v6

    .line 220
    .restart local v6    # "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardAction;->getActionType()Ljava/lang/String;

    move-result-object v9

    .line 222
    .restart local v9    # "type":Ljava/lang/String;
    if-eqz v9, :cond_2

    if-eqz v6, :cond_2

    .line 223
    const-string v11, "activity"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 224
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v11, v6}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 225
    :cond_6
    const-string v11, "service"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 226
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v11, v6}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_1

    .line 227
    :cond_7
    const-string v11, "broadcast"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 228
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v11, v6}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 236
    .end local v1    # "action2":Lcom/samsung/android/magazine/cardchannel/CardAction;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v9    # "type":Ljava/lang/String;
    :pswitch_5
    # getter for: Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mHandlerPullToRefresh:Landroid/os/Handler;
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->access$100()Landroid/os/Handler;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 237
    # getter for: Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mHandlerPullToRefresh:Landroid/os/Handler;
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->access$100()Landroid/os/Handler;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 238
    const/4 v11, 0x0

    # setter for: Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->mHandlerPullToRefresh:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil;->access$102(Landroid/os/Handler;)Landroid/os/Handler;

    goto/16 :goto_1

    .line 247
    :pswitch_6
    :try_start_0
    new-instance v6, Landroid/content/Intent;

    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$activity:Landroid/app/Activity;

    const-class v12, Lcom/samsung/android/app/headlines/activity/HeadlinesTermsOfUseActivity;

    invoke-direct {v6, v11, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 248
    .restart local v6    # "intent":Landroid/content/Intent;
    iget-object v11, p0, Lcom/samsung/android/app/headlines/HeadlinesDialogUtil$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v11, v6}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 249
    .end local v6    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v3

    .line 250
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 173
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method
