.class public Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;
.super Landroid/os/AsyncTask;
.source "CardTemplateUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/template/CardTemplateUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DecodeIconBitmapTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field mUri:Ljava/lang/String;

.field mView:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 210
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 206
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mContext:Landroid/content/Context;

    .line 207
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mUri:Ljava/lang/String;

    .line 208
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mView:Landroid/widget/ImageView;

    .line 211
    iput-object p1, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mContext:Landroid/content/Context;

    .line 212
    iput-object p2, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mUri:Ljava/lang/String;

    .line 213
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "v"    # Landroid/widget/ImageView;

    .prologue
    const/4 v0, 0x0

    .line 215
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 206
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mContext:Landroid/content/Context;

    .line 207
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mUri:Ljava/lang/String;

    .line 208
    iput-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mView:Landroid/widget/ImageView;

    .line 216
    iput-object p1, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mContext:Landroid/content/Context;

    .line 217
    iput-object p2, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mUri:Ljava/lang/String;

    .line 218
    iput-object p3, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mView:Landroid/widget/ImageView;

    .line 219
    return-void
.end method

.method private setDrawable(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 281
    if-eqz p1, :cond_0

    .line 282
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 286
    :goto_0
    return-void

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mView:Landroid/widget/ImageView;

    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v6, 0x0

    .line 223
    const/4 v1, 0x0

    .line 225
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mUri:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 226
    const-string v5, "CardTemplateUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getIconBitmap : img URI = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mUri:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :try_start_0
    # getter for: Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;
    invoke-static {}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->access$000()Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    move-result-object v5

    iget-object v7, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mUri:Ljava/lang/String;

    invoke-virtual {v5, v7}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    .line 230
    if-eqz v1, :cond_0

    move-object v5, v1

    .line 262
    :goto_0
    return-object v5

    .line 233
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f080053

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 234
    .local v3, "maxSize":I
    const-string v5, "CardTemplateUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "bitmap max size "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080053

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_3

    .line 237
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v7, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 238
    .local v4, "src":Landroid/graphics/Bitmap;
    int-to-float v5, v3

    const/4 v7, 0x0

    invoke-static {v4, v5, v7}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->scaleBitmap(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    .line 248
    :goto_1
    if-nez v1, :cond_1

    move-object v5, v6

    .line 249
    goto :goto_0

    .line 239
    .end local v4    # "src":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v2

    .line 240
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 241
    const-string v5, "CardTemplateUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "decodeBitmapTask cache trim size. current size : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # getter for: Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;
    invoke-static {}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->access$000()Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    # getter for: Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;
    invoke-static {}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->access$000()Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->trimToSize(I)V

    .line 244
    iget-object v5, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v7, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 245
    .restart local v4    # "src":Landroid/graphics/Bitmap;
    int-to-float v5, v3

    const/4 v7, 0x0

    invoke-static {v4, v5, v7}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->scaleBitmap(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v1

    goto :goto_1

    .line 250
    .end local v2    # "e":Ljava/lang/OutOfMemoryError;
    .end local v3    # "maxSize":I
    .end local v4    # "src":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v2

    .line 251
    .local v2, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v5, v6

    .line 252
    goto/16 :goto_0

    .line 253
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v2

    .line 254
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .line 255
    goto/16 :goto_0

    .line 256
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 257
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    move-object v5, v6

    .line 258
    goto/16 :goto_0

    .end local v2    # "e":Ljava/lang/OutOfMemoryError;
    :cond_1
    move-object v5, v1

    .line 262
    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 205
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 267
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 269
    if-eqz p1, :cond_0

    .line 270
    # getter for: Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;
    invoke-static {}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->access$000()Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 271
    # getter for: Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;
    invoke-static {}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->access$000()Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mUri:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->mView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 276
    invoke-direct {p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->setDrawable(Landroid/graphics/Bitmap;)V

    .line 278
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 205
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
