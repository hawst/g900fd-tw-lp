.class public Lcom/samsung/android/app/headlines/template/CardTemplateUtil;
.super Ljava/lang/Object;
.source "CardTemplateUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;,
        Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CardTemplateUtil"

.field private static mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 29
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v4

    long-to-int v1, v4

    int-to-long v2, v1

    .line 30
    .local v2, "totalAppHeap":J
    long-to-int v1, v2

    div-int/lit8 v0, v1, 0x3

    .line 31
    .local v0, "runtimeCacheLimit":I
    new-instance v1, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    invoke-direct {v1, v0}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;-><init>(I)V

    sput-object v1, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    .line 32
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    return-void
.end method

.method static synthetic access$000()Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    return-object v0
.end method

.method static synthetic access$100(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # Landroid/widget/ImageView;
    .param p3, "x3"    # Landroid/widget/ImageView;

    .prologue
    .line 25
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->setImage(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    return-void
.end method

.method protected static getBackgroundColor(Lcom/samsung/android/magazine/cardchannel/Card;)I
    .locals 6
    .param p0, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    .line 84
    const/4 v1, 0x0

    .line 86
    .local v1, "color":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/magazine/cardchannel/Card;->getAttributes()Ljava/util/Map;

    move-result-object v0

    .line 87
    .local v0, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 88
    const-string v4, "backgroundColor"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "color":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 91
    .restart local v1    # "color":Ljava/lang/String;
    :cond_0
    const v3, 0x7f07000b

    .line 93
    .local v3, "intColor":I
    if-eqz v1, :cond_1

    :try_start_0
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 98
    :goto_0
    return v3

    .line 93
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 94
    :catch_0
    move-exception v2

    .line 95
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "CardTemplateUtil"

    const-string v5, "getBackgroundColor() : Color is not valid."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getCacheBitmap(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .param p1, "elementKey"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardElement(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardElement;

    move-result-object v1

    .line 51
    .local v1, "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    const/4 v0, 0x0

    .line 52
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .line 53
    .local v2, "uri":Ljava/lang/String;
    instance-of v3, v1, Lcom/samsung/android/magazine/cardchannel/CardImage;

    if-eqz v3, :cond_0

    .line 54
    check-cast v1, Lcom/samsung/android/magazine/cardchannel/CardImage;

    .end local v1    # "cardImage":Lcom/samsung/android/magazine/cardchannel/CardElement;
    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardImage;->getImageUri()Ljava/lang/String;

    move-result-object v2

    .line 55
    if-eqz v2, :cond_0

    .line 56
    sget-object v3, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    invoke-virtual {v3, v2}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .line 59
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-object v0
.end method

.method public static getIconImage(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "iconUri"    # Ljava/lang/String;
    .param p2, "v"    # Landroid/widget/ImageView;

    .prologue
    .line 303
    const/4 v0, 0x0

    .line 305
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_0

    .line 306
    sget-object v2, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    invoke-virtual {v2, p1}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .line 309
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    if-eqz v0, :cond_1

    .line 310
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 315
    :goto_0
    return-void

    .line 312
    :cond_1
    new-instance v1, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 313
    .local v1, "task":Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public static getImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .param p2, "elementKey"    # Ljava/lang/String;
    .param p3, "v"    # Landroid/widget/ImageView;

    .prologue
    .line 174
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 175
    return-void
.end method

.method public static getImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .param p2, "elementKey"    # Ljava/lang/String;
    .param p3, "v"    # Landroid/widget/ImageView;
    .param p4, "gradientView"    # Landroid/widget/ImageView;

    .prologue
    const/4 v3, 0x0

    .line 178
    invoke-static {p1, p2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->getCacheBitmap(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 179
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 180
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 181
    invoke-virtual {p3, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 182
    if-eqz p4, :cond_0

    .line 183
    invoke-virtual {p4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    new-instance v1, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;

    invoke-direct {v1, p0, p2, p3, p4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 186
    .local v1, "task":Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/samsung/android/magazine/cardchannel/Card;

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public static getText(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 4
    .param p0, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .param p1, "elementKey"    # Ljava/lang/String;

    .prologue
    .line 35
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 37
    .local v1, "text":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardElement(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardElement;

    move-result-object v0

    .line 38
    .local v0, "cardText":Lcom/samsung/android/magazine/cardchannel/CardElement;
    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/samsung/android/magazine/cardchannel/CardText;

    if-eqz v2, :cond_0

    .line 39
    check-cast v0, Lcom/samsung/android/magazine/cardchannel/CardText;

    .end local v0    # "cardText":Lcom/samsung/android/magazine/cardchannel/CardElement;
    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardText;->getText()Ljava/lang/String;

    move-result-object v1

    .line 42
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 45
    .end local v1    # "text":Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v1    # "text":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getTextColor(Lcom/samsung/android/magazine/cardchannel/Card;Ljava/lang/String;)I
    .locals 7
    .param p0, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;
    .param p1, "elementKey"    # Ljava/lang/String;

    .prologue
    .line 63
    const/4 v2, 0x0

    .line 65
    .local v2, "color":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/samsung/android/magazine/cardchannel/Card;->getCardElement(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardElement;

    move-result-object v1

    .line 66
    .local v1, "cardText":Lcom/samsung/android/magazine/cardchannel/CardElement;
    instance-of v5, v1, Lcom/samsung/android/magazine/cardchannel/CardText;

    if-eqz v5, :cond_0

    .line 67
    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardElement;->getAttributes()Ljava/util/Map;

    move-result-object v0

    .line 68
    .local v0, "attribute":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 69
    const-string v5, "backgroundColor"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "color":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 73
    .end local v0    # "attribute":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v2    # "color":Ljava/lang/String;
    :cond_0
    const/high16 v4, -0x1000000

    .line 75
    .local v4, "intColor":I
    if-eqz v2, :cond_1

    :try_start_0
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 80
    :goto_0
    return v4

    .line 75
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 76
    :catch_0
    move-exception v3

    .line 77
    .local v3, "e":Ljava/lang/Exception;
    const-string v5, "CardTemplateUtil"

    const-string v6, "getBackgroundColor() : Color is not valid."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static preloadImage(Landroid/content/Context;Lcom/samsung/android/magazine/cardchannel/Card;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "card"    # Lcom/samsung/android/magazine/cardchannel/Card;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 352
    new-instance v0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;

    const-string v3, "image1"

    invoke-direct {v0, p0, v3, v4, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 353
    .local v0, "task1":Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;
    new-array v3, v6, [Lcom/samsung/android/magazine/cardchannel/Card;

    aput-object p1, v3, v5

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 354
    new-instance v1, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;

    const-string v3, "image2"

    invoke-direct {v1, p0, v3, v4, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 355
    .local v1, "task2":Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;
    new-array v3, v6, [Lcom/samsung/android/magazine/cardchannel/Card;

    aput-object p1, v3, v5

    invoke-virtual {v1, v3}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 356
    new-instance v2, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;

    const-string v3, "background"

    invoke-direct {v2, p0, v3, v4, v4}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 357
    .local v2, "task3":Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;
    new-array v3, v6, [Lcom/samsung/android/magazine/cardchannel/Card;

    aput-object p1, v3, v5

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$decodeBitmapTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 358
    return-void
.end method

.method public static scaleBitmap(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "maxImageSize"    # F
    .param p2, "filter"    # Z

    .prologue
    .line 318
    if-nez p0, :cond_0

    .line 319
    const/4 p0, 0x0

    .line 335
    .end local p0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object p0

    .line 321
    .restart local p0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float v4, p1, v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float v5, p1, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 323
    .local v2, "ratio":F
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v2, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-nez v4, :cond_1

    .line 324
    const-string v4, "CardTemplateUtil"

    const-string v5, "no need to scale"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 328
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v2

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 329
    .local v3, "width":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v2

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 331
    .local v0, "height":I
    invoke-static {p0, v3, v0, p2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 333
    .local v1, "newBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 334
    const-string v4, "CardTemplateUtil"

    const-string v5, "scaled bitmap"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object p0, v1

    .line 335
    goto :goto_0
.end method

.method public static setIconImage(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "iconUri"    # Ljava/lang/String;

    .prologue
    .line 290
    const/4 v0, 0x0

    .line 292
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_0

    .line 293
    sget-object v2, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    invoke-virtual {v2, p1}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .line 296
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    if-nez v0, :cond_1

    .line 297
    new-instance v1, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 298
    .local v1, "task":Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 300
    .end local v1    # "task":Lcom/samsung/android/app/headlines/template/CardTemplateUtil$DecodeIconBitmapTask;
    :cond_1
    return-void
.end method

.method private static setImage(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "v"    # Landroid/widget/ImageView;
    .param p3, "gradientView"    # Landroid/widget/ImageView;

    .prologue
    const/4 v0, 0x0

    .line 192
    if-eqz p2, :cond_0

    .line 195
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 196
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    :cond_0
    if-eqz p3, :cond_1

    .line 201
    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 203
    :cond_1
    return-void
.end method

.method public static trim(I)V
    .locals 2
    .param p0, "level"    # I

    .prologue
    .line 339
    sget-object v0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    if-nez v0, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    const/16 v0, 0x3c

    if-lt p0, v0, :cond_2

    .line 343
    sget-object v0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    invoke-virtual {v0}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->evictAll()V

    goto :goto_0

    .line 345
    :cond_2
    const/16 v0, 0x28

    if-lt p0, v0, :cond_0

    .line 346
    sget-object v0, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    sget-object v1, Lcom/samsung/android/app/headlines/template/CardTemplateUtil;->mCache:Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;

    invoke-virtual {v1}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->size()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/headlines/template/BitmapLruImageCache;->trimToSize(I)V

    goto :goto_0
.end method
