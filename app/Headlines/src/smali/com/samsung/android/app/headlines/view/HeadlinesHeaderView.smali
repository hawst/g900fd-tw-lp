.class public Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;
.super Landroid/widget/RelativeLayout;
.source "HeadlinesHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$1;,
        Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HeadlinesHeaderView"


# instance fields
.field private mActionBarListener:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;

.field private mContext:Landroid/content/Context;

.field private mIsPopupShown:Z

.field private mMoreMenu:Landroid/widget/PopupMenu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mIsPopupShown:Z

    .line 31
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    .line 32
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;

    .line 33
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$1;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mActionBarListener:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;

    .line 37
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    .line 39
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->init()V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mIsPopupShown:Z

    .line 31
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    .line 32
    iput-object v1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;

    .line 33
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;-><init>(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$1;)V

    iput-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mActionBarListener:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;

    .line 44
    iput-object p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    .line 46
    invoke-direct {p0}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->init()V

    .line 47
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mIsPopupShown:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mIsPopupShown:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;)Landroid/widget/PopupMenu;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;

    return-object v0
.end method

.method private init()V
    .locals 5

    .prologue
    .line 50
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 51
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030007

    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 55
    const v2, 0x7f0c001e

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 56
    .local v0, "buttonMore":Landroid/widget/ImageButton;
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mActionBarListener:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    new-instance v2, Landroid/widget/PopupMenu;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    const v4, 0x800005

    invoke-direct {v2, v3, v0, v4}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    iput-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;

    .line 58
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;

    const/high16 v3, 0x7f0b0000

    invoke-virtual {v2, v3}, Landroid/widget/PopupMenu;->inflate(I)V

    .line 59
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mActionBarListener:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;

    invoke-virtual {v2, v3}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 60
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mActionBarListener:Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView$ActionBarEventListener;

    invoke-virtual {v2, v3}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 66
    return-void
.end method


# virtual methods
.method public dismissMoreMenu()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 168
    :cond_0
    return-void
.end method

.method public isPopupShown()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mIsPopupShown:Z

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 176
    const/4 v0, 0x1

    return v0
.end method

.method public openAbout()V
    .locals 4

    .prologue
    .line 79
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    const-class v3, Lcom/samsung/android/app/headlines/activity/HeadlinesAboutActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 80
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public openHelp()V
    .locals 6

    .prologue
    .line 90
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.helphub"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 92
    .local v1, "info":Landroid/content/pm/PackageInfo;
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 108
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return-void

    .line 96
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_1
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 97
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.samsung.helphub.HELP"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 98
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "helphub:section"

    const-string v4, "homescreen"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 105
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    .end local v2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 100
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    :cond_2
    :try_start_1
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 101
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.samsung.helphub.HELP"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 102
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v3, "helphub:appid"

    const-string v4, "home_screen"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    iget-object v3, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public openSetting()V
    .locals 4

    .prologue
    .line 70
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    const-class v3, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 71
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public showMoreMenu()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 153
    iget-boolean v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mIsPopupShown:Z

    if-eq v0, v2, :cond_1

    .line 154
    const-string v0, "HeadlinesHeaderView"

    const-string v1, "onClick(): Show POPUP"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mMoreMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    .line 159
    :cond_0
    iput-boolean v2, p0, Lcom/samsung/android/app/headlines/view/HeadlinesHeaderView;->mIsPopupShown:Z

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_1
    const-string v0, "HeadlinesHeaderView"

    const-string v1, "onClick(): POPUP is already shown."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
