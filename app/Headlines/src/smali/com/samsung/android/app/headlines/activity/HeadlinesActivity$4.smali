.class Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$4;
.super Ljava/lang/Object;
.source "HeadlinesActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->dialogCustomizeSettingPopup(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$4;->this$0:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;

    iput-object p2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$4;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 389
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$4;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 390
    .local v0, "doNotShow":Z
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$4;->this$0:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;

    const-string v4, "mymagazine"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 391
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 392
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "DO_NOT_SHOW_CUSTOMIZE_SETTING_POPUP_SHOW_FOR_ATT"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 393
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 394
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 395
    return-void
.end method
