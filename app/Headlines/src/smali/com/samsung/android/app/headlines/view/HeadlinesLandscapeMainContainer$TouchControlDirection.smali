.class final enum Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;
.super Ljava/lang/Enum;
.source "HeadlinesLandscapeMainContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TouchControlDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

.field public static final enum STATE_FLICK_HORIZONTAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

.field public static final enum STATE_INITIAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

.field public static final enum STATE_SCROLL_VERTICAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 91
    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    const-string v1, "STATE_INITIAL"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_INITIAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    const-string v1, "STATE_SCROLL_VERTICAL"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_SCROLL_VERTICAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    new-instance v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    const-string v1, "STATE_FLICK_HORIZONTAL"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_FLICK_HORIZONTAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    .line 90
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_INITIAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_SCROLL_VERTICAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->STATE_FLICK_HORIZONTAL:Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->$VALUES:[Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 90
    const-class v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->$VALUES:[Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    invoke-virtual {v0}, [Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/headlines/view/HeadlinesLandscapeMainContainer$TouchControlDirection;

    return-object v0
.end method
