.class Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity$FlickGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "HeadlinesWelcomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FlickGestureListener"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity$FlickGestureListener;->this$0:Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 155
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity$FlickGestureListener;->mContext:Landroid/content/Context;

    .line 158
    iput-object p2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity$FlickGestureListener;->mContext:Landroid/content/Context;

    .line 159
    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "event1"    # Landroid/view/MotionEvent;
    .param p2, "event2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v2, 0x0

    .line 163
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v2

    .line 166
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 167
    .local v0, "e1X":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 169
    .local v1, "e2X":F
    sub-float v3, v0, v1

    iget-object v4, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity$FlickGestureListener;->this$0:Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;

    # getter for: Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->MIN_LENGTH_X_FOR_LAUNCHING_HOME:I
    invoke-static {v4}, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;->access$000(Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity;)I

    move-result v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 170
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesWelcomeActivity$FlickGestureListener;->mContext:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->startLauncher(Landroid/content/Context;Z)V

    .line 171
    const/4 v2, 0x1

    goto :goto_0
.end method
