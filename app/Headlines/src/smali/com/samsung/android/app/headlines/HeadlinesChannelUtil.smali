.class public Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;
.super Ljava/lang/Object;
.source "HeadlinesChannelUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;
    }
.end annotation


# static fields
.field public static final COUNTRY_CHINA:Ljava/lang/String; = "CN"

.field public static final COUNTRY_TAIWAN:Ljava/lang/String; = "TW"

.field private static final LAUNCHER_CLASS_NAME:Ljava/lang/String; = "com.android.launcher2.Launcher"

.field private static final LAUNCHER_EASY_HOME_CLASS_NAME:Ljava/lang/String; = "com.sec.android.app.easylauncher.Launcher"

.field private static final LAUNCHER_EASY_HOME_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.easylauncher"

.field private static final LAUNCHER_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.launcher"

.field public static final MODEL_M2:Ljava/lang/String; = "SM-C115"

.field public static final NEWS_ARTS:Ljava/lang/String; = "news arts"

.field public static final NEWS_BOOKS:Ljava/lang/String; = "news books"

.field public static final NEWS_BUSINESS:Ljava/lang/String; = "news business"

.field public static final NEWS_BY:Ljava/lang/String; = "news by"

.field public static final NEWS_ENGLISHONLY:Ljava/lang/String; = "news englishonly"

.field public static final NEWS_FIRSTLAUNCH:Ljava/lang/String; = "news firstlaunch"

.field public static final NEWS_FOOD:Ljava/lang/String; = "news food"

.field public static final NEWS_LIVING:Ljava/lang/String; = "news living"

.field public static final NEWS_LOCAL:Ljava/lang/String; = "news local"

.field public static final NEWS_MUSIC:Ljava/lang/String; = "news music"

.field public static final NEWS_NETHERLANDS:Ljava/lang/String; = "news netherlands"

.field public static final NEWS_NEW:Ljava/lang/String; = "news new"

.field public static final NEWS_NEWS:Ljava/lang/String; = "news news"

.field public static final NEWS_PHOTOS:Ljava/lang/String; = "news photos"

.field public static final NEWS_SPORT:Ljava/lang/String; = "news sports"

.field public static final NEWS_STYLE:Ljava/lang/String; = "news style"

.field public static final NEWS_TECH:Ljava/lang/String; = "news tech"

.field public static final NEWS_TRAVEL:Ljava/lang/String; = "news travel"

.field public static final OPERATOR_ATT:Ljava/lang/String; = "ATT"

.field public static final OPERATOR_CHM:Ljava/lang/String; = "CHM"

.field public static final OPERATOR_LRA:Ljava/lang/String; = "LRA"

.field public static final OPERATOR_SKT:Ljava/lang/String; = "SKC"

.field public static final OPERATOR_VZW:Ljava/lang/String; = "VZW"

.field public static final SOCIAL_FIVEHUNDREDPX:Ljava/lang/String; = "social fivehundredpx"

.field public static final SOCIAL_FLICKR:Ljava/lang/String; = "social flickr"

.field public static final SOCIAL_GOOGLEPLUS:Ljava/lang/String; = "social googleplus"

.field public static final SOCIAL_LINKEDIN:Ljava/lang/String; = "social linkedin"

.field public static final SOCIAL_RENREN:Ljava/lang/String; = "social renren"

.field public static final SOCIAL_TUMBLR:Ljava/lang/String; = "social tumblr"

.field public static final SOCIAL_TWITTER:Ljava/lang/String; = "social twitter"

.field public static final SOCIAL_WEIBO:Ljava/lang/String; = "social weibo"

.field public static final SOCIAL_YOUTUBE:Ljava/lang/String; = "social youtube"

.field public static final TAG:Ljava/lang/String; = "HeadlinesChannelUtil"

.field public static resourceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final typefaceRobotoRegular:Landroid/graphics/Typeface;

.field public static final typefaceSamsungSansBold:Landroid/graphics/Typeface;

.field public static final typefaceSamsungSansLight:Landroid/graphics/Typeface;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 38
    const-string v0, "/system/fonts/Roboto-Regular.ttf"

    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->typefaceRobotoRegular:Landroid/graphics/Typeface;

    .line 39
    const-string v0, "/system/fonts/SamsungSans-Bold.ttf"

    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->typefaceSamsungSansBold:Landroid/graphics/Typeface;

    .line 40
    const-string v0, "/system/fonts/SamsungSans-Light.ttf"

    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->typefaceSamsungSansLight:Landroid/graphics/Typeface;

    .line 95
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    .line 97
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news news"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090018

    const v4, 0x7f020050

    const v5, 0x7f02003c

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news tech"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f09001a

    const v4, 0x7f020058

    const v5, 0x7f020043

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news new"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090017

    const v4, 0x7f02004f

    const v5, 0x7f02003b

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news sports"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f09001b

    const v4, 0x7f020055

    const v5, 0x7f020041

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news business"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090019

    const v4, 0x7f020009

    const v5, 0x7f020031

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news photos"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f09001c

    const v4, 0x7f020051

    const v5, 0x7f02003d

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news style"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090021

    const v4, 0x7f020057

    const v5, 0x7f020042

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news arts"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f09001d

    const v4, 0x7f020005

    const v5, 0x7f02002f

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news living"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f09001e

    const v4, 0x7f02001e

    const v5, 0x7f020037

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news music"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090022

    const v4, 0x7f02004b

    const v5, 0x7f020039

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news by"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f09002f

    const v4, 0x7f02005b

    const v5, 0x7f02003e

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news travel"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090020

    const v4, 0x7f02005c

    const v5, 0x7f020044

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news books"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090023

    const v4, 0x7f020008

    const v5, 0x7f020030

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news englishonly"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090024

    const v4, 0x7f02000b

    const v5, 0x7f020032

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news food"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f09001f

    const v4, 0x7f02000c

    const v5, 0x7f020034

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news netherlands"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090025

    const v4, 0x7f02004e

    const v5, 0x7f02003a

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news firstlaunch"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090026

    const v4, 0x7f02007a

    const v5, 0x7f020047

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "news local"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090027

    const v4, 0x7f02001f

    const v5, 0x7f020038

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "social twitter"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090037

    const v4, 0x7f020018

    const v5, 0x7f020046

    invoke-direct {v2, v3, v6, v4, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "social googleplus"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090032

    const v4, 0x7f020014

    const v5, 0x7f020035

    invoke-direct {v2, v3, v6, v4, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "social linkedin"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090033

    const v4, 0x7f020015

    const v5, 0x7f020036

    invoke-direct {v2, v3, v6, v4, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "social flickr"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090031

    const v4, 0x7f020013

    const v5, 0x7f020033

    invoke-direct {v2, v3, v6, v4, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "social tumblr"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090036

    const v4, 0x7f020017

    const v5, 0x7f020045

    invoke-direct {v2, v3, v6, v4, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "social fivehundredpx"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090030

    const v4, 0x7f020012

    const v5, 0x7f02002e

    invoke-direct {v2, v3, v6, v4, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "social weibo"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090035

    const v4, 0x7f020019

    const v5, 0x7f020040

    invoke-direct {v2, v3, v6, v4, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "social renren"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090034

    const v4, 0x7f020016

    const v5, 0x7f02003f

    invoke-direct {v2, v3, v6, v4, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    const-string v1, "social youtube"

    new-instance v2, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    const v3, 0x7f090038

    const v4, 0x7f02001a

    const v5, 0x7f020048

    invoke-direct {v2, v3, v6, v4, v5}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    return-void
.end method

.method public static getCardDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cardName"    # Ljava/lang/String;

    .prologue
    .line 187
    sget-object v1, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    .line 188
    .local v0, "res":Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;
    if-nez v0, :cond_0

    .end local p1    # "cardName":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "cardName":Ljava/lang/String;
    :cond_0
    iget v1, v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;->mCardName:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private static getCountryCode()Ljava/lang/String;
    .locals 9

    .prologue
    .line 135
    const/4 v2, 0x0

    .line 138
    .local v2, "countryCode":Ljava/lang/String;
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 139
    .local v1, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 140
    .local v4, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "ro.csc.countryiso_code"

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 141
    const-string v5, "HeadlinesChannelUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getCountryCode(): countryCode = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    .end local v1    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 142
    :catch_0
    move-exception v3

    .line 143
    .local v3, "e":Ljava/lang/Exception;
    const-string v5, "HeadlinesChannelUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSalesCode failed"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getDefaultCardNames()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    const-string v2, "HeadlinesChannelUtil"

    const-string v3, "getDefaultCardNames(): subscribe default card names."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 161
    .local v0, "cardNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "SM-C115"

    invoke-static {v2}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->isModel(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 162
    const-string v2, "HeadlinesChannelUtil"

    const-string v3, "getDefaultCardNames(): M2(SM-C115) model."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    const-string v2, "news photos"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    const-string v2, "news arts"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    const-string v2, "news news"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    const-string v2, "news business"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    const-string v2, "news new"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    const-string v2, "news sports"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    .end local v0    # "cardNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v0

    .line 170
    .restart local v0    # "cardNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    const-string v2, "HeadlinesChannelUtil"

    const-string v3, "getDefaultCardNames(): open model."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const-string v2, "news arts"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    const-string v2, "news news"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    const-string v2, "news business"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    const-string v2, "news new"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    const-string v2, "news photos"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    const-string v2, "news sports"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 180
    :catch_0
    move-exception v1

    .line 181
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 182
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getSalesCode()Ljava/lang/String;
    .locals 4

    .prologue
    .line 149
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 150
    .local v0, "salesCode":Ljava/lang/String;
    const-string v1, "HeadlinesChannelUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSalesCode(): salesCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    return-object v0
.end method

.method public static getSettingPreviewIcon(Ljava/lang/String;)I
    .locals 2
    .param p0, "cardName"    # Ljava/lang/String;

    .prologue
    .line 202
    sget-object v1, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    .line 203
    .local v0, "res":Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;
    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;->mPreviewImage:I

    goto :goto_0
.end method

.method public static getWelcomeBackgroundImage(Ljava/lang/String;)I
    .locals 2
    .param p0, "cardName"    # Ljava/lang/String;

    .prologue
    .line 192
    sget-object v1, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    .line 193
    .local v0, "res":Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;
    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;->mBgImage:I

    goto :goto_0
.end method

.method public static getWelcomeSocialIcon(Ljava/lang/String;)I
    .locals 2
    .param p0, "cardName"    # Ljava/lang/String;

    .prologue
    .line 197
    sget-object v1, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->resourceMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;

    .line 198
    .local v0, "res":Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;
    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil$ResourceItem;->mIconImage:I

    goto :goto_0
.end method

.method public static isConnected(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 207
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 208
    .local v1, "manager":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 210
    .local v0, "mActiveNetInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    .line 211
    const-string v2, "HeadlinesChannelUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isConnected: mActiveNetInfo.getType() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    const-string v2, "HeadlinesChannelUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isConnected: mActiveNetInfo.getTypeName() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const-string v2, "HeadlinesChannelUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isConnected: mActiveNetInfo.isConnected() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 216
    const-string v2, "HeadlinesChannelUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isConnected: mActiveNetInfo.getReason() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isCountryModel(Ljava/lang/String;)Z
    .locals 2
    .param p0, "country"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isModel(Ljava/lang/String;)Z
    .locals 4
    .param p0, "model"    # Ljava/lang/String;

    .prologue
    .line 128
    const-string v1, "ro.product.model"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "productModel":Ljava/lang/String;
    const-string v1, "HeadlinesChannelUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isModel(): productModel = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    if-eqz v0, :cond_0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isOperatorModel(Ljava/lang/String;)Z
    .locals 2
    .param p0, "sales"    # Ljava/lang/String;

    .prologue
    .line 77
    invoke-static {}, Lcom/samsung/android/app/headlines/HeadlinesChannelUtil;->getSalesCode()Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isOrientationSupport(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 255
    const/4 v0, 0x0

    return v0
.end method

.method public static startLauncher(Landroid/content/Context;Z)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "moveToLastPage"    # Z

    .prologue
    const/4 v8, 0x1

    .line 225
    :try_start_0
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 226
    .local v3, "intent":Landroid/content/Intent;
    const-string v5, "android.intent.category.HOME"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    const/4 v0, 0x1

    .line 228
    .local v0, "NORMAL_MODE":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "easy_mode_switch"

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 229
    .local v2, "homeMode":I
    if-ne v2, v8, :cond_0

    .line 230
    const-string v5, "com.sec.android.app.launcher"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 233
    :goto_0
    const/high16 v5, 0x10200000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 234
    const/4 v4, 0x0

    .line 235
    .local v4, "opts":Landroid/app/ActivityOptions;
    if-eqz p1, :cond_1

    .line 236
    const-string v5, "moveToLastPage"

    invoke-virtual {v3, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 237
    const v5, 0x7f040002

    const v6, 0x7f040005

    invoke-static {p0, v5, v6}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    move-result-object v4

    .line 242
    :goto_1
    invoke-virtual {v4}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p0, v3, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 246
    .end local v0    # "NORMAL_MODE":I
    .end local v2    # "homeMode":I
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "opts":Landroid/app/ActivityOptions;
    :goto_2
    return-void

    .line 232
    .restart local v0    # "NORMAL_MODE":I
    .restart local v2    # "homeMode":I
    .restart local v3    # "intent":Landroid/content/Intent;
    :cond_0
    const-string v5, "com.sec.android.app.easylauncher"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 243
    .end local v0    # "NORMAL_MODE":I
    .end local v2    # "homeMode":I
    .end local v3    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 244
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 239
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    .restart local v0    # "NORMAL_MODE":I
    .restart local v2    # "homeMode":I
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v4    # "opts":Landroid/app/ActivityOptions;
    :cond_1
    :try_start_1
    const-string v5, "moveToFirstPage"

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 240
    const v5, 0x7f040003

    const v6, 0x7f040004

    invoke-static {p0, v5, v6}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    goto :goto_1
.end method
