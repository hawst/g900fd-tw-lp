.class Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$3;
.super Ljava/lang/Object;
.source "HeadlinesActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->dialogCustomizeSettingPopup(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 374
    iput-object p1, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$3;->this$0:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;

    iput-object p2, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$3;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 377
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$3;->this$0:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;

    const-string v4, "mymagazine"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 378
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 379
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "DO_NOT_SHOW_CUSTOMIZE_SETTING_POPUP_SHOW_FOR_ATT"

    const/4 v4, 0x1

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 380
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 381
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$3;->val$context:Landroid/content/Context;

    const-class v4, Lcom/samsung/android/app/headlines/activity/HeadlinesSettingActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 382
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity$3;->this$0:Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;

    invoke-virtual {v3, v1}, Lcom/samsung/android/app/headlines/activity/HeadlinesActivity;->startActivity(Landroid/content/Intent;)V

    .line 383
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 384
    return-void
.end method
