.class Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;
.super Ljava/lang/Thread;
.source "HeadlinesTermsDownloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TermsLoadingThread"
.end annotation


# instance fields
.field private mType:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;Ljava/lang/String;)V
    .locals 1
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 112
    iput-object p1, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->this$0:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 110
    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->mType:Ljava/lang/String;

    .line 111
    iput-object v0, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->mUrl:Ljava/lang/String;

    .line 113
    iput-object p2, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->mType:Ljava/lang/String;

    .line 114
    return-void
.end method

.method private getBodyString(I)Ljava/lang/String;
    .locals 12
    .param p1, "typeUrlCountryLanguage"    # I

    .prologue
    .line 162
    const/4 v5, 0x0

    .line 163
    .local v5, "connection":Ljava/net/HttpURLConnection;
    const/4 v7, 0x0

    .line 164
    .local v7, "fis":Ljava/io/FileInputStream;
    const/4 v2, 0x0

    .line 166
    .local v2, "bufferReader":Ljava/io/BufferedReader;
    :try_start_0
    iget-object v10, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->this$0:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    iget-object v11, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->mType:Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->createURL(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v10, v11, p1}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->access$200(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->mUrl:Ljava/lang/String;

    .line 183
    new-instance v9, Ljava/net/URL;

    iget-object v10, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->mUrl:Ljava/lang/String;

    invoke-direct {v9, v10}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 184
    .local v9, "url":Ljava/net/URL;
    invoke-virtual {v9}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v5, v0

    .line 185
    const-string v10, "GET"

    invoke-virtual {v5, v10}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 186
    const/16 v10, 0xbb8

    invoke-virtual {v5, v10}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 187
    const/16 v10, 0xbb8

    invoke-virtual {v5, v10}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 189
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .local v3, "bufferReader":Ljava/io/BufferedReader;
    :try_start_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 191
    .local v1, "buffer":Ljava/lang/StringBuffer;
    const/4 v8, 0x0

    .line 192
    .local v8, "read":I
    const/16 v10, 0x400

    new-array v4, v10, [C

    .line 193
    .local v4, "cbuff":[C
    :goto_0
    invoke-virtual {v3, v4}, Ljava/io/BufferedReader;->read([C)I

    move-result v8

    if-lez v8, :cond_3

    .line 194
    const/4 v10, 0x0

    invoke-virtual {v1, v4, v10, v8}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 223
    .end local v1    # "buffer":Ljava/lang/StringBuffer;
    .end local v4    # "cbuff":[C
    .end local v8    # "read":I
    :catch_0
    move-exception v6

    move-object v2, v3

    .line 224
    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .end local v9    # "url":Ljava/net/URL;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .local v6, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 226
    if-eqz v5, :cond_0

    .line 227
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 230
    :cond_0
    if-eqz v7, :cond_1

    .line 232
    :try_start_3
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 237
    :cond_1
    :goto_2
    if-eqz v2, :cond_2

    .line 239
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 245
    :cond_2
    :goto_3
    const/4 v10, 0x0

    .end local v6    # "e":Ljava/lang/Exception;
    :goto_4
    return-object v10

    .line 220
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v1    # "buffer":Ljava/lang/StringBuffer;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "cbuff":[C
    .restart local v8    # "read":I
    .restart local v9    # "url":Ljava/net/URL;
    :cond_3
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v10

    .line 226
    if-eqz v5, :cond_4

    .line 227
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 230
    :cond_4
    if-eqz v7, :cond_5

    .line 232
    :try_start_6
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 237
    :cond_5
    :goto_5
    if-eqz v3, :cond_6

    .line 239
    :try_start_7
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    :cond_6
    :goto_6
    move-object v2, v3

    .line 241
    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    goto :goto_4

    .line 226
    .end local v1    # "buffer":Ljava/lang/StringBuffer;
    .end local v4    # "cbuff":[C
    .end local v8    # "read":I
    .end local v9    # "url":Ljava/net/URL;
    :catchall_0
    move-exception v10

    :goto_7
    if-eqz v5, :cond_7

    .line 227
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 230
    :cond_7
    if-eqz v7, :cond_8

    .line 232
    :try_start_8
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 237
    :cond_8
    :goto_8
    if-eqz v2, :cond_9

    .line 239
    :try_start_9
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 241
    :cond_9
    :goto_9
    throw v10

    .line 233
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v1    # "buffer":Ljava/lang/StringBuffer;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "cbuff":[C
    .restart local v8    # "read":I
    .restart local v9    # "url":Ljava/net/URL;
    :catch_1
    move-exception v11

    goto :goto_5

    .line 240
    :catch_2
    move-exception v11

    goto :goto_6

    .line 233
    .end local v1    # "buffer":Ljava/lang/StringBuffer;
    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .end local v4    # "cbuff":[C
    .end local v8    # "read":I
    .end local v9    # "url":Ljava/net/URL;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v6    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v10

    goto :goto_2

    .line 240
    :catch_4
    move-exception v10

    goto :goto_3

    .line 233
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v11

    goto :goto_8

    .line 240
    :catch_6
    move-exception v11

    goto :goto_9

    .line 226
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v9    # "url":Ljava/net/URL;
    :catchall_1
    move-exception v10

    move-object v2, v3

    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    goto :goto_7

    .line 223
    .end local v9    # "url":Ljava/net/URL;
    :catch_7
    move-exception v6

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 118
    invoke-direct {p0, v6}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->getBodyString(I)Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "body":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-ne v3, v5, :cond_1

    .line 121
    :cond_0
    invoke-direct {p0, v5}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->getBodyString(I)Ljava/lang/String;

    move-result-object v0

    .line 124
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-ne v3, v5, :cond_3

    .line 125
    :cond_2
    const/4 v3, 0x2

    invoke-direct {p0, v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->getBodyString(I)Ljava/lang/String;

    move-result-object v0

    .line 128
    :cond_3
    const/4 v2, 0x0

    .line 130
    .local v2, "message":Landroid/os/Message;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-ne v3, v5, :cond_7

    .line 131
    :cond_4
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 132
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v3, "type"

    const-string v4, "NoResponse"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v3, "body"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->this$0:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    # getter for: Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mDownloadEventHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->access$000(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 136
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->this$0:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    # getter for: Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mDownloadEventHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->access$000(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 137
    invoke-virtual {v2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 139
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->this$0:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    # getter for: Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mDownloadEventHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->access$000(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 156
    :cond_5
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->this$0:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    # getter for: Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mTimeOutTimer:Ljava/util/Timer;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->access$100(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;)Ljava/util/Timer;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 157
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->this$0:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    # getter for: Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mTimeOutTimer:Ljava/util/Timer;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->access$100(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;)Ljava/util/Timer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Timer;->cancel()V

    .line 159
    :cond_6
    return-void

    .line 143
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_7
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 144
    .restart local v1    # "bundle":Landroid/os/Bundle;
    const-string v3, "type"

    iget-object v4, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->mType:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v3, "body"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->this$0:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    # getter for: Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mDownloadEventHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->access$000(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 148
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->this$0:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    # getter for: Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mDownloadEventHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->access$000(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 149
    invoke-virtual {v2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 151
    iget-object v3, p0, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader$TermsLoadingThread;->this$0:Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;

    # getter for: Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->mDownloadEventHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;->access$000(Lcom/samsung/android/app/headlines/activity/terms/HeadlinesTermsDownloader;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
