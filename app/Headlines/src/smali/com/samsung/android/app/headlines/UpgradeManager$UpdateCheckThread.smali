.class public Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;
.super Ljava/lang/Thread;
.source "UpgradeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/headlines/UpgradeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateCheckThread"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mManager:Lcom/samsung/android/app/headlines/UpgradeManager;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/headlines/UpgradeManager;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mgr"    # Lcom/samsung/android/app/headlines/UpgradeManager;

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;->mContext:Landroid/content/Context;

    .line 45
    iput-object v0, p0, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;->mManager:Lcom/samsung/android/app/headlines/UpgradeManager;

    .line 48
    iput-object p1, p0, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;->mContext:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;->mManager:Lcom/samsung/android/app/headlines/UpgradeManager;

    .line 50
    return-void
.end method


# virtual methods
.method public run()V
    .locals 22

    .prologue
    .line 55
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const-string v18, "mymagazine"

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v12

    .line 56
    .local v12, "pref":Landroid/content/SharedPreferences;
    const-string v17, "LAST_UPGRADE_CHECK_TIME"

    const-wide/16 v18, 0x0

    move-object/from16 v0, v17

    move-wide/from16 v1, v18

    invoke-interface {v12, v0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v8

    .line 57
    .local v8, "lastUpgradeCheckTime":J
    const/4 v11, 0x0

    .line 60
    .local v11, "packageInfo":Landroid/content/pm/PackageInfo;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v11

    .line 65
    :goto_0
    if-eqz v11, :cond_2

    .line 66
    :try_start_2
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 69
    .local v16, "versionName":Ljava/lang/String;
    :goto_1
    const-string v17, "VERSION_IN_SAMSUNG_APPS"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-interface {v12, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 70
    .local v15, "versionInStore":Ljava/lang/String;
    move-object/from16 v0, v16

    # invokes: Lcom/samsung/android/app/headlines/UpgradeManager;->isForceUpdate(Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {v0, v15}, Lcom/samsung/android/app/headlines/UpgradeManager;->access$000(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    .line 71
    .local v10, "needForceUpdate":Z
    # invokes: Lcom/samsung/android/app/headlines/UpgradeManager;->isTestMode()Z
    invoke-static {}, Lcom/samsung/android/app/headlines/UpgradeManager;->access$100()Z

    move-result v13

    .line 72
    .local v13, "testMode":Z
    const/4 v14, 0x0

    .line 73
    .local v14, "update":Z
    if-nez v10, :cond_0

    if-nez v13, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v18, v18, v8

    const-wide/32 v20, 0x5265c00

    cmp-long v17, v18, v20

    if-lez v17, :cond_1

    .line 74
    :cond_0
    # getter for: Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/headlines/UpgradeManager;->access$200()Ljava/lang/String;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "::updateCheck - needForceUpdate : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", TestMode : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", time : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v20, v20, v8

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const/4 v14, 0x1

    .line 78
    :cond_1
    if-eqz v14, :cond_3

    .line 79
    # getter for: Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/headlines/UpgradeManager;->access$200()Ljava/lang/String;

    move-result-object v17

    const-string v18, "::updateCheck - doing far call"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;->mManager:Lcom/samsung/android/app/headlines/UpgradeManager;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/headlines/UpgradeManager$UpdateCheckThread;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/samsung/android/app/headlines/update/UpdateUtil;->getAppPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    # invokes: Lcom/samsung/android/app/headlines/UpgradeManager;->checkUpdate(Landroid/content/Context;Ljava/lang/String;Z)V
    invoke-static {v0, v1, v2, v13}, Lcom/samsung/android/app/headlines/UpgradeManager;->access$300(Lcom/samsung/android/app/headlines/UpgradeManager;Landroid/content/Context;Ljava/lang/String;Z)V

    .line 84
    invoke-interface {v12}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 85
    .local v7, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 86
    .local v4, "checkTime":J
    const-string v17, "LAST_UPGRADE_CHECK_TIME"

    move-object/from16 v0, v17

    invoke-interface {v7, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 87
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 95
    .end local v4    # "checkTime":J
    .end local v7    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v8    # "lastUpgradeCheckTime":J
    .end local v10    # "needForceUpdate":Z
    .end local v11    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v12    # "pref":Landroid/content/SharedPreferences;
    .end local v13    # "testMode":Z
    .end local v14    # "update":Z
    .end local v15    # "versionInStore":Ljava/lang/String;
    .end local v16    # "versionName":Ljava/lang/String;
    :goto_2
    return-void

    .line 61
    .restart local v8    # "lastUpgradeCheckTime":J
    .restart local v11    # "packageInfo":Landroid/content/pm/PackageInfo;
    .restart local v12    # "pref":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v6

    .line 62
    .local v6, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 92
    .end local v6    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v8    # "lastUpgradeCheckTime":J
    .end local v11    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v12    # "pref":Landroid/content/SharedPreferences;
    :catch_1
    move-exception v6

    .line 93
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 68
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v8    # "lastUpgradeCheckTime":J
    .restart local v11    # "packageInfo":Landroid/content/pm/PackageInfo;
    .restart local v12    # "pref":Landroid/content/SharedPreferences;
    :cond_2
    :try_start_3
    const-string v16, "1.0.0"

    .restart local v16    # "versionName":Ljava/lang/String;
    goto/16 :goto_1

    .line 90
    .restart local v10    # "needForceUpdate":Z
    .restart local v13    # "testMode":Z
    .restart local v14    # "update":Z
    .restart local v15    # "versionInStore":Ljava/lang/String;
    :cond_3
    # getter for: Lcom/samsung/android/app/headlines/UpgradeManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/headlines/UpgradeManager;->access$200()Ljava/lang/String;

    move-result-object v17

    const-string v18, "::updateCheck - skipped, less than alloted time"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2
.end method
