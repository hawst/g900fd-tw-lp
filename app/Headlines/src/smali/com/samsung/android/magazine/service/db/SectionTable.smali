.class public Lcom/samsung/android/magazine/service/db/SectionTable;
.super Ljava/lang/Object;
.source "SectionTable.java"


# static fields
.field public static final COLUMN_CHANNEL_KEY:Ljava/lang/String; = "channel_key"

.field public static final COLUMN_CREATOR:Ljava/lang/String; = "creator"

.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final COLUMN_KEY:Ljava/lang/String; = "key"

.field public static final SQL_CREATE:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS section (_id INTEGER PRIMARY KEY AUTOINCREMENT, key TEXT NOT NULL UNIQUE, channel_key TEXT NOT NULL, creator TEXT );"

.field public static final TABLE_NAME:Ljava/lang/String; = "section"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addTableName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "column"    # Ljava/lang/String;

    .prologue
    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "section."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
