.class public final Lcom/samsung/android/magazine/service/contract/CardChannelContract$CardElement;
.super Ljava/lang/Object;
.source "CardChannelContract.java"

# interfaces
.implements Lcom/samsung/android/magazine/service/contract/CardChannelContract$CardElementColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/service/contract/CardChannelContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardElement"
.end annotation


# static fields
.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardchannel.card_element"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardchannel.card_element"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final IMAGE_URI_PATH_SEGMENT:Ljava/lang/String; = "image"

.field public static final TYPE_ACTION:Ljava/lang/String; = "action"

.field public static final TYPE_IMAGE:Ljava/lang/String; = "image"

.field public static final TYPE_TEXT:Ljava/lang/String; = "text"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 710
    sget-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "card_element"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$CardElement;->CONTENT_URI:Landroid/net/Uri;

    .line 754
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 702
    return-void
.end method
