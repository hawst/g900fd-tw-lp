.class Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;
.super Ljava/lang/Object;
.source "AdministratorContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DataHolder"
.end annotation


# instance fields
.field public mData1:Ljava/lang/String;

.field public mData2:Ljava/lang/String;

.field public mData3:Ljava/lang/String;

.field public mIntData1:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "intData1"    # I
    .param p2, "data1"    # Ljava/lang/String;
    .param p3, "data2"    # Ljava/lang/String;
    .param p4, "data3"    # Ljava/lang/String;

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput p1, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mIntData1:I

    .line 118
    iput-object p2, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData1:Ljava/lang/String;

    .line 119
    iput-object p3, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData2:Ljava/lang/String;

    .line 120
    iput-object p4, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData3:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "data1"    # Ljava/lang/String;
    .param p2, "data2"    # Ljava/lang/String;
    .param p3, "data3"    # Ljava/lang/String;

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData1:Ljava/lang/String;

    .line 110
    iput-object p2, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData2:Ljava/lang/String;

    .line 111
    iput-object p3, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData3:Ljava/lang/String;

    .line 112
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mIntData1:I

    .line 113
    return-void
.end method
