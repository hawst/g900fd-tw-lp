.class public Lcom/samsung/android/magazine/cardchannel/Card;
.super Ljava/lang/Object;
.source "Card.java"


# instance fields
.field private mActions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/magazine/cardchannel/CardAction;",
            ">;"
        }
    .end annotation
.end field

.field private mAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCardElements:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/magazine/cardchannel/CardElement;",
            ">;"
        }
    .end annotation
.end field

.field private mCardId:I

.field private mCardName:Ljava/lang/String;

.field private mLastMosifiedTime:J

.field private mProviderName:Ljava/lang/String;

.field private mTemplate:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "cardId"    # I
    .param p2, "cardName"    # Ljava/lang/String;
    .param p3, "template"    # Ljava/lang/String;
    .param p4, "cardProviderName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardId:I

    .line 29
    iput-object v2, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardName:Ljava/lang/String;

    .line 30
    iput-object v2, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mTemplate:Ljava/lang/String;

    .line 31
    iput-object v2, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mProviderName:Ljava/lang/String;

    .line 32
    iput-object v2, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mAttributes:Ljava/util/Map;

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mLastMosifiedTime:J

    .line 34
    iput-object v2, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardElements:Ljava/util/HashMap;

    .line 35
    iput-object v2, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mActions:Ljava/util/HashMap;

    .line 47
    iput p1, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardId:I

    .line 48
    iput-object p2, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardName:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mTemplate:Ljava/lang/String;

    .line 50
    iput-object p4, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mProviderName:Ljava/lang/String;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardElements:Ljava/util/HashMap;

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mActions:Ljava/util/HashMap;

    .line 53
    return-void
.end method


# virtual methods
.method addCardAction(Ljava/lang/String;Lcom/samsung/android/magazine/cardchannel/CardAction;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "action"    # Lcom/samsung/android/magazine/cardchannel/CardAction;

    .prologue
    .line 194
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mActions:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method addCardElement(Lcom/samsung/android/magazine/cardchannel/CardElement;)V
    .locals 2
    .param p1, "element"    # Lcom/samsung/android/magazine/cardchannel/CardElement;

    .prologue
    .line 179
    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardchannel/CardElement;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 180
    .local v0, "key":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 184
    :goto_0
    return-void

    .line 183
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardElements:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mAttributes:Ljava/util/Map;

    return-object v0
.end method

.method public getCardAction(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardAction;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mActions:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/CardAction;

    return-object v0
.end method

.method public getCardActionKeys()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mActions:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getCardElement(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardElement;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardElements:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/CardElement;

    return-object v0
.end method

.method public getCardElements()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/CardElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardElements:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getCardId()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardId:I

    return v0
.end method

.method public getCardName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardName:Ljava/lang/String;

    return-object v0
.end method

.method public getLastModifiedTime()J
    .locals 2

    .prologue
    .line 123
    iget-wide v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mLastMosifiedTime:J

    return-wide v0
.end method

.method public getProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mProviderName:Ljava/lang/String;

    return-object v0
.end method

.method public getTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mTemplate:Ljava/lang/String;

    return-object v0
.end method

.method setAttributes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mAttributes:Ljava/util/Map;

    .line 166
    return-void
.end method

.method setCardId(I)V
    .locals 0
    .param p1, "cardId"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardId:I

    .line 135
    return-void
.end method

.method setCardName(Ljava/lang/String;)V
    .locals 0
    .param p1, "cardName"    # Ljava/lang/String;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mCardName:Ljava/lang/String;

    .line 145
    return-void
.end method

.method setLastModifiedTime(J)V
    .locals 1
    .param p1, "timeInMillis"    # J

    .prologue
    .line 169
    iput-wide p1, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mLastMosifiedTime:J

    .line 170
    return-void
.end method

.method setTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "template"    # Ljava/lang/String;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/Card;->mTemplate:Ljava/lang/String;

    .line 156
    return-void
.end method
