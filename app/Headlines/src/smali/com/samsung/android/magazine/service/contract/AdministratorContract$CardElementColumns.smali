.class public interface abstract Lcom/samsung/android/magazine/service/contract/AdministratorContract$CardElementColumns;
.super Ljava/lang/Object;
.source "AdministratorContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/service/contract/AdministratorContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60c
    name = "CardElementColumns"
.end annotation


# static fields
.field public static final ACTION_ATTRIBUTES:Ljava/lang/String; = "action_attributes"

.field public static final ACTION_DATA:Ljava/lang/String; = "action_data"

.field public static final ACTION_LABEL:Ljava/lang/String; = "action_label"

.field public static final ACTION_TYPE:Ljava/lang/String; = "action_type"

.field public static final ATTRIBUTES:Ljava/lang/String; = "attributes"

.field public static final CARD_ID:Ljava/lang/String; = "card_id"

.field public static final DATA1:Ljava/lang/String; = "data1"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final _ID:Ljava/lang/String; = "_id"
