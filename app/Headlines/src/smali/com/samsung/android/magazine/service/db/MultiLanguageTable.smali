.class public Lcom/samsung/android/magazine/service/db/MultiLanguageTable;
.super Ljava/lang/Object;
.source "MultiLanguageTable.java"


# static fields
.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final COLUMN_KEY1:Ljava/lang/String; = "key1"

.field public static final COLUMN_KEY2:Ljava/lang/String; = "key2"

.field public static final COLUMN_LANGUAGE_CODE:Ljava/lang/String; = "language_code"

.field public static final COLUMN_TYPE:Ljava/lang/String; = "type"

.field public static final COLUMN_VALUE1:Ljava/lang/String; = "value1"

.field public static final COLUMN_VALUE2:Ljava/lang/String; = "value2"

.field public static final COLUMN_VALUE3:Ljava/lang/String; = "value3"

.field public static final SQL_CREATE:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS multi_language (_id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT NOT NULL, key1 TEXT NOT NULL, key2 TEXT NOT NULL, language_code TEXT NOT NULL, value1 TEXT NOT NULL, value2 TEXT, value3 TEXT );"

.field public static final TABLE_NAME:Ljava/lang/String; = "multi_language"

.field public static final TYPE_CARD_TYPE:Ljava/lang/String; = "card_type"

.field public static final TYPE_SECTION:Ljava/lang/String; = "section"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addTableName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "column"    # Ljava/lang/String;

    .prologue
    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "multi_language."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
