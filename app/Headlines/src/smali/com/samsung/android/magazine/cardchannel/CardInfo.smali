.class public Lcom/samsung/android/magazine/cardchannel/CardInfo;
.super Ljava/lang/Object;
.source "CardInfo.java"


# instance fields
.field private mDescription:Ljava/lang/String;

.field private mDisplayName:Ljava/lang/String;

.field private mIconUri:Ljava/lang/String;

.field private mIntroImageUri:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mProvider:Ljava/lang/String;

.field private mSection:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "providerName"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mName:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mProvider:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mDisplayName:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mDescription:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mIntroImageUri:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mSection:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mIconUri:Ljava/lang/String;

    .line 23
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mProvider:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mName:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public getCardProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mProvider:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIconUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mIconUri:Ljava/lang/String;

    return-object v0
.end method

.method public getIntroductionImageUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mIntroImageUri:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mSection:Ljava/lang/String;

    return-object v0
.end method

.method setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mDescription:Ljava/lang/String;

    .line 116
    return-void
.end method

.method setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mDisplayName:Ljava/lang/String;

    .line 108
    return-void
.end method

.method setIconUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "iconUri"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mIconUri:Ljava/lang/String;

    .line 120
    return-void
.end method

.method setIntroductionImageUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mIntroImageUri:Ljava/lang/String;

    .line 112
    return-void
.end method

.method setSection(Ljava/lang/String;)V
    .locals 0
    .param p1, "section"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardInfo;->mSection:Ljava/lang/String;

    .line 124
    return-void
.end method
