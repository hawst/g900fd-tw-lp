.class public interface abstract Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardTypeColumns;
.super Ljava/lang/Object;
.source "CardProviderContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/cardprovider/CardProviderContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60c
    name = "ProvidedCardTypeColumns"
.end annotation


# static fields
.field public static final CARD_TYPE_KEY:Ljava/lang/String; = "card_type_key"

.field public static final CHANNEL_KEY:Ljava/lang/String; = "channel_key"

.field public static final DEFAULT_SUBSCRIPTION:Ljava/lang/String; = "default_subscription"

.field public static final ICON_IMAGE:Ljava/lang/String; = "icon_image"

.field public static final PROVIDER:Ljava/lang/String; = "provider"

.field public static final SECTION_KEY:Ljava/lang/String; = "section_key"

.field public static final _ID:Ljava/lang/String; = "_id"
