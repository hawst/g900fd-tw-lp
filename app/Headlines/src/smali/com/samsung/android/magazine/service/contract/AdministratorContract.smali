.class public Lcom/samsung/android/magazine/service/contract/AdministratorContract;
.super Ljava/lang/Object;
.source "AdministratorContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$Setting;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$Provider;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$ProvidedCardType;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$MultiLanguage;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$Channel;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$CardElement;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$Card;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$SettingColumns;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$ProviderColumns;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$ProvidedCardTypeColumns;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$MultiLanguageColumns;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$ChannelColumns;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$CardElementColumns;,
        Lcom/samsung/android/magazine/service/contract/AdministratorContract$CardColumns;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.magazine.provider.administrator"

.field public static final AUTHORITY_URI:Landroid/net/Uri;

.field private static final TABLE_NAME_CARD:Ljava/lang/String; = "card"

.field private static final TABLE_NAME_CARD_ELEMENT:Ljava/lang/String; = "card_element"

.field private static final TABLE_NAME_CHANNEL:Ljava/lang/String; = "channel"

.field private static final TABLE_NAME_MULTI_LANGUAGE:Ljava/lang/String; = "multi_language"

.field private static final TABLE_NAME_PROVIDED_CARD_TYPE:Ljava/lang/String; = "provided_card_type"

.field private static final TABLE_NAME_PROVIDER:Ljava/lang/String; = "provider"

.field private static final TABLE_NAME_SETTING:Ljava/lang/String; = "setting"

.field public static final VERSION:Ljava/lang/String; = "11"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-string v0, "content://com.samsung.android.magazine.provider.administrator"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/AdministratorContract;->AUTHORITY_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 769
    return-void
.end method
