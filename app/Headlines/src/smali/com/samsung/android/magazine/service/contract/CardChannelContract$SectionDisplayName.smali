.class public final Lcom/samsung/android/magazine/service/contract/CardChannelContract$SectionDisplayName;
.super Ljava/lang/Object;
.source "CardChannelContract.java"

# interfaces
.implements Lcom/samsung/android/magazine/service/contract/CardChannelContract$SectionMultiLanguageColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/service/contract/CardChannelContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SectionDisplayName"
.end annotation


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardchannel.section_display_name"

.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 943
    sget-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 944
    const-string v1, "section_display_name"

    .line 943
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$SectionDisplayName;->CONTENT_URI:Landroid/net/Uri;

    .line 953
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 935
    return-void
.end method
