.class public interface abstract Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;
.super Ljava/lang/Object;
.source "CardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/cardchannel/CardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CardChangeListener"
.end annotation


# virtual methods
.method public abstract onCardAdded(I)V
.end method

.method public abstract onCardRemoved(I)V
.end method

.method public abstract onCardUpdated(I)V
.end method
