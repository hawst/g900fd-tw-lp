.class public Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;
.super Landroid/content/ContentProvider;
.source "CardProviderContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;
    }
.end annotation


# static fields
.field private static final CODE_CARD_ELEMENT_ID:I = 0xc9

.field private static final CODE_CARD_ELEMENT_LIST:I = 0xc8

.field private static final CODE_CARD_ELEMENT_UPDATE_CARD:I = 0xca

.field private static final CODE_CARD_ID:I = 0x65

.field private static final CODE_CARD_IMAGE:I = 0x6e

.field private static final CODE_CARD_LIST:I = 0x64

.field private static final CODE_CARD_NO_ELEMENT:I = 0x66

.field private static final CODE_CHANNEL_ID:I = 0x12d

.field private static final CODE_CHANNEL_LIST:I = 0x12c

.field private static final CODE_MULTI_LANGUAGE_CARD_TYPE:I = 0x190

.field private static final CODE_MULTI_LANGUAGE_CARD_TYPE_UPDATE:I = 0x192

.field private static final CODE_MULTI_LANGUAGE_ID:I = 0x191

.field private static final CODE_MULTI_LANGUAGE_SECTION:I = 0x193

.field private static final CODE_MULTI_LANGUAGE_SECTION_UPDATE:I = 0x194

.field private static final CODE_PROVIDED_CARD_TYPE_CREATE:I = 0x1f6

.field private static final CODE_PROVIDED_CARD_TYPE_ID:I = 0x1f5

.field private static final CODE_PROVIDED_CARD_TYPE_IMAGE:I = 0x1fe

.field private static final CODE_PROVIDED_CARD_TYPE_LIST:I = 0x1f4

.field private static final CODE_PROVIDER_ID:I = 0x259

.field private static final CODE_PROVIDER_LIST:I = 0x258

.field private static final CODE_SECTION_ID:I = 0x2bd

.field private static final CODE_SECTION_LIST:I = 0x2bc

.field private static final CODE_SETTING_ID:I = 0x321

.field private static final CODE_SETTING_LIST:I = 0x320

.field private static final ERROR_CODE_DATA_ALREADY_EXISTS:I = -0x2

.field private static final ERROR_CODE_DATA_NOT_FOUND:I = -0x3

.field private static final TAG:Ljava/lang/String; = "MagazineService::CardProviderContentProvider"

.field private static final mUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 96
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 97
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "card"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 98
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "card/no_element"

    const/16 v3, 0x66

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 99
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "card/#"

    const/16 v3, 0x65

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 100
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "card/#/image/*/*"

    const/16 v3, 0x6e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 101
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "card_element"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 102
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "card_element/#"

    const/16 v3, 0xc9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 103
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "card_element/update_card"

    const/16 v3, 0xca

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 104
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "channel"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 105
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "channel/#"

    const/16 v3, 0x12d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 106
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "multi_language"

    const/16 v3, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 107
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "multi_language/update"

    const/16 v3, 0x192

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 108
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "multi_language/section"

    const/16 v3, 0x193

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 109
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "multi_language/section_update"

    const/16 v3, 0x194

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 110
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "multi_language/#"

    const/16 v3, 0x191

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 111
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "provided_card_type"

    const/16 v3, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 112
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "provided_card_type/create"

    const/16 v3, 0x1f6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 113
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "provided_card_type/#"

    const/16 v3, 0x1f5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 114
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "provided_card_type/image/*/*/*"

    const/16 v3, 0x1fe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 115
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "provider"

    const/16 v3, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 116
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "provider/#"

    const/16 v3, 0x259

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 117
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "section"

    const/16 v3, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 118
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "section/#"

    const/16 v3, 0x2bd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 119
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "setting"

    const/16 v3, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 120
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.cardprovider"

    const-string v2, "setting/#"

    const/16 v3, 0x321

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 121
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 91
    iput-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    .line 92
    iput-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    .line 123
    return-void
.end method

.method private bulkInsertMultiLanguageForCardType(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;)I
    .locals 14
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "valuesArray"    # [Landroid/content/ContentValues;

    .prologue
    .line 374
    const/4 v12, 0x0

    aget-object v12, p2, v12

    const-string v13, "type"

    invoke-virtual {v12, v13}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 375
    .local v10, "type":Ljava/lang/String;
    if-eqz v10, :cond_0

    const-string v12, "card_type"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 376
    :cond_0
    const-string v12, "MagazineService::CardProviderContentProvider"

    const-string v13, "Can not insert rows in multi_language table. type column is invalid."

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    const/4 v6, 0x0

    .line 411
    :cond_1
    :goto_0
    return v6

    .line 380
    :cond_2
    const/4 v12, 0x0

    aget-object v12, p2, v12

    const-string v13, "key1"

    invoke-virtual {v12, v13}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 381
    .local v1, "cardTypeKey":Ljava/lang/String;
    const/4 v12, 0x0

    aget-object v12, p2, v12

    const-string v13, "key2"

    invoke-virtual {v12, v13}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 382
    .local v5, "provider":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_3

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 383
    :cond_3
    const-string v12, "MagazineService::CardProviderContentProvider"

    const-string v13, "Can not insert rows in multi_language table. key1 or key2 column is empty."

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    const/4 v6, 0x0

    goto :goto_0

    .line 388
    :cond_4
    invoke-direct {p0, p1, v5, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->isProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 389
    const/4 v12, 0x3

    new-array v11, v12, [Ljava/lang/String;

    const/4 v12, 0x0

    const-string v13, "card_type"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v1, v11, v12

    const/4 v12, 0x2

    aput-object v5, v11, v12

    .line 391
    .local v11, "whereArgs":[Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v12, "type"

    invoke-direct {v7, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 392
    .local v7, "sb":Ljava/lang/StringBuilder;
    const-string v12, "=? AND "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    const-string v12, "key1"

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    const-string v12, "=? AND "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    const-string v12, "key2"

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    const-string v12, "=?"

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    const-string v12, "multi_language"

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {p1, v12, v13, v11}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 399
    const/4 v6, 0x0

    .line 400
    .local v6, "rowCount":I
    move-object/from16 v0, p2

    .local v0, "arr$":[Landroid/content/ContentValues;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    .line 402
    .local v2, "contentValues":Landroid/content/ContentValues;
    const-string v12, "multi_language"

    const/4 v13, 0x0

    invoke-virtual {p1, v12, v13, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 403
    .local v8, "rowId":J
    const-wide/16 v12, -0x1

    cmp-long v12, v8, v12

    if-lez v12, :cond_5

    .line 404
    add-int/lit8 v6, v6, 0x1

    .line 400
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 410
    .end local v0    # "arr$":[Landroid/content/ContentValues;
    .end local v2    # "contentValues":Landroid/content/ContentValues;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v6    # "rowCount":I
    .end local v7    # "sb":Ljava/lang/StringBuilder;
    .end local v8    # "rowId":J
    .end local v11    # "whereArgs":[Ljava/lang/String;
    :cond_6
    const-string v12, "MagazineService::CardProviderContentProvider"

    const-string v13, "Can not insert rows in multi_language table. Card name is not registered."

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    const/4 v6, 0x0

    goto/16 :goto_0
.end method

.method private bulkInsertMultiLanguageForCardTypeUpdate(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;)I
    .locals 19
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "valuesArray"    # [Landroid/content/ContentValues;

    .prologue
    .line 418
    const/4 v2, 0x0

    aget-object v2, p2, v2

    const-string v3, "type"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 419
    .local v4, "type":Ljava/lang/String;
    if-eqz v4, :cond_0

    const-string v2, "card_type"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 420
    :cond_0
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "Can not insert rows in multi_language table. type column is invalid."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    const/4 v13, 0x0

    .line 473
    :cond_1
    :goto_0
    return v13

    .line 424
    :cond_2
    const/4 v2, 0x0

    aget-object v2, p2, v2

    const-string v3, "key1"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 425
    .local v5, "cardType":Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, p2, v2

    const-string v3, "key2"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 426
    .local v6, "provider":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 427
    :cond_3
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "Can not insert rows in multi_language table. key1 or key2 column is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    const/4 v13, 0x0

    goto :goto_0

    .line 432
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v5}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->isProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 433
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "Can not insert rows in multi_language table. Card name is not registered."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const/4 v13, 0x0

    goto :goto_0

    .line 437
    :cond_5
    const/4 v13, 0x0

    .line 438
    .local v13, "rowCount":I
    move-object/from16 v8, p2

    .local v8, "arr$":[Landroid/content/ContentValues;
    array-length v12, v8

    .local v12, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_1
    if-ge v10, v12, :cond_1

    aget-object v9, v8, v10

    .line 439
    .local v9, "contentValues":Landroid/content/ContentValues;
    const-string v2, "language_code"

    invoke-virtual {v9, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .local v7, "languageCode":Ljava/lang/String;
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 441
    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->getMultiLanguageId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    .line 442
    .local v11, "id":I
    const/4 v2, -0x1

    if-le v11, v2, :cond_8

    .line 445
    const-string v2, "type"

    invoke-virtual {v9, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 446
    const-string v2, "key1"

    invoke-virtual {v9, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 447
    const-string v2, "key2"

    invoke-virtual {v9, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 448
    const-string v2, "language_code"

    invoke-virtual {v9, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 449
    invoke-virtual {v9}, Landroid/content/ContentValues;->size()I

    move-result v2

    if-lez v2, :cond_6

    .line 451
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 452
    .local v16, "selection":Ljava/lang/String;
    const-string v2, "multi_language"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v9, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v17

    .line 453
    .local v17, "updatedCount":I
    if-lez v17, :cond_7

    .line 454
    add-int/lit8 v13, v13, 0x1

    .line 438
    .end local v16    # "selection":Ljava/lang/String;
    .end local v17    # "updatedCount":I
    :cond_6
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 457
    .restart local v16    # "selection":Ljava/lang/String;
    .restart local v17    # "updatedCount":I
    :cond_7
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Failed to update a row(card_name) of multi_language table: "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v18, ", "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v18, ","

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 463
    .end local v16    # "selection":Ljava/lang/String;
    .end local v17    # "updatedCount":I
    :cond_8
    const-string v2, "multi_language"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v9}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v14

    .line 464
    .local v14, "rowId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v14, v2

    if-lez v2, :cond_9

    .line 465
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 468
    :cond_9
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "Failed to insert a row(card_name) of multi_language table"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private bulkInsertMultiLanguageForSection(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;)I
    .locals 25
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "valuesArray"    # [Landroid/content/ContentValues;

    .prologue
    .line 479
    const/4 v2, 0x0

    aget-object v2, p2, v2

    const-string v3, "type"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 480
    .local v23, "type":Ljava/lang/String;
    if-eqz v23, :cond_0

    const-string v2, "section"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 481
    :cond_0
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "Can not insert rows in multi_language table. type column is invaild."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    const/16 v16, 0x0

    .line 532
    :cond_1
    :goto_0
    return v16

    .line 485
    :cond_2
    const/4 v2, 0x0

    aget-object v2, p2, v2

    const-string v3, "key1"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 486
    .local v22, "sectionKey":Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, p2, v2

    const-string v3, "key2"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 487
    .local v11, "channelKey":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 488
    :cond_3
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "Can not insert rows in multi_language table. key1 or key2 column is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    const/16 v16, 0x0

    goto :goto_0

    .line 494
    :cond_4
    const/4 v14, 0x0

    .line 495
    .local v14, "isValidSection":Z
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v2, "key"

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 496
    .local v17, "sb":Ljava/lang/StringBuilder;
    const-string v2, "=? AND "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 497
    const-string v2, "channel_key"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 498
    const-string v2, "=?"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 499
    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v22, v6, v2

    const/4 v2, 0x1

    aput-object v11, v6, v2

    .line 500
    .local v6, "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    .line 501
    .local v4, "projection":[Ljava/lang/String;
    const-string v3, "section"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 502
    .local v21, "sectionCursor":Landroid/database/Cursor;
    if-eqz v21, :cond_6

    .line 503
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_5

    .line 504
    const/4 v14, 0x1

    .line 506
    :cond_5
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 509
    :cond_6
    if-eqz v14, :cond_8

    .line 510
    const/4 v2, 0x3

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/4 v2, 0x0

    const-string v3, "section"

    aput-object v3, v24, v2

    const/4 v2, 0x1

    aput-object v22, v24, v2

    const/4 v2, 0x2

    aput-object v11, v24, v2

    .line 512
    .local v24, "whereArgs":[Ljava/lang/String;
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v2, "type"

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 513
    .local v20, "sb2":Ljava/lang/StringBuilder;
    const-string v2, "=? AND "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 514
    const-string v2, "key1"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    const-string v2, "=? AND "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    const-string v2, "key2"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 517
    const-string v2, "=?"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 518
    const-string v2, "multi_language"

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 520
    const/16 v16, 0x0

    .line 521
    .local v16, "rowCount":I
    move-object/from16 v10, p2

    .local v10, "arr$":[Landroid/content/ContentValues;
    array-length v15, v10

    .local v15, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_1
    if-ge v13, v15, :cond_1

    aget-object v12, v10, v13

    .line 523
    .local v12, "contentValues":Landroid/content/ContentValues;
    const-string v2, "multi_language"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v12}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v18

    .line 524
    .local v18, "rowId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v18, v2

    if-lez v2, :cond_7

    .line 525
    add-int/lit8 v16, v16, 0x1

    .line 521
    :cond_7
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 531
    .end local v10    # "arr$":[Landroid/content/ContentValues;
    .end local v12    # "contentValues":Landroid/content/ContentValues;
    .end local v13    # "i$":I
    .end local v15    # "len$":I
    .end local v16    # "rowCount":I
    .end local v18    # "rowId":J
    .end local v20    # "sb2":Ljava/lang/StringBuilder;
    .end local v24    # "whereArgs":[Ljava/lang/String;
    :cond_8
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can not insert rows in multi_language table. Section does not exist.: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    const/16 v16, 0x0

    goto/16 :goto_0
.end method

.method private bulkInsertMultiLanguageForSectionUpdate(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;)I
    .locals 27
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "valuesArray"    # [Landroid/content/ContentValues;

    .prologue
    .line 539
    const/4 v2, 0x0

    aget-object v2, p2, v2

    const-string v3, "type"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 540
    .local v25, "type":Ljava/lang/String;
    if-eqz v25, :cond_0

    const-string v2, "section"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 541
    :cond_0
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "Can not insert rows in multi_language table. type column is invalid."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    const/16 v19, 0x0

    .line 610
    :cond_1
    :goto_0
    return v19

    .line 545
    :cond_2
    const/4 v2, 0x0

    aget-object v2, p2, v2

    const-string v3, "key1"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 546
    .local v10, "sectionKey":Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, p2, v2

    const-string v3, "key2"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 547
    .local v11, "channelKey":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 548
    :cond_3
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "Can not insert rows in multi_language table. key1 or key2 column is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    const/16 v19, 0x0

    goto :goto_0

    .line 554
    :cond_4
    const/16 v17, 0x0

    .line 555
    .local v17, "isValidSection":Z
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v2, "key"

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 556
    .local v22, "sb":Ljava/lang/StringBuilder;
    const-string v2, "=? AND "

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 557
    const-string v2, "channel_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    const-string v2, "=?"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559
    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v10, v6, v2

    const/4 v2, 0x1

    aput-object v11, v6, v2

    .line 560
    .local v6, "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    .line 561
    .local v4, "projection":[Ljava/lang/String;
    const-string v3, "section"

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 562
    .local v23, "sectionCursor":Landroid/database/Cursor;
    if-eqz v23, :cond_6

    .line 563
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_5

    .line 564
    const/16 v17, 0x1

    .line 566
    :cond_5
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 569
    :cond_6
    if-nez v17, :cond_7

    .line 570
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can not insert rows in multi_language table. Section does not exist.: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 574
    :cond_7
    const/16 v19, 0x0

    .line 575
    .local v19, "rowCount":I
    move-object/from16 v13, p2

    .local v13, "arr$":[Landroid/content/ContentValues;
    array-length v0, v13

    move/from16 v18, v0

    .local v18, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_1
    move/from16 v0, v18

    if-ge v15, v0, :cond_1

    aget-object v14, v13, v15

    .line 576
    .local v14, "contentValues":Landroid/content/ContentValues;
    const-string v2, "language_code"

    invoke-virtual {v14, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .local v12, "languageCode":Ljava/lang/String;
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, v25

    .line 578
    invoke-direct/range {v7 .. v12}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->getMultiLanguageId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    .line 579
    .local v16, "id":I
    const/4 v2, -0x1

    move/from16 v0, v16

    if-le v0, v2, :cond_a

    .line 582
    const-string v2, "type"

    invoke-virtual {v14, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 583
    const-string v2, "key1"

    invoke-virtual {v14, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 584
    const-string v2, "key2"

    invoke-virtual {v14, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 585
    const-string v2, "language_code"

    invoke-virtual {v14, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 586
    invoke-virtual {v14}, Landroid/content/ContentValues;->size()I

    move-result v2

    if-lez v2, :cond_8

    .line 588
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 589
    .local v24, "selection":Ljava/lang/String;
    const-string v2, "multi_language"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v2, v14, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v26

    .line 590
    .local v26, "updatedCount":I
    if-lez v26, :cond_9

    .line 591
    add-int/lit8 v19, v19, 0x1

    .line 575
    .end local v24    # "selection":Ljava/lang/String;
    .end local v26    # "updatedCount":I
    :cond_8
    :goto_2
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 594
    .restart local v24    # "selection":Ljava/lang/String;
    .restart local v26    # "updatedCount":I
    :cond_9
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to udpate a row(section) of multi_language table: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 600
    .end local v24    # "selection":Ljava/lang/String;
    .end local v26    # "updatedCount":I
    :cond_a
    const-string v2, "multi_language"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v14}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v20

    .line 601
    .local v20, "rowId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v20, v2

    if-lez v2, :cond_b

    .line 602
    add-int/lit8 v19, v19, 0x1

    goto :goto_2

    .line 605
    :cond_b
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "Failed to insert a row(section) of multi_language table"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private deleteCardElements(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 17
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 867
    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "type"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "data1"

    aput-object v4, v5, v3

    .line 872
    .local v5, "projection":[Ljava/lang/String;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 874
    .local v14, "imgUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v4, "card_element"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 875
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_2

    .line 876
    :cond_0
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 877
    const/4 v3, 0x0

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 878
    .local v16, "type":Ljava/lang/String;
    if-eqz v16, :cond_0

    const-string v3, "image"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 879
    const/4 v3, 0x1

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 882
    .end local v16    # "type":Ljava/lang/String;
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 885
    :cond_2
    const-string v3, "card_element"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 886
    .local v15, "rowCount":I
    if-lez v15, :cond_3

    .line 888
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 889
    .local v13, "imageUri":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteCardFile(Ljava/lang/String;)V

    goto :goto_1

    .line 893
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "imageUri":Ljava/lang/String;
    :cond_3
    return v15
.end method

.method private deleteCardFile(Ljava/lang/String;)V
    .locals 7
    .param p1, "fileUri"    # Ljava/lang/String;

    .prologue
    .line 897
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 913
    :cond_0
    :goto_0
    return-void

    .line 903
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 904
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 905
    .local v2, "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x5

    if-lt v4, v5, :cond_0

    .line 906
    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    const-string v5, "image"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v0, v5, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 907
    .local v0, "directory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 908
    new-instance v1, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x3

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v4, 0x4

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 909
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 910
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private deleteCardImageFiles(Ljava/lang/String;)V
    .locals 9
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 916
    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    const-string v7, "image"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v6

    invoke-direct {v1, v6, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 917
    .local v1, "directory":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 918
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 919
    .local v3, "fileList":[Ljava/io/File;
    if-eqz v3, :cond_0

    .line 920
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 921
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 920
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 924
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 926
    .end local v3    # "fileList":[Ljava/io/File;
    :cond_1
    return-void
.end method

.method private deleteCardTypeFile(Ljava/lang/String;)V
    .locals 8
    .param p1, "fileUri"    # Ljava/lang/String;

    .prologue
    .line 929
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 947
    :cond_0
    :goto_0
    return-void

    .line 935
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 936
    .local v4, "uri":Landroid/net/Uri;
    invoke-virtual {v4}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 937
    .local v2, "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x5

    if-lt v5, v6, :cond_0

    .line 938
    new-instance v3, Ljava/io/File;

    iget-object v5, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    const-string v6, "image"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v6

    const/4 v5, 0x2

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {v3, v6, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 939
    .local v3, "providerDirectory":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const/4 v5, 0x3

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {v0, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 940
    .local v0, "cardNameDirectory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 941
    new-instance v1, Ljava/io/File;

    const/4 v5, 0x4

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {v1, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 942
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 943
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 944
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private deleteCardTypeImageFiles(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "cardType"    # Ljava/lang/String;

    .prologue
    .line 950
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    const-string v8, "image"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v7

    invoke-direct {v6, v7, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 951
    .local v6, "providerDirectory":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v6, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 952
    .local v1, "cardNameDirectory":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 953
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 954
    .local v3, "fileList":[Ljava/io/File;
    if-eqz v3, :cond_0

    .line 955
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 956
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 955
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 959
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 960
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 962
    .end local v3    # "fileList":[Ljava/io/File;
    :cond_1
    return-void
.end method

.method private deleteCards(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 25
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 809
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    .line 810
    .local v21, "deletedCardMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "card_channel"

    aput-object v4, v5, v3

    .line 811
    .local v5, "projection":[Ljava/lang/String;
    const-string v4, "card"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 812
    .local v20, "cursor":Landroid/database/Cursor;
    if-eqz v20, :cond_2

    .line 813
    :goto_0
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 814
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 815
    .local v23, "id":I
    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 816
    .local v17, "channel":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 817
    const-string v17, "channel_not_defined"

    .line 819
    :cond_0
    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 821
    .end local v17    # "channel":Ljava/lang/String;
    .end local v23    # "id":I
    :cond_1
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 824
    :cond_2
    const-string v3, "card"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v24

    .line 825
    .local v24, "rowCount":I
    if-lez v24, :cond_6

    .line 828
    invoke-virtual/range {v21 .. v21}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v15

    .line 829
    .local v15, "cardEntryList":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Map$Entry;

    .line 830
    .local v14, "cardEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 831
    .local v23, "id":Ljava/lang/String;
    invoke-interface {v14}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 833
    .restart local v17    # "channel":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    move-object/from16 v0, v23

    invoke-static {v3, v0}, Lcom/samsung/android/magazine/service/MagazineService;->cancelExpirationTimer(Landroid/content/Context;Ljava/lang/String;)V

    .line 835
    const/16 v19, 0x0

    .line 836
    .local v19, "channelPackageName":Ljava/lang/String;
    const-string v3, "channel_not_defined"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 837
    const/4 v3, 0x1

    new-array v8, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "package_name"

    aput-object v4, v8, v3

    .line 838
    .local v8, "projection2":[Ljava/lang/String;
    const-string v9, "key=?"

    .line 839
    .local v9, "selection2":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v17, v10, v3

    .line 840
    .local v10, "selectionArgs2":[Ljava/lang/String;
    const-string v7, "channel"

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v6, p1

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 841
    .local v18, "channelCursor":Landroid/database/Cursor;
    if-eqz v18, :cond_4

    .line 842
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 843
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 845
    :cond_3
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 850
    .end local v8    # "projection2":[Ljava/lang/String;
    .end local v9    # "selection2":Ljava/lang/String;
    .end local v10    # "selectionArgs2":[Ljava/lang/String;
    .end local v18    # "channelCursor":Landroid/database/Cursor;
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI_DELETE_NOTIFICATION:Landroid/net/Uri;

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-static {v3, v4, v0, v1}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-static {v3, v0, v4}, Lcom/samsung/android/magazine/service/Notifier;->sendCardDeletedBroadcast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_1

    .line 855
    .end local v14    # "cardEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v17    # "channel":Ljava/lang/String;
    .end local v19    # "channelPackageName":Ljava/lang/String;
    .end local v23    # "id":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {v21 .. v21}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v16

    .line 856
    .local v16, "cardIdList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 857
    .restart local v23    # "id":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "card_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 858
    .restart local v9    # "selection2":Ljava/lang/String;
    const-string v3, "card_element"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 859
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteCardImageFiles(Ljava/lang/String;)V

    goto :goto_2

    .line 863
    .end local v9    # "selection2":Ljava/lang/String;
    .end local v15    # "cardEntryList":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v16    # "cardIdList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    .end local v23    # "id":Ljava/lang/String;
    :cond_6
    return v24
.end method

.method private deleteProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 23
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 965
    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "provider"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "card_type_key"

    aput-object v4, v5, v3

    .line 967
    .local v5, "projection":[Ljava/lang/String;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 968
    .local v14, "dataHolderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;>;"
    const-string v4, "provided_card_type"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 969
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_1

    .line 970
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 972
    new-instance v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;

    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v13, v3, v4}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    .local v13, "dataHolder":Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;
    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 975
    .end local v13    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;
    :cond_0
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 980
    :cond_1
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v3, "provider"

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 981
    .local v18, "sb1":Ljava/lang/StringBuilder;
    const-string v3, "=? AND "

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 982
    const-string v3, "card_type_key"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 983
    const-string v3, "=?"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 984
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 987
    .local v21, "settingWhere":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v3, "card_provider"

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 988
    .local v19, "sb2":Ljava/lang/StringBuilder;
    const-string v3, "=? AND "

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 989
    const-string v3, "card_key"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 990
    const-string v3, "=?"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 991
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 994
    .local v11, "cardWhere":Ljava/lang/String;
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v3, "type"

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 995
    .local v20, "sb3":Ljava/lang/StringBuilder;
    const-string v3, "=? AND "

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 996
    const-string v3, "key1"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 997
    const-string v3, "=? AND "

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 998
    const-string v3, "key2"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 999
    const-string v3, "=?"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1000
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 1002
    .local v16, "multiLanguageWhere":Ljava/lang/String;
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;

    .line 1003
    .restart local v13    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/4 v3, 0x0

    iget-object v4, v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;->mData1:Ljava/lang/String;

    aput-object v4, v22, v3

    const/4 v3, 0x1

    iget-object v4, v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;->mData2:Ljava/lang/String;

    aput-object v4, v22, v3

    .line 1005
    .local v22, "whereArgs":[Ljava/lang/String;
    const-string v3, "setting"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1007
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v11, v2}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteCards(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1009
    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/4 v3, 0x0

    const-string v4, "card_type"

    aput-object v4, v17, v3

    const/4 v3, 0x1

    iget-object v4, v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;->mData2:Ljava/lang/String;

    aput-object v4, v17, v3

    const/4 v3, 0x2

    iget-object v4, v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;->mData1:Ljava/lang/String;

    aput-object v4, v17, v3

    .line 1010
    .local v17, "multiLanguageWhereArgs":[Ljava/lang/String;
    const-string v3, "multi_language"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1012
    iget-object v3, v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;->mData1:Ljava/lang/String;

    iget-object v4, v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;->mData2:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteCardTypeImageFiles(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1015
    .end local v13    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;
    .end local v17    # "multiLanguageWhereArgs":[Ljava/lang/String;
    .end local v22    # "whereArgs":[Ljava/lang/String;
    :cond_2
    const-string v3, "provided_card_type"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    return v3
.end method

.method private deleteSections(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 22
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1019
    const/4 v4, 0x2

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "key"

    aput-object v5, v6, v4

    const/4 v4, 0x1

    const-string v5, "channel_key"

    aput-object v5, v6, v4

    .line 1021
    .local v6, "projection":[Ljava/lang/String;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1022
    .local v14, "dataHolderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;>;"
    const-string v5, "section"

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1023
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_1

    .line 1024
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1026
    new-instance v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;

    const/4 v4, 0x0

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v12, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v13, v4, v5}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    .local v13, "dataHolder":Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;
    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1029
    .end local v13    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;
    :cond_0
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1034
    :cond_1
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v4, "channel_key"

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1035
    .local v20, "sb1":Ljava/lang/StringBuilder;
    const-string v4, "=? AND "

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1036
    const-string v4, "section_key"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1037
    const-string v4, "=?"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1038
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 1041
    .local v18, "providedCardTypeWhere":Ljava/lang/String;
    new-instance v21, Ljava/lang/StringBuilder;

    const-string v4, "type"

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1042
    .local v21, "sb2":Ljava/lang/StringBuilder;
    const-string v4, "=? AND "

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1043
    const-string v4, "key1"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044
    const-string v4, "=? AND "

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045
    const-string v4, "key2"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1046
    const-string v4, "=?"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1047
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 1049
    .local v16, "multiLanguageWhere":Ljava/lang/String;
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;

    .line 1052
    .restart local v13    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;
    const/4 v4, 0x2

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/4 v4, 0x0

    iget-object v5, v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;->mData2:Ljava/lang/String;

    aput-object v5, v19, v4

    const/4 v4, 0x1

    iget-object v5, v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;->mData1:Ljava/lang/String;

    aput-object v5, v19, v4

    .line 1053
    .local v19, "providedCardTypeWhereArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1055
    const/4 v4, 0x3

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/4 v4, 0x0

    const-string v5, "section"

    aput-object v5, v17, v4

    const/4 v4, 0x1

    iget-object v5, v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;->mData1:Ljava/lang/String;

    aput-object v5, v17, v4

    const/4 v4, 0x2

    iget-object v5, v13, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;->mData2:Ljava/lang/String;

    aput-object v5, v17, v4

    .line 1056
    .local v17, "multiLanguageWhereArgs":[Ljava/lang/String;
    const-string v4, "multi_language"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v4, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    .line 1059
    .end local v13    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider$DataHolder;
    .end local v17    # "multiLanguageWhereArgs":[Ljava/lang/String;
    .end local v19    # "providedCardTypeWhereArgs":[Ljava/lang/String;
    :cond_2
    const-string v4, "section"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v4, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    return v4
.end method

.method private getMultiLanguageId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 11
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "key1"    # Ljava/lang/String;
    .param p4, "key2"    # Ljava/lang/String;
    .param p5, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 1417
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 1418
    .local v2, "projection":[Ljava/lang/String;
    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v4, v0

    const/4 v0, 0x1

    aput-object p3, v4, v0

    const/4 v0, 0x2

    aput-object p4, v4, v0

    const/4 v0, 0x3

    aput-object p5, v4, v0

    .line 1420
    .local v4, "selectionArgs":[Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v0, "type"

    invoke-direct {v10, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1421
    .local v10, "sb":Ljava/lang/StringBuilder;
    const-string v0, "=? AND "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1422
    const-string v0, "key1"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1423
    const-string v0, "=? AND "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1424
    const-string v0, "key2"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1425
    const-string v0, "=? AND "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1426
    const-string v0, "language_code"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1427
    const-string v0, "=?"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1428
    const-string v1, "multi_language"

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1429
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 1430
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1431
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1432
    .local v9, "id":I
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1437
    .end local v9    # "id":I
    :goto_0
    return v9

    .line 1435
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1437
    :cond_1
    const/4 v9, -0x1

    goto :goto_0
.end method

.method private insertCard(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Z)Landroid/net/Uri;
    .locals 19
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "enableNotification"    # Z

    .prologue
    .line 1283
    const-string v2, "card_provider"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1284
    .local v15, "provider":Ljava/lang/String;
    const-string v2, "card_type"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1285
    .local v10, "cardType":Ljava/lang/String;
    const-string v2, "card_channel"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1286
    .local v12, "channel":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1288
    .local v6, "whereArgs":[Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v2, "provider"

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1289
    .local v18, "sb":Ljava/lang/StringBuilder;
    const-string v2, "=? AND "

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1290
    const-string v2, "card_type_key"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1291
    const-string v2, "=?"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1292
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1293
    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "whereArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    aput-object v15, v6, v2

    const/4 v2, 0x1

    aput-object v10, v6, v2

    .line 1294
    .restart local v6    # "whereArgs":[Ljava/lang/String;
    const-string v12, "channel_not_defined"

    .line 1305
    :goto_0
    const/4 v11, 0x0

    .line 1306
    .local v11, "cardUri":Landroid/net/Uri;
    const-wide/16 v16, -0x2

    .line 1307
    .local v16, "rowId":J
    const-string v3, "provided_card_type"

    const/4 v4, 0x0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1308
    .local v13, "cursor":Landroid/database/Cursor;
    if-eqz v13, :cond_1

    .line 1309
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 1310
    const-string v2, "card"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v16

    .line 1311
    const-wide/16 v2, 0x0

    cmp-long v2, v16, v2

    if-gez v2, :cond_0

    .line 1312
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to insert a row to card table: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1315
    :cond_0
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1317
    :cond_1
    const-wide/16 v2, -0x1

    cmp-long v2, v16, v2

    if-lez v2, :cond_5

    .line 1318
    sget-object v2, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Card;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 1319
    const-string v2, "expiration_time"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v14

    .line 1320
    .local v14, "expirationTime":Ljava/lang/Long;
    if-eqz v14, :cond_2

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 1321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-wide/from16 v0, v16

    invoke-static {v2, v0, v1, v4, v5}, Lcom/samsung/android/magazine/service/MagazineService;->setExpirationTimer(Landroid/content/Context;JJ)V

    .line 1323
    :cond_2
    if-eqz p3, :cond_3

    .line 1324
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI_INSERT_NOTIFICATION:Landroid/net/Uri;

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4, v12}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 1333
    .end local v14    # "expirationTime":Ljava/lang/Long;
    :cond_3
    :goto_1
    return-object v11

    .line 1297
    .end local v11    # "cardUri":Landroid/net/Uri;
    .end local v13    # "cursor":Landroid/database/Cursor;
    .end local v16    # "rowId":J
    :cond_4
    const-string v2, " AND ("

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1298
    const-string v2, "channel_key"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1299
    const-string v2, " IS NULL OR "

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1300
    const-string v2, "channel_key"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1301
    const-string v2, "=?)"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1302
    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "whereArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    aput-object v15, v6, v2

    const/4 v2, 0x1

    aput-object v10, v6, v2

    const/4 v2, 0x2

    aput-object v12, v6, v2

    .restart local v6    # "whereArgs":[Ljava/lang/String;
    goto/16 :goto_0

    .line 1329
    .restart local v11    # "cardUri":Landroid/net/Uri;
    .restart local v13    # "cursor":Landroid/database/Cursor;
    .restart local v16    # "rowId":J
    :cond_5
    const-wide/16 v2, -0x2

    cmp-long v2, v16, v2

    if-nez v2, :cond_3

    .line 1330
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can not insert a card. The card type is not registered. ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private insertCardElement(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 7
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1338
    const-string v4, "type"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1339
    .local v1, "type":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v4, "image"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1340
    const-string v4, "data1"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1341
    .local v0, "imageUri":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1342
    const-string v4, "data1"

    const-string v5, "com.samsung.android.magazine.provider.cardprovider"

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1345
    .end local v0    # "imageUri":Ljava/lang/String;
    :cond_0
    const-string v4, "card_element"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 1346
    .local v2, "rowId":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gez v4, :cond_1

    .line 1347
    const-string v4, "MagazineService::CardProviderContentProvider"

    const-string v5, "Failed to insert a row to card_element table"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1349
    :cond_1
    return-wide v2
.end method

.method private insertMultiLanguage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 12
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    .line 1353
    const-string v0, "type"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1354
    .local v2, "type":Ljava/lang/String;
    const-string v0, "card_type"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1355
    const-string v0, "MagazineService::CardProviderContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unsupported type of multi_language: "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v10

    .line 1387
    :goto_0
    return-wide v0

    .line 1358
    :cond_0
    const-string v0, "key1"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1359
    .local v3, "cardType":Ljava/lang/String;
    const-string v0, "key2"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1360
    .local v4, "provider":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1361
    :cond_1
    const-string v0, "MagazineService::CardProviderContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Keys of multi_language are empty: "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, ", "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v10

    .line 1362
    goto :goto_0

    .line 1364
    :cond_2
    const-string v0, "language_code"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1366
    .local v5, "languageCode":Ljava/lang/String;
    invoke-direct {p0, p1, v4, v3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->isProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    move-wide v0, v10

    .line 1367
    goto :goto_0

    :cond_3
    move-object v0, p0

    move-object v1, p1

    .line 1368
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->getMultiLanguageId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 1369
    .local v6, "id":I
    const/4 v0, -0x1

    if-le v6, v0, :cond_5

    .line 1371
    const-string v0, "type"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1372
    const-string v0, "key1"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1373
    const-string v0, "key2"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1374
    const-string v0, "language_code"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1375
    invoke-virtual {p2}, Landroid/content/ContentValues;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 1377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1378
    .local v7, "selection":Ljava/lang/String;
    const-string v0, "multi_language"

    invoke-virtual {p1, v0, p2, v7, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 1379
    .local v8, "updatedCount":I
    if-lez v8, :cond_4

    .line 1380
    const-string v0, "MagazineService::CardProviderContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "a row of multi_language table is updated.: "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, ", "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, ","

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1381
    int-to-long v0, v6

    goto/16 :goto_0

    .end local v7    # "selection":Ljava/lang/String;
    .end local v8    # "updatedCount":I
    :cond_4
    move-wide v0, v10

    .line 1384
    goto/16 :goto_0

    .line 1387
    :cond_5
    const-string v0, "multi_language"

    invoke-virtual {p1, v0, v9, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_0
.end method

.method private insertProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 24
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1442
    const-string v2, "card_type_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1443
    .local v15, "cardType":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1444
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "insertProvidedCardType: The value of card_type_key column is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1445
    const-wide/16 v20, -0x1

    .line 1509
    :cond_0
    :goto_0
    return-wide v20

    .line 1447
    :cond_1
    const-string v2, "provider"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1448
    .local v19, "provider":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1449
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "insertProvidedCardType: The value of provider column is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1450
    const-wide/16 v20, -0x1

    goto :goto_0

    .line 1452
    :cond_2
    const-string v2, "channel_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1454
    .local v16, "channel":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    .line 1455
    .local v4, "projection1":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v19, v6, v2

    .line 1456
    .local v6, "selectionArgs1":[Ljava/lang/String;
    const-string v5, "package_name=?"

    .line 1459
    .local v5, "selection1":Ljava/lang/String;
    const-string v3, "provider"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 1460
    .local v23, "typeCursor":Landroid/database/Cursor;
    if-eqz v23, :cond_3

    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_5

    .line 1461
    :cond_3
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insertProvidedCardType: provider is not registered.: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    if-eqz v23, :cond_4

    .line 1463
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 1464
    :cond_4
    const-wide/16 v20, -0x3

    goto :goto_0

    .line 1467
    :cond_5
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 1469
    const/4 v2, 0x1

    new-array v9, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v9, v2

    .line 1470
    .local v9, "projection2":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 1472
    .local v11, "selectionArgs2":[Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v2, "provider"

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1473
    .local v22, "sb2":Ljava/lang/StringBuilder;
    const-string v2, "=? AND "

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1474
    const-string v2, "card_type_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1475
    const-string v2, "=?"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1476
    const/4 v2, 0x2

    new-array v11, v2, [Ljava/lang/String;

    .end local v11    # "selectionArgs2":[Ljava/lang/String;
    const/4 v2, 0x0

    aput-object v19, v11, v2

    const/4 v2, 0x1

    aput-object v15, v11, v2

    .line 1489
    .restart local v11    # "selectionArgs2":[Ljava/lang/String;
    const-string v8, "provided_card_type"

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 1490
    .local v17, "cursor":Landroid/database/Cursor;
    if-eqz v17, :cond_7

    .line 1491
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_6

    .line 1492
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "insertProvidedCardType: Already provided card type"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1493
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 1494
    const-wide/16 v20, -0x2

    goto/16 :goto_0

    .line 1496
    :cond_6
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 1499
    :cond_7
    const-string v2, "icon_image"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1500
    .local v18, "imageUri":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1501
    const-string v2, "icon_image"

    const-string v3, "com.samsung.android.magazine.provider.cardprovider"

    const-string v7, "com.samsung.android.magazine.provider.cardchannel"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1504
    :cond_8
    const-string v2, "last_modified_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1505
    const-string v2, "provided_card_type"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v20

    .line 1506
    .local v20, "rowId":J
    const-wide/16 v2, 0x0

    cmp-long v2, v20, v2

    if-gez v2, :cond_0

    .line 1507
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to insert a row to provided_card_type table. "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private insertProvidedCardTypeWithSetting(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 35
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1515
    const-string v2, "card_type_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 1516
    .local v20, "cardType":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1517
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "insertProvidedCardTypeWithSetting: The value of card_type_key column is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1518
    const-wide/16 v26, -0x1

    .line 1610
    :goto_0
    return-wide v26

    .line 1520
    :cond_0
    const-string v2, "provider"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 1521
    .local v25, "provider":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1522
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "insertProvidedCardTypeWithSetting: The value of provider column is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1523
    const-wide/16 v26, -0x1

    goto :goto_0

    .line 1526
    :cond_1
    const-string v2, "section_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 1527
    .local v30, "sectionKey":Ljava/lang/String;
    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1528
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "insertProvidedCardTypeWithSetting: The value of section_key column is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1529
    const-wide/16 v26, -0x1

    goto :goto_0

    .line 1533
    :cond_2
    const/16 v21, 0x0

    .line 1534
    .local v21, "channelKey":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "channel_key"

    aput-object v3, v4, v2

    .line 1535
    .local v4, "projection1":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v30, v6, v2

    .line 1536
    .local v6, "selectionArgs1":[Ljava/lang/String;
    const-string v5, "key=?"

    .line 1537
    .local v5, "selection1":Ljava/lang/String;
    const-string v3, "section"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v29

    .line 1538
    .local v29, "sectionCursor":Landroid/database/Cursor;
    if-eqz v29, :cond_4

    .line 1539
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1540
    const/4 v2, 0x0

    move-object/from16 v0, v29

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 1542
    :cond_3
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->close()V

    .line 1544
    :cond_4
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1545
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insertProvidedCardTypeWithSetting: section is invalid.: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1546
    const-wide/16 v26, -0x3

    goto/16 :goto_0

    .line 1549
    :cond_5
    const/4 v2, 0x1

    new-array v9, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v9, v2

    .line 1550
    .local v9, "projection2":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v11, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v25, v11, v2

    .line 1551
    .local v11, "selectionArgs2":[Ljava/lang/String;
    const-string v10, "package_name=?"

    .line 1553
    .local v10, "selection2":Ljava/lang/String;
    const-string v8, "provider"

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v34

    .line 1554
    .local v34, "typeCursor":Landroid/database/Cursor;
    if-eqz v34, :cond_6

    invoke-interface/range {v34 .. v34}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_8

    .line 1555
    :cond_6
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insertProvidedCardTypeWithSetting: provider is not registered.: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1556
    if-eqz v34, :cond_7

    .line 1557
    invoke-interface/range {v34 .. v34}, Landroid/database/Cursor;->close()V

    .line 1558
    :cond_7
    const-wide/16 v26, -0x3

    goto/16 :goto_0

    .line 1561
    :cond_8
    invoke-interface/range {v34 .. v34}, Landroid/database/Cursor;->close()V

    .line 1563
    const/4 v2, 0x1

    new-array v14, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v14, v2

    .line 1564
    .local v14, "projection3":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 1566
    .local v16, "selectionArgs3":[Ljava/lang/String;
    new-instance v28, Ljava/lang/StringBuilder;

    const-string v2, "provider"

    move-object/from16 v0, v28

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1567
    .local v28, "sb3":Ljava/lang/StringBuilder;
    const-string v2, "=? AND "

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1568
    const-string v2, "card_type_key"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1569
    const-string v2, "=?"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1570
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v16, v0

    .end local v16    # "selectionArgs3":[Ljava/lang/String;
    const/4 v2, 0x0

    aput-object v25, v16, v2

    const/4 v2, 0x1

    aput-object v20, v16, v2

    .line 1571
    .restart local v16    # "selectionArgs3":[Ljava/lang/String;
    const-string v13, "provided_card_type"

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v12, p1

    invoke-virtual/range {v12 .. v19}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 1572
    .local v22, "cursor":Landroid/database/Cursor;
    if-eqz v22, :cond_a

    .line 1573
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_9

    .line 1574
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "insertProvidedCardTypeWithSetting: Already provided card type"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1575
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 1576
    const-wide/16 v26, -0x2

    goto/16 :goto_0

    .line 1578
    :cond_9
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 1582
    :cond_a
    const-string v2, "icon_image"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 1583
    .local v24, "imageUri":Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 1584
    const-string v2, "icon_image"

    const-string v3, "com.samsung.android.magazine.provider.cardprovider"

    const-string v7, "com.samsung.android.magazine.provider.cardchannel"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1587
    :cond_b
    const-string v2, "channel_key"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1588
    const-string v2, "last_modified_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1589
    const-string v2, "provided_card_type"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v26

    .line 1590
    .local v26, "rowId":J
    const-wide/16 v2, 0x0

    cmp-long v2, v26, v2

    if-gez v2, :cond_c

    .line 1591
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to insert a row to provided_card_type table. "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1595
    :cond_c
    const-string v2, "default_subscription"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v23

    .line 1596
    .local v23, "enable":Ljava/lang/Integer;
    new-instance v31, Landroid/content/ContentValues;

    invoke-direct/range {v31 .. v31}, Landroid/content/ContentValues;-><init>()V

    .line 1597
    .local v31, "settingValues":Landroid/content/ContentValues;
    const-string v2, "channel_key"

    move-object/from16 v0, v31

    move-object/from16 v1, v21

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1598
    const-string v2, "section_key"

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1599
    const-string v2, "provider"

    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1600
    const-string v2, "card_type_key"

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1601
    const-string v2, "enable"

    move-object/from16 v0, v31

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1602
    const-string v2, "setting"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v32

    .line 1603
    .local v32, "settingRowId":J
    const-wide/16 v2, 0x0

    cmp-long v2, v32, v2

    if-gez v2, :cond_d

    .line 1604
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to insert a row to setting table. "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1607
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI_CHANGE_NOTIFICATION:Landroid/net/Uri;

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-static {v2, v3, v0, v1}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private insertProvider(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 13
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 1615
    const-string v0, "package_name"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1616
    .local v9, "packageName":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1617
    const-string v0, "MagazineService::CardProviderContentProvider"

    const-string v1, "insertProvider: The value of package_name column is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1618
    const-wide/16 v10, -0x1

    .line 1648
    :cond_0
    :goto_0
    return-wide v10

    .line 1621
    :cond_1
    new-array v2, v6, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 1622
    .local v2, "projection":[Ljava/lang/String;
    new-array v4, v6, [Ljava/lang/String;

    aput-object v9, v4, v1

    .line 1624
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v3, "package_name=?"

    .line 1627
    .local v3, "selection":Ljava/lang/String;
    const-string v1, "provider"

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1628
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_4

    .line 1629
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 1630
    const-string v0, "MagazineService::CardProviderContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insertProvider: provider already exists.: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1631
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1633
    const-string v0, "package_name"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1634
    const-string v0, "provider"

    invoke-virtual {p1, v0, p2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v12

    .line 1635
    .local v12, "updatedCount":I
    if-lez v12, :cond_2

    .line 1636
    const-string v0, "MagazineService::CardProviderContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insertProvider: provider is updated.: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1639
    :cond_2
    const-wide/16 v10, -0x2

    goto :goto_0

    .line 1641
    .end local v12    # "updatedCount":I
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1644
    :cond_4
    const-string v0, "provider"

    invoke-virtual {p1, v0, v5, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v10

    .line 1645
    .local v10, "rowId":J
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-gez v0, :cond_0

    .line 1646
    const-string v0, "MagazineService::CardProviderContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to insert a row to provider table: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private insertSection(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 23
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1653
    const-string v2, "key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 1654
    .local v22, "sectionKey":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1655
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "insertSection: The value of key column is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1656
    const-wide/16 v18, -0x1

    .line 1701
    :cond_0
    :goto_0
    return-wide v18

    .line 1658
    :cond_1
    const-string v2, "channel_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1659
    .local v16, "channelKey":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1660
    const-string v2, "MagazineService::CardProviderContentProvider"

    const-string v3, "insertSection: The value of channel_key column is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1661
    const-wide/16 v18, -0x1

    goto :goto_0

    .line 1665
    :cond_2
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v2, "key"

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1666
    .local v20, "sb":Ljava/lang/StringBuilder;
    const-string v2, "=?"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1667
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v22, v6, v2

    .line 1668
    .local v6, "selectionArgs":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    .line 1669
    .local v4, "projection":[Ljava/lang/String;
    const-string v3, "section"

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 1670
    .local v21, "sectionCursor":Landroid/database/Cursor;
    if-eqz v21, :cond_4

    .line 1671
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 1672
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 1673
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insertSection: section already exists.: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1674
    const-wide/16 v18, -0x2

    goto :goto_0

    .line 1676
    :cond_3
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 1680
    :cond_4
    const/16 v17, 0x0

    .line 1681
    .local v17, "isValidChannel":Z
    const-string v10, "key=?"

    .line 1682
    .local v10, "selection2":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v11, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v16, v11, v2

    .line 1683
    .local v11, "selectionArgs2":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v9, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v9, v2

    .line 1684
    .local v9, "projection2":[Ljava/lang/String;
    const-string v8, "channel"

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 1685
    .local v15, "channelCursor":Landroid/database/Cursor;
    if-eqz v15, :cond_6

    .line 1686
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_5

    .line 1687
    const/16 v17, 0x1

    .line 1689
    :cond_5
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 1692
    :cond_6
    if-eqz v17, :cond_7

    .line 1693
    const-string v2, "section"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v18

    .line 1694
    .local v18, "rowId":J
    const-wide/16 v2, 0x0

    cmp-long v2, v18, v2

    if-gez v2, :cond_0

    .line 1695
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to insert a row to section table: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1700
    .end local v18    # "rowId":J
    :cond_7
    const-string v2, "MagazineService::CardProviderContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insertSection: Can not insert a section. The channel key is invalid.: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1701
    const-wide/16 v18, -0x3

    goto/16 :goto_0
.end method

.method private isProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "provider"    # Ljava/lang/String;
    .param p3, "cardType"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1392
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v0, "provider"

    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1393
    .local v9, "sb":Ljava/lang/StringBuilder;
    const-string v0, "=? AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1394
    const-string v0, "card_type_key"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1395
    const-string v0, "=?"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1396
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    aput-object p2, v4, v11

    aput-object p3, v4, v10

    .line 1397
    .local v4, "selectionArgs":[Ljava/lang/String;
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v11

    .line 1398
    .local v2, "projection":[Ljava/lang/String;
    const-string v1, "provided_card_type"

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1399
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 1400
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1401
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move v0, v10

    .line 1412
    :goto_0
    return v0

    .line 1405
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1406
    const-string v0, "MagazineService::CardProviderContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Card type is not registered.: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v11

    .line 1407
    goto :goto_0

    .line 1411
    :cond_1
    const-string v0, "MagazineService::CardProviderContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to query to provider_card_type table: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v11

    .line 1412
    goto :goto_0
.end method

.method private updateMultiLanguages(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 2112
    const-string v0, "type"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 2113
    const-string v0, "key1"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 2114
    const-string v0, "key2"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 2115
    const-string v0, "language_code"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 2117
    invoke-virtual {p2}, Landroid/content/ContentValues;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2118
    const-string v0, "multi_language"

    invoke-virtual {p1, v0, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2121
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 18
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 2125
    const/16 v17, 0x0

    .line 2127
    .local v17, "rowCount":I
    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    const-string v4, "icon_image"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2128
    const-string v4, "icon_image"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 2129
    .local v14, "imageUri":Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2130
    const-string v4, "com.samsung.android.magazine.provider.cardprovider"

    const-string v5, "com.samsung.android.magazine.provider.cardchannel"

    invoke-virtual {v14, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v14

    .line 2131
    const-string v4, "icon_image"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2134
    :cond_0
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 2135
    .local v16, "oldUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "icon_image"

    aput-object v5, v6, v4

    .line 2136
    .local v6, "projection":[Ljava/lang/String;
    const-string v5, "provided_card_type"

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 2137
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_2

    .line 2138
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2139
    const/4 v4, 0x0

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2141
    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 2144
    :cond_2
    const-string v4, "provided_card_type"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v17

    .line 2145
    if-lez v17, :cond_5

    .line 2146
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 2147
    .local v15, "oldImageUri":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2148
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2150
    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteCardTypeFile(Ljava/lang/String;)V

    goto :goto_1

    .line 2156
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "imageUri":Ljava/lang/String;
    .end local v15    # "oldImageUri":Ljava/lang/String;
    .end local v16    # "oldUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    return v17
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 34
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "valuesArray"    # [Landroid/content/ContentValues;

    .prologue
    .line 161
    if-eqz p2, :cond_0

    :try_start_0
    move-object/from16 v0, p2

    array-length v3, v0

    const/4 v6, 0x1

    if-ge v3, v6, :cond_1

    .line 162
    :cond_0
    const/16 v31, 0x0

    .line 367
    :goto_0
    return v31

    .line 165
    :cond_1
    sget-object v3, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 362
    invoke-super/range {p0 .. p2}, Landroid/content/ContentProvider;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v31

    goto :goto_0

    .line 168
    :sswitch_0
    const/16 v31, 0x0

    .line 169
    .local v31, "rowCount":I
    monitor-enter p0
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 171
    .local v2, "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 173
    const/4 v3, 0x0

    aget-object v3, p2, v3

    const-string v6, "card_id"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v21

    .line 174
    .local v21, "cardId":Ljava/lang/Integer;
    if-eqz v21, :cond_8

    .line 175
    const/16 v20, 0x0

    .line 176
    .local v20, "cardChannel":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "card_channel"

    aput-object v6, v4, v3

    .line 177
    .local v4, "projection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 179
    .local v5, "selection":Ljava/lang/String;
    const-string v3, "card"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v26

    .line 180
    .local v26, "cursor":Landroid/database/Cursor;
    if-eqz v26, :cond_3

    .line 181
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 182
    const/4 v3, 0x0

    move-object/from16 v0, v26

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 183
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 184
    const-string v20, "channel_not_defined"

    .line 187
    :cond_2
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 190
    :cond_3
    if-eqz v20, :cond_8

    .line 191
    move-object/from16 v19, p2

    .local v19, "arr$":[Landroid/content/ContentValues;
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v29, v0

    .local v29, "len$":I
    const/16 v28, 0x0

    .local v28, "i$":I
    :goto_1
    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_5

    aget-object v25, v19, v28

    .line 192
    .local v25, "contentValues":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->insertCardElement(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v32

    .line 193
    .local v32, "rowId":J
    const-wide/16 v6, -0x1

    cmp-long v3, v32, v6

    if-lez v3, :cond_4

    .line 194
    add-int/lit8 v31, v31, 0x1

    .line 191
    :cond_4
    add-int/lit8 v28, v28, 0x1

    goto :goto_1

    .line 198
    .end local v25    # "contentValues":Landroid/content/ContentValues;
    .end local v32    # "rowId":J
    :cond_5
    const/16 v24, 0x0

    .line 199
    .local v24, "channelPackageName":Ljava/lang/String;
    const-string v3, "channel_not_defined"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 200
    const/4 v3, 0x1

    new-array v8, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "package_name"

    aput-object v6, v8, v3

    .line 201
    .local v8, "projection2":[Ljava/lang/String;
    const-string v9, "key=?"

    .line 202
    .local v9, "selection2":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v20, v10, v3

    .line 203
    .local v10, "selectionArgs2":[Ljava/lang/String;
    const-string v7, "channel"

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v6, v2

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 204
    .local v23, "channelCursor":Landroid/database/Cursor;
    if-eqz v23, :cond_7

    .line 205
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 206
    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 208
    :cond_6
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 214
    .end local v8    # "projection2":[Ljava/lang/String;
    .end local v9    # "selection2":Ljava/lang/String;
    .end local v10    # "selectionArgs2":[Ljava/lang/String;
    .end local v23    # "channelCursor":Landroid/database/Cursor;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI_INSERT_NOTIFICATION:Landroid/net/Uri;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    invoke-static {v3, v6, v7, v0}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move-object/from16 v0, v24

    invoke-static {v3, v0, v6}, Lcom/samsung/android/magazine/service/Notifier;->sendCardAddedBroadcast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 221
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "selection":Ljava/lang/String;
    .end local v19    # "arr$":[Landroid/content/ContentValues;
    .end local v20    # "cardChannel":Ljava/lang/String;
    .end local v24    # "channelPackageName":Ljava/lang/String;
    .end local v26    # "cursor":Landroid/database/Cursor;
    .end local v28    # "i$":I
    .end local v29    # "len$":I
    :cond_8
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 222
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 223
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v21    # "cardId":Ljava/lang/Integer;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 364
    .end local v31    # "rowCount":I
    :catch_0
    move-exception v27

    .line 365
    .local v27, "e":Landroid/database/sqlite/SQLiteException;
    const-string v3, "MagazineService::CardProviderContentProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Database operation failed. : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v27 .. v27}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v27 .. v27}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    invoke-virtual/range {v27 .. v27}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 367
    const/16 v31, 0x0

    goto/16 :goto_0

    .line 228
    .end local v27    # "e":Landroid/database/sqlite/SQLiteException;
    :sswitch_1
    const/16 v31, 0x0

    .line 229
    .restart local v31    # "rowCount":I
    :try_start_3
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 230
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 231
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 232
    const/4 v3, 0x0

    aget-object v3, p2, v3

    const-string v6, "card_id"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v21

    .line 233
    .restart local v21    # "cardId":Ljava/lang/Integer;
    if-eqz v21, :cond_f

    .line 234
    const/16 v22, 0x0

    .line 236
    .local v22, "channel":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "card_channel"

    aput-object v6, v4, v3

    .line 237
    .restart local v4    # "projection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 238
    .restart local v5    # "selection":Ljava/lang/String;
    const-string v12, "card"

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v11, v2

    move-object v13, v4

    move-object v14, v5

    invoke-virtual/range {v11 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v26

    .line 239
    .restart local v26    # "cursor":Landroid/database/Cursor;
    if-eqz v26, :cond_a

    .line 240
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 241
    const/4 v3, 0x0

    move-object/from16 v0, v26

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 242
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 243
    const-string v22, "channel_not_defined"

    .line 246
    :cond_9
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 249
    :cond_a
    if-eqz v22, :cond_f

    .line 250
    move-object/from16 v19, p2

    .restart local v19    # "arr$":[Landroid/content/ContentValues;
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v29, v0

    .restart local v29    # "len$":I
    const/16 v28, 0x0

    .restart local v28    # "i$":I
    :goto_2
    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_c

    aget-object v25, v19, v28

    .line 251
    .restart local v25    # "contentValues":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->insertCardElement(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v32

    .line 252
    .restart local v32    # "rowId":J
    const-wide/16 v6, -0x1

    cmp-long v3, v32, v6

    if-lez v3, :cond_b

    .line 253
    add-int/lit8 v31, v31, 0x1

    .line 250
    :cond_b
    add-int/lit8 v28, v28, 0x1

    goto :goto_2

    .line 257
    .end local v25    # "contentValues":Landroid/content/ContentValues;
    .end local v32    # "rowId":J
    :cond_c
    const/16 v24, 0x0

    .line 258
    .restart local v24    # "channelPackageName":Ljava/lang/String;
    const-string v3, "channel_not_defined"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 259
    const/4 v3, 0x1

    new-array v8, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "package_name"

    aput-object v6, v8, v3

    .line 260
    .restart local v8    # "projection2":[Ljava/lang/String;
    const-string v9, "key=?"

    .line 261
    .restart local v9    # "selection2":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v22, v10, v3

    .line 262
    .restart local v10    # "selectionArgs2":[Ljava/lang/String;
    const-string v7, "channel"

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v6, v2

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 263
    .restart local v23    # "channelCursor":Landroid/database/Cursor;
    if-eqz v23, :cond_e

    .line 264
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 265
    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 267
    :cond_d
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 273
    .end local v8    # "projection2":[Ljava/lang/String;
    .end local v9    # "selection2":Ljava/lang/String;
    .end local v10    # "selectionArgs2":[Ljava/lang/String;
    .end local v23    # "channelCursor":Landroid/database/Cursor;
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI_UPDATE_NOTIFICATION:Landroid/net/Uri;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v22

    invoke-static {v3, v6, v7, v0}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move-object/from16 v0, v24

    invoke-static {v3, v0, v6}, Lcom/samsung/android/magazine/service/Notifier;->sendCardUpdatedBroadcast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 279
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "selection":Ljava/lang/String;
    .end local v19    # "arr$":[Landroid/content/ContentValues;
    .end local v22    # "channel":Ljava/lang/String;
    .end local v24    # "channelPackageName":Ljava/lang/String;
    .end local v26    # "cursor":Landroid/database/Cursor;
    .end local v28    # "i$":I
    .end local v29    # "len$":I
    :cond_f
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 280
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 281
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v21    # "cardId":Ljava/lang/Integer;
    :catchall_1
    move-exception v3

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v3

    .line 286
    .end local v31    # "rowCount":I
    :sswitch_2
    const/16 v31, 0x0

    .line 287
    .restart local v31    # "rowCount":I
    monitor-enter p0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 288
    :try_start_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 289
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 290
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->bulkInsertMultiLanguageForCardType(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;)I

    move-result v31

    .line 291
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 292
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 293
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_2
    move-exception v3

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v3

    .line 298
    .end local v31    # "rowCount":I
    :sswitch_3
    const/16 v31, 0x0

    .line 299
    .restart local v31    # "rowCount":I
    monitor-enter p0
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    .line 300
    :try_start_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 301
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 302
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->bulkInsertMultiLanguageForCardTypeUpdate(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;)I

    move-result v31

    .line 303
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 304
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 305
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_3
    move-exception v3

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    throw v3

    .line 310
    .end local v31    # "rowCount":I
    :sswitch_4
    const/16 v31, 0x0

    .line 311
    .restart local v31    # "rowCount":I
    monitor-enter p0
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 312
    :try_start_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 313
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 314
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->bulkInsertMultiLanguageForSection(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;)I

    move-result v31

    .line 315
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 316
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 317
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_4
    move-exception v3

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :try_start_b
    throw v3

    .line 322
    .end local v31    # "rowCount":I
    :sswitch_5
    const/16 v31, 0x0

    .line 323
    .restart local v31    # "rowCount":I
    monitor-enter p0
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_0

    .line 324
    :try_start_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 325
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 326
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->bulkInsertMultiLanguageForSectionUpdate(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;)I

    move-result v31

    .line 327
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 328
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 329
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_5
    move-exception v3

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    :try_start_d
    throw v3

    .line 334
    .end local v31    # "rowCount":I
    :sswitch_6
    const/16 v31, 0x0

    .line 335
    .restart local v31    # "rowCount":I
    monitor-enter p0
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_0

    .line 336
    :try_start_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 337
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 339
    move-object/from16 v19, p2

    .restart local v19    # "arr$":[Landroid/content/ContentValues;
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v29, v0

    .restart local v29    # "len$":I
    const/16 v28, 0x0

    .restart local v28    # "i$":I
    :goto_3
    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_11

    aget-object v25, v19, v28

    .line 340
    .restart local v25    # "contentValues":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->insertProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v32

    .line 341
    .restart local v32    # "rowId":J
    const-wide/16 v6, -0x1

    cmp-long v3, v32, v6

    if-lez v3, :cond_10

    .line 342
    add-int/lit8 v31, v31, 0x1

    .line 339
    :cond_10
    add-int/lit8 v28, v28, 0x1

    goto :goto_3

    .line 346
    .end local v25    # "contentValues":Landroid/content/ContentValues;
    .end local v32    # "rowId":J
    :cond_11
    if-lez v31, :cond_13

    .line 347
    const/4 v3, 0x0

    aget-object v3, p2, v3

    const-string v6, "provider"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 348
    .local v30, "provider":Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, p2, v3

    const-string v6, "channel_key"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 349
    .restart local v22    # "channel":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 350
    const-string v22, "channel_not_defined"

    .line 353
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI_CHANGE_NOTIFICATION:Landroid/net/Uri;

    move-object/from16 v0, v30

    move-object/from16 v1, v22

    invoke-static {v3, v6, v0, v1}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    .end local v22    # "channel":Ljava/lang/String;
    .end local v30    # "provider":Ljava/lang/String;
    :cond_13
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 357
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 358
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v19    # "arr$":[Landroid/content/ContentValues;
    .end local v28    # "i$":I
    .end local v29    # "len$":I
    :catchall_6
    move-exception v3

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    :try_start_f
    throw v3
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_0

    .line 165
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xca -> :sswitch_1
        0x190 -> :sswitch_2
        0x192 -> :sswitch_3
        0x193 -> :sswitch_4
        0x194 -> :sswitch_5
        0x1f4 -> :sswitch_6
    .end sparse-switch
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 629
    :try_start_0
    sget-object v4, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 798
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unsupported URI: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 800
    :catch_0
    move-exception v1

    .line 801
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    const-string v4, "MagazineService::CardProviderContentProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Database operation failed. : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 803
    const/4 v2, 0x0

    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    return v2

    .line 632
    :sswitch_0
    const/4 v2, 0x0

    .line 633
    .local v2, "rowCount":I
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 634
    :try_start_2
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 635
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 636
    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteCards(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 637
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 638
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 639
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v4

    .line 644
    .end local v2    # "rowCount":I
    :sswitch_1
    const/4 v2, 0x0

    .line 646
    .restart local v2    # "rowCount":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 647
    .local v3, "sb":Ljava/lang/StringBuilder;
    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 648
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 649
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 650
    const-string v4, " AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 651
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 652
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 654
    :cond_0
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 655
    :try_start_4
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 656
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 657
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v4, p3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteCards(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 658
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 659
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 660
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v4

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v4

    .line 665
    .end local v2    # "rowCount":I
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :sswitch_2
    const/4 v2, 0x0

    .line 666
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 667
    :try_start_6
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 668
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 669
    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteCardElements(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 670
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 671
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 672
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_2
    move-exception v4

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v4

    .line 677
    .end local v2    # "rowCount":I
    :sswitch_3
    const/4 v2, 0x0

    .line 679
    .restart local v2    # "rowCount":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 680
    .restart local v3    # "sb":Ljava/lang/StringBuilder;
    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 681
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 683
    const-string v4, " AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 684
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 685
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 687
    :cond_1
    monitor-enter p0
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    .line 688
    :try_start_8
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 689
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 690
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v4, p3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteCardElements(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 691
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 692
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 693
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_3
    move-exception v4

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    throw v4

    .line 699
    .end local v2    # "rowCount":I
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :sswitch_4
    const/4 v2, 0x0

    .line 700
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 701
    :try_start_a
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 702
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v4, "multi_language"

    invoke-virtual {v0, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 703
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_4
    move-exception v4

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :try_start_b
    throw v4

    .line 708
    .end local v2    # "rowCount":I
    :sswitch_5
    const/4 v2, 0x0

    .line 710
    .restart local v2    # "rowCount":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 711
    .restart local v3    # "sb":Ljava/lang/StringBuilder;
    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 712
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 713
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 714
    const-string v4, " AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 715
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 716
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 718
    :cond_2
    monitor-enter p0
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_0

    .line 719
    :try_start_c
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 720
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v4, "multi_language"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 721
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_5
    move-exception v4

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    :try_start_d
    throw v4

    .line 726
    .end local v2    # "rowCount":I
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :sswitch_6
    const/4 v2, 0x0

    .line 727
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_0

    .line 728
    :try_start_e
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 729
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 730
    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 731
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 732
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 733
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_6
    move-exception v4

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    :try_start_f
    throw v4

    .line 738
    .end local v2    # "rowCount":I
    :sswitch_7
    const/4 v2, 0x0

    .line 740
    .restart local v2    # "rowCount":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 741
    .restart local v3    # "sb":Ljava/lang/StringBuilder;
    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 742
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 743
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 744
    const-string v4, " AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 745
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 746
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 748
    :cond_3
    monitor-enter p0
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_0

    .line 749
    :try_start_10
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 750
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 751
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v4, p3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 752
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 753
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 754
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_7
    move-exception v4

    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_7

    :try_start_11
    throw v4

    .line 759
    .end local v2    # "rowCount":I
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :sswitch_8
    const/4 v2, 0x0

    .line 760
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_0

    .line 761
    :try_start_12
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 762
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 763
    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteSections(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 764
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 765
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 766
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_8
    move-exception v4

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_8

    :try_start_13
    throw v4

    .line 771
    .end local v2    # "rowCount":I
    :sswitch_9
    const/4 v2, 0x0

    .line 773
    .restart local v2    # "rowCount":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 774
    .restart local v3    # "sb":Ljava/lang/StringBuilder;
    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 775
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 776
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 777
    const-string v4, " AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 778
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 779
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 781
    :cond_4
    monitor-enter p0
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_0

    .line 782
    :try_start_14
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 783
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 784
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v4, p3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->deleteSections(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 785
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 786
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 787
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_9
    move-exception v4

    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_9

    :try_start_15
    throw v4

    .line 796
    .end local v2    # "rowCount":I
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :sswitch_a
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unsupported opeation: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_15
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_15 .. :try_end_15} :catch_0

    .line 629
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_3
        0x12c -> :sswitch_a
        0x12d -> :sswitch_a
        0x190 -> :sswitch_4
        0x191 -> :sswitch_5
        0x193 -> :sswitch_4
        0x1f4 -> :sswitch_6
        0x1f5 -> :sswitch_7
        0x258 -> :sswitch_a
        0x259 -> :sswitch_a
        0x2bc -> :sswitch_8
        0x2bd -> :sswitch_9
        0x320 -> :sswitch_a
        0x321 -> :sswitch_a
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1078
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1080
    :sswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardprovider.card"

    .line 1111
    :goto_0
    return-object v0

    .line 1082
    :sswitch_1
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardprovider.card"

    goto :goto_0

    .line 1084
    :sswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardprovider.card_element"

    goto :goto_0

    .line 1086
    :sswitch_3
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardprovider.card_element"

    goto :goto_0

    .line 1088
    :sswitch_4
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardprovider.channel"

    goto :goto_0

    .line 1090
    :sswitch_5
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardprovider.channel"

    goto :goto_0

    .line 1093
    :sswitch_6
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardprovider.multi_language"

    goto :goto_0

    .line 1095
    :sswitch_7
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardprovider.multi_language"

    goto :goto_0

    .line 1097
    :sswitch_8
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardprovider.provided_card_type"

    goto :goto_0

    .line 1099
    :sswitch_9
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardprovider.provided_card_type"

    goto :goto_0

    .line 1101
    :sswitch_a
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardprovider.provider"

    goto :goto_0

    .line 1103
    :sswitch_b
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardprovider.provider"

    goto :goto_0

    .line 1105
    :sswitch_c
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardprovider.section"

    goto :goto_0

    .line 1107
    :sswitch_d
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardprovider.section"

    goto :goto_0

    .line 1109
    :sswitch_e
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardprovider.setting"

    goto :goto_0

    .line 1111
    :sswitch_f
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardprovider.setting"

    goto :goto_0

    .line 1078
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_3
        0x12c -> :sswitch_4
        0x12d -> :sswitch_5
        0x190 -> :sswitch_6
        0x191 -> :sswitch_7
        0x193 -> :sswitch_6
        0x1f4 -> :sswitch_8
        0x1f5 -> :sswitch_9
        0x258 -> :sswitch_a
        0x259 -> :sswitch_b
        0x2bc -> :sswitch_c
        0x2bd -> :sswitch_d
        0x320 -> :sswitch_e
        0x321 -> :sswitch_f
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 20
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1134
    :try_start_0
    sget-object v3, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 1273
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unsupported URI: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1275
    :catch_0
    move-exception v14

    .line 1276
    .local v14, "e":Landroid/database/sqlite/SQLiteException;
    const-string v3, "MagazineService::CardProviderContentProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Database operation failed. : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1277
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 1278
    const/16 v16, 0x0

    .end local v14    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    return-object v16

    .line 1137
    :sswitch_0
    const/4 v11, 0x0

    .line 1138
    .local v11, "cardUri":Landroid/net/Uri;
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1139
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1140
    .local v2, "database":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1, v3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->insertCard(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Z)Landroid/net/Uri;

    move-result-object v11

    .line 1141
    monitor-exit p0

    move-object/from16 v16, v11

    .line 1142
    goto :goto_0

    .line 1141
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v3

    .line 1146
    .end local v11    # "cardUri":Landroid/net/Uri;
    :sswitch_1
    const/4 v11, 0x0

    .line 1147
    .restart local v11    # "cardUri":Landroid/net/Uri;
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1148
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1149
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1, v3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->insertCard(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Z)Landroid/net/Uri;

    move-result-object v11

    .line 1150
    monitor-exit p0

    move-object/from16 v16, v11

    .line 1151
    goto :goto_0

    .line 1150
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v3

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v3

    .line 1155
    .end local v11    # "cardUri":Landroid/net/Uri;
    :sswitch_2
    const/16 v16, 0x0

    .line 1156
    .local v16, "resultUri":Landroid/net/Uri;
    monitor-enter p0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1157
    :try_start_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1158
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1159
    const-wide/16 v18, -0x2

    .line 1160
    .local v18, "rowId":J
    const-string v3, "card_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    .line 1161
    .local v10, "cardId":Ljava/lang/Integer;
    if-eqz v10, :cond_1

    .line 1162
    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v3

    .line 1163
    .local v4, "projection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v10}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1164
    .local v5, "selection":Ljava/lang/String;
    const-string v3, "card"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1165
    .local v13, "cursor":Landroid/database/Cursor;
    if-eqz v13, :cond_1

    .line 1166
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 1167
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->insertCardElement(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v18

    .line 1169
    :cond_0
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1173
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "selection":Ljava/lang/String;
    .end local v13    # "cursor":Landroid/database/Cursor;
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v3, v18, v6

    if-lez v3, :cond_3

    .line 1174
    sget-object v3, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$CardElement;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 1181
    :cond_2
    :goto_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1182
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1183
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v10    # "cardId":Ljava/lang/Integer;
    .end local v18    # "rowId":J
    :catchall_2
    move-exception v3

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v3
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    .line 1177
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v10    # "cardId":Ljava/lang/Integer;
    .restart local v18    # "rowId":J
    :cond_3
    const-wide/16 v6, -0x2

    cmp-long v3, v18, v6

    if-nez v3, :cond_2

    .line 1178
    :try_start_8
    const-string v3, "MagazineService::CardProviderContentProvider"

    const-string v6, "Can not insert a card element. The card do not exist."

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_1

    .line 1188
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v10    # "cardId":Ljava/lang/Integer;
    .end local v16    # "resultUri":Landroid/net/Uri;
    .end local v18    # "rowId":J
    :sswitch_3
    const-wide/16 v18, -0x1

    .line 1189
    .restart local v18    # "rowId":J
    :try_start_9
    monitor-enter p0
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 1190
    :try_start_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1191
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->insertMultiLanguage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v18

    .line 1192
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 1193
    const-wide/16 v6, -0x1

    cmp-long v3, v18, v6

    if-lez v3, :cond_4

    .line 1194
    :try_start_b
    sget-object v3, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_0

    move-result-object v16

    goto/16 :goto_0

    .line 1192
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_3
    move-exception v3

    :try_start_c
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    throw v3

    .line 1197
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_4
    const/16 v16, 0x0

    goto/16 :goto_0

    .line 1202
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v18    # "rowId":J
    :sswitch_4
    const/16 v16, 0x0

    .line 1203
    .restart local v16    # "resultUri":Landroid/net/Uri;
    monitor-enter p0
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_0

    .line 1204
    :try_start_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1205
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->insertProvider(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v18

    .line 1206
    .restart local v18    # "rowId":J
    const-wide/16 v6, -0x1

    cmp-long v3, v18, v6

    if-gtz v3, :cond_5

    const-wide/16 v6, -0x2

    cmp-long v3, v18, v6

    if-nez v3, :cond_6

    .line 1207
    :cond_5
    sget-object v3, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Provider;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 1209
    :cond_6
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v18    # "rowId":J
    :catchall_4
    move-exception v3

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    :try_start_f
    throw v3

    .line 1214
    .end local v16    # "resultUri":Landroid/net/Uri;
    :sswitch_5
    const-wide/16 v18, -0x1

    .line 1215
    .restart local v18    # "rowId":J
    monitor-enter p0
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_0

    .line 1216
    :try_start_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1217
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->insertProvidedCardTypeWithSetting(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v18

    .line 1218
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    .line 1219
    const-wide/16 v6, -0x1

    cmp-long v3, v18, v6

    if-lez v3, :cond_7

    .line 1220
    :try_start_11
    sget-object v3, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_0

    move-result-object v16

    goto/16 :goto_0

    .line 1218
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_5
    move-exception v3

    :try_start_12
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    :try_start_13
    throw v3

    .line 1223
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_7
    const/16 v16, 0x0

    goto/16 :goto_0

    .line 1228
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v18    # "rowId":J
    :sswitch_6
    const/16 v16, 0x0

    .line 1229
    .restart local v16    # "resultUri":Landroid/net/Uri;
    monitor-enter p0
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_0

    .line 1230
    :try_start_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1231
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1233
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->insertProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v18

    .line 1234
    .restart local v18    # "rowId":J
    const-wide/16 v6, -0x1

    cmp-long v3, v18, v6

    if-lez v3, :cond_a

    .line 1235
    const-string v3, "provider"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1236
    .local v15, "provider":Ljava/lang/String;
    const-string v3, "channel_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1237
    .local v12, "channel":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1238
    const-string v12, "channel_not_defined"

    .line 1242
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI_CHANGE_NOTIFICATION:Landroid/net/Uri;

    invoke-static {v3, v6, v15, v12}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 1244
    sget-object v3, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 1252
    .end local v12    # "channel":Ljava/lang/String;
    .end local v15    # "provider":Ljava/lang/String;
    :cond_9
    :goto_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1253
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1254
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v18    # "rowId":J
    :catchall_6
    move-exception v3

    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    :try_start_15
    throw v3
    :try_end_15
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_15 .. :try_end_15} :catch_0

    .line 1247
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v18    # "rowId":J
    :cond_a
    const-wide/16 v6, -0x2

    cmp-long v3, v18, v6

    if-eqz v3, :cond_b

    const-wide/16 v6, -0x3

    cmp-long v3, v18, v6

    if-nez v3, :cond_9

    .line 1249
    :cond_b
    :try_start_16
    sget-object v3, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_6

    move-result-object v16

    goto :goto_2

    .line 1259
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v16    # "resultUri":Landroid/net/Uri;
    .end local v18    # "rowId":J
    :sswitch_7
    const/16 v16, 0x0

    .line 1260
    .restart local v16    # "resultUri":Landroid/net/Uri;
    :try_start_17
    monitor-enter p0
    :try_end_17
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_17 .. :try_end_17} :catch_0

    .line 1261
    :try_start_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1262
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->insertSection(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v18

    .line 1263
    .restart local v18    # "rowId":J
    const-wide/16 v6, -0x1

    cmp-long v3, v18, v6

    if-lez v3, :cond_c

    .line 1264
    sget-object v3, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Section;->CONTENT_URI:Landroid/net/Uri;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 1266
    :cond_c
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v18    # "rowId":J
    :catchall_7
    move-exception v3

    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_7

    :try_start_19
    throw v3

    .line 1271
    .end local v16    # "resultUri":Landroid/net/Uri;
    :sswitch_8
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unsupported opeation: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_19
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_19 .. :try_end_19} :catch_0

    .line 1134
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x66 -> :sswitch_1
        0xc8 -> :sswitch_2
        0x12c -> :sswitch_8
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_6
        0x1f6 -> :sswitch_5
        0x258 -> :sswitch_4
        0x2bc -> :sswitch_7
        0x320 -> :sswitch_8
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 149
    const-string v0, "MagazineService Version"

    const-string v1, "Magazine-Provider: 14"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-virtual {p0}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    .line 151
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/magazine/service/db/CardDbHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    .line 152
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    if-nez v0, :cond_0

    .line 153
    const/4 v0, 0x0

    .line 155
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 2162
    const/4 v4, 0x0

    .line 2164
    .local v4, "fileDescriptor":Landroid/os/ParcelFileDescriptor;
    sget-object v8, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v8, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 2254
    new-instance v8, Ljava/io/FileNotFoundException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unsupported URI: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2169
    :sswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    .line 2170
    .local v6, "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x5

    if-lt v8, v9, :cond_0

    .line 2171
    new-instance v1, Ljava/io/File;

    iget-object v8, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    const-string v9, "image"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v9

    const/4 v8, 0x1

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v1, v9, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2172
    .local v1, "directory":Ljava/io/File;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v8, 0x3

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v8, 0x4

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2173
    .local v5, "fileName":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2175
    .local v3, "file":Ljava/io/File;
    const-string v8, "r"

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2176
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2177
    const/high16 v8, 0x10000000

    invoke-static {v3, v8}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    .line 2257
    .end local v1    # "directory":Ljava/io/File;
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "fileName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v4

    .line 2180
    .restart local v1    # "directory":Ljava/io/File;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fileName":Ljava/lang/String;
    :cond_1
    new-instance v8, Ljava/io/FileNotFoundException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "File does not exist: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2183
    :cond_2
    const-string v8, "w"

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2184
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2185
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2186
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 2194
    :cond_3
    :goto_1
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2201
    :goto_2
    const/high16 v8, 0x20000000

    invoke-static {v3, v8}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    goto :goto_0

    .line 2190
    :cond_4
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    goto :goto_1

    .line 2196
    :catch_0
    move-exception v2

    .line 2197
    .local v2, "e":Ljava/io/IOException;
    const-string v8, "MagazineService::CardProviderContentProvider"

    const-string v9, "Failed to create image file for card"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2198
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 2204
    .end local v2    # "e":Ljava/io/IOException;
    :cond_5
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unsupported Mode: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2213
    .end local v1    # "directory":Ljava/io/File;
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v6    # "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    .line 2214
    .restart local v6    # "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x5

    if-lt v8, v9, :cond_0

    .line 2215
    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    const-string v9, "image"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v9

    const/4 v8, 0x2

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v7, v9, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2216
    .local v7, "providerDirectory":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const/4 v8, 0x3

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v0, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2217
    .local v0, "cardNameDirectory":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    const/4 v8, 0x4

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v3, v0, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2218
    .restart local v3    # "file":Ljava/io/File;
    const-string v8, "r"

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2219
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2220
    const/high16 v8, 0x10000000

    invoke-static {v3, v8}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    goto/16 :goto_0

    .line 2223
    :cond_6
    new-instance v8, Ljava/io/FileNotFoundException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "File does not exist: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2226
    :cond_7
    const-string v8, "w"

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 2227
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 2228
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2229
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 2237
    :cond_8
    :goto_3
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2244
    :goto_4
    const/high16 v8, 0x20000000

    invoke-static {v3, v8}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    goto/16 :goto_0

    .line 2233
    :cond_9
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    goto :goto_3

    .line 2239
    :catch_1
    move-exception v2

    .line 2240
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "MagazineService::CardProviderContentProvider"

    const-string v9, "Failed to create image file for card name"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2241
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 2248
    .end local v2    # "e":Ljava/io/IOException;
    :cond_a
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unsupported Mode: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2164
    nop

    :sswitch_data_0
    .sparse-switch
        0x6e -> :sswitch_0
        0x1fe -> :sswitch_1
    .end sparse-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 19
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1723
    :try_start_0
    sget-object v3, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 1939
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unsupported URI: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1941
    :catch_0
    move-exception v17

    .line 1942
    .local v17, "e":Landroid/database/sqlite/SQLiteException;
    const-string v3, "MagazineService::CardProviderContentProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Database operation failed. : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1943
    invoke-virtual/range {v17 .. v17}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 1944
    const/16 v16, 0x0

    .end local v17    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    return-object v16

    .line 1726
    :sswitch_0
    const/4 v7, 0x0

    .line 1727
    .local v7, "groupBy":Ljava/lang/String;
    if-eqz p2, :cond_0

    :try_start_1
    move-object/from16 v0, p2

    array-length v3, v0

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v3, 0x0

    aget-object v3, p2, v3

    const-string v4, "card_key"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1729
    const-string v7, "card_key"

    .line 1731
    :cond_0
    const/16 v16, 0x0

    .line 1732
    .local v16, "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1733
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1734
    .local v2, "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "card"

    const/4 v8, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v9, p5

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1735
    monitor-exit p0

    goto :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v3

    .line 1740
    .end local v7    # "groupBy":Ljava/lang/String;
    .end local v16    # "cursor":Landroid/database/Cursor;
    :sswitch_1
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v3, "_id"

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1741
    .local v18, "sb":Ljava/lang/StringBuilder;
    const-string v3, "="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1742
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1743
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1744
    const-string v3, " AND ("

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1745
    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1746
    const-string v3, ")"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1748
    :cond_1
    const/16 v16, 0x0

    .line 1749
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1750
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1751
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "card"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1752
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v3

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v3

    .line 1757
    .end local v16    # "cursor":Landroid/database/Cursor;
    .end local v18    # "sb":Ljava/lang/StringBuilder;
    :sswitch_2
    const/16 v16, 0x0

    .line 1758
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1759
    :try_start_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1760
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "card_element"

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1761
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_2
    move-exception v3

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v3

    .line 1766
    .end local v16    # "cursor":Landroid/database/Cursor;
    :sswitch_3
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v3, "_id"

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1767
    .restart local v18    # "sb":Ljava/lang/StringBuilder;
    const-string v3, "="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1768
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1769
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1770
    const-string v3, " AND ("

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1771
    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1772
    const-string v3, ")"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1774
    :cond_2
    const/16 v16, 0x0

    .line 1775
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    .line 1776
    :try_start_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1777
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "card_element"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1778
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_3
    move-exception v3

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    throw v3

    .line 1783
    .end local v16    # "cursor":Landroid/database/Cursor;
    .end local v18    # "sb":Ljava/lang/StringBuilder;
    :sswitch_4
    const/16 v16, 0x0

    .line 1784
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 1785
    :try_start_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1786
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "channel"

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1787
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_4
    move-exception v3

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :try_start_b
    throw v3

    .line 1792
    .end local v16    # "cursor":Landroid/database/Cursor;
    :sswitch_5
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v3, "_id"

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1793
    .restart local v18    # "sb":Ljava/lang/StringBuilder;
    const-string v3, "="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1794
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1795
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1796
    const-string v3, " AND ("

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1797
    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1798
    const-string v3, ")"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1800
    :cond_3
    const/16 v16, 0x0

    .line 1801
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_0

    .line 1802
    :try_start_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1803
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "channel"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1804
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_5
    move-exception v3

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    :try_start_d
    throw v3

    .line 1810
    .end local v16    # "cursor":Landroid/database/Cursor;
    .end local v18    # "sb":Ljava/lang/StringBuilder;
    :sswitch_6
    const/16 v16, 0x0

    .line 1811
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_0

    .line 1812
    :try_start_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1813
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "multi_language"

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1814
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_6
    move-exception v3

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    :try_start_f
    throw v3

    .line 1819
    .end local v16    # "cursor":Landroid/database/Cursor;
    :sswitch_7
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v3, "_id"

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1820
    .restart local v18    # "sb":Ljava/lang/StringBuilder;
    const-string v3, "="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1821
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1822
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1823
    const-string v3, " AND ("

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1824
    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1825
    const-string v3, ")"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1827
    :cond_4
    const/16 v16, 0x0

    .line 1828
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_0

    .line 1829
    :try_start_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1830
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "multi_language"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1831
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_7
    move-exception v3

    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_7

    :try_start_11
    throw v3

    .line 1836
    .end local v16    # "cursor":Landroid/database/Cursor;
    .end local v18    # "sb":Ljava/lang/StringBuilder;
    :sswitch_8
    const/16 v16, 0x0

    .line 1837
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_0

    .line 1838
    :try_start_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1839
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "provider"

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1840
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_8
    move-exception v3

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_8

    :try_start_13
    throw v3

    .line 1845
    .end local v16    # "cursor":Landroid/database/Cursor;
    :sswitch_9
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v3, "_id"

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1846
    .restart local v18    # "sb":Ljava/lang/StringBuilder;
    const-string v3, "="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1847
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1848
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1849
    const-string v3, " AND ("

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1850
    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1851
    const-string v3, ")"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1853
    :cond_5
    const/16 v16, 0x0

    .line 1854
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_0

    .line 1855
    :try_start_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1856
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "provider"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1857
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_9
    move-exception v3

    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_9

    :try_start_15
    throw v3

    .line 1862
    .end local v16    # "cursor":Landroid/database/Cursor;
    .end local v18    # "sb":Ljava/lang/StringBuilder;
    :sswitch_a
    const/16 v16, 0x0

    .line 1863
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_15
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_15 .. :try_end_15} :catch_0

    .line 1864
    :try_start_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1865
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "provided_card_type"

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1866
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_a
    move-exception v3

    monitor-exit p0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_a

    :try_start_17
    throw v3

    .line 1871
    .end local v16    # "cursor":Landroid/database/Cursor;
    :sswitch_b
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v3, "_id"

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1872
    .restart local v18    # "sb":Ljava/lang/StringBuilder;
    const-string v3, "="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1873
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1874
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 1875
    const-string v3, " AND ("

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1876
    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1877
    const-string v3, ")"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879
    :cond_6
    const/16 v16, 0x0

    .line 1880
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_17
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_17 .. :try_end_17} :catch_0

    .line 1881
    :try_start_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1882
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "provided_card_type"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1883
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_b
    move-exception v3

    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_b

    :try_start_19
    throw v3

    .line 1888
    .end local v16    # "cursor":Landroid/database/Cursor;
    .end local v18    # "sb":Ljava/lang/StringBuilder;
    :sswitch_c
    const/16 v16, 0x0

    .line 1889
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_19
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_19 .. :try_end_19} :catch_0

    .line 1890
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1891
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "section"

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1892
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_c
    move-exception v3

    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_c

    :try_start_1b
    throw v3

    .line 1897
    .end local v16    # "cursor":Landroid/database/Cursor;
    :sswitch_d
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v3, "_id"

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1898
    .restart local v18    # "sb":Ljava/lang/StringBuilder;
    const-string v3, "="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1899
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1900
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1901
    const-string v3, " AND ("

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1902
    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1903
    const-string v3, ")"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1905
    :cond_7
    const/16 v16, 0x0

    .line 1906
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_1b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1b .. :try_end_1b} :catch_0

    .line 1907
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1908
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "section"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1909
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_d
    move-exception v3

    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_d

    :try_start_1d
    throw v3

    .line 1914
    .end local v16    # "cursor":Landroid/database/Cursor;
    .end local v18    # "sb":Ljava/lang/StringBuilder;
    :sswitch_e
    const/16 v16, 0x0

    .line 1915
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_1d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1d .. :try_end_1d} :catch_0

    .line 1916
    :try_start_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1917
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "setting"

    const-string v13, "card_type_key"

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1918
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_e
    move-exception v3

    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_e

    :try_start_1f
    throw v3

    .line 1923
    .end local v16    # "cursor":Landroid/database/Cursor;
    :sswitch_f
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v3, "_id"

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1924
    .restart local v18    # "sb":Ljava/lang/StringBuilder;
    const-string v3, "="

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1925
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1926
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 1927
    const-string v3, " AND ("

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1928
    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1929
    const-string v3, ")"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1931
    :cond_8
    const/16 v16, 0x0

    .line 1932
    .restart local v16    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_1f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1f .. :try_end_1f} :catch_0

    .line 1933
    :try_start_20
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1934
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v9, "setting"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const-string v13, "card_type_key"

    const/4 v14, 0x0

    move-object v8, v2

    move-object/from16 v10, p2

    move-object/from16 v12, p4

    move-object/from16 v15, p5

    invoke-virtual/range {v8 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1935
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_f
    move-exception v3

    monitor-exit p0
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_f

    :try_start_21
    throw v3
    :try_end_21
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_21 .. :try_end_21} :catch_0

    .line 1723
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_3
        0x12c -> :sswitch_4
        0x12d -> :sswitch_5
        0x190 -> :sswitch_6
        0x191 -> :sswitch_7
        0x193 -> :sswitch_6
        0x1f4 -> :sswitch_a
        0x1f5 -> :sswitch_b
        0x258 -> :sswitch_8
        0x259 -> :sswitch_9
        0x2bc -> :sswitch_c
        0x2bd -> :sswitch_d
        0x320 -> :sswitch_e
        0x321 -> :sswitch_f
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 21
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1965
    :try_start_0
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 2101
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unsupported URI: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2103
    :catch_0
    move-exception v16

    .line 2104
    .local v16, "e":Landroid/database/sqlite/SQLiteException;
    const-string v5, "MagazineService::CardProviderContentProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Database operation failed. : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2105
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 2106
    const/16 v19, 0x0

    .end local v16    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    :goto_0
    return v19

    .line 1968
    :sswitch_0
    const/16 v19, 0x0

    .line 1969
    .local v19, "rowCount":I
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1970
    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1971
    .local v4, "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v5, "card"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v19

    .line 1972
    monitor-exit p0

    goto :goto_0

    .end local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v5

    .line 1977
    .end local v19    # "rowCount":I
    :sswitch_1
    const/16 v19, 0x0

    .line 1978
    .restart local v19    # "rowCount":I
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1979
    :try_start_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1980
    .restart local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v5, 0x2

    new-array v6, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "_id"

    aput-object v7, v6, v5

    const/4 v5, 0x1

    const-string v7, "card_channel"

    aput-object v7, v6, v5

    .line 1981
    .local v6, "projection":[Ljava/lang/String;
    new-instance v13, Landroid/util/SparseArray;

    invoke-direct {v13}, Landroid/util/SparseArray;-><init>()V

    .line 1983
    .local v13, "cardList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const-string v5, "card"

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 1984
    .local v15, "cursor":Landroid/database/Cursor;
    if-eqz v15, :cond_3

    .line 1985
    :goto_1
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1986
    const/4 v5, 0x0

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 1987
    .local v12, "cardId":I
    const/4 v5, 0x1

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1988
    .local v14, "channel":Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1989
    const-string v14, "channel_not_defined"

    .line 1991
    :cond_1
    invoke-virtual {v13, v12, v14}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_1

    .line 2006
    .end local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v12    # "cardId":I
    .end local v13    # "cardList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .end local v14    # "channel":Ljava/lang/String;
    .end local v15    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v5

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v5
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1993
    .restart local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v6    # "projection":[Ljava/lang/String;
    .restart local v13    # "cardList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .restart local v15    # "cursor":Landroid/database/Cursor;
    :cond_2
    :try_start_6
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 1996
    :cond_3
    const-string v5, "card"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v19

    .line 1997
    if-lez v19, :cond_4

    .line 1998
    invoke-virtual {v13}, Landroid/util/SparseArray;->size()I

    move-result v18

    .line 1999
    .local v18, "listSize":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_2
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_4

    .line 2000
    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v12

    .line 2001
    .restart local v12    # "cardId":I
    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 2002
    .restart local v14    # "channel":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mContext:Landroid/content/Context;

    sget-object v7, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI_UPDATE_NOTIFICATION:Landroid/net/Uri;

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v7, v8, v14}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 1999
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 2006
    .end local v12    # "cardId":I
    .end local v14    # "channel":Ljava/lang/String;
    .end local v17    # "i":I
    .end local v18    # "listSize":I
    :cond_4
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 2011
    .end local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v13    # "cardList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .end local v15    # "cursor":Landroid/database/Cursor;
    .end local v19    # "rowCount":I
    :sswitch_2
    const/16 v19, 0x0

    .line 2012
    .restart local v19    # "rowCount":I
    :try_start_7
    monitor-enter p0
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    .line 2013
    :try_start_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 2014
    .restart local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->updateMultiLanguages(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v19

    .line 2015
    monitor-exit p0

    goto/16 :goto_0

    .end local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_2
    move-exception v5

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v5

    .line 2020
    .end local v19    # "rowCount":I
    :sswitch_3
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v5, "_id"

    move-object/from16 v0, v20

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2021
    .local v20, "sb":Ljava/lang/StringBuilder;
    const-string v5, "="

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2022
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2023
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2024
    const-string v5, " AND ("

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2025
    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2026
    const-string v5, ")"

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2027
    :cond_5
    const/16 v19, 0x0

    .line 2028
    .restart local v19    # "rowCount":I
    monitor-enter p0
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 2029
    :try_start_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 2030
    .restart local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    invoke-direct {v0, v4, v1, v5, v2}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->updateMultiLanguages(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v19

    .line 2031
    monitor-exit p0

    goto/16 :goto_0

    .end local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_3
    move-exception v5

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :try_start_b
    throw v5

    .line 2036
    .end local v19    # "rowCount":I
    .end local v20    # "sb":Ljava/lang/StringBuilder;
    :sswitch_4
    const/16 v19, 0x0

    .line 2037
    .restart local v19    # "rowCount":I
    const-string v5, "package_name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 2038
    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 2039
    monitor-enter p0
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_0

    .line 2040
    :try_start_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 2041
    .restart local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v5, "provider"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v19

    .line 2042
    monitor-exit p0

    goto/16 :goto_0

    .end local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_4
    move-exception v5

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    :try_start_d
    throw v5

    .line 2048
    .end local v19    # "rowCount":I
    :sswitch_5
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v5, "_id"

    move-object/from16 v0, v20

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2049
    .restart local v20    # "sb":Ljava/lang/StringBuilder;
    const-string v5, "="

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2050
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2051
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 2052
    const-string v5, " AND ("

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2053
    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2054
    const-string v5, ")"

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2056
    :cond_6
    const/16 v19, 0x0

    .line 2057
    .restart local v19    # "rowCount":I
    const-string v5, "package_name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 2058
    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 2059
    monitor-enter p0
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_0

    .line 2060
    :try_start_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 2061
    .restart local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v5, "provider"

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v4, v5, v0, v7, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v19

    .line 2062
    monitor-exit p0

    goto/16 :goto_0

    .end local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_5
    move-exception v5

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    :try_start_f
    throw v5

    .line 2068
    .end local v19    # "rowCount":I
    .end local v20    # "sb":Ljava/lang/StringBuilder;
    :sswitch_6
    const/16 v19, 0x0

    .line 2069
    .restart local v19    # "rowCount":I
    monitor-enter p0
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_0

    .line 2070
    :try_start_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 2071
    .restart local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->updateProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v19

    .line 2072
    monitor-exit p0

    goto/16 :goto_0

    .end local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_6
    move-exception v5

    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_6

    :try_start_11
    throw v5

    .line 2077
    .end local v19    # "rowCount":I
    :sswitch_7
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v5, "_id"

    move-object/from16 v0, v20

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2078
    .restart local v20    # "sb":Ljava/lang/StringBuilder;
    const-string v5, "="

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2079
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2080
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 2081
    const-string v5, " AND ("

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2082
    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2083
    const-string v5, ")"

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2085
    :cond_7
    const/16 v19, 0x0

    .line 2086
    .restart local v19    # "rowCount":I
    monitor-enter p0
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_0

    .line 2087
    :try_start_12
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 2088
    .restart local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    invoke-direct {v0, v4, v1, v5, v2}, Lcom/samsung/android/magazine/service/provider/CardProviderContentProvider;->updateProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v19

    .line 2089
    monitor-exit p0

    goto/16 :goto_0

    .end local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_7
    move-exception v5

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_7

    :try_start_13
    throw v5

    .line 2099
    .end local v19    # "rowCount":I
    .end local v20    # "sb":Ljava/lang/StringBuilder;
    :sswitch_8
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unsupported opeation: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_0

    .line 1965
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_8
        0x66 -> :sswitch_1
        0xc8 -> :sswitch_8
        0xc9 -> :sswitch_8
        0x12c -> :sswitch_8
        0x12d -> :sswitch_8
        0x190 -> :sswitch_2
        0x191 -> :sswitch_3
        0x1f4 -> :sswitch_6
        0x1f5 -> :sswitch_7
        0x258 -> :sswitch_4
        0x259 -> :sswitch_5
        0x320 -> :sswitch_8
        0x321 -> :sswitch_8
    .end sparse-switch
.end method
