.class public Lcom/samsung/android/magazine/cardprovider/card/CardAction;
.super Ljava/lang/Object;
.source "CardAction.java"


# static fields
.field public static final ACTION_ACTIVITY:Ljava/lang/String; = "activity"

.field public static final ACTION_BROADCAST:Ljava/lang/String; = "broadcast"

.field public static final ACTION_SERVICE:Ljava/lang/String; = "service"


# instance fields
.field private mActionType:Ljava/lang/String;

.field private mAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mData:Landroid/content/Intent;

.field private mLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "actionType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mActionType:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mLabel:Ljava/lang/String;

    .line 47
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mData:Landroid/content/Intent;

    .line 48
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mAttributes:Ljava/util/Map;

    .line 59
    if-nez p1, :cond_0

    .line 60
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid argument. ActionType is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mActionType:Ljava/lang/String;

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mAttributes:Ljava/util/Map;

    .line 65
    return-void
.end method


# virtual methods
.method public getActionType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mActionType:Ljava/lang/String;

    return-object v0
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mAttributes:Ljava/util/Map;

    return-object v0
.end method

.method public getData()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mData:Landroid/content/Intent;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mLabel:Ljava/lang/String;

    return-object v0
.end method

.method public setAttributes(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 130
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 135
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mAttributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public setData(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mData:Landroid/content/Intent;

    .line 85
    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->mLabel:Ljava/lang/String;

    .line 75
    return-void
.end method
