.class public Lcom/samsung/android/magazine/cardprovider/card/CardElement;
.super Ljava/lang/Object;
.source "CardElement.java"


# instance fields
.field private mAction:Lcom/samsung/android/magazine/cardprovider/card/CardAction;

.field private mAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->mKey:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->mAction:Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    .line 25
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->mAttributes:Ljava/util/Map;

    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid argument. Key is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->mKey:Ljava/lang/String;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->mAttributes:Ljava/util/Map;

    .line 40
    return-void
.end method


# virtual methods
.method public getAction()Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->mAction:Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    return-object v0
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->mAttributes:Ljava/util/Map;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method public setAction(Lcom/samsung/android/magazine/cardprovider/card/CardAction;)V
    .locals 0
    .param p1, "action"    # Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->mAction:Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    .line 62
    return-void
.end method

.method public setAttributes(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 87
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->mAttributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0
.end method
