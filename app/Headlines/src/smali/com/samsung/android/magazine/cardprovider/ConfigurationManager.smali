.class public Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;
.super Ljava/lang/Object;
.source "ConfigurationManager.java"

# interfaces
.implements Lcom/samsung/android/magazine/NotificationDescriptor;


# static fields
.field private static final CHANNEL_HEADLINES:Ljava/lang/String; = "headlines"

.field private static final DEFAULT_LANGUAGE_CODE:Ljava/lang/String; = "default"

.field private static final PREFERENCE_KEY_PROVIDER_REGISTRATION:Ljava/lang/String; = "MagazineCardProviderRegistered"

.field private static final TAG:Ljava/lang/String;

.field private static final VERSION_ALPHABETA:I = 0x1

.field private static final VERSION_ALPHABETA_NUM:I = 0x6

.field private static final VERSION_MAJOR:I = 0x1

.field private static final VERSION_MINOR:I

.field private static final VERSION_PATCH:I

.field private static mInstance:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

.field static mLock:Ljava/lang/Object;


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mPackageName:Ljava/lang/String;

.field private mPackageVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mInstance:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v1, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContext:Landroid/content/Context;

    .line 57
    iput-object v1, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    .line 59
    iput v2, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageVersion:I

    .line 70
    iput-object p1, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContext:Landroid/content/Context;

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    .line 74
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v1, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageVersion:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private addSection(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Z
    .locals 12
    .param p1, "channelName"    # Ljava/lang/String;
    .param p2, "section"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 441
    .local p3, "multiLanguage":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 442
    sget-object v8, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v9, "addSection : Invalid argument. channel is empty."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    const/4 v8, 0x0

    .line 498
    :goto_0
    return v8

    .line 446
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 447
    sget-object v8, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v9, "addSection : Invalid argument. section is empty."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    const/4 v8, 0x0

    goto :goto_0

    .line 451
    :cond_1
    if-eqz p3, :cond_2

    const-string v8, "default"

    invoke-interface {p3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 452
    sget-object v8, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v9, "addSection: Invalid argument. displayNames has no data for default"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    const/4 v8, 0x0

    goto :goto_0

    .line 456
    :cond_2
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 457
    .local v5, "sectionContentValues":Landroid/content/ContentValues;
    const-string v8, "key"

    invoke-virtual {v5, v8, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    const-string v8, "channel_key"

    invoke-virtual {v5, v8, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v8, "creator"

    iget-object v9, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    sget-object v9, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v9

    .line 463
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v10, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Section;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v10, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v6

    .line 464
    .local v6, "sectionUri":Landroid/net/Uri;
    if-nez v6, :cond_3

    .line 465
    sget-object v8, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v10, "addSection : Fail to insert section"

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466
    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v8, 0x0

    goto :goto_0

    .line 469
    :cond_3
    if-eqz p3, :cond_5

    .line 471
    :try_start_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 472
    .local v7, "valuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    .line 473
    .local v1, "displayNameEntrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_4

    .line 485
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_5

    .line 486
    iget-object v10, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v11, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;->CONTENT_URI_SECTION:Landroid/net/Uri;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v8, v8, [Landroid/content/ContentValues;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Landroid/content/ContentValues;

    invoke-virtual {v10, v11, v8}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v3

    .line 487
    .local v3, "insertCount":I
    const/4 v8, 0x1

    if-ge v3, v8, :cond_5

    .line 488
    sget-object v8, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v10, "addSection : Fail to insert Multilanguage values."

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 489
    :try_start_3
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v8, 0x0

    goto/16 :goto_0

    .line 473
    .end local v3    # "insertCount":I
    :cond_4
    :try_start_4
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 475
    .local v0, "displayNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 476
    .local v4, "languageContentValues":Landroid/content/ContentValues;
    const-string v8, "type"

    const-string v11, "section"

    invoke-virtual {v4, v8, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    const-string v8, "key1"

    invoke-virtual {v4, v8, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    const-string v8, "key2"

    invoke-virtual {v4, v8, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const-string v11, "language_code"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v4, v11, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const-string v11, "value1"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v4, v11, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 493
    .end local v0    # "displayNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "displayNameEntrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v4    # "languageContentValues":Landroid/content/ContentValues;
    .end local v6    # "sectionUri":Landroid/net/Uri;
    .end local v7    # "valuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :catch_0
    move-exception v2

    .line 494
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    :try_start_5
    sget-object v8, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v10, "[Exception] addSection"

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 461
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :cond_5
    monitor-exit v9

    .line 498
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 461
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v8
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 88
    const-class v3, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    monitor-enter v3

    :try_start_0
    const-string v4, "SA Version"

    const-string v5, "CardProvider Library : 14"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    if-nez p0, :cond_0

    .line 91
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v5, "Invalid argument : The context is null."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    :goto_0
    monitor-exit v3

    return-object v2

    .line 95
    :cond_0
    const/4 v0, 0x0

    .line 97
    .local v0, "appContext":Landroid/content/Context;
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 105
    :try_start_2
    sget-object v2, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mInstance:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    if-nez v2, :cond_1

    .line 106
    new-instance v2, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    invoke-direct {v2, v0}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mInstance:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    .line 109
    :cond_1
    sget-object v2, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mInstance:Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;

    goto :goto_0

    .line 98
    :catch_0
    move-exception v1

    .line 100
    .local v1, "e":Ljava/lang/NullPointerException;
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v5, "Invalid arguments. context is invalid."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 88
    .end local v0    # "appContext":Landroid/content/Context;
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private getSections(Ljava/lang/String;)Ljava/util/Set;
    .locals 11
    .param p1, "channelName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 652
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 653
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "getSections: Invalid argument. channelName is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    :goto_0
    return-object v9

    .line 657
    :cond_0
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 659
    .local v9, "sections":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    sget-object v10, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v10

    .line 660
    :try_start_0
    const-string v3, "channel_key=?"

    .line 661
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 662
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "key"

    aput-object v1, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 664
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 666
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Section;->CONTENT_URI:Landroid/net/Uri;

    .line 667
    const/4 v5, 0x0

    .line 666
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 673
    :goto_1
    if-eqz v6, :cond_1

    .line 674
    :goto_2
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 678
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 659
    :cond_1
    monitor-exit v10

    goto :goto_0

    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 668
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 669
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getSections"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 675
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 676
    .local v8, "section":Ljava/lang/String;
    invoke-virtual {v9, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method private isCardSubscribed(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p1, "cardName"    # Ljava/lang/String;
    .param p2, "channelName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 226
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->isRegistered()Z

    move-result v0

    if-nez v0, :cond_1

    .line 227
    :cond_0
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "CardProvider is not registered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v8

    .line 276
    :goto_0
    return v0

    .line 231
    :cond_1
    sget-object v10, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v10

    .line 232
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[isCardNameSubscribed] Invalid argument. cardName is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    monitor-exit v10

    move v0, v8

    goto :goto_0

    .line 237
    :cond_2
    const/4 v3, 0x0

    .line 238
    .local v3, "where":Ljava/lang/String;
    const/4 v4, 0x0

    .line 239
    .local v4, "whereArgs":[Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 240
    const-string v3, "card_type_key=? AND enable=1"

    .line 242
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 251
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    :goto_1
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 252
    const-string v1, "card_type_key"

    aput-object v1, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 255
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 257
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Setting;->CONTENT_URI:Landroid/net/Uri;

    .line 258
    const/4 v5, 0x0

    .line 257
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 264
    :goto_2
    if-eqz v6, :cond_3

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v0, v9, :cond_6

    .line 265
    :cond_3
    if-eqz v6, :cond_4

    .line 266
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 268
    :cond_4
    monitor-exit v10

    move v0, v8

    goto :goto_0

    .line 245
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_5
    const-string v3, "card_type_key=? AND enable=1 AND channel_key=?"

    .line 248
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v0, 0x1

    aput-object p2, v4, v0

    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 259
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 260
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] isCardSubscribed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 231
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 271
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_6
    if-eqz v6, :cond_7

    .line 272
    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 231
    :cond_7
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v9

    .line 276
    goto :goto_0
.end method

.method private isRegistered()Z
    .locals 3

    .prologue
    .line 886
    iget-object v1, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 887
    .local v0, "preferences":Landroid/content/SharedPreferences;
    const-string v1, "MagazineCardProviderRegistered"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private removeSection(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "channelName"    # Ljava/lang/String;
    .param p2, "section"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 512
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 513
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v5, "removeSection: Invalid argument. channel is empty."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    :goto_0
    return-void

    .line 517
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 518
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v5, "removeSection: Invalid argument. section is empty."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 522
    :cond_1
    const-string v2, "channel_key=? AND creator=? AND key=?"

    .line 525
    .local v2, "where":Ljava/lang/String;
    const/4 v4, 0x3

    new-array v3, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    iget-object v4, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    aput-object v4, v3, v7

    const/4 v4, 0x2

    aput-object p2, v3, v4

    .line 527
    .local v3, "whereArgs":[Ljava/lang/String;
    sget-object v5, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 529
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Section;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v6, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 530
    .local v0, "deleteCount":I
    if-ge v0, v7, :cond_2

    .line 531
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v6, "removeSection: Failed to remove section."

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 527
    .end local v0    # "deleteCount":I
    :cond_2
    :goto_1
    :try_start_1
    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 533
    :catch_0
    move-exception v1

    .line 534
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v6, "[Exception] removeSection"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private sendImageToMagazine(Landroid/graphics/Bitmap;Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 899
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 900
    .local v1, "imageFileName":Ljava/lang/String;
    invoke-static {p2, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 901
    .local v3, "uri":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 904
    .local v2, "outStream":Ljava/io/OutputStream;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v4, v3}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v2

    .line 905
    if-eqz v2, :cond_0

    .line 906
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {p1, v4, v5, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 907
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 917
    :cond_0
    if-eqz v2, :cond_1

    .line 918
    :try_start_1
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 924
    :cond_1
    :goto_0
    if-nez v3, :cond_3

    .line 925
    const/4 v4, 0x0

    .line 927
    :goto_1
    return-object v4

    .line 909
    :catch_0
    move-exception v0

    .line 910
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 911
    const/4 v3, 0x0

    .line 917
    if-eqz v2, :cond_1

    .line 918
    :try_start_3
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 920
    :catch_1
    move-exception v4

    goto :goto_0

    .line 912
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 913
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_4
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v5, "[Exception] sendImageToMagazine"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 917
    if-eqz v2, :cond_1

    .line 918
    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_0

    .line 920
    :catch_3
    move-exception v4

    goto :goto_0

    .line 915
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v4

    .line 917
    if-eqz v2, :cond_2

    .line 918
    :try_start_6
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 922
    :cond_2
    :goto_2
    throw v4

    .line 927
    :cond_3
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 920
    :catch_4
    move-exception v5

    goto :goto_2

    :catch_5
    move-exception v4

    goto :goto_0
.end method

.method private setRegistered(Z)V
    .locals 3
    .param p1, "register"    # Z

    .prologue
    .line 891
    iget-object v2, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 892
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 893
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "MagazineCardProviderRegistered"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 894
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 895
    return-void
.end method

.method private unregisterCardName(Ljava/lang/String;)Z
    .locals 9
    .param p1, "cardName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 288
    iget-object v6, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContext:Landroid/content/Context;

    if-eqz v6, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->isRegistered()Z

    move-result v6

    if-nez v6, :cond_1

    .line 289
    :cond_0
    sget-object v5, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v6, "CardProvider is not registered"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :goto_0
    return v4

    .line 293
    :cond_1
    sget-object v6, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v6

    .line 294
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 295
    sget-object v5, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v7, "[unregisterCardName] Invalid argument. cardName is empty."

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    monitor-exit v6

    goto :goto_0

    .line 293
    :catchall_0
    move-exception v4

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 299
    :cond_2
    :try_start_1
    const-string v2, "card_type_key=? AND provider=?"

    .line 301
    .local v2, "where":Ljava/lang/String;
    const/4 v7, 0x2

    new-array v3, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 302
    aput-object p1, v3, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    aput-object v8, v3, v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 306
    .local v3, "whereArgs":[Ljava/lang/String;
    :try_start_2
    iget-object v7, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v8, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 308
    .local v1, "ret":I
    if-nez v1, :cond_3

    .line 309
    sget-object v7, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v8, "[unregisterCardName] The card name is not registered."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 310
    :try_start_3
    monitor-exit v6

    goto :goto_0

    .line 312
    .end local v1    # "ret":I
    :catch_0
    move-exception v0

    .line 313
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v7, "[Exception] unregisterCardName"

    invoke-static {v4, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 293
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v4, v5

    .line 317
    goto :goto_0
.end method

.method private updateSection(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Z
    .locals 16
    .param p1, "channelName"    # Ljava/lang/String;
    .param p2, "section"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 562
    .local p3, "displayNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 563
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v2, "updateSection: Invalid argument. channel is empty."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    const/4 v1, 0x0

    .line 637
    :goto_0
    return v1

    .line 567
    :cond_0
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 568
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v2, "updateSection: Invalid argument. section is empty."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    const/4 v1, 0x0

    goto :goto_0

    .line 572
    :cond_1
    if-eqz p3, :cond_2

    const-string v1, "default"

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 573
    :cond_2
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v2, "setCardDisplayName: Invalid argument. displayNames is empty or has no data for default"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    const/4 v1, 0x0

    goto :goto_0

    .line 577
    :cond_3
    sget-object v15, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v15

    .line 578
    :try_start_0
    const-string v4, "channel_key=? AND creator=? AND key=?"

    .line 581
    .local v4, "where":Ljava/lang/String;
    const/4 v1, 0x3

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 582
    aput-object p1, v5, v1

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    aput-object v2, v5, v1

    const/4 v1, 0x2

    aput-object p2, v5, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 585
    .local v5, "whereArgs":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 587
    .local v7, "checkResult":Z
    const/4 v8, 0x0

    .line 589
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Section;->CONTENT_URI:Landroid/net/Uri;

    .line 590
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 589
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 596
    :goto_1
    if-eqz v8, :cond_5

    .line 597
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_4

    .line 598
    const/4 v7, 0x1

    .line 600
    :cond_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 603
    :cond_5
    if-nez v7, :cond_6

    .line 604
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v2, "updateSection: Invalid argument. The section is not exist."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    monitor-exit v15

    const/4 v1, 0x0

    goto :goto_0

    .line 591
    :catch_0
    move-exception v11

    .line 592
    .local v11, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v2, "[Exception] getSections"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    invoke-virtual {v11}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 577
    .end local v4    # "where":Ljava/lang/String;
    .end local v5    # "whereArgs":[Ljava/lang/String;
    .end local v7    # "checkResult":Z
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v11    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    monitor-exit v15
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .restart local v4    # "where":Ljava/lang/String;
    .restart local v5    # "whereArgs":[Ljava/lang/String;
    .restart local v7    # "checkResult":Z
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :cond_6
    :try_start_3
    monitor-exit v15
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 609
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 610
    .local v14, "valuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    .line 611
    .local v10, "displayNameEntrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_7

    .line 623
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_9

    .line 624
    sget-object v2, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 626
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;->CONTENT_URI_SECTION_UPDATE:Landroid/net/Uri;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/content/ContentValues;

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/content/ContentValues;

    invoke-virtual {v3, v6, v1}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v12

    .line 627
    .local v12, "insertCount":I
    const/4 v1, 0x1

    if-ge v12, v1, :cond_8

    .line 628
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v3, "addSection : Fail to insert Multilanguage values."

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 629
    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/4 v1, 0x0

    goto/16 :goto_0

    .line 611
    .end local v12    # "insertCount":I
    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 613
    .local v9, "displayNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 614
    .local v13, "languageContentValues":Landroid/content/ContentValues;
    const-string v1, "type"

    const-string v3, "section"

    invoke-virtual {v13, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    const-string v1, "key1"

    move-object/from16 v0, p2

    invoke-virtual {v13, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    const-string v1, "key2"

    move-object/from16 v0, p1

    invoke-virtual {v13, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    const-string v3, "language_code"

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v13, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    const-string v3, "value1"

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v13, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 631
    .end local v9    # "displayNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v13    # "languageContentValues":Landroid/content/ContentValues;
    :catch_1
    move-exception v11

    .line 632
    .restart local v11    # "e":Ljava/lang/IllegalArgumentException;
    :try_start_6
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v3, "[Exception] updateSection"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    invoke-virtual {v11}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 624
    .end local v11    # "e":Ljava/lang/IllegalArgumentException;
    :cond_8
    monitor-exit v2

    .line 637
    :cond_9
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 624
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v1
.end method


# virtual methods
.method public addCardName(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/util/Map;Z)Z
    .locals 17
    .param p1, "cardName"    # Ljava/lang/String;
    .param p2, "section"    # Ljava/lang/String;
    .param p3, "icon"    # Landroid/graphics/Bitmap;
    .param p5, "isSubscribed"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 743
    .local p4, "displayNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContext:Landroid/content/Context;

    if-eqz v13, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->isRegistered()Z

    move-result v13

    if-nez v13, :cond_1

    .line 744
    :cond_0
    sget-object v13, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v14, "CardProvider is not registered"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    const/4 v10, 0x0

    .line 823
    :goto_0
    return v10

    .line 748
    :cond_1
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 749
    sget-object v13, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v14, "addCardName: Invalid argument. cardNames is empty."

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    const/4 v10, 0x0

    goto :goto_0

    .line 753
    :cond_2
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 754
    sget-object v13, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v14, "addCardName: Invalid argument. section is empty."

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 755
    const/4 v10, 0x0

    goto :goto_0

    .line 758
    :cond_3
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->isCardNameAdded(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 759
    sget-object v13, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v15, " is already added"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    const/4 v10, 0x0

    goto :goto_0

    .line 763
    :cond_4
    if-eqz p4, :cond_5

    const-string v13, "default"

    move-object/from16 v0, p4

    invoke-interface {v0, v13}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_5

    .line 764
    sget-object v13, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v14, "addCardName: Invalid argument. displayNames has no data for default"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    const/4 v10, 0x0

    goto :goto_0

    .line 768
    :cond_5
    const/4 v10, 0x1

    .line 770
    .local v10, "success":Z
    sget-object v14, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v14

    .line 772
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 773
    .local v2, "contentValues":Landroid/content/ContentValues;
    const-string v13, "card_type_key"

    move-object/from16 v0, p1

    invoke-virtual {v2, v13, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    const-string v13, "provider"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v13, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    const-string v13, "section_key"

    move-object/from16 v0, p2

    invoke-virtual {v2, v13, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    const-string v15, "default_subscription"

    if-eqz p5, :cond_7

    const/4 v13, 0x1

    :goto_1
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v2, v15, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 778
    if-eqz p3, :cond_6

    .line 779
    sget-object v13, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI_IMAGE:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    invoke-static {v13, v15}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 780
    .local v6, "imageUri":Landroid/net/Uri;
    move-object/from16 v0, p1

    invoke-static {v6, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 781
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->sendImageToMagazine(Landroid/graphics/Bitmap;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    .line 782
    .local v7, "imageUriStr":Ljava/lang/String;
    const-string v13, "icon_image"

    invoke-virtual {v2, v13, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 785
    .end local v6    # "imageUri":Landroid/net/Uri;
    .end local v7    # "imageUriStr":Ljava/lang/String;
    :cond_6
    const/4 v11, 0x0

    .line 787
    .local v11, "uri":Landroid/net/Uri;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v15, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI_CREATE:Landroid/net/Uri;

    invoke-virtual {v13, v15, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v11

    .line 789
    if-nez v11, :cond_8

    .line 790
    sget-object v13, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Failed to insert cardName ("

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "), section ("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ")"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 791
    :try_start_2
    monitor-exit v14
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v10, 0x0

    goto/16 :goto_0

    .line 776
    .end local v11    # "uri":Landroid/net/Uri;
    :cond_7
    const/4 v13, 0x0

    goto :goto_1

    .line 794
    .restart local v11    # "uri":Landroid/net/Uri;
    :cond_8
    if-eqz p4, :cond_a

    .line 795
    :try_start_3
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 796
    .local v12, "valuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-interface/range {p4 .. p4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    .line 797
    .local v4, "displayNameEntrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_9

    .line 809
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_a

    .line 810
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v16, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v13

    new-array v13, v13, [Landroid/content/ContentValues;

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Landroid/content/ContentValues;

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v13}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v8

    .line 811
    .local v8, "insertCount":I
    const/4 v13, 0x1

    if-ge v8, v13, :cond_a

    .line 812
    sget-object v13, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v15, "addCardName : Fail to insert Multilanguage values."

    invoke-static {v13, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 813
    :try_start_4
    monitor-exit v14
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v10, 0x0

    goto/16 :goto_0

    .line 797
    .end local v8    # "insertCount":I
    :cond_9
    :try_start_5
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 799
    .local v3, "displayNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 800
    .local v9, "languageContentValues":Landroid/content/ContentValues;
    const-string v13, "type"

    const-string v16, "card_type"

    move-object/from16 v0, v16

    invoke-virtual {v9, v13, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    const-string v13, "key1"

    move-object/from16 v0, p1

    invoke-virtual {v9, v13, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 802
    const-string v13, "key2"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v9, v13, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    const-string v16, "language_code"

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v9, v0, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    const-string v16, "value1"

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v9, v0, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    invoke-virtual {v12, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 817
    .end local v3    # "displayNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "displayNameEntrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v9    # "languageContentValues":Landroid/content/ContentValues;
    .end local v12    # "valuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :catch_0
    move-exception v5

    .line 818
    .local v5, "e":Ljava/lang/IllegalArgumentException;
    :try_start_6
    sget-object v13, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v15, "[Exception] addCardName"

    invoke-static {v13, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    invoke-virtual {v5}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 770
    .end local v5    # "e":Ljava/lang/IllegalArgumentException;
    :cond_a
    monitor-exit v14

    goto/16 :goto_0

    .end local v2    # "contentValues":Landroid/content/ContentValues;
    .end local v11    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v13

    monitor-exit v14
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v13
.end method

.method public addSection(Ljava/lang/String;Ljava/util/Map;)Z
    .locals 1
    .param p1, "section"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 437
    .local p2, "sectionDisplayName":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "headlines"

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->addSection(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public getRegisteredCardNames()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 327
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->isRegistered()Z

    move-result v0

    if-nez v0, :cond_1

    .line 328
    :cond_0
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "CardProvider is not registered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    :goto_0
    return-object v7

    .line 332
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 334
    .local v7, "cardTypeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v10, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v10

    .line 335
    :try_start_0
    const-string v3, "provider=?"

    .line 336
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    aput-object v1, v4, v0

    .line 337
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "card_type_key"

    aput-object v1, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    .local v2, "projection":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 341
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    .line 342
    const/4 v5, 0x0

    .line 341
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 348
    :goto_1
    if-eqz v8, :cond_2

    .line 349
    :goto_2
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 353
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 334
    :cond_2
    monitor-exit v10

    goto :goto_0

    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 343
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 344
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getRegisteredCardNames"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 350
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 351
    .local v6, "cardType":Ljava/lang/String;
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method public getSections()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 648
    const-string v0, "headlines"

    invoke-direct {p0, v0}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->getSections(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 119
    const v0, 0x1000016

    .line 120
    .local v0, "version":I
    return v0
.end method

.method public isCardNameAdded(Ljava/lang/String;)Z
    .locals 9
    .param p1, "cardName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 696
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->isRegistered()Z

    move-result v0

    if-nez v0, :cond_1

    .line 697
    :cond_0
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "CardProvider is not registered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    :goto_0
    return v7

    .line 701
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 702
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "isCardNameAdded: Invalid argument. cardName is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 706
    :cond_2
    const-string v3, "card_type_key=?"

    .line 707
    .local v3, "where":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    aput-object p1, v4, v7

    .line 708
    .local v4, "whereArg":[Ljava/lang/String;
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v7

    .line 710
    .local v2, "projection":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 711
    .local v7, "isAdded":Z
    sget-object v8, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v8

    .line 712
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 713
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    .line 714
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 715
    const/4 v7, 0x1

    .line 717
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 711
    :cond_4
    monitor-exit v8

    goto :goto_0

    .end local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isCardProviderRegistered()Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v8, 0x0

    .line 132
    iget-object v1, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 133
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "context is invalid."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :goto_0
    return v8

    .line 137
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->isRegistered()Z

    move-result v1

    if-eqz v1, :cond_1

    move v8, v0

    .line 138
    goto :goto_0

    .line 140
    :cond_1
    const-string v3, "package_name=?"

    .line 141
    .local v3, "where":Ljava/lang/String;
    new-array v4, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    aput-object v1, v4, v8

    .line 142
    .local v4, "whereArgs":[Ljava/lang/String;
    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "package_version"

    aput-object v0, v2, v8

    .line 143
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 144
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 146
    .local v8, "isRegistered":Z
    sget-object v9, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v9

    .line 148
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Provider;->CONTENT_URI:Landroid/net/Uri;

    .line 149
    const/4 v5, 0x0

    .line 148
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 151
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 152
    const/4 v8, 0x1

    .line 154
    :cond_2
    if-eqz v6, :cond_3

    .line 155
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :cond_3
    :goto_1
    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    invoke-direct {p0, v8}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->setRegistered(Z)V

    goto :goto_0

    .line 157
    :catch_0
    move-exception v7

    .line 158
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    if-eqz v6, :cond_3

    .line 159
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 146
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public isCardSubscribed(Ljava/lang/String;)Z
    .locals 1
    .param p1, "cardName"    # Ljava/lang/String;

    .prologue
    .line 222
    const-string v0, "headlines"

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->isCardSubscribed(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public registerCardProvider(Landroid/content/Intent;)Z
    .locals 12
    .param p1, "configIntent"    # Landroid/content/Intent;

    .prologue
    .line 181
    const/4 v3, 0x1

    .line 183
    .local v3, "success":Z
    sget-object v6, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v6

    .line 184
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 185
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v5, "package_name"

    iget-object v7, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v5, "package_version"

    iget v7, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageVersion:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 187
    const-string v5, "setting_action"

    .line 188
    invoke-static {p1}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v7

    .line 187
    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v7, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Provider;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v7, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    .line 192
    .local v4, "uri":Landroid/net/Uri;
    if-nez v4, :cond_1

    .line 193
    sget-object v5, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v7, "Provider was not registered"

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    const/4 v3, 0x0

    .line 207
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    :try_start_2
    invoke-direct {p0, v3}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->setRegistered(Z)V

    .line 183
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 210
    return v3

    .line 197
    .restart local v4    # "uri":Landroid/net/Uri;
    :cond_1
    :try_start_3
    invoke-virtual {v4}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 198
    .local v2, "id":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    const-wide/16 v10, -0x2

    cmp-long v5, v8, v10

    if-nez v5, :cond_0

    .line 199
    sget-object v5, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v7, "registerCardProvider: provider already exists."

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 202
    .end local v2    # "id":Ljava/lang/String;
    .end local v4    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 203
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const/4 v3, 0x0

    goto :goto_0

    .line 183
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v5

    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v5
.end method

.method public removeCardName(Ljava/lang/String;)V
    .locals 0
    .param p1, "cardName"    # Ljava/lang/String;

    .prologue
    .line 882
    invoke-direct {p0, p1}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->unregisterCardName(Ljava/lang/String;)Z

    .line 883
    return-void
.end method

.method public removeSection(Ljava/lang/String;)V
    .locals 1
    .param p1, "section"    # Ljava/lang/String;

    .prologue
    .line 508
    const-string v0, "headlines"

    invoke-direct {p0, v0, p1}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->removeSection(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    return-void
.end method

.method public setCardDisplayName(Ljava/lang/String;Ljava/util/Map;)Z
    .locals 12
    .param p1, "cardName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "displayNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 378
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 379
    sget-object v6, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v8, "setCardDisplayName: Invalid argument. cardName is empty."

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    .line 416
    :goto_0
    return v6

    .line 382
    :cond_0
    if-eqz p2, :cond_1

    const-string v6, "default"

    invoke-interface {p2, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 383
    :cond_1
    sget-object v6, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v8, "setCardDisplayName: Invalid argument. displayNames is empty or has no data for default"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    .line 384
    goto :goto_0

    .line 388
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 389
    .local v5, "valuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    .line 390
    .local v1, "displayNameEntrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 400
    sget-object v9, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v9

    .line 402
    :try_start_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_4

    .line 403
    iget-object v10, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v11, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;->CONTENT_URI:Landroid/net/Uri;

    .line 404
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Landroid/content/ContentValues;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/content/ContentValues;

    .line 403
    invoke-virtual {v10, v11, v6}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v3

    .line 405
    .local v3, "rowCount":I
    if-ge v3, v8, :cond_4

    .line 406
    sget-object v6, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v10, "setCardDisplayName: Failed to insert display names."

    invoke-static {v6, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407
    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v6, v7

    goto :goto_0

    .line 390
    .end local v3    # "rowCount":I
    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 391
    .local v0, "displayNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 392
    .local v4, "values":Landroid/content/ContentValues;
    const-string v6, "type"

    const-string v10, "card_type"

    invoke-virtual {v4, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    const-string v6, "key1"

    invoke-virtual {v4, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const-string v6, "key2"

    iget-object v10, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    const-string v10, "language_code"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v4, v10, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    const-string v10, "value1"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v4, v10, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 410
    .end local v0    # "displayNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v2

    .line 411
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    sget-object v6, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v7, "[Exception] setCardDisplayName"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 400
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :cond_4
    monitor-exit v9

    move v6, v8

    .line 416
    goto/16 :goto_0

    .line 400
    :catchall_0
    move-exception v6

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6
.end method

.method public setCardIcon(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 10
    .param p1, "cardName"    # Ljava/lang/String;
    .param p2, "icon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 835
    iget-object v7, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContext:Landroid/content/Context;

    if-eqz v7, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->isRegistered()Z

    move-result v7

    if-nez v7, :cond_1

    .line 836
    :cond_0
    sget-object v7, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v8, "CardProvider is not registered"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    :goto_0
    return-void

    .line 840
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 841
    sget-object v7, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v8, "[setCardIcon] Invalid argument. cardNames is empty."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 845
    :cond_2
    if-nez p2, :cond_3

    .line 846
    sget-object v7, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v8, "[setCardIcon] icon is null."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 850
    :cond_3
    const-string v5, "card_type_key=? AND provider=?"

    .line 852
    .local v5, "where":Ljava/lang/String;
    const/4 v7, 0x2

    new-array v6, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    aput-object v8, v6, v7

    .line 854
    .local v6, "whereArg":[Ljava/lang/String;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 856
    .local v0, "contentValues":Landroid/content/ContentValues;
    sget-object v7, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI_IMAGE:Landroid/net/Uri;

    iget-object v8, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mPackageName:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 857
    .local v2, "imageUri":Landroid/net/Uri;
    invoke-static {v2, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 858
    invoke-direct {p0, p2, v2}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->sendImageToMagazine(Landroid/graphics/Bitmap;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 859
    .local v3, "imageUriStr":Ljava/lang/String;
    const-string v7, "icon_image"

    invoke-virtual {v0, v7, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    :try_start_0
    sget-object v8, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 863
    :try_start_1
    iget-object v7, p0, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v9, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v9, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 865
    .local v4, "result":I
    if-gtz v4, :cond_4

    .line 866
    sget-object v7, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v9, "Failed to update card icon."

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    :cond_4
    monitor-exit v8

    goto :goto_0

    .end local v4    # "result":I
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v7
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    .line 869
    :catch_0
    move-exception v1

    .line 870
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v7, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v8, "[Exception] setCardIcon"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public updateSection(Ljava/lang/String;Ljava/util/Map;)Z
    .locals 1
    .param p1, "section"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 558
    .local p2, "displayNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "headlines"

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->updateSection(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Z

    move-result v0

    return v0
.end method
