.class public Lcom/samsung/android/magazine/cardprovider/card/Card;
.super Ljava/lang/Object;
.source "Card.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/magazine/cardprovider/card/CardAction;",
            ">;"
        }
    .end annotation
.end field

.field private mAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCardElements:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/magazine/cardprovider/card/CardElement;",
            ">;"
        }
    .end annotation
.end field

.field private mCardId:Ljava/lang/String;

.field private mCardType:Ljava/lang/String;

.field private mExpiredTime:J

.field private mTemplate:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/samsung/android/magazine/cardprovider/card/Card;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/card/Card;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "cardName"    # Ljava/lang/String;
    .param p2, "cardId"    # Ljava/lang/String;
    .param p3, "template"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v2, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardId:Ljava/lang/String;

    .line 31
    iput-object v2, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardType:Ljava/lang/String;

    .line 32
    iput-object v2, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mTemplate:Ljava/lang/String;

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mExpiredTime:J

    .line 34
    iput-object v2, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mActions:Ljava/util/HashMap;

    .line 35
    iput-object v2, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardElements:Ljava/util/HashMap;

    .line 36
    iput-object v2, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mAttributes:Ljava/util/Map;

    .line 49
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 50
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid argument."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mActions:Ljava/util/HashMap;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardElements:Ljava/util/HashMap;

    .line 55
    iput-object p3, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mTemplate:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mAttributes:Ljava/util/Map;

    .line 57
    iput-object p1, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardType:Ljava/lang/String;

    .line 58
    iput-object p2, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardId:Ljava/lang/String;

    .line 59
    return-void
.end method


# virtual methods
.method public addCardAction(Ljava/lang/String;Lcom/samsung/android/magazine/cardprovider/card/CardAction;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "action"    # Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    .prologue
    .line 125
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mActions:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mAttributes:Ljava/util/Map;

    return-object v0
.end method

.method public getCardAction(Ljava/lang/String;)Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mActions:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    return-object v0
.end method

.method public getCardActionKeys()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mActions:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getCardElement(Ljava/lang/String;)Lcom/samsung/android/magazine/cardprovider/card/CardElement;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardElements:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardprovider/card/CardElement;

    return-object v0
.end method

.method public getCardElements()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/samsung/android/magazine/cardprovider/card/CardElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardElements:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getCardId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardId:Ljava/lang/String;

    return-object v0
.end method

.method public getCardName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardType:Ljava/lang/String;

    return-object v0
.end method

.method public getExpirationTime()J
    .locals 2

    .prologue
    .line 180
    iget-wide v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mExpiredTime:J

    return-wide v0
.end method

.method public getTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public setAttributes(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 233
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 238
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mAttributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public setCardElement(Lcom/samsung/android/magazine/cardprovider/card/CardElement;)V
    .locals 3
    .param p1, "element"    # Lcom/samsung/android/magazine/cardprovider/card/CardElement;

    .prologue
    .line 190
    if-nez p1, :cond_1

    .line 191
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/card/Card;->TAG:Ljava/lang/String;

    const-string v2, "Invalid argument. CardElement is null."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 196
    .local v0, "key":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardElements:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setCardId(Ljava/lang/String;)V
    .locals 0
    .param p1, "cardId"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardId:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setCardName(Ljava/lang/String;)V
    .locals 0
    .param p1, "cardName"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mCardType:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public setExpirationTime(J)V
    .locals 3
    .param p1, "triggerAtMillis"    # J

    .prologue
    .line 163
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 164
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/card/Card;->TAG:Ljava/lang/String;

    const-string v1, "Invalid Argument"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :goto_0
    return-void

    .line 169
    :cond_0
    iput-wide p1, p0, Lcom/samsung/android/magazine/cardprovider/card/Card;->mExpiredTime:J

    goto :goto_0
.end method
