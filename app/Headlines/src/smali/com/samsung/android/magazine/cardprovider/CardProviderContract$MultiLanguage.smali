.class public final Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;
.super Ljava/lang/Object;
.source "CardProviderContract.java"

# interfaces
.implements Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguageColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/cardprovider/CardProviderContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MultiLanguage"
.end annotation


# static fields
.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardprovider.multi_language"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardprovider.multi_language"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CONTENT_URI_SECTION:Landroid/net/Uri;

.field public static final CONTENT_URI_SECTION_UPDATE:Landroid/net/Uri;

.field public static final CONTENT_URI_UPDATE:Landroid/net/Uri;

.field public static final TYPE_CARD_TYPE:Ljava/lang/String; = "card_type"

.field public static final TYPE_SECTION:Ljava/lang/String; = "section"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 706
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 707
    const-string v1, "multi_language"

    .line 706
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;->CONTENT_URI:Landroid/net/Uri;

    .line 714
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "update"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;->CONTENT_URI_UPDATE:Landroid/net/Uri;

    .line 721
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "section"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;->CONTENT_URI_SECTION:Landroid/net/Uri;

    .line 728
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "section_update"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;->CONTENT_URI_SECTION_UPDATE:Landroid/net/Uri;

    .line 760
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 698
    return-void
.end method
