.class public interface abstract Lcom/samsung/android/magazine/service/contract/CardChannelContract$ChannelColumns;
.super Ljava/lang/Object;
.source "CardChannelContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/service/contract/CardChannelContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60c
    name = "ChannelColumns"
.end annotation


# static fields
.field public static final ATTRIBUTES:Ljava/lang/String; = "attributes"

.field public static final ENABLE:Ljava/lang/String; = "enable"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field public static final STYLE:Ljava/lang/String; = "style"

.field public static final _ID:Ljava/lang/String; = "_id"
