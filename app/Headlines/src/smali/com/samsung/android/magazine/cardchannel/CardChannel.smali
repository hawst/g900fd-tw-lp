.class public Lcom/samsung/android/magazine/cardchannel/CardChannel;
.super Ljava/lang/Object;
.source "CardChannel.java"


# instance fields
.field private mAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCardChannelName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mStyle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cardChannelName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardChannel;->mContext:Landroid/content/Context;

    .line 17
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardChannel;->mCardChannelName:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardChannel;->mAttributes:Ljava/util/Map;

    .line 19
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardChannel;->mStyle:Ljava/lang/String;

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardChannel;->mContext:Landroid/content/Context;

    .line 30
    iput-object p2, p0, Lcom/samsung/android/magazine/cardchannel/CardChannel;->mCardChannelName:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardChannel;->mAttributes:Ljava/util/Map;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardChannel;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardChannel;->mCardChannelName:Ljava/lang/String;

    return-object v0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardChannel;->mStyle:Ljava/lang/String;

    return-object v0
.end method

.method public setAttributes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardChannel;->mAttributes:Ljava/util/Map;

    .line 71
    return-void
.end method

.method public setStyle(Ljava/lang/String;)V
    .locals 0
    .param p1, "style"    # Ljava/lang/String;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardChannel;->mStyle:Ljava/lang/String;

    .line 81
    return-void
.end method
