.class public final Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Section;
.super Ljava/lang/Object;
.source "CardProviderContract.java"

# interfaces
.implements Lcom/samsung/android/magazine/cardprovider/CardProviderContract$SectionColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/cardprovider/CardProviderContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Section"
.end annotation


# static fields
.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardprovider.section"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardprovider.section"

.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 866
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 867
    const-string v1, "section"

    .line 866
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Section;->CONTENT_URI:Landroid/net/Uri;

    .line 883
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 858
    return-void
.end method
