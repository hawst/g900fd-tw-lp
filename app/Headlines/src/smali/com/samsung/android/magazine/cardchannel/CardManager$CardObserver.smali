.class Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;
.super Landroid/database/ContentObserver;
.source "CardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/cardchannel/CardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CardObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/magazine/cardchannel/CardManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/magazine/cardchannel/CardManager;)V
    .locals 1

    .prologue
    .line 143
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;->this$0:Lcom/samsung/android/magazine/cardchannel/CardManager;

    .line 144
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 145
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 8
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 150
    invoke-virtual {p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    .line 151
    .local v5, "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x4

    if-ge v6, v7, :cond_1

    .line 185
    :cond_0
    return-void

    .line 155
    :cond_1
    const/4 v6, 0x3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 156
    .local v1, "channel":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;->this$0:Lcom/samsung/android/magazine/cardchannel/CardManager;

    # getter for: Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;
    invoke-static {v6}, Lcom/samsung/android/magazine/cardchannel/CardManager;->access$0(Lcom/samsung/android/magazine/cardchannel/CardManager;)Lcom/samsung/android/magazine/cardchannel/CardChannel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "channel_not_defined"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 157
    :cond_2
    const/4 v6, 0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 158
    .local v4, "operation":Ljava/lang/String;
    const/4 v6, 0x2

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 159
    .local v0, "cardId":I
    const-string v6, "insert"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 160
    iget-object v6, p0, Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;->this$0:Lcom/samsung/android/magazine/cardchannel/CardManager;

    # getter for: Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardUpdateListeners:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/magazine/cardchannel/CardManager;->access$1(Lcom/samsung/android/magazine/cardchannel/CardManager;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 161
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 162
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;

    .line 163
    .local v3, "listener":Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;
    invoke-interface {v3, v0}, Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;->onCardAdded(I)V

    goto :goto_0

    .line 166
    .end local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;>;"
    .end local v3    # "listener":Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;
    :cond_3
    const-string v6, "update"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 167
    iget-object v6, p0, Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;->this$0:Lcom/samsung/android/magazine/cardchannel/CardManager;

    # getter for: Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardUpdateListeners:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/magazine/cardchannel/CardManager;->access$1(Lcom/samsung/android/magazine/cardchannel/CardManager;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 168
    .restart local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;>;"
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 169
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;

    .line 170
    .restart local v3    # "listener":Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;
    invoke-interface {v3, v0}, Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;->onCardUpdated(I)V

    goto :goto_1

    .line 173
    .end local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;>;"
    .end local v3    # "listener":Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;
    :cond_4
    const-string v6, "delete"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 174
    iget-object v6, p0, Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;->this$0:Lcom/samsung/android/magazine/cardchannel/CardManager;

    # getter for: Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardList:Landroid/util/SparseArray;
    invoke-static {v6}, Lcom/samsung/android/magazine/cardchannel/CardManager;->access$2(Lcom/samsung/android/magazine/cardchannel/CardManager;)Landroid/util/SparseArray;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 175
    iget-object v6, p0, Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;->this$0:Lcom/samsung/android/magazine/cardchannel/CardManager;

    # getter for: Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardList:Landroid/util/SparseArray;
    invoke-static {v6}, Lcom/samsung/android/magazine/cardchannel/CardManager;->access$2(Lcom/samsung/android/magazine/cardchannel/CardManager;)Landroid/util/SparseArray;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->remove(I)V

    .line 177
    iget-object v6, p0, Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;->this$0:Lcom/samsung/android/magazine/cardchannel/CardManager;

    # getter for: Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardUpdateListeners:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/magazine/cardchannel/CardManager;->access$1(Lcom/samsung/android/magazine/cardchannel/CardManager;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 178
    .restart local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;>;"
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 179
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;

    .line 180
    .restart local v3    # "listener":Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;
    invoke-interface {v3, v0}, Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;->onCardRemoved(I)V

    goto :goto_2
.end method
