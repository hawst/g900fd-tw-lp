.class public final Lcom/samsung/android/magazine/cardprovider/CardProviderContract;
.super Ljava/lang/Object;
.source "CardProviderContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Card;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$CardColumns;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$CardElement;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$CardElementColumns;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Channel;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ChannelColumns;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguage;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$MultiLanguageColumns;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardTypeColumns;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Provider;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProviderColumns;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Section;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$SectionColumns;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Setting;,
        Lcom/samsung/android/magazine/cardprovider/CardProviderContract$SettingColumns;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.magazine.provider.cardprovider"

.field public static final AUTHORITY_URI:Landroid/net/Uri;

.field public static final TABLE_NAME_CARD:Ljava/lang/String; = "card"

.field public static final TABLE_NAME_CARD_ELEMENT:Ljava/lang/String; = "card_element"

.field public static final TABLE_NAME_CARD_TYPE:Ljava/lang/String; = "card_type"

.field public static final TABLE_NAME_CHANNEL:Ljava/lang/String; = "channel"

.field private static final TABLE_NAME_MULTI_LANGUAGE:Ljava/lang/String; = "multi_language"

.field public static final TABLE_NAME_PROVIDED_CARD_TYPE:Ljava/lang/String; = "provided_card_type"

.field public static final TABLE_NAME_PROVIDER:Ljava/lang/String; = "provider"

.field public static final TABLE_NAME_SECTION:Ljava/lang/String; = "section"

.field public static final TABLE_NAME_SETTING:Ljava/lang/String; = "setting"

.field public static final VERSION:Ljava/lang/String; = "14"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-string v0, "content://com.samsung.android.magazine.provider.cardprovider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 105
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
