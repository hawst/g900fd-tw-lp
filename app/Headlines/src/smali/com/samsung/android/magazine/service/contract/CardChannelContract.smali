.class public final Lcom/samsung/android/magazine/service/contract/CardChannelContract;
.super Ljava/lang/Object;
.source "CardChannelContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$CardColumns;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$CardElement;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$CardElementColumns;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$CardTypeInfoColumns;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$CardTypeMultiLanguageColumns;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$Channel;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$ChannelColumns;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardTypeColumns;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$Provider;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProviderColumns;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$QuerySettingColumns;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$RegisteredProvider;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$RegisteredSection;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$SectionColumns;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$SectionDisplayName;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$SectionMultiLanguageColumns;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$Setting;,
        Lcom/samsung/android/magazine/service/contract/CardChannelContract$SettingColumns;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.magazine.provider.cardchannel"

.field public static final AUTHORITY_URI:Landroid/net/Uri;

.field public static final TABLE_NAME_CARD:Ljava/lang/String; = "card"

.field public static final TABLE_NAME_CARD_ELEMENT:Ljava/lang/String; = "card_element"

.field public static final TABLE_NAME_CHANNEL:Ljava/lang/String; = "channel"

.field public static final TABLE_NAME_PROVIDED_CARD_TYPE:Ljava/lang/String; = "provided_card_type"

.field public static final TABLE_NAME_PROVIDER:Ljava/lang/String; = "provider"

.field private static final TABLE_NAME_SECTION_DISPLAY_NAME:Ljava/lang/String; = "section_display_name"

.field public static final TABLE_NAME_SETTING:Ljava/lang/String; = "setting"

.field public static final VERSION:Ljava/lang/String; = "14"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "content://com.samsung.android.magazine.provider.cardchannel"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 96
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
