.class public Lcom/samsung/android/magazine/service/MagazineService;
.super Landroid/app/IntentService;
.source "MagazineService.java"


# static fields
.field public static final ACTION_CARD_EXPIRED:Ljava/lang/String; = "com.samsung.android.magazine.service.intent.action.CARD_EXPIRED"

.field public static final ACTION_PACKAGE_INSTALLED:Ljava/lang/String; = "com.samsung.android.magazine.service.intent.action.PACKAGE_INSTALLED"

.field public static final ACTION_PACKAGE_REMOVED:Ljava/lang/String; = "com.samsung.android.magazine.service.intent.action.PACKAGE_REMOVED"

.field public static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.magazine.service.intent.extra.PACKAGE_NAME"

.field private static final TAG:Ljava/lang/String; = "MagazineService::MagazineService"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    const-string v0, "MagazineService::MagazineService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public static cancelExpirationTimer(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cardId"    # Ljava/lang/String;

    .prologue
    .line 147
    sget-object v4, Lcom/samsung/android/magazine/service/contract/AdministratorContract$Card;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 148
    .local v1, "cardUri":Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.samsung.android.magazine.service.intent.action.CARD_EXPIRED"

    const-class v5, Lcom/samsung/android/magazine/service/MagazineService;

    invoke-direct {v2, v4, v1, p0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 149
    .local v2, "intent":Landroid/content/Intent;
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/high16 v5, 0x8000000

    invoke-static {p0, v4, v2, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 150
    .local v3, "pendingIntent":Landroid/app/PendingIntent;
    const-string v4, "alarm"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 151
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    invoke-virtual {v0, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 152
    return-void
.end method

.method public static setExpirationTimer(Landroid/content/Context;JJ)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cardId"    # J
    .param p3, "expirationTime"    # J

    .prologue
    .line 137
    sget-object v4, Lcom/samsung/android/magazine/service/contract/AdministratorContract$Card;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 138
    .local v1, "cardUri":Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.samsung.android.magazine.service.intent.action.CARD_EXPIRED"

    const-class v5, Lcom/samsung/android/magazine/service/MagazineService;

    invoke-direct {v2, v4, v1, p0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 139
    .local v2, "intent":Landroid/content/Intent;
    long-to-int v4, p1

    const/high16 v5, 0x8000000

    invoke-static {p0, v4, v2, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 141
    .local v3, "pendingIntent":Landroid/app/PendingIntent;
    const-string v4, "alarm"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 142
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    const/4 v4, 0x1

    invoke-virtual {v0, v4, p3, p4, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 143
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 13
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    const/4 v12, 0x0

    .line 62
    if-nez p1, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 69
    const-string v10, "com.samsung.android.magazine.service.intent.action.PACKAGE_INSTALLED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 70
    const-string v10, "MagazineService::MagazineService"

    const-string v11, "[onHandleIntent] ACTION_PACKAGE_INSTALLED"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    const-string v10, "com.samsung.android.magazine.service.intent.extra.PACKAGE_NAME"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 72
    .local v6, "packageName":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 73
    const-string v10, "MagazineService::MagazineService"

    const-string v11, "[onHandleIntent] packageName is empty."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 77
    :cond_2
    invoke-static {p0, v6}, Lcom/samsung/android/magazine/service/Notifier;->sendInitializeCardProviderBroadcast(Landroid/content/Context;Ljava/lang/String;)V

    .line 78
    const-string v10, "MagazineService::MagazineService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[onHandleIntent] Send broadcast to ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 93
    .end local v6    # "packageName":Ljava/lang/String;
    :cond_3
    const-string v10, "com.samsung.android.magazine.service.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 94
    const-string v10, "MagazineService::MagazineService"

    const-string v11, "[onHandleIntent] ACTION_PACKAGE_REMOVED"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const-string v10, "com.samsung.android.magazine.service.intent.extra.PACKAGE_NAME"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 96
    .restart local v6    # "packageName":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 97
    const-string v10, "MagazineService::MagazineService"

    const-string v11, "[onHandleIntent] packageName is empty."

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 102
    :cond_4
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/magazine/service/MagazineService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v6, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 104
    :catch_0
    move-exception v5

    .line 106
    .local v5, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "package_name=?"

    .line 107
    .local v8, "providerWhere":Ljava/lang/String;
    const/4 v10, 0x1

    new-array v9, v10, [Ljava/lang/String;

    aput-object v6, v9, v12

    .line 108
    .local v9, "whereArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/magazine/service/MagazineService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    sget-object v11, Lcom/samsung/android/magazine/service/contract/AdministratorContract$Provider;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v10, v11, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 109
    .local v7, "providerCount":I
    if-lez v7, :cond_0

    .line 110
    const-string v10, "MagazineService::MagazineService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[onHandleIntent] Provider("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ") is removed."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 122
    .end local v5    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "providerCount":I
    .end local v8    # "providerWhere":Ljava/lang/String;
    .end local v9    # "whereArgs":[Ljava/lang/String;
    :cond_5
    const-string v10, "com.samsung.android.magazine.service.intent.action.CARD_EXPIRED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 123
    const-string v10, "MagazineService::MagazineService"

    const-string v11, "[onHandleIntent] ACTION_CARD_EXPIRED"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 126
    .local v2, "currentTime":J
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "expiration_time>0 AND expiration_time<="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "cardWhere":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/magazine/service/MagazineService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    sget-object v11, Lcom/samsung/android/magazine/service/contract/AdministratorContract$Card;->CONTENT_URI:Landroid/net/Uri;

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v1, v12}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 129
    .local v4, "deletedCount":I
    if-lez v4, :cond_0

    .line 130
    const-string v10, "MagazineService::MagazineService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[onHandleIntent] "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " cards are expired. ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
