.class public Lcom/samsung/android/magazine/cardprovider/card/CardImage;
.super Lcom/samsung/android/magazine/cardprovider/card/CardElement;
.source "CardImage.java"


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mImageFilePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/magazine/cardprovider/card/CardElement;-><init>(Ljava/lang/String;)V

    .line 21
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 22
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardImage;->mImageFilePath:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/samsung/android/magazine/cardprovider/card/CardImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "imagePath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1}, Lcom/samsung/android/magazine/cardprovider/card/CardElement;-><init>(Ljava/lang/String;)V

    .line 21
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardImage;->mBitmap:Landroid/graphics/Bitmap;

    .line 22
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardImage;->mImageFilePath:Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lcom/samsung/android/magazine/cardprovider/card/CardImage;->mImageFilePath:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public getImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardImage;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/card/CardImage;->mImageFilePath:Ljava/lang/String;

    return-object v0
.end method
