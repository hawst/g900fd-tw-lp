.class public Lcom/samsung/android/magazine/service/contract/CardNotification;
.super Ljava/lang/Object;
.source "CardNotification.java"


# static fields
.field public static final ACTION_CARD_ADDED:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CARD_ADDED"

.field public static final ACTION_CARD_REMOVED:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CARD_REMOVED"

.field public static final ACTION_CARD_UPDATED:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CARD_UPDATED"

.field public static final EXTRA_CARD_PRIMARY_KEY:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.CARD_PRIMARY_KEY"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
