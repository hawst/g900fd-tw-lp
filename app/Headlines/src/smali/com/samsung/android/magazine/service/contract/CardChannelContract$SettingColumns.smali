.class public interface abstract Lcom/samsung/android/magazine/service/contract/CardChannelContract$SettingColumns;
.super Ljava/lang/Object;
.source "CardChannelContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/service/contract/CardChannelContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60c
    name = "SettingColumns"
.end annotation


# static fields
.field public static final CARD_TYPE_KEY:Ljava/lang/String; = "card_type_key"

.field public static final CHANNEL_KEY:Ljava/lang/String; = "channel_key"

.field public static final ENABLE:Ljava/lang/String; = "enable"

.field public static final PROVIDER:Ljava/lang/String; = "provider"

.field public static final SECTION_KEY:Ljava/lang/String; = "section_key"

.field public static final _ID:Ljava/lang/String; = "_id"
