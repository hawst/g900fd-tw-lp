.class public Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;
.super Landroid/content/ContentProvider;
.source "CardChannelContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;
    }
.end annotation


# static fields
.field private static final CARD_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final CARD_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

.field private static final CODE_CARD_ELEMENT_ID:I = 0xc9

.field private static final CODE_CARD_ELEMENT_LIST:I = 0xc8

.field private static final CODE_CARD_ID:I = 0x65

.field private static final CODE_CARD_IMAGE:I = 0x6e

.field private static final CODE_CARD_LIST:I = 0x64

.field private static final CODE_CHANNEL_ID:I = 0x12d

.field private static final CODE_CHANNEL_LIST:I = 0x12c

.field private static final CODE_MULTI_LANGUAGE_SECTION:I = 0x190

.field private static final CODE_PROVIDED_CARD_TYPE_ID:I = 0x1f5

.field private static final CODE_PROVIDED_CARD_TYPE_IMAGE:I = 0x1fe

.field private static final CODE_PROVIDED_CARD_TYPE_LIST:I = 0x1f4

.field private static final CODE_PROVIDER_ID:I = 0x259

.field private static final CODE_PROVIDER_LIST:I = 0x258

.field private static final CODE_REGISTERED_PROVIDER:I = 0x320

.field private static final CODE_REGISTERED_SECTION:I = 0x321

.field private static final CODE_SETTING_ID:I = 0x2bd

.field private static final CODE_SETTING_LIST:I = 0x2bc

.field private static final DEFAULT_LANGUAGE_CODE:Ljava/lang/String; = "default"

.field private static final DEFAULT_REGISTERED_PROVIDER_PROJECTION:[Ljava/lang/String;

.field private static final DEFAULT_REGISTERED_SECTION_PROJECTION:[Ljava/lang/String;

.field private static final PROVIDED_CARD_TYPE_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PROVIDED_CARD_TYPE_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

.field private static final REGISTERED_PROVIDER_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final REGISTERED_SECTION_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SETTING_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SETTING_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "MagazineService::CardChannelContentProvider"

.field private static final mUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 81
    const/16 v5, 0x9

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "_id"

    aput-object v6, v5, v8

    const-string v6, "card_key"

    aput-object v6, v5, v9

    const-string v6, "card_provider"

    aput-object v6, v5, v10

    const-string v6, "channel_key"

    aput-object v6, v5, v11

    const-string v6, "card_type"

    aput-object v6, v5, v12

    const/4 v6, 0x5

    const-string v7, "template_type"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "attributes"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    const-string v7, "time_stamp"

    aput-object v7, v5, v6

    const/16 v6, 0x8

    const-string v7, "section_key"

    aput-object v7, v5, v6

    sput-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->CARD_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

    .line 93
    const/16 v5, 0xb

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "_id"

    aput-object v6, v5, v8

    const-string v6, "provider"

    aput-object v6, v5, v9

    const-string v6, "card_type_key"

    aput-object v6, v5, v10

    const-string v6, "channel_key"

    aput-object v6, v5, v11

    const-string v6, "section_key"

    aput-object v6, v5, v12

    const/4 v6, 0x5

    const-string v7, "icon_image"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "default_subscription"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    const-string v7, "value1"

    aput-object v7, v5, v6

    const/16 v6, 0x8

    const-string v7, "value2"

    aput-object v7, v5, v6

    const/16 v6, 0x9

    const-string v7, "value3"

    aput-object v7, v5, v6

    const/16 v6, 0xa

    const-string v7, "language_code"

    aput-object v7, v5, v6

    sput-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->PROVIDED_CARD_TYPE_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

    .line 107
    const/16 v5, 0xb

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "_id"

    aput-object v6, v5, v8

    const-string v6, "channel_key"

    aput-object v6, v5, v9

    const-string v6, "section_key"

    aput-object v6, v5, v10

    const-string v6, "provider"

    aput-object v6, v5, v11

    const-string v6, "card_type_key"

    aput-object v6, v5, v12

    const/4 v6, 0x5

    const-string v7, "enable"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "value1"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    const-string v7, "value2"

    aput-object v7, v5, v6

    const/16 v6, 0x8

    const-string v7, "value3"

    aput-object v7, v5, v6

    const/16 v6, 0x9

    const-string v7, "language_code"

    aput-object v7, v5, v6

    const/16 v6, 0xa

    const-string v7, "icon_image"

    aput-object v7, v5, v6

    sput-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->SETTING_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

    .line 121
    new-array v5, v12, [Ljava/lang/String;

    const-string v6, "_id"

    aput-object v6, v5, v8

    const-string v6, "package_name"

    aput-object v6, v5, v9

    const-string v6, "package_version"

    aput-object v6, v5, v10

    const-string v6, "setting_action"

    aput-object v6, v5, v11

    sput-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->DEFAULT_REGISTERED_PROVIDER_PROJECTION:[Ljava/lang/String;

    .line 128
    new-array v5, v12, [Ljava/lang/String;

    const-string v6, "_id"

    aput-object v6, v5, v8

    const-string v6, "key"

    aput-object v6, v5, v9

    const-string v6, "channel_key"

    aput-object v6, v5, v10

    const-string v6, "creator"

    aput-object v6, v5, v11

    sput-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->DEFAULT_REGISTERED_SECTION_PROJECTION:[Ljava/lang/String;

    .line 142
    new-instance v5, Landroid/content/UriMatcher;

    const/4 v6, -0x1

    invoke-direct {v5, v6}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 143
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "card"

    const/16 v8, 0x64

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 144
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "card/#"

    const/16 v8, 0x65

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 145
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "card/#/image/*/*"

    const/16 v8, 0x6e

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 146
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "card_element"

    const/16 v8, 0xc8

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 147
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "card_element/#"

    const/16 v8, 0xc9

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 148
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "channel"

    const/16 v8, 0x12c

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 149
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "channel/#"

    const/16 v8, 0x12d

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 150
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "provided_card_type"

    const/16 v8, 0x1f4

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 151
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "provided_card_type/#"

    const/16 v8, 0x1f5

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 152
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "provided_card_type/image/*/*/*"

    const/16 v8, 0x1fe

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 153
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "provider"

    const/16 v8, 0x258

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 154
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "provider/#"

    const/16 v8, 0x259

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 155
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "registered_provider"

    const/16 v8, 0x320

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 156
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "registered_section"

    const/16 v8, 0x321

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 157
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "section_display_name"

    const/16 v8, 0x190

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 158
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "setting"

    const/16 v8, 0x2bc

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 159
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v6, "com.samsung.android.magazine.provider.cardchannel"

    const-string v7, "setting/#"

    const/16 v8, 0x2bd

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 164
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sput-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->CARD_PROJECTION_MAP:Ljava/util/HashMap;

    .line 166
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->CARD_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v1, v0, v3

    .line 167
    .local v1, "column":Ljava/lang/String;
    const-string v5, "channel_key"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 168
    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/SettingTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 173
    .local v2, "columnWithTable":Ljava/lang/String;
    :goto_1
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->CARD_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v5, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 169
    .end local v2    # "columnWithTable":Ljava/lang/String;
    :cond_0
    const-string v5, "section_key"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 170
    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/SettingTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "columnWithTable":Ljava/lang/String;
    goto :goto_1

    .line 172
    .end local v2    # "columnWithTable":Ljava/lang/String;
    :cond_1
    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/CardTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "columnWithTable":Ljava/lang/String;
    goto :goto_1

    .line 176
    .end local v1    # "column":Ljava/lang/String;
    .end local v2    # "columnWithTable":Ljava/lang/String;
    :cond_2
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sput-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->PROVIDED_CARD_TYPE_PROJECTION_MAP:Ljava/util/HashMap;

    .line 177
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->PROVIDED_CARD_TYPE_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_5

    aget-object v1, v0, v3

    .line 178
    .restart local v1    # "column":Ljava/lang/String;
    const-string v5, "value1"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "value2"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "value3"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "language_code"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 182
    :cond_3
    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/MultiLanguageTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 187
    .restart local v2    # "columnWithTable":Ljava/lang/String;
    :goto_3
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->PROVIDED_CARD_TYPE_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v5, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 185
    .end local v2    # "columnWithTable":Ljava/lang/String;
    :cond_4
    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/ProvidedCardTypeTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "columnWithTable":Ljava/lang/String;
    goto :goto_3

    .line 191
    .end local v1    # "column":Ljava/lang/String;
    .end local v2    # "columnWithTable":Ljava/lang/String;
    :cond_5
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sput-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->SETTING_PROJECTION_MAP:Ljava/util/HashMap;

    .line 192
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->SETTING_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v4, :cond_9

    aget-object v1, v0, v3

    .line 193
    .restart local v1    # "column":Ljava/lang/String;
    const-string v5, "value1"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "value2"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "value3"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "language_code"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 197
    :cond_6
    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/MultiLanguageTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 205
    .restart local v2    # "columnWithTable":Ljava/lang/String;
    :goto_5
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->SETTING_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v5, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 199
    .end local v2    # "columnWithTable":Ljava/lang/String;
    :cond_7
    const-string v5, "icon_image"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 200
    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/ProvidedCardTypeTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "columnWithTable":Ljava/lang/String;
    goto :goto_5

    .line 203
    .end local v2    # "columnWithTable":Ljava/lang/String;
    :cond_8
    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/SettingTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "columnWithTable":Ljava/lang/String;
    goto :goto_5

    .line 208
    .end local v1    # "column":Ljava/lang/String;
    .end local v2    # "columnWithTable":Ljava/lang/String;
    :cond_9
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sput-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->REGISTERED_PROVIDER_PROJECTION_MAP:Ljava/util/HashMap;

    .line 209
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->DEFAULT_REGISTERED_PROVIDER_PROJECTION:[Ljava/lang/String;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_6
    if-ge v3, v4, :cond_a

    aget-object v1, v0, v3

    .line 210
    .restart local v1    # "column":Ljava/lang/String;
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->REGISTERED_PROVIDER_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/ProviderTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 213
    .end local v1    # "column":Ljava/lang/String;
    :cond_a
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sput-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->REGISTERED_SECTION_PROJECTION_MAP:Ljava/util/HashMap;

    .line 214
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->DEFAULT_REGISTERED_SECTION_PROJECTION:[Ljava/lang/String;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_7
    if-ge v3, v4, :cond_b

    aget-object v1, v0, v3

    .line 215
    .restart local v1    # "column":Ljava/lang/String;
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->REGISTERED_SECTION_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/SectionTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 217
    .end local v1    # "column":Ljava/lang/String;
    :cond_b
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 219
    iput-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    .line 220
    iput-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    .line 222
    return-void
.end method

.method private containIconImageColumn([Ljava/lang/String;)Z
    .locals 5
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    .line 935
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 936
    .local v1, "column":Ljava/lang/String;
    const-string v4, "icon_image"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 937
    const/4 v4, 0x1

    .line 941
    .end local v1    # "column":Ljava/lang/String;
    :goto_1
    return v4

    .line 935
    .restart local v1    # "column":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 941
    .end local v1    # "column":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private containIdColumn([Ljava/lang/String;)Z
    .locals 5
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    .line 925
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 926
    .local v1, "column":Ljava/lang/String;
    const-string v4, "_id"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 927
    const/4 v4, 0x1

    .line 931
    .end local v1    # "column":Ljava/lang/String;
    :goto_1
    return v4

    .line 925
    .restart local v1    # "column":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 931
    .end local v1    # "column":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private containMultiLanguageColumn([Ljava/lang/String;)Z
    .locals 5
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    .line 945
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 946
    .local v1, "column":Ljava/lang/String;
    const-string v4, "value1"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "value2"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "value3"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 949
    :cond_0
    const/4 v4, 0x1

    .line 953
    .end local v1    # "column":Ljava/lang/String;
    :goto_1
    return v4

    .line 945
    .restart local v1    # "column":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 953
    .end local v1    # "column":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private deleteCards(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 24
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 413
    const/4 v3, 0x4

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "card_key"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "card_provider"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string v4, "card_channel"

    aput-object v4, v5, v3

    .line 420
    .local v5, "projection":[Ljava/lang/String;
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 422
    .local v19, "dataHolderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;>;"
    const-string v4, "card"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 423
    .local v17, "cursor":Landroid/database/Cursor;
    if-eqz v17, :cond_2

    .line 424
    :goto_0
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 425
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 426
    .local v22, "id":I
    const/4 v3, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 427
    .local v14, "channel":Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 428
    const-string v14, "channel_not_defined"

    .line 430
    :cond_0
    new-instance v18, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-direct {v0, v3, v4, v6, v14}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    .local v18, "dataHolder":Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 433
    .end local v14    # "channel":Ljava/lang/String;
    .end local v18    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;
    .end local v22    # "id":I
    :cond_1
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 436
    :cond_2
    const-string v3, "card"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v23

    .line 437
    .local v23, "rowCount":I
    if-lez v23, :cond_6

    .line 438
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;

    .line 440
    .restart local v18    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData1:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/samsung/android/magazine/service/MagazineService;->cancelExpirationTimer(Landroid/content/Context;Ljava/lang/String;)V

    .line 442
    const/16 v16, 0x0

    .line 443
    .local v16, "channelPackageName":Ljava/lang/String;
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData4:Ljava/lang/String;

    const-string v4, "channel_not_defined"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 444
    const/4 v3, 0x1

    new-array v8, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "package_name"

    aput-object v4, v8, v3

    .line 445
    .local v8, "projection2":[Ljava/lang/String;
    const-string v9, "key=?"

    .line 446
    .local v9, "selection2":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData4:Ljava/lang/String;

    aput-object v4, v10, v3

    .line 447
    .local v10, "selectionArgs2":[Ljava/lang/String;
    const-string v7, "channel"

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v6, p1

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 448
    .local v15, "channelCursor":Landroid/database/Cursor;
    if-eqz v15, :cond_4

    .line 449
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 450
    const/4 v3, 0x0

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 452
    :cond_3
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 458
    .end local v8    # "projection2":[Ljava/lang/String;
    .end local v9    # "selection2":Ljava/lang/String;
    .end local v10    # "selectionArgs2":[Ljava/lang/String;
    .end local v15    # "channelCursor":Landroid/database/Cursor;
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI_DELETE_NOTIFICATION:Landroid/net/Uri;

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData1:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData4:Ljava/lang/String;

    invoke-static {v3, v4, v6, v7}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData1:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v16

    invoke-static {v3, v0, v4}, Lcom/samsung/android/magazine/service/Notifier;->sendCardDeletedBroadcast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 465
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData3:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData2:Ljava/lang/String;

    invoke-static {v3, v4, v6}, Lcom/samsung/android/magazine/service/Notifier;->sendCardDismissedBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 470
    .end local v16    # "channelPackageName":Ljava/lang/String;
    .end local v18    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;
    :cond_5
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;

    .line 471
    .restart local v18    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "card_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 472
    .local v20, "elementSelection":Ljava/lang/String;
    const-string v3, "card_element"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 473
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData1:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->deleteImageFiles(Ljava/lang/String;)V

    goto :goto_2

    .line 477
    .end local v18    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;
    .end local v20    # "elementSelection":Ljava/lang/String;
    .end local v21    # "i$":Ljava/util/Iterator;
    :cond_6
    return v23
.end method

.method private deleteImageFiles(Ljava/lang/String;)V
    .locals 7
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 481
    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    const-string v5, "image"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v4

    invoke-direct {v0, v4, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 482
    .local v0, "directory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 483
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 484
    .local v2, "fileList":[Ljava/io/File;
    array-length v1, v2

    .line 485
    .local v1, "fileCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 486
    aget-object v4, v2, v3

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 485
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 488
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 490
    .end local v1    # "fileCount":I
    .end local v2    # "fileList":[Ljava/io/File;
    .end local v3    # "i":I
    :cond_1
    return-void
.end method

.method private deleteSettings(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 16
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 493
    const/4 v3, 0x3

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "channel_key"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "provider"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "card_type_key"

    aput-object v4, v5, v3

    .line 499
    .local v5, "projection":[Ljava/lang/String;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 500
    .local v13, "dataHolderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;>;"
    const-string v4, "setting"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 501
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_1

    .line 502
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 504
    new-instance v12, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;

    const/4 v3, 0x0

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x2

    invoke-interface {v11, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v12, v3, v4, v6, v7}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    .local v12, "dataHolder":Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 507
    .end local v12    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;
    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 510
    :cond_1
    const-string v3, "setting"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 511
    .local v15, "rowCount":I
    if-lez v15, :cond_2

    .line 513
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;

    .line 514
    .restart local v12    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    iget-object v4, v12, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData2:Ljava/lang/String;

    iget-object v6, v12, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData1:Ljava/lang/String;

    iget-object v7, v12, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;->mData3:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v3, v4, v6, v7, v8}, Lcom/samsung/android/magazine/service/Notifier;->sendCardTypeSubscriptionChangedBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 518
    .end local v12    # "dataHolder":Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider$DataHolder;
    .end local v14    # "i$":Ljava/util/Iterator;
    :cond_2
    return v15
.end method

.method private insertSetting(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 26
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 641
    const-string v2, "value1"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 642
    const-string v2, "value2"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 643
    const-string v2, "value3"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 647
    const-string v2, "provider"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 648
    .local v19, "providerName":Ljava/lang/String;
    const-string v2, "channel_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 649
    .local v16, "channelKey":Ljava/lang/String;
    const-string v2, "section_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 650
    .local v24, "sectionKey":Ljava/lang/String;
    const-string v2, "card_type_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 652
    .local v15, "cardTypeKey":Ljava/lang/String;
    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v19, v6, v2

    const/4 v2, 0x1

    aput-object v15, v6, v2

    const/4 v2, 0x2

    aput-object v16, v6, v2

    .line 654
    .local v6, "selectionArgs1":[Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v2, "provider"

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 655
    .local v22, "sb1":Ljava/lang/StringBuilder;
    const-string v2, "=?"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 656
    const-string v2, " AND "

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 657
    const-string v2, "card_type_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 658
    const-string v2, "=? AND ("

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 659
    const-string v2, "channel_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 660
    const-string v2, " IS NULL OR "

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 661
    const-string v2, "channel_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 662
    const-string v2, "=?)"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 663
    const-string v3, "provided_card_type"

    const/4 v4, 0x0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 664
    .local v25, "typeCursor":Landroid/database/Cursor;
    if-eqz v25, :cond_0

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_3

    .line 665
    :cond_0
    if-eqz v25, :cond_1

    .line 666
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 668
    :cond_1
    const-string v2, "MagazineService::CardChannelContentProvider"

    const-string v3, "Can not insert a row to Setting table: The card type is not provied."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    const-wide/16 v20, -0x1

    .line 709
    :cond_2
    :goto_0
    return-wide v20

    .line 671
    :cond_3
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 675
    new-instance v23, Ljava/lang/StringBuilder;

    const-string v2, "channel_key"

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 676
    .local v23, "sb2":Ljava/lang/StringBuilder;
    const-string v2, "=? AND "

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 677
    const-string v2, "provider"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 678
    const-string v2, "=? AND "

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 679
    const-string v2, "card_type_key"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 680
    const-string v2, "=?"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 681
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 682
    const/4 v2, 0x3

    new-array v11, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v16, v11, v2

    const/4 v2, 0x1

    aput-object v19, v11, v2

    const/4 v2, 0x2

    aput-object v15, v11, v2

    .line 691
    .local v11, "selectionArgs2":[Ljava/lang/String;
    :goto_1
    const-string v8, "setting"

    const/4 v9, 0x0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 692
    .local v17, "cursor":Landroid/database/Cursor;
    if-eqz v17, :cond_6

    .line 693
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_5

    .line 694
    const-string v2, "MagazineService::CardChannelContentProvider"

    const-string v3, "Can not insert a row to setting table: Already existed row."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 696
    const-wide/16 v20, -0x1

    goto :goto_0

    .line 685
    .end local v11    # "selectionArgs2":[Ljava/lang/String;
    .end local v17    # "cursor":Landroid/database/Cursor;
    :cond_4
    const-string v2, " AND "

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 686
    const-string v2, "section_key"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 687
    const-string v2, "=?"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 688
    const/4 v2, 0x4

    new-array v11, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v16, v11, v2

    const/4 v2, 0x1

    aput-object v19, v11, v2

    const/4 v2, 0x2

    aput-object v15, v11, v2

    const/4 v2, 0x3

    aput-object v24, v11, v2

    .restart local v11    # "selectionArgs2":[Ljava/lang/String;
    goto :goto_1

    .line 698
    .restart local v17    # "cursor":Landroid/database/Cursor;
    :cond_5
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 701
    :cond_6
    const-string v2, "setting"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v20

    .line 702
    .local v20, "rowId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v20, v2

    if-lez v2, :cond_2

    .line 703
    const-string v2, "enable"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v18

    .line 704
    .local v18, "enable":Ljava/lang/Integer;
    if-eqz v18, :cond_2

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 705
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-static {v2, v0, v1, v15, v3}, Lcom/samsung/android/magazine/service/Notifier;->sendCardTypeSubscriptionChangedBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method private queryCard(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 965
    if-nez p2, :cond_0

    .line 966
    sget-object p2, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->CARD_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

    .line 968
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 969
    .local v9, "sb":Ljava/lang/StringBuilder;
    const-string v1, "card"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 970
    const-string v1, " INNER JOIN "

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 971
    const-string v1, "setting"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 972
    const-string v1, " ON (setting.enable=1 AND card.card_provider=setting.provider AND card.card_type=setting.card_type_key)"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 973
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 975
    .local v8, "inTables":Ljava/lang/String;
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 976
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-virtual {v0, v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 977
    sget-object v1, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->CARD_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 978
    const-string v1, "card.card_channel IS NULL OR card.card_channel=setting.channel_key"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 980
    const-string v1, "_id"

    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/CardTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private queryMultiLanguageSection(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 21
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 985
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v17

    .line 986
    .local v17, "languageCode":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 987
    const-string v17, "default"

    .line 990
    :cond_0
    const/4 v15, 0x0

    .line 991
    .local v15, "appendFlag":Z
    const/4 v6, 0x0

    .line 992
    .local v6, "addedSelectionArgs":[Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 993
    .local v18, "sb":Ljava/lang/StringBuilder;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 994
    const-string v2, "("

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 995
    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 996
    const-string v2, ") AND "

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 998
    if-eqz p4, :cond_1

    .line 999
    const/4 v15, 0x1

    .line 1002
    :cond_1
    const-string v2, "type"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1003
    const-string v2, "=? AND "

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1004
    const-string v2, "language_code"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1005
    const-string v2, "=?"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1006
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1008
    .local v5, "addedSelection":Ljava/lang/String;
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/4 v2, 0x0

    const-string v3, "section"

    aput-object v3, v19, v2

    const/4 v2, 0x1

    aput-object v17, v19, v2

    .line 1009
    .local v19, "selectionArgs2":[Ljava/lang/String;
    if-eqz v15, :cond_2

    .line 1010
    move-object/from16 v0, p4

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->appendSelectionArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1015
    :goto_0
    const-string v3, "multi_language"

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p1

    move-object/from16 v4, p2

    move-object/from16 v9, p5

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1016
    .local v16, "cursor":Landroid/database/Cursor;
    if-eqz v16, :cond_4

    .line 1017
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 1030
    .end local v16    # "cursor":Landroid/database/Cursor;
    :goto_1
    return-object v16

    .line 1013
    :cond_2
    move-object/from16 v6, v19

    goto :goto_0

    .line 1020
    .restart local v16    # "cursor":Landroid/database/Cursor;
    :cond_3
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 1023
    :cond_4
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/4 v2, 0x0

    const-string v3, "section"

    aput-object v3, v20, v2

    const/4 v2, 0x1

    const-string v3, "default"

    aput-object v3, v20, v2

    .line 1024
    .local v20, "selectionArgs3":[Ljava/lang/String;
    if-eqz v15, :cond_5

    .line 1025
    move-object/from16 v0, p4

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->appendSelectionArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1030
    :goto_2
    const-string v8, "multi_language"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v7, p1

    move-object/from16 v9, p2

    move-object v11, v6

    move-object/from16 v14, p5

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    goto :goto_1

    .line 1028
    :cond_5
    move-object/from16 v6, v20

    goto :goto_2
.end method

.method private queryProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 32
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1035
    const/4 v5, 0x0

    .line 1036
    .local v5, "joinProjection":[Ljava/lang/String;
    const/16 v18, 0x1

    .line 1037
    .local v18, "isMultiLanguage":Z
    if-nez p2, :cond_3

    .line 1038
    sget-object p2, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->PROVIDED_CARD_TYPE_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

    .line 1039
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->PROVIDED_CARD_TYPE_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

    .line 1045
    :goto_0
    if-eqz v18, :cond_e

    .line 1046
    const/16 v22, 0x0

    .line 1047
    .local v22, "matrixCursor":Landroid/database/MatrixCursor;
    if-nez v5, :cond_0

    .line 1048
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->containIdColumn([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1049
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "language_code"

    aput-object v7, v4, v6

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/database/DatabaseUtils;->appendSelectionArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1064
    :cond_0
    :goto_1
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    .line 1065
    .local v27, "sb":Ljava/lang/StringBuilder;
    const-string v4, "provided_card_type"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1066
    const-string v4, " LEFT OUTER JOIN "

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1067
    const-string v4, "multi_language"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1068
    const-string v4, " ON (multi_language.type=\'card_type\' AND provided_card_type.card_type_key=multi_language.key1 AND provided_card_type.provider=multi_language.key2)"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1083
    new-instance v31, Ljava/lang/StringBuilder;

    const-string v4, "multi_language.language_code IS NULL OR (multi_language.language_code=\'"

    move-object/from16 v0, v31

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1084
    .local v31, "whereSb":Ljava/lang/StringBuilder;
    const-string v4, "default"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1085
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1086
    .local v20, "languageCode":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1088
    const-string v4, "\' OR multi_language.language_code=\'"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1089
    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1090
    const-string v4, "\')"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1098
    :goto_2
    new-instance v3, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v3}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1099
    .local v3, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1100
    sget-object v4, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->PROVIDED_CARD_TYPE_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 1101
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1103
    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v4, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v10, p5

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v29

    .line 1104
    .local v29, "tempCursor":Landroid/database/Cursor;
    if-eqz v29, :cond_d

    .line 1105
    new-instance v23, Landroid/util/SparseArray;

    invoke-direct/range {v23 .. v23}, Landroid/util/SparseArray;-><init>()V

    .line 1106
    .local v23, "providedCardTypeArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    :cond_1
    :goto_3
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1107
    const-string v4, "_id"

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    .line 1108
    .local v26, "rowId":I
    const-string v4, "language_code"

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 1109
    .local v19, "language":Ljava/lang/String;
    if-eqz v19, :cond_2

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1110
    :cond_2
    move-object/from16 v0, v23

    move/from16 v1, v26

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_3

    .line 1042
    .end local v3    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v19    # "language":Ljava/lang/String;
    .end local v20    # "languageCode":Ljava/lang/String;
    .end local v22    # "matrixCursor":Landroid/database/MatrixCursor;
    .end local v23    # "providedCardTypeArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .end local v26    # "rowId":I
    .end local v27    # "sb":Ljava/lang/StringBuilder;
    .end local v29    # "tempCursor":Landroid/database/Cursor;
    .end local v31    # "whereSb":Ljava/lang/StringBuilder;
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->containMultiLanguageColumn([Ljava/lang/String;)Z

    move-result v18

    goto/16 :goto_0

    .line 1053
    .restart local v22    # "matrixCursor":Landroid/database/MatrixCursor;
    :cond_4
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "_id"

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const-string v7, "language_code"

    aput-object v7, v4, v6

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/database/DatabaseUtils;->appendSelectionArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 1094
    .restart local v20    # "languageCode":Ljava/lang/String;
    .restart local v27    # "sb":Ljava/lang/StringBuilder;
    .restart local v31    # "whereSb":Ljava/lang/StringBuilder;
    :cond_5
    const-string v4, "\')"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1095
    const-string v20, "default"

    goto/16 :goto_2

    .line 1113
    .restart local v3    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v19    # "language":Ljava/lang/String;
    .restart local v23    # "providedCardTypeArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .restart local v26    # "rowId":I
    .restart local v29    # "tempCursor":Landroid/database/Cursor;
    :cond_6
    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v4

    if-gez v4, :cond_1

    .line 1114
    move-object/from16 v0, v23

    move/from16 v1, v26

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_3

    .line 1118
    .end local v19    # "language":Ljava/lang/String;
    .end local v26    # "rowId":I
    :cond_7
    new-instance v22, Landroid/database/MatrixCursor;

    .end local v22    # "matrixCursor":Landroid/database/MatrixCursor;
    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1119
    .restart local v22    # "matrixCursor":Landroid/database/MatrixCursor;
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1121
    :cond_8
    const-string v4, "_id"

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    .line 1122
    .restart local v26    # "rowId":I
    const-string v4, "language_code"

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 1123
    .restart local v19    # "language":Ljava/lang/String;
    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    .line 1124
    .local v28, "selectedLanuguage":Ljava/lang/String;
    if-nez v19, :cond_9

    if-eqz v28, :cond_a

    :cond_9
    if-eqz v19, :cond_b

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1126
    :cond_a
    invoke-virtual/range {v22 .. v22}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v25

    .line 1127
    .local v25, "rowBuilder":Landroid/database/MatrixCursor$RowBuilder;
    move-object/from16 v14, p2

    .local v14, "arr$":[Ljava/lang/String;
    array-length v0, v14

    move/from16 v21, v0

    .local v21, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_4
    move/from16 v0, v16

    move/from16 v1, v21

    if-ge v0, v1, :cond_b

    aget-object v24, v14, v16

    .line 1128
    .local v24, "resultColumn":Ljava/lang/String;
    move-object/from16 v0, v29

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 1129
    .local v17, "index":I
    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getType(I)I

    move-result v30

    .line 1130
    .local v30, "type":I
    const/4 v15, 0x0

    .line 1131
    .local v15, "columnValue":Ljava/lang/Object;
    packed-switch v30, :pswitch_data_0

    .line 1146
    :pswitch_0
    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1149
    .end local v15    # "columnValue":Ljava/lang/Object;
    :goto_5
    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1127
    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    .line 1133
    .restart local v15    # "columnValue":Ljava/lang/Object;
    :pswitch_1
    const/4 v15, 0x0

    .line 1134
    goto :goto_5

    .line 1136
    :pswitch_2
    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v15

    .line 1137
    .local v15, "columnValue":[B
    goto :goto_5

    .line 1139
    .local v15, "columnValue":Ljava/lang/Object;
    :pswitch_3
    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    .line 1140
    .local v15, "columnValue":Ljava/lang/Float;
    goto :goto_5

    .line 1142
    .local v15, "columnValue":Ljava/lang/Object;
    :pswitch_4
    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    .line 1143
    .local v15, "columnValue":Ljava/lang/Integer;
    goto :goto_5

    .line 1152
    .end local v14    # "arr$":[Ljava/lang/String;
    .end local v15    # "columnValue":Ljava/lang/Integer;
    .end local v16    # "i$":I
    .end local v17    # "index":I
    .end local v21    # "len$":I
    .end local v24    # "resultColumn":Ljava/lang/String;
    .end local v25    # "rowBuilder":Landroid/database/MatrixCursor$RowBuilder;
    .end local v30    # "type":I
    :cond_b
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_8

    .line 1154
    .end local v19    # "language":Ljava/lang/String;
    .end local v26    # "rowId":I
    .end local v28    # "selectedLanuguage":Ljava/lang/String;
    :cond_c
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->close()V

    .line 1168
    .end local v3    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v20    # "languageCode":Ljava/lang/String;
    .end local v22    # "matrixCursor":Landroid/database/MatrixCursor;
    .end local v23    # "providedCardTypeArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .end local v27    # "sb":Ljava/lang/StringBuilder;
    .end local v29    # "tempCursor":Landroid/database/Cursor;
    .end local v31    # "whereSb":Ljava/lang/StringBuilder;
    :cond_d
    :goto_6
    return-object v22

    .line 1159
    :cond_e
    const/4 v11, 0x0

    .line 1160
    .local v11, "groupBy":Ljava/lang/String;
    if-eqz p2, :cond_f

    move-object/from16 v0, p2

    array-length v4, v0

    const/4 v6, 0x1

    if-ne v4, v6, :cond_f

    const/4 v4, 0x0

    aget-object v4, p2, v4

    if-eqz v4, :cond_f

    .line 1161
    const/4 v4, 0x0

    aget-object v4, p2, v4

    const-string v6, "provider"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1162
    const-string v11, "provider"

    .line 1168
    :cond_f
    :goto_7
    const-string v7, "provided_card_type"

    const/4 v12, 0x0

    move-object/from16 v6, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v13, p5

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    goto :goto_6

    .line 1164
    :cond_10
    const/4 v4, 0x0

    aget-object v4, p2, v4

    const-string v6, "card_type_key"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1165
    const-string v11, "card_type_key"

    goto :goto_7

    .line 1131
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private queryRegisteredProvider(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1178
    if-nez p2, :cond_0

    .line 1179
    sget-object p2, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->DEFAULT_REGISTERED_PROVIDER_PROJECTION:[Ljava/lang/String;

    .line 1182
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v1, "provider"

    invoke-direct {v8, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1183
    .local v8, "sb":Ljava/lang/StringBuilder;
    const-string v1, " INNER JOIN "

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1184
    const-string v1, "setting"

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1185
    const-string v1, " ON ("

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1186
    const-string v1, "visible"

    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/SettingTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1187
    const-string v1, "=1 AND "

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1188
    const-string v1, "package_name"

    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/ProviderTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1189
    const-string v1, "="

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1190
    const-string v1, "provider"

    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/SettingTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1191
    const-string v1, ")"

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1193
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1194
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1195
    sget-object v1, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->REGISTERED_PROVIDER_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 1197
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1198
    const-string v1, "_id"

    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/ProviderTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 1200
    :cond_1
    const-string v5, "package_name"

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private queryRegisteredSection(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1210
    if-nez p2, :cond_0

    .line 1211
    sget-object p2, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->DEFAULT_REGISTERED_SECTION_PROJECTION:[Ljava/lang/String;

    .line 1214
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v1, "section"

    invoke-direct {v8, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1215
    .local v8, "sb":Ljava/lang/StringBuilder;
    const-string v1, " INNER JOIN "

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1216
    const-string v1, "setting"

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1217
    const-string v1, " ON ("

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1218
    const-string v1, "visible"

    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/SettingTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1219
    const-string v1, "=1 AND "

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1220
    const-string v1, "key"

    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/SectionTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1221
    const-string v1, "="

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1222
    const-string v1, "section_key"

    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/SettingTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1223
    const-string v1, ")"

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1225
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1226
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1227
    sget-object v1, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->REGISTERED_SECTION_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 1229
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1230
    const-string v1, "_id"

    invoke-static {v1}, Lcom/samsung/android/magazine/service/db/SectionTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 1233
    :cond_1
    const-string v5, "key"

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private querySetting(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 32
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1239
    const/4 v5, 0x0

    .line 1240
    .local v5, "joinProjection":[Ljava/lang/String;
    const/16 v18, 0x1

    .line 1241
    .local v18, "isMultiLanguage":Z
    if-nez p2, :cond_5

    .line 1242
    sget-object p2, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->SETTING_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

    .line 1243
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->SETTING_TABLE_VIRTUAL_COLUMNS:[Ljava/lang/String;

    .line 1249
    :goto_0
    if-eqz v18, :cond_10

    .line 1250
    const/16 v22, 0x0

    .line 1251
    .local v22, "matrixCursor":Landroid/database/MatrixCursor;
    if-nez v5, :cond_0

    .line 1252
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->containIdColumn([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1253
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "language_code"

    aput-object v7, v4, v6

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/database/DatabaseUtils;->appendSelectionArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1271
    :cond_0
    :goto_1
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    .line 1272
    .local v25, "sb":Ljava/lang/StringBuilder;
    const-string v4, "setting"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1273
    const-string v4, " LEFT OUTER JOIN "

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1274
    const-string v4, "multi_language"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1275
    const-string v4, " ON (multi_language.type=\'card_type\' AND setting.card_type_key=multi_language.key1 AND setting.provider=multi_language.key2)"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1276
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->containIconImageColumn([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1277
    const-string v4, " LEFT OUTER JOIN "

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1278
    const-string v4, "provided_card_type"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1279
    const-string v4, " ON (provided_card_type.provider=setting.provider AND provided_card_type.card_type_key=setting.card_type_key)"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1282
    :cond_1
    new-instance v31, Ljava/lang/StringBuilder;

    const-string v4, "setting.visible=1 AND multi_language.language_code IS NULL OR (multi_language.language_code=\'"

    move-object/from16 v0, v31

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1284
    .local v31, "whereSb":Ljava/lang/StringBuilder;
    const-string v4, "default"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1285
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1286
    .local v20, "languageCode":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 1289
    const-string v4, "\' OR multi_language.language_code=\'"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1290
    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1291
    const-string v4, "\')"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1300
    :goto_2
    new-instance v3, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v3}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1301
    .local v3, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1302
    sget-object v4, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->SETTING_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 1303
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1305
    if-nez p5, :cond_2

    .line 1306
    const-string v4, "_id"

    invoke-static {v4}, Lcom/samsung/android/magazine/service/db/SettingTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 1308
    :cond_2
    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v4, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v10, p5

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v29

    .line 1309
    .local v29, "tempCursor":Landroid/database/Cursor;
    if-eqz v29, :cond_f

    .line 1310
    new-instance v27, Landroid/util/SparseArray;

    invoke-direct/range {v27 .. v27}, Landroid/util/SparseArray;-><init>()V

    .line 1311
    .local v27, "settingArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    :cond_3
    :goto_3
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1312
    const-string v4, "_id"

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    .line 1313
    .local v28, "settingId":I
    const-string v4, "language_code"

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 1314
    .local v19, "language":Ljava/lang/String;
    if-eqz v19, :cond_4

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1315
    :cond_4
    move-object/from16 v0, v27

    move/from16 v1, v28

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_3

    .line 1246
    .end local v3    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v19    # "language":Ljava/lang/String;
    .end local v20    # "languageCode":Ljava/lang/String;
    .end local v22    # "matrixCursor":Landroid/database/MatrixCursor;
    .end local v25    # "sb":Ljava/lang/StringBuilder;
    .end local v27    # "settingArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .end local v28    # "settingId":I
    .end local v29    # "tempCursor":Landroid/database/Cursor;
    .end local v31    # "whereSb":Ljava/lang/StringBuilder;
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->containMultiLanguageColumn([Ljava/lang/String;)Z

    move-result v18

    goto/16 :goto_0

    .line 1257
    .restart local v22    # "matrixCursor":Landroid/database/MatrixCursor;
    :cond_6
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "_id"

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const-string v7, "language_code"

    aput-object v7, v4, v6

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/database/DatabaseUtils;->appendSelectionArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 1296
    .restart local v20    # "languageCode":Ljava/lang/String;
    .restart local v25    # "sb":Ljava/lang/StringBuilder;
    .restart local v31    # "whereSb":Ljava/lang/StringBuilder;
    :cond_7
    const-string v4, "\')"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1297
    const-string v20, "default"

    goto/16 :goto_2

    .line 1318
    .restart local v3    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v19    # "language":Ljava/lang/String;
    .restart local v27    # "settingArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .restart local v28    # "settingId":I
    .restart local v29    # "tempCursor":Landroid/database/Cursor;
    :cond_8
    invoke-virtual/range {v27 .. v28}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v4

    if-gez v4, :cond_3

    .line 1319
    move-object/from16 v0, v27

    move/from16 v1, v28

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_3

    .line 1323
    .end local v19    # "language":Ljava/lang/String;
    .end local v28    # "settingId":I
    :cond_9
    new-instance v22, Landroid/database/MatrixCursor;

    .end local v22    # "matrixCursor":Landroid/database/MatrixCursor;
    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1324
    .restart local v22    # "matrixCursor":Landroid/database/MatrixCursor;
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1326
    :cond_a
    const-string v4, "_id"

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    .line 1327
    .restart local v28    # "settingId":I
    const-string v4, "language_code"

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v29

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 1328
    .restart local v19    # "language":Ljava/lang/String;
    invoke-virtual/range {v27 .. v28}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 1329
    .local v26, "selectedLanuguage":Ljava/lang/String;
    if-nez v19, :cond_b

    if-eqz v26, :cond_c

    :cond_b
    if-eqz v19, :cond_d

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1331
    :cond_c
    invoke-virtual/range {v22 .. v22}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v24

    .line 1332
    .local v24, "rowBuilder":Landroid/database/MatrixCursor$RowBuilder;
    move-object/from16 v14, p2

    .local v14, "arr$":[Ljava/lang/String;
    array-length v0, v14

    move/from16 v21, v0

    .local v21, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_4
    move/from16 v0, v16

    move/from16 v1, v21

    if-ge v0, v1, :cond_d

    aget-object v23, v14, v16

    .line 1333
    .local v23, "resultColumn":Ljava/lang/String;
    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 1334
    .local v17, "index":I
    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getType(I)I

    move-result v30

    .line 1335
    .local v30, "type":I
    const/4 v15, 0x0

    .line 1336
    .local v15, "columnValue":Ljava/lang/Object;
    packed-switch v30, :pswitch_data_0

    .line 1351
    :pswitch_0
    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1354
    .end local v15    # "columnValue":Ljava/lang/Object;
    :goto_5
    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 1332
    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    .line 1338
    .restart local v15    # "columnValue":Ljava/lang/Object;
    :pswitch_1
    const/4 v15, 0x0

    .line 1339
    goto :goto_5

    .line 1341
    :pswitch_2
    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v15

    .line 1342
    .local v15, "columnValue":[B
    goto :goto_5

    .line 1344
    .local v15, "columnValue":Ljava/lang/Object;
    :pswitch_3
    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    .line 1345
    .local v15, "columnValue":Ljava/lang/Float;
    goto :goto_5

    .line 1347
    .local v15, "columnValue":Ljava/lang/Object;
    :pswitch_4
    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    .line 1348
    .local v15, "columnValue":Ljava/lang/Integer;
    goto :goto_5

    .line 1357
    .end local v14    # "arr$":[Ljava/lang/String;
    .end local v15    # "columnValue":Ljava/lang/Integer;
    .end local v16    # "i$":I
    .end local v17    # "index":I
    .end local v21    # "len$":I
    .end local v23    # "resultColumn":Ljava/lang/String;
    .end local v24    # "rowBuilder":Landroid/database/MatrixCursor$RowBuilder;
    .end local v30    # "type":I
    :cond_d
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_a

    .line 1359
    .end local v19    # "language":Ljava/lang/String;
    .end local v26    # "selectedLanuguage":Ljava/lang/String;
    .end local v28    # "settingId":I
    :cond_e
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->close()V

    .line 1393
    .end local v3    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v20    # "languageCode":Ljava/lang/String;
    .end local v22    # "matrixCursor":Landroid/database/MatrixCursor;
    .end local v25    # "sb":Ljava/lang/StringBuilder;
    .end local v27    # "settingArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .end local v29    # "tempCursor":Landroid/database/Cursor;
    .end local v31    # "whereSb":Ljava/lang/StringBuilder;
    :cond_f
    :goto_6
    return-object v22

    .line 1364
    :cond_10
    if-eqz p2, :cond_12

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->containIconImageColumn([Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1371
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    .line 1372
    .restart local v25    # "sb":Ljava/lang/StringBuilder;
    const-string v4, "setting"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1373
    const-string v4, " LEFT OUTER JOIN "

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1374
    const-string v4, "provided_card_type"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1375
    const-string v4, " ON (provided_card_type.provider=setting.provider AND provided_card_type.card_type_key=setting.card_type_key)"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1377
    new-instance v3, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v3}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1378
    .restart local v3    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1379
    sget-object v4, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->SETTING_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 1380
    if-nez p5, :cond_11

    .line 1381
    const-string v4, "_id"

    invoke-static {v4}, Lcom/samsung/android/magazine/service/db/SettingTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 1383
    :cond_11
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v6, v3

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v13, p5

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    goto :goto_6

    .line 1386
    .end local v3    # "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v25    # "sb":Ljava/lang/StringBuilder;
    :cond_12
    const/4 v11, 0x0

    .line 1387
    .local v11, "groupBy":Ljava/lang/String;
    if-eqz p2, :cond_13

    move-object/from16 v0, p2

    array-length v4, v0

    const/4 v6, 0x1

    if-ne v4, v6, :cond_13

    const/4 v4, 0x0

    aget-object v4, p2, v4

    if-eqz v4, :cond_13

    .line 1388
    const/4 v4, 0x0

    aget-object v4, p2, v4

    const-string v6, "section_key"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1389
    const-string v11, "section_key"

    .line 1393
    :cond_13
    const-string v7, "setting"

    const/4 v12, 0x0

    move-object/from16 v6, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v13, p5

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    goto/16 :goto_6

    .line 1336
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private updateChannels(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 16
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1537
    const/4 v15, 0x0

    .line 1538
    .local v15, "rowCount":I
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 1539
    .local v12, "contentValues":Landroid/content/ContentValues;
    const-string v3, "enable"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1540
    const-string v3, "enable"

    const-string v4, "enable"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1542
    :cond_0
    const-string v3, "attributes"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1543
    const-string v3, "attributes"

    const-string v4, "attributes"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1546
    :cond_1
    invoke-virtual {v12}, Landroid/content/ContentValues;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 1547
    const-string v3, "channel"

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v0, v3, v12, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 1548
    if-lez v15, :cond_4

    .line 1549
    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "key"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "enable"

    aput-object v4, v5, v3

    .line 1554
    .local v5, "projection":[Ljava/lang/String;
    const-string v4, "channel"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1555
    .local v13, "cursor":Landroid/database/Cursor;
    if-eqz v13, :cond_4

    .line 1556
    :goto_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1557
    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1558
    .local v11, "channelKey":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 1560
    .local v14, "enable":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    if-ne v14, v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-static {v4, v11, v3}, Lcom/samsung/android/magazine/service/Notifier;->sendChannelRunningStateChangedBroadcast(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 1562
    .end local v11    # "channelKey":Ljava/lang/String;
    .end local v14    # "enable":I
    :cond_3
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1566
    .end local v5    # "projection":[Ljava/lang/String;
    .end local v13    # "cursor":Landroid/database/Cursor;
    :cond_4
    return v15
.end method

.method private updateSettings(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 18
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1500
    const/16 v17, 0x0

    .line 1502
    .local v17, "rowCount":I
    const-string v3, "enable"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1503
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1504
    .local v13, "contentValues":Landroid/content/ContentValues;
    const-string v3, "enable"

    const-string v4, "enable"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1506
    const-string v3, "setting"

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v0, v3, v13, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v17

    .line 1507
    if-lez v17, :cond_2

    .line 1508
    const/4 v3, 0x4

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "channel_key"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "provider"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "card_type_key"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string v4, "enable"

    aput-object v4, v5, v3

    .line 1515
    .local v5, "projection":[Ljava/lang/String;
    const-string v4, "setting"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 1516
    .local v14, "cursor":Landroid/database/Cursor;
    if-eqz v14, :cond_2

    .line 1517
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1518
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1519
    .local v12, "channelName":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1520
    .local v16, "providerName":Ljava/lang/String;
    const/4 v3, 0x2

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1521
    .local v11, "cardTypeName":Ljava/lang/String;
    const/4 v3, 0x3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 1524
    .local v15, "enable":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    if-ne v15, v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    move-object/from16 v0, v16

    invoke-static {v4, v0, v12, v11, v3}, Lcom/samsung/android/magazine/service/Notifier;->sendCardTypeSubscriptionChangedBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 1527
    .end local v11    # "cardTypeName":Ljava/lang/String;
    .end local v12    # "channelName":Ljava/lang/String;
    .end local v15    # "enable":I
    .end local v16    # "providerName":Ljava/lang/String;
    :cond_1
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 1532
    .end local v5    # "projection":[Ljava/lang/String;
    .end local v13    # "contentValues":Landroid/content/ContentValues;
    .end local v14    # "cursor":Landroid/database/Cursor;
    :cond_2
    return v17
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "valuesArray"    # [Landroid/content/ContentValues;

    .prologue
    const/4 v7, 0x0

    .line 264
    :try_start_0
    array-length v10, p2

    const/4 v11, 0x1

    if-ge v10, v11, :cond_0

    move v6, v7

    .line 292
    :goto_0
    return v6

    .line 268
    :cond_0
    sget-object v10, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v10, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 287
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v6

    goto :goto_0

    .line 271
    :pswitch_0
    const/4 v6, 0x0

    .line 272
    .local v6, "rowCount":I
    monitor-enter p0
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    :try_start_1
    iget-object v10, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v10}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 274
    .local v2, "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 275
    move-object v0, p2

    .local v0, "arr$":[Landroid/content/ContentValues;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v1, v0, v4

    .line 276
    .local v1, "contentValues":Landroid/content/ContentValues;
    invoke-direct {p0, v2, v1}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->insertSetting(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 277
    .local v8, "rowId":J
    const-wide/16 v10, -0x1

    cmp-long v10, v8, v10

    if-lez v10, :cond_1

    .line 278
    add-int/lit8 v6, v6, 0x1

    .line 275
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 281
    .end local v1    # "contentValues":Landroid/content/ContentValues;
    .end local v8    # "rowId":J
    :cond_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 282
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 283
    monitor-exit p0

    goto :goto_0

    .end local v0    # "arr$":[Landroid/content/ContentValues;
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catchall_0
    move-exception v10

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v10
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 289
    .end local v6    # "rowCount":I
    :catch_0
    move-exception v3

    .line 290
    .local v3, "e":Landroid/database/sqlite/SQLiteException;
    const-string v10, "MagazineService::CardChannelContentProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Database operation failed. : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    move v6, v7

    .line 292
    goto :goto_0

    .line 268
    nop

    :pswitch_data_0
    .packed-switch 0x2bc
        :pswitch_0
    .end packed-switch
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 19
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 313
    :try_start_0
    sget-object v4, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 403
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unsupported URI: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 405
    :catch_0
    move-exception v13

    .line 406
    .local v13, "e":Landroid/database/sqlite/SQLiteException;
    const-string v4, "MagazineService::CardChannelContentProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Database operation failed. : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 408
    const/4 v15, 0x0

    .end local v13    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    return v15

    .line 316
    :sswitch_0
    const/4 v15, 0x0

    .line 317
    .local v15, "rowCount":I
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 318
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 319
    .local v3, "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 320
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v3, v1, v2}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->deleteCards(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 321
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 322
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 323
    monitor-exit p0

    goto :goto_0

    .end local v3    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v4

    .line 328
    .end local v15    # "rowCount":I
    :sswitch_1
    const/4 v15, 0x0

    .line 329
    .restart local v15    # "rowCount":I
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 330
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 331
    .restart local v3    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 332
    const/4 v11, 0x0

    .line 333
    .local v11, "cardKey":Ljava/lang/String;
    const/4 v14, 0x0

    .line 334
    .local v14, "provider":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_id="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 335
    .local v6, "whereClause1":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "card_key"

    aput-object v7, v5, v4

    const/4 v4, 0x1

    const-string v7, "card_provider"

    aput-object v7, v5, v4

    .line 336
    .local v5, "projection":[Ljava/lang/String;
    const-string v4, "card"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 337
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_1

    .line 338
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 339
    const/4 v4, 0x0

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 340
    const/4 v4, 0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 342
    :cond_0
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 345
    :cond_1
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 347
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v4, "card_key"

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 348
    .local v17, "sb2":Ljava/lang/StringBuilder;
    const-string v4, "=? AND "

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    const-string v4, "card_provider"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    const-string v4, "=?"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    const/4 v4, 0x2

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v4, 0x0

    aput-object v11, v18, v4

    const/4 v4, 0x1

    aput-object v14, v18, v4

    .line 352
    .local v18, "whereArgs":[Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v3, v4, v1}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->deleteCards(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 354
    .end local v17    # "sb2":Ljava/lang/StringBuilder;
    .end local v18    # "whereArgs":[Ljava/lang/String;
    :cond_2
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 355
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 356
    monitor-exit p0

    goto/16 :goto_0

    .end local v3    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v5    # "projection":[Ljava/lang/String;
    .end local v6    # "whereClause1":Ljava/lang/String;
    .end local v11    # "cardKey":Ljava/lang/String;
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v14    # "provider":Ljava/lang/String;
    :catchall_1
    move-exception v4

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v4

    .line 361
    .end local v15    # "rowCount":I
    :sswitch_2
    const/4 v15, 0x0

    .line 362
    .restart local v15    # "rowCount":I
    monitor-enter p0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 363
    :try_start_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 364
    .restart local v3    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 365
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v3, v1, v2}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->deleteSettings(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 366
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 367
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 368
    monitor-exit p0

    goto/16 :goto_0

    .end local v3    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_2
    move-exception v4

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v4

    .line 373
    .end local v15    # "rowCount":I
    :sswitch_3
    new-instance v16, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 374
    .local v16, "sb":Ljava/lang/StringBuilder;
    const-string v4, "="

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 377
    const-string v4, " AND ("

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    const-string v4, ")"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    :cond_3
    const/4 v15, 0x0

    .line 382
    .restart local v15    # "rowCount":I
    monitor-enter p0
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    .line 383
    :try_start_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 384
    .restart local v3    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 385
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v3, v4, v1}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->deleteSettings(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 386
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 387
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 388
    monitor-exit p0

    goto/16 :goto_0

    .end local v3    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_3
    move-exception v4

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    throw v4

    .line 401
    .end local v15    # "rowCount":I
    .end local v16    # "sb":Ljava/lang/StringBuilder;
    :sswitch_4
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unsupported opeation: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 313
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0xc8 -> :sswitch_4
        0xc9 -> :sswitch_4
        0x12c -> :sswitch_4
        0x12d -> :sswitch_4
        0x190 -> :sswitch_4
        0x1f4 -> :sswitch_4
        0x1f5 -> :sswitch_4
        0x258 -> :sswitch_4
        0x259 -> :sswitch_4
        0x2bc -> :sswitch_2
        0x2bd -> :sswitch_3
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 537
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 565
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 539
    :sswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardchannel.card"

    .line 563
    :goto_0
    return-object v0

    .line 541
    :sswitch_1
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardchannel.card"

    goto :goto_0

    .line 543
    :sswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardchannel.card_element"

    goto :goto_0

    .line 545
    :sswitch_3
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardchannel.card_element"

    goto :goto_0

    .line 547
    :sswitch_4
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardchannel.channel"

    goto :goto_0

    .line 549
    :sswitch_5
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardchannel.channel"

    goto :goto_0

    .line 551
    :sswitch_6
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardchannel.section_display_name"

    goto :goto_0

    .line 553
    :sswitch_7
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardchannel.provider"

    goto :goto_0

    .line 555
    :sswitch_8
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardchannel.provider"

    goto :goto_0

    .line 557
    :sswitch_9
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardchannel.provided_card_type"

    goto :goto_0

    .line 559
    :sswitch_a
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardchannel.provided_card_type"

    goto :goto_0

    .line 561
    :sswitch_b
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardchannel.setting"

    goto :goto_0

    .line 563
    :sswitch_c
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardchannel.setting"

    goto :goto_0

    .line 537
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_3
        0x12c -> :sswitch_4
        0x12d -> :sswitch_5
        0x190 -> :sswitch_6
        0x1f4 -> :sswitch_9
        0x1f5 -> :sswitch_a
        0x258 -> :sswitch_7
        0x259 -> :sswitch_8
        0x2bc -> :sswitch_b
        0x2bd -> :sswitch_c
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 586
    :try_start_0
    sget-object v5, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 630
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unsupported URI: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 632
    :catch_0
    move-exception v2

    .line 633
    .local v2, "e":Landroid/database/sqlite/SQLiteException;
    const-string v5, "MagazineService::CardChannelContentProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Database operation failed. : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 635
    const/4 v4, 0x0

    .end local v2    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    return-object v4

    .line 589
    :sswitch_0
    const/4 v4, 0x0

    .line 590
    .local v4, "resultUri":Landroid/net/Uri;
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 591
    :try_start_2
    iget-object v5, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 592
    .local v1, "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 593
    invoke-direct {p0, v1, p2}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->insertSetting(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v6

    .line 594
    .local v6, "rowId":J
    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    .line 595
    sget-object v5, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Setting;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 597
    :cond_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 598
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 599
    monitor-exit p0

    goto :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v6    # "rowId":J
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v5

    .line 604
    .end local v4    # "resultUri":Landroid/net/Uri;
    :sswitch_1
    const/4 v4, 0x0

    .line 605
    .restart local v4    # "resultUri":Landroid/net/Uri;
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 606
    :try_start_4
    iget-object v5, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 607
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v5, "channel"

    const/4 v8, 0x0

    invoke-virtual {v1, v5, v8, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    .line 608
    .restart local v6    # "rowId":J
    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    .line 609
    const-string v5, "key"

    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 610
    .local v0, "channelKey":Ljava/lang/String;
    const-string v5, "enable"

    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    .line 611
    .local v3, "enable":Ljava/lang/Integer;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v8, 0x1

    if-ne v5, v8, :cond_1

    .line 612
    iget-object v5, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    const/4 v8, 0x1

    invoke-static {v5, v0, v8}, Lcom/samsung/android/magazine/service/Notifier;->sendChannelRunningStateChangedBroadcast(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 614
    :cond_1
    sget-object v5, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Channel;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 616
    .end local v0    # "channelKey":Ljava/lang/String;
    .end local v3    # "enable":Ljava/lang/Integer;
    :cond_2
    monitor-exit p0

    goto :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v6    # "rowId":J
    :catchall_1
    move-exception v5

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v5

    .line 628
    .end local v4    # "resultUri":Landroid/net/Uri;
    :sswitch_2
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unsupported opeation: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 586
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_2
        0x65 -> :sswitch_2
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_2
        0x12c -> :sswitch_1
        0x190 -> :sswitch_2
        0x1f4 -> :sswitch_2
        0x1f5 -> :sswitch_2
        0x258 -> :sswitch_2
        0x259 -> :sswitch_2
        0x2bc -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 252
    const-string v0, "MagazineService Version"

    const-string v1, "Magazine-Channel: 14"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    invoke-virtual {p0}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    .line 254
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/magazine/service/db/CardDbHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    .line 255
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    if-nez v0, :cond_0

    .line 256
    const/4 v0, 0x0

    .line 258
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/high16 v12, 0x10000000

    const/4 v8, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x0

    .line 1572
    const/4 v3, 0x0

    .line 1574
    .local v3, "fileDescriptor":Landroid/os/ParcelFileDescriptor;
    sget-object v7, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v7, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 1624
    new-instance v7, Ljava/io/FileNotFoundException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unsupported URI: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1579
    :sswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    .line 1580
    .local v5, "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    if-lt v7, v8, :cond_0

    .line 1581
    const-string v7, "r"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1582
    new-instance v1, Ljava/io/File;

    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    const-string v8, "image"

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v8

    const/4 v7, 0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {v1, v8, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1583
    .local v1, "directory":Ljava/io/File;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v5, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1584
    .local v4, "fileName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1585
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1586
    invoke-static {v2, v12}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    .line 1627
    .end local v1    # "directory":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "fileName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v3

    .line 1589
    .restart local v1    # "directory":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "fileName":Ljava/lang/String;
    :cond_1
    new-instance v7, Ljava/io/FileNotFoundException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "File does not exist: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1594
    .end local v1    # "directory":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "fileName":Ljava/lang/String;
    :cond_2
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unsupported Mode: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1603
    .end local v5    # "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    .line 1604
    .restart local v5    # "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    if-lt v7, v8, :cond_0

    .line 1605
    const-string v7, "r"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1606
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mContext:Landroid/content/Context;

    const-string v8, "image"

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v8

    const/4 v7, 0x2

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {v6, v8, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1607
    .local v6, "providerDirectory":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {v0, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1608
    .local v0, "cardNameDirectory":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    invoke-interface {v5, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {v2, v0, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1609
    .restart local v2    # "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1610
    invoke-static {v2, v12}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    goto :goto_0

    .line 1613
    :cond_3
    new-instance v7, Ljava/io/FileNotFoundException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "File does not exist: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1618
    .end local v0    # "cardNameDirectory":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v6    # "providerDirectory":Ljava/io/File;
    :cond_4
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unsupported Mode: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1574
    nop

    :sswitch_data_0
    .sparse-switch
        0x6e -> :sswitch_0
        0x1fe -> :sswitch_1
    .end sparse-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 730
    :try_start_0
    sget-object v0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 915
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 917
    :catch_0
    move-exception v10

    .line 918
    .local v10, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "MagazineService::CardChannelContentProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Database operation failed. : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 920
    const/4 v9, 0x0

    .end local v10    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    return-object v9

    .line 733
    :sswitch_0
    const/4 v9, 0x0

    .line 734
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 735
    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .local v1, "database":Landroid/database/sqlite/SQLiteDatabase;
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    .line 736
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->queryCard(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 737
    monitor-exit p0

    goto :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0

    .line 742
    .end local v9    # "cursor":Landroid/database/Cursor;
    :sswitch_1
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v0, "_id"

    invoke-static {v0}, Lcom/samsung/android/magazine/service/db/CardTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 743
    .local v11, "sb":Ljava/lang/StringBuilder;
    const-string v0, "="

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 744
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 745
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 746
    const-string v0, " AND ("

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 747
    invoke-virtual {v11, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 748
    const-string v0, ")"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 750
    :cond_0
    const/4 v9, 0x0

    .line 751
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 752
    :try_start_4
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 753
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v2, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->queryCard(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 754
    monitor-exit p0

    goto :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0

    .line 759
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v11    # "sb":Ljava/lang/StringBuilder;
    :sswitch_2
    const/4 v9, 0x0

    .line 760
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 761
    :try_start_6
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 762
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "card_element"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 763
    monitor-exit p0

    goto :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v0

    .line 768
    .end local v9    # "cursor":Landroid/database/Cursor;
    :sswitch_3
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v0, "_id"

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 769
    .restart local v11    # "sb":Ljava/lang/StringBuilder;
    const-string v0, "="

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 770
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 771
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 772
    const-string v0, " AND ("

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 773
    invoke-virtual {v11, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 774
    const-string v0, ")"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 776
    :cond_1
    const/4 v9, 0x0

    .line 777
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    .line 778
    :try_start_8
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 779
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "card_element"

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p2

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 780
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_3
    move-exception v0

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    throw v0

    .line 785
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v11    # "sb":Ljava/lang/StringBuilder;
    :sswitch_4
    const/4 v9, 0x0

    .line 786
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 787
    :try_start_a
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 788
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "channel"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 789
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_4
    move-exception v0

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :try_start_b
    throw v0

    .line 794
    .end local v9    # "cursor":Landroid/database/Cursor;
    :sswitch_5
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v0, "_id"

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 795
    .restart local v11    # "sb":Ljava/lang/StringBuilder;
    const-string v0, "="

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 797
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 798
    const-string v0, " AND ("

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 799
    invoke-virtual {v11, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 800
    const-string v0, ")"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 802
    :cond_2
    const/4 v9, 0x0

    .line 803
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_0

    .line 804
    :try_start_c
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 805
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "channel"

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p2

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 806
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_5
    move-exception v0

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    :try_start_d
    throw v0

    .line 811
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v11    # "sb":Ljava/lang/StringBuilder;
    :sswitch_6
    const/4 v9, 0x0

    .line 812
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_0

    .line 813
    :try_start_e
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    .line 814
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->queryMultiLanguageSection(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 815
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_6
    move-exception v0

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    :try_start_f
    throw v0

    .line 820
    .end local v9    # "cursor":Landroid/database/Cursor;
    :sswitch_7
    const/4 v9, 0x0

    .line 821
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_0

    .line 822
    :try_start_10
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 823
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "provider"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 824
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_7
    move-exception v0

    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_7

    :try_start_11
    throw v0

    .line 829
    .end local v9    # "cursor":Landroid/database/Cursor;
    :sswitch_8
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v0, "_id"

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 830
    .restart local v11    # "sb":Ljava/lang/StringBuilder;
    const-string v0, "="

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 831
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 832
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 833
    const-string v0, " AND ("

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 834
    invoke-virtual {v11, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 835
    const-string v0, ")"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 837
    :cond_3
    const/4 v9, 0x0

    .line 838
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_0

    .line 839
    :try_start_12
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 840
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "provider"

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p2

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 841
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_8
    move-exception v0

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_8

    :try_start_13
    throw v0

    .line 846
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v11    # "sb":Ljava/lang/StringBuilder;
    :sswitch_9
    const/4 v9, 0x0

    .line 847
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_0

    .line 848
    :try_start_14
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    .line 849
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->queryProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 850
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_9
    move-exception v0

    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_9

    :try_start_15
    throw v0

    .line 855
    .end local v9    # "cursor":Landroid/database/Cursor;
    :sswitch_a
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v0, "_id"

    invoke-static {v0}, Lcom/samsung/android/magazine/service/db/ProvidedCardTypeTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 856
    .restart local v11    # "sb":Ljava/lang/StringBuilder;
    const-string v0, "="

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 857
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 858
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 859
    const-string v0, " AND ("

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 860
    invoke-virtual {v11, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 861
    const-string v0, ")"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 863
    :cond_4
    const/4 v9, 0x0

    .line 864
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_15
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_15 .. :try_end_15} :catch_0

    .line 865
    :try_start_16
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 866
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v2, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->queryProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 867
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_a
    move-exception v0

    monitor-exit p0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_a

    :try_start_17
    throw v0

    .line 872
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v11    # "sb":Ljava/lang/StringBuilder;
    :sswitch_b
    const/4 v9, 0x0

    .line 873
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_17
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_17 .. :try_end_17} :catch_0

    .line 874
    :try_start_18
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    .line 875
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->queryRegisteredProvider(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 876
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_b
    move-exception v0

    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_b

    :try_start_19
    throw v0

    .line 881
    .end local v9    # "cursor":Landroid/database/Cursor;
    :sswitch_c
    const/4 v9, 0x0

    .line 882
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_19
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_19 .. :try_end_19} :catch_0

    .line 883
    :try_start_1a
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    .line 884
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->queryRegisteredSection(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 885
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_c
    move-exception v0

    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_c

    :try_start_1b
    throw v0

    .line 890
    .end local v9    # "cursor":Landroid/database/Cursor;
    :sswitch_d
    const/4 v9, 0x0

    .line 891
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_1b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1b .. :try_end_1b} :catch_0

    .line 892
    :try_start_1c
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    .line 893
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->querySetting(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 894
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_d
    move-exception v0

    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_d

    :try_start_1d
    throw v0

    .line 899
    .end local v9    # "cursor":Landroid/database/Cursor;
    :sswitch_e
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v0, "_id"

    invoke-static {v0}, Lcom/samsung/android/magazine/service/db/SettingTable;->addTableName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 900
    .restart local v11    # "sb":Ljava/lang/StringBuilder;
    const-string v0, "="

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 901
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 902
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 903
    const-string v0, " AND ("

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 904
    invoke-virtual {v11, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 905
    const-string v0, ")"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 907
    :cond_5
    const/4 v9, 0x0

    .line 908
    .restart local v9    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_1d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1d .. :try_end_1d} :catch_0

    .line 909
    :try_start_1e
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 910
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v2, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->querySetting(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 911
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_e
    move-exception v0

    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_e

    :try_start_1f
    throw v0
    :try_end_1f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1f .. :try_end_1f} :catch_0

    .line 730
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_3
        0x12c -> :sswitch_4
        0x12d -> :sswitch_5
        0x190 -> :sswitch_6
        0x1f4 -> :sswitch_9
        0x1f5 -> :sswitch_a
        0x258 -> :sswitch_7
        0x259 -> :sswitch_8
        0x2bc -> :sswitch_d
        0x2bd -> :sswitch_e
        0x320 -> :sswitch_b
        0x321 -> :sswitch_c
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1416
    :try_start_0
    sget-object v4, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 1490
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unsupported URI: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1492
    :catch_0
    move-exception v1

    .line 1493
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    const-string v4, "MagazineService::CardChannelContentProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Database operation failed. : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1494
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 1495
    const/4 v2, 0x0

    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    return v2

    .line 1419
    :sswitch_0
    const/4 v2, 0x0

    .line 1420
    .local v2, "rowCount":I
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1421
    :try_start_2
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1422
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1423
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->updateSettings(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1424
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1425
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1426
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v4

    .line 1431
    .end local v2    # "rowCount":I
    :sswitch_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1432
    .local v3, "sb":Ljava/lang/StringBuilder;
    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1433
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1434
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1435
    const-string v4, " AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1436
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1437
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1439
    :cond_0
    const/4 v2, 0x0

    .line 1440
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1441
    :try_start_4
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1442
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1443
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, p2, v4, p4}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->updateSettings(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1444
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1445
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1446
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v4

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v4

    .line 1451
    .end local v2    # "rowCount":I
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :sswitch_2
    const/4 v2, 0x0

    .line 1452
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1453
    :try_start_6
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1454
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1455
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->updateChannels(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1456
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1457
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1458
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_2
    move-exception v4

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v4

    .line 1463
    .end local v2    # "rowCount":I
    :sswitch_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1464
    .restart local v3    # "sb":Ljava/lang/StringBuilder;
    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1465
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1466
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1467
    const-string v4, " AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1468
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1469
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1471
    :cond_1
    const/4 v2, 0x0

    .line 1472
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    .line 1473
    :try_start_8
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1474
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1475
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, p2, v4, p4}, Lcom/samsung/android/magazine/service/provider/CardChannelContentProvider;->updateChannels(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1476
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1477
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1478
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_3
    move-exception v4

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    throw v4

    .line 1488
    .end local v2    # "rowCount":I
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :sswitch_4
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unsupported operation: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 1416
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_4
        0x65 -> :sswitch_4
        0x12c -> :sswitch_2
        0x12d -> :sswitch_3
        0x190 -> :sswitch_4
        0x1f4 -> :sswitch_4
        0x1f5 -> :sswitch_4
        0x258 -> :sswitch_4
        0x259 -> :sswitch_4
        0x2bc -> :sswitch_0
        0x2bd -> :sswitch_1
    .end sparse-switch
.end method
