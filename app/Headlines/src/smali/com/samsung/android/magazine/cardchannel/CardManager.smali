.class public Lcom/samsung/android/magazine/cardchannel/CardManager;
.super Ljava/lang/Object;
.source "CardManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;,
        Lcom/samsung/android/magazine/cardchannel/CardManager$CardInfo;,
        Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;
    }
.end annotation


# static fields
.field public static final ACTION_CARD_ADDED:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CARD_ADDED"

.field public static final ACTION_CARD_REMOVED:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CARD_REMOVED"

.field public static final ACTION_CARD_UPDATED:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CARD_UPDATED"

.field private static final BROADCAST_RECEIVER_PERMISSION:Ljava/lang/String; = "com.samsung.android.magazine.permission.READ_CARD_PROVIDER"

.field private static final CARD_DELETE:Ljava/lang/String; = "delete"

.field private static final CARD_INSERT:Ljava/lang/String; = "insert"

.field private static final CARD_UPDATE:Ljava/lang/String; = "update"

.field private static final CHANNEL_NOT_SPECIFIED:Ljava/lang/String; = "channel_not_defined"

.field public static final EXTRA_CARD_PRIMARY_KEY:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.CARD_PRIMARY_KEY"

.field private static final TAG:Ljava/lang/String;

.field private static mRegisteredCardChannels:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

.field private mCardList:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/CardManager$CardInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCardObserver:Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;

.field private mCardUpdateListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/samsung/android/magazine/cardchannel/CardManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    .line 77
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mRegisteredCardChannels:Ljava/util/HashSet;

    .line 103
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mRegisteredCardChannels:Ljava/util/HashSet;

    .line 104
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/magazine/cardchannel/CardChannel;)V
    .locals 2
    .param p1, "cardChannel"    # Lcom/samsung/android/magazine/cardchannel/CardChannel;

    .prologue
    const/4 v1, 0x0

    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object v1, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    .line 80
    iput-object v1, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 81
    iput-object v1, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardObserver:Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardUpdateListeners:Ljava/util/HashMap;

    .line 83
    iput-object v1, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardList:Landroid/util/SparseArray;

    .line 194
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    .line 195
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 196
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardUpdateListeners:Ljava/util/HashMap;

    .line 197
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardList:Landroid/util/SparseArray;

    .line 198
    iput-object v1, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardObserver:Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;

    .line 199
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/magazine/cardchannel/CardManager;)Lcom/samsung/android/magazine/cardchannel/CardChannel;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/magazine/cardchannel/CardManager;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardUpdateListeners:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/magazine/cardchannel/CardManager;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardList:Landroid/util/SparseArray;

    return-object v0
.end method

.method private createCard(Landroid/database/Cursor;)Lcom/samsung/android/magazine/cardchannel/Card;
    .locals 25
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 549
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 550
    .local v21, "id":I
    const-string v2, "card_provider"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 551
    .local v23, "provider":Ljava/lang/String;
    new-instance v13, Lcom/samsung/android/magazine/cardchannel/Card;

    .line 552
    const-string v2, "card_type"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 553
    const-string v3, "template_type"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 551
    move/from16 v0, v21

    move-object/from16 v1, v23

    invoke-direct {v13, v0, v2, v3, v1}, Lcom/samsung/android/magazine/cardchannel/Card;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    .local v13, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    const-string v2, "attributes"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 556
    .local v11, "attributes":Ljava/lang/String;
    if-eqz v11, :cond_0

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 557
    invoke-static {v11}, Lcom/samsung/android/magazine/cardchannel/util/DataConverter;->getMapFromJson(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v13, v2}, Lcom/samsung/android/magazine/cardchannel/Card;->setAttributes(Ljava/util/Map;)V

    .line 559
    :cond_0
    const-string v2, "time_stamp"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v13, v2, v3}, Lcom/samsung/android/magazine/cardchannel/Card;->setLastModifiedTime(J)V

    .line 561
    const-string v2, "card_key"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 564
    .local v14, "cardKey":Ljava/lang/String;
    const/16 v20, 0x0

    .line 566
    .local v20, "elementCursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "card_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 567
    .local v5, "where":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 568
    sget-object v3, Lcom/samsung/android/magazine/service/contract/CardChannelContract$CardElement;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 569
    const/4 v7, 0x0

    .line 567
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v20

    .line 574
    .end local v5    # "where":Ljava/lang/String;
    :goto_0
    if-eqz v20, :cond_2

    .line 575
    :cond_1
    :goto_1
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 632
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 635
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardList:Landroid/util/SparseArray;

    new-instance v3, Lcom/samsung/android/magazine/cardchannel/CardManager$CardInfo;

    move-object/from16 v0, v23

    invoke-direct {v3, v0, v14}, Lcom/samsung/android/magazine/cardchannel/CardManager$CardInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v0, v21

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 636
    return-object v13

    .line 570
    :catch_0
    move-exception v16

    .line 571
    .local v16, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v3, "[Exception] removeCard"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    invoke-virtual/range {v16 .. v16}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 576
    .end local v16    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    const-string v2, "type"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 577
    .local v24, "type":Ljava/lang/String;
    const-string v2, "key"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 579
    .local v22, "key":Ljava/lang/String;
    const-string v2, "action"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 580
    new-instance v8, Lcom/samsung/android/magazine/cardchannel/CardAction;

    .line 581
    const-string v2, "action_type"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 580
    invoke-direct {v8, v2}, Lcom/samsung/android/magazine/cardchannel/CardAction;-><init>(Ljava/lang/String;)V

    .line 582
    .local v8, "action":Lcom/samsung/android/magazine/cardchannel/CardAction;
    const-string v2, "action_label"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/samsung/android/magazine/cardchannel/CardAction;->setLabel(Ljava/lang/String;)V

    .line 583
    const-string v2, "action_data"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 584
    .local v12, "bytes":Ljava/lang/String;
    if-eqz v12, :cond_4

    .line 585
    invoke-static {v12}, Lcom/samsung/android/magazine/cardchannel/util/DataConverter;->getIntentFromJson(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/samsung/android/magazine/cardchannel/CardAction;->setData(Landroid/content/Intent;)V

    .line 589
    :cond_4
    const-string v2, "action_attributes"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 590
    .local v9, "actionAttributes":Ljava/lang/String;
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 591
    invoke-static {v9}, Lcom/samsung/android/magazine/cardchannel/util/DataConverter;->getMapFromJson(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/samsung/android/magazine/cardchannel/CardAction;->setAttributes(Ljava/util/Map;)V

    .line 594
    :cond_5
    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v8}, Lcom/samsung/android/magazine/cardchannel/Card;->addCardAction(Ljava/lang/String;Lcom/samsung/android/magazine/cardchannel/CardAction;)V

    goto/16 :goto_1

    .line 597
    .end local v8    # "action":Lcom/samsung/android/magazine/cardchannel/CardAction;
    .end local v9    # "actionAttributes":Ljava/lang/String;
    .end local v12    # "bytes":Ljava/lang/String;
    :cond_6
    const/16 v17, 0x0

    .line 598
    .local v17, "element":Lcom/samsung/android/magazine/cardchannel/CardElement;
    const-string v2, "data1"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 599
    .local v15, "data":Ljava/lang/String;
    const-string v2, "image"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 600
    new-instance v17, Lcom/samsung/android/magazine/cardchannel/CardImage;

    .end local v17    # "element":Lcom/samsung/android/magazine/cardchannel/CardElement;
    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v15}, Lcom/samsung/android/magazine/cardchannel/CardImage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    .restart local v17    # "element":Lcom/samsung/android/magazine/cardchannel/CardElement;
    :goto_2
    const-string v2, "attributes"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 610
    .local v19, "elementAttributes":Ljava/lang/String;
    if-eqz v19, :cond_7

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_7

    .line 611
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/magazine/cardchannel/util/DataConverter;->getMapFromJson(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/samsung/android/magazine/cardchannel/CardElement;->setAttributes(Ljava/util/Map;)V

    .line 614
    :cond_7
    const-string v2, "action_type"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 615
    .local v10, "actionType":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 616
    new-instance v18, Lcom/samsung/android/magazine/cardchannel/CardAction;

    move-object/from16 v0, v18

    invoke-direct {v0, v10}, Lcom/samsung/android/magazine/cardchannel/CardAction;-><init>(Ljava/lang/String;)V

    .line 617
    .local v18, "elementAction":Lcom/samsung/android/magazine/cardchannel/CardAction;
    const-string v2, "action_label"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/samsung/android/magazine/cardchannel/CardAction;->setLabel(Ljava/lang/String;)V

    .line 618
    const-string v2, "action_data"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 619
    .restart local v12    # "bytes":Ljava/lang/String;
    if-eqz v12, :cond_8

    .line 620
    invoke-static {v12}, Lcom/samsung/android/magazine/cardchannel/util/DataConverter;->getIntentFromJson(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/samsung/android/magazine/cardchannel/CardAction;->setData(Landroid/content/Intent;)V

    .line 623
    :cond_8
    const-string v2, "action_attributes"

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 624
    .restart local v9    # "actionAttributes":Ljava/lang/String;
    if-eqz v9, :cond_9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_9

    .line 625
    invoke-static {v9}, Lcom/samsung/android/magazine/cardchannel/util/DataConverter;->getMapFromJson(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/samsung/android/magazine/cardchannel/CardAction;->setAttributes(Ljava/util/Map;)V

    .line 627
    :cond_9
    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/magazine/cardchannel/CardElement;->setAction(Lcom/samsung/android/magazine/cardchannel/CardAction;)V

    .line 629
    .end local v9    # "actionAttributes":Ljava/lang/String;
    .end local v12    # "bytes":Ljava/lang/String;
    .end local v18    # "elementAction":Lcom/samsung/android/magazine/cardchannel/CardAction;
    :cond_a
    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/samsung/android/magazine/cardchannel/Card;->addCardElement(Lcom/samsung/android/magazine/cardchannel/CardElement;)V

    goto/16 :goto_1

    .line 602
    .end local v10    # "actionType":Ljava/lang/String;
    .end local v19    # "elementAttributes":Ljava/lang/String;
    :cond_b
    const-string v2, "text"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 603
    new-instance v17, Lcom/samsung/android/magazine/cardchannel/CardText;

    .end local v17    # "element":Lcom/samsung/android/magazine/cardchannel/CardElement;
    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v15}, Lcom/samsung/android/magazine/cardchannel/CardText;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v17    # "element":Lcom/samsung/android/magazine/cardchannel/CardElement;
    goto/16 :goto_2
.end method

.method static isRegistered(Ljava/lang/String;)Z
    .locals 1
    .param p0, "channelName"    # Ljava/lang/String;

    .prologue
    .line 648
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mRegisteredCardChannels:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static removeRegisteredCardChannel(Ljava/lang/String;)V
    .locals 1
    .param p0, "channelName"    # Ljava/lang/String;

    .prologue
    .line 644
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mRegisteredCardChannels:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 645
    return-void
.end method

.method static setRegisteredCardChannel(Ljava/lang/String;)V
    .locals 1
    .param p0, "channelName"    # Ljava/lang/String;

    .prologue
    .line 640
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mRegisteredCardChannels:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 641
    return-void
.end method


# virtual methods
.method public addCardChangeListener(Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;

    .prologue
    .line 415
    if-nez p1, :cond_0

    .line 416
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v3, "Failed to add listener. listener is null."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    :goto_0
    return-void

    .line 421
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardObserver:Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;

    if-nez v2, :cond_1

    .line 422
    new-instance v2, Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;

    invoke-direct {v2, p0}, Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;-><init>(Lcom/samsung/android/magazine/cardchannel/CardManager;)V

    iput-object v2, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardObserver:Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;

    .line 423
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardObserver:Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    :cond_1
    :goto_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x40

    const/16 v4, 0x24

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 431
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardUpdateListeners:Ljava/util/HashMap;

    invoke-virtual {v2, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 425
    .end local v1    # "key":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 426
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v3, "[Exception] addCardChangeListener"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public getCard(I)Lcom/samsung/android/magazine/cardchannel/Card;
    .locals 9
    .param p1, "cardId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 380
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/magazine/cardchannel/CardManager;->isRegistered(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 381
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    const-string v2, "CardChannel is not registered"

    invoke-direct {v0, v2}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383
    :cond_0
    if-gez p1, :cond_2

    .line 384
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v2, "[getCard] Invalid cardId."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    :cond_1
    :goto_0
    return-object v6

    .line 388
    :cond_2
    const/4 v6, 0x0

    .line 390
    .local v6, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    :try_start_0
    const-string v3, "channel_key=?"

    .line 391
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    .line 392
    .local v4, "whereArgs":[Ljava/lang/String;
    sget-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 393
    .local v1, "cardUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 394
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 395
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 396
    invoke-direct {p0, v7}, Lcom/samsung/android/magazine/cardchannel/CardManager;->createCard(Landroid/database/Cursor;)Lcom/samsung/android/magazine/cardchannel/Card;

    move-result-object v6

    .line 398
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 400
    .end local v1    # "cardUri":Landroid/net/Uri;
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 401
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v2, "[Exception] getCard"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    invoke-virtual {v8}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCards()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/Card;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/magazine/cardchannel/CardManager;->isRegistered(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    const-string v1, "CardChannel is not registered"

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 216
    .local v7, "cards":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    :try_start_0
    const-string v3, "channel_key=?"

    .line 217
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 218
    .local v4, "whereArgs":[Ljava/lang/String;
    const-string v5, "card._id ASC"

    .line 219
    .local v5, "sortOrder":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 220
    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    .line 219
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 222
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 223
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 227
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 234
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v5    # "sortOrder":Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_1
    :goto_1
    return-object v7

    .line 224
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v5    # "sortOrder":Ljava/lang/String;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :cond_2
    invoke-direct {p0, v8}, Lcom/samsung/android/magazine/cardchannel/CardManager;->createCard(Landroid/database/Cursor;)Lcom/samsung/android/magazine/cardchannel/Card;

    move-result-object v6

    .line 225
    .local v6, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 229
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v5    # "sortOrder":Ljava/lang/String;
    .end local v6    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 230
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getCards"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public getCards(II)Ljava/util/List;
    .locals 11
    .param p1, "startCardId"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/Card;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/magazine/cardchannel/CardManager;->isRegistered(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    const-string v1, "CardChannel is not registered"

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    .line 253
    .local v10, "startId":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 256
    .local v7, "cards":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    :try_start_0
    const-string v3, "channel_key=? AND card._id>=?"

    .line 258
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object v10, v4, v0

    .line 259
    .local v4, "whereArgs":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "card._id ASC LIMIT "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 260
    .local v5, "sortOrder":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 261
    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    .line 260
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 263
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 264
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 268
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 275
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v5    # "sortOrder":Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_1
    :goto_1
    return-object v7

    .line 265
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v5    # "sortOrder":Ljava/lang/String;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :cond_2
    invoke-direct {p0, v8}, Lcom/samsung/android/magazine/cardchannel/CardManager;->createCard(Landroid/database/Cursor;)Lcom/samsung/android/magazine/cardchannel/Card;

    move-result-object v6

    .line 266
    .local v6, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 270
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v5    # "sortOrder":Ljava/lang/String;
    .end local v6    # "card":Lcom/samsung/android/magazine/cardchannel/Card;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 271
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getCards"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public getCards(Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p1, "section"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/Card;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 287
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/magazine/cardchannel/CardManager;->isRegistered(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 288
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    const-string v1, "CardChannel is not registered"

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 292
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v1, "[getCards] Failed to get cards. section is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :cond_1
    :goto_0
    return-object v7

    .line 296
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 299
    .local v7, "cards":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    :try_start_0
    const-string v3, "channel_key=? AND section_key=?"

    .line 301
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    .line 302
    .local v4, "whereArgs":[Ljava/lang/String;
    const-string v5, "card._id ASC"

    .line 304
    .local v5, "sortOrder":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 305
    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    .line 304
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 307
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 308
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 312
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 314
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v5    # "sortOrder":Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 315
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getCards"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 309
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v5    # "sortOrder":Ljava/lang/String;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :cond_3
    :try_start_1
    invoke-direct {p0, v8}, Lcom/samsung/android/magazine/cardchannel/CardManager;->createCard(Landroid/database/Cursor;)Lcom/samsung/android/magazine/cardchannel/Card;

    move-result-object v6

    .line 310
    .local v6, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public getCards(Ljava/lang/String;II)Ljava/util/List;
    .locals 11
    .param p1, "section"    # Ljava/lang/String;
    .param p2, "startCardId"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/Card;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 333
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/magazine/cardchannel/CardManager;->isRegistered(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    const-string v1, "CardChannel is not registered"

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 337
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 338
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v1, "[getCards] Failed to get cards. section is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    :cond_1
    :goto_0
    return-object v7

    .line 342
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 343
    .local v7, "cards":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/magazine/cardchannel/Card;>;"
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    .line 346
    .local v10, "startId":Ljava/lang/String;
    :try_start_0
    const-string v3, "channel_key=? AND section_key=? AND card._id>=?"

    .line 349
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    const/4 v0, 0x2

    aput-object v10, v4, v0

    .line 350
    .local v4, "whereArgs":[Ljava/lang/String;
    const-string v0, "whereArgs"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "card._id ASC LIMIT "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 353
    .local v5, "sortOrder":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 354
    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    .line 353
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 355
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 356
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 360
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 362
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v5    # "sortOrder":Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 363
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getCards"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 357
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v5    # "sortOrder":Ljava/lang/String;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :cond_3
    :try_start_1
    invoke-direct {p0, v8}, Lcom/samsung/android/magazine/cardchannel/CardManager;->createCard(Landroid/database/Cursor;)Lcom/samsung/android/magazine/cardchannel/Card;

    move-result-object v6

    .line 358
    .local v6, "card":Lcom/samsung/android/magazine/cardchannel/Card;
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public removeCard(I)V
    .locals 6
    .param p1, "cardId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    .line 468
    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/magazine/cardchannel/CardManager;->isRegistered(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 469
    new-instance v3, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    const-string v4, "CardChannel is not registered"

    invoke-direct {v3, v4}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 471
    :cond_0
    if-gez p1, :cond_1

    .line 472
    sget-object v3, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v4, "[removeCard] Invalid cardId."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    :goto_0
    return-void

    .line 476
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardList:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 478
    sget-object v3, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 480
    .local v0, "cardUri":Landroid/net/Uri;
    if-eqz v0, :cond_2

    .line 481
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 482
    .local v1, "count":I
    sget-object v3, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[removeCard] deleted card count : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 486
    .end local v1    # "count":I
    :catch_0
    move-exception v2

    .line 487
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v4, "[Exception] removeCard"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 484
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v4, "[removeCard] cardUri is null."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public removeCardChangeListener(Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/samsung/android/magazine/cardchannel/CardManager$CardChangeListener;

    .prologue
    .line 441
    if-nez p1, :cond_1

    .line 442
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v3, "Failed to add listener. listener is null."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x40

    const/16 v4, 0x24

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 447
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardUpdateListeners:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardUpdateListeners:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardObserver:Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;

    if-eqz v2, :cond_0

    .line 451
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardObserver:Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 452
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardObserver:Lcom/samsung/android/magazine/cardchannel/CardManager$CardObserver;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 454
    :catch_0
    move-exception v0

    .line 455
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    const-string v3, "[Exception] removeCardChangeListener"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public requestCardRefresh(I)V
    .locals 6
    .param p1, "cardId"    # I

    .prologue
    .line 527
    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/magazine/cardchannel/CardManager;->isRegistered(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 528
    new-instance v3, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    const-string v4, "CardChannel is not registered"

    invoke-direct {v3, v4}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 530
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardList:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazine/cardchannel/CardManager$CardInfo;

    .line 531
    .local v0, "cardInfo":Lcom/samsung/android/magazine/cardchannel/CardManager$CardInfo;
    if-nez v0, :cond_2

    .line 532
    sget-object v3, Lcom/samsung/android/magazine/cardchannel/CardManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "invalid cardId : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    :cond_1
    :goto_0
    return-void

    .line 536
    :cond_2
    iget-object v2, v0, Lcom/samsung/android/magazine/cardchannel/CardManager$CardInfo;->provider:Ljava/lang/String;

    .line 537
    .local v2, "packageName":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 540
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.samsung.android.magazine.intent.action.CARD_REFRESH"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 541
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 542
    const-string v3, "com.samsung.android.magazine.intent.extra.CHANNEL_NAME"

    iget-object v4, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 543
    const-string v3, "com.samsung.android.magazine.intent.extra.CARD_ID"

    iget-object v4, v0, Lcom/samsung/android/magazine/cardchannel/CardManager$CardInfo;->cardIdStr:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 544
    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 545
    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.samsung.android.magazine.permission.READ_CARD_PROVIDER"

    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public requestCardRefresh(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 503
    .local p1, "cardProviders":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/magazine/cardchannel/CardManager;->isRegistered(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 504
    new-instance v3, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    const-string v4, "CardChannel is not registered"

    invoke-direct {v3, v4}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 507
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v0

    .line 508
    .local v0, "channelName":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 515
    return-void

    .line 508
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 509
    .local v2, "provider":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.samsung.android.magazine.intent.action.CARD_REFRESH"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 510
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 511
    const-string v4, "com.samsung.android.magazine.intent.extra.CHANNEL_NAME"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 512
    const/16 v4, 0x20

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 513
    iget-object v4, p0, Lcom/samsung/android/magazine/cardchannel/CardManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method
