.class public Lcom/samsung/android/magazine/cardchannel/CardElement;
.super Ljava/lang/Object;
.source "CardElement.java"


# instance fields
.field private mAction:Lcom/samsung/android/magazine/cardchannel/CardAction;

.field private mAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mKey:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardElement;->mKey:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardElement;->mAction:Lcom/samsung/android/magazine/cardchannel/CardAction;

    .line 26
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardElement;->mAttributes:Ljava/util/Map;

    .line 35
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardElement;->mKey:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public getAction()Lcom/samsung/android/magazine/cardchannel/CardAction;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardElement;->mAction:Lcom/samsung/android/magazine/cardchannel/CardAction;

    return-object v0
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardElement;->mAttributes:Ljava/util/Map;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardElement;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method setAction(Lcom/samsung/android/magazine/cardchannel/CardAction;)V
    .locals 0
    .param p1, "action"    # Lcom/samsung/android/magazine/cardchannel/CardAction;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardElement;->mAction:Lcom/samsung/android/magazine/cardchannel/CardAction;

    .line 57
    return-void
.end method

.method public setAttributes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardElement;->mAttributes:Ljava/util/Map;

    .line 88
    return-void
.end method
