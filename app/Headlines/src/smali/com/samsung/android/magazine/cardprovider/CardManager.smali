.class public Lcom/samsung/android/magazine/cardprovider/CardManager;
.super Ljava/lang/Object;
.source "CardManager.java"


# static fields
.field private static final CHANNEL_HEADLINES:Ljava/lang/String; = "headlines"

.field private static final PREFERENCE_KEY_REGISTRATION:Ljava/lang/String; = "MagazineCardProviderRegistered"

.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/samsung/android/magazine/cardprovider/CardManager;


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mPackageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/samsung/android/magazine/cardprovider/CardManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mInstance:Lcom/samsung/android/magazine/cardprovider/CardManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContext:Landroid/content/Context;

    .line 57
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 58
    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mPackageName:Ljava/lang/String;

    .line 61
    iput-object p1, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContext:Landroid/content/Context;

    .line 62
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 63
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mPackageName:Ljava/lang/String;

    .line 64
    return-void
.end method

.method private getImageUri(Landroid/net/Uri;Lcom/samsung/android/magazine/cardprovider/card/CardImage;)Ljava/lang/String;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "cardImage"    # Lcom/samsung/android/magazine/cardprovider/card/CardImage;

    .prologue
    .line 504
    invoke-virtual {p2}, Lcom/samsung/android/magazine/cardprovider/card/CardImage;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 505
    .local v2, "imageKey":Ljava/lang/String;
    invoke-static {p1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 506
    .local v4, "imageUri":Landroid/net/Uri;
    const/4 v1, 0x0

    .line 508
    .local v1, "cardImageUri":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/magazine/cardprovider/card/CardImage;->getImagePath()Ljava/lang/String;

    move-result-object v3

    .line 509
    .local v3, "imagePath":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 510
    invoke-direct {p0, v3, v4}, Lcom/samsung/android/magazine/cardprovider/CardManager;->sendImageToMagazine(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 519
    :cond_0
    :goto_0
    return-object v1

    .line 513
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/magazine/cardprovider/card/CardImage;->getImage()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 514
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 515
    invoke-direct {p0, v0, v4}, Lcom/samsung/android/magazine/cardprovider/CardManager;->sendImageToMagazine(Landroid/graphics/Bitmap;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/magazine/cardprovider/CardManager;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 78
    const-class v3, Lcom/samsung/android/magazine/cardprovider/CardManager;

    monitor-enter v3

    if-nez p0, :cond_0

    .line 79
    :try_start_0
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v5, "Invalid argument : context is null."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :goto_0
    monitor-exit v3

    return-object v2

    .line 83
    :cond_0
    const/4 v0, 0x0

    .line 86
    .local v0, "appContext":Landroid/content/Context;
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 91
    :try_start_2
    sget-object v2, Lcom/samsung/android/magazine/cardprovider/CardManager;->mInstance:Lcom/samsung/android/magazine/cardprovider/CardManager;

    if-nez v2, :cond_1

    .line 92
    new-instance v2, Lcom/samsung/android/magazine/cardprovider/CardManager;

    invoke-direct {v2, v0}, Lcom/samsung/android/magazine/cardprovider/CardManager;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/samsung/android/magazine/cardprovider/CardManager;->mInstance:Lcom/samsung/android/magazine/cardprovider/CardManager;

    .line 95
    :cond_1
    sget-object v2, Lcom/samsung/android/magazine/cardprovider/CardManager;->mInstance:Lcom/samsung/android/magazine/cardprovider/CardManager;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 87
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 78
    .end local v0    # "appContext":Landroid/content/Context;
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private insertCardElements(Landroid/net/Uri;Lcom/samsung/android/magazine/cardprovider/card/Card;)Z
    .locals 26
    .param p1, "cardUri"    # Landroid/net/Uri;
    .param p2, "card"    # Lcom/samsung/android/magazine/cardprovider/card/Card;

    .prologue
    .line 523
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v18

    .line 524
    .local v18, "strId":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    .line 525
    .local v14, "id":I
    const/16 v19, 0x0

    .line 527
    .local v19, "success":Z
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 530
    .local v21, "valueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardActionKeys()Ljava/util/Set;

    move-result-object v7

    .line 531
    .local v7, "actionKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-nez v23, :cond_2

    .line 552
    const-string v22, "image"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    .line 553
    .local v15, "imageUri":Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardElements()Ljava/util/Collection;

    move-result-object v11

    .line 554
    .local v11, "cardElementList":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/magazine/cardprovider/card/CardElement;>;"
    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :cond_0
    :goto_1
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-nez v22, :cond_5

    .line 606
    :try_start_0
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v22

    if-lez v22, :cond_1

    .line 607
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$CardElement;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    new-array v0, v0, [Landroid/content/ContentValues;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v22

    check-cast v22, [Landroid/content/ContentValues;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v17

    .line 608
    .local v17, "result":I
    if-lez v17, :cond_1

    .line 609
    const/16 v19, 0x1

    .line 617
    .end local v17    # "result":I
    :cond_1
    :goto_2
    return v19

    .line 531
    .end local v11    # "cardElementList":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/magazine/cardprovider/card/CardElement;>;"
    .end local v15    # "imageUri":Landroid/net/Uri;
    :cond_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 532
    .local v6, "actionKey":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardAction(Ljava/lang/String;)Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    move-result-object v9

    .line 533
    .local v9, "cardAction":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 534
    .local v12, "contentValues":Landroid/content/ContentValues;
    const-string v23, "card_id"

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 535
    const-string v23, "key"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    const-string v23, "type"

    const-string v24, "action"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    const-string v23, "action_type"

    invoke-virtual {v9}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getActionType()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    const-string v23, "action_label"

    invoke-virtual {v9}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getLabel()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    invoke-virtual {v9}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getData()Landroid/content/Intent;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v5

    .line 540
    .local v5, "actionData":Ljava/lang/String;
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v23

    if-lez v23, :cond_3

    .line 541
    const-string v23, "action_data"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    :cond_3
    invoke-virtual {v9}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getAttributes()Ljava/util/Map;

    move-result-object v8

    .line 544
    .local v8, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v8, :cond_4

    invoke-interface {v8}, Ljava/util/Map;->isEmpty()Z

    move-result v23

    if-nez v23, :cond_4

    .line 545
    const-string v23, "action_attributes"

    .line 546
    invoke-static {v8}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromMap(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v24

    .line 545
    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    :cond_4
    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 554
    .end local v5    # "actionData":Ljava/lang/String;
    .end local v6    # "actionKey":Ljava/lang/String;
    .end local v8    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v9    # "cardAction":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    .end local v12    # "contentValues":Landroid/content/ContentValues;
    .restart local v11    # "cardElementList":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/magazine/cardprovider/card/CardElement;>;"
    .restart local v15    # "imageUri":Landroid/net/Uri;
    :cond_5
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/magazine/cardprovider/card/CardElement;

    .line 555
    .local v10, "cardElement":Lcom/samsung/android/magazine/cardprovider/card/CardElement;
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 556
    .restart local v12    # "contentValues":Landroid/content/ContentValues;
    const-string v22, "card_id"

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 557
    const-string v22, "key"

    invoke-virtual {v10}, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->getKey()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    instance-of v0, v10, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    move/from16 v22, v0

    if-eqz v22, :cond_a

    .line 560
    const-string v22, "type"

    const-string v24, "text"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v22, v10

    .line 561
    check-cast v22, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/magazine/cardprovider/card/CardText;->getText()Ljava/lang/String;

    move-result-object v20

    .line 562
    .local v20, "text":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 563
    sget-object v22, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "CAN NOT insert a card element. Text of CardText is empty. (key: "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->getKey()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ")"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 566
    :cond_6
    const-string v22, "data1"

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    .end local v20    # "text":Ljava/lang/String;
    :goto_3
    invoke-virtual {v10}, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->getAttributes()Ljava/util/Map;

    move-result-object v8

    .line 582
    .restart local v8    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v8, :cond_7

    invoke-interface {v8}, Ljava/util/Map;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_7

    .line 583
    const-string v22, "attributes"

    .line 584
    invoke-static {v8}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromMap(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v24

    .line 583
    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    :cond_7
    invoke-virtual {v10}, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->getAction()Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    move-result-object v3

    .line 588
    .local v3, "action":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    if-eqz v3, :cond_9

    .line 589
    const-string v22, "action_type"

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getActionType()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    const-string v22, "action_label"

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getLabel()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getData()Landroid/content/Intent;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v5

    .line 592
    .restart local v5    # "actionData":Ljava/lang/String;
    if-eqz v5, :cond_8

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v22

    if-lez v22, :cond_8

    .line 593
    const-string v22, "action_data"

    move-object/from16 v0, v22

    invoke-virtual {v12, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    :cond_8
    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getAttributes()Ljava/util/Map;

    move-result-object v4

    .line 596
    .local v4, "actionAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v4, :cond_9

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_9

    .line 597
    const-string v22, "action_attributes"

    .line 598
    invoke-static {v4}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromMap(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v24

    .line 597
    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    .end local v4    # "actionAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "actionData":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 568
    .end local v3    # "action":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    .end local v8    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_a
    instance-of v0, v10, Lcom/samsung/android/magazine/cardprovider/card/CardImage;

    move/from16 v22, v0

    if-eqz v22, :cond_0

    .line 569
    const-string v22, "type"

    const-string v24, "image"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v22, v10

    .line 570
    check-cast v22, Lcom/samsung/android/magazine/cardprovider/card/CardImage;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v15, v1}, Lcom/samsung/android/magazine/cardprovider/CardManager;->getImageUri(Landroid/net/Uri;Lcom/samsung/android/magazine/cardprovider/card/CardImage;)Ljava/lang/String;

    move-result-object v16

    .line 571
    .local v16, "imageUriString":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 572
    sget-object v22, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "CAN NOT insert a card element. image of CardImage is invalid. (key: "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->getKey()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ")"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 575
    :cond_b
    const-string v22, "data1"

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 612
    .end local v10    # "cardElement":Lcom/samsung/android/magazine/cardprovider/card/CardElement;
    .end local v12    # "contentValues":Landroid/content/ContentValues;
    .end local v16    # "imageUriString":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 613
    .local v13, "e":Ljava/lang/IllegalArgumentException;
    sget-object v22, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v23, "[Exception] insertCardElements"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    invoke-virtual {v13}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_2
.end method

.method private isCardSubscribed(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p1, "cardName"    # Ljava/lang/String;
    .param p2, "channelName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 742
    sget-object v10, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v10

    .line 743
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 744
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v1, "[isCardNameSubscribed] Invalid argument. cardName is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    monitor-exit v10

    move v0, v8

    .line 787
    :goto_0
    return v0

    .line 748
    :cond_0
    const/4 v3, 0x0

    .line 749
    .local v3, "where":Ljava/lang/String;
    const/4 v4, 0x0

    .line 750
    .local v4, "whereArgs":[Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 751
    const-string v3, "card_type_key=? AND enable=1"

    .line 753
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 762
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    :goto_1
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 763
    const-string v1, "card_type_key"

    aput-object v1, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 766
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 768
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Setting;->CONTENT_URI:Landroid/net/Uri;

    .line 769
    const/4 v5, 0x0

    .line 768
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 775
    :goto_2
    if-eqz v6, :cond_1

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v0, v9, :cond_4

    .line 776
    :cond_1
    if-eqz v6, :cond_2

    .line 777
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 779
    :cond_2
    monitor-exit v10

    move v0, v8

    goto :goto_0

    .line 756
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_3
    const-string v3, "card_type_key=? AND enable=1 AND channel_key=?"

    .line 759
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v0, 0x1

    aput-object p2, v4, v0

    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 770
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 771
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] isCardSubscribed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 742
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 782
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_4
    if-eqz v6, :cond_5

    .line 783
    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 742
    :cond_5
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v9

    .line 787
    goto :goto_0
.end method

.method private isRegistered()Z
    .locals 3

    .prologue
    .line 883
    iget-object v1, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 884
    .local v0, "preferences":Landroid/content/SharedPreferences;
    const-string v1, "MagazineCardProviderRegistered"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private isValidCard(Lcom/samsung/android/magazine/cardprovider/card/Card;)Z
    .locals 3
    .param p1, "card"    # Lcom/samsung/android/magazine/cardprovider/card/Card;

    .prologue
    const/4 v0, 0x0

    .line 621
    if-nez p1, :cond_0

    .line 622
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v2, "Invalid argument. Card is null."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    :goto_0
    return v0

    .line 626
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 627
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed to post card. Card id is empty."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 631
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 632
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed to post card. Card name is empty."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 636
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getTemplate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 637
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed to post card. Card template is empty."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 641
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardElements()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    if-nez v1, :cond_4

    .line 642
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed to post card. Card elements is empty."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 646
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private postCardInner(Lcom/samsung/android/magazine/cardprovider/card/Card;Ljava/lang/String;)Z
    .locals 18
    .param p1, "card"    # Lcom/samsung/android/magazine/cardprovider/card/Card;
    .param p2, "channel"    # Ljava/lang/String;

    .prologue
    .line 650
    const/4 v10, 0x1

    .line 652
    .local v10, "success":Z
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getTemplate()Ljava/lang/String;

    move-result-object v11

    .line 654
    .local v11, "templateId":Ljava/lang/String;
    sget-object v14, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v14

    .line 655
    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 656
    .local v3, "contentValues":Landroid/content/ContentValues;
    const-string v13, "card_key"

    .line 657
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardId()Ljava/lang/String;

    move-result-object v15

    .line 656
    invoke-virtual {v3, v13, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    const-string v13, "card_type"

    .line 659
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardName()Ljava/lang/String;

    move-result-object v15

    .line 658
    invoke-virtual {v3, v13, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    const-string v13, "template_type"

    invoke-virtual {v3, v13, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    const-string v13, "card_provider"

    .line 663
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mPackageName:Ljava/lang/String;

    .line 662
    invoke-virtual {v3, v13, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    .line 665
    const-string v13, "card_channel"

    move-object/from16 v0, p2

    invoke-virtual {v3, v13, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getAttributes()Ljava/util/Map;

    move-result-object v2

    .line 669
    .local v2, "cardAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_1

    .line 670
    const-string v13, "attributes"

    .line 671
    invoke-static {v2}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromMap(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v15

    .line 670
    invoke-virtual {v3, v13, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 675
    .local v4, "currentTime":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getExpirationTime()J

    move-result-wide v8

    .line 676
    .local v8, "expirationTime":J
    const-wide/16 v16, 0x0

    cmp-long v13, v8, v16

    if-lez v13, :cond_3

    .line 679
    cmp-long v13, v4, v8

    if-ltz v13, :cond_2

    .line 680
    sget-object v13, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v15, "ExpirationTime is invalid."

    invoke-static {v13, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    monitor-exit v14

    const/4 v13, 0x0

    .line 718
    :goto_0
    return v13

    .line 683
    :cond_2
    const-string v13, "expiration_time"

    .line 684
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 683
    invoke-virtual {v3, v13, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 697
    :cond_3
    const-string v13, "time_stamp"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v3, v13, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 699
    const/4 v12, 0x0

    .line 701
    .local v12, "uri":Landroid/net/Uri;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 702
    sget-object v15, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Card;->CONTENT_URI:Landroid/net/Uri;

    .line 701
    invoke-virtual {v13, v15, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v12

    .line 708
    :goto_1
    if-nez v12, :cond_5

    .line 709
    :try_start_2
    sget-object v13, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v15, "Failed to insert card."

    invoke-static {v13, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    const/4 v10, 0x0

    .line 654
    :cond_4
    :goto_2
    monitor-exit v14

    move v13, v10

    .line 718
    goto :goto_0

    .line 703
    :catch_0
    move-exception v6

    .line 704
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    sget-object v13, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v15, "[Exception] postCardInner"

    invoke-static {v13, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 654
    .end local v2    # "cardAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "contentValues":Landroid/content/ContentValues;
    .end local v4    # "currentTime":J
    .end local v6    # "e":Ljava/lang/IllegalArgumentException;
    .end local v8    # "expirationTime":J
    .end local v12    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v13

    monitor-exit v14
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v13

    .line 712
    .restart local v2    # "cardAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v3    # "contentValues":Landroid/content/ContentValues;
    .restart local v4    # "currentTime":J
    .restart local v8    # "expirationTime":J
    .restart local v12    # "uri":Landroid/net/Uri;
    :cond_5
    :try_start_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v12, v1}, Lcom/samsung/android/magazine/cardprovider/CardManager;->insertCardElements(Landroid/net/Uri;Lcom/samsung/android/magazine/cardprovider/card/Card;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v7

    .line 713
    .local v7, "result":Z
    if-nez v7, :cond_4

    .line 714
    const/4 v10, 0x0

    goto :goto_2
.end method

.method private removeCardInner(Ljava/lang/String;)I
    .locals 7
    .param p1, "cardId"    # Ljava/lang/String;

    .prologue
    .line 723
    const/4 v1, 0x0

    .line 725
    .local v1, "result":I
    :try_start_0
    sget-object v5, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 726
    :try_start_1
    const-string v2, "card_key=? AND card_provider=?"

    .line 728
    .local v2, "where":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v3, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 729
    aput-object p1, v3, v4

    const/4 v4, 0x1

    iget-object v6, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mPackageName:Ljava/lang/String;

    aput-object v6, v3, v4

    .line 731
    .local v3, "whereArgs":[Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Card;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v6, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 725
    monitor-exit v5

    .line 738
    .end local v2    # "where":Ljava/lang/String;
    .end local v3    # "whereArgs":[Ljava/lang/String;
    :goto_0
    return v1

    .line 725
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    .line 733
    :catch_0
    move-exception v0

    .line 734
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v5, "[Exception] removeCardInner"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method private sendImageToMagazine(Landroid/graphics/Bitmap;Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 792
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 793
    .local v1, "imageFileName":Ljava/lang/String;
    invoke-static {p2, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 794
    .local v3, "uri":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 797
    .local v2, "outStream":Ljava/io/OutputStream;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v4, v3}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v2

    .line 798
    if-eqz v2, :cond_0

    .line 799
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {p1, v4, v5, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 800
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 810
    :cond_0
    if-eqz v2, :cond_1

    .line 811
    :try_start_1
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 817
    :cond_1
    :goto_0
    if-nez v3, :cond_3

    .line 818
    const/4 v4, 0x0

    .line 820
    :goto_1
    return-object v4

    .line 802
    :catch_0
    move-exception v0

    .line 803
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 804
    const/4 v3, 0x0

    .line 810
    if-eqz v2, :cond_1

    .line 811
    :try_start_3
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 813
    :catch_1
    move-exception v4

    goto :goto_0

    .line 805
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 806
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_4
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v5, "[Exception] sendImageToMagazine"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 810
    if-eqz v2, :cond_1

    .line 811
    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_0

    .line 813
    :catch_3
    move-exception v4

    goto :goto_0

    .line 808
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v4

    .line 810
    if-eqz v2, :cond_2

    .line 811
    :try_start_6
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 815
    :cond_2
    :goto_2
    throw v4

    .line 820
    :cond_3
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 813
    :catch_4
    move-exception v5

    goto :goto_2

    :catch_5
    move-exception v4

    goto :goto_0
.end method

.method private sendImageToMagazine(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;
    .locals 11
    .param p1, "imagePath"    # Ljava/lang/String;
    .param p2, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 825
    if-eqz p1, :cond_0

    .line 826
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "content://"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 879
    .end local p1    # "imagePath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 830
    .restart local p1    # "imagePath":Ljava/lang/String;
    :cond_1
    const/4 v8, 0x0

    .line 831
    .local v8, "uri":Landroid/net/Uri;
    const-string v9, ""

    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 832
    const-string v9, "/"

    invoke-virtual {p1, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    .line 833
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    .line 832
    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 835
    .local v3, "imageFileName":Ljava/lang/String;
    invoke-static {p2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 836
    const/4 v6, 0x0

    .line 837
    .local v6, "os":Ljava/io/OutputStream;
    const/4 v4, 0x0

    .line 838
    .local v4, "is":Ljava/io/InputStream;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 841
    .local v2, "file":Ljava/io/File;
    :try_start_0
    iget-object v9, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v6

    .line 843
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 845
    .end local v4    # "is":Ljava/io/InputStream;
    .local v5, "is":Ljava/io/InputStream;
    const/high16 v9, 0x20000

    :try_start_1
    new-array v0, v9, [B

    .line 848
    .local v0, "buffer":[B
    if-eqz v6, :cond_6

    .line 849
    :goto_1
    invoke-virtual {v5, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    .local v7, "read":I
    const/4 v9, -0x1

    if-ne v7, v9, :cond_4

    .line 852
    invoke-virtual {v6}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 865
    .end local v7    # "read":I
    :goto_2
    if-eqz v5, :cond_2

    .line 866
    :try_start_2
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 868
    :cond_2
    if-eqz v6, :cond_3

    .line 869
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 876
    .end local v0    # "buffer":[B
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "imageFileName":Ljava/lang/String;
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v6    # "os":Ljava/io/OutputStream;
    :cond_3
    :goto_3
    if-nez v8, :cond_a

    .line 877
    const/4 p1, 0x0

    goto :goto_0

    .line 850
    .restart local v0    # "buffer":[B
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "imageFileName":Ljava/lang/String;
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v6    # "os":Ljava/io/OutputStream;
    .restart local v7    # "read":I
    :cond_4
    const/4 v9, 0x0

    :try_start_3
    invoke-virtual {v6, v0, v9, v7}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 857
    .end local v0    # "buffer":[B
    .end local v7    # "read":I
    :catch_0
    move-exception v1

    move-object v4, v5

    .line 858
    .end local v5    # "is":Ljava/io/InputStream;
    .local v1, "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "is":Ljava/io/InputStream;
    :goto_4
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 859
    const/4 v8, 0x0

    .line 865
    if-eqz v4, :cond_5

    .line 866
    :try_start_5
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 868
    :cond_5
    if-eqz v6, :cond_3

    .line 869
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_3

    .line 871
    :catch_1
    move-exception v9

    goto :goto_3

    .line 854
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v0    # "buffer":[B
    .restart local v5    # "is":Ljava/io/InputStream;
    :cond_6
    :try_start_6
    sget-object v9, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v10, "Sending image error : Output stream is null"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 855
    const/4 v8, 0x0

    goto :goto_2

    .line 860
    .end local v0    # "buffer":[B
    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    :catch_2
    move-exception v1

    .line 861
    .local v1, "e":Ljava/io/IOException;
    :goto_5
    :try_start_7
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 862
    const/4 v8, 0x0

    .line 865
    if-eqz v4, :cond_7

    .line 866
    :try_start_8
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 868
    :cond_7
    if-eqz v6, :cond_3

    .line 869
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_3

    .line 871
    :catch_3
    move-exception v9

    goto :goto_3

    .line 863
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    .line 865
    :goto_6
    if-eqz v4, :cond_8

    .line 866
    :try_start_9
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 868
    :cond_8
    if-eqz v6, :cond_9

    .line 869
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 873
    :cond_9
    :goto_7
    throw v9

    .line 879
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "imageFileName":Ljava/lang/String;
    .end local v4    # "is":Ljava/io/InputStream;
    .end local v6    # "os":Ljava/io/OutputStream;
    :cond_a
    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 871
    .restart local v0    # "buffer":[B
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "imageFileName":Ljava/lang/String;
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v6    # "os":Ljava/io/OutputStream;
    :catch_4
    move-exception v9

    goto :goto_3

    .end local v0    # "buffer":[B
    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    :catch_5
    move-exception v10

    goto :goto_7

    .line 863
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    :catchall_1
    move-exception v9

    move-object v4, v5

    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    goto :goto_6

    .line 860
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    :catch_6
    move-exception v1

    move-object v4, v5

    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    goto :goto_5

    .line 857
    :catch_7
    move-exception v1

    goto :goto_4
.end method


# virtual methods
.method public containsCard(Ljava/lang/String;)Z
    .locals 10
    .param p1, "cardId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 108
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/magazine/cardprovider/CardManager;->isRegistered()Z

    move-result v0

    if-nez v0, :cond_1

    .line 109
    :cond_0
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v1, "CardProvider is not registered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :goto_0
    return v6

    .line 113
    :cond_1
    const/4 v6, 0x0

    .line 115
    .local v6, "contain":Z
    sget-object v9, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v9

    .line 117
    :try_start_0
    const-string v3, "card_key=? AND card_provider=?"

    .line 119
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mPackageName:Ljava/lang/String;

    aput-object v1, v4, v0

    .line 120
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    .local v2, "projection":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 124
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 125
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Card;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    .line 124
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 131
    :goto_1
    if-eqz v7, :cond_3

    .line 132
    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    const/4 v6, 0x1

    .line 135
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 115
    :cond_3
    monitor-exit v9

    goto :goto_0

    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 126
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 127
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] containsCard"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-virtual {v8}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public getAllPostedCards()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 149
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/magazine/cardprovider/CardManager;->isRegistered()Z

    move-result v0

    if-nez v0, :cond_1

    .line 150
    :cond_0
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v1, "CardProvider is not registered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :goto_0
    return-object v7

    .line 154
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v7, "cardIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v10, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v10

    .line 158
    :try_start_0
    const-string v3, "card_provider=?"

    .line 159
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mPackageName:Ljava/lang/String;

    aput-object v1, v4, v0

    .line 160
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "card_key"

    aput-object v1, v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    .local v2, "projection":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 164
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 165
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Card;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    .line 164
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 171
    :goto_1
    if-eqz v8, :cond_2

    .line 172
    :goto_2
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 176
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 156
    :cond_2
    monitor-exit v10

    goto :goto_0

    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 166
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 167
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getAllPostedCards"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 173
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 174
    .local v6, "cardId":Ljava/lang/String;
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method public postCard(Lcom/samsung/android/magazine/cardprovider/card/Card;)Z
    .locals 3
    .param p1, "card"    # Lcom/samsung/android/magazine/cardprovider/card/Card;

    .prologue
    const/4 v0, 0x0

    .line 195
    iget-object v1, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/magazine/cardprovider/CardManager;->isRegistered()Z

    move-result v1

    if-nez v1, :cond_2

    .line 196
    :cond_0
    sget-object v1, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v2, "CardProvider is not registered"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_1
    :goto_0
    return v0

    .line 200
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/magazine/cardprovider/CardManager;->isValidCard(Lcom/samsung/android/magazine/cardprovider/card/Card;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 204
    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "headlines"

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/magazine/cardprovider/CardManager;->isCardSubscribed(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 208
    invoke-virtual {p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/magazine/cardprovider/CardManager;->removeCardInner(Ljava/lang/String;)I

    .line 209
    const-string v0, "headlines"

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/magazine/cardprovider/CardManager;->postCardInner(Lcom/samsung/android/magazine/cardprovider/card/Card;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public removeAllCards()Z
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 220
    iget-object v6, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContext:Landroid/content/Context;

    if-eqz v6, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/magazine/cardprovider/CardManager;->isRegistered()Z

    move-result v6

    if-nez v6, :cond_1

    .line 221
    :cond_0
    sget-object v5, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v6, "CardProvider is not registered"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    :goto_0
    return v4

    .line 226
    :cond_1
    sget-object v6, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v6

    .line 227
    :try_start_0
    const-string v2, "card_provider=?"

    .line 228
    .local v2, "where":Ljava/lang/String;
    const/4 v7, 0x1

    new-array v3, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mPackageName:Ljava/lang/String;

    aput-object v8, v3, v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    .local v3, "whereArgs":[Ljava/lang/String;
    const/4 v1, -0x1

    .line 232
    .local v1, "rowCount":I
    :try_start_1
    iget-object v7, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 233
    sget-object v8, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Card;->CONTENT_URI:Landroid/net/Uri;

    .line 232
    invoke-virtual {v7, v8, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 234
    sget-object v7, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "removeAllCards: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " cards are deleted."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 240
    :goto_1
    if-gtz v1, :cond_2

    .line 241
    :try_start_2
    monitor-exit v6

    goto :goto_0

    .line 226
    .end local v1    # "rowCount":I
    .end local v2    # "where":Ljava/lang/String;
    .end local v3    # "whereArgs":[Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 235
    .restart local v1    # "rowCount":I
    .restart local v2    # "where":Ljava/lang/String;
    .restart local v3    # "whereArgs":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 236
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    sget-object v7, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v8, "[Exception] removeAllCards"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 226
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_2
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v4, v5

    .line 245
    goto :goto_0
.end method

.method public removeCard(Ljava/lang/String;)Z
    .locals 4
    .param p1, "cardId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 257
    iget-object v2, p0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/magazine/cardprovider/CardManager;->isRegistered()Z

    move-result v2

    if-nez v2, :cond_1

    .line 258
    :cond_0
    sget-object v2, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v3, "CardProvider is not registered"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :goto_0
    return v1

    .line 262
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 263
    sget-object v2, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v3, "Failed to remove a card. cardId is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 267
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/magazine/cardprovider/CardManager;->removeCardInner(Ljava/lang/String;)I

    move-result v0

    .line 268
    .local v0, "rowCount":I
    if-gtz v0, :cond_3

    .line 269
    sget-object v2, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v3, "Failed to remove card."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 273
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public updateCard(Lcom/samsung/android/magazine/cardprovider/card/Card;)Z
    .locals 46
    .param p1, "card"    # Lcom/samsung/android/magazine/cardprovider/card/Card;

    .prologue
    .line 287
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/magazine/cardprovider/CardManager;->isRegistered()Z

    move-result v4

    if-nez v4, :cond_1

    .line 288
    :cond_0
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v5, "CardProvider is not registered"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    const/16 v38, 0x0

    .line 500
    :goto_0
    return v38

    .line 292
    :cond_1
    if-nez p1, :cond_2

    .line 293
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v5, "Invalid argument. Card is null."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/16 v38, 0x0

    goto :goto_0

    .line 297
    :cond_2
    const/16 v38, 0x1

    .line 300
    .local v38, "success":Z
    :try_start_0
    sget-object v43, Lcom/samsung/android/magazine/cardprovider/ConfigurationManager;->mLock:Ljava/lang/Object;

    monitor-enter v43
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardId()Ljava/lang/String;

    move-result-object v22

    .line 302
    .local v22, "cardKey":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 303
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v5, "Failed to update card. CardId is empty."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    const/16 v38, 0x0

    .line 300
    :cond_3
    :goto_1
    monitor-exit v43

    goto :goto_0

    .end local v22    # "cardKey":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit v43
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    .line 495
    :catch_0
    move-exception v28

    .line 496
    .local v28, "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v5, "[Exception] updateCard"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    invoke-virtual/range {v28 .. v28}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 307
    .end local v28    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v22    # "cardKey":Ljava/lang/String;
    :cond_4
    :try_start_3
    new-instance v34, Ljava/util/ArrayList;

    invoke-direct/range {v34 .. v34}, Ljava/util/ArrayList;-><init>()V

    .line 309
    .local v34, "idList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v7, "card_key=?"

    .line 310
    .local v7, "selection":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v22, v8, v4

    .line 311
    .local v8, "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v6, v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 313
    .local v6, "projection":[Ljava/lang/String;
    const/16 v19, 0x0

    .line 315
    .local v19, "cardCursor":Landroid/database/Cursor;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Card;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v19

    .line 321
    :goto_2
    if-eqz v19, :cond_5

    .line 322
    :goto_3
    :try_start_5
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_c

    .line 326
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 329
    :cond_5
    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1a

    .line 331
    new-instance v23, Landroid/content/ContentValues;

    invoke-direct/range {v23 .. v23}, Landroid/content/ContentValues;-><init>()V

    .line 332
    .local v23, "cardValues":Landroid/content/ContentValues;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getTemplate()Ljava/lang/String;

    move-result-object v39

    .line 334
    .local v39, "templateId":Ljava/lang/String;
    invoke-static/range {v39 .. v39}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 335
    const-string v4, "template_type"

    move-object/from16 v0, v23

    move-object/from16 v1, v39

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getAttributes()Ljava/util/Map;

    move-result-object v18

    .line 339
    .local v18, "cardAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v18, :cond_7

    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    .line 340
    const-string v4, "attributes"

    invoke-static/range {v18 .. v18}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromMap(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    .line 344
    .local v26, "currentTime":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getExpirationTime()J

    move-result-wide v30

    .line 345
    .local v30, "expirationTime":J
    const-wide/16 v4, 0x0

    cmp-long v4, v30, v4

    if-lez v4, :cond_8

    .line 347
    cmp-long v4, v26, v30

    if-gez v4, :cond_d

    .line 348
    const-string v4, "expiration_time"

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 355
    :cond_8
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardElements()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardActionKeys()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_e

    const/16 v32, 0x1

    .line 357
    .local v32, "hasNoElement":Z
    :goto_5
    invoke-virtual/range {v23 .. v23}, Landroid/content/ContentValues;->size()I

    move-result v4

    if-lez v4, :cond_9

    .line 358
    if-eqz v32, :cond_f

    .line 359
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Card;->CONTENT_URI_NO_ELEMENT:Landroid/net/Uri;

    move-object/from16 v0, v23

    invoke-virtual {v4, v5, v0, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v40

    .line 360
    .local v40, "updatedCount":I
    const/4 v4, 0x1

    move/from16 v0, v40

    if-ge v0, v4, :cond_9

    .line 361
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v5, "updateCard: Failed to update card."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    const/16 v38, 0x0

    .line 374
    .end local v40    # "updatedCount":I
    :cond_9
    :goto_6
    if-eqz v38, :cond_3

    if-nez v32, :cond_3

    .line 377
    const-string v15, "card_id=? AND key=? AND type=?"

    .line 381
    .local v15, "actionWhere":Ljava/lang/String;
    const-string v29, "card_id=? AND key=? AND (type=? OR type=?)"

    .line 386
    .local v29, "elementWhere":Ljava/lang/String;
    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_a
    :goto_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Ljava/lang/String;

    .line 387
    .local v33, "id":Ljava/lang/String;
    new-instance v41, Ljava/util/ArrayList;

    invoke-direct/range {v41 .. v41}, Ljava/util/ArrayList;-><init>()V

    .line 389
    .local v41, "valueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardActionKeys()Ljava/util/Set;

    move-result-object v14

    .line 390
    .local v14, "actionKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_10

    .line 422
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Card;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v33

    invoke-static {v4, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v35

    .line 423
    .local v35, "imageUri":Landroid/net/Uri;
    const-string v4, "image"

    move-object/from16 v0, v35

    invoke-static {v0, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v35

    .line 424
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardElements()Ljava/util/Collection;

    move-result-object v21

    .line 425
    .local v21, "cardElementList":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/magazine/cardprovider/card/CardElement;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_b
    :goto_9
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_14

    .line 481
    invoke-virtual/range {v41 .. v41}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_a

    .line 482
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v44, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$CardElement;->CONTENT_URI_UPDATE_CARD:Landroid/net/Uri;

    invoke-virtual/range {v41 .. v41}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Landroid/content/ContentValues;

    move-object/from16 v0, v41

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/content/ContentValues;

    move-object/from16 v0, v44

    invoke-virtual {v9, v0, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v37

    .line 483
    .local v37, "result":I
    if-gtz v37, :cond_a

    .line 484
    const/16 v38, 0x0

    goto :goto_7

    .line 316
    .end local v14    # "actionKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v15    # "actionWhere":Ljava/lang/String;
    .end local v18    # "cardAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v21    # "cardElementList":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/magazine/cardprovider/card/CardElement;>;"
    .end local v23    # "cardValues":Landroid/content/ContentValues;
    .end local v26    # "currentTime":J
    .end local v29    # "elementWhere":Ljava/lang/String;
    .end local v30    # "expirationTime":J
    .end local v32    # "hasNoElement":Z
    .end local v33    # "id":Ljava/lang/String;
    .end local v35    # "imageUri":Landroid/net/Uri;
    .end local v37    # "result":I
    .end local v39    # "templateId":Ljava/lang/String;
    .end local v41    # "valueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :catch_1
    move-exception v28

    .line 317
    .restart local v28    # "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v5, "[Exception] updateCard"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    invoke-virtual/range {v28 .. v28}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_2

    .line 323
    .end local v28    # "e":Ljava/lang/IllegalArgumentException;
    :cond_c
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 324
    .local v33, "id":I
    invoke-static/range {v33 .. v33}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 351
    .end local v33    # "id":I
    .restart local v18    # "cardAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v23    # "cardValues":Landroid/content/ContentValues;
    .restart local v26    # "currentTime":J
    .restart local v30    # "expirationTime":J
    .restart local v39    # "templateId":Ljava/lang/String;
    :cond_d
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v5, "ExpirationTime is invalid."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 355
    :cond_e
    const/16 v32, 0x0

    goto/16 :goto_5

    .line 366
    .restart local v32    # "hasNoElement":Z
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$Card;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v23

    invoke-virtual {v4, v5, v0, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v40

    .line 367
    .restart local v40    # "updatedCount":I
    const/4 v4, 0x1

    move/from16 v0, v40

    if-ge v0, v4, :cond_9

    .line 368
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    const-string v5, "updateCard: Failed to update card."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    const/16 v38, 0x0

    goto/16 :goto_6

    .line 390
    .end local v40    # "updatedCount":I
    .restart local v14    # "actionKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v15    # "actionWhere":Ljava/lang/String;
    .restart local v29    # "elementWhere":Ljava/lang/String;
    .local v33, "id":Ljava/lang/String;
    .restart local v41    # "valueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_10
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 392
    .local v13, "actionKey":Ljava/lang/String;
    const/4 v9, 0x3

    new-array v0, v9, [Ljava/lang/String;

    move-object/from16 v42, v0

    const/4 v9, 0x0

    .line 393
    aput-object v33, v42, v9

    const/4 v9, 0x1

    .line 394
    aput-object v13, v42, v9

    const/4 v9, 0x2

    .line 395
    const-string v44, "action"

    aput-object v44, v42, v9

    .line 397
    .local v42, "whereArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v44, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$CardElement;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v44

    move-object/from16 v1, v42

    invoke-virtual {v9, v0, v15, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v25

    .line 398
    .local v25, "deletedCount":I
    if-lez v25, :cond_11

    .line 399
    sget-object v9, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    new-instance v44, Ljava/lang/StringBuilder;

    const-string v45, "updateCard: Existed card action is deleted. key: "

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v44

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-static {v9, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    :cond_11
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/magazine/cardprovider/card/Card;->getCardAction(Ljava/lang/String;)Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    move-result-object v17

    .line 403
    .local v17, "cardAction":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    new-instance v24, Landroid/content/ContentValues;

    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    .line 404
    .local v24, "contentValues":Landroid/content/ContentValues;
    const-string v9, "card_id"

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v44

    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 405
    const-string v9, "key"

    move-object/from16 v0, v24

    invoke-virtual {v0, v9, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    const-string v9, "type"

    const-string v44, "action"

    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string v9, "action_type"

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getActionType()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const-string v9, "action_label"

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getLabel()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getData()Landroid/content/Intent;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v12

    .line 410
    .local v12, "actionData":Ljava/lang/String;
    if-eqz v12, :cond_12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_12

    .line 411
    const-string v9, "action_data"

    move-object/from16 v0, v24

    invoke-virtual {v0, v9, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_12
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getAttributes()Ljava/util/Map;

    move-result-object v11

    .line 413
    .local v11, "actionAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v11, :cond_13

    invoke-interface {v11}, Ljava/util/Map;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_13

    .line 414
    const-string v9, "action_attributes"

    .line 415
    invoke-static {v11}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromMap(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v44

    .line 414
    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    :cond_13
    move-object/from16 v0, v41

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 425
    .end local v11    # "actionAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v12    # "actionData":Ljava/lang/String;
    .end local v13    # "actionKey":Ljava/lang/String;
    .end local v17    # "cardAction":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    .end local v24    # "contentValues":Landroid/content/ContentValues;
    .end local v25    # "deletedCount":I
    .end local v42    # "whereArgs":[Ljava/lang/String;
    .restart local v21    # "cardElementList":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/magazine/cardprovider/card/CardElement;>;"
    .restart local v35    # "imageUri":Landroid/net/Uri;
    :cond_14
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/magazine/cardprovider/card/CardElement;

    .line 426
    .local v20, "cardElement":Lcom/samsung/android/magazine/cardprovider/card/CardElement;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->getKey()Ljava/lang/String;

    move-result-object v36

    .line 428
    .local v36, "key":Ljava/lang/String;
    const/4 v4, 0x4

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v42, v0

    const/4 v4, 0x0

    .line 429
    aput-object v33, v42, v4

    const/4 v4, 0x1

    .line 430
    aput-object v36, v42, v4

    const/4 v4, 0x2

    .line 431
    const-string v44, "image"

    aput-object v44, v42, v4

    const/4 v4, 0x3

    .line 432
    const-string v44, "text"

    aput-object v44, v42, v4

    .line 434
    .restart local v42    # "whereArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/magazine/cardprovider/CardManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v44, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$CardElement;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v44

    move-object/from16 v1, v29

    move-object/from16 v2, v42

    invoke-virtual {v4, v0, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v25

    .line 435
    .restart local v25    # "deletedCount":I
    if-lez v25, :cond_15

    .line 436
    sget-object v4, Lcom/samsung/android/magazine/cardprovider/CardManager;->TAG:Ljava/lang/String;

    new-instance v44, Ljava/lang/StringBuilder;

    const-string v45, "updateCard: Existed card element is deleted. key: "

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v44

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :cond_15
    new-instance v24, Landroid/content/ContentValues;

    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    .line 440
    .restart local v24    # "contentValues":Landroid/content/ContentValues;
    const-string v4, "card_id"

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v44

    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 441
    const-string v4, "key"

    move-object/from16 v0, v24

    move-object/from16 v1, v36

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    move-object/from16 v0, v20

    instance-of v4, v0, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    if-eqz v4, :cond_19

    .line 444
    const-string v4, "type"

    const-string v44, "text"

    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const-string v44, "data1"

    move-object/from16 v0, v20

    check-cast v0, Lcom/samsung/android/magazine/cardprovider/card/CardText;

    move-object v4, v0

    invoke-virtual {v4}, Lcom/samsung/android/magazine/cardprovider/card/CardText;->getText()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    :goto_a
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->getAttributes()Ljava/util/Map;

    move-result-object v16

    .line 456
    .local v16, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v16, :cond_16

    invoke-interface/range {v16 .. v16}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_16

    .line 457
    const-string v4, "attributes"

    .line 458
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromMap(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v44

    .line 457
    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    :cond_16
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/magazine/cardprovider/card/CardElement;->getAction()Lcom/samsung/android/magazine/cardprovider/card/CardAction;

    move-result-object v10

    .line 462
    .local v10, "action":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    if-eqz v10, :cond_18

    .line 463
    const-string v4, "action_type"

    invoke-virtual {v10}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getActionType()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    const-string v4, "action_label"

    invoke-virtual {v10}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getLabel()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    invoke-virtual {v10}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getData()Landroid/content/Intent;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v12

    .line 466
    .restart local v12    # "actionData":Ljava/lang/String;
    if-eqz v12, :cond_17

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_17

    .line 467
    const-string v4, "action_data"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :cond_17
    invoke-virtual {v10}, Lcom/samsung/android/magazine/cardprovider/card/CardAction;->getAttributes()Ljava/util/Map;

    move-result-object v11

    .line 470
    .restart local v11    # "actionAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v11, :cond_18

    invoke-interface {v11}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_18

    .line 471
    const-string v4, "action_attributes"

    .line 472
    invoke-static {v11}, Lcom/samsung/android/magazine/cardprovider/DataConverter;->getJsonFromMap(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v44

    .line 471
    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    .end local v11    # "actionAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v12    # "actionData":Ljava/lang/String;
    :cond_18
    move-object/from16 v0, v41

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_9

    .line 447
    .end local v10    # "action":Lcom/samsung/android/magazine/cardprovider/card/CardAction;
    .end local v16    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_19
    move-object/from16 v0, v20

    instance-of v4, v0, Lcom/samsung/android/magazine/cardprovider/card/CardImage;

    if-eqz v4, :cond_b

    .line 448
    const-string v4, "type"

    const-string v44, "image"

    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const-string v44, "data1"

    move-object/from16 v0, v20

    check-cast v0, Lcom/samsung/android/magazine/cardprovider/card/CardImage;

    move-object v4, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/magazine/cardprovider/CardManager;->getImageUri(Landroid/net/Uri;Lcom/samsung/android/magazine/cardprovider/card/CardImage;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_a

    .line 491
    .end local v14    # "actionKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v15    # "actionWhere":Ljava/lang/String;
    .end local v18    # "cardAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v20    # "cardElement":Lcom/samsung/android/magazine/cardprovider/card/CardElement;
    .end local v21    # "cardElementList":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/magazine/cardprovider/card/CardElement;>;"
    .end local v23    # "cardValues":Landroid/content/ContentValues;
    .end local v24    # "contentValues":Landroid/content/ContentValues;
    .end local v25    # "deletedCount":I
    .end local v26    # "currentTime":J
    .end local v29    # "elementWhere":Ljava/lang/String;
    .end local v30    # "expirationTime":J
    .end local v32    # "hasNoElement":Z
    .end local v33    # "id":Ljava/lang/String;
    .end local v35    # "imageUri":Landroid/net/Uri;
    .end local v36    # "key":Ljava/lang/String;
    .end local v39    # "templateId":Ljava/lang/String;
    .end local v41    # "valueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    .end local v42    # "whereArgs":[Ljava/lang/String;
    :cond_1a
    const/16 v38, 0x0

    goto/16 :goto_1
.end method
