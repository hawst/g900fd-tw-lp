.class public Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;
.super Ljava/lang/Object;
.source "ConfigurationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;,
        Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final VERSION_ALPHABETA:I = 0x1

.field private static final VERSION_ALPHABETA_NUM:I = 0x6

.field private static final VERSION_MAJOR:I = 0x1

.field private static final VERSION_MINOR:I

.field private static final VERSION_PATCH:I


# instance fields
.field private mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

.field private mChannelRegistered:Z

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mProviderObserver:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;

.field private mSubscriptionChangeRequestListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/magazine/cardchannel/CardChannel;)V
    .locals 2
    .param p1, "cardChannel"    # Lcom/samsung/android/magazine/cardchannel/CardChannel;

    .prologue
    const/4 v1, 0x0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    .line 50
    iput-object v1, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    .line 51
    iput-object v1, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mProviderObserver:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;

    .line 53
    iput-object v1, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mSubscriptionChangeRequestListeners:Ljava/util/HashMap;

    .line 111
    const-string v0, "SA Version"

    const-string v1, "CardChannel Library : 14"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    .line 113
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mSubscriptionChangeRequestListeners:Ljava/util/HashMap;

    .line 114
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;)Lcom/samsung/android/magazine/cardchannel/CardChannel;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mSubscriptionChangeRequestListeners:Ljava/util/HashMap;

    return-object v0
.end method

.method static getProvidedCardNames(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cardChannelName"    # Ljava/lang/String;
    .param p2, "cardProviderName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 1094
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1097
    .local v7, "cardNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "provider=? AND (channel_key IS NULL OR channel_key=?)"

    .line 1100
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    aput-object p2, v4, v9

    aput-object p1, v4, v1

    .line 1101
    .local v4, "whereArgs":[Ljava/lang/String;
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "card_type_key"

    aput-object v0, v2, v9

    .line 1103
    .local v2, "projection":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1104
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 1105
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1109
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1112
    :cond_0
    return-object v7

    .line 1106
    :cond_1
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1107
    .local v6, "cardName":Ljava/lang/String;
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private getRegisteredCardNames(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p1, "where"    # Ljava/lang/String;
    .param p2, "whereArg"    # [Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/CardInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 574
    .local v7, "cardInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/magazine/cardchannel/CardInfo;>;"
    const/4 v5, 0x0

    .line 576
    .local v5, "sort":Ljava/lang/String;
    if-eqz p3, :cond_1

    .line 577
    const-string v0, "ASC"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DESC"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 578
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "value1 "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 582
    :cond_1
    const/4 v9, 0x0

    .line 584
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 585
    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Setting;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    move-object v3, p1

    move-object v4, p2

    .line 584
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 592
    :goto_0
    if-eqz v9, :cond_2

    .line 593
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 612
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 615
    :cond_2
    return-object v7

    .line 587
    :catch_0
    move-exception v10

    .line 588
    .local v10, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getRegisteredCardNames"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    invoke-virtual {v10}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 595
    .end local v10    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    const-string v0, "card_type_key"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 594
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 596
    .local v8, "cardName":Ljava/lang/String;
    const-string v0, "provider"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 598
    .local v11, "providerName":Ljava/lang/String;
    new-instance v6, Lcom/samsung/android/magazine/cardchannel/CardInfo;

    invoke-direct {v6, v11, v8}, Lcom/samsung/android/magazine/cardchannel/CardInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    .local v6, "cardInfo":Lcom/samsung/android/magazine/cardchannel/CardInfo;
    const-string v0, "value1"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 599
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->setDisplayName(Ljava/lang/String;)V

    .line 602
    const-string v0, "value2"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 601
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->setDescription(Ljava/lang/String;)V

    .line 604
    const-string v0, "value3"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 603
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->setIntroductionImageUri(Ljava/lang/String;)V

    .line 606
    const-string v0, "section_key"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 605
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->setSection(Ljava/lang/String;)V

    .line 608
    const-string v0, "icon_image"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 607
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->setIconUri(Ljava/lang/String;)V

    .line 610
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private setCardNameEnabled(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p1, "cardProviderName"    # Ljava/lang/String;
    .param p2, "cardName"    # Ljava/lang/String;
    .param p3, "section"    # Ljava/lang/String;
    .param p4, "isEnabled"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    .line 692
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 694
    .local v3, "whereArgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "channel_key"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 695
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string v4, "=?"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 696
    iget-object v4, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 698
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 699
    const-string v4, " AND "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 700
    const-string v4, "provider"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 701
    const-string v4, "=?"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 702
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 704
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 705
    const-string v4, " AND "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 706
    const-string v4, "card_type_key"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 707
    const-string v4, "=?"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 708
    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 710
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 711
    const-string v4, " AND "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 712
    const-string v4, "section_key"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 713
    const-string v4, "=?"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 714
    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 717
    :cond_2
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 718
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v5, "enable"

    if-eqz p4, :cond_3

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 721
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 722
    sget-object v6, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Setting;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 723
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 721
    invoke-virtual {v5, v6, v0, v7, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 728
    :goto_1
    return-void

    .line 718
    :cond_3
    const/4 v4, 0x0

    goto :goto_0

    .line 724
    :catch_0
    move-exception v1

    .line 725
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v5, "[Exception] setCardNameEnabled"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public addCardProviderChangeListener(Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    .line 136
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v2, :cond_1

    .line 137
    :cond_0
    new-instance v2, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 138
    const-string v3, "CardChannel is not registered"

    .line 137
    invoke-direct {v2, v3}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 141
    :cond_1
    if-nez p1, :cond_2

    .line 142
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v3, "Failed to add listener. listener is null."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :goto_0
    return-void

    .line 147
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mProviderObserver:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;

    if-nez v2, :cond_3

    .line 148
    new-instance v2, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;

    invoke-direct {v2, p0}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;-><init>(Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;)V

    iput-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mProviderObserver:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;

    .line 149
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI_CHANGE_NOTIFICATION:Landroid/net/Uri;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mProviderObserver:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :cond_3
    :goto_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x40

    const/16 v4, 0x24

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mSubscriptionChangeRequestListeners:Ljava/util/HashMap;

    invoke-virtual {v2, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 151
    .end local v1    # "key":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v3, "[Exception] addCardProviderChangeListener"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public getCardInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardInfo;
    .locals 13
    .param p1, "cardProviderName"    # Ljava/lang/String;
    .param p2, "cardName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x3

    const/4 v6, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1043
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v0, :cond_1

    .line 1044
    :cond_0
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 1045
    const-string v1, "CardChannel is not registered"

    .line 1044
    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1049
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[getCardName] Failed to get cardInfo. cardProviderName is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1089
    :cond_2
    :goto_0
    return-object v6

    .line 1053
    :cond_3
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1054
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[getCardName] Failed to get cardInfo. cardName is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1058
    :cond_4
    const-string v3, "provider=? AND card_type_key=?"

    .line 1060
    .local v3, "where":Ljava/lang/String;
    new-array v4, v11, [Ljava/lang/String;

    aput-object p1, v4, v9

    aput-object p2, v4, v10

    .line 1061
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "value1"

    aput-object v0, v2, v9

    .line 1062
    const-string v0, "value2"

    aput-object v0, v2, v10

    .line 1063
    const-string v0, "value3"

    aput-object v0, v2, v11

    .line 1064
    const-string v0, "icon_image"

    aput-object v0, v2, v12

    const/4 v0, 0x4

    .line 1065
    const-string v1, "section_key"

    aput-object v1, v2, v0

    .line 1067
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 1068
    .local v6, "cardNameValue":Lcom/samsung/android/magazine/cardchannel/CardInfo;
    const/4 v7, 0x0

    .line 1070
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    .line 1071
    const/4 v5, 0x0

    .line 1070
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 1077
    :goto_1
    if-eqz v7, :cond_2

    .line 1078
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1079
    new-instance v6, Lcom/samsung/android/magazine/cardchannel/CardInfo;

    .end local v6    # "cardNameValue":Lcom/samsung/android/magazine/cardchannel/CardInfo;
    invoke-direct {v6, p1, p2}, Lcom/samsung/android/magazine/cardchannel/CardInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    .restart local v6    # "cardNameValue":Lcom/samsung/android/magazine/cardchannel/CardInfo;
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->setDisplayName(Ljava/lang/String;)V

    .line 1081
    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->setDescription(Ljava/lang/String;)V

    .line 1082
    invoke-interface {v7, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->setIntroductionImageUri(Ljava/lang/String;)V

    .line 1083
    invoke-interface {v7, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->setIconUri(Ljava/lang/String;)V

    .line 1084
    const/4 v0, 0x4

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/magazine/cardchannel/CardInfo;->setSection(Ljava/lang/String;)V

    .line 1086
    :cond_5
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1072
    :catch_0
    move-exception v8

    .line 1073
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getCardInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1074
    invoke-virtual {v8}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public getCardProvider(Ljava/lang/String;)Lcom/samsung/android/magazine/cardchannel/CardProvider;
    .locals 12
    .param p1, "cardProviderName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v1, 0x1

    const/4 v11, 0x0

    .line 995
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v0, :cond_1

    .line 996
    :cond_0
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 997
    const-string v1, "CardChannel is not registered"

    .line 996
    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1000
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1001
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[getCardProvider] Failed to get card provider. cardProviderName is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1030
    :cond_2
    :goto_0
    return-object v10

    .line 1005
    :cond_3
    const/4 v10, 0x0

    .line 1006
    .local v10, "provider":Lcom/samsung/android/magazine/cardchannel/CardProvider;
    const-string v3, "package_name=?"

    .line 1007
    .local v3, "where":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    aput-object p1, v4, v11

    .line 1008
    .local v4, "whereArgs":[Ljava/lang/String;
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "setting_action"

    aput-object v0, v2, v11

    .line 1010
    .local v2, "projection":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 1012
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Provider;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 1018
    :goto_1
    if-eqz v7, :cond_2

    .line 1019
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1020
    new-instance v10, Lcom/samsung/android/magazine/cardchannel/CardProvider;

    .end local v10    # "provider":Lcom/samsung/android/magazine/cardchannel/CardProvider;
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-direct {v10, v0, p1}, Lcom/samsung/android/magazine/cardchannel/CardProvider;-><init>(Lcom/samsung/android/magazine/cardchannel/CardChannel;Ljava/lang/String;)V

    .line 1021
    .restart local v10    # "provider":Lcom/samsung/android/magazine/cardchannel/CardProvider;
    invoke-interface {v7, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1022
    .local v9, "intentBytes":Ljava/lang/String;
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 1023
    invoke-static {v9}, Lcom/samsung/android/magazine/cardchannel/util/DataConverter;->getIntentFromJson(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 1024
    .local v6, "configurationIntent":Landroid/content/Intent;
    invoke-virtual {v10, v6}, Lcom/samsung/android/magazine/cardchannel/CardProvider;->setConfiguratonIntent(Landroid/content/Intent;)V

    .line 1027
    .end local v6    # "configurationIntent":Landroid/content/Intent;
    .end local v9    # "intentBytes":Ljava/lang/String;
    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1013
    :catch_0
    move-exception v8

    .line 1014
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getCardProvider"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    invoke-virtual {v8}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public getRegisteredCardInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1, "section"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/CardInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 496
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v2, :cond_1

    .line 497
    :cond_0
    new-instance v2, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 498
    const-string v3, "CardChannel is not registered"

    .line 497
    invoke-direct {v2, v3}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 501
    :cond_1
    const/4 v1, 0x0

    .line 503
    .local v1, "whereArgsSetting":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 504
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v2, "setting"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 505
    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    const-string v2, "channel_key"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 507
    const-string v2, "=?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 510
    new-array v1, v4, [Ljava/lang/String;

    .end local v1    # "whereArgsSetting":[Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    .line 520
    .restart local v1    # "whereArgsSetting":[Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1, p2}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->getRegisteredCardNames(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    return-object v2

    .line 512
    :cond_2
    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    const-string v2, "setting"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 514
    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    const-string v2, "section_key"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    const-string v2, "=?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 517
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/String;

    .end local v1    # "whereArgsSetting":[Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    .restart local v1    # "whereArgsSetting":[Ljava/lang/String;
    goto :goto_0
.end method

.method public getRegisteredCardInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "cardProviderName"    # Ljava/lang/String;
    .param p2, "section"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/CardInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 536
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v2, :cond_1

    .line 537
    :cond_0
    new-instance v2, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 538
    const-string v3, "CardChannel is not registered"

    .line 537
    invoke-direct {v2, v3}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 541
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 542
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v3, "Failed to get registered card name. cardProviderName is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    const/4 v2, 0x0

    .line 569
    :goto_0
    return-object v2

    .line 546
    :cond_2
    const/4 v1, 0x0

    .line 547
    .local v1, "whereArgsSetting":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 549
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v2, "setting"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    const-string v2, "channel_key"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    const-string v2, "=? AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    const-string v2, "setting"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555
    const-string v2, "provider"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    const-string v2, "=?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 559
    new-array v1, v5, [Ljava/lang/String;

    .end local v1    # "whereArgsSetting":[Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    .line 569
    .restart local v1    # "whereArgsSetting":[Ljava/lang/String;
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1, p3}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->getRegisteredCardNames(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    goto :goto_0

    .line 561
    :cond_3
    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    const-string v2, "setting"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    const-string v2, "section_key"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 565
    const-string v2, "=?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    const/4 v2, 0x3

    new-array v1, v2, [Ljava/lang/String;

    .end local v1    # "whereArgsSetting":[Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    aput-object p2, v1, v5

    .restart local v1    # "whereArgsSetting":[Ljava/lang/String;
    goto :goto_1
.end method

.method public getRegisteredCardProviders(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p1, "section"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/magazine/cardchannel/CardProvider;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    .line 743
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v0, :cond_1

    .line 744
    :cond_0
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 745
    const-string v1, "CardChannel is not registered"

    .line 744
    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 748
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 749
    .local v7, "cardProviderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/magazine/cardchannel/CardProvider;>;"
    const/4 v3, 0x0

    .line 750
    .local v3, "where":Ljava/lang/String;
    const/4 v4, 0x0

    .line 751
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "package_name"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    .line 752
    const-string v1, "setting_action"

    aput-object v1, v2, v0

    .line 753
    .local v2, "projection":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 756
    .local v5, "sort":Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 757
    const-string v3, "setting.channel_key=?"

    .line 758
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 765
    .restart local v4    # "whereArgs":[Ljava/lang/String;
    :goto_0
    if-eqz p2, :cond_3

    .line 766
    const-string v0, "ASC"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "DESC"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 767
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "package_name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 771
    :cond_3
    const/4 v9, 0x0

    .line 773
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$RegisteredProvider;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 780
    :goto_1
    if-eqz v9, :cond_4

    .line 781
    :goto_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_6

    .line 792
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 795
    :cond_4
    return-object v7

    .line 760
    .end local v9    # "cursor":Landroid/database/Cursor;
    :cond_5
    const-string v3, "setting.channel_key=? AND setting.section_key=?"

    .line 762
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    .restart local v4    # "whereArgs":[Ljava/lang/String;
    goto :goto_0

    .line 775
    .restart local v9    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v10

    .line 776
    .local v10, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getRegisteredCardProviders"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    invoke-virtual {v10}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 782
    .end local v10    # "e":Ljava/lang/IllegalArgumentException;
    :cond_6
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 783
    .local v12, "providerName":Ljava/lang/String;
    new-instance v6, Lcom/samsung/android/magazine/cardchannel/CardProvider;

    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-direct {v6, v0, v12}, Lcom/samsung/android/magazine/cardchannel/CardProvider;-><init>(Lcom/samsung/android/magazine/cardchannel/CardChannel;Ljava/lang/String;)V

    .line 785
    .local v6, "cardProvider":Lcom/samsung/android/magazine/cardchannel/CardProvider;
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 786
    .local v11, "intentBytes":Ljava/lang/String;
    if-eqz v11, :cond_7

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 787
    invoke-static {v11}, Lcom/samsung/android/magazine/cardchannel/util/DataConverter;->getIntentFromJson(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    .line 788
    .local v8, "configurationIntent":Landroid/content/Intent;
    invoke-virtual {v6, v8}, Lcom/samsung/android/magazine/cardchannel/CardProvider;->setConfiguratonIntent(Landroid/content/Intent;)V

    .line 790
    .end local v8    # "configurationIntent":Landroid/content/Intent;
    :cond_7
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public getSectionDisplayName(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "section"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 1219
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1220
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[getSectionDisplayName] Failed to get a display name. section is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1244
    :cond_0
    :goto_0
    return-object v8

    .line 1224
    :cond_1
    const-string v3, "key1=? AND key2=?"

    .line 1226
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v9

    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 1227
    .local v4, "whereArg":[Ljava/lang/String;
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "value1"

    aput-object v0, v2, v9

    .line 1229
    .local v2, "projection":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 1230
    .local v8, "sectionName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1232
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$SectionDisplayName;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1237
    :goto_1
    if-eqz v6, :cond_0

    .line 1238
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1239
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1241
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1233
    :catch_0
    move-exception v7

    .line 1234
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getSectionDisplayName"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1235
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public getSections()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v10, 0x0

    .line 1183
    const-string v3, "setting.channel_key=?"

    .line 1184
    .local v3, "where":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    .line 1185
    .local v4, "whereArg":[Ljava/lang/String;
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "key"

    aput-object v0, v2, v10

    .line 1187
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 1189
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 1190
    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$RegisteredSection;->CONTENT_URI:Landroid/net/Uri;

    .line 1191
    const/4 v5, 0x0

    .line 1189
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1197
    :goto_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1199
    .local v9, "sectionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v6, :cond_0

    .line 1200
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1205
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1208
    :cond_0
    return-object v9

    .line 1192
    .end local v9    # "sectionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v7

    .line 1193
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getSections"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1194
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1201
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v9    # "sectionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1202
    .local v8, "section":Ljava/lang/String;
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getUpdatedCardNames(Ljava/lang/String;JLjava/lang/String;)Ljava/util/List;
    .locals 10
    .param p1, "cardProviderName"    # Ljava/lang/String;
    .param p2, "fromTime"    # J
    .param p4, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    .line 940
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v0, :cond_1

    .line 941
    :cond_0
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 942
    const-string v1, "CardChannel is not registered"

    .line 941
    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 945
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 946
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "Failed to get card names. cardProviderName is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 947
    const/4 v7, 0x0

    .line 983
    :cond_2
    :goto_0
    return-object v7

    .line 950
    :cond_3
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 952
    .local v7, "cardNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "(channel_key IS NULL OR channel_key=?) AND provider=? AND last_modified_time>=?"

    .line 956
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    const/4 v0, 0x2

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 957
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "card_type_key"

    aput-object v1, v2, v0

    .line 958
    .local v2, "projection":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 960
    .local v5, "sort":Ljava/lang/String;
    if-eqz p4, :cond_5

    .line 961
    const-string v0, "ASC"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "DESC"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 962
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "card_type_key "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 966
    :cond_5
    const/4 v8, 0x0

    .line 968
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 975
    :goto_1
    if-eqz v8, :cond_2

    .line 976
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_6

    .line 980
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 970
    :catch_0
    move-exception v9

    .line 971
    .local v9, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getUpdatedCardNames"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 977
    .end local v9    # "e":Ljava/lang/IllegalArgumentException;
    :cond_6
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 978
    .local v6, "cardName":Ljava/lang/String;
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public getUpdatedCardProviders(JLjava/lang/String;)Ljava/util/List;
    .locals 11
    .param p1, "fromTime"    # J
    .param p3, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v10, 0x0

    .line 888
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v0, :cond_1

    .line 889
    :cond_0
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 890
    const-string v1, "CardChannel is not registered"

    .line 889
    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 893
    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 895
    .local v9, "providerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "(channel_key IS NULL OR channel_key=?) AND last_modified_time>=?"

    .line 898
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 899
    .local v4, "whereArgs":[Ljava/lang/String;
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "provider"

    aput-object v0, v2, v10

    .line 900
    .local v2, "projection":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 902
    .local v5, "sort":Ljava/lang/String;
    if-eqz p3, :cond_3

    .line 903
    const-string v0, "ASC"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "DESC"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 904
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "provider "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 908
    :cond_3
    const/4 v6, 0x0

    .line 910
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 917
    :goto_0
    if-eqz v6, :cond_4

    .line 918
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_5

    .line 922
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 925
    :cond_4
    return-object v9

    .line 912
    :catch_0
    move-exception v7

    .line 913
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] getUpdatedCardProviders"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 919
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :cond_5
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 920
    .local v8, "provider":Ljava/lang/String;
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 124
    const v0, 0x1000016

    .line 125
    .local v0, "version":I
    return v0
.end method

.method public isRegistered(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "cardProviderName"    # Ljava/lang/String;
    .param p2, "cardName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 1126
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v0, :cond_1

    .line 1127
    :cond_0
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 1128
    const-string v1, "CardChannel is not registered"

    .line 1127
    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1131
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1132
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "Failed to check subscription state. cardProviderName is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1172
    :cond_2
    :goto_0
    return v8

    .line 1136
    :cond_3
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1137
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "Failed to check subscription state. cardName is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1142
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 1143
    .local v9, "sb":Ljava/lang/StringBuilder;
    const-string v0, "setting"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144
    const-string v0, "."

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145
    const-string v0, "provider"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146
    const-string v0, "=? AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1147
    const-string v0, "setting"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1148
    const-string v0, "."

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1149
    const-string v0, "card_type_key"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1150
    const-string v0, "=?"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1151
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v8

    aput-object p2, v4, v1

    .line 1152
    .local v4, "whereArg":[Ljava/lang/String;
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v8

    .line 1154
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 1156
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 1157
    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Setting;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    .line 1156
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1163
    :goto_1
    const/4 v8, 0x0

    .line 1164
    .local v8, "isRegistered":Z
    if-eqz v6, :cond_2

    .line 1165
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 1166
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "[isRegistered] configuration row count: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1167
    const/4 v8, 0x1

    .line 1169
    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1158
    .end local v8    # "isRegistered":Z
    :catch_0
    move-exception v7

    .line 1159
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] Failed to query() operation."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1160
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public isSubscribed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p1, "cardProviderName"    # Ljava/lang/String;
    .param p2, "cardName"    # Ljava/lang/String;
    .param p3, "section"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    .line 810
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v0, :cond_1

    .line 811
    :cond_0
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 812
    const-string v1, "CardChannel is not registered"

    .line 811
    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 815
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 816
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "Failed to check subscription state of cardname. cardName is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    const/4 v0, 0x0

    .line 874
    :goto_0
    return v0

    .line 820
    :cond_2
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 822
    .local v10, "whereArgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 823
    .local v9, "sb":Ljava/lang/StringBuilder;
    const-string v0, "setting"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 824
    const-string v0, "."

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 825
    const-string v0, "channel_key"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 826
    const-string v0, "=? AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 827
    const-string v0, "setting"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 828
    const-string v0, "."

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 829
    const-string v0, "card_type_key"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 830
    const-string v0, "=?"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 831
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 832
    invoke-virtual {v10, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 834
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 835
    const-string v0, " AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 836
    const-string v0, "setting"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 837
    const-string v0, "."

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 838
    const-string v0, "provider"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 839
    const-string v0, "=?"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 840
    invoke-virtual {v10, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 842
    :cond_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 843
    const-string v0, " AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 844
    const-string v0, "setting"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 845
    const-string v0, "."

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 846
    const-string v0, "section_key"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 847
    const-string v0, "=?"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 848
    invoke-virtual {v10, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 851
    :cond_4
    const/4 v8, 0x0

    .line 852
    .local v8, "enable":I
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "enable"

    aput-object v1, v2, v0

    .line 854
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 856
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 857
    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Setting;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 856
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 863
    :goto_1
    if-eqz v6, :cond_7

    .line 864
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_5

    .line 865
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "[isSubscribed] configuration row count: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 869
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 871
    :cond_6
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 874
    :cond_7
    const/4 v0, 0x1

    if-ne v8, v0, :cond_8

    const/4 v0, 0x1

    goto/16 :goto_0

    .line 858
    :catch_0
    move-exception v7

    .line 859
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] isSubscribed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 874
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public registerCardChannel()Z
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 200
    :try_start_0
    iget-object v9, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v9}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    iget-object v9, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v9}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 207
    const/4 v4, 0x1

    .line 209
    .local v4, "success":Z
    const-string v6, "key=?"

    .line 210
    .local v6, "where":Ljava/lang/String;
    new-array v7, v10, [Ljava/lang/String;

    iget-object v9, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v9}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 212
    .local v7, "whereArgs":[Ljava/lang/String;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 213
    .local v1, "contentValues":Landroid/content/ContentValues;
    const-string v8, "enable"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 214
    const-string v8, "style"

    iget-object v9, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v9}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getStyle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object v8, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v8}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getAttributes()Ljava/util/Map;

    move-result-object v0

    .line 216
    .local v0, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 217
    const-string v8, "attributes"

    invoke-static {v0}, Lcom/samsung/android/magazine/cardchannel/util/DataConverter;->getJsonFromMap(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :cond_0
    :try_start_1
    iget-object v8, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v9, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Channel;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v9, v1, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 222
    .local v3, "rowCount":I
    if-nez v3, :cond_1

    .line 223
    const-string v8, "key"

    iget-object v9, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v9}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v8, "package_name"

    iget-object v9, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v9}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v8, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 227
    sget-object v9, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Channel;->CONTENT_URI:Landroid/net/Uri;

    .line 226
    invoke-virtual {v8, v9, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 228
    .local v5, "uri":Landroid/net/Uri;
    if-nez v5, :cond_1

    .line 229
    const/4 v4, 0x0

    .line 236
    .end local v3    # "rowCount":I
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_1
    :goto_0
    iput-boolean v4, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    .line 237
    iget-boolean v8, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-eqz v8, :cond_2

    .line 238
    iget-object v8, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v8}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/magazine/cardchannel/CardManager;->setRegisteredCardChannel(Ljava/lang/String;)V

    .line 240
    .end local v0    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "contentValues":Landroid/content/ContentValues;
    .end local v4    # "success":Z
    .end local v6    # "where":Ljava/lang/String;
    .end local v7    # "whereArgs":[Ljava/lang/String;
    :cond_2
    :goto_1
    return v4

    .line 201
    :catch_0
    move-exception v2

    .line 202
    .local v2, "e":Ljava/lang/NullPointerException;
    sget-object v9, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v10, "registerCardChannel - invalid context"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v8

    .line 203
    goto :goto_1

    .line 232
    .end local v2    # "e":Ljava/lang/NullPointerException;
    .restart local v0    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v1    # "contentValues":Landroid/content/ContentValues;
    .restart local v4    # "success":Z
    .restart local v6    # "where":Ljava/lang/String;
    .restart local v7    # "whereArgs":[Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 233
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public registerCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p1, "cardProviderName"    # Ljava/lang/String;
    .param p2, "cardName"    # Ljava/lang/String;
    .param p3, "section"    # Ljava/lang/String;
    .param p4, "subscriptionState"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;,
            Lcom/samsung/android/magazine/cardchannel/CardNameNotFoundException;
        }
    .end annotation

    .prologue
    .line 359
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v2, :cond_1

    .line 360
    :cond_0
    new-instance v2, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 361
    const-string v3, "CardChannel is not registered"

    .line 360
    invoke-direct {v2, v3}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 364
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 365
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v3, "[registerCardName] Failed to register card name. cardName is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    :goto_0
    return-void

    .line 368
    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 369
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v3, "[registerCardName] Failed to register card name. cardProviderName is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 373
    :cond_3
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 374
    .local v0, "contentValues":Landroid/content/ContentValues;
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 375
    const-string v2, "channel_key"

    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const-string v2, "provider"

    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const-string v2, "card_type_key"

    invoke-virtual {v0, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 379
    const-string v2, "section_key"

    invoke-virtual {v0, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    :cond_4
    const-string v3, "enable"

    if-eqz p4, :cond_5

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 383
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 384
    sget-object v3, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Setting;->CONTENT_URI:Landroid/net/Uri;

    .line 383
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 385
    :catch_0
    move-exception v1

    .line 386
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v3, "[Exception] registerCardNames"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 380
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_5
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public registerCardNames(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 13
    .param p1, "cardProviderName"    # Ljava/lang/String;
    .param p2, "section"    # Ljava/lang/String;
    .param p3, "subscriptionState"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    .line 287
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v0, :cond_1

    .line 288
    :cond_0
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 289
    const-string v1, "CardChannel is not registered"

    .line 288
    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 292
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 293
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[registerCardNames] Failed to register card name. cardProviderName is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    :goto_0
    return-void

    .line 297
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v8

    .line 300
    .local v8, "channelName":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 301
    .local v7, "cardNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "provider=? AND (channel_key IS NULL OR channel_key=?)"

    .line 304
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v0, 0x1

    aput-object v8, v4, v0

    .line 305
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "card_type_key"

    aput-object v1, v2, v0

    .line 307
    .local v2, "projection":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 309
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v10

    .line 315
    :goto_1
    if-eqz v10, :cond_3

    .line 316
    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 320
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 323
    :cond_3
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 324
    .local v12, "valuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    .line 337
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Setting;->CONTENT_URI:Landroid/net/Uri;

    .line 338
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/content/ContentValues;

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/ContentValues;

    .line 337
    invoke-virtual {v1, v5, v0}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 339
    :catch_0
    move-exception v11

    .line 340
    .local v11, "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] registerCardNames"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    invoke-virtual {v11}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 310
    .end local v11    # "e":Ljava/lang/IllegalArgumentException;
    .end local v12    # "valuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :catch_1
    move-exception v11

    .line 311
    .restart local v11    # "e":Ljava/lang/IllegalArgumentException;
    sget-object v0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v1, "[Exception] registerCardNames"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    invoke-virtual {v11}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 317
    .end local v11    # "e":Ljava/lang/IllegalArgumentException;
    :cond_4
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 318
    .local v6, "cardName":Ljava/lang/String;
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 324
    .end local v6    # "cardName":Ljava/lang/String;
    .restart local v12    # "valuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 325
    .restart local v6    # "cardName":Ljava/lang/String;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 326
    .local v9, "contentValues":Landroid/content/ContentValues;
    const-string v0, "channel_key"

    invoke-virtual {v9, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const-string v0, "provider"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const-string v0, "card_type_key"

    invoke-virtual {v9, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 330
    const-string v0, "section_key"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :cond_6
    const-string v5, "enable"

    if-eqz p3, :cond_7

    const/4 v0, 0x1

    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v9, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 333
    invoke-virtual {v12, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 331
    :cond_7
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public removeCardProviderChangeListener(Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    .line 168
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v2, :cond_1

    .line 169
    :cond_0
    new-instance v2, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 170
    const-string v3, "CardChannel is not registered"

    .line 169
    invoke-direct {v2, v3}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 173
    :cond_1
    if-nez p1, :cond_3

    .line 174
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v3, "Failed to remove listener. listener is null."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_2
    :goto_0
    return-void

    .line 178
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x40

    const/16 v4, 0x24

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 179
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mSubscriptionChangeRequestListeners:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mSubscriptionChangeRequestListeners:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mProviderObserver:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;

    if-eqz v2, :cond_2

    .line 183
    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mProviderObserver:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 184
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mProviderObserver:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 186
    :catch_0
    move-exception v0

    .line 187
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v3, "[Exception] removeCardProviderChangeListener"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public subscribeToCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "cardProvider"    # Ljava/lang/String;
    .param p2, "cardName"    # Ljava/lang/String;
    .param p3, "section"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    .line 661
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v0, :cond_1

    .line 662
    :cond_0
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 663
    const-string v1, "CardChannel is not registered"

    .line 662
    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 666
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->setCardNameEnabled(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 667
    return-void
.end method

.method public unregisterCardChannel()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 249
    iget-object v5, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v5, :cond_1

    .line 250
    :cond_0
    new-instance v5, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 251
    const-string v6, "CardChannel is not registered"

    .line 250
    invoke-direct {v5, v6}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 254
    :cond_1
    const-string v3, "key=?"

    .line 255
    .local v3, "where":Ljava/lang/String;
    const/4 v5, 0x1

    new-array v4, v5, [Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 257
    .local v4, "whereArgs":[Ljava/lang/String;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 258
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v5, "enable"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 260
    const/4 v2, -0x1

    .line 262
    .local v2, "rowCount":I
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Channel;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 267
    :goto_0
    if-nez v2, :cond_2

    .line 268
    sget-object v5, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v6, "Failed to unregister card channel."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    :cond_2
    iput-boolean v7, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    .line 272
    iget-object v5, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v5}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/magazine/cardchannel/CardManager;->removeRegisteredCardChannel(Ljava/lang/String;)V

    .line 273
    return-void

    .line 263
    :catch_0
    move-exception v1

    .line 264
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v5, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v6, "[Exception] unregisterCardChannel"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public unregisterCardName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "cardProviderName"    # Ljava/lang/String;
    .param p2, "cardName"    # Ljava/lang/String;
    .param p3, "section"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 403
    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v3, :cond_1

    .line 404
    :cond_0
    new-instance v3, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 405
    const-string v4, "CardChannel is not registered"

    .line 404
    invoke-direct {v3, v4}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 408
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 409
    sget-object v3, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v4, "Failed to unregister card name. cardName is empty."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :goto_0
    return-void

    .line 412
    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 413
    sget-object v3, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v4, "Failed to unregister card name. cardProviderName is empty."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 419
    :cond_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 420
    const-string v1, "channel_key=? AND card_type_key=? AND provider=?"

    .line 423
    .local v1, "where":Ljava/lang/String;
    new-array v2, v7, [Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object p2, v2, v5

    aput-object p1, v2, v6

    .line 434
    .local v2, "whereArgs":[Ljava/lang/String;
    :goto_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Setting;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 435
    :catch_0
    move-exception v0

    .line 436
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v4, "[Exception] unregisterCardName"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 426
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v1    # "where":Ljava/lang/String;
    .end local v2    # "whereArgs":[Ljava/lang/String;
    :cond_4
    const-string v1, "channel_key=? AND card_type_key=? AND provider=? AND section_key=?"

    .line 430
    .restart local v1    # "where":Ljava/lang/String;
    const/4 v3, 0x4

    new-array v2, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object p2, v2, v5

    aput-object p1, v2, v6

    aput-object p3, v2, v7

    .restart local v2    # "whereArgs":[Ljava/lang/String;
    goto :goto_1
.end method

.method public unregisterCardNames(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "cardProviderName"    # Ljava/lang/String;
    .param p2, "section"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 452
    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v3, :cond_1

    .line 453
    :cond_0
    new-instance v3, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 454
    const-string v4, "CardChannel is not registered"

    .line 453
    invoke-direct {v3, v4}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 457
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 458
    sget-object v3, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v4, "Failed to unregister card name. cardProviderName is empty."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    :goto_0
    return-void

    .line 464
    :cond_2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 465
    const-string v1, "channel_key=? AND provider=?"

    .line 467
    .local v1, "where":Ljava/lang/String;
    new-array v2, v6, [Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object p1, v2, v5

    .line 477
    .local v2, "whereArgs":[Ljava/lang/String;
    :goto_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Setting;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 478
    :catch_0
    move-exception v0

    .line 479
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->TAG:Ljava/lang/String;

    const-string v4, "[Exception] unregisterCardNames"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 470
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v1    # "where":Ljava/lang/String;
    .end local v2    # "whereArgs":[Ljava/lang/String;
    :cond_3
    const-string v1, "channel_key=? AND provider=? AND section_key=?"

    .line 473
    .restart local v1    # "where":Ljava/lang/String;
    const/4 v3, 0x3

    new-array v2, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object p1, v2, v5

    aput-object p2, v2, v6

    .restart local v2    # "whereArgs":[Ljava/lang/String;
    goto :goto_1
.end method

.method public unsubscribeToCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "cardProvider"    # Ljava/lang/String;
    .param p2, "cardName"    # Ljava/lang/String;
    .param p3, "section"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;
        }
    .end annotation

    .prologue
    .line 681
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mChannelRegistered:Z

    if-nez v0, :cond_1

    .line 682
    :cond_0
    new-instance v0, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;

    .line 683
    const-string v1, "CardChannel is not registered"

    .line 682
    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/cardchannel/CardChannelNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 686
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->setCardNameEnabled(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 687
    return-void
.end method
