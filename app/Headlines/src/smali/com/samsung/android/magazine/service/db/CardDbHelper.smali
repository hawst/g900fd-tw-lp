.class public Lcom/samsung/android/magazine/service/db/CardDbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "CardDbHelper.java"


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "card.db"

.field private static final DATABASE_VERSION:I = 0xf

.field private static final TAG:Ljava/lang/String; = "MagazineService::CardDbHelper"

.field private static mInstance:Lcom/samsung/android/magazine/service/db/CardDbHelper;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/magazine/service/db/CardDbHelper;->mInstance:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 78
    const-string v0, "card.db"

    const/16 v1, 0xf

    invoke-direct {p0, p1, v0, v2, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 70
    iput-object v2, p0, Lcom/samsung/android/magazine/service/db/CardDbHelper;->mContext:Landroid/content/Context;

    .line 79
    iput-object p1, p0, Lcom/samsung/android/magazine/service/db/CardDbHelper;->mContext:Landroid/content/Context;

    .line 80
    return-void
.end method

.method private createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 124
    :try_start_0
    const-string v1, "CREATE TABLE IF NOT EXISTS card (_id INTEGER PRIMARY KEY AUTOINCREMENT, card_key TEXT NOT NULL, card_provider TEXT NOT NULL, card_type TEXT NOT NULL, template_type TEXT NOT NULL, card_channel TEXT, attributes TEXT, expiration_time INTEGER, time_stamp INTEGER );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 125
    const-string v1, "CREATE TABLE IF NOT EXISTS card_element (_id INTEGER PRIMARY KEY AUTOINCREMENT, card_id INTEGAER NOT NULL, key TEXT NOT NULL, type TEXT NOT NULL, data1 TEXT, attributes TEXT, action_type TEXT, action_label TEXT, action_data TEXT, action_attributes TEXT );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 126
    const-string v1, "CREATE TABLE IF NOT EXISTS channel (_id INTEGER PRIMARY KEY AUTOINCREMENT, key TEXT NOT NULL UNIQUE, package_name TEXT NOT NULL, attributes TEXT, style TEXT, enable INTEGER DEFAULT 1 );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 127
    const-string v1, "CREATE TABLE IF NOT EXISTS multi_language (_id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT NOT NULL, key1 TEXT NOT NULL, key2 TEXT NOT NULL, language_code TEXT NOT NULL, value1 TEXT NOT NULL, value2 TEXT, value3 TEXT );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 128
    const-string v1, "CREATE TABLE IF NOT EXISTS provided_card_type (_id INTEGER PRIMARY KEY AUTOINCREMENT, provider TEXT NOT NULL, card_type_key TEXT NOT NULL, channel_key TEXT, section_key TEXT, icon_image TEXT, default_subscription INTEGER DEFAULT 0, last_modified_time INTEGER );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 129
    const-string v1, "CREATE TABLE IF NOT EXISTS provider (_id INTEGER PRIMARY KEY AUTOINCREMENT, package_name TEXT NOT NULL UNIQUE, package_version INTEGER, setting_action TEXT );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 130
    const-string v1, "CREATE TABLE IF NOT EXISTS section (_id INTEGER PRIMARY KEY AUTOINCREMENT, key TEXT NOT NULL UNIQUE, channel_key TEXT NOT NULL, creator TEXT );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 131
    const-string v1, "CREATE TABLE IF NOT EXISTS setting (_id INTEGER PRIMARY KEY AUTOINCREMENT, channel_key TEXT NOT NULL, section_key TEXT, provider TEXT, card_type_key TEXT NOT NULL, enable INTEGER DEFAULT 1, visible INTEGER DEFAULT 1 );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_0
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "MagazineService::CardDbHelper"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private deleteFilesInDirectory(Ljava/io/File;)V
    .locals 6
    .param p1, "directory"    # Ljava/io/File;

    .prologue
    .line 186
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 187
    .local v2, "fileList":[Ljava/io/File;
    if-eqz v2, :cond_1

    .line 188
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 189
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 190
    invoke-direct {p0, v1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->deleteFilesInDirectory(Ljava/io/File;)V

    .line 191
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 188
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 194
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 198
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_1
    return-void
.end method

.method private deleteImageFiles()V
    .locals 4

    .prologue
    .line 177
    iget-object v1, p0, Lcom/samsung/android/magazine/service/db/CardDbHelper;->mContext:Landroid/content/Context;

    const-string v2, "image"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 180
    .local v0, "imageDirectory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 181
    invoke-direct {p0, v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->deleteFilesInDirectory(Ljava/io/File;)V

    .line 183
    :cond_0
    return-void
.end method

.method private deleteImageFilesForCard()V
    .locals 10

    .prologue
    .line 157
    iget-object v7, p0, Lcom/samsung/android/magazine/service/db/CardDbHelper;->mContext:Landroid/content/Context;

    const-string v8, "image"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    .line 160
    .local v5, "imageDirectory":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 161
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 162
    .local v3, "fileList":[Ljava/io/File;
    if-eqz v3, :cond_1

    .line 163
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v2, v0, v4

    .line 164
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 165
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 167
    .local v1, "dirName":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 168
    invoke-direct {p0, v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->deleteFilesInDirectory(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    .end local v1    # "dirName":Ljava/lang/String;
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 174
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fileList":[Ljava/io/File;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    :cond_1
    return-void

    .line 169
    .restart local v0    # "arr$":[Ljava/io/File;
    .restart local v1    # "dirName":Ljava/lang/String;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "fileList":[Ljava/io/File;
    .restart local v4    # "i$":I
    .restart local v6    # "len$":I
    :catch_0
    move-exception v7

    goto :goto_1
.end method

.method private dropTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 140
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS card;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 141
    const-string v1, "DROP TABLE IF EXISTS card_element;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 142
    const-string v1, "DROP TABLE IF EXISTS channel;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 143
    const-string v1, "DROP TABLE IF EXISTS multi_language;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 144
    const-string v1, "DROP TABLE IF EXISTS provided_card_type;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 145
    const-string v1, "DROP TABLE IF EXISTS provider;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 146
    const-string v1, "DROP TABLE IF EXISTS section;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 147
    const-string v1, "DROP TABLE IF EXISTS setting;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    invoke-direct {p0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->deleteImageFiles()V

    .line 154
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "MagazineService::CardDbHelper"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/magazine/service/db/CardDbHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 370
    const-class v1, Lcom/samsung/android/magazine/service/db/CardDbHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/magazine/service/db/CardDbHelper;->mInstance:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    if-nez v0, :cond_0

    .line 371
    new-instance v0, Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-direct {v0, p0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/magazine/service/db/CardDbHelper;->mInstance:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    .line 374
    :cond_0
    sget-object v0, Lcom/samsung/android/magazine/service/db/CardDbHelper;->mInstance:Lcom/samsung/android/magazine/service/db/CardDbHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 370
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private initializeTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 201
    iget-object v3, p0, Lcom/samsung/android/magazine/service/db/CardDbHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 204
    .local v2, "resources":Landroid/content/res/Resources;
    const/high16 v3, 0x7f050000

    :try_start_0
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 205
    .local v1, "inStream":Ljava/io/InputStream;
    if-eqz v1, :cond_0

    .line 206
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->insertData(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/InputStream;)Z

    .line 207
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 209
    :cond_0
    const v3, 0x7f050001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 210
    if-eqz v1, :cond_1

    .line 211
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->insertData(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/InputStream;)Z

    .line 212
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 214
    :cond_1
    const v3, 0x7f050002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 215
    if-eqz v1, :cond_2

    .line 216
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->insertData(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/InputStream;)Z

    .line 217
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 219
    :cond_2
    const v3, 0x7f050003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 221
    if-eqz v1, :cond_3

    .line 222
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->insertData(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/InputStream;)Z

    .line 223
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 225
    :cond_3
    const v3, 0x7f050004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 226
    if-eqz v1, :cond_4

    .line 227
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->insertData(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/InputStream;)Z

    .line 228
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 230
    :cond_4
    const v3, 0x7f050005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 231
    if-eqz v1, :cond_5

    .line 232
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->insertData(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/InputStream;)Z

    .line 233
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    .end local v1    # "inStream":Ljava/io/InputStream;
    :cond_5
    :goto_0
    return-void

    .line 235
    :catch_0
    move-exception v0

    .line 236
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "MagazineService::CardDbHelper"

    const-string v4, "Fail to initialize tables."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private insertData(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/InputStream;)Z
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "inputStream"    # Ljava/io/InputStream;

    .prologue
    const/4 v7, 0x0

    .line 242
    if-nez p2, :cond_0

    .line 243
    const-string v8, "MagazineService::CardDbHelper"

    const-string v9, "InputStream is empty."

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    :goto_0
    return v7

    .line 247
    :cond_0
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 248
    .local v3, "inputReader":Ljava/io/InputStreamReader;
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 249
    .local v0, "bufReader":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 250
    .local v5, "tableName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 252
    .local v1, "columns":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 253
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 260
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 261
    :cond_1
    const-string v8, "MagazineService::CardDbHelper"

    const-string v9, "Table name or column list is empty."

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 254
    :catch_0
    move-exception v2

    .line 255
    .local v2, "e":Ljava/io/IOException;
    const-string v8, "MagazineService::CardDbHelper"

    const-string v9, "Failed to read Table name or column list"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 265
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    const-string v7, "MagazineService::CardDbHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Table name: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_3
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 270
    .local v6, "values":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 272
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "INSERT INTO "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 273
    .local v4, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    const-string v7, " VALUES "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    const-string v7, ";"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_2

    .line 282
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    if-nez v6, :cond_3

    .line 291
    .end local v6    # "values":Ljava/lang/String;
    :goto_1
    const-string v7, "MagazineService::CardDbHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Complete to insert data to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 283
    :catch_1
    move-exception v2

    .line 284
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "MagazineService::CardDbHelper"

    const-string v8, "Failed to read a row"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 286
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 287
    .local v2, "e":Landroid/database/sqlite/SQLiteException;
    const-string v7, "MagazineService::CardDbHelper"

    const-string v8, "coulmn is not unique"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1
.end method

.method private upgradeDbFrom14To15(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 20
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 297
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 299
    const-string v2, "DROP TABLE IF EXISTS card;"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 300
    const-string v2, "DROP TABLE IF EXISTS card_element;"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 301
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->deleteImageFilesForCard()V

    .line 303
    const-string v2, "CREATE TABLE IF NOT EXISTS card (_id INTEGER PRIMARY KEY AUTOINCREMENT, card_key TEXT NOT NULL, card_provider TEXT NOT NULL, card_type TEXT NOT NULL, template_type TEXT NOT NULL, card_channel TEXT, attributes TEXT, expiration_time INTEGER, time_stamp INTEGER );"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 304
    const-string v2, "CREATE TABLE IF NOT EXISTS card_element (_id INTEGER PRIMARY KEY AUTOINCREMENT, card_id INTEGAER NOT NULL, key TEXT NOT NULL, type TEXT NOT NULL, data1 TEXT, attributes TEXT, action_type TEXT, action_label TEXT, action_data TEXT, action_attributes TEXT );"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 307
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 308
    .local v14, "channelValuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "key"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "package_name"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "style"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string v3, "enable"

    aput-object v3, v4, v2

    .line 313
    .local v4, "channelProjection":[Ljava/lang/String;
    const-string v3, "channel"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 315
    .local v13, "channelCursor":Landroid/database/Cursor;
    if-eqz v13, :cond_2

    .line 316
    :goto_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 317
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 318
    .local v19, "values":Landroid/content/ContentValues;
    const-string v2, "key"

    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const-string v2, "package_name"

    const/4 v3, 0x1

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string v2, "style"

    const/4 v3, 0x2

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const-string v2, "enable"

    const/4 v3, 0x3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 322
    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 363
    .end local v4    # "channelProjection":[Ljava/lang/String;
    .end local v13    # "channelCursor":Landroid/database/Cursor;
    .end local v14    # "channelValuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    .end local v19    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v15

    .line 364
    .local v15, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "MagazineService::CardDbHelper"

    invoke-virtual {v15}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    .end local v15    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    return-void

    .line 324
    .restart local v4    # "channelProjection":[Ljava/lang/String;
    .restart local v13    # "channelCursor":Landroid/database/Cursor;
    .restart local v14    # "channelValuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_1
    :try_start_1
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 326
    :cond_2
    const-string v2, "DROP TABLE IF EXISTS channel;"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 328
    const-string v2, "CREATE TABLE IF NOT EXISTS channel (_id INTEGER PRIMARY KEY AUTOINCREMENT, key TEXT NOT NULL UNIQUE, package_name TEXT NOT NULL, attributes TEXT, style TEXT, enable INTEGER DEFAULT 1 );"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 329
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/content/ContentValues;

    .line 330
    .restart local v19    # "values":Landroid/content/ContentValues;
    const-string v2, "channel"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_1

    .line 334
    .end local v19    # "values":Landroid/content/ContentValues;
    :cond_3
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 335
    .local v18, "providerValuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const/4 v2, 0x2

    new-array v7, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "package_name"

    aput-object v3, v7, v2

    const/4 v2, 0x1

    const-string v3, "package_version"

    aput-object v3, v7, v2

    .line 338
    .local v7, "providerProjection":[Ljava/lang/String;
    const-string v6, "provider"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v5, p1

    invoke-virtual/range {v5 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 340
    .local v17, "providerCursor":Landroid/database/Cursor;
    if-eqz v17, :cond_5

    .line 341
    :goto_2
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 342
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 343
    .restart local v19    # "values":Landroid/content/ContentValues;
    const-string v2, "package_name"

    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string v2, "package_version"

    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 345
    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 347
    .end local v19    # "values":Landroid/content/ContentValues;
    :cond_4
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 349
    :cond_5
    const-string v2, "DROP TABLE IF EXISTS provider;"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 351
    const-string v2, "CREATE TABLE IF NOT EXISTS provider (_id INTEGER PRIMARY KEY AUTOINCREMENT, package_name TEXT NOT NULL UNIQUE, package_version INTEGER, setting_action TEXT );"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 352
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/content/ContentValues;

    .line 353
    .restart local v19    # "values":Landroid/content/ContentValues;
    const-string v2, "provider"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_3

    .line 356
    .end local v19    # "values":Landroid/content/ContentValues;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 357
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 359
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/content/ContentValues;

    .line 360
    .restart local v19    # "values":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/db/CardDbHelper;->mContext:Landroid/content/Context;

    const-string v3, "package_name"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/magazine/service/Notifier;->sendDbMigrationBroadcast(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 84
    const-string v0, "MagazineService::CardDbHelper"

    const-string v1, "[onCreate]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 88
    invoke-direct {p0, p1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 89
    invoke-direct {p0, p1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->initializeTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 91
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 92
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 95
    iget-object v0, p0, Lcom/samsung/android/magazine/service/db/CardDbHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/magazine/service/Notifier;->sendMagazineServiceReadyBroadcast(Landroid/content/Context;)V

    .line 96
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 100
    const-string v0, "MagazineService::CardDbHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onUpgrade]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const/16 v0, 0xe

    if-ne p2, v0, :cond_0

    const/16 v0, 0xf

    if-ne p3, v0, :cond_0

    .line 103
    invoke-direct {p0, p1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->upgradeDbFrom14To15(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 118
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 108
    invoke-direct {p0, p1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 109
    invoke-direct {p0, p1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 110
    invoke-direct {p0, p1}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->initializeTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 112
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 113
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 116
    iget-object v0, p0, Lcom/samsung/android/magazine/service/db/CardDbHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/magazine/service/Notifier;->sendMagazineServiceReadyBroadcast(Landroid/content/Context;)V

    goto :goto_0
.end method
