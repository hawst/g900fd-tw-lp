.class public final Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;
.super Ljava/lang/Object;
.source "CardProviderContract.java"

# interfaces
.implements Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardTypeColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/cardprovider/CardProviderContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvidedCardType"
.end annotation


# static fields
.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardprovider.provided_card_type"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardprovider.provided_card_type"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CONTENT_URI_CREATE:Landroid/net/Uri;

.field public static final CONTENT_URI_IMAGE:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 815
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 816
    const-string v1, "provided_card_type"

    .line 815
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    .line 823
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "image"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI_IMAGE:Landroid/net/Uri;

    .line 830
    sget-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "create"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/cardprovider/CardProviderContract$ProvidedCardType;->CONTENT_URI_CREATE:Landroid/net/Uri;

    .line 848
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807
    return-void
.end method
