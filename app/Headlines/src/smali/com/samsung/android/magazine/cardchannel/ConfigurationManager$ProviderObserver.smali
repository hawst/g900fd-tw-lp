.class Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;
.super Landroid/database/ContentObserver;
.source "ConfigurationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProviderObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;)V
    .locals 1

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;->this$0:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 78
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 7
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 83
    invoke-virtual {p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    .line 84
    .local v3, "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x4

    if-lt v5, v6, :cond_2

    .line 85
    const/4 v5, 0x2

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 86
    .local v4, "providerName":Ljava/lang/String;
    const/4 v5, 0x3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    .local v0, "channel":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 88
    const-string v5, "channel_not_defined"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 89
    iget-object v5, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;->this$0:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    # getter for: Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;
    invoke-static {v5}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->access$0(Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;)Lcom/samsung/android/magazine/cardchannel/CardChannel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 91
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$ProviderObserver;->this$0:Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;

    # getter for: Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->mSubscriptionChangeRequestListeners:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->access$1(Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 93
    .local v2, "listeners":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;>;>;"
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    .line 101
    .end local v0    # "channel":Ljava/lang/String;
    .end local v2    # "listeners":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;>;>;"
    .end local v4    # "providerName":Ljava/lang/String;
    :cond_2
    return-void

    .line 94
    .restart local v0    # "channel":Ljava/lang/String;
    .restart local v2    # "listeners":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;>;>;"
    .restart local v4    # "providerName":Ljava/lang/String;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 95
    .local v1, "listener":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;>;"
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 96
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;

    invoke-interface {v5, v4}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager$CardProviderChangeListener;->onChanged(Ljava/lang/String;)V

    goto :goto_0
.end method
