.class public Lcom/samsung/android/magazine/service/db/ProviderTable;
.super Ljava/lang/Object;
.source "ProviderTable.java"


# static fields
.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final COLUMN_PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field public static final COLUMN_PACKAGE_VERSION:Ljava/lang/String; = "package_version"

.field public static final COLUMN_SETTING_ACTION:Ljava/lang/String; = "setting_action"

.field public static final SQL_CREATE:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS provider (_id INTEGER PRIMARY KEY AUTOINCREMENT, package_name TEXT NOT NULL UNIQUE, package_version INTEGER, setting_action TEXT );"

.field public static final TABLE_NAME:Ljava/lang/String; = "provider"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addTableName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "column"    # Ljava/lang/String;

    .prologue
    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "provider."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
