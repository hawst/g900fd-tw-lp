.class public final Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;
.super Ljava/lang/Object;
.source "CardChannelContract.java"

# interfaces
.implements Lcom/samsung/android/magazine/service/contract/CardChannelContract$CardColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/service/contract/CardChannelContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Card"
.end annotation


# static fields
.field public static final CHANNEL_NOT_DEFINED:Ljava/lang/String; = "channel_not_defined"

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardchannel.card"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardchannel.card"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CONTENT_URI_DELETE_NOTIFICATION:Landroid/net/Uri;

.field public static final CONTENT_URI_INSERT_NOTIFICATION:Landroid/net/Uri;

.field public static final CONTENT_URI_UPDATE_NOTIFICATION:Landroid/net/Uri;

.field public static final SECTION_KEY:Ljava/lang/String; = "section_key"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 651
    sget-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "card"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI:Landroid/net/Uri;

    .line 658
    sget-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "delete"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI_DELETE_NOTIFICATION:Landroid/net/Uri;

    .line 665
    sget-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "insert"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI_INSERT_NOTIFICATION:Landroid/net/Uri;

    .line 672
    sget-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "update"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI_UPDATE_NOTIFICATION:Landroid/net/Uri;

    .line 690
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
