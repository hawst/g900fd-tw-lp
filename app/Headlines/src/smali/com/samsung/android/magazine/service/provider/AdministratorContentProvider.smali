.class public Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;
.super Landroid/content/ContentProvider;
.source "AdministratorContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;
    }
.end annotation


# static fields
.field private static final CODE_CARD_ELEMENT_ID:I = 0xc9

.field private static final CODE_CARD_ELEMENT_LIST:I = 0xc8

.field private static final CODE_CARD_ID:I = 0x65

.field private static final CODE_CARD_IMAGE:I = 0x6e

.field private static final CODE_CARD_LIST:I = 0x64

.field private static final CODE_CHANNEL_ID:I = 0x12d

.field private static final CODE_CHANNEL_LIST:I = 0x12c

.field private static final CODE_MULTI_LANGUAGE_ID:I = 0x191

.field private static final CODE_MULTI_LANGUAGE_LIST:I = 0x190

.field private static final CODE_PROVIDED_CARD_TYPE_ID:I = 0x1f5

.field private static final CODE_PROVIDED_CARD_TYPE_IMAGE:I = 0x1fe

.field private static final CODE_PROVIDED_CARD_TYPE_LIST:I = 0x1f4

.field private static final CODE_PROVIDER_ID:I = 0x259

.field private static final CODE_PROVIDER_LIST:I = 0x258

.field private static final CODE_SETTING_ID:I = 0x2bd

.field private static final CODE_SETTING_LIST:I = 0x2bc

.field private static final ERROR_CODE_DATA_ALREADY_EXISTS:I = -0x2

.field private static final ERROR_CODE_DATA_NOT_FOUND:I = -0x3

.field private static final TAG:Ljava/lang/String; = "MagazineService::AdministratorContentProvider"

.field private static final mUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 82
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 83
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "card"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 84
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "card/#"

    const/16 v3, 0x65

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 85
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "card/#/image/*/*"

    const/16 v3, 0x6e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 86
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "card_element"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 87
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "card_element/#"

    const/16 v3, 0xc9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 88
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "channel"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 89
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "channel/#"

    const/16 v3, 0x12d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 90
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "multi_language"

    const/16 v3, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 91
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "multi_language/#"

    const/16 v3, 0x191

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 92
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "provided_card_type"

    const/16 v3, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 93
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "provided_card_type/#"

    const/16 v3, 0x1f5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 94
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "provided_card_type/image/*/*/*"

    const/16 v3, 0x1fe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 95
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "provider"

    const/16 v3, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 96
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "provider/#"

    const/16 v3, 0x259

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 97
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "setting"

    const/16 v3, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 98
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v2, "setting/#"

    const/16 v3, 0x2bd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 99
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 77
    iput-object v0, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    .line 78
    iput-object v0, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    .line 101
    return-void
.end method

.method private deleteCardElements(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 17
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 513
    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "type"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "data1"

    aput-object v4, v5, v3

    .line 518
    .local v5, "projection":[Ljava/lang/String;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 519
    .local v14, "imgUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v4, "card_element"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 520
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_2

    .line 521
    :cond_0
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 522
    const/4 v3, 0x0

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 523
    .local v16, "type":Ljava/lang/String;
    if-eqz v16, :cond_0

    const-string v3, "image"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 524
    const/4 v3, 0x1

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 527
    .end local v16    # "type":Ljava/lang/String;
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 530
    :cond_2
    const-string v3, "card_element"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 531
    .local v15, "rowCount":I
    if-lez v15, :cond_3

    .line 534
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 535
    .local v13, "imageUri":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteCardFile(Ljava/lang/String;)V

    goto :goto_1

    .line 539
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "imageUri":Ljava/lang/String;
    :cond_3
    return v15
.end method

.method private deleteCardFile(Ljava/lang/String;)V
    .locals 7
    .param p1, "fileUri"    # Ljava/lang/String;

    .prologue
    .line 556
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 570
    :cond_0
    :goto_0
    return-void

    .line 560
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 561
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 562
    .local v2, "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x5

    if-lt v4, v5, :cond_0

    .line 563
    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    const-string v5, "image"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v0, v5, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 564
    .local v0, "directory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 565
    new-instance v1, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x3

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v4, 0x4

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 566
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 567
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private deleteCardImageFiles(Ljava/lang/String;)V
    .locals 9
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 543
    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    const-string v7, "image"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v6

    invoke-direct {v1, v6, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 544
    .local v1, "directory":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 545
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 546
    .local v3, "fileList":[Ljava/io/File;
    if-eqz v3, :cond_0

    .line 547
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 548
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 547
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 551
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 553
    .end local v3    # "fileList":[Ljava/io/File;
    :cond_1
    return-void
.end method

.method private deleteCardTypeImageFiles(Ljava/lang/String;)V
    .locals 14
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 573
    new-instance v8, Ljava/io/File;

    iget-object v11, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    const-string v12, "image"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v11

    invoke-direct {v8, v11, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 574
    .local v8, "providerDirectory":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 575
    invoke-virtual {v8}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v10

    .line 576
    .local v10, "subDirList":[Ljava/io/File;
    if-eqz v10, :cond_1

    .line 577
    move-object v0, v10

    .local v0, "arr$":[Ljava/io/File;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    move v5, v4

    .end local v0    # "arr$":[Ljava/io/File;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v9, v0, v5

    .line 578
    .local v9, "subDir":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 579
    invoke-virtual {v9}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 580
    .local v3, "fileList":[Ljava/io/File;
    if-eqz v3, :cond_0

    .line 581
    move-object v1, v3

    .local v1, "arr$":[Ljava/io/File;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v4, 0x0

    .end local v5    # "i$":I
    .restart local v4    # "i$":I
    :goto_1
    if-ge v4, v7, :cond_0

    aget-object v2, v1, v4

    .line 582
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 581
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 586
    .end local v1    # "arr$":[Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fileList":[Ljava/io/File;
    .end local v4    # "i$":I
    .end local v7    # "len$":I
    :cond_0
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 577
    add-int/lit8 v4, v5, 0x1

    .restart local v4    # "i$":I
    move v5, v4

    .end local v4    # "i$":I
    .restart local v5    # "i$":I
    goto :goto_0

    .line 589
    .end local v5    # "i$":I
    .end local v9    # "subDir":Ljava/io/File;
    :cond_1
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    .line 591
    .end local v10    # "subDirList":[Ljava/io/File;
    :cond_2
    return-void
.end method

.method private deleteCardTypeImageFiles(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "cardType"    # Ljava/lang/String;

    .prologue
    .line 594
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    const-string v8, "image"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v7

    invoke-direct {v6, v7, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 595
    .local v6, "providerDirectory":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v6, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 596
    .local v1, "cardNameDirectory":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 597
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 598
    .local v3, "fileList":[Ljava/io/File;
    if-eqz v3, :cond_0

    .line 599
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 600
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 599
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 603
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 604
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 606
    .end local v3    # "fileList":[Ljava/io/File;
    :cond_1
    return-void
.end method

.method private deleteCards(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 25
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 454
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    .line 455
    .local v21, "deletedCardMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "card_channel"

    aput-object v4, v5, v3

    .line 456
    .local v5, "projection":[Ljava/lang/String;
    const-string v4, "card"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 457
    .local v20, "cursor":Landroid/database/Cursor;
    if-eqz v20, :cond_2

    .line 458
    :goto_0
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 459
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 460
    .local v23, "id":I
    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 461
    .local v17, "channel":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 462
    const-string v17, "channel_not_defined"

    .line 464
    :cond_0
    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 466
    .end local v17    # "channel":Ljava/lang/String;
    .end local v23    # "id":I
    :cond_1
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 469
    :cond_2
    const-string v3, "card"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v24

    .line 470
    .local v24, "rowCount":I
    if-lez v24, :cond_6

    .line 473
    invoke-virtual/range {v21 .. v21}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v15

    .line 474
    .local v15, "cardEntryList":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Map$Entry;

    .line 475
    .local v14, "cardEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 476
    .local v23, "id":Ljava/lang/String;
    invoke-interface {v14}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 478
    .restart local v17    # "channel":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    move-object/from16 v0, v23

    invoke-static {v3, v0}, Lcom/samsung/android/magazine/service/MagazineService;->cancelExpirationTimer(Landroid/content/Context;Ljava/lang/String;)V

    .line 480
    const/16 v19, 0x0

    .line 481
    .local v19, "channelPackageName":Ljava/lang/String;
    const-string v3, "channel_not_defined"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 482
    const/4 v3, 0x1

    new-array v8, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "package_name"

    aput-object v4, v8, v3

    .line 483
    .local v8, "projection2":[Ljava/lang/String;
    const-string v9, "key=?"

    .line 484
    .local v9, "selection2":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v17, v10, v3

    .line 485
    .local v10, "selectionArgs2":[Ljava/lang/String;
    const-string v7, "channel"

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v6, p1

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 486
    .local v18, "channelCursor":Landroid/database/Cursor;
    if-eqz v18, :cond_4

    .line 487
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 488
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 490
    :cond_3
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 496
    .end local v8    # "projection2":[Ljava/lang/String;
    .end local v9    # "selection2":Ljava/lang/String;
    .end local v10    # "selectionArgs2":[Ljava/lang/String;
    .end local v18    # "channelCursor":Landroid/database/Cursor;
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI_DELETE_NOTIFICATION:Landroid/net/Uri;

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-static {v3, v4, v0, v1}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-static {v3, v0, v4}, Lcom/samsung/android/magazine/service/Notifier;->sendCardDeletedBroadcast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_1

    .line 501
    .end local v14    # "cardEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v17    # "channel":Ljava/lang/String;
    .end local v19    # "channelPackageName":Ljava/lang/String;
    .end local v23    # "id":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {v21 .. v21}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v16

    .line 502
    .local v16, "cardIdList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 503
    .restart local v23    # "id":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "card_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 504
    .restart local v9    # "selection2":Ljava/lang/String;
    const-string v3, "card_element"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 505
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteCardImageFiles(Ljava/lang/String;)V

    goto :goto_2

    .line 509
    .end local v9    # "selection2":Ljava/lang/String;
    .end local v15    # "cardEntryList":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v16    # "cardIdList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    .end local v23    # "id":Ljava/lang/String;
    :cond_6
    return v24
.end method

.method private deleteChannels(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 19
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 609
    const/16 v17, 0x0

    .line 611
    .local v17, "rowCount":I
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 612
    .local v13, "channelKeyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x1

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "key"

    aput-object v4, v5, v3

    .line 613
    .local v5, "projection":[Ljava/lang/String;
    const-string v4, "channel"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 614
    .local v15, "cursor":Landroid/database/Cursor;
    if-eqz v15, :cond_1

    .line 615
    :goto_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 616
    const/4 v3, 0x0

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 618
    :cond_0
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 621
    :cond_1
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 623
    const-string v3, "channel"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v17

    .line 625
    :cond_2
    if-lez v17, :cond_3

    .line 626
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 628
    .local v12, "channelKey":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v14, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v12, v14, v3

    .line 629
    .local v14, "channelWhereArgs":[Ljava/lang/String;
    const-string v18, "channel_key=?"

    .line 630
    .local v18, "settingWhere":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2, v14}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteSettings(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    .line 638
    const-string v11, "card_channel=?"

    .line 639
    .local v11, "cardWhere":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11, v14}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteCards(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    .line 643
    .end local v11    # "cardWhere":Ljava/lang/String;
    .end local v12    # "channelKey":Ljava/lang/String;
    .end local v14    # "channelWhereArgs":[Ljava/lang/String;
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v18    # "settingWhere":Ljava/lang/String;
    :cond_3
    return v17
.end method

.method private deleteProvidedCardTypes(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 26
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 647
    const-string v4, "provided_card_type"

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 648
    .local v14, "cursor":Landroid/database/Cursor;
    if-eqz v14, :cond_1

    .line 650
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 651
    .local v16, "dataHolderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;>;"
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 652
    const-string v3, "provider"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 653
    .local v20, "provider":Ljava/lang/String;
    const-string v3, "card_type_key"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 654
    .local v11, "cardTypeKey":Ljava/lang/String;
    const-string v3, "channel_key"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 655
    .local v13, "channel":Ljava/lang/String;
    new-instance v15, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;

    move-object/from16 v0, v20

    invoke-direct {v15, v0, v11, v13}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    .local v15, "dataHolder":Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 658
    .end local v11    # "cardTypeKey":Ljava/lang/String;
    .end local v13    # "channel":Ljava/lang/String;
    .end local v15    # "dataHolder":Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;
    .end local v20    # "provider":Ljava/lang/String;
    :cond_0
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 662
    new-instance v21, Ljava/lang/StringBuilder;

    const-string v3, "provider"

    move-object/from16 v0, v21

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 663
    .local v21, "sb1":Ljava/lang/StringBuilder;
    const-string v3, "=? AND "

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 664
    const-string v3, "card_type_key"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 665
    const-string v3, "=?"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 666
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 669
    .local v24, "settingWhere":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v3, "card_provider"

    move-object/from16 v0, v22

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 670
    .local v22, "sb2":Ljava/lang/StringBuilder;
    const-string v3, "=? AND "

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671
    const-string v3, "card_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 672
    const-string v3, "=?"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 673
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 676
    .local v12, "cardWhere":Ljava/lang/String;
    new-instance v23, Ljava/lang/StringBuilder;

    const-string v3, "type"

    move-object/from16 v0, v23

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 677
    .local v23, "sb3":Ljava/lang/StringBuilder;
    const-string v3, "=? AND "

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 678
    const-string v3, "key1"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 679
    const-string v3, "=? AND "

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 680
    const-string v3, "key2"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 681
    const-string v3, "=?"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 683
    .local v18, "multiLanguageWhere":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;

    .line 685
    .restart local v15    # "dataHolder":Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v25, v0

    const/4 v3, 0x0

    iget-object v4, v15, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData1:Ljava/lang/String;

    aput-object v4, v25, v3

    const/4 v3, 0x1

    iget-object v4, v15, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData2:Ljava/lang/String;

    aput-object v4, v25, v3

    .line 687
    .local v25, "whereArgs":[Ljava/lang/String;
    const-string v3, "setting"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 689
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v12, v2}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteCards(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    .line 691
    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/4 v3, 0x0

    const-string v4, "card_type"

    aput-object v4, v19, v3

    const/4 v3, 0x1

    iget-object v4, v15, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData2:Ljava/lang/String;

    aput-object v4, v19, v3

    const/4 v3, 0x2

    iget-object v4, v15, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData1:Ljava/lang/String;

    aput-object v4, v19, v3

    .line 692
    .local v19, "multiLanguageWhereArgs":[Ljava/lang/String;
    const-string v3, "multi_language"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 694
    iget-object v3, v15, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData1:Ljava/lang/String;

    iget-object v4, v15, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData2:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteCardTypeImageFiles(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 698
    .end local v12    # "cardWhere":Ljava/lang/String;
    .end local v15    # "dataHolder":Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;
    .end local v16    # "dataHolderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;>;"
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v18    # "multiLanguageWhere":Ljava/lang/String;
    .end local v19    # "multiLanguageWhereArgs":[Ljava/lang/String;
    .end local v21    # "sb1":Ljava/lang/StringBuilder;
    .end local v22    # "sb2":Ljava/lang/StringBuilder;
    .end local v23    # "sb3":Ljava/lang/StringBuilder;
    .end local v24    # "settingWhere":Ljava/lang/String;
    .end local v25    # "whereArgs":[Ljava/lang/String;
    :cond_1
    const-string v3, "provided_card_type"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    return v3
.end method

.method private deleteProviders(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 23
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 702
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 703
    .local v18, "providerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x1

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "package_name"

    aput-object v4, v5, v3

    .line 704
    .local v5, "projection":[Ljava/lang/String;
    const-string v4, "provider"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 705
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_1

    .line 706
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 707
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 709
    :cond_0
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 713
    :cond_1
    const/16 v19, 0x0

    .line 714
    .local v19, "rowCount":I
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 716
    const-string v3, "provider"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v19

    .line 717
    if-lez v19, :cond_2

    .line 718
    const-string v16, "provider=?"

    .line 719
    .local v16, "providedWhere":Ljava/lang/String;
    const-string v21, "provider=?"

    .line 720
    .local v21, "settingWhere":Ljava/lang/String;
    const-string v11, "card_provider=?"

    .line 722
    .local v11, "cardWhere":Ljava/lang/String;
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v3, "type"

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 723
    .local v20, "sb":Ljava/lang/StringBuilder;
    const-string v3, "=? AND "

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 724
    const-string v3, "key2"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 725
    const-string v3, "=?"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 726
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 727
    .local v14, "multiLanguageWhere":Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 728
    .local v17, "provider":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/4 v3, 0x0

    aput-object v17, v22, v3

    .line 730
    .local v22, "whereArgs":[Ljava/lang/String;
    const-string v3, "provided_card_type"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move-object/from16 v2, v22

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 732
    const-string v3, "setting"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 734
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v11, v2}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteCards(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    .line 736
    const/4 v3, 0x2

    new-array v15, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "card_type"

    aput-object v4, v15, v3

    const/4 v3, 0x1

    aput-object v17, v15, v3

    .line 737
    .local v15, "multiLanguageWhereArgs":[Ljava/lang/String;
    const-string v3, "multi_language"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 739
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteCardTypeImageFiles(Ljava/lang/String;)V

    goto :goto_1

    .line 743
    .end local v11    # "cardWhere":Ljava/lang/String;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "multiLanguageWhere":Ljava/lang/String;
    .end local v15    # "multiLanguageWhereArgs":[Ljava/lang/String;
    .end local v16    # "providedWhere":Ljava/lang/String;
    .end local v17    # "provider":Ljava/lang/String;
    .end local v20    # "sb":Ljava/lang/StringBuilder;
    .end local v21    # "settingWhere":Ljava/lang/String;
    .end local v22    # "whereArgs":[Ljava/lang/String;
    :cond_2
    return v19
.end method

.method private deleteSettings(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 16
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 747
    const/4 v3, 0x3

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "channel_key"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "provider"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "card_type_key"

    aput-object v4, v5, v3

    .line 752
    .local v5, "projection":[Ljava/lang/String;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 753
    .local v13, "dataHolderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;>;"
    const-string v4, "setting"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 754
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_1

    .line 755
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 757
    new-instance v12, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;

    const/4 v3, 0x0

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x2

    invoke-interface {v11, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v12, v3, v4, v6}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    .local v12, "dataHolder":Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 760
    .end local v12    # "dataHolder":Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;
    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 763
    :cond_1
    const/4 v15, 0x0

    .line 764
    .local v15, "rowCount":I
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 766
    const-string v3, "setting"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 767
    if-lez v15, :cond_2

    .line 770
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;

    .line 771
    .restart local v12    # "dataHolder":Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    iget-object v4, v12, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData2:Ljava/lang/String;

    iget-object v6, v12, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData1:Ljava/lang/String;

    iget-object v7, v12, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;->mData3:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v3, v4, v6, v7, v8}, Lcom/samsung/android/magazine/service/Notifier;->sendCardTypeSubscriptionChangedBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 777
    .end local v12    # "dataHolder":Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider$DataHolder;
    .end local v14    # "i$":Ljava/util/Iterator;
    :cond_2
    return v15
.end method

.method private insertCard(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 18
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 973
    const-wide/16 v16, -0x2

    .line 974
    .local v16, "rowId":J
    const-string v2, "card_provider"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 975
    .local v14, "provider":Ljava/lang/String;
    const-string v2, "card_type"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 976
    .local v10, "cardType":Ljava/lang/String;
    const-string v2, "card_channel"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 978
    .local v11, "channel":Ljava/lang/String;
    const/4 v6, 0x0

    .line 980
    .local v6, "whereArgs":[Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    const-string v2, "provider"

    invoke-direct {v15, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 981
    .local v15, "sb":Ljava/lang/StringBuilder;
    const-string v2, "=? AND "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 982
    const-string v2, "card_type_key"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 983
    const-string v2, "=?"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 984
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 985
    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "whereArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    aput-object v14, v6, v2

    const/4 v2, 0x1

    aput-object v10, v6, v2

    .line 995
    .restart local v6    # "whereArgs":[Ljava/lang/String;
    :goto_0
    const-string v3, "provided_card_type"

    const/4 v4, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 996
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_1

    .line 997
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 998
    const-string v2, "card"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v16

    .line 1000
    :cond_0
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1002
    :cond_1
    const-wide/16 v2, -0x1

    cmp-long v2, v16, v2

    if-lez v2, :cond_4

    .line 1003
    const-string v2, "expiration_time"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v13

    .line 1004
    .local v13, "expirationTime":Ljava/lang/Long;
    if-eqz v13, :cond_2

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 1005
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-wide/from16 v0, v16

    invoke-static {v2, v0, v1, v4, v5}, Lcom/samsung/android/magazine/service/MagazineService;->setExpirationTimer(Landroid/content/Context;JJ)V

    .line 1013
    .end local v13    # "expirationTime":Ljava/lang/Long;
    :cond_2
    :goto_1
    return-wide v16

    .line 988
    .end local v12    # "cursor":Landroid/database/Cursor;
    :cond_3
    const-string v2, " AND ("

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 989
    const-string v2, "channel_key"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 990
    const-string v2, " IS NULL OR "

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 991
    const-string v2, "channel_key"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 992
    const-string v2, "=?)"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 993
    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "whereArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    aput-object v14, v6, v2

    const/4 v2, 0x1

    aput-object v10, v6, v2

    const/4 v2, 0x2

    aput-object v11, v6, v2

    .restart local v6    # "whereArgs":[Ljava/lang/String;
    goto :goto_0

    .line 1009
    .restart local v12    # "cursor":Landroid/database/Cursor;
    :cond_4
    const-wide/16 v2, -0x2

    cmp-long v2, v16, v2

    if-nez v2, :cond_2

    .line 1010
    const-string v2, "MagazineService::AdministratorContentProvider"

    const-string v3, "Unregistered card type. Can not insert a card."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private insertCardElement(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 12
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1017
    const-string v0, "card_id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    .line 1018
    .local v8, "cardId":Ljava/lang/Integer;
    if-nez v8, :cond_0

    .line 1019
    const-wide/16 v0, -0x1

    .line 1041
    :goto_0
    return-wide v0

    .line 1022
    :cond_0
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 1023
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1024
    .local v3, "selection":Ljava/lang/String;
    const-string v1, "card"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1025
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 1026
    :cond_1
    if-eqz v9, :cond_2

    .line 1027
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1029
    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 1031
    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1033
    const-string v0, "type"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1034
    .local v11, "type":Ljava/lang/String;
    if-eqz v11, :cond_4

    const-string v0, "image"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1035
    const-string v0, "data1"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1036
    .local v10, "imageUri":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1037
    const-string v0, "data1"

    const-string v1, "com.samsung.android.magazine.provider.administrator"

    const-string v4, "com.samsung.android.magazine.provider.cardchannel"

    invoke-virtual {v10, v1, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1041
    .end local v10    # "imageUri":Ljava/lang/String;
    :cond_4
    const-string v0, "card_element"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto :goto_0
.end method

.method private insertChannel(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 12
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v5, 0x0

    .line 1045
    const-string v0, "key"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1046
    .local v9, "key":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1047
    const-string v0, "MagazineService::AdministratorContentProvider"

    const-string v1, "insertChannel: The value of key column is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1048
    const-wide/16 v10, -0x1

    .line 1071
    :cond_0
    :goto_0
    return-wide v10

    .line 1051
    :cond_1
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 1052
    .local v2, "projection1":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 1054
    .local v4, "selectionArgs1":[Ljava/lang/String;
    const-string v3, "key=?"

    .line 1057
    .local v3, "selection1":Ljava/lang/String;
    const-string v1, "channel"

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1058
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_3

    .line 1059
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 1060
    const-string v0, "MagazineService::AdministratorContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insertChannel: channel key already exists: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1062
    const-wide/16 v10, -0x2

    goto :goto_0

    .line 1064
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1067
    :cond_3
    const-string v0, "channel"

    invoke-virtual {p1, v0, v5, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v10

    .line 1068
    .local v10, "rowId":J
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-gez v0, :cond_0

    .line 1069
    const-string v0, "MagazineService::AdministratorContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to insert a row to channel table: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private insertMultiLanguage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 13
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1075
    const-string v0, "type"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1076
    .local v12, "type":Ljava/lang/String;
    const-string v0, "card_type"

    invoke-virtual {v12, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1077
    const-string v0, "MagazineService::AdministratorContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported type of multi_language: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1078
    const-wide/16 v0, -0x1

    .line 1109
    :goto_0
    return-wide v0

    .line 1080
    :cond_0
    const-string v0, "key1"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1081
    .local v8, "cardType":Ljava/lang/String;
    const-string v0, "key2"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1082
    .local v10, "provider":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1083
    :cond_1
    const-string v0, "MagazineService::AdministratorContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Keys of multi_language are empty: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1084
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 1087
    :cond_2
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v0, "provider"

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1088
    .local v11, "sb":Ljava/lang/StringBuilder;
    const-string v0, "=? AND "

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1089
    const-string v0, "card_type_key"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1090
    const-string v0, "=?"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1091
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v10, v4, v0

    const/4 v0, 0x1

    aput-object v8, v4, v0

    .line 1092
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 1094
    .local v2, "projection":[Ljava/lang/String;
    const-string v1, "provided_card_type"

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1095
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_4

    .line 1096
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1097
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1109
    const-string v0, "multi_language"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_0

    .line 1100
    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1101
    const-string v0, "MagazineService::AdministratorContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Card type is not registered.: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    const-wide/16 v0, -0x1

    goto/16 :goto_0

    .line 1106
    :cond_4
    const-wide/16 v0, -0x1

    goto/16 :goto_0
.end method

.method private insertProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 24
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1113
    const-string v2, "card_type_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1114
    .local v15, "cardType":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1115
    const-string v2, "MagazineService::AdministratorContentProvider"

    const-string v3, "insertProvidedCardType: The value of card_type_key column is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    const-wide/16 v20, -0x1

    .line 1178
    :cond_0
    :goto_0
    return-wide v20

    .line 1118
    :cond_1
    const-string v2, "provider"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1119
    .local v19, "provider":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1120
    const-string v2, "MagazineService::AdministratorContentProvider"

    const-string v3, "insertProvidedCardType: The value of provider column is empty."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1121
    const-wide/16 v20, -0x1

    goto :goto_0

    .line 1123
    :cond_2
    const-string v2, "channel_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1125
    .local v16, "channel":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    .line 1126
    .local v4, "projection":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v19, v6, v2

    .line 1127
    .local v6, "selectionArgs1":[Ljava/lang/String;
    const-string v5, "package_name=?"

    .line 1129
    .local v5, "selection1":Ljava/lang/String;
    const-string v3, "provider"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 1130
    .local v23, "typeCursor":Landroid/database/Cursor;
    if-eqz v23, :cond_3

    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_5

    .line 1131
    :cond_3
    const-string v2, "MagazineService::AdministratorContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insertProvidedCardType: provider is not registered.: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1132
    if-eqz v23, :cond_4

    .line 1133
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 1134
    :cond_4
    const-wide/16 v20, -0x3

    goto :goto_0

    .line 1137
    :cond_5
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 1139
    const/4 v2, 0x1

    new-array v9, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v9, v2

    .line 1140
    .local v9, "projection2":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 1143
    .local v11, "selectionArgs2":[Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v2, "provider"

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1144
    .local v22, "sb2":Ljava/lang/StringBuilder;
    const-string v2, "=? AND "

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145
    const-string v2, "card_type_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146
    const-string v2, "=?"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1147
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1148
    const/4 v2, 0x2

    new-array v11, v2, [Ljava/lang/String;

    .end local v11    # "selectionArgs2":[Ljava/lang/String;
    const/4 v2, 0x0

    aput-object v19, v11, v2

    const/4 v2, 0x1

    aput-object v15, v11, v2

    .line 1158
    .restart local v11    # "selectionArgs2":[Ljava/lang/String;
    :goto_1
    const-string v8, "provided_card_type"

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 1159
    .local v17, "cursor":Landroid/database/Cursor;
    if-eqz v17, :cond_8

    .line 1160
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_7

    .line 1162
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 1163
    const-wide/16 v20, -0x2

    goto/16 :goto_0

    .line 1151
    .end local v17    # "cursor":Landroid/database/Cursor;
    :cond_6
    const-string v2, " AND ("

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1152
    const-string v2, "channel_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1153
    const-string v2, "=? OR "

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1154
    const-string v2, "channel_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1155
    const-string v2, " IS NULL)"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1156
    const/4 v2, 0x3

    new-array v11, v2, [Ljava/lang/String;

    .end local v11    # "selectionArgs2":[Ljava/lang/String;
    const/4 v2, 0x0

    aput-object v19, v11, v2

    const/4 v2, 0x1

    aput-object v15, v11, v2

    const/4 v2, 0x2

    aput-object v16, v11, v2

    .restart local v11    # "selectionArgs2":[Ljava/lang/String;
    goto :goto_1

    .line 1165
    .restart local v17    # "cursor":Landroid/database/Cursor;
    :cond_7
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 1168
    :cond_8
    const-string v2, "icon_image"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1169
    .local v18, "imageUri":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1170
    const-string v2, "icon_image"

    const-string v3, "com.samsung.android.magazine.provider.administrator"

    const-string v7, "com.samsung.android.magazine.provider.cardchannel"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1173
    :cond_9
    const-string v2, "last_modified_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1174
    const-string v2, "provided_card_type"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v20

    .line 1175
    .local v20, "rowId":J
    const-wide/16 v2, 0x0

    cmp-long v2, v20, v2

    if-gez v2, :cond_0

    .line 1176
    const-string v2, "MagazineService::AdministratorContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to insert a row to provided_card_type table. "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private insertProvider(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 12
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 1183
    const-string v0, "package_name"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1184
    .local v9, "packageName":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1185
    const-string v0, "MagazineService::AdministratorContentProvider"

    const-string v1, "insertProvider: The value of package_name column is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1186
    const-wide/16 v10, -0x1

    .line 1209
    :cond_0
    :goto_0
    return-wide v10

    .line 1189
    :cond_1
    new-array v2, v6, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 1190
    .local v2, "projection":[Ljava/lang/String;
    new-array v4, v6, [Ljava/lang/String;

    aput-object v9, v4, v1

    .line 1192
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v3, "package_name=?"

    .line 1195
    .local v3, "selection":Ljava/lang/String;
    const-string v1, "provider"

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1196
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_3

    .line 1197
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 1198
    const-string v0, "MagazineService::AdministratorContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insertProvider: provider already exists: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1199
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1200
    const-wide/16 v10, -0x2

    goto :goto_0

    .line 1202
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1205
    :cond_3
    const-string v0, "provider"

    invoke-virtual {p1, v0, v5, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v10

    .line 1206
    .local v10, "rowId":J
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-gez v0, :cond_0

    .line 1207
    const-string v0, "MagazineService::AdministratorContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to insert a row to provider table: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private insertSetting(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J
    .locals 26
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1215
    const-string v2, "channel_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1217
    .local v16, "channelKey":Ljava/lang/String;
    const-string v2, "section_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 1218
    .local v24, "sectionKey":Ljava/lang/String;
    const-string v2, "provider"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1219
    .local v19, "providerName":Ljava/lang/String;
    const-string v2, "card_type_key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1222
    .local v15, "cardTypeKey":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v2, "provider"

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1223
    .local v22, "sb1":Ljava/lang/StringBuilder;
    const-string v2, "=? AND "

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1224
    const-string v2, "card_type_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1225
    const-string v2, "=? AND ("

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1226
    const-string v2, "channel_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1227
    const-string v2, " IS NULL OR "

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1228
    const-string v2, "channel_key"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1229
    const-string v2, "=?)"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1230
    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v19, v6, v2

    const/4 v2, 0x1

    aput-object v15, v6, v2

    const/4 v2, 0x2

    aput-object v16, v6, v2

    .line 1231
    .local v6, "selectionArgs1":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    .line 1232
    .local v4, "projection1":[Ljava/lang/String;
    const-string v3, "provided_card_type"

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 1233
    .local v25, "typeCursor":Landroid/database/Cursor;
    if-eqz v25, :cond_0

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_3

    .line 1234
    :cond_0
    if-eqz v25, :cond_1

    .line 1235
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 1237
    :cond_1
    const-string v2, "MagazineService::AdministratorContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to insert a row to Setting table: The card type is not provided. ("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1238
    const-wide/16 v20, -0x1

    .line 1270
    :cond_2
    :goto_0
    return-wide v20

    .line 1240
    :cond_3
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 1244
    new-instance v23, Ljava/lang/StringBuilder;

    const-string v2, "channel_key"

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1245
    .local v23, "sb2":Ljava/lang/StringBuilder;
    const-string v2, "=? AND "

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1246
    const-string v2, "provider"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1247
    const-string v2, "=? AND "

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1248
    const-string v2, "card_type_key"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1249
    const-string v2, "=?"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1250
    const/4 v2, 0x1

    new-array v9, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v9, v2

    .line 1251
    .local v9, "projection2":[Ljava/lang/String;
    const/4 v2, 0x3

    new-array v11, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v16, v11, v2

    const/4 v2, 0x1

    aput-object v19, v11, v2

    const/4 v2, 0x2

    aput-object v15, v11, v2

    .line 1253
    .local v11, "selectionArgs2":[Ljava/lang/String;
    const-string v8, "setting"

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 1254
    .local v17, "cursor":Landroid/database/Cursor;
    if-eqz v17, :cond_5

    .line 1255
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 1256
    const-string v2, "MagazineService::AdministratorContentProvider"

    const-string v3, "Can not insert a row to setting table. The setting already exists."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 1258
    const-wide/16 v20, -0x2

    goto :goto_0

    .line 1260
    :cond_4
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 1263
    :cond_5
    const-string v2, "setting"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v20

    .line 1264
    .local v20, "rowId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v20, v2

    if-lez v2, :cond_2

    .line 1265
    const-string v2, "enable"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v18

    .line 1266
    .local v18, "enable":Ljava/lang/Integer;
    if-eqz v18, :cond_2

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1267
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-static {v2, v0, v1, v15, v3}, Lcom/samsung/android/magazine/service/Notifier;->sendCardTypeSubscriptionChangedBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method private updateCards(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1604
    const-string v0, "card_key"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1605
    const-string v0, "card_provider"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1606
    const-string v0, "card_type"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1607
    const-string v0, "card_channel"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1608
    invoke-virtual {p2}, Landroid/content/ContentValues;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1609
    const-string v0, "card"

    invoke-virtual {p1, v0, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1612
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateChannels(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 17
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1617
    const/16 v16, 0x0

    .line 1619
    .local v16, "stateChanged":Z
    const-string v4, "key"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1620
    const-string v4, "package_name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1622
    const-string v4, "enable"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1623
    const/16 v16, 0x1

    .line 1626
    :cond_0
    const/4 v15, 0x0

    .line 1627
    .local v15, "rowCount":I
    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 1628
    const-string v4, "channel"

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 1629
    if-lez v15, :cond_3

    if-eqz v16, :cond_3

    .line 1630
    const/4 v4, 0x2

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "key"

    aput-object v5, v6, v4

    const/4 v4, 0x1

    const-string v5, "enable"

    aput-object v5, v6, v4

    .line 1634
    .local v6, "projection":[Ljava/lang/String;
    const-string v5, "channel"

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1635
    .local v13, "cursor":Landroid/database/Cursor;
    if-eqz v13, :cond_3

    .line 1636
    :goto_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1637
    const/4 v4, 0x0

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1638
    .local v12, "channelKey":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 1640
    .local v14, "enable":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    const/4 v4, 0x1

    if-ne v14, v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    invoke-static {v5, v12, v4}, Lcom/samsung/android/magazine/service/Notifier;->sendChannelRunningStateChangedBroadcast(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 1642
    .end local v12    # "channelKey":Ljava/lang/String;
    .end local v14    # "enable":I
    :cond_2
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1646
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v13    # "cursor":Landroid/database/Cursor;
    :cond_3
    return v15
.end method

.method private updateMultiLanguages(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1651
    const-string v0, "type"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1652
    const-string v0, "key1"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1653
    const-string v0, "key2"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1654
    const-string v0, "language_code"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1656
    invoke-virtual {p2}, Landroid/content/ContentValues;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1657
    const-string v0, "multi_language"

    invoke-virtual {p1, v0, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1660
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateProviders(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1664
    const-string v0, "package_name"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1666
    invoke-virtual {p2}, Landroid/content/ContentValues;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1667
    const-string v0, "provider"

    invoke-virtual {p1, v0, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1670
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateSettings(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 19
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1674
    const/16 v18, 0x0

    .line 1675
    .local v18, "stateChanged":Z
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1676
    .local v13, "contentValues":Landroid/content/ContentValues;
    const-string v3, "visible"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1677
    const-string v3, "visible"

    const-string v4, "visible"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1679
    :cond_0
    const-string v3, "enable"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1680
    const-string v3, "enable"

    const-string v4, "enable"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1681
    const/16 v18, 0x1

    .line 1683
    :cond_1
    const/16 v17, 0x0

    .line 1684
    .local v17, "rowCount":I
    invoke-virtual/range {p2 .. p2}, Landroid/content/ContentValues;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 1685
    const-string v3, "setting"

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v0, v3, v13, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v17

    .line 1686
    if-lez v17, :cond_4

    if-eqz v18, :cond_4

    .line 1687
    const/4 v3, 0x4

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "channel_key"

    aput-object v4, v5, v3

    const/4 v3, 0x1

    const-string v4, "provider"

    aput-object v4, v5, v3

    const/4 v3, 0x2

    const-string v4, "card_type_key"

    aput-object v4, v5, v3

    const/4 v3, 0x3

    const-string v4, "enable"

    aput-object v4, v5, v3

    .line 1694
    .local v5, "projection":[Ljava/lang/String;
    const-string v4, "setting"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 1695
    .local v14, "cursor":Landroid/database/Cursor;
    if-eqz v14, :cond_4

    .line 1696
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1697
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1698
    .local v12, "channelName":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1699
    .local v16, "providerName":Ljava/lang/String;
    const/4 v3, 0x2

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1700
    .local v11, "cardTypeName":Ljava/lang/String;
    const/4 v3, 0x3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 1703
    .local v15, "enable":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    if-ne v15, v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    move-object/from16 v0, v16

    invoke-static {v4, v0, v12, v11, v3}, Lcom/samsung/android/magazine/service/Notifier;->sendCardTypeSubscriptionChangedBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 1706
    .end local v11    # "cardTypeName":Ljava/lang/String;
    .end local v12    # "channelName":Ljava/lang/String;
    .end local v15    # "enable":I
    .end local v16    # "providerName":Ljava/lang/String;
    :cond_3
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 1711
    .end local v5    # "projection":[Ljava/lang/String;
    .end local v14    # "cursor":Landroid/database/Cursor;
    :cond_4
    return v17
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 38
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "valuesArray"    # [Landroid/content/ContentValues;

    .prologue
    .line 164
    :try_start_0
    move-object/from16 v0, p2

    array-length v3, v0

    const/4 v6, 0x1

    if-ge v3, v6, :cond_0

    .line 165
    const/16 v33, 0x0

    .line 332
    :goto_0
    return v33

    .line 168
    :cond_0
    sget-object v3, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 327
    invoke-super/range {p0 .. p2}, Landroid/content/ContentProvider;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v33

    goto :goto_0

    .line 171
    :sswitch_0
    const/16 v33, 0x0

    .line 172
    .local v33, "rowCount":I
    monitor-enter p0
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 174
    .local v2, "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 176
    const/4 v3, 0x0

    aget-object v3, p2, v3

    const-string v6, "card_id"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v21

    .line 177
    .local v21, "cardId":Ljava/lang/Integer;
    if-eqz v21, :cond_8

    .line 178
    const/16 v20, 0x0

    .line 179
    .local v20, "cardChannel":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "card_channel"

    aput-object v6, v4, v3

    .line 180
    .local v4, "projection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 181
    .local v5, "selection":Ljava/lang/String;
    const-string v3, "card"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    .line 182
    .local v27, "cursor":Landroid/database/Cursor;
    if-eqz v27, :cond_2

    .line 183
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 184
    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 185
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 186
    const-string v20, "channel_not_defined"

    .line 189
    :cond_1
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 192
    :cond_2
    if-eqz v20, :cond_8

    .line 193
    move-object/from16 v19, p2

    .local v19, "arr$":[Landroid/content/ContentValues;
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v31, v0

    .local v31, "len$":I
    const/16 v29, 0x0

    .local v29, "i$":I
    :goto_1
    move/from16 v0, v29

    move/from16 v1, v31

    if-ge v0, v1, :cond_5

    aget-object v26, v19, v29

    .line 194
    .local v26, "contentValues":Landroid/content/ContentValues;
    const-string v3, "type"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 195
    .local v37, "type":Ljava/lang/String;
    if-eqz v37, :cond_3

    const-string v3, "image"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 196
    const-string v3, "data1"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 197
    .local v30, "imageUri":Ljava/lang/String;
    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 198
    const-string v3, "data1"

    const-string v6, "com.samsung.android.magazine.provider.administrator"

    const-string v7, "com.samsung.android.magazine.provider.cardchannel"

    move-object/from16 v0, v30

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    .end local v30    # "imageUri":Ljava/lang/String;
    :cond_3
    const-string v3, "card_element"

    const/4 v6, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v2, v3, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v34

    .line 203
    .local v34, "rowId":J
    const-wide/16 v6, -0x1

    cmp-long v3, v34, v6

    if-lez v3, :cond_4

    .line 204
    add-int/lit8 v33, v33, 0x1

    .line 193
    :cond_4
    add-int/lit8 v29, v29, 0x1

    goto :goto_1

    .line 208
    .end local v26    # "contentValues":Landroid/content/ContentValues;
    .end local v34    # "rowId":J
    .end local v37    # "type":Ljava/lang/String;
    :cond_5
    const/16 v25, 0x0

    .line 209
    .local v25, "channelPackageName":Ljava/lang/String;
    const-string v3, "channel_not_defined"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 210
    const/4 v3, 0x1

    new-array v8, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "package_name"

    aput-object v6, v8, v3

    .line 211
    .local v8, "projection2":[Ljava/lang/String;
    const-string v9, "key=?"

    .line 212
    .local v9, "selection2":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v20, v10, v3

    .line 213
    .local v10, "selectionArgs2":[Ljava/lang/String;
    const-string v7, "channel"

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v6, v2

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 214
    .local v24, "channelCursor":Landroid/database/Cursor;
    if-eqz v24, :cond_7

    .line 215
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 216
    const/4 v3, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 218
    :cond_6
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 224
    .end local v8    # "projection2":[Ljava/lang/String;
    .end local v9    # "selection2":Ljava/lang/String;
    .end local v10    # "selectionArgs2":[Ljava/lang/String;
    .end local v24    # "channelCursor":Landroid/database/Cursor;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/samsung/android/magazine/service/contract/CardChannelContract$Card;->CONTENT_URI_INSERT_NOTIFICATION:Landroid/net/Uri;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    invoke-static {v3, v6, v7, v0}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move-object/from16 v0, v25

    invoke-static {v3, v0, v6}, Lcom/samsung/android/magazine/service/Notifier;->sendCardAddedBroadcast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 231
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "selection":Ljava/lang/String;
    .end local v19    # "arr$":[Landroid/content/ContentValues;
    .end local v20    # "cardChannel":Ljava/lang/String;
    .end local v25    # "channelPackageName":Ljava/lang/String;
    .end local v27    # "cursor":Landroid/database/Cursor;
    .end local v29    # "i$":I
    .end local v31    # "len$":I
    :cond_8
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 232
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 233
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v21    # "cardId":Ljava/lang/Integer;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 329
    .end local v33    # "rowCount":I
    :catch_0
    move-exception v28

    .line 330
    .local v28, "e":Landroid/database/sqlite/SQLiteException;
    const-string v3, "MagazineService::AdministratorContentProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Database operation failed. : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 332
    const/16 v33, 0x0

    goto/16 :goto_0

    .line 238
    .end local v28    # "e":Landroid/database/sqlite/SQLiteException;
    :sswitch_1
    const/16 v33, 0x0

    .line 239
    .restart local v33    # "rowCount":I
    :try_start_3
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 240
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 241
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 243
    move-object/from16 v19, p2

    .restart local v19    # "arr$":[Landroid/content/ContentValues;
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v31, v0

    .restart local v31    # "len$":I
    const/16 v29, 0x0

    .restart local v29    # "i$":I
    :goto_2
    move/from16 v0, v29

    move/from16 v1, v31

    if-ge v0, v1, :cond_a

    aget-object v26, v19, v29

    .line 244
    .restart local v26    # "contentValues":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->insertProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v34

    .line 245
    .restart local v34    # "rowId":J
    const-wide/16 v6, -0x1

    cmp-long v3, v34, v6

    if-lez v3, :cond_9

    .line 246
    add-int/lit8 v33, v33, 0x1

    .line 243
    :cond_9
    add-int/lit8 v29, v29, 0x1

    goto :goto_2

    .line 252
    .end local v26    # "contentValues":Landroid/content/ContentValues;
    .end local v34    # "rowId":J
    :cond_a
    if-lez v33, :cond_c

    .line 253
    const/4 v3, 0x0

    aget-object v3, p2, v3

    const-string v6, "provider"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 254
    .local v32, "provider":Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, p2, v3

    const-string v6, "channel_key"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 255
    .local v23, "channel":Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 256
    const-string v23, "channel_not_defined"

    .line 259
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI_CHANGE_NOTIFICATION:Landroid/net/Uri;

    move-object/from16 v0, v32

    move-object/from16 v1, v23

    invoke-static {v3, v6, v0, v1}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    .end local v23    # "channel":Ljava/lang/String;
    .end local v32    # "provider":Ljava/lang/String;
    :cond_c
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 263
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 264
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v19    # "arr$":[Landroid/content/ContentValues;
    .end local v29    # "i$":I
    .end local v31    # "len$":I
    :catchall_1
    move-exception v3

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v3

    .line 270
    .end local v33    # "rowCount":I
    :sswitch_2
    const/16 v33, 0x0

    .line 271
    .restart local v33    # "rowCount":I
    monitor-enter p0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 272
    :try_start_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 273
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 276
    const/4 v3, 0x0

    aget-object v3, p2, v3

    const-string v6, "type"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 277
    .restart local v37    # "type":Ljava/lang/String;
    if-eqz v37, :cond_f

    const-string v3, "card_type"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 278
    const/4 v3, 0x0

    aget-object v3, p2, v3

    const-string v6, "key1"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 279
    .local v22, "cardType":Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, p2, v3

    const-string v6, "key2"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 280
    .restart local v32    # "provider":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_f

    invoke-static/range {v32 .. v32}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 282
    new-instance v36, Ljava/lang/StringBuilder;

    const-string v3, "provider"

    move-object/from16 v0, v36

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 283
    .local v36, "sb":Ljava/lang/StringBuilder;
    const-string v3, "=? AND "

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    const-string v3, "card_type_key"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    const-string v3, "=?"

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    const/4 v3, 0x2

    new-array v15, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v32, v15, v3

    const/4 v3, 0x1

    aput-object v22, v15, v3

    .line 287
    .local v15, "selectionArgs":[Ljava/lang/String;
    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v3

    .line 288
    .restart local v4    # "projection":[Ljava/lang/String;
    const-string v12, "provided_card_type"

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v11, v2

    move-object v13, v4

    invoke-virtual/range {v11 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    .line 289
    .restart local v27    # "cursor":Landroid/database/Cursor;
    if-eqz v27, :cond_f

    .line 290
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 291
    move-object/from16 v19, p2

    .restart local v19    # "arr$":[Landroid/content/ContentValues;
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v31, v0

    .restart local v31    # "len$":I
    const/16 v29, 0x0

    .restart local v29    # "i$":I
    :goto_3
    move/from16 v0, v29

    move/from16 v1, v31

    if-ge v0, v1, :cond_e

    aget-object v26, v19, v29

    .line 292
    .restart local v26    # "contentValues":Landroid/content/ContentValues;
    const-string v3, "multi_language"

    const/4 v6, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v2, v3, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v34

    .line 293
    .restart local v34    # "rowId":J
    const-wide/16 v6, -0x1

    cmp-long v3, v34, v6

    if-lez v3, :cond_d

    .line 294
    add-int/lit8 v33, v33, 0x1

    .line 291
    :cond_d
    add-int/lit8 v29, v29, 0x1

    goto :goto_3

    .line 298
    .end local v19    # "arr$":[Landroid/content/ContentValues;
    .end local v26    # "contentValues":Landroid/content/ContentValues;
    .end local v29    # "i$":I
    .end local v31    # "len$":I
    .end local v34    # "rowId":J
    :cond_e
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 302
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v15    # "selectionArgs":[Ljava/lang/String;
    .end local v22    # "cardType":Ljava/lang/String;
    .end local v27    # "cursor":Landroid/database/Cursor;
    .end local v32    # "provider":Ljava/lang/String;
    .end local v36    # "sb":Ljava/lang/StringBuilder;
    :cond_f
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 303
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 305
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v37    # "type":Ljava/lang/String;
    :catchall_2
    move-exception v3

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v3

    .line 310
    .end local v33    # "rowCount":I
    :sswitch_3
    const/16 v33, 0x0

    .line 311
    .restart local v33    # "rowCount":I
    monitor-enter p0
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    .line 312
    :try_start_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 313
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 315
    move-object/from16 v19, p2

    .restart local v19    # "arr$":[Landroid/content/ContentValues;
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v31, v0

    .restart local v31    # "len$":I
    const/16 v29, 0x0

    .restart local v29    # "i$":I
    :goto_4
    move/from16 v0, v29

    move/from16 v1, v31

    if-ge v0, v1, :cond_11

    aget-object v26, v19, v29

    .line 316
    .restart local v26    # "contentValues":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->insertSetting(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v34

    .line 317
    .restart local v34    # "rowId":J
    const-wide/16 v6, -0x1

    cmp-long v3, v34, v6

    if-lez v3, :cond_10

    .line 318
    add-int/lit8 v33, v33, 0x1

    .line 315
    :cond_10
    add-int/lit8 v29, v29, 0x1

    goto :goto_4

    .line 321
    .end local v26    # "contentValues":Landroid/content/ContentValues;
    .end local v34    # "rowId":J
    :cond_11
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 322
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 323
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v19    # "arr$":[Landroid/content/ContentValues;
    .end local v29    # "i$":I
    .end local v31    # "len$":I
    :catchall_3
    move-exception v3

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    throw v3
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 168
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x190 -> :sswitch_2
        0x1f4 -> :sswitch_1
        0x2bc -> :sswitch_3
    .end sparse-switch
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 352
    :try_start_0
    sget-object v3, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 443
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unsupported URI: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 445
    :catch_0
    move-exception v1

    .line 446
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    const-string v3, "MagazineService::AdministratorContentProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Database operation failed. : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 448
    const/4 v2, 0x0

    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    return v2

    .line 355
    :sswitch_0
    const/4 v2, 0x0

    .line 356
    .local v2, "rowCount":I
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 357
    :try_start_2
    iget-object v3, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 358
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 359
    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteCards(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 360
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 361
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 362
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v3

    .line 367
    .end local v2    # "rowCount":I
    :sswitch_1
    const/4 v2, 0x0

    .line 368
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 369
    :try_start_4
    iget-object v3, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 370
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 371
    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteCardElements(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 372
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 373
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 374
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v3

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v3

    .line 379
    .end local v2    # "rowCount":I
    :sswitch_2
    const/4 v2, 0x0

    .line 380
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 381
    :try_start_6
    iget-object v3, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 382
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 383
    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteChannels(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 384
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 385
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 386
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_2
    move-exception v3

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v3

    .line 391
    .end local v2    # "rowCount":I
    :sswitch_3
    const/4 v2, 0x0

    .line 392
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    .line 393
    :try_start_8
    iget-object v3, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 394
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "multi_language"

    invoke-virtual {v0, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 395
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_3
    move-exception v3

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    throw v3

    .line 400
    .end local v2    # "rowCount":I
    :sswitch_4
    const/4 v2, 0x0

    .line 401
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 402
    :try_start_a
    iget-object v3, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 403
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 404
    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteProvidedCardTypes(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 405
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 406
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 407
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_4
    move-exception v3

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :try_start_b
    throw v3

    .line 412
    .end local v2    # "rowCount":I
    :sswitch_5
    const/4 v2, 0x0

    .line 413
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_0

    .line 414
    :try_start_c
    iget-object v3, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 415
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 416
    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteProviders(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 417
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 418
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 419
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_5
    move-exception v3

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    :try_start_d
    throw v3

    .line 424
    .end local v2    # "rowCount":I
    :sswitch_6
    const/4 v2, 0x0

    .line 425
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_0

    .line 426
    :try_start_e
    iget-object v3, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 427
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 428
    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->deleteSettings(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 429
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 430
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 431
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_6
    move-exception v3

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    :try_start_f
    throw v3

    .line 441
    .end local v2    # "rowCount":I
    :sswitch_7
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unsupported opeation: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_0

    .line 352
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_7
        0xc8 -> :sswitch_1
        0xc9 -> :sswitch_7
        0x12c -> :sswitch_2
        0x12d -> :sswitch_7
        0x190 -> :sswitch_3
        0x191 -> :sswitch_7
        0x1f4 -> :sswitch_4
        0x1f5 -> :sswitch_7
        0x258 -> :sswitch_5
        0x259 -> :sswitch_7
        0x2bc -> :sswitch_6
        0x2bd -> :sswitch_7
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 795
    sget-object v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 825
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 797
    :sswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.administrator.card"

    .line 823
    :goto_0
    return-object v0

    .line 799
    :sswitch_1
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.administrator.card"

    goto :goto_0

    .line 801
    :sswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.administrator.card_element"

    goto :goto_0

    .line 803
    :sswitch_3
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.administrator.card_element"

    goto :goto_0

    .line 805
    :sswitch_4
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.administrator.channel"

    goto :goto_0

    .line 807
    :sswitch_5
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.administrator.channel"

    goto :goto_0

    .line 809
    :sswitch_6
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.administrator.multi_language"

    goto :goto_0

    .line 811
    :sswitch_7
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.administrator.multi_language"

    goto :goto_0

    .line 813
    :sswitch_8
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.administrator.provided_card_type"

    goto :goto_0

    .line 815
    :sswitch_9
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.administrator.provided_card_type"

    goto :goto_0

    .line 817
    :sswitch_a
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.administrator.provider"

    goto :goto_0

    .line 819
    :sswitch_b
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.administrator.provider"

    goto :goto_0

    .line 821
    :sswitch_c
    const-string v0, "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.administrator.setting"

    goto :goto_0

    .line 823
    :sswitch_d
    const-string v0, "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.administrator.setting"

    goto :goto_0

    .line 795
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_3
        0x12c -> :sswitch_4
        0x12d -> :sswitch_5
        0x190 -> :sswitch_6
        0x191 -> :sswitch_7
        0x1f4 -> :sswitch_8
        0x1f5 -> :sswitch_9
        0x258 -> :sswitch_a
        0x259 -> :sswitch_b
        0x2bc -> :sswitch_c
        0x2bd -> :sswitch_d
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 845
    :try_start_0
    sget-object v7, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v7, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 963
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unsupported URI: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 965
    :catch_0
    move-exception v3

    .line 966
    .local v3, "e":Landroid/database/sqlite/SQLiteException;
    const-string v7, "MagazineService::AdministratorContentProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Database operation failed. : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 967
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 968
    const/4 v6, 0x0

    .end local v3    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    return-object v6

    .line 848
    :sswitch_0
    const-wide/16 v8, 0x0

    .line 849
    .local v8, "rowId":J
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 850
    :try_start_2
    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v7}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 851
    .local v2, "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v2, p2}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->insertCard(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 852
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 853
    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-lez v7, :cond_0

    .line 854
    :try_start_3
    sget-object v7, Lcom/samsung/android/magazine/service/contract/AdministratorContract$Card;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v6

    goto :goto_0

    .line 852
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v7

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v7

    .line 857
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 862
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v8    # "rowId":J
    :sswitch_1
    const-wide/16 v8, 0x0

    .line 863
    .restart local v8    # "rowId":J
    monitor-enter p0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 864
    :try_start_6
    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v7}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 865
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v2, p2}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->insertCardElement(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 866
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 867
    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-lez v7, :cond_1

    .line 868
    :try_start_7
    sget-object v7, Lcom/samsung/android/magazine/service/contract/AdministratorContract$CardElement;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    move-result-object v6

    goto :goto_0

    .line 866
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v7

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v7

    .line 871
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 876
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v8    # "rowId":J
    :sswitch_2
    const/4 v6, 0x0

    .line 877
    .local v6, "resultUri":Landroid/net/Uri;
    monitor-enter p0
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 878
    :try_start_a
    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v7}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 879
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v2, p2}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->insertChannel(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 880
    .restart local v8    # "rowId":J
    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-lez v7, :cond_4

    .line 881
    const-string v7, "key"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 882
    .local v1, "channelKey":Ljava/lang/String;
    const-string v7, "enable"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 883
    .local v4, "enable":Ljava/lang/Integer;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v10, 0x1

    if-ne v7, v10, :cond_2

    .line 884
    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-static {v7, v1, v10}, Lcom/samsung/android/magazine/service/Notifier;->sendChannelRunningStateChangedBroadcast(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 886
    :cond_2
    sget-object v7, Lcom/samsung/android/magazine/service/contract/AdministratorContract$Channel;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 891
    .end local v1    # "channelKey":Ljava/lang/String;
    .end local v4    # "enable":Ljava/lang/Integer;
    :cond_3
    :goto_1
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v8    # "rowId":J
    :catchall_2
    move-exception v7

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    throw v7
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_0

    .line 888
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v8    # "rowId":J
    :cond_4
    const-wide/16 v10, -0x2

    cmp-long v7, v8, v10

    if-nez v7, :cond_3

    .line 889
    :try_start_c
    sget-object v7, Lcom/samsung/android/magazine/service/contract/AdministratorContract$Channel;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    move-result-object v6

    goto :goto_1

    .line 897
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v6    # "resultUri":Landroid/net/Uri;
    .end local v8    # "rowId":J
    :sswitch_3
    const-wide/16 v8, -0x1

    .line 898
    .restart local v8    # "rowId":J
    :try_start_d
    monitor-enter p0
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_0

    .line 899
    :try_start_e
    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v7}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 900
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v2, p2}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->insertMultiLanguage(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 901
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 902
    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-lez v7, :cond_5

    .line 903
    :try_start_f
    sget-object v7, Lcom/samsung/android/magazine/service/contract/AdministratorContract$MultiLanguage;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_0

    move-result-object v6

    goto/16 :goto_0

    .line 901
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_3
    move-exception v7

    :try_start_10
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    :try_start_11
    throw v7

    .line 906
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 911
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v8    # "rowId":J
    :sswitch_4
    const-wide/16 v8, -0x1

    .line 912
    .restart local v8    # "rowId":J
    monitor-enter p0
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_0

    .line 913
    :try_start_12
    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v7}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 914
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v2, p2}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->insertProvider(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 915
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    .line 916
    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-gtz v7, :cond_6

    const-wide/16 v10, -0x2

    cmp-long v7, v8, v10

    if-nez v7, :cond_7

    .line 917
    :cond_6
    :try_start_13
    sget-object v7, Lcom/samsung/android/magazine/service/contract/AdministratorContract$Provider;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_0

    move-result-object v6

    goto/16 :goto_0

    .line 915
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_4
    move-exception v7

    :try_start_14
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    :try_start_15
    throw v7

    .line 920
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 925
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v8    # "rowId":J
    :sswitch_5
    const/4 v6, 0x0

    .line 926
    .restart local v6    # "resultUri":Landroid/net/Uri;
    monitor-enter p0
    :try_end_15
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_15 .. :try_end_15} :catch_0

    .line 927
    :try_start_16
    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v7}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 928
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v2, p2}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->insertProvidedCardType(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 929
    .restart local v8    # "rowId":J
    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-lez v7, :cond_a

    .line 930
    const-string v7, "provider"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 931
    .local v5, "provider":Ljava/lang/String;
    const-string v7, "channel_key"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 932
    .local v0, "channel":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 933
    const-string v0, "channel_not_defined"

    .line 937
    :cond_8
    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    sget-object v10, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI_CHANGE_NOTIFICATION:Landroid/net/Uri;

    invoke-static {v7, v10, v5, v0}, Lcom/samsung/android/magazine/service/Notifier;->notifyChangeToChannel(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    sget-object v7, Lcom/samsung/android/magazine/service/contract/AdministratorContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 944
    .end local v0    # "channel":Ljava/lang/String;
    .end local v5    # "provider":Ljava/lang/String;
    :cond_9
    :goto_2
    monitor-exit p0

    goto/16 :goto_0

    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v8    # "rowId":J
    :catchall_5
    move-exception v7

    monitor-exit p0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_5

    :try_start_17
    throw v7
    :try_end_17
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_17 .. :try_end_17} :catch_0

    .line 941
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v8    # "rowId":J
    :cond_a
    const-wide/16 v10, -0x2

    cmp-long v7, v8, v10

    if-nez v7, :cond_9

    .line 942
    :try_start_18
    sget-object v7, Lcom/samsung/android/magazine/service/contract/AdministratorContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_5

    move-result-object v6

    goto :goto_2

    .line 950
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v6    # "resultUri":Landroid/net/Uri;
    .end local v8    # "rowId":J
    :sswitch_6
    const-wide/16 v8, -0x1

    .line 951
    .restart local v8    # "rowId":J
    :try_start_19
    monitor-enter p0
    :try_end_19
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_19 .. :try_end_19} :catch_0

    .line 952
    :try_start_1a
    iget-object v7, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v7}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 953
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v2, p2}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->insertSetting(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 954
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_6

    .line 955
    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-gtz v7, :cond_b

    const-wide/16 v10, -0x2

    cmp-long v7, v8, v10

    if-nez v7, :cond_c

    .line 956
    :cond_b
    :try_start_1b
    sget-object v7, Lcom/samsung/android/magazine/service/contract/AdministratorContract$Card;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1b .. :try_end_1b} :catch_0

    move-result-object v6

    goto/16 :goto_0

    .line 954
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_6
    move-exception v7

    :try_start_1c
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_6

    :try_start_1d
    throw v7
    :try_end_1d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1d .. :try_end_1d} :catch_0

    .line 959
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_c
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 845
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_5
        0x258 -> :sswitch_4
        0x2bc -> :sswitch_6
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 139
    const-string v0, "MagazineService Version"

    const-string v1, "Magazine-Administrator: 11"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-virtual {p0}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    .line 141
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/magazine/service/db/CardDbHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    .line 142
    iget-object v0, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    if-nez v0, :cond_0

    .line 143
    const/4 v0, 0x0

    .line 145
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 1729
    const/4 v4, 0x0

    .line 1731
    .local v4, "fileDescriptor":Landroid/os/ParcelFileDescriptor;
    sget-object v8, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v8, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 1820
    new-instance v8, Ljava/io/FileNotFoundException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unsupported URI: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1736
    :sswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    .line 1737
    .local v6, "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x5

    if-lt v8, v9, :cond_0

    .line 1738
    new-instance v1, Ljava/io/File;

    iget-object v8, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    const-string v9, "image"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v9

    const/4 v8, 0x1

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v1, v9, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1739
    .local v1, "directory":Ljava/io/File;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v8, 0x3

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v8, 0x4

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1740
    .local v5, "fileName":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1742
    .local v3, "file":Ljava/io/File;
    const-string v8, "r"

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1743
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1744
    const/high16 v8, 0x10000000

    invoke-static {v3, v8}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    .line 1823
    .end local v1    # "directory":Ljava/io/File;
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "fileName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v4

    .line 1747
    .restart local v1    # "directory":Ljava/io/File;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "fileName":Ljava/lang/String;
    :cond_1
    new-instance v8, Ljava/io/FileNotFoundException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "File does not exist: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1750
    :cond_2
    const-string v8, "w"

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1751
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1752
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1753
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 1761
    :cond_3
    :goto_1
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1767
    :goto_2
    const/high16 v8, 0x20000000

    invoke-static {v3, v8}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    goto :goto_0

    .line 1757
    :cond_4
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    goto :goto_1

    .line 1763
    :catch_0
    move-exception v2

    .line 1764
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 1770
    .end local v2    # "e":Ljava/io/IOException;
    :cond_5
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unsupported Mode: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1779
    .end local v1    # "directory":Ljava/io/File;
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v6    # "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    .line 1780
    .restart local v6    # "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x5

    if-lt v8, v9, :cond_0

    .line 1781
    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mContext:Landroid/content/Context;

    const-string v9, "image"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v9

    const/4 v8, 0x2

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v7, v9, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1782
    .local v7, "providerDirectory":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const/4 v8, 0x3

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v0, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1783
    .local v0, "cardNameDirectory":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    const/4 v8, 0x4

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v3, v0, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1784
    .restart local v3    # "file":Ljava/io/File;
    const-string v8, "r"

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1785
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1786
    const/high16 v8, 0x10000000

    invoke-static {v3, v8}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    goto/16 :goto_0

    .line 1789
    :cond_6
    new-instance v8, Ljava/io/FileNotFoundException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "File does not exist: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1792
    :cond_7
    const-string v8, "w"

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1793
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1794
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1795
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 1803
    :cond_8
    :goto_3
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1810
    :goto_4
    const/high16 v8, 0x20000000

    invoke-static {v3, v8}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    goto/16 :goto_0

    .line 1799
    :cond_9
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    goto :goto_3

    .line 1805
    :catch_1
    move-exception v2

    .line 1806
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "MagazineService::AdministratorContentProvider"

    const-string v9, "Failed to create image file for card name"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1807
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1814
    .end local v2    # "e":Ljava/io/IOException;
    :cond_a
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unsupported Mode: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1731
    :sswitch_data_0
    .sparse-switch
        0x6e -> :sswitch_0
        0x1fe -> :sswitch_1
    .end sparse-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 21
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1289
    :try_start_0
    sget-object v2, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1450
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1452
    :catch_0
    move-exception v20

    .line 1453
    .local v20, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "MagazineService::AdministratorContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Database operation failed. : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1454
    invoke-virtual/range {v20 .. v20}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 1455
    const/16 v19, 0x0

    .end local v20    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    return-object v19

    .line 1292
    :sswitch_0
    const/4 v6, 0x0

    .line 1293
    .local v6, "groupBy":Ljava/lang/String;
    if-eqz p2, :cond_0

    :try_start_1
    move-object/from16 v0, p2

    array-length v2, v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/4 v2, 0x0

    aget-object v2, p2, v2

    const-string v3, "card_key"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1295
    const-string v6, "card_key"

    .line 1297
    :cond_0
    const/16 v19, 0x0

    .line 1298
    .local v19, "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1299
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1300
    .local v1, "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "card"

    const/4 v7, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1301
    monitor-exit p0

    goto :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2

    .line 1306
    .end local v6    # "groupBy":Ljava/lang/String;
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1307
    .local v10, "whereClause":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1308
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1310
    :cond_1
    const/16 v19, 0x0

    .line 1311
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1312
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1313
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v8, "card"

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v7, v1

    move-object/from16 v9, p2

    move-object/from16 v11, p4

    move-object/from16 v14, p5

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1314
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v2

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v2

    .line 1319
    .end local v10    # "whereClause":Ljava/lang/String;
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_2
    const/16 v19, 0x0

    .line 1320
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1321
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1322
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v12, "card_element"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v11, v1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v18, p5

    invoke-virtual/range {v11 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1323
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_2
    move-exception v2

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v2

    .line 1328
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1329
    .restart local v10    # "whereClause":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1330
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1332
    :cond_2
    const/16 v19, 0x0

    .line 1333
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    .line 1334
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1335
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v8, "card_element"

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v7, v1

    move-object/from16 v9, p2

    move-object/from16 v11, p4

    move-object/from16 v14, p5

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1336
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_3
    move-exception v2

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    throw v2

    .line 1341
    .end local v10    # "whereClause":Ljava/lang/String;
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_4
    const/16 v19, 0x0

    .line 1342
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 1343
    :try_start_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1344
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v12, "channel"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v11, v1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v18, p5

    invoke-virtual/range {v11 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1345
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_4
    move-exception v2

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :try_start_b
    throw v2

    .line 1350
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1351
    .restart local v10    # "whereClause":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1352
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1354
    :cond_3
    const/16 v19, 0x0

    .line 1355
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_0

    .line 1356
    :try_start_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1357
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v8, "channel"

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v7, v1

    move-object/from16 v9, p2

    move-object/from16 v11, p4

    move-object/from16 v14, p5

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1358
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_5
    move-exception v2

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    :try_start_d
    throw v2

    .line 1363
    .end local v10    # "whereClause":Ljava/lang/String;
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_6
    const/16 v19, 0x0

    .line 1364
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_0

    .line 1365
    :try_start_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1366
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v12, "multi_language"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v11, v1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v18, p5

    invoke-virtual/range {v11 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1367
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_6
    move-exception v2

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    :try_start_f
    throw v2

    .line 1372
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1373
    .restart local v10    # "whereClause":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1374
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1376
    :cond_4
    const/16 v19, 0x0

    .line 1377
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_0

    .line 1378
    :try_start_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1379
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v8, "multi_language"

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v7, v1

    move-object/from16 v9, p2

    move-object/from16 v11, p4

    move-object/from16 v14, p5

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1380
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_7
    move-exception v2

    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_7

    :try_start_11
    throw v2

    .line 1385
    .end local v10    # "whereClause":Ljava/lang/String;
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_8
    const/16 v19, 0x0

    .line 1386
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_0

    .line 1387
    :try_start_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1388
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v12, "provided_card_type"

    const-string v16, "card_type_key"

    const/16 v17, 0x0

    move-object v11, v1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v18, p5

    invoke-virtual/range {v11 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1389
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_8
    move-exception v2

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_8

    :try_start_13
    throw v2

    .line 1394
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1395
    .restart local v10    # "whereClause":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1396
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1398
    :cond_5
    const/16 v19, 0x0

    .line 1399
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_0

    .line 1400
    :try_start_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1401
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v8, "provided_card_type"

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v7, v1

    move-object/from16 v9, p2

    move-object/from16 v11, p4

    move-object/from16 v14, p5

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1402
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_9
    move-exception v2

    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_9

    :try_start_15
    throw v2

    .line 1407
    .end local v10    # "whereClause":Ljava/lang/String;
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_a
    const/16 v19, 0x0

    .line 1408
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_15
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_15 .. :try_end_15} :catch_0

    .line 1409
    :try_start_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1410
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v12, "provider"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v11, v1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v18, p5

    invoke-virtual/range {v11 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1411
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_a
    move-exception v2

    monitor-exit p0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_a

    :try_start_17
    throw v2

    .line 1416
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1417
    .restart local v10    # "whereClause":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1418
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1420
    :cond_6
    const/16 v19, 0x0

    .line 1421
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_17
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_17 .. :try_end_17} :catch_0

    .line 1422
    :try_start_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1423
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v8, "provider"

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v7, v1

    move-object/from16 v9, p2

    move-object/from16 v11, p4

    move-object/from16 v14, p5

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1424
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_b
    move-exception v2

    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_b

    :try_start_19
    throw v2

    .line 1429
    .end local v10    # "whereClause":Ljava/lang/String;
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_c
    const/16 v19, 0x0

    .line 1430
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_19
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_19 .. :try_end_19} :catch_0

    .line 1431
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1432
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v12, "setting"

    const-string v16, "card_type_key"

    const/16 v17, 0x0

    move-object v11, v1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v18, p5

    invoke-virtual/range {v11 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1433
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_c
    move-exception v2

    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_c

    :try_start_1b
    throw v2

    .line 1438
    .end local v19    # "cursor":Landroid/database/Cursor;
    :sswitch_d
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1439
    .restart local v10    # "whereClause":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1440
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1442
    :cond_7
    const/16 v19, 0x0

    .line 1443
    .restart local v19    # "cursor":Landroid/database/Cursor;
    monitor-enter p0
    :try_end_1b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1b .. :try_end_1b} :catch_0

    .line 1444
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1445
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v8, "setting"

    const-string v12, "card_type_key"

    const/4 v13, 0x0

    move-object v7, v1

    move-object/from16 v9, p2

    move-object/from16 v11, p4

    move-object/from16 v14, p5

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1446
    monitor-exit p0

    goto/16 :goto_0

    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_d
    move-exception v2

    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_d

    :try_start_1d
    throw v2
    :try_end_1d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1d .. :try_end_1d} :catch_0

    .line 1289
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_3
        0x12c -> :sswitch_4
        0x12d -> :sswitch_5
        0x190 -> :sswitch_6
        0x191 -> :sswitch_7
        0x1f4 -> :sswitch_8
        0x1f5 -> :sswitch_9
        0x258 -> :sswitch_a
        0x259 -> :sswitch_b
        0x2bc -> :sswitch_c
        0x2bd -> :sswitch_d
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1475
    :try_start_0
    sget-object v4, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 1592
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unsupported URI: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1594
    :catch_0
    move-exception v1

    .line 1595
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    const-string v4, "MagazineService::AdministratorContentProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Database operation failed. : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1596
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 1597
    const/4 v2, 0x0

    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    return v2

    .line 1478
    :sswitch_0
    const/4 v2, 0x0

    .line 1479
    .local v2, "rowCount":I
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1480
    :try_start_2
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1481
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->updateCards(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1482
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v4

    .line 1487
    .end local v2    # "rowCount":I
    :sswitch_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1488
    .local v3, "whereClause":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1489
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1491
    :cond_0
    const/4 v2, 0x0

    .line 1492
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1493
    :try_start_4
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1494
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v0, p2, v3, p4}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->updateCards(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1495
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_1
    move-exception v4

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v4

    .line 1500
    .end local v2    # "rowCount":I
    .end local v3    # "whereClause":Ljava/lang/String;
    :sswitch_2
    const/4 v2, 0x0

    .line 1501
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1502
    :try_start_6
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1503
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->updateChannels(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1504
    monitor-exit p0

    goto :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_2
    move-exception v4

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v4

    .line 1509
    .end local v2    # "rowCount":I
    :sswitch_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1510
    .restart local v3    # "whereClause":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1511
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1513
    :cond_1
    const/4 v2, 0x0

    .line 1514
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0

    .line 1515
    :try_start_8
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1516
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v0, p2, v3, p4}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->updateChannels(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1517
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_3
    move-exception v4

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    throw v4

    .line 1522
    .end local v2    # "rowCount":I
    .end local v3    # "whereClause":Ljava/lang/String;
    :sswitch_4
    const/4 v2, 0x0

    .line 1523
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_0

    .line 1524
    :try_start_a
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1525
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->updateMultiLanguages(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1526
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_4
    move-exception v4

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :try_start_b
    throw v4

    .line 1531
    .end local v2    # "rowCount":I
    :sswitch_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1532
    .restart local v3    # "whereClause":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1533
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1535
    :cond_2
    const/4 v2, 0x0

    .line 1536
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_b} :catch_0

    .line 1537
    :try_start_c
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1538
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v0, p2, v3, p4}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->updateMultiLanguages(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1539
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_5
    move-exception v4

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    :try_start_d
    throw v4

    .line 1544
    .end local v2    # "rowCount":I
    .end local v3    # "whereClause":Ljava/lang/String;
    :sswitch_6
    const/4 v2, 0x0

    .line 1545
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_0

    .line 1546
    :try_start_e
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1547
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->updateProviders(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1548
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_6
    move-exception v4

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    :try_start_f
    throw v4

    .line 1553
    .end local v2    # "rowCount":I
    :sswitch_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1554
    .restart local v3    # "whereClause":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1555
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1557
    :cond_3
    const/4 v2, 0x0

    .line 1558
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_0

    .line 1559
    :try_start_10
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1560
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v0, p2, v3, p4}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->updateProviders(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1561
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_7
    move-exception v4

    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_7

    :try_start_11
    throw v4

    .line 1566
    .end local v2    # "rowCount":I
    .end local v3    # "whereClause":Ljava/lang/String;
    :sswitch_8
    const/4 v2, 0x0

    .line 1567
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_0

    .line 1568
    :try_start_12
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1569
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->updateSettings(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1570
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_8
    move-exception v4

    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_8

    :try_start_13
    throw v4

    .line 1575
    .end local v2    # "rowCount":I
    :sswitch_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1576
    .restart local v3    # "whereClause":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1577
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1579
    :cond_4
    const/4 v2, 0x0

    .line 1580
    .restart local v2    # "rowCount":I
    monitor-enter p0
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_13 .. :try_end_13} :catch_0

    .line 1581
    :try_start_14
    iget-object v4, p0, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->mCardDbHelper:Lcom/samsung/android/magazine/service/db/CardDbHelper;

    invoke-virtual {v4}, Lcom/samsung/android/magazine/service/db/CardDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1582
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, v0, p2, v3, p4}, Lcom/samsung/android/magazine/service/provider/AdministratorContentProvider;->updateSettings(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1583
    monitor-exit p0

    goto/16 :goto_0

    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_9
    move-exception v4

    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_9

    :try_start_15
    throw v4

    .line 1590
    .end local v2    # "rowCount":I
    .end local v3    # "whereClause":Ljava/lang/String;
    :sswitch_a
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unsupported opeation: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_15
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_15 .. :try_end_15} :catch_0

    .line 1475
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0xc8 -> :sswitch_a
        0xc9 -> :sswitch_a
        0x12c -> :sswitch_2
        0x12d -> :sswitch_3
        0x190 -> :sswitch_4
        0x191 -> :sswitch_5
        0x1f4 -> :sswitch_a
        0x1f5 -> :sswitch_a
        0x258 -> :sswitch_6
        0x259 -> :sswitch_7
        0x2bc -> :sswitch_8
        0x2bd -> :sswitch_9
    .end sparse-switch
.end method
