.class public Lcom/samsung/android/magazine/service/db/CardTable;
.super Ljava/lang/Object;
.source "CardTable.java"


# static fields
.field public static final COLUMN_ATTRIBUTES:Ljava/lang/String; = "attributes"

.field public static final COLUMN_CARD_CHANNEL:Ljava/lang/String; = "card_channel"

.field public static final COLUMN_CARD_KEY:Ljava/lang/String; = "card_key"

.field public static final COLUMN_CARD_PROVIDER:Ljava/lang/String; = "card_provider"

.field public static final COLUMN_CARD_TYPE:Ljava/lang/String; = "card_type"

.field public static final COLUMN_EXPIRATION_TIME:Ljava/lang/String; = "expiration_time"

.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final COLUMN_TEMPLATE_TYPE:Ljava/lang/String; = "template_type"

.field public static final COLUMN_TIME_STAMP:Ljava/lang/String; = "time_stamp"

.field public static final SQL_CREATE:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS card (_id INTEGER PRIMARY KEY AUTOINCREMENT, card_key TEXT NOT NULL, card_provider TEXT NOT NULL, card_type TEXT NOT NULL, template_type TEXT NOT NULL, card_channel TEXT, attributes TEXT, expiration_time INTEGER, time_stamp INTEGER );"

.field public static final TABLE_NAME:Ljava/lang/String; = "card"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addTableName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "column"    # Ljava/lang/String;

    .prologue
    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "card."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
