.class public final Lcom/samsung/android/magazine/service/contract/CardChannelContract$RegisteredSection;
.super Ljava/lang/Object;
.source "CardChannelContract.java"

# interfaces
.implements Lcom/samsung/android/magazine/service/contract/CardChannelContract$QuerySettingColumns;
.implements Lcom/samsung/android/magazine/service/contract/CardChannelContract$SectionColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/service/contract/CardChannelContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RegisteredSection"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 923
    sget-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "registered_section"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$RegisteredSection;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
