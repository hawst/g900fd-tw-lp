.class public Lcom/samsung/android/magazine/cardchannel/CardAction;
.super Ljava/lang/Object;
.source "CardAction.java"


# static fields
.field public static final ACTION_ACTIVITY:Ljava/lang/String; = "activity"

.field public static final ACTION_BROADCAST:Ljava/lang/String; = "broadcast"

.field public static final ACTION_SERVICE:Ljava/lang/String; = "service"


# instance fields
.field private mActionType:Ljava/lang/String;

.field private mAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mData:Landroid/content/Intent;

.field private mLabel:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "actionType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardAction;->mActionType:Ljava/lang/String;

    .line 48
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardAction;->mLabel:Ljava/lang/String;

    .line 49
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardAction;->mData:Landroid/content/Intent;

    .line 50
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardAction;->mAttributes:Ljava/util/Map;

    .line 61
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardAction;->mActionType:Ljava/lang/String;

    .line 62
    return-void
.end method


# virtual methods
.method public getActionType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardAction;->mActionType:Ljava/lang/String;

    return-object v0
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardAction;->mAttributes:Ljava/util/Map;

    return-object v0
.end method

.method public getData()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardAction;->mData:Landroid/content/Intent;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardAction;->mLabel:Ljava/lang/String;

    return-object v0
.end method

.method setAttributes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 125
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardAction;->mAttributes:Ljava/util/Map;

    .line 126
    return-void
.end method

.method setData(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardAction;->mData:Landroid/content/Intent;

    .line 116
    return-void
.end method

.method setLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardAction;->mLabel:Ljava/lang/String;

    .line 105
    return-void
.end method
