.class public final Lcom/samsung/android/magazine/service/contract/AdministratorContract$Provider;
.super Ljava/lang/Object;
.source "AdministratorContract.java"

# interfaces
.implements Lcom/samsung/android/magazine/service/contract/AdministratorContract$ProviderColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/service/contract/AdministratorContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Provider"
.end annotation


# static fields
.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.administrator.provider"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.administrator.provider"

.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 741
    sget-object v0, Lcom/samsung/android/magazine/service/contract/AdministratorContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "provider"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/AdministratorContract$Provider;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 733
    return-void
.end method
