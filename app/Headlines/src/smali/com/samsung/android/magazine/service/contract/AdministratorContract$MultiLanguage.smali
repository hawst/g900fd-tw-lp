.class public final Lcom/samsung/android/magazine/service/contract/AdministratorContract$MultiLanguage;
.super Ljava/lang/Object;
.source "AdministratorContract.java"

# interfaces
.implements Lcom/samsung/android/magazine/service/contract/AdministratorContract$MultiLanguageColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/service/contract/AdministratorContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MultiLanguage"
.end annotation


# static fields
.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.administrator.multi_language"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.administrator.multi_language"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final TYPE_CARD_TYPE:Ljava/lang/String; = "card_type"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 656
    sget-object v0, Lcom/samsung/android/magazine/service/contract/AdministratorContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "multi_language"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/AdministratorContract$MultiLanguage;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 648
    return-void
.end method
