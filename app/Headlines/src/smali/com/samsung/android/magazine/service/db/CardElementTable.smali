.class public Lcom/samsung/android/magazine/service/db/CardElementTable;
.super Ljava/lang/Object;
.source "CardElementTable.java"


# static fields
.field public static final COLUMN_ACTION_ATTRIBUTES:Ljava/lang/String; = "action_attributes"

.field public static final COLUMN_ACTION_DATA:Ljava/lang/String; = "action_data"

.field public static final COLUMN_ACTION_LABEL:Ljava/lang/String; = "action_label"

.field public static final COLUMN_ACTION_TYPE:Ljava/lang/String; = "action_type"

.field public static final COLUMN_ATTRIBUTES:Ljava/lang/String; = "attributes"

.field public static final COLUMN_CARD_ID:Ljava/lang/String; = "card_id"

.field public static final COLUMN_DATA1:Ljava/lang/String; = "data1"

.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final COLUMN_KEY:Ljava/lang/String; = "key"

.field public static final COLUMN_TYPE:Ljava/lang/String; = "type"

.field public static final IMAGE_FILE_DIRECTORY_NAME:Ljava/lang/String; = "image"

.field public static final SQL_CREATE:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS card_element (_id INTEGER PRIMARY KEY AUTOINCREMENT, card_id INTEGAER NOT NULL, key TEXT NOT NULL, type TEXT NOT NULL, data1 TEXT, attributes TEXT, action_type TEXT, action_label TEXT, action_data TEXT, action_attributes TEXT );"

.field public static final TABLE_NAME:Ljava/lang/String; = "card_element"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addTableName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "column"    # Ljava/lang/String;

    .prologue
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "card_element."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
