.class public Lcom/samsung/android/magazine/cardchannel/CardProvider;
.super Ljava/lang/Object;
.source "CardProvider.java"


# instance fields
.field private mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

.field private mConfigurationIntent:Landroid/content/Intent;

.field private mName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/magazine/cardchannel/CardChannel;Ljava/lang/String;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/android/magazine/cardchannel/CardChannel;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardProvider;->mName:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardProvider;->mConfigurationIntent:Landroid/content/Intent;

    .line 28
    iput-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardProvider;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    .line 39
    iput-object p2, p0, Lcom/samsung/android/magazine/cardchannel/CardProvider;->mName:Ljava/lang/String;

    .line 40
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardProvider;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    .line 41
    return-void
.end method


# virtual methods
.method public getCardNames()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardProvider;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v0}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/magazine/cardchannel/CardProvider;->mCardChannel:Lcom/samsung/android/magazine/cardchannel/CardChannel;

    invoke-virtual {v1}, Lcom/samsung/android/magazine/cardchannel/CardChannel;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/magazine/cardchannel/CardProvider;->mName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/magazine/cardchannel/ConfigurationManager;->getProvidedCardNames(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getConfigurationIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardProvider;->mConfigurationIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/magazine/cardchannel/CardProvider;->mName:Ljava/lang/String;

    return-object v0
.end method

.method setConfiguratonIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "configurationIntent"    # Landroid/content/Intent;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardProvider;->mConfigurationIntent:Landroid/content/Intent;

    .line 92
    return-void
.end method

.method setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/samsung/android/magazine/cardchannel/CardProvider;->mName:Ljava/lang/String;

    .line 82
    return-void
.end method
