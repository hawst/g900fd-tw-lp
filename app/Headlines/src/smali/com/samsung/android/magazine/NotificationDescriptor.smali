.class public interface abstract Lcom/samsung/android/magazine/NotificationDescriptor;
.super Ljava/lang/Object;
.source "NotificationDescriptor.java"


# static fields
.field public static final ACTION_CARD_DISMISSED:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CARD_DISMISSED"

.field public static final ACTION_CARD_NAME_SUBSCRIPTION_STATE_CHANGED:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CARD_NAME_SUBSCRIPTION_STATE_CHANGED"

.field public static final ACTION_CARD_REFRESH:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CARD_REFRESH"

.field public static final ACTION_CHANNEL_RUNNING_STATE_CHANGED:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.CHANNEL_RUNNING_STATE_CHANGED"

.field public static final ACTION_INITIALIZE_CARD_PROVIDER:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.INITIALIZE_CARD_PROVIDER"

.field public static final ACTION_MAGAZINE_SERVICE_READY:Ljava/lang/String; = "com.samsung.android.magazine.intent.action.MAGAZINE_SERVICE_READY"

.field public static final EXTRA_CARD_ID:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.CARD_ID"

.field public static final EXTRA_CARD_NAME:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.CARD_NAME"

.field public static final EXTRA_CARD_NAME_SUBSCRIBED:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.CARD_NAME_SUBSCRIBED"

.field public static final EXTRA_CHANNEL_ACTIVE:Ljava/lang/String; = "com.samsung.android.magazine.intent.extra.CHANNEL_ACTIVE"
