.class public final Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;
.super Ljava/lang/Object;
.source "CardChannelContract.java"

# interfaces
.implements Lcom/samsung/android/magazine/service/contract/CardChannelContract$CardTypeMultiLanguageColumns;
.implements Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardTypeColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/service/contract/CardChannelContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvidedCardType"
.end annotation


# static fields
.field public static final CHANNEL_NOT_DEFINED:Ljava/lang/String; = "channel_not_defined"

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.com.samsung.android.magazine.provider.cardchannel.provided_card_type"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.samsung.android.magazine.provider.cardchannel.provided_card_type"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CONTENT_URI_CHANGE_NOTIFICATION:Landroid/net/Uri;

.field public static final CONTENT_URI_IMAGE:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 849
    sget-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "provided_card_type"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    .line 856
    sget-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "image"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI_IMAGE:Landroid/net/Uri;

    .line 863
    sget-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "change"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/magazine/service/contract/CardChannelContract$ProvidedCardType;->CONTENT_URI_CHANGE_NOTIFICATION:Landroid/net/Uri;

    .line 881
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
