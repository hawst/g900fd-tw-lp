.class public interface abstract Lcom/samsung/android/magazine/service/contract/AdministratorContract$MultiLanguageColumns;
.super Ljava/lang/Object;
.source "AdministratorContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/magazine/service/contract/AdministratorContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60c
    name = "MultiLanguageColumns"
.end annotation


# static fields
.field public static final KEY1:Ljava/lang/String; = "key1"

.field public static final KEY2:Ljava/lang/String; = "key2"

.field public static final LANGUAGE_CODE:Ljava/lang/String; = "language_code"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final VALUE1:Ljava/lang/String; = "value1"

.field public static final VALUE2:Ljava/lang/String; = "value2"

.field public static final VALUE3:Ljava/lang/String; = "value3"

.field public static final _ID:Ljava/lang/String; = "_id"
