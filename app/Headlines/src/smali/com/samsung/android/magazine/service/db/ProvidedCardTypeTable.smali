.class public Lcom/samsung/android/magazine/service/db/ProvidedCardTypeTable;
.super Ljava/lang/Object;
.source "ProvidedCardTypeTable.java"


# static fields
.field public static final COLUMN_CARD_TYPE_KEY:Ljava/lang/String; = "card_type_key"

.field public static final COLUMN_CHANNEL_KEY:Ljava/lang/String; = "channel_key"

.field public static final COLUMN_DEFAULT_SUBSCRIPTION:Ljava/lang/String; = "default_subscription"

.field public static final COLUMN_ICON_IMAGE:Ljava/lang/String; = "icon_image"

.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final COLUMN_LAST_MODIFIED_TIME:Ljava/lang/String; = "last_modified_time"

.field public static final COLUMN_PROVIDER:Ljava/lang/String; = "provider"

.field public static final COLUMN_SECTION_KEY:Ljava/lang/String; = "section_key"

.field public static final SQL_CREATE:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS provided_card_type (_id INTEGER PRIMARY KEY AUTOINCREMENT, provider TEXT NOT NULL, card_type_key TEXT NOT NULL, channel_key TEXT, section_key TEXT, icon_image TEXT, default_subscription INTEGER DEFAULT 0, last_modified_time INTEGER );"

.field public static final TABLE_NAME:Ljava/lang/String; = "provided_card_type"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addTableName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "column"    # Ljava/lang/String;

    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "provided_card_type."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
