.class Lcom/sec/android/app/parser/CExpression;
.super Ljava/lang/Object;
.source "Logic.java"


# static fields
.field static final FACT:[D

.field static final GAMMA:[D


# instance fields
.field Cursor:I

.field private exception:Lcom/sec/android/app/parser/SyntaxException;

.field private infixStringExp:Ljava/lang/StringBuilder;

.field private infixTokenExp:[Lcom/sec/android/app/parser/CToken;

.field private postfixTokenExp:[Lcom/sec/android/app/parser/CToken;

.field private value:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 240
    const/16 v0, 0xe

    new-array v0, v0, [D

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/parser/CExpression;->GAMMA:[D

    .line 262
    const/16 v0, 0x16

    new-array v0, v0, [D

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/parser/CExpression;->FACT:[D

    return-void

    .line 240
    nop

    :array_0
    .array-data 8
        0x404c93ff87c1acceL    # 57.15623566586292
        -0x3fb2337608fa76d0L    # -59.59796035547549
        0x402c45aea23d22a1L    # 14.136097974741746
        -0x4020847be9da401cL    # -0.4919138160976202
        0x3f01d2af4786183aL    # 3.399464998481189E-5
        0x3f08644bb7c5e3bdL    # 4.652362892704858E-5
        -0x40e63633621a8b49L    # -9.837447530487956E-5
        0x3f24b8939ed4e66dL    # 1.580887032249125E-4
        -0x40d470b232d541caL    # -2.1026444172410488E-4
        0x3f2c801018e9e826L    # 2.1743961811521265E-4
        -0x40da7666366ad9c0L    # -1.643181065367639E-4
        0x3f1621360b773d55L    # 8.441822398385275E-5
        -0x410489734a2e1dfaL    # -2.6190838401581408E-5
        0x3ecef40a04fc9810L    # 3.6899182659531625E-6
    .end array-data

    .line 262
    :array_1
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x40e3b00000000000L    # 40320.0
        0x42b3077775800000L    # 2.0922789888E13
        0x44e06c52687a7b9aL    # 6.204484017332394E23
        0x474956ad0aae33a4L    # 2.631308369336935E35
        0x49e1dd5d037098feL    # 8.159152832478977E47
        0x4c9ee69a78d72cb6L    # 1.2413915592536073E61
        0x4f792693359a4003L    # 7.109985878048635E74
        0x526fe478ee34844aL    # 1.2688693218588417E89
        0x557b5705796695b6L    # 6.1234458376886085E103
        0x589c619094edabffL    # 7.156945704626381E118
        0x5bd0550c4b30743eL    # 1.8548264225739844E134
        0x5f13638dd7bd6347L    # 9.916779348709496E149
        0x62665b0eb1760a70L    # 1.0299016745145628E166
        0x65c7cac197cfe503L    # 1.974506857221074E182
        0x69365f6380a9d916L    # 6.689502913449127E198
        0x6cb1e5dfc140e1e5L    # 3.856204823625804E215
        0x70379185413b0855L    # 3.659042881952549E232
        0x73c8ce85fadb707eL    # 5.5502938327393044E249
        0x776455903aefd5a3L    # 1.3113358856834524E267
        0x7b095d5f3d928edeL    # 4.7147236359920616E284
        0x7eb7932fa79d3a43L    # 2.5260757449731984E302
    .end array-data
.end method

.method constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x100

    .line 1215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/parser/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    .line 234
    new-array v1, v4, [Lcom/sec/android/app/parser/CToken;

    iput-object v1, p0, Lcom/sec/android/app/parser/CExpression;->infixTokenExp:[Lcom/sec/android/app/parser/CToken;

    .line 236
    new-array v1, v4, [Lcom/sec/android/app/parser/CToken;

    iput-object v1, p0, Lcom/sec/android/app/parser/CExpression;->postfixTokenExp:[Lcom/sec/android/app/parser/CToken;

    .line 238
    new-instance v1, Lcom/sec/android/app/parser/SyntaxException;

    invoke-direct {v1}, Lcom/sec/android/app/parser/SyntaxException;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/parser/CExpression;->exception:Lcom/sec/android/app/parser/SyntaxException;

    .line 248
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/parser/CExpression;->Cursor:I

    .line 1216
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/parser/CExpression;->value:D

    .line 1218
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 1219
    iget-object v1, p0, Lcom/sec/android/app/parser/CExpression;->infixTokenExp:[Lcom/sec/android/app/parser/CToken;

    new-instance v2, Lcom/sec/android/app/parser/CToken;

    invoke-direct {v2}, Lcom/sec/android/app/parser/CToken;-><init>()V

    aput-object v2, v1, v0

    .line 1220
    iget-object v1, p0, Lcom/sec/android/app/parser/CExpression;->postfixTokenExp:[Lcom/sec/android/app/parser/CToken;

    new-instance v2, Lcom/sec/android/app/parser/CToken;

    invoke-direct {v2}, Lcom/sec/android/app/parser/CToken;-><init>()V

    aput-object v2, v1, v0

    .line 1218
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1222
    :cond_0
    return-void
.end method
