.class Lcom/sec/android/app/parser/EventHandler$1;
.super Ljava/lang/Object;
.source "EventHandler.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/parser/EventHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/parser/EventHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/app/parser/EventHandler;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/app/parser/EventHandler$1;->this$0:Lcom/sec/android/app/parser/EventHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 129
    const-string v0, "FactoryModeEventHandler"

    const-string v1, "* Keystring parsing service * onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/parser/EventHandler$1;->this$0:Lcom/sec/android/app/parser/EventHandler;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/android/app/parser/EventHandler;->mMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/parser/EventHandler;->access$002(Lcom/sec/android/app/parser/EventHandler;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 131
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 133
    const-string v0, "FactoryModeEventHandler"

    const-string v1, "* Keystring parsing service * onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/parser/EventHandler$1;->this$0:Lcom/sec/android/app/parser/EventHandler;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/parser/EventHandler;->mMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/parser/EventHandler;->access$002(Lcom/sec/android/app/parser/EventHandler;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 135
    return-void
.end method
