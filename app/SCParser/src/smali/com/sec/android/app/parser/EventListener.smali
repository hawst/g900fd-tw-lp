.class Lcom/sec/android/app/parser/EventListener;
.super Ljava/lang/Object;
.source "EventListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field historyCount:[I

.field mCheck:Z

.field mDisplay:Landroid/widget/EditText;

.field mHandler:Lcom/sec/android/app/parser/EventHandler;

.field mdisCheck:Z

.field private sb:Ljava/lang/StringBuilder;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-boolean v0, p0, Lcom/sec/android/app/parser/EventListener;->mCheck:Z

    .line 33
    iput-boolean v0, p0, Lcom/sec/android/app/parser/EventListener;->mdisCheck:Z

    .line 35
    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/parser/EventListener;->historyCount:[I

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    return-void
.end method

.method private clearSB()V
    .locals 3

    .prologue
    .line 63
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 65
    .local v0, "len":I
    if-nez v0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 75
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    iget-object v2, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/parser/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/parser/EventHandler;->setFrontText(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 78
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 125
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/parser/Cmyfunc;->isOnlyDigit(C)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v1}, Lcom/sec/android/app/parser/EventHandler;->isOpenedSignParenthesis()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 128
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v1}, Lcom/sec/android/app/parser/EventHandler;->closeSignParenthesis()V

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    iget-object v2, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/parser/EventHandler;->insert(Ljava/lang/StringBuilder;)V

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/parser/EventHandler;->setSelecting(Z)V

    .line 135
    invoke-direct {p0}, Lcom/sec/android/app/parser/EventListener;->clearSB()V

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->mDisplay:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/parser/EventHandler;->getTextLength()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 137
    return-void

    .line 80
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v1}, Lcom/sec/android/app/parser/EventHandler;->onClear()V

    goto :goto_0

    .line 83
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v1}, Lcom/sec/android/app/parser/EventHandler;->onBackspace()V

    goto :goto_0

    .line 86
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 89
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 92
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 95
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 98
    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 101
    :pswitch_7
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 104
    :pswitch_8
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v2, "4"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 107
    :pswitch_9
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v2, "5"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 110
    :pswitch_a
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v2, "6"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 113
    :pswitch_b
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v2, "7"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 116
    :pswitch_c
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v2, "8"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 119
    :pswitch_d
    iget-object v1, p0, Lcom/sec/android/app/parser/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v2, "9"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 78
    :pswitch_data_0
    .packed-switch 0x7f060005
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 46
    iget-object v2, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/parser/EventHandler;->setSelecting(Z)V

    .line 47
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 49
    .local v0, "id":I
    const v2, 0x7f060006

    if-eq v0, v2, :cond_0

    .line 53
    :goto_0
    return v1

    .line 52
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    iget-object v2, v2, Lcom/sec/android/app/parser/EventHandler;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 53
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public removeHandlerEvent()V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    iget-object v0, v0, Lcom/sec/android/app/parser/EventHandler;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 58
    return-void
.end method

.method setHandler(Lcom/sec/android/app/parser/EventHandler;Landroid/widget/EditText;)V
    .locals 2
    .param p1, "handler"    # Lcom/sec/android/app/parser/EventHandler;
    .param p2, "display"    # Landroid/widget/EditText;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/parser/EventListener;->mHandler:Lcom/sec/android/app/parser/EventHandler;

    .line 40
    iput-object p2, p0, Lcom/sec/android/app/parser/EventListener;->mDisplay:Landroid/widget/EditText;

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/parser/EventListener;->mDisplay:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setRawInputType(I)V

    .line 42
    return-void
.end method
