.class public Lcom/sec/android/app/parser/KeystringCommon;
.super Ljava/lang/Object;
.source "KeystringCommon.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;
    }
.end annotation


# static fields
.field private static final isCanada:Z

.field private static final isI747M:Z

.field private static final isMarvell:Z

.field private static final isModelCRI:Z

.field private static final isModelOrange:Z

.field private static final isModelTeleO2:Z

.field private static final isModelTelstra:Z

.field private static final isModelVFG:Z

.field private static isOneChipSolution:Z

.field private static final isT999V:Z

.field private static final isTMO:Z

.field private static isWifiOnly:Z

.field private static mDeviceID:Ljava/lang/String;

.field private static final mHiddenMenuDisabledKeystringList:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

.field private static final mHiddenMenuDisabledKeystringListTFN:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

.field private static final mHiddenMenuEnabledKeystringList:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

.field private static final mHiddenMenuEnabledKeystringListTFN:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

.field private static final mHiddenMenuKeystringListACG:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

.field private static final mHiddenMenuKeystringListCRI:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

.field private static mKeystrBlockCheck:Lcom/sec/android/app/parser/KeystrBlockCheck;

.field private static mKeystringBlockFlag:Z

.field private static final mOneChipSolution:[Ljava/lang/String;

.field private static final mProductShip:Ljava/lang/String;

.field private static final mSalesCode:Ljava/lang/String;


# instance fields
.field private device:Ljava/lang/String;

.field private isACG:Z

.field private isHiddnemenu:Z

.field final isModelINDIA:Z

.field final isModelSING:Z

.field private mContext:Landroid/content/Context;

.field private mCountryCode:Ljava/lang/String;

.field private final mKeystringList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTFNReceiver:Landroid/content/BroadcastReceiver;

.field private mVZWReceiver:Landroid/content/BroadcastReceiver;

.field private model:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private vzwFamily:Z


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    const/16 v7, 0xca

    .line 30
    sput-object v8, Lcom/sec/android/app/parser/KeystringCommon;->mDeviceID:Ljava/lang/String;

    .line 31
    sput-boolean v2, Lcom/sec/android/app/parser/KeystringCommon;->isWifiOnly:Z

    .line 32
    const-string v0, "ro.csc.sales_code"

    const-string v3, "NONE"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    .line 38
    sget-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v3, "TMB"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->isTMO:Z

    .line 39
    const-string v0, "RCS/RWC/XAC/FMC/CAN/BMC/BWA/MTS/VMC/FNC/CHR/TLS/CLN/KDO/SOL/PCM/PMB/VTR/GLW/MTA"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->isCanada:Z

    .line 41
    const-string v0, "RWC/TLS/BMC/BWA"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    sput-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->isI747M:Z

    .line 42
    const-string v0, "VTR/MCT/GLW"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    sput-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->isT999V:Z

    .line 43
    const-string v0, "EVR/ORA/TMU/TMP/FTM/IDE/AMN/ORO/ORS/ORG/OPT/MST"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    sput-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->isModelOrange:Z

    .line 46
    const-string v0, "CYV/OMN/TCL/VD2/VDI/VOD"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    sput-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->isModelVFG:Z

    .line 47
    const-string v0, "TEL"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    sput-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->isModelTelstra:Z

    .line 48
    const-string v0, "mrvl"

    const-string v3, "ro.board.platform"

    const-string v4, "Unknown"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->isMarvell:Z

    .line 49
    sget-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v3, "CRI"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->isModelCRI:Z

    .line 50
    const-string v0, "O2U/O2I/O2C/VIA/VIT/VID/XEC"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    sput-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->isModelTeleO2:Z

    .line 60
    sput-boolean v2, Lcom/sec/android/app/parser/KeystringCommon;->isOneChipSolution:Z

    .line 61
    const-string v0, "ro.product_ship"

    const-string v3, "FALSE"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mProductShip:Ljava/lang/String;

    .line 63
    invoke-static {}, Lcom/sec/android/app/parser/KeystringCommon;->isKeystringBlockFlagSet()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringBlockFlag:Z

    .line 263
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "MSM8960"

    aput-object v3, v0, v2

    const-string v3, "MSM8260A"

    aput-object v3, v0, v1

    const-string v3, "MSM8930"

    aput-object v3, v0, v9

    sput-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mOneChipSolution:[Ljava/lang/String;

    .line 1472
    const/16 v0, 0x24

    new-array v0, v0, [Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "#7465625*638*#"

    const-string v5, "7465625*638*"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v0, v2

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "#7465625*77*#"

    const-string v5, "7465625*77*"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v0, v1

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "##433346#"

    const-string v5, "IOTHIDDENMENU"

    invoke-direct {v3, v4, v5, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v0, v9

    const/4 v3, 0x3

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#9999#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/4 v3, 0x4

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#8888#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/4 v3, 0x5

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#7284#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/4 v3, 0x6

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "**7838"

    const-string v6, "STEALTHMODE"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/4 v3, 0x7

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "**367738"

    const-string v6, "DNSSET"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x8

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "##3282#"

    const-string v6, "DATA"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x9

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#9090#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0xa

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#232337#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0xb

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#1111#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0xc

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#2663#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0xd

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#2664#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0xe

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "##786#"

    const-string v6, "RTN"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0xf

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#27663368378#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x10

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "47*68#13580"

    const-string v6, "TESTMODE"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x11

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#9900#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x12

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#7412365#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x13

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#0*#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x14

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#0228#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x15

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#0283#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x16

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#06#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x17

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#1234#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x18

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#0011#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x19

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#00112#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1a

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#12580*369#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1b

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#22558463#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1c

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#7353#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1d

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#66336#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1e

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "**6828378"

    const-string v6, "OTATEST"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1f

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "##7764726"

    const-string v6, "PROGRAM"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x20

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#0808#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x21

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#83786633"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x22

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#467#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x23

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#99732#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    sput-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuEnabledKeystringList:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    .line 1537
    const/16 v0, 0x24

    new-array v0, v0, [Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "#7465625*638*#"

    const-string v5, "7465625*638*"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v0, v2

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "#7465625*77*#"

    const-string v5, "7465625*77*"

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v0, v1

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "##433346#"

    const-string v5, "IOTHIDDENMENU"

    invoke-direct {v3, v4, v5, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v0, v9

    const/4 v3, 0x3

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#9999#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/4 v3, 0x4

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#8888#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/4 v3, 0x5

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#7284#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/4 v3, 0x6

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "**7838"

    const-string v6, "STEALTHMODE"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/4 v3, 0x7

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "**367738"

    const-string v6, "DNSSET"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x8

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "##3282#"

    const-string v6, "DATA"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x9

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#9090#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0xa

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#232337#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0xb

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#1111#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0xc

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#2663#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0xd

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#2664#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0xe

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "##786#"

    const-string v6, "RTN"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0xf

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#27663368378#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x10

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "47*68#13580"

    const-string v6, "TESTMODE"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x11

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#9900#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x12

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#7412365#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x13

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#0*#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x14

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#0228#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x15

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#0283#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x16

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#06#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x17

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#1234#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x18

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#0011#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x19

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#00112#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1a

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#12580*369#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1b

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#22558463#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1c

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#7353#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1d

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#66336#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1e

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "**6828378"

    const-string v6, "OTATEST"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x1f

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "##7764726"

    const-string v6, "PROGRAM"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x20

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#0808#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x21

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#83786633"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x22

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#467#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/16 v3, 0x23

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "*#99732#"

    invoke-direct {v4, v5, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    sput-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuDisabledKeystringList:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    .line 1603
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "*#232337#"

    invoke-direct {v3, v4, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v0, v2

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "*#1111#"

    invoke-direct {v3, v4, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v0, v1

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "*#2663#"

    invoke-direct {v3, v4, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v0, v9

    sput-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuEnabledKeystringListTFN:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    .line 1627
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "*#232337#"

    invoke-direct {v3, v4, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v0, v2

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "*#1111#"

    invoke-direct {v3, v4, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v0, v1

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "*#2663#"

    invoke-direct {v3, v4, v8, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v0, v9

    sput-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuDisabledKeystringListTFN:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    .line 1653
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "##8378#"

    const-string v5, "TEST"

    invoke-direct {v3, v4, v5, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v0, v2

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "##4357*"

    const-string v5, "FIELDTESTMODE"

    invoke-direct {v3, v4, v5, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v0, v1

    new-instance v3, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v4, "##73738#"

    const-string v5, "RESET"

    invoke-direct {v3, v4, v5, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v0, v9

    const/4 v3, 0x3

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "##7764*"

    const-string v6, "PROG"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/4 v3, 0x4

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "##626*"

    const-string v6, "NAMBASIC"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    const/4 v3, 0x5

    new-instance v4, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v5, "##33284#"

    const-string v6, "DEBUG"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v4, v0, v3

    sput-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListCRI:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    .line 1661
    new-array v0, v1, [Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    new-instance v1, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v3, "##626#"

    const-string v4, "NAMBASIC"

    invoke-direct {v1, v3, v4, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListACG:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    return-void

    :cond_0
    move v0, v2

    .line 38
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 41
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 42
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 43
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 46
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 47
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 50
    goto/16 :goto_6
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/16 v5, 0xca

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "ro.product.model"

    const-string v3, "Unknown"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    .line 35
    const-string v0, "ro.product.device"

    const-string v3, "Unknown"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->device:Ljava/lang/String;

    .line 37
    const-string v0, "ro.product.name"

    const-string v3, "Unknown"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->name:Ljava/lang/String;

    .line 52
    const-string v0, "INS/INU/NPL/ETR/TML/SLK"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->isModelINDIA:Z

    .line 54
    const-string v0, "SIN/MM1/STH/XSP"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_1
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->isModelSING:Z

    .line 64
    const-string v0, "ro.csc.country_code"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mCountryCode:Ljava/lang/String;

    .line 73
    iput-boolean v2, p0, Lcom/sec/android/app/parser/KeystringCommon;->isHiddnemenu:Z

    .line 74
    iput-boolean v2, p0, Lcom/sec/android/app/parser/KeystringCommon;->vzwFamily:Z

    .line 75
    iput-boolean v2, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    .line 1168
    iput-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mVZWReceiver:Landroid/content/BroadcastReceiver;

    .line 1271
    iput-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mTFNReceiver:Landroid/content/BroadcastReceiver;

    .line 78
    iput-object p1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mContext:Landroid/content/Context;

    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->getDeviceID()V

    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->getWifiOnlyFlag()V

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->isOneChipSolution()V

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->setVzwFamily()V

    .line 84
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    .line 85
    new-instance v0, Lcom/sec/android/app/parser/KeystrBlockCheck;

    invoke-direct {v0, p1}, Lcom/sec/android/app/parser/KeystrBlockCheck;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystrBlockCheck:Lcom/sec/android/app/parser/KeystrBlockCheck;

    .line 87
    const-string v0, "SCH-I545L"

    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SCH-I545L"

    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v1, "*#22745927"

    new-instance v2, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v3, "*#22745927"

    const-string v4, "HIDDENMENUENABLE"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registReceiverForSpecialKeystring_VZW()V

    .line 90
    sget-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringBlockFlag:Z

    if-nez v0, :cond_1

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringVZW()V

    .line 93
    const-string v0, "CRI"

    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringCRI()V

    .line 129
    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerENGModeKeystrings()V

    .line 130
    return-void

    :cond_2
    move v0, v2

    .line 52
    goto/16 :goto_0

    :cond_3
    move v1, v2

    .line 54
    goto :goto_1

    .line 97
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->vzwFamily:Z

    if-eqz v0, :cond_6

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registReceiverForSpecialKeystring_VZW()V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v1, "##366633#"

    new-instance v2, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v3, "##366633#"

    const-string v4, "DMMODE"

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringVZW()V

    .line 105
    iget-boolean v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    if-eqz v0, :cond_5

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringACG()V

    .line 108
    :cond_5
    const-string v0, "CRI"

    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringCRI()V

    goto :goto_2

    .line 111
    :cond_6
    const-string v0, "TFN"

    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registReceiverForSpecialKeystring_TFN()V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v1, "*#22745927"

    new-instance v2, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v3, "*#22745927"

    const-string v4, "HIDDENMENUENABLE"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registkeystrings()V

    .line 116
    sget-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringBlockFlag:Z

    if-eqz v0, :cond_1

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/parser/KeystringCommon;->removeBlockedKeystrings()V

    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerFactoryModeKeystrings()V

    goto :goto_2

    .line 121
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registkeystrings()V

    .line 123
    sget-boolean v0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringBlockFlag:Z

    if-eqz v0, :cond_1

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/parser/KeystringCommon;->removeBlockedKeystrings()V

    .line 125
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerFactoryModeKeystrings()V

    goto :goto_2
.end method

.method static synthetic access$1000(Lcom/sec/android/app/parser/KeystringCommon;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringCRI()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/parser/KeystringCommon;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->unRegisterSpecialKeystringVZW()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/parser/KeystringCommon;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->unRegisterSpecialKeystringACG()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/parser/KeystringCommon;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->unRegisterSpecialKeystringCRI()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/parser/KeystringCommon;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringTFN()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/parser/KeystringCommon;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->unRegisterSpecialKeystringTFN()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/parser/KeystringCommon;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringVZW()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/parser/KeystringCommon;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerFactoryModeKeystrings()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/parser/KeystringCommon;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/parser/KeystringCommon;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/parser/KeystringCommon;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/parser/KeystringCommon;->registerSpecialKeystringACG()V

    return-void
.end method

.method static synthetic access$900()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    return-object v0
.end method

.method public static checkHiddnemenu()Z
    .locals 7

    .prologue
    .line 1303
    const/4 v3, 0x0

    .line 1304
    .local v3, "result":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1307
    .local v1, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    const-string v6, "/efs/carrier/HiddenMenu"

    invoke-direct {v5, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1308
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .local v2, "reader":Ljava/io/BufferedReader;
    if-eqz v2, :cond_0

    .line 1309
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 1310
    .local v4, "tmp":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 1311
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 1318
    .end local v4    # "tmp":Ljava/lang/String;
    :cond_0
    if-eqz v2, :cond_3

    .line 1320
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 1328
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    const-string v5, "ON"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    return v5

    .line 1322
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 1323
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    .line 1324
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 1314
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1315
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1318
    if-eqz v1, :cond_1

    .line 1320
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1322
    :catch_2
    move-exception v0

    .line 1323
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1318
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v1, :cond_2

    .line 1320
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1324
    :cond_2
    :goto_3
    throw v5

    .line 1322
    :catch_3
    move-exception v0

    .line 1323
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1318
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 1314
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method public static final checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "isACG"    # Z
    .param p1, "mSalesCode"    # Ljava/lang/String;
    .param p2, "condition"    # Ljava/lang/String;

    .prologue
    .line 155
    const-string v0, "XAR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p0, :cond_1

    .line 156
    :cond_0
    invoke-virtual {p2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    const/4 v0, 0x1

    .line 160
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getDeviceID()V
    .locals 3

    .prologue
    .line 243
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType()I

    move-result v0

    .line 245
    .local v0, "phoneType":I
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mDeviceID:Ljava/lang/String;

    .line 248
    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mDeviceID:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 249
    const-string v1, "719434266344"

    sput-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mDeviceID:Ljava/lang/String;

    .line 252
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 253
    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mDeviceID:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/parser/KeystringCommon;->replaceMeidToImei(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mDeviceID:Ljava/lang/String;

    .line 255
    :cond_1
    return-void
.end method

.method private getWifiOnlyFlag()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 258
    iget-object v2, p0, Lcom/sec/android/app/parser/KeystringCommon;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 260
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    sput-boolean v1, Lcom/sec/android/app/parser/KeystringCommon;->isWifiOnly:Z

    .line 261
    return-void
.end method

.method private static isKeystringBlockFlagSet()Z
    .locals 3

    .prologue
    .line 218
    const/4 v0, 0x0

    .line 220
    .local v0, "keystringblockflag":Z
    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mProductShip:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 221
    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mProductShip:Ljava/lang/String;

    const-string v2, "TRUE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/parser/ParseService;->isKeyStringBlocked()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 225
    :cond_0
    :goto_0
    return v0

    .line 221
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isOneChipSolution()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 268
    const-string v2, "ro.chipname"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 270
    .local v1, "solutionName":Ljava/lang/String;
    const-string v2, "NONE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 271
    const-string v2, "ro.product.board"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 274
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mOneChipSolution:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 275
    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mOneChipSolution:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v4, :cond_2

    .line 276
    sput-boolean v4, Lcom/sec/android/app/parser/KeystringCommon;->isOneChipSolution:Z

    .line 280
    :cond_1
    return-void

    .line 274
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private registerENGModeKeystrings()V
    .locals 2

    .prologue
    .line 178
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    :cond_0
    return-void
.end method

.method private registerFactoryModeKeystrings()V
    .locals 0

    .prologue
    .line 175
    return-void
.end method

.method private registerSpecialKeystringACG()V
    .locals 4

    .prologue
    .line 1259
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListACG:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1260
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListACG:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v2, v2, v0

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1261
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListACG:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v2, v2, v0

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListACG:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v3, v3, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1259
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1265
    :cond_1
    return-void
.end method

.method private registerSpecialKeystringCRI()V
    .locals 4

    .prologue
    .line 1244
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListCRI:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1245
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListCRI:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v2, v2, v0

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1246
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListCRI:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v2, v2, v0

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListCRI:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v3, v3, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1244
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1250
    :cond_1
    return-void
.end method

.method private registerSpecialKeystringTFN()V
    .locals 4

    .prologue
    .line 1229
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuEnabledKeystringListTFN:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1230
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuEnabledKeystringListTFN:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v2, v2, v0

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1231
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuEnabledKeystringListTFN:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v2, v2, v0

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuEnabledKeystringListTFN:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v3, v3, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1229
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1235
    :cond_1
    return-void
.end method

.method private registerSpecialKeystringVZW()V
    .locals 4

    .prologue
    .line 1154
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuEnabledKeystringList:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1155
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuEnabledKeystringList:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v2, v2, v0

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1156
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuEnabledKeystringList:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v2, v2, v0

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuEnabledKeystringList:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v3, v3, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1154
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1160
    :cond_1
    return-void
.end method

.method private replaceMeidToImei(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "meid"    # Ljava/lang/String;

    .prologue
    .line 233
    move-object v0, p1

    .line 235
    .local v0, "imei":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 236
    const-string v1, "[a-fA-F]"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 239
    :cond_0
    return-object v0
.end method

.method private setVzwFamily()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 137
    const-string v2, "ACG"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 138
    iput-boolean v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    .line 144
    :goto_0
    const-string v2, "VZW"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "USC"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "CRI"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "CSP"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "LRA"

    sget-object v3, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    if-eqz v2, :cond_3

    :cond_0
    const-string v2, "GT-P5210"

    iget-object v3, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->vzwFamily:Z

    .line 145
    return-void

    .line 139
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/parser/KeystringCommon;->device:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_2

    .line 140
    const-string v2, "WW"

    iget-object v3, p0, Lcom/sec/android/app/parser/KeystringCommon;->device:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->device:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->device:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    goto :goto_0

    .line 142
    :cond_2
    iput-boolean v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 144
    goto :goto_1
.end method

.method private unRegisterSpecialKeystringACG()V
    .locals 3

    .prologue
    .line 1267
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListACG:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1268
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListACG:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v2, v2, v0

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1267
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1270
    :cond_0
    return-void
.end method

.method private unRegisterSpecialKeystringCRI()V
    .locals 3

    .prologue
    .line 1253
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListCRI:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1254
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuKeystringListCRI:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v2, v2, v0

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1253
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1256
    :cond_0
    return-void
.end method

.method private unRegisterSpecialKeystringTFN()V
    .locals 3

    .prologue
    .line 1238
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuDisabledKeystringListTFN:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1239
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuDisabledKeystringListTFN:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v2, v2, v0

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1238
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1241
    :cond_0
    return-void
.end method

.method private unRegisterSpecialKeystringVZW()V
    .locals 3

    .prologue
    .line 1163
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuDisabledKeystringList:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1164
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/parser/KeystringCommon;->mHiddenMenuDisabledKeystringList:[Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    aget-object v2, v2, v0

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mInput:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$400(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1163
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1166
    :cond_0
    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 229
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 230
    return-void
.end method

.method public getFactorymodeFlag(Ljava/lang/String;)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 330
    sget-boolean v0, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v0, :cond_0

    .line 331
    const-string v0, "KeystringCommon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFactorymodeFlag key : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsFactorymode:Z
    invoke-static {v0}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$300(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Z

    move-result v0

    .line 338
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getKeystringBlockFlag(Ljava/lang/String;)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 318
    sget-boolean v0, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v0, :cond_0

    .line 319
    const-string v0, "KeystringCommon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getKeystringBlockFlag key : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsBlocking:Z
    invoke-static {v0}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$200(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Z

    move-result v0

    .line 326
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 295
    sget-boolean v0, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v0, :cond_0

    .line 296
    const-string v0, "KeystringCommon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getUri key : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mUri:Landroid/net/Uri;
    invoke-static {v0}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$000(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Landroid/net/Uri;

    move-result-object v0

    .line 303
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasKeystring(Ljava/lang/String;)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    sget-boolean v0, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v0, :cond_0

    .line 285
    const-string v0, "KeystringCommon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hasKeystring key : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    :cond_0
    const/4 v0, 0x1

    .line 291
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGoogleKeystring(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    # getter for: Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->mIsGoogleKeystring:Z
    invoke-static {v0}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;->access$100(Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;)Z

    move-result v0

    .line 311
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHiddnemenuOn()Z
    .locals 1

    .prologue
    .line 1336
    iget-boolean v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->isHiddnemenu:Z

    return v0
.end method

.method public isVzwFamily()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->vzwFamily:Z

    return v0
.end method

.method public registReceiverForSpecialKeystring_TFN()V
    .locals 3

    .prologue
    .line 1276
    sget-boolean v1, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v1, :cond_0

    .line 1277
    const-string v1, "KeystringCommon"

    const-string v2, "registReceiverForSpecialKeystring_TFN"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1280
    :cond_0
    new-instance v1, Lcom/sec/android/app/parser/KeystringCommon$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/parser/KeystringCommon$2;-><init>(Lcom/sec/android/app/parser/KeystringCommon;)V

    iput-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mTFNReceiver:Landroid/content/BroadcastReceiver;

    .line 1295
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1296
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.factorymode.SEND_SPECIAL_KEYSTRING_ENABLED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1297
    const-string v1, "com.sec.android.app.factorymode.SEND_SPECIAL_KEYSTRING_DISABLED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1298
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/parser/KeystringCommon;->mTFNReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1299
    return-void
.end method

.method public registReceiverForSpecialKeystring_VZW()V
    .locals 3

    .prologue
    .line 1173
    sget-boolean v1, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v1, :cond_0

    .line 1174
    const-string v1, "KeystringCommon"

    const-string v2, "registReceiverForSpecialKeystring_VZW"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1177
    :cond_0
    new-instance v1, Lcom/sec/android/app/parser/KeystringCommon$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/parser/KeystringCommon$1;-><init>(Lcom/sec/android/app/parser/KeystringCommon;)V

    iput-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mVZWReceiver:Landroid/content/BroadcastReceiver;

    .line 1210
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1211
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.factorymode.SEND_SPECIAL_KEYSTRING_ENABLED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1212
    const-string v1, "com.sec.android.app.factorymode.SEND_SPECIAL_KEYSTRING_DISABLED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1213
    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/parser/KeystringCommon;->mVZWReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1214
    return-void
.end method

.method public registkeystrings()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 358
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mContext:Landroid/content/Context;

    const-string v6, "phone"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 359
    .local v3, "tm":Landroid/telephony/TelephonyManager;
    const/4 v2, 0x1

    .line 360
    .local v2, "mPhoneType":I
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v2

    .line 361
    if-nez v2, :cond_38

    move v1, v4

    .line 362
    .local v1, "isModelWifi":Z
    :goto_0
    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/parser/KeystrBlockCheck;->isModelCDMA()Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "TFN"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v4

    .line 379
    .local v0, "isModelGSM":Z
    :cond_0
    sget-boolean v5, Lcom/sec/android/app/parser/KeystringCommon;->isTMO:Z

    if-eqz v5, :cond_1

    .line 380
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#9999#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#9999#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#0514#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#0514#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#*#8663366#*#*"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#*#8663366#*#*"

    const-string v9, "8663366"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#12580*369#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#12580*369#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#06#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#06#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#0*#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#0*#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#66336#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#66336#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#0228#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#0228#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#7353#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#7353#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#7412365#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#7412365#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#7412365*"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#7412365*"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    sget-boolean v5, Lcom/sec/android/app/parser/KeystringCommon;->isOneChipSolution:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->device:Ljava/lang/String;

    const-string v6, "d2dcm"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->device:Ljava/lang/String;

    const-string v6, "k2kdi"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-boolean v5, Lcom/sec/android/app/parser/KeystringCommon;->isModelOrange:Z

    if-nez v5, :cond_2

    sget-boolean v5, Lcom/sec/android/app/parser/KeystringCommon;->isModelVFG:Z

    if-nez v5, :cond_2

    sget-boolean v5, Lcom/sec/android/app/parser/KeystringCommon;->isModelTelstra:Z

    if-eqz v5, :cond_3

    .line 456
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#9090#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#9090#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#86824#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#86824#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#638#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#638#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "ATT|AIO"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 483
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#*#4636#*#*"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#*#4636#*#*"

    const-string v9, "4636"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#2663#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#2663#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    sget-boolean v5, Lcom/sec/android/app/parser/KeystringCommon;->isModelTeleO2:Z

    if-eqz v5, :cond_5

    .line 523
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#487634751964#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#272837883#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#232337#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#232337#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "CHM|CTC"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 541
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#27663368378#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#27663368378#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    :cond_6
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "CHM|CTC"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 545
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#276633683782#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#276633683782#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    :cond_7
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "CTC"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 549
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#147235981#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#147235981#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    :cond_8
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "CTC"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 553
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#1472359810#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#1472359810#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    :cond_9
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "AMN|AMO|EVR|FTM|IDE|MST|ORA|ORO|ORS|TMP|TMU"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 561
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#301279#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#301279#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#878737837*66#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#878737837*66#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#878737837*633#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#878737837*633#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#0283#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#0283#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 597
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    const-string v6, "gt-p3113"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 599
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#34971539#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#34971539#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    :cond_b
    sget-boolean v5, Lcom/sec/android/app/parser/KeystringCommon;->isMarvell:Z

    if-eqz v5, :cond_39

    .line 621
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#0011#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#0011#"

    const-string v9, "0011_NETWORKINFO"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 625
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    const-string v6, "sm-g350"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    const-string v6, "sm-g313"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    const-string v6, "gt-s5312"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    const-string v6, "sm-g110"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    const-string v6, "sm-g130"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    const-string v5, "gt-i9082"

    iget-object v6, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    const-string v5, "gt-s5282"

    iget-object v6, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_c

    const-string v5, "gt-s7262"

    iget-object v6, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 627
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#00112#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#00112#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 657
    :cond_d
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#8736364#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#8736364#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 661
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#87976633#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#87976633#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 684
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#22558463#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#22558463#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 686
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#22228378#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#22228378#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 696
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "#7465625*638*#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "#7465625*638*#"

    const-string v9, "7465625*638*"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 697
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "#7465625*77*#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "#7465625*77*#"

    const-string v9, "7465625*77*"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 720
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "*#272*"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/parser/KeystringCommon;->mDeviceID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "#"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "*#272*"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/parser/KeystringCommon;->mDeviceID:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "#"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "83052020100812173552301071192687#"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 724
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "DTM|TNL|MAX|TMZ|CRO|COS|TPL|TMS|TMH|MBM|TTR|DTR|TNP|COA|TRG|AMN|AMM|AMO|DDE|DHR|DNL|DPL|TMO|DBB|DBE|DCO|DHE"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 725
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#272*62826#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#272*62826#"

    const-string v9, "83052020100812173552301071192687#"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 734
    :cond_e
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "SKT|SKC|SKO|KTT|KTC|KTO|LGT|LUC|LUO|KOR|ANY|KOO|TGY"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 735
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#248659#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#248659#"

    const-string v9, "248659"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 737
    :cond_f
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mCountryCode:Ljava/lang/String;

    const-string v6, "KOREA"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 738
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*66723646#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*66723646#"

    const-string v9, "66723646"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 750
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "319712358"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "319712358"

    const-string v9, "319712358"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 758
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "#758353266#646#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "#758353266#646#"

    const-string v9, "758353266"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 759
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*147359*682*"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*147359*682*"

    const-string v9, "147359"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 760
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "#5487587#682#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "#5487587#682#"

    const-string v9, "5487587"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 771
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#638732#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#638732#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 773
    const-string v5, "SKT"

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_10

    const-string v5, "SKC"

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_10

    const-string v5, "SKO"

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 774
    :cond_10
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*123456#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*123456#"

    const-string v9, "123456"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 788
    :cond_11
    const-string v5, "KTT"

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    const-string v5, "KTC"

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    const-string v5, "KTO"

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 790
    :cond_12
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*123456#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*123456#"

    const-string v9, "123456"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 795
    :cond_13
    const-string v5, "LGT"

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_14

    const-string v5, "LUC"

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_14

    const-string v5, "LUO"

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 796
    :cond_14
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#*#7415369#*#*"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#*#7415369#*#*"

    const-string v9, "7415369"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 800
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#57739#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#57739#"

    const-string v9, "MEDIASHARE"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 801
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "1478912358"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "1478912358"

    const-string v9, "1478912358"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 802
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##700629#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##700629#"

    const-string v9, "FACTINIT"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 803
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*123456#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*123456#"

    const-string v9, "LGTDEBUG"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 804
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##10306#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##10306#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 808
    :cond_15
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "SKT|SKC|SKO|LGT|LUC|LUO"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 809
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*1232580#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*1232580#"

    const-string v9, "232580"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 811
    :cond_16
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "KTT|KTC|KTO|LGT|LUC|LUO"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 812
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*147359#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*147359#"

    const-string v9, "47359"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 819
    :cond_17
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#*#87222#*#*"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#*#87222#*#*"

    const-string v9, "87222"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#222875#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#222875#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 824
    const-string v5, "SPH-L900"

    iget-object v6, p0, Lcom/sec/android/app/parser/KeystringCommon;->model:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 825
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#2627#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#2627#"

    const-string v9, "com.samsung.rmt_exercise"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 830
    :cond_18
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "CHM"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 831
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#87363645#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#87363645#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 864
    :cond_19
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#24320#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#24320#"

    const-string v9, "22623277326634424320#"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 865
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#243203855#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#243203855#"

    const-string v9, "83052020100812173552301071192687#"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 871
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#07#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#07#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 879
    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "VZW"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1a

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "USC"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1a

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "MTR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1a

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "XAR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1a

    const-string v5, "CRI"

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3a

    .line 881
    :cond_1a
    sget-boolean v5, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v5, :cond_1b

    .line 882
    const-string v5, "KeystringCommon"

    const-string v6, "CSC_CODE :vzw"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    :cond_1b
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##7764726"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##7764726"

    const-string v9, "PROGRAM"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 889
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "47*68#13580"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "47*68#13580"

    const-string v9, "TESTMODE"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 904
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##72786#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##72786#"

    const-string v9, "SCRTN"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 907
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##889#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##889#"

    const-string v9, "TTY"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 915
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "**7838"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "**7838"

    const-string v9, "STEALTHMODE"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 922
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "**6828378"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "**6828378"

    const-string v9, "OTATEST"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1047
    :cond_1c
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#272837883#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#272837883#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1050
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "AIO|BBY|CRI|CSP|MTR|SPR|STA|TRF|USC|VMU|XAR|XAS|DTR|VDP|VIT|VIA|VDR"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 1051
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#467#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#467#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1052
    :cond_1d
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "ATT|TMB|FTM"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 1053
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#77467#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#77467#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1054
    :cond_1e
    iget-boolean v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v7, "VMU|ATT|CRI|SPR|TRF|AIO|BBY|BST|CSP|MTR|STA|XAR|XAS|USC"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 1055
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#*#2432546#*#*"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#*#2432546#*#*"

    const-string v9, "2432546"

    invoke-direct {v7, v8, v9, v4}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1056
    :cond_1f
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#*#8255#*#*"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#*#8255#*#*"

    const-string v9, "8255"

    invoke-direct {v7, v8, v9, v4}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1057
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#1106#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#1106#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1068
    const-string v4, "TFN"

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 1069
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "##72786#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "##72786#"

    const-string v8, "SCRTN"

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1070
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "##889#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "##889#"

    const-string v8, "TTY"

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1071
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "3215987123580"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "3215987123580"

    const-string v8, "TESTMODE"

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1076
    :cond_20
    const-string v4, "mrvl"

    const-string v5, "ro.board.platform"

    const-string v6, "Unknown"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 1077
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#6278355872#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#6278355872#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1080
    :cond_21
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "CHC|CHM|CHN|CHU|CTC"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_22

    const-string v4, "mrvl"

    const-string v5, "ro.board.platform"

    const-string v6, "Unknown"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 1082
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#62783553424#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#62783553424#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086
    :cond_22
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "LAO|CLM|MXI|XME|MYA|XEV|XXV|MM1|SIN|STH|XSP|XSE|CAM|THL|THW|GLB|SMA|XTC|XTE|ATT|TMB|AIO|TRF|MTR|BMC|RWC|TLS|VTR|BWA|FMC|KDO|GLW|PCM|MTA|SPC|MCT|SOL|ESK|VMC|AMN|AMO|EVR|FTM|IDE|MST|ORA|ORO|ORS|TMP|TMU|CHM|DTM|TMZ|DTR"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 1087
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#2263#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#2263#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1089
    :cond_23
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "LAO|CLM|MXI|XME|MYA|XEV|XXV|MM1|SIN|STH|XSP|XSE|CAM|THL|THW|GLB|SMA|XTC|XTE|ATT|TMB|AIO|TRF|MTR|BMC|RWC|TLS|VTR|BWA|FMC|KDO|GLW|PCM|MTA|SPC|MCT|SOL|ESK|VMC|AMN|EVR|FTM|IDE|MST|OPT|ORA|ORG|ORO|ORS|TMP|TMU|CHM"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 1090
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#22632#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#22632#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1094
    :cond_24
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#745#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#745#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1096
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "|ATT|TMB|AIO|TRF|MTR|BMC|RWC|TLS|VTR|BWA|FMC|KDO|GLW|PCM|MTA|SPC|MCT|SOL|ESK|VMC|AMN|AMO|EVR|FTM|IDE|MST|ORA|ORO|ORS|TMP|TMU|CHN"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 1097
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#197328640#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#197328640#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1099
    :cond_25
    const-string v4, "CHM"

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_26

    .line 1100
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#2255369273#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#2255369273#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1103
    :cond_26
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "CHM"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_27

    .line 1104
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#83583872#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#83583872#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1107
    :cond_27
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "ATT|TMB|AIO|TRF|MTR"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_28

    .line 1108
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#1*#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#1*#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1109
    :cond_28
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "VZW|TMB"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_29

    .line 1110
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#1234#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#1234#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112
    :cond_29
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "SPR|BST|VMU|XAS"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 1113
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "##4382#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "##4382#"

    const-string v8, "com.samsung.operator_defined"

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1115
    :cond_2a
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "VZW|USC|MTR|XAR|SKT|SKC|SKO|KTT|KTC|KTO|LGT|LUC|LUO|ANY|KOO|TMB"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2c

    .line 1116
    if-nez v1, :cond_2b

    sget-boolean v4, Lcom/sec/android/app/parser/KeystringCommon;->isMarvell:Z

    if-eqz v4, :cond_40

    .line 1117
    :cond_2b
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#1111#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#1111#"

    const-string v8, "1111_WIFI"

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1121
    :cond_2c
    :goto_3
    if-nez v0, :cond_2d

    if-eqz v1, :cond_2f

    :cond_2d
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "VZW|SPR|XAS|VMU|BST|USC|MTR|XAR|SKT|SKC|SKO|KTT|KTC|KTO|LGT|LUC|LUO|ANY|KOO|TMB"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2f

    .line 1122
    if-nez v1, :cond_2e

    sget-boolean v4, Lcom/sec/android/app/parser/KeystringCommon;->isMarvell:Z

    if-eqz v4, :cond_41

    .line 1123
    :cond_2e
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#2222#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#2222#"

    const-string v8, "2222_WIFI"

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1127
    :cond_2f
    :goto_4
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "TRF"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_30

    .line 1128
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "##675#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "##675#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1129
    :cond_30
    if-nez v0, :cond_31

    if-eqz v1, :cond_32

    :cond_31
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "VZW|USC|XAR|SKT|SKC|SKO|KTT|KTC|KTO|LGT|LUC|LUO|ANY|KOO|BST|SPR|VMU|XAS"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_32

    .line 1130
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#0808#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#0808#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1132
    :cond_32
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "ILO|CEL|PCL|PTR|MIR"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_33

    .line 1133
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "*#170582*"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mDeviceID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "*#170582*"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/parser/KeystringCommon;->mDeviceID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "#"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "83052020100812173552301071192687#"

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1136
    :cond_33
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "SPR|BST|VMU|XAS"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_34

    .line 1137
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "##3282#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "##3282#"

    const-string v8, "DATA"

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1139
    :cond_34
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "VMU|SPR|BST|XAS|TRF"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_35

    .line 1140
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "##786#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "##786#"

    const-string v8, "RTN"

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1141
    :cond_35
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "VMU|CHM"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_36

    .line 1142
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#7284#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#7284#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1143
    :cond_36
    iget-boolean v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->isACG:Z

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "TMB|MTR"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/parser/KeystringCommon;->checkSalesCode(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_37

    .line 1144
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#8888#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#8888#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1147
    :cond_37
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#9900#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#9900#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1148
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#877#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#877#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1150
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#477#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#477#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151
    return-void

    .end local v0    # "isModelGSM":Z
    .end local v1    # "isModelWifi":Z
    :cond_38
    move v1, v0

    .line 361
    goto/16 :goto_0

    .line 623
    .restart local v0    # "isModelGSM":Z
    .restart local v1    # "isModelWifi":Z
    :cond_39
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#0011#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#0011#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 940
    :cond_3a
    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "SPR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3b

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "VMU"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3b

    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "BST"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3b

    const-string v5, "XAS"

    sget-object v6, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3d

    .line 942
    :cond_3b
    sget-boolean v5, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v5, :cond_3c

    .line 943
    const-string v5, "KeystringCommon"

    const-string v6, "CSC_CODE : spr"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 945
    :cond_3c
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##33284#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##33284#"

    const-string v9, "DEBUG"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 948
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "3215987123580"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "3215987123580"

    const-string v9, "TESTMODE"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 950
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##3424#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##3424#"

    const-string v9, "PUTIL"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 956
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##4772579#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##4772579#"

    const-string v9, "GPSCLRX"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 958
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##564#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##564#"

    const-string v9, "LOG"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 959
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##66236#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##66236#"

    const-string v9, "OMADM"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 960
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##66264#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##66264#"

    const-string v9, "OMANI"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 961
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##873283#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##873283#"

    const-string v9, "UPDATE"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 964
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##72786#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##72786#"

    const-string v9, "SCRTN"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 967
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##889#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##889#"

    const-string v9, "TTY"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 978
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "**7838"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "**7838"

    const-string v9, "STEALTHMODE"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 982
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##737425#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##737425#"

    const-string v9, "SERIALSET"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 984
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##2627#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##2627#"

    const-string v9, "com.samsung.rmt_exercise"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 991
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##8378#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##8378#"

    const-string v9, "TEST"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1018
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##6343#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##6343#"

    const-string v9, "MEID"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 1023
    :cond_3d
    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "CTC"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3f

    .line 1024
    sget-boolean v5, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v5, :cond_3e

    .line 1025
    const-string v5, "KeystringCommon"

    const-string v6, "CSC_CODE : CTC"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1028
    :cond_3e
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "*#0000#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "*#0000#"

    invoke-direct {v7, v8}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1029
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "##33284#"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "##33284#"

    const-string v9, "DEBUG"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1030
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "3215987123580"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "3215987123580"

    const-string v9, "TESTMODE"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 1036
    :cond_3f
    sget-object v5, Lcom/sec/android/app/parser/KeystringCommon;->mSalesCode:Ljava/lang/String;

    const-string v6, "KDI"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 1037
    iget-object v5, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v6, "3215987123580"

    new-instance v7, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v8, "3215987123580"

    const-string v9, "TESTMODE"

    invoke-direct {v7, v8, v9}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 1119
    :cond_40
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#1111#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#1111#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 1125
    :cond_41
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    const-string v5, "*#2222#"

    new-instance v6, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;

    const-string v7, "*#2222#"

    invoke-direct {v6, v7}, Lcom/sec/android/app/parser/KeystringCommon$KeystringInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4
.end method

.method public removeBlockedKeystrings()V
    .locals 5

    .prologue
    .line 206
    iget-object v3, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    .line 207
    .local v1, "keyStrings":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v2

    .line 209
    .local v2, "mKeystringListSize":I
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 210
    sget-object v4, Lcom/sec/android/app/parser/KeystringCommon;->mKeystrBlockCheck:Lcom/sec/android/app/parser/KeystrBlockCheck;

    aget-object v3, v1, v0

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/parser/KeystrBlockCheck;->isKeystringAlwaysOpen(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 211
    iget-object v4, p0, Lcom/sec/android/app/parser/KeystringCommon;->mKeystringList:Ljava/util/HashMap;

    aget-object v3, v1, v0

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 214
    :cond_1
    return-void
.end method

.method public unregistReceiverForSpecialKeystring_TFN()V
    .locals 2

    .prologue
    .line 1340
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mTFNReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 1341
    sget-boolean v0, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v0, :cond_0

    .line 1342
    const-string v0, "KeystringCommon"

    const-string v1, "unregistReceiverForSpecialKeystring_TFN"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1345
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mTFNReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1346
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mTFNReceiver:Landroid/content/BroadcastReceiver;

    .line 1348
    :cond_1
    return-void
.end method

.method public unregistReceiverForSpecialKeystring_VZW()V
    .locals 2

    .prologue
    .line 1217
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mVZWReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 1218
    sget-boolean v0, Lcom/sec/android/app/parser/ParseService;->IS_DEBUG:Z

    if-eqz v0, :cond_0

    .line 1219
    const-string v0, "KeystringCommon"

    const-string v1, "unregistReceiverForSpecialKeystring_VZW"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1222
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/parser/KeystringCommon;->mVZWReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1223
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->mVZWReceiver:Landroid/content/BroadcastReceiver;

    .line 1225
    :cond_1
    return-void
.end method

.method public updateHiddnemenu()V
    .locals 1

    .prologue
    .line 1332
    invoke-static {}, Lcom/sec/android/app/parser/KeystringCommon;->checkHiddnemenu()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/parser/KeystringCommon;->isHiddnemenu:Z

    .line 1333
    return-void
.end method
